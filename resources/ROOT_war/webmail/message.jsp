<%@ page errorPage="errorpage.jsp" 
%><%@ page language="java" import="javax.mail.*,javax.mail.search.*,java.text.DateFormat" 
%><%@ include file="include.jsp" 
%><%!
private static final DateFormat FORMATTER = DateFormat.getDateTimeInstance();
private static final String MULTIPART = "multipart/*";
private static final String PLAIN = "text/plain";
private static final String HTML = "text/html";
%><%
	// Cache-control
	response.setHeader("Cache-Control","no-store"); //HTTP 1.1
	response.setHeader("Pragma","no-cache"); //HTTP 1.0
	response.setDateHeader("Expires", 0); //prevents caching at the proxy server

	// If no username then redirect to login form
	String username = trim(request.getParameter("u"));
	if (username == null) {
	    this.getServletContext().getRequestDispatcher("index.jsp").forward(request, response);
	    return;
	}
	// If no message unique id then redirect to inbox
	String idString = trim(request.getParameter("id"));
	if (idString == null) {
	    this.getServletContext().getRequestDispatcher("inbox.jsp").forward(request, response);
	    return;
	}
	// Password defaults to username, hostname defaults to localhost and port defaults to -1
	String password = trim(request.getParameter("p"));
	if (password == null)
	    password = username;
	String hostname = trim(request.getParameter("h"));
	if (hostname == null)
	    hostname = "localhost";
	String portString = trim(request.getParameter("port"));
	int port = portString == null ? -1 : Integer.parseInt(portString);
	// Load messages from inbox; imap folder must be UIDFolder
	Folder folder = null;
	UIDFolder uidFolder = null;
	Message message = null;
	Throwable throwable = null;
	try {
		folder = Session.getDefaultInstance(System.getProperties()).getFolder(new URLName(
			"imap", hostname, port, "INBOX", username, password));
		uidFolder = (UIDFolder)(folder);
		folder.open(Folder.READ_WRITE);
	    message = uidFolder.getMessageByUID(Long.parseLong(idString));
	} catch (Throwable messagingThrowable) {
	    throwable = messagingThrowable;
	    if (folder != null)
			try {
			    folder.close(false);
			} catch (Throwable closeThrowable) {
			}
	}
%><html><head><title>Message for <%= username %></title></head><body>
<h1>Message for <%= username %></h1>
<%
	if (message == null) {
%>
	<span style="color: red">No message #<%= idString %> found</span>
<%
		if ((throwable != null) && !(throwable instanceof AuthenticationFailedException)) {
%>
			<p style="color: red; "><pre><% throwable.printStackTrace(new java.io.PrintWriter(out)); %></pre></p>
<%
		}
	} else {
%>
	Date: <%= message.getSentDate() == null ? "n/a" : FORMATTER.format(message.getSentDate()) %><br />
	From: <%= encode(message.getFrom(), "unknown") %><br />
	To: <%= encode(message.getRecipients(Message.RecipientType.TO), "none") %><br />
	Reply-To: <%= encode(message.getReplyTo(), "none") %><br />
	CC: <%= encode(message.getRecipients(Message.RecipientType.CC), "none") %><br />
	Subject: <%= message.getSubject() == null ? "{no subject}" : encode(message.getSubject()) %><br />
<%
		if (message.isMimeType(HTML)) { %>
<%= message.getContent() %>
<%
		} else if (message.isMimeType(PLAIN)) { %>
	<PRE>
<%= message.getContent() %>
	</PRE>
<%
		} else if (message.isMimeType(MULTIPART)) {
		    Multipart multipart = (Multipart) message.getContent();
    		for (int index = 0; index < multipart.getCount(); index++) {
    		    BodyPart bodyPart = multipart.getBodyPart(index);
				if (bodyPart.isMimeType(PLAIN)) {
%>
    <hr><pre><%= bodyPart.getContent() %></pre>
<%
				} else if (bodyPart.isMimeType(HTML)) {
%>
    <hr><%= bodyPart.getContent() %>
<%
				} else {
%>
	<hr>Attachment of type <%= encode(bodyPart.getContentType()) %>
<%
				} // else BodyPart.isMimeType()
    		} // for Multipart.getCount()
		} // else Message.isMimeType()
		try {
			folder.close(true);
		} catch (Throwable closeThrowable) {
		}
	} // else message not null
%>
</body>
</html>

