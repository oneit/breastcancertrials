<%@ page errorPage="errorpage.jsp" 
%><%@ page language="java" import="javax.mail.*,javax.mail.search.*,java.text.DateFormat" 
%><%@ include file="include.jsp" 
%><%!
private static final DateFormat FORMATTER = DateFormat.getDateInstance(DateFormat.SHORT);
%><%
	// Cache-control
	response.setHeader("Cache-Control","no-store"); //HTTP 1.1
	response.setHeader("Pragma","no-cache"); //HTTP 1.0
	response.setDateHeader("Expires", 0); //prevents caching at the proxy server

	// If no username then redirect to login form
	String username = trim(request.getParameter("u"));
	if (username == null) {
	    this.getServletContext().getRequestDispatcher("index.jsp").forward(request, response);
	    return;
	}
	// Password defaults to username, hostname defaults to localhost and port defaults to -1
	String password = trim(request.getParameter("p"));
	if (password == null)
	    password = username;
	String hostname = trim(request.getParameter("h"));
	if (hostname == null)
	    hostname = "localhost";
	String portString = trim(request.getParameter("port"));
	int port = portString == null ? -1 : Integer.parseInt(portString);
	// Load messages from inbox; imap folder must be UIDFolder
	Folder folder = null;
	UIDFolder uidFolder = null;
	Message[] messages = null;
	Throwable throwable = null;
	try {
		folder = Session.getDefaultInstance(System.getProperties()).getFolder(new URLName(
			"imap", hostname, port, "INBOX", username, password));
		uidFolder = (UIDFolder)(folder);
		folder.open(Folder.READ_WRITE);
	    messages = folder.search(new FlagTerm(new Flags(Flags.Flag.DELETED), false));
	} catch (Throwable messagingThrowable) {
	    throwable = messagingThrowable;
	    if (folder != null)
			try {
			    folder.close(false);
			} catch (Throwable closeThrowable) {
			}
	}
%><html><head><title>Messages for <%= username %></title></head><body>
<h1>Messages for <%= username %></h1>
<table width="100%" border="2"><tr><th>Sent</th><th>From</th><th>Subject</th></tr>
<%
	if ((messages == null) || (messages.length == 0)) {
%>
	<tr><td colspan="3" style="color: red">No messages found</td></tr>
<%
		if ((throwable != null) && !(throwable instanceof AuthenticationFailedException)) {
%>
			<tr><td colspan="3" style="color: red"><pre><% throwable.printStackTrace(new java.io.PrintWriter(out)); %></pre></td></tr>
<%
		}
	} else {
	    for (Message message : messages) {
%>
	<tr><td><%= message.getSentDate() == null ? "n/a" : FORMATTER.format(message.getSentDate()) %></td>
		<td><%= encode(message.getFrom(), "unknown") %></td>
		<td><a href="message.jsp?u=<%=encode(username)%>&p=<%=encode(password)%>&h=<%=encode(hostname)%>&port=<%=port%>&id=<%=uidFolder.getUID(message)%>">
		<%= message.getSubject() == null ? "{no subject}" : encode(message.getSubject()) %></a></td></tr>
<%
		} // for
		try {
			folder.close(true);
		} catch (Throwable closeThrowable) {
		}
	} // if messages not null
%>
</table>
</body>
</html>

