<%@ page language="java" import="javax.mail.Address" 
%><%!
private static final String trim(String string) {
    if (string == null)
		return null;
    string = string.trim();
	return (string.length() == 0) ? null : string;
};
private static final String encode(String string) {
    return string.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;").replace("@","&#64;");
}
private static final String encode(Address[] addresses, String def) {
    String string = "";
    if (addresses != null)
		for (Address address : addresses)
			string += ' ' + encode(address.toString());
    string = string.trim();
    return (string.length() == 0) ? def : string;
}
%>