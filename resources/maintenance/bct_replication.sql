-- bct_replication.sql
-- Refreshes trial tables in the bct/pdq and bct/public schemas

-- PDQ schema refresh; trialsite is the only join table
delete from pdq.pdq_trialsite;
delete from pdq.pdq_contact;
delete from pdq.pdq_site;
delete from pdq.pdq_trial;
copy pdq.pdq_contact from '/var/lib/pgsql/backups/dev/shm/pdq_contact.bin' BINARY;
copy pdq.pdq_site from '/var/lib/pgsql/backups/dev/shm/pdq_site.bin' BINARY;
copy pdq.pdq_trial from '/var/lib/pgsql/backups/dev/shm/pdq_trial.bin' BINARY;
copy pdq.pdq_trialsite from '/var/lib/pgsql/backups/dev/shm/pdq_trialsite.bin' BINARY;
-- public schema: Delete join tables first
delete from trialsite;
delete from screenergroup_site;
delete from screenergroup_userscreener;
-- Delete independent tables
delete from contact;
delete from site;
delete from trial;
delete from trialevent;
delete from screenergroup;
delete from userscreener;
-- Import independent tables
copy contact from '/var/lib/pgsql/backups/dev/shm/contact.bin' BINARY;
copy site from '/var/lib/pgsql/backups/dev/shm/site.bin' BINARY;
copy trial from '/var/lib/pgsql/backups/dev/shm/trial.bin' BINARY;
copy trialevent from '/var/lib/pgsql/backups/dev/shm/trialevent.bin' BINARY;
copy screenergroup from '/var/lib/pgsql/backups/dev/shm/screenergroup.bin' BINARY;
copy userscreener from '/var/lib/pgsql/backups/dev/shm/userscreener.bin' BINARY;
-- Import join tables last
copy trialsite from '/var/lib/pgsql/backups/dev/shm/trialsite.bin' BINARY;
copy screenergroup_site from '/var/lib/pgsql/backups/dev/shm/screenergroup_site.bin' BINARY;
copy screenergroup_userscreener from '/var/lib/pgsql/backups/dev/shm/screenergroup_userscreener.bin' BINARY;
