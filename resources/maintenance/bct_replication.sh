#!/bin/bash
# Copy zipped binary trial data from production
# Note: using OpenSSH RSA public/private key
/usr/bin/scp -p username@server:/dev/shm/post-update.zip /var/lib/pgsql/backups
# Unzip file
/usr/bin/unzip -o /var/lib/pgsql/backups/post-update.zip -d /var/lib/pgsql/backups
# Replace trial data
/usr/bin/psql -d bct -f /var/lib/pgsql/bct_replication.sql
