# Dump bct database each evening at 11:00PM into rotating backup files
0 23 * * * /home/pg_dump/rotating_backups.sh >> /dev/null 2>&1
# Dump bct trial data to /dev/shm each Sunday morning at 6:00AM
0 6 * * sun /home/pg_dump/bct_trial_copy_binary.sh >> /dev/null 2>&1
# Replicate production trial data each Sunday at 7AM
# 0 7 * * sun /var/lib/pgsql/bct_replication.sh >> /dev/null 2>&1
