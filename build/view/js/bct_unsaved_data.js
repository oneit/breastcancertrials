var needToConfirm = false;
var checkConfirmCount = false;
window.onbeforeunload=function() {
	if (needToConfirm) {
		if (!checkConfirmCount || checkCount('BCTSavePromptCount'))
			return "You have not saved your Health History data yet. If you leave this page, you may lose the data you've entered. " +
				"People who save their health history can learn about newly listed trials by signing up for our Trial Alert Service.";
	}
}
function checkCount(cookieName) {
	var count = GetCookie(cookieName);
	var expirationDate = new Date();
	expirationDate.setTime(expirationDate.getTime() + (1*60*60*1000)); // 1 hour from now
	if (count == null) {
		count=1;
		SetCookie(cookieName, count, expirationDate);
		return true;
	} else {
		count++;
		SetCookie(cookieName, count, expirationDate);
		return false;
	}
}
