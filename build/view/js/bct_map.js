// Google maps code for BreastCancerTrials.org
var map;
function loadMap(id, lat, lng, zoom, titHm, latHm, lngHm ) { // Load on demand
	if ((null==id) || !GBrowserIsCompatible()) return;
	if (map == null) {
		map = new GMap2(document.getElementById("map"));
		map.addControl(new GLargeMapControl()); map.addControl(new GMapTypeControl()); map.addControl(new GOverviewMapControl());
	} else map.clearOverlays();
	map.setCenter(new GLatLng(lat, lng), zoom);
	if ((null!=titHm) && (null!=latHm) && (null!=lngHm)) { // Home icon
		var homeIcon = new GIcon();
		homeIcon.image = "./images/icn_home.png"; homeIcon.iconSize = new GSize(20, 18); homeIcon.iconAnchor = new GPoint(10, 9);
		map.addOverlay(new GMarker(new GLatLng(latHm, lngHm), {icon:homeIcon,title:titHm,clickable:false}));
	}
	var siteIcon = new GIcon(); // Each site's data loaded from XML into a marker
	siteIcon.image = "./images/icn_g.png"; siteIcon.iconSize = new GSize(20, 34); siteIcon.iconAnchor = new GPoint(9, 34);
	GDownloadUrl("./sitedata/" + id + ".xml", function(data) {
		var xml = GXml.parse(data);
		var markers = xml.documentElement.getElementsByTagName("site");
		for (var index = 0; index < markers.length; index++) { // First link in site's div is clicked when marker clicked
			var siteId = "s" + markers[index].getAttribute("id"); var siteName = markers[index].getAttribute("name");
			var siteCoordinate = new GLatLng(parseFloat(markers[index].getAttribute("lat")), parseFloat(markers[index].getAttribute("lng")));
			map.addOverlay(createMarker(siteIcon, siteCoordinate, siteName, document.getElementById(siteId).getElementsByTagName('a')[0]));
		}
	});
} // loadMap()
window.onunload = function() {if (map) {GUnload();}} // Clean up browser memory leaks on unload

function createMarker(icn, coordinate, tooltip, div) {
	var newMarker;
	if (null==div) newMarker = new GMarker(coordinate, {icon:icn,title:tooltip,clickable:false});
	else { // Clicking on marker will either jump to div's href or call its onclick
		newMarker = new GMarker(coordinate, {icon:icn,title:tooltip,clickable:true});
		GEvent.addListener(newMarker, "click", function() {if (div.href.length>0) window.location.href=div.href; else div.onclick();});
	}
	return newMarker;
} // createMarker()

// Load trial on demand, 1.5 seconds after it is asked for so fade animation finishes
function animap(id, lat, lng, zip, latZip, lngZip) {
	if (map == null) {
		setTimeout( function() {
			var titZip = null;
			if (null!=zip) { // Pad zip code to five chars
				zip = zip.toString();
				while(zip.length<5) zip='0'+zip;
				titZip = 'Zip ' + zip;
			}
			loadMap(id, lat, lng, 9, titZip, latZip, lngZip);
			// Resize the map because the hidden div may have width/height of zero
			if (null!=map) map.checkResize();
		}, 1500 );
	}
}
