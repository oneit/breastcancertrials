package org.quantumleaphealth.utility;

import java.util.HashSet;

/**
 * Class implementing a basic discrete finite automata of Lucene Search Strings.  We parse the search string ourselves
 * so that we can format the search string with lucene control characters to add some desired default search behavior.
 * 
 * @author wgweis
 *
 */
public class LuceneSearchStringFormatter 
{
//	private static final String NORMAL_SEARCH_TOKEN_SUFFIX = "*";  // allow a wild card search with exact match.
//	private static final String NORMAL_SEARCH_TOKEN_PREFIX = "*";  // allow a wild card search with exact match.

	private static final String NORMAL_SEARCH_TOKEN_SUFFIX = "";  // allow a wild card search with exact match.
	private static final String NORMAL_SEARCH_TOKEN_PREFIX = "";  // allow a wild card search with exact match.

	
	/**
	 * Do not apply any special handling to lucene boolean operators.
	 */
	private static final HashSet<String> BOOLEAN_OPERATORS = new HashSet<String>();
	static
	{
		BOOLEAN_OPERATORS.add("AND"); 
		BOOLEAN_OPERATORS.add("OR"); 
		BOOLEAN_OPERATORS.add("NOT");
	}
	
	/**
	 * Return a quote term.  Consumes the search string until a matching quote is found or the end of the search string
	 * is reached.
	 * @param s
	 * @param ndx
	 * @return The double quoted search term
	 */
	private String getQuoteTerm(char[] s, StringNdx ndx)
	{
		StringBuilder quoteTerm = new StringBuilder();
		quoteTerm.append(s[ndx.value++]);  // capture first Character
		while (ndx.value < s.length && s[ndx.value] != '"')
		{
			quoteTerm.append(s[ndx.value++]);
		}
		
		if (ndx.value < s.length)
		{
			quoteTerm.append(s[ndx.value]); // capture closing double quote
		}
		return quoteTerm.toString();
	}
	
	/**
	 * Return a term surrounded by parentheses (the lucence grouping character).  Calls getQuoteTerm if a double quote character is
	 * encountered.
	 * @param s
	 * @param ndx
	 * @return the search term surrounded by parentheses.
	 */
	private String getParenthesesTerm(char[] s, StringNdx ndx)
	{
		StringBuilder parenthesesTerm = new StringBuilder();
		parenthesesTerm.append(s[ndx.value++]);  // capture first character
		int braceLevel = 1;
		
		while (ndx.value < s.length && braceLevel > 0)
		{
			if (s[ndx.value] == '(')
			{
				braceLevel++;
			}
			else if (s[ndx.value] == ')')
			{
				braceLevel--;
			}
			else if (s[ndx.value] == '"')
			{
				parenthesesTerm.append(getQuoteTerm(s, ndx));
				ndx.value++;
				continue;
			}
			parenthesesTerm.append(s[ndx.value++]);
		}
		return parenthesesTerm.toString();
	}

	/**
	 * if neither a double quote character or an open parentheses is encountered this is a normal term
	 * and we consume the search string until we reach the end of it, or until we encounter a white space
	 * character.
	 * @param s
	 * @param ndx
	 * @return search term with whitespace removed from the beginning and end.
	 */
	private String getNormalTerm(char[] s, StringNdx ndx)
	{
		StringBuilder normalTerm = new StringBuilder();
		normalTerm.append(s[ndx.value++]);  // capture first quote as we preserve the string inside quotes.
		while (ndx.value < s.length && !Character.isWhitespace(s[ndx.value]))
		{
			normalTerm.append(s[ndx.value++]);
		}
		return normalTerm.toString();
	}

	/**
	 * normal search terms may have a suffix appended to them.  Currently, we are using fuzzy search with a factor of one.
	 * If the normal term does not have a letter character as its last character, we assume the user has already appended
	 * one of lucene's special characters such as a wild card character (asterisk) and leave it alone.
	 * @param searchTerm
	 * @return searchTerm as is, or searchTerm with a suffix. 
	 */
	private String formatSearchTerm(String searchTerm)
	{
		String formattedSearchString = searchTerm == null? "": searchTerm;
		if (formattedSearchString.length() > 0 && Character.isLetterOrDigit(formattedSearchString.charAt(searchTerm.length() - 1)) && !BOOLEAN_OPERATORS.contains(searchTerm))
		{
			return NORMAL_SEARCH_TOKEN_PREFIX +  formattedSearchString + NORMAL_SEARCH_TOKEN_SUFFIX;  // fuzzy search with 1 character miss allowance.
		}
		return formattedSearchString;
	}
	
	/**
	 * The only public method for this class.  Given the user's search string, return a formatted version that includes 
	 * lucene control characters we may wish to add to control lucene's default search behavior.
	 * 
	 * @param searchString
	 * @return the formatted search string.
	 */
	public String formatSearchString(String searchString)
	{
		StringBuilder formattedSearchString = new StringBuilder();
		StringNdx ndx = new StringNdx(0);
		
		char[] s = searchString.toCharArray();
		while (ndx.value < s.length)
		{
			String currentTerm = "";
			
			// advance to first non-white space character
			while (ndx.value < s.length && Character.isWhitespace(s[ndx.value]))
			{
				ndx.value++;
			}
			
			if (ndx.value < s.length)
			{	
				switch(s[ndx.value])
				{
				case '"': currentTerm = getQuoteTerm(s, ndx);
						  break;
						  
				case '(': currentTerm = getParenthesesTerm(s, ndx);
						  break;
						  
				default: currentTerm = getNormalTerm(s, ndx);
				  		 break;
				}
			}
			
			if (currentTerm.length() > 0)
			{	
				formattedSearchString.append(formatSearchTerm(currentTerm));
				formattedSearchString.append(" ");
			}	
			ndx.value++;
		}
		return formattedSearchString.toString().trim();
	}
	
	/**
	 * A main class for testing purposes only.
	 * @param argv
	 */
	public static void main(String... argv)
	{
		String searchString = "01401959";
		System.err.println("Final String looks like this: Z" + new LuceneSearchStringFormatter().formatSearchString(searchString) + "Z");
	}
	
	/**
	 * Inner class to allow us to pass the string integer index by reference to the term methods, so that it can be incremented by them. 
	 * 
	 * @author wgweis
	 *
	 */
	private class StringNdx
	{
		private Integer value;
		StringNdx(int startValue)
		{
			value = new Integer(startValue);
		}
	}
}
