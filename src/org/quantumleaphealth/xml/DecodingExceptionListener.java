/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.xml;

import java.beans.ExceptionListener;
import java.beans.XMLDecoder;
import java.io.InputStream;
import java.util.LinkedList;

/**
 * Stores exceptions that occur while unmarshaling objects from XML format.
 * 
 * @author Tom Bechtold
 * @version 2008-02-19
 */
public class DecodingExceptionListener extends LinkedList<Exception> implements ExceptionListener
{
	/**
	 * Add the exception to the list
	 * 
	 * @see java.beans.ExceptionListener#exceptionThrown(java.lang.Exception)
	 */
	public void exceptionThrown(Exception exception)
	{
		add(exception);
	}

	/**
	 * Decodes one object from an input stream. This method is a convenience for
	 * calling <code>XMLDecoder</code>, listening for decoding exceptions and
	 * returning one result.
	 * 
	 * @param inputStream
	 *            the stream that contains the marshaled object
	 * @return the decoded object
	 * @throws DecodingException
	 *             if <code>inputStream</code> is <code>null</code> or if there
	 *             were any exceptions during decoding or if the resulting
	 *             object is <code>null</code>
	 */
	public static Object decode(InputStream inputStream) throws DecodingException
	{
		if (inputStream == null)
			throw new DecodingException();
		DecodingExceptionListener exceptionListener = new DecodingExceptionListener();
		XMLDecoder decoder = new XMLDecoder(inputStream, exceptionListener, exceptionListener);
		Object object = decoder.readObject();
		decoder.close();
		// Validate result
		if (!exceptionListener.isEmpty())
			throw new DecodingException(exceptionListener.size(), exceptionListener.getFirst());
		if (object == null)
			throw new DecodingException();
		return object;
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -9094740056444506834L;
}
