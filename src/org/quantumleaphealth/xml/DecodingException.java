/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.xml;

/**
 * Indicates that exceptions where thrown when unmarshaling an object from XML
 * format.
 * 
 * @author Tom Bechtold
 * @version 2008-02-19
 */
public class DecodingException extends Exception
{

	/**
	 * The number of exceptions encountered while decoding the XML
	 */
	private final int count;

	/**
	 * Indicates that the object returned by decoding was null
	 */
	public DecodingException()
	{
		super();
		count = 0;
	}

	/**
	 * @param firstException
	 *            the first
	 */
	public DecodingException(int count, Exception firstException)
	{
		super(firstException);
		this.count = count;
	}

	/**
	 * @return the number of exceptions encountered while decoding the XML
	 */
	public int getCount()
	{
		return count;
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -922649020997115946L;
}
