/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.client;

import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.quantumleaphealth.action.EngineManager;
import org.quantumleaphealth.action.PatientsAssociationManager;

/**
 * Reloads the trial matching engine and refreshes the patient's associations on
 * a remote server. TODO: Add JNDI authentication
 * 
 * @author Tom Bechtold
 * @version 2009-06-17
 * @see EngineManager
 * @see PatientsAssociationManager
 */
public class EngineManagerClient
{

	/**
	 * Logger for debugging purposes is registered to this class' name
	 */
	private static final Logger LOGGER = Logger.getLogger(EngineManagerClient.class.getName());

	/**
	 * The default JNDI location of <code>EngineManager</code> remote instance.
	 */
	private static final String JNDI_ENGINEMANAGER = "bct_nation/EngineManagerAction/remote";
	/**
	 * The default JNDI location of <code>PatientsAssociationManager</code>
	 * remote instance.
	 */
	private static final String JNDI_PATIENTSASSOCIATIONMANAGER = "bct_nation/PatientsAssociationManagerAction/remote";

	/**
	 * Calls <code>EngineManager.load()</code> and/or
	 * <code>PatientsAssociationManager.updateScreenedTrialsAndInvitations()</code>
	 * on a remote server.
	 * 
	 * @param url
	 *            the JNDI url to connect to
	 * @param names
	 *            the JNDI name(s) of the <code>EngineManager</code> and/or
	 *            <code>PatientsAssociationManager</code> remote interfaces; the
	 *            <code>EngineManager</code> name should come first
	 * @throws IllegalArgumentException
	 *             if <code>names</code> is empty
	 */
	private static void loadAndRefreshJBoss(String url, String[] names) throws IllegalArgumentException
	{
		if (url == null)
			throw new IllegalArgumentException("No JNDI server specified");
		if ((names == null) || (names.length == 0))
			throw new IllegalArgumentException("No remote operations specified");
		// Setup JNDI connection properties per JBoss
		Properties environment = new Properties();
		environment.setProperty(InitialContext.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
		environment.setProperty(InitialContext.URL_PKG_PREFIXES, "org.jboss.naming");
		environment.setProperty(InitialContext.PROVIDER_URL, url);
		InitialContext initialContext = null;
		try
		{
			initialContext = new InitialContext(environment);
		}
		catch (NamingException namingException)
		{
			LOGGER.log(Level.SEVERE, "Could not create initial context using " + environment, namingException);
			if (initialContext != null)
				try
				{
					initialContext.close();
				}
				catch (NamingException closeException)
				{
					LOGGER.log(Level.WARNING, "Could not close initial context", closeException);
				}
			return;
		}
		// Lookup and execute each supported operation: load engine, refresh
		// associations
		try
		{
			for (String name : names)
				try
				{
					Object object = initialContext.lookup(name);
					if (object instanceof EngineManager)
					{
						long startTime = System.currentTimeMillis();
						((EngineManager) (object)).load();
						LOGGER.info("Loaded trials in "
								+ Double.toString((System.currentTimeMillis() - startTime) / 1000d) + "s");
						continue;
					}
					if (object instanceof PatientsAssociationManager)
					{
						long startTime = System.currentTimeMillis();
						((PatientsAssociationManager) (object)).updateScreenedTrialsAndInvitations();
						LOGGER.info("Refreshed patient associations in "
								+ Double.toString((System.currentTimeMillis() - startTime) / 1000d) + "s");
						continue;
					}
					LOGGER.warning("Ignoring operation '" + name + "' of type "
							+ (object == null ? "null" : object.getClass().toString()));
				}
				catch (NamingException namingException)
				{
					LOGGER.log(Level.SEVERE, "Could not lookup operation '" + name + '\'', namingException);
					continue;
				}
				catch (Throwable throwable)
				{
					LOGGER.log(Level.SEVERE, "Could not execute operation", throwable);
					continue;
				}
		}
		finally
		{
			try
			{
				initialContext.close();
			}
			catch (NamingException namingException)
			{
				LOGGER.log(Level.WARNING, "Could not close initial context", namingException);
			}
		}
	}

	/**
	 * Calls EJB methods on a remote server. If no actions specified then all
	 * supported actions are run:
	 * <ol>
	 * <li><code>#JNDI_ENGINEMANAGER</code>: Reloads the trial data using
	 * <code>EngineManager.load()</code></li>
	 * <li><code>#JNDI_PATIENTSASSOCIATIONMANAGER</code>: Refreshes patient
	 * associations using
	 * <code>PatientsAssociationManager.updateScreenedTrialsAndInvitations()</code>
	 * </li>
	 * 
	 * @param arguments
	 *            if the first argument is <code>-h</code> then the second
	 *            argument is a valid JNDI url (default
	 *            <code>localhost:1099</code>); all other arguments are the JNDI
	 *            name of the <code>EngineManager</code> and/or
	 *            <code>PatientsAssociationManager</code> remote interfaces
	 * @see #JNDI_ENGINEMANAGER
	 * @see #JNDI_PATIENTSASSOCIATIONMANAGER
	 */
	public static void main(String[] args)
	{
		// Find JNDI url in second argument or use default
		String url = "localhost:1099";
		String[] names = args;
		if ((args.length > 1) && "-u".equalsIgnoreCase(args[0]))
		{
			url = args[1];
			names = new String[args.length - 2];
			System.arraycopy(args, 2, names, 0, names.length);
		}
		// Use default reload/refresh actions if none specified
		if (names.length == 0)
			names = new String[] { JNDI_ENGINEMANAGER, JNDI_PATIENTSASSOCIATIONMANAGER };
		loadAndRefreshJBoss(url, names);
	}
}
