package org.quantumleaphealth.avonpfizer.action;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANATOMIC_SITE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AVATAR_DIAGNOSISAREA_ANY;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AVATAR_DIAGNOSISAREA_NONE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BIRTHDATE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREASTMETASTASIS_BRAIN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREAST_CANCER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_BONE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_LIVER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_LUNG;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_LYMPHNODE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_OTHER_LOCATION;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_OVARIES;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_SKIN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.FEMALE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.INFLAMMATORY;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.MALE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.METASTATIC;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.METASTATIC_BREAST_CANCER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.POSTMENOPAUSAL;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PREMENOPAUSAL;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.hibernate.CacheMode;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Transactional;
import org.jboss.seam.log.Log;
import org.jboss.seam.text.SeamTextLexer;
import org.jboss.seam.text.SeamTextParser;
import org.quantumleaphealth.action.PostalCodeManager;
import org.quantumleaphealth.action.TrialBrowser;
import org.quantumleaphealth.action.TrialProximity;
import org.quantumleaphealth.action.TrialProximityGroup;
import org.quantumleaphealth.action.TrialProximityGroups;
import org.quantumleaphealth.action.TrialSiteProximity.DistanceUnit;
import org.quantumleaphealth.avonpfizer.action.updater.BioMarkerDiagnosisUpdater;
import org.quantumleaphealth.avonpfizer.action.updater.BrainDiagnosisUpdater;
import org.quantumleaphealth.avonpfizer.action.updater.DefaultUpdater;
import org.quantumleaphealth.avonpfizer.action.updater.GenderUpdater;
import org.quantumleaphealth.avonpfizer.action.updater.MenoPausalUpdater;
import org.quantumleaphealth.avonpfizer.action.updater.MultipleDiagnosisUpdater;
import org.quantumleaphealth.avonpfizer.action.updater.PatientHistoryUpdater;
import org.quantumleaphealth.avonpfizer.action.updater.SingleDiagnosisUpdater;
import org.quantumleaphealth.avonpfizer.utility.MiniFormAttributes;
import org.quantumleaphealth.model.TrialResource;
import org.quantumleaphealth.model.TrialResourceLink;
import org.quantumleaphealth.model.TrialResourceSite;
import org.quantumleaphealth.model.patient.AvatarHistory;
import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.patient.UserPatient.UserType;
import org.quantumleaphealth.model.trial.Contact;
import org.quantumleaphealth.model.trial.Geocoding;
import org.quantumleaphealth.model.trial.Site;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.Trial.Link;
import org.quantumleaphealth.model.trial.Trial.Type;
import org.quantumleaphealth.model.trial.TrialSite;
import org.quantumleaphealth.model.trial.Trial_Category;
import org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.screen.MatchingEngine;
import org.quantumleaphealth.screen.PatientHistoryMatchingIterator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Stateless
@Name("trialResource")
public class AvonPfizerServiceAction implements AvonPfizerService {
	private static HashMap<Integer, PatientHistoryUpdater> patientHistoryNewUpdateActions = new HashMap<Integer, PatientHistoryUpdater>();
	static
	{
		patientHistoryNewUpdateActions.put(MiniFormAttributes.FEMALE.getValue(), new GenderUpdater(FEMALE));
		patientHistoryNewUpdateActions.put(MiniFormAttributes.MALE.getValue(), new GenderUpdater(MALE));
		patientHistoryNewUpdateActions.put(MiniFormAttributes.PREMENOPAUSAL.getValue(), new MenoPausalUpdater(PREMENOPAUSAL));
		patientHistoryNewUpdateActions.put(MiniFormAttributes.POSTMENOPAUSAL.getValue(), new MenoPausalUpdater(POSTMENOPAUSAL));
		patientHistoryNewUpdateActions.put(null, new DefaultUpdater());
	}
	
	private static HashMap<Integer, Boolean[]> patientHistoryBioMarkerUpdateActions = new HashMap<Integer, Boolean[]>();
	static
	{
		patientHistoryBioMarkerUpdateActions.put(MiniFormAttributes.HORMONE_POSITIVE.getValue(), new Boolean[] {false, true, true});
		patientHistoryBioMarkerUpdateActions.put(MiniFormAttributes.HER2_POSITIVE.getValue(), new Boolean[] {true, false, false});
		patientHistoryBioMarkerUpdateActions.put(MiniFormAttributes.HORMONE_HER2_POSITIVE.getValue(), new Boolean[] {true, true, true});
		patientHistoryBioMarkerUpdateActions.put(MiniFormAttributes.TRIPLE_NEGATIVE.getValue(), new Boolean[] {false, false, false});
	}

	private static HashMap<Integer, PatientHistoryUpdater> patientHistoryDiseaseUpdateActions = new HashMap<Integer, PatientHistoryUpdater>();
	static
	{
		patientHistoryDiseaseUpdateActions.put(MiniFormAttributes.BRAIN.getValue(), new BrainDiagnosisUpdater());
		patientHistoryDiseaseUpdateActions.put(MiniFormAttributes.BONE.getValue(), new SingleDiagnosisUpdater(DIAGNOSISAREA_BONE));
		patientHistoryDiseaseUpdateActions.put(MiniFormAttributes.LIVER.getValue(), new SingleDiagnosisUpdater(DIAGNOSISAREA_LIVER));
		patientHistoryDiseaseUpdateActions.put(MiniFormAttributes.LUNG_OTHER.getValue(), new MultipleDiagnosisUpdater());
	}

    /**
     * The matching engine
     */
    @In
    private MatchingEngine engine;

	@EJB
	private PostalCodeManager postalCodeManager;

	@EJB
	private BitMapAvatarRetriever bitMapAvatarRetriever;
	
	/**
	 * Persists the entity beans using EJB3 persistence context
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;

	/**
	 * These are the attributes available on the current metastatic mini-form.
	 */
    private Integer[] attributes;
    
    /**
     * These are new attributes available on the screener iframe from.
     */
    private Integer[] newattributes;

	/**
	 * postal code for proximity searches.
	 */
	private Integer zip;
    
    private Short yearOfBirth;

    private Boolean noEvidenceOfDisease;
	
    private Integer listView;

    private Long identifier;
    
    /**
	 * Injected logging facility
	 */
	@Logger
	private static Log logger;

	/**
     * initialized on demand
     */
    private List<TrialProximityGroup> trialProxmityGroupList;
    
    /**
     * a reference to the original class used to create the browse_trials page.
     */
	@In (create=true)
	private TrialBrowser trialBrowser;
	
	/**
	 * In case it is needed.  This goes over all available trials, so we may have to modify this to use our matched list instead, if requested.
	 */
	   
	/**
	 * If a match to an existing avatar cannot be found, then we must construct an avatar history from our update strategies.
	 * 
	 * @param patientHistory The patientHistory we will inject site or disease characteristics into.
	 */
	private void applyOldUpdateActions(PatientHistory patientHistory)
	{
		Diagnosis diagnosis = new Diagnosis();

		// Everyone has Breast Cancer
		patientHistory.getValueHistory().put(BREAST_CANCER, METASTATIC);
		diagnosis.getValueHistory().put(ANATOMIC_SITE, METASTATIC_BREAST_CANCER);

		if (attributes != null && attributes.length > 0)
		{	
			Integer bioMarkerAttribute = 0;
			// collect biomarker updates
			for (Integer attribute: attributes)
			{
				if (patientHistoryBioMarkerUpdateActions.containsKey(attribute)) {
					bioMarkerAttribute |= attribute;
				}
			}
			Boolean[] args = patientHistoryBioMarkerUpdateActions.get(bioMarkerAttribute);
			if (args != null) {
				BioMarkerDiagnosisUpdater.executeUpdate(diagnosis, args[0], args[1], args[2]);
			}
			
			// apply evidence of disease updates
			for (Integer attribute: attributes)
			{
				PatientHistoryUpdater patientHistoryUpdater = patientHistoryDiseaseUpdateActions.get(attribute);
				if (patientHistoryUpdater != null)
				{
					if (attribute.equals(MiniFormAttributes.LUNG_OTHER.getValue())) // special case for LUNG and OTHER
					{
						patientHistoryUpdater.executeUpdate(patientHistory, 
															DIAGNOSISAREA_OVARIES, 
															DIAGNOSISAREA_LYMPHNODE, 
															DIAGNOSISAREA_SKIN, 
															DIAGNOSISAREA_OTHER_LOCATION,
															DIAGNOSISAREA_LUNG,
															DIAGNOSISAREA_BREAST);
					}
					else
					{
						patientHistoryUpdater.executeUpdate(patientHistory);
					}
				}
			}
		}
		patientHistory.getDiagnoses().add(diagnosis);
	}
	
	@Transactional
	private void storeHistoryTest(PatientHistory patientHistory, AvatarHistory avatarHistory) 
	{
		try
		{
			UserPatient up = (UserPatient) em.createQuery("select up from UserPatient up where principal = 'AvonPfizer@bct.org'").getSingleResult();
			if (up != null)
			{	
				up.setPatientHistory(patientHistory);
				up.setAvatarHistory(avatarHistory);
				up.setTermsAcceptedTime(new Date());
				em.merge(up);
				logger.debug("Stored Patient History in 'AvonPfizer@bct.org'");
			}
		} catch (Exception e) {
			// Oh well.  It is not important that this succeed so silent fail.
		}
	}
	
	/**
	 * prepare and return the history to be matched.  Try to find an existing avatar first.  Create one if necessary.
	 * Then add the new attributes to it.
	 * @return the prepared patient history.
	 */
	private PatientHistory getPreparedPatientHistory()
	{
		// first try and retrieve an existing avatar
		PatientHistory patientHistory = this.bitMapAvatarRetriever.getAvatarAttributesPrincipal(attributes);
		
		// create a new one if we can't find an existing one
		if (patientHistory == null)
		{	
			logger.debug("Avatar not found for attributes.  Creating a new one.  attributes = " + attributes);
			
			patientHistory = new PatientHistory();
			patientHistory.setUserType(UserType.AVATAR_PROFILE);
			
			applyOldUpdateActions(patientHistory);
		}
		
		// apply the attributes added on the screener form
		if (yearOfBirth != null)
		{
			patientHistory.getYearMonthHistory().setShort(BIRTHDATE, yearOfBirth);
		}
		
		if (noEvidenceOfDisease != null && noEvidenceOfDisease)
		{
			if (patientHistory.getAttributeCharacteristics().containsKey(DIAGNOSISAREA))
			{	
				for (CharacteristicCode code: patientHistory.getAttributeCharacteristics().get(DIAGNOSISAREA))
				{
					patientHistory.getAttributeCharacteristics().get(DIAGNOSISAREA).remove(code);
				}
			}
		}

		// apply the new attributes
		if (newattributes != null)
		{	
			for (Integer attribute: newattributes)
			{
				PatientHistoryUpdater updater = patientHistoryNewUpdateActions.get(attribute);
				if (updater != null) // we won't check this later as we want an exception to occur.
				{
					updater.executeUpdate(patientHistory);
				}
			}
		}
		
		// always remove inflammatory information
		logger.info("Removing inflammatory");	
		for (Diagnosis diagnosis: patientHistory.getDiagnoses()) {
			CharacteristicCode removedCode = diagnosis.getValueHistory().remove(INFLAMMATORY);
			if (removedCode != null) {
				logger.info("Removed inflammatory entry");
			}
		}
		return patientHistory;
	}
	
	private boolean evidenceOfDiseaseSelected() 
	{		
		if (noEvidenceOfDisease != null && noEvidenceOfDisease)
		{
			return true;
		}
		
		for (Integer se: attributes) 
		{
			if (se.equals(MiniFormAttributes.BONE.getValue()) || 
					se.equals(MiniFormAttributes.BRAIN.getValue()) || 
					se.equals(MiniFormAttributes.LIVER.getValue()) || 
					se.equals(MiniFormAttributes.LUNG_OTHER.getValue()))
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Get the appropriate static Avatar History.
	 * The order here is important as NONE will remove all evidence of disease and evidenceOfDiseaseSelected() checks for that.
	 * 
	 * @return the avatar hisory.
	 */
	private AvatarHistory getAvatarHistory()
	{
		if (noEvidenceOfDisease != null && noEvidenceOfDisease)
		{
			return AvatarHistoryInjectableFactory.getAvatarPatientHistoryNoEvidenceOfDisease();
		}
		else if (!evidenceOfDiseaseSelected()) 
		{
			return AvatarHistoryInjectableFactory.getAvatarPatientHistoryAnyEvidenceOfDisease();
		}
		else 
		{
			return AvatarHistoryInjectableFactory.getAvatarPatientHistory();
		}
	}
	
	private boolean isListView() {
		return listView != null && listView == 1;
	}
	
	/**
	 * return sorted list for the proximity groups matched.
	 */
	private List<TrialProximityGroup> getTrialGroups()
	{
//		if (trialProxmityGroupList == null)
//		{	
			trialProxmityGroupList = new ArrayList<TrialProximityGroup>();
			
			TrialProximityGroups trialProximityGroups = this.getTrials();
			
			if (zip != null)
			{	
				// then this is a proximity search
				trialProximityGroups.setPostalCodeGeocoding(zip, postalCodeManager.getPostalCodeGeocoding(zip).getGeocoding());
// BCT-915: Keep grouping with zip code.
//				trialProximityGroups.setGroupedByProximityBoolean(true);
			}
			
			trialProximityGroups.setGroupedByProximityBoolean(isListView());
			for ( TrialProximityGroup trialProximityGroup: trialProximityGroups.getSortedList())
			{
				trialProxmityGroupList.add(trialProximityGroup);
			}	
//		}
		return trialProxmityGroupList;
	}

	private void clearStageFourDiagnoses(PatientHistory patientHistory) {
		
		if (patientHistory.getAttributeCharacteristics().containsKey(DIAGNOSISAREA))
		{	
			for (CharacteristicCode code: BreastCancerCharacteristicCodes.AVATAR_STAGEIV_SPREAD_OF_DISEASE_SET)
			{
				patientHistory.getAttributeCharacteristics().get(DIAGNOSISAREA).remove(code);
			}
			patientHistory.getValueHistory().remove(BREASTMETASTASIS_BRAIN);
		}
	}
	
	/**
	 * This method loops through the set of stage IV evidence of disease codes.
	 * Clears the history of any such markers.
	 * Injects one and then does a match.
	 * We can therefore do an 'OR' of the individual choices.  We don't currently do ANY on all possible combinations
	 * of site with evidence of disease markers.
	 * 
	 * @param patientHistory
	 * @param avatarHistory
	 * @return
	 */
	private TrialProximityGroups getAnyResult(PatientHistory patientHistory, AvatarHistory avatarHistory) 
	{
		TrialProximityGroups trials = new TrialProximityGroups(DistanceUnit.MILE);
		for (CharacteristicCode code: BreastCancerCharacteristicCodes.AVATAR_STAGEIV_SPREAD_OF_DISEASE_SET) {
			
			clearStageFourDiagnoses(patientHistory);
			if (BREASTMETASTASIS_BRAIN.equals(code)) {
				patientHistoryDiseaseUpdateActions.get(MiniFormAttributes.BRAIN.getValue()).executeUpdate(patientHistory);
			}
			else {
				patientHistory.addAttribute(DIAGNOSISAREA, code);
			}
			
			PatientHistoryMatchingIterator iterator = engine.getMatchingIterator(patientHistory, avatarHistory);
			while (true)
			{
				Trial trial = ( Trial ) iterator.nextMatch();

				if (trial == null)
				{
					break;
				}
				trials.add( trial ); 
			}	
		}
		return trials;
	}
	
	private TrialProximityGroups getSingleResult(PatientHistory patientHistory, AvatarHistory avatarHistory)
	{
		TrialProximityGroups trials = new TrialProximityGroups(DistanceUnit.MILE);

		PatientHistoryMatchingIterator iterator = engine.getMatchingIterator(patientHistory, avatarHistory);
		while (true)
		{
			Trial trial = ( Trial ) iterator.nextMatch();

			if (trial == null)
			{
				break;
			}
			trials.add( trial ); 
		}	
		return trials;
	}
	
	/**
	 * Get proximity groups associated with the prepared history.  Lock until created.
	 */
	private synchronized TrialProximityGroups getTrials()
	{
		// Populate with engine's trials
		PatientHistory patientHistory = getPreparedPatientHistory();
		AvatarHistory avatarHistory = getAvatarHistory();
		
		this.storeHistoryTest(patientHistory, avatarHistory);
		
		if (avatarHistory.containsAttribute(DIAGNOSISAREA, AVATAR_DIAGNOSISAREA_ANY)) {
			return getAnyResult(patientHistory, avatarHistory);
		}

		return getSingleResult(patientHistory, avatarHistory);
	}
	 
	private static class AvatarHistoryInjectableFactory
	{
		private static AvatarHistory avatarPatientHistory;
		private static AvatarHistory avatarPatientHistoryNoEvidenceOfDisease;
		private static AvatarHistory avatarPatientHistoryAnyEvidenceOfDisease;

		private static AvatarHistory initializeAvatarPatientHistory()
		{
			Map<CharacteristicCode, Set<CharacteristicCode>> attributeCharacteristics = new HashMap<CharacteristicCode, Set<CharacteristicCode>>();
			attributeCharacteristics.put(BreastCancerCharacteristicCodes.STAGE, new HashSet<CharacteristicCode>(Arrays.asList(BreastCancerCharacteristicCodes.STAGE_IV_SET)));
			AvatarHistory avatarPatientHistory = new AvatarHistory();
			avatarPatientHistory.setAttributeCharacteristics(attributeCharacteristics);
			return avatarPatientHistory;
		}
		
		private static AvatarHistory getAvatarPatientHistoryNoEvidenceOfDisease()
		{
			if (avatarPatientHistoryNoEvidenceOfDisease == null)
			{
				avatarPatientHistoryNoEvidenceOfDisease = initializeAvatarPatientHistory();
				avatarPatientHistoryNoEvidenceOfDisease.addAttribute(DIAGNOSISAREA, AVATAR_DIAGNOSISAREA_NONE);
			}
			return avatarPatientHistoryNoEvidenceOfDisease;
		}

		private static AvatarHistory getAvatarPatientHistoryAnyEvidenceOfDisease()
		{
			if (avatarPatientHistoryAnyEvidenceOfDisease == null)
			{
				avatarPatientHistoryAnyEvidenceOfDisease = initializeAvatarPatientHistory();
				Map<CharacteristicCode, Set<CharacteristicCode>> attributeCharacteristics = avatarPatientHistoryAnyEvidenceOfDisease.getAttributeCharacteristics();
				List<CharacteristicCode> codes = new ArrayList<CharacteristicCode>();
				codes.add(BreastCancerCharacteristicCodes.AVATAR_DIAGNOSISAREA_ANY);
				attributeCharacteristics.put(DIAGNOSISAREA, new HashSet<CharacteristicCode>(codes));
				avatarPatientHistoryAnyEvidenceOfDisease.setAttributeCharacteristics(attributeCharacteristics);
			}
			return avatarPatientHistoryAnyEvidenceOfDisease;
		}

		private static AvatarHistory getAvatarPatientHistory() 
		{
			if (avatarPatientHistory == null)
			{
				avatarPatientHistory = initializeAvatarPatientHistory();
			}
			return avatarPatientHistory;
		}
	}	
	
	public String getMatchedTrials(
			@QueryParam("attributes") final String[] paramAttributes,
			@QueryParam("newattributes") final String[] paramNewAttributes,
			@QueryParam("zip") final String paramZip,
			@QueryParam("yob") final String paramYearOfBirth,
			@QueryParam("noEvidenceOfDisease") final String paramNoEvidenceOfDisease,
			@QueryParam("listView") final String paramListView) {
		
		try {
			if (paramAttributes != null) {
				attributes = new Integer[paramAttributes.length];
				for (int i = 0; i < paramAttributes.length; i++) {
					if (paramAttributes[i].trim().length() > 0) {
						attributes[i] = Integer.parseInt(paramAttributes[i].trim());
					}
				}
			} else {
				attributes = new Integer[0];
			}
			
			if (paramNewAttributes != null) {
				newattributes = new Integer[paramNewAttributes.length];
				for (int i = 0; i < paramNewAttributes.length; i++) {
					if (paramNewAttributes[i].trim().length() > 0) {
						newattributes[i] = Integer.parseInt(paramNewAttributes[i]);
					}
				}
			} else {
				newattributes = new Integer[0];
			}
			
			if (paramZip != null && paramZip.trim().length() > 0) {
				zip = Integer.parseInt(paramZip.trim()); 
			}
			
			if (paramYearOfBirth != null && paramYearOfBirth.trim().length() > 0) {
				yearOfBirth = Short.parseShort(paramYearOfBirth.trim()); 
			}
			
			if (paramNoEvidenceOfDisease != null && paramNoEvidenceOfDisease.trim().length() > 0) {
				noEvidenceOfDisease = Boolean.parseBoolean(paramNoEvidenceOfDisease.trim()); 
			}
			
			if (paramListView != null && paramListView.trim().length() > 0) {
				listView = Integer.parseInt(paramListView.trim()); 
			}
			
		} catch (Exception e) {
			throw buildError("Invalide query parameter, " + e.getMessage());
		}

		List<TrialResource> resources = new ArrayList<TrialResource>();
		List<TrialProximityGroup> groups = getTrialGroups();
		for (TrialProximityGroup group : groups) {
			for (TrialProximity trial : group) {
				resources.add(convertToResource(trial));
			}
		}

		Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
		return gson.toJson(resources);
	}
	
	public String getTrial (
			@QueryParam("identifier") final String paramIdentifier,
			@QueryParam("zip") final String paramZip) {

		identifier = null;
		if (paramIdentifier != null && paramIdentifier.trim().length() > 0) {
			try {
				identifier = Long.parseLong(paramIdentifier);
			} catch (NumberFormatException e) {
				throw buildError("Invalide zip code parameter, " + e.getMessage());
			}
		}
		if (identifier == null) {
			throw buildError("Identifier parameter is invalid.");
		}
		
		zip = null;
		if (paramZip != null && paramZip.trim().length() > 0) {
			try {
				zip = Integer.parseInt(paramZip.trim());
			} catch (NumberFormatException e) {
				throw buildError("Invalide zip code parameter, " + e.getMessage());
			} 
		}
		
		Trial trial = engine.getTrial(identifier);
		if (trial == null) {
			String nct = String.format("NCT%08d", identifier);
			@SuppressWarnings("unchecked")
			List<Trial> trials = em.createQuery("select distinct t from Trial t left join fetch t.trialSite ts left join fetch ts.site left join fetch ts.contact left join fetch ts.principalInvestigator left join fetch ts.screenerGroup "
					+ "where t.open=true and t.listed=true and primaryId = ?1 order by t.id")
					.setHint("org.hibernate.readOnly", Boolean.TRUE)
					.setHint("org.hibernate.cacheMode", CacheMode.IGNORE)
					.setParameter(1, nct)
					.getResultList();
			if (trials.size() > 0) {
				trial = trials.get(0);
			}
		}
		if (trial == null) {
			throw buildError("Trial not found: " + paramIdentifier);
		}
		
		TrialResource resource = convertToResource(trial);
		Gson gson = new GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create();
		return gson.toJson(resource);
	}
	
	private TrialResource convertToResource(TrialProximity trial) {
		TrialResource resource = new TrialResource();
		resource.setIdentifier(trial.getTrial().getId());
		resource.setOfficialTitle(trial.getTrialTitle());
		resource.setBriefTitle(trial.getTrialName());
		resource.setPurpose(trial.getTrialPurpose());
		resource.setTreatmentPlan(convertSeamText(trial.getTrialTreatmentPlan()));
		resource.setProcedures(trial.getTrialProcedures());
		resource.setDuration(trial.getTrialDuration());
		resource.setFollowup(trial.getTrialFollowup());
		resource.setSponsor(trial.getTrialSponsor());
		resource.setPrimaryId(trial.getTrial().getPrimaryID());
		resource.setBurden(trial.getTrialBurden());
		resource.setPhase(trial.getTrial().getPhase().toString());
		resource.setPostDate(trial.getTrial().getPostDate());
		resource.setLastModifiedDate(trial.getTrial().getLastModified());
		
		List<TrialResourceSite> resourceSites = resource.getSites();
		Set<TrialSite> sites = trial.getTrial().getTrialSite();
		for (TrialSite site : sites) {
			TrialResourceSite resourceSite = new TrialResourceSite();
			Site siteSite = site.getSite();
			if (siteSite != null) {
				resourceSite.setFacilityName(siteSite.getName());
				resourceSite.setCity(siteSite.getCity());
				resourceSite.setPoliticalSubUnitName(siteSite.getPoliticalSubUnitName());
				resourceSite.setZipCode(convertPostalCode(siteSite.getPostalCode()));
			}
			Contact contact = site.getContact();
			if (contact != null) {
				resourceSite.setContactName((contact.getSurName() == null ? "" : contact.getSurName()) + (contact.getGivenName() == null ? "" : ", " + contact.getGivenName()));
				resourceSite.setContactEmail(contact.getEmail());
				resourceSite.setContactPhone(contact.getPhone());
			}
			resourceSites.add(resourceSite);
		}
		
		List<TrialResourceLink> resourceLinks = resource.getLinks();
		List<Link> links = trial.getTrial().getLink();
		for (Link link : links) {
			TrialResourceLink resourceLink = new TrialResourceLink();
			resourceLink.setLabel(link.getLabel());
			resourceLink.setUrl(link.getUrl());
			resourceLinks.add(resourceLink);
		}
		
		Type[] types = Type.values();
		List<String> resourceCategories = resource.getCategories();
		Set<Trial_Category> categories = trial.getTrial().getTrial_Categories();
		for (Trial_Category category : categories) {
			resourceCategories.add(types[category.getTrial_CategoryPK().getCategory()].toString());
		}
		String typeString = types[trial.getTrial().getType().ordinal()].toString();
		if (!resourceCategories.contains(typeString)) {
			resourceCategories.add(0, typeString);
		};		
		
		return resource;		
	}
	
	private TrialResource convertToResource(Trial trial) {
		TrialResource resource = new TrialResource();
		resource.setIdentifier(trial.getId());
		resource.setOfficialTitle(trial.getTitle());
		resource.setBriefTitle(trial.getName());
		resource.setPurpose(trial.getPurpose());
		resource.setTreatmentPlan(convertSeamText(trial.getTreatmentPlan()));
		resource.setProcedures(trial.getProcedures());
		resource.setDuration(trial.getDuration());
		resource.setFollowup(trial.getFollowup());
		resource.setSponsor(trial.getSponsor());
		resource.setPrimaryId(trial.getPrimaryID());
		resource.setBurden(trial.getBurden());
		resource.setPhase(trial.getPhase().toString());
		resource.setPostDate(trial.getPostDate());
		resource.setLastModifiedDate(trial.getLastModified());
		
		List<TrialResourceSite> resourceSites = resource.getSites();
		Set<TrialSite> sites = trial.getTrialSite();
		
		Geocoding geocoding = (zip == null ? null : postalCodeManager.getPostalCodeGeocoding(zip).getGeocoding());
		for (TrialSite site : sites) {
			TrialResourceSite resourceSite = new TrialResourceSite();
			Site siteSite = site.getSite();
			if (siteSite != null) {
				resourceSite.setFacilityName(siteSite.getName());
				resourceSite.setCity(siteSite.getCity());
				resourceSite.setPoliticalSubUnitName(siteSite.getPoliticalSubUnitName());
				resourceSite.setZipCode(convertPostalCode(siteSite.getPostalCode()));
				if (geocoding != null) {
					double distance = siteSite.getGeocoding().getDistanceMiles(geocoding);
					resourceSite.setDistance(Long.valueOf(Math.round(distance)).intValue());
				}
			}
			Contact contact = site.getContact();
			if (contact != null) {
				resourceSite.setContactName((contact.getSurName() == null ? "" : contact.getSurName()) + (contact.getGivenName() == null ? "" : ", " + contact.getGivenName()));
				resourceSite.setContactEmail(contact.getEmail());
				resourceSite.setContactPhone(contact.getPhone());
			}
			resourceSites.add(resourceSite);
		}
		
		List<TrialResourceLink> resourceLinks = resource.getLinks();
		List<Link> links = trial.getLink();
		for (Link link : links) {
			TrialResourceLink resourceLink = new TrialResourceLink();
			resourceLink.setLabel(link.getLabel());
			resourceLink.setUrl(link.getUrl());
			resourceLinks.add(resourceLink);
		}
		
		Type[] types = Type.values();
		List<String> resourceCategories = resource.getCategories();
		Set<Trial_Category> categories = trial.getTrial_Categories();
		for (Trial_Category category : categories) {
			resourceCategories.add(types[category.getTrial_CategoryPK().getCategory()].toString());
		}
		String typeString = types[trial.getType().ordinal()].toString();
		if (!resourceCategories.contains(typeString)) {
			resourceCategories.add(0, typeString);
		};
		
		return resource;		
	}	
	private Integer convertPostalCode(String postalCode) {
		Integer zip = null;
		try {
			zip = Integer.parseInt(postalCode);
		} catch (Exception e) {
			
		}
		return zip;
	}
	
	private String convertSeamText(String text) {
		try {
			SeamTextParser parser = new SeamTextParser(new SeamTextLexer(new StringReader(text)));
			parser.startRule();
			return parser.toString();
		} catch (Exception e) {
			logger.warn("Unable to parse the Seam formatted text: " + text);
		}
		// If unable to parse, return the original text	
		return text;
	}
	
	private WebApplicationException buildError(String message) {
		return new WebApplicationException(Response.status(Response.Status.BAD_REQUEST).entity(message).type(MediaType.TEXT_PLAIN).build());
	}
}