package org.quantumleaphealth.avonpfizer.action;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import org.quantumleaphealth.action.TrialProximityGroup;

/**
 * This interface and the corresponding implementation 
 * are used for the browse trials page replacement.
 * @author wgweis
 */
@Local
public interface AvonPfizer
{
	public String getHistoryDescription();

    public Integer[] getAttributes();
    public void setAttributes(Integer[] attributes);
    
	public Integer[] getNewattributes();
	public void setNewattributes(Integer[] newattributes);


	public Integer getZip();
	public void setZip(Integer zip);
	
	public List<TrialProximityGroup> getTrialGroups();

    /**
     * @return the maximum last modified date from the list of trials to be displayed. 
     */
    public Date getLastModified();
    

    /**
     * BCT-693:  return count of trials modified or registered in the last two weeks.
     */
	public Integer getNewTrialCount();

	/**
	 * Let us know if we are sorting by proximity
	 * @return true if zip is not null, false otherwise
	 */
	public boolean isProximitySearch();

	public boolean isListView();
	
	public void toggleProximitySearchView();
	
	/**
	 * @return count of trials matched
	 */
	public Integer getTrialMatchCount();

	/**
	 * Needed by EJB3
	 */
	public void destroy();
}
