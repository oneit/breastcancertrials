package org.quantumleaphealth.avonpfizer.action.updater;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.GENDER;

public class GenderUpdater extends PatientHistoryUpdater
{
	public GenderUpdater(CharacteristicCode code)
	{
		super(code);
	}
	
	@Override
	public void executeUpdate(PatientHistory patientHistory) 
	{
		patientHistory.getValueHistory().put(GENDER, getCode());
	}

}
