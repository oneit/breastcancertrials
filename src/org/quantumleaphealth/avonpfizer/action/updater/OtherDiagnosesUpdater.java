package org.quantumleaphealth.avonpfizer.action.updater;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AVATAR_STAGEIII_LOCAL_SPREAD_OF_DISEASE_SET;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_BONE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_BRAIN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_LIVER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_SPINALCORD;

import org.quantumleaphealth.action.FacadePatientHistory;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;

public class OtherDiagnosesUpdater extends PatientHistoryUpdater
{
	@Override
	public void executeUpdate(PatientHistory patientHistory) 
	{
		for (CharacteristicCode code: FacadePatientHistory.getDiagnosisAreaAttributeCharacteristics())
		{
			// ignore Stage III local spread of disease.
			if (code.isMember(AVATAR_STAGEIII_LOCAL_SPREAD_OF_DISEASE_SET))
			{
				continue;
			}
			
			if (!code.equals(DIAGNOSISAREA_BRAIN) && !code.equals(DIAGNOSISAREA_BONE) && !code.equals(DIAGNOSISAREA_LIVER) && !code.equals(DIAGNOSISAREA_SPINALCORD))
			{
				patientHistory.addAttribute(DIAGNOSISAREA, code);
			}
		}
	}
}
