package org.quantumleaphealth.avonpfizer.action.updater;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREASTMETASTASIS_BRAIN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_BRAIN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PROGRESSING;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;

public class BrainDiagnosisUpdater extends PatientHistoryUpdater 
{
	@Override
	public void executeUpdate(PatientHistory patientHistory) 
	{
		Diagnosis diagnosis = null;

		if (patientHistory.getDiagnoses().size() <= 0)
		{
			diagnosis = new Diagnosis();
		}
		else
		{
			diagnosis = (Diagnosis) patientHistory.getDiagnoses().toArray()[0];
		}
		
		// Add BRAIN to the Attribute section
		patientHistory.addAttribute(DIAGNOSISAREA, DIAGNOSISAREA_BRAIN);

		/**
		 * All brain cancer for avatars is automatically set to PROGRESSING.
		 */
		diagnosis.getValueHistory().put(BREASTMETASTASIS_BRAIN, PROGRESSING);
		patientHistory.getDiagnoses().add(diagnosis);
	}
}
