package org.quantumleaphealth.avonpfizer.action.updater;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;

public class SingleDiagnosisUpdater extends PatientHistoryUpdater
{
	public SingleDiagnosisUpdater(CharacteristicCode code) 
	{
		super(code);
	}

	@Override
	public void executeUpdate(PatientHistory patientHistory) {
		patientHistory.addAttribute(DIAGNOSISAREA, getCode());
	}
	
}