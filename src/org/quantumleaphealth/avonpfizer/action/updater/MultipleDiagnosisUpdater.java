package org.quantumleaphealth.avonpfizer.action.updater;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;

public class MultipleDiagnosisUpdater extends PatientHistoryUpdater
{
	@Override
	public void executeUpdate(PatientHistory patientHistory, CharacteristicCode... codes) 
	{
		for (CharacteristicCode code: codes)
		{	
			patientHistory.addAttribute(DIAGNOSISAREA, code);
		}	
	}

	@Override
	public void executeUpdate(PatientHistory patientHistory) 
	{
		throw new UnsupportedOperationException("MultipleDiagnosisUpdater does not support single operations.");
	}
	
}