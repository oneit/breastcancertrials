package org.quantumleaphealth.avonpfizer.action.updater;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;

public abstract class PatientHistoryUpdater
{
	private CharacteristicCode code;
	
	public PatientHistoryUpdater() {}
	
	public PatientHistoryUpdater(CharacteristicCode code)
	{
		this.code = code;
	}
	
	abstract public void executeUpdate(PatientHistory patientHistory);

	public CharacteristicCode getCode() 
	{
		return code;
	}
	
	/**
	 * Do nothing for all but the classes needing to implement this.
	 * @param patientHistory
	 * @param codes
	 */
	public void executeUpdate(PatientHistory patientHistory, CharacteristicCode... codes) {}
}