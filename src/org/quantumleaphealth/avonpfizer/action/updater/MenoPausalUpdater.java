package org.quantumleaphealth.avonpfizer.action.updater;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.MENOPAUSAL;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.GENDER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.FEMALE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREAST_CANCER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.METASTATIC;

public class MenoPausalUpdater extends PatientHistoryUpdater 
{
	public MenoPausalUpdater(CharacteristicCode code)
	{
		super(code);
	}
	
	@Override
	public void executeUpdate(PatientHistory patientHistory) 
	{
		// Pre or Post menopausal, although the BCT health form offers a third alternative as well
		patientHistory.getValueHistory().put(MENOPAUSAL, getCode());
		
		// This should have been set elsewhere but it can't hurt to set it again.
		patientHistory.getValueHistory().put(GENDER, FEMALE);
		patientHistory.getValueHistory().put(BREAST_CANCER, METASTATIC);
		

	}

}
