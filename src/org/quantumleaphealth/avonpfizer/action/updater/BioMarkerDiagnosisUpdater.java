package org.quantumleaphealth.avonpfizer.action.updater;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ESTROGENRECEPTOR;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ESTROGENRECEPTOR_NEGATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ESTROGENRECEPTOR_POSITIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.HER2NEUDIAGNOSIS;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.HER2NEUDIAGNOSIS_NEGATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.HER2NEUDIAGNOSIS_POSITIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PROGESTERONERECEPTOR;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PROGESTERONERECEPTOR_NEGATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PROGESTERONERECEPTOR_POSITIVE;

import org.quantumleaphealth.model.patient.Diagnosis;

public class BioMarkerDiagnosisUpdater
{
	public static void executeUpdate(Diagnosis diagnosis, boolean isHer2Positive, boolean isEstrogenPositive, boolean isProgesteronePositive)
	{
		if (diagnosis == null)
		{
			throw new IllegalArgumentException("patient History diagnois instance must be initialized prior to use.");
		}
		diagnosis.getValueHistory().put(HER2NEUDIAGNOSIS, isHer2Positive? HER2NEUDIAGNOSIS_POSITIVE: HER2NEUDIAGNOSIS_NEGATIVE);
		diagnosis.getValueHistory().put(ESTROGENRECEPTOR, isEstrogenPositive? ESTROGENRECEPTOR_POSITIVE: ESTROGENRECEPTOR_NEGATIVE);
		diagnosis.getValueHistory().put(PROGESTERONERECEPTOR, isProgesteronePositive? PROGESTERONERECEPTOR_POSITIVE: PROGESTERONERECEPTOR_NEGATIVE);
	}
}
