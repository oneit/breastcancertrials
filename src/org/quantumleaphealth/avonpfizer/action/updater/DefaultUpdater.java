package org.quantumleaphealth.avonpfizer.action.updater;

import org.quantumleaphealth.model.patient.PatientHistory;

public class DefaultUpdater extends PatientHistoryUpdater
{
	@Override
	public void executeUpdate(PatientHistory patientHistory) {
		throw new IllegalStateException("There is no default update Action.  Please define one.");
	}
}