package org.quantumleaphealth.avonpfizer.action;

import javax.ejb.Local;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

@Local
@Path("/metastatic-trial-search")
public interface AvonPfizerService {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Path("/trials")
	public String getMatchedTrials(
			@QueryParam("attributes") final String[] paramAttributes,
			@QueryParam("newattributes") final String[] paramNewAttributes,
			@QueryParam("zip") final String paramZip,
			@QueryParam("yob") final String paramYearOfBirth,
			@QueryParam("noEvidenceOfDisease") final String paramNoEvidenceOfDisease,
			@QueryParam("listView") final String paramListView);
	
	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	@Path("/trial/{identifier}")
	public String getTrial (
			@PathParam("identifier") final String paramIdentifier,
			@QueryParam("zip") final String paramZip);
}
