package org.quantumleaphealth.avonpfizer.action;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;

@Stateless
@Name("bitMapAvatarRetriever")
public class BitMapAvatarRetrieverAction implements BitMapAvatarRetriever
{
	/**
	 * Persists the entity beans using EJB3 persistence context
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;

    /**
     * Injected logging facility
     */
    @Logger
    private static Log logger;

	/**
	 * Get the bitmap created from the avatar attribute list passed in when the user clicks on one of the start forms
	 * on the index or home page.
	 * 
	 * @return The "Or'ed" bitmap of the attributes passed in.
	 */
	private Integer getAttributesBitmap(Integer[] attributes)
	{
		Integer avatarAttributeBitMap = 0;
		for (Integer attribute: attributes)
		{
			avatarAttributeBitMap = avatarAttributeBitMap | attribute;
		}
		return avatarAttributeBitMap;
	}

	/**
	 * If bean is being called as a result of the user clicking on one of the links from the index page,
	 * try to retrieve the username of the avatar associated with these characteristics.
	 * @return null if not found, otherwise the values of userpatient.principal found.
	 */
	public PatientHistory getAvatarAttributesPrincipal(Integer[] attributes)
	{
		if (attributes == null)
		{
			return null;
		}
		
		Integer avatarAttributeBitMap = null;
		try
		{
			avatarAttributeBitMap = getAttributesBitmap(attributes);
			// find the corresponding avatar
			UserPatient avatar = (UserPatient) em.createQuery("select p from UserPatient p where p.characteristicBitmap = :characteristicBitmap")
					.setParameter("characteristicBitmap", avatarAttributeBitMap).setHint("org.hibernate.readOnly", Boolean.TRUE).getSingleResult();
			
			logger.info("Retrieved Avatar " + avatar.getPrincipal() + ", description: X" + avatar.getAvatarProfileDescription() + "X for bitmap " + avatarAttributeBitMap);
			return avatar.getPatientHistory();
		}
		catch(Exception e)
		{
			logger.error("Could not retrieve and avatar result for bitmap " + avatarAttributeBitMap);
		}
			
		return null;
	}

}
