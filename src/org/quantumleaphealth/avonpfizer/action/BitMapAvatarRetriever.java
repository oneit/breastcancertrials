package org.quantumleaphealth.avonpfizer.action;

import javax.ejb.Local;

import org.quantumleaphealth.model.patient.PatientHistory;

@Local
public interface BitMapAvatarRetriever 
{
	public PatientHistory getAvatarAttributesPrincipal(Integer[] attributes);
}
