package org.quantumleaphealth.avonpfizer.utility;

import java.util.List;

import org.quantumleaphealth.action.TrialProximityGroup;

public class AvonPfizerTrialCountBuilder 
{
	public static int getTrialCountOverProximityGroups(List<TrialProximityGroup> groups)
	{
		int trialCount = 0;
		for (TrialProximityGroup group: groups)
		{
			trialCount += group.size();
		}
		return trialCount;
	}
}
