package org.quantumleaphealth.avonpfizer.utility;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Utility class to build titles based on the user choices on the mini search forms.
 * 
 * @author wgweis
 *
 */
public class AvatarTitleBuilder 
{
	
//	MALE(2048),
//	PREMENOPAUSAL(4096),
//	POSTMENOPAUSAL(8192);
	
	private static HashMap<Integer, String> genderDescriptionMap = new HashMap<Integer, String>();
	static
	{
		genderDescriptionMap.put(MiniFormAttributes.PREMENOPAUSAL.getValue(), "Pre-menopausal");
		genderDescriptionMap.put(MiniFormAttributes.POSTMENOPAUSAL.getValue(), "Post-menopausal");
		genderDescriptionMap.put(MiniFormAttributes.MALE.getValue(), "Male");
	}

	private static HashMap<Integer, String> bioDescriptionMap = new HashMap<Integer, String>();
	static
	{
		bioDescriptionMap.put(MiniFormAttributes.HORMONE_POSITIVE.getValue(), "Hormone-positive");
		bioDescriptionMap.put(MiniFormAttributes.HER2_POSITIVE.getValue(), "HER2-positive");
		bioDescriptionMap.put(MiniFormAttributes.TRIPLE_NEGATIVE.getValue(), "Triple negative");
	}

	private static HashMap<Integer, String> siteDescriptionMap = new HashMap<Integer, String>();
	static
	{
		siteDescriptionMap.put(MiniFormAttributes.BRAIN.getValue(), "Brain");
		siteDescriptionMap.put(MiniFormAttributes.BONE.getValue(), "Bone");
		siteDescriptionMap.put(MiniFormAttributes.LIVER.getValue(), "Liver");
		siteDescriptionMap.put(MiniFormAttributes.LUNG_OTHER.getValue(), "Lung/Other");
	}

	/**
	 * Append the strings mapping to the attributes into a human readable form.
	 * @param map
	 * @param glue
	 * @param attributes
	 * @return
	 */
	private static String getSubDescription(Map<Integer, String> map, String glue, Integer[] attributes)
	{
		
		StringBuilder out = new StringBuilder();

		if (attributes != null && attributes.length > 0)
		{
			Set<String> unique = new HashSet<String>();

			for (Integer attribute: attributes)
			{
				if (map.containsKey(attribute))
				{
					unique.add(map.get(attribute));
				}
			}
			
			if (unique.size() > 0) // may not have set an attribute for the map
			{	
				String[] sa = unique.toArray(new String[] {});
				out.append( sa[0] );
				for ( int x=1; x < sa.length; ++x )
				{
					out.append(glue).append(sa[x]);
				}
			}
		}
		return out.toString();
	}

	/**
	 * Main API method.  Returns a title string based on user choices for gender, site and evidence of disease.
	 * 
	 * @param attributes attributes for site and evidences of disease
	 * @param newAttributes attributes for gender and menopausal.
	 * @param noEvidenceOfDisease special attribute for evidence of disease
	 * @return
	 */
	public static String build(Integer[] attributes, Integer[] newAttributes, Boolean noEvidenceOfDisease)
	{
		final int genderDescriptionNdx = 0;
		final int bioDescriptionNdx = 1;
		final int siteDescriptionNdx = 2;
		
		String[] subDescriptions = new String[3];
		StringBuilder sb = new StringBuilder();

		subDescriptions[genderDescriptionNdx] = getSubDescription(genderDescriptionMap, "", newAttributes);
		subDescriptions[bioDescriptionNdx] = getSubDescription(bioDescriptionMap, "/", attributes);
		subDescriptions[siteDescriptionNdx] = getSubDescription(siteDescriptionMap, " and ", attributes);

		if (subDescriptions[genderDescriptionNdx].length() > 0)
		{
			sb.append(subDescriptions[genderDescriptionNdx]);
		}

		if (subDescriptions[bioDescriptionNdx].length() > 0)
		{
			if (subDescriptions[genderDescriptionNdx].length() > 0) 
			{
				sb.append(" - ");
			}
			sb.append(subDescriptions[bioDescriptionNdx]);
		}

		if (noEvidenceOfDisease != null && noEvidenceOfDisease)
		{
			sb.append(" - ").append("No evidence of disease");
		}
		else if (subDescriptions[siteDescriptionNdx].length() > 0)
		{
			sb.append(" - Disease in: ").append(subDescriptions[siteDescriptionNdx]);
		}
		return sb.toString();
	}

	/**
	 * sample use
	 * 
	 * @param args
	 */
	public static void main(String[] args)
	{
//		METASTATIC(2), 
//		HORMONE_POSITIVE(8), 
//		HER2_POSITIVE(16), 
//		HORMONE_HER2_POSITIVE(24),  // user can choose *both* Hormone and Her2 Positive.
//		TRIPLE_NEGATIVE(32), 
//		BRAIN(64), 
//		BONE(128), 
//		LIVER(256), 
//		LUNG_OTHER(512),
//		FEMALE(1024),
//		MALE(2048),
//		PREMENOPAUSAL(4096),
//		POSTMENOPAUSAL(8192);
		
		Integer[] attributes = {2, 8, 16, 24, 64, 128, 256, 512};//, 16, 32}; //, 64, 128, 256, 512, 512};
		Integer[] newAttributes = {};

		System.out.println(AvatarTitleBuilder.build(attributes, newAttributes, false));
	}
}
