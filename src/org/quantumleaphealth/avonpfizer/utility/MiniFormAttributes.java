package org.quantumleaphealth.avonpfizer.utility;

/**
 * These are the bits set in response to the user's choices on the miniforms.
 * 
 * @author wgweis
 *
 */
public enum MiniFormAttributes 
{
	METASTATIC(2), 
	HORMONE_POSITIVE(8), 
	HER2_POSITIVE(16), 
	HORMONE_HER2_POSITIVE(24),  // user can choose *both* Hormone and Her2 Positive.
	TRIPLE_NEGATIVE(32), 
	BRAIN(64), 
	BONE(128), 
	LIVER(256), 
	LUNG_OTHER(512),
	FEMALE(1024),
	MALE(2048),
	PREMENOPAUSAL(4096),
	POSTMENOPAUSAL(8192);
	
	private int attributeValue;
	private MiniFormAttributes(int attributeValue)
	{
		this.attributeValue = attributeValue;
	}
	
	public int getValue()
	{
		return attributeValue;
	}
}
