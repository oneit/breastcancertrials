/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Matches a patient characteristic that is limited by a numerical requirement.
 * The superclass' <code>invert</code> field designates whether the requirement
 * is a maximum.
 * 
 * @author Tom Bechtold
 * @version 2008-02-22
 */
public class ComparativeCriterion extends AbstractCriterion
{

	/**
	 * The characteristicCode patient characteristic
	 */
	private CharacteristicCode characteristicCode;

	/**
	 * The unit-of-measure characteristic
	 */
	private CharacteristicCode unitOfMeasure;

	/**
	 * The characteristic's limit
	 */
	private double requirement;

	/**
	 * @return the patient characteristic
	 */
	public CharacteristicCode getCharacteristicCode()
	{
		return characteristicCode;
	}

	/**
	 * @param characteristicCode
	 *            the patient characteristic
	 */
	public void setCharacteristicCode(CharacteristicCode primary)
	{
		this.characteristicCode = primary;
	}

	/**
	 * @return the unit-of-measure characteristic
	 */
	public CharacteristicCode getUnitOfMeasure()
	{
		return unitOfMeasure;
	}

	/**
	 * @param unitOfMeasure
	 *            the unit-of-measure characteristic
	 */
	public void setUnitOfMeasure(CharacteristicCode unitOfMeasure)
	{
		this.unitOfMeasure = unitOfMeasure;
	}

	/**
	 * @return the characteristic's limit
	 */
	public double getRequirement()
	{
		return requirement;
	}

	/**
	 * @param requirement
	 *            the characteristic's limit
	 */
	public void setRequirement(double requirement)
	{
		this.requirement = requirement;
	}

	/**
	 * Returns <code>true</code> if this criterion matches the same patient
	 * characteristic as another. This method adds to the superclass'
	 * implementation a comparison of <code>characteristicCode</code>.
	 * 
	 * @param other
	 *            the other criterion to compare
	 * @return <code>true</code> if this criterion matches the same
	 *         characteristic as <code>other</code>.
	 * @see org.quantumleaphealth.model.trial.AbstractCriterion#matchesSameCharacteristic(org.quantumleaphealth.model.trial.AbstractCriterion)
	 */
	@Override
	public boolean matchesSameCharacteristic(AbstractCriterion other)
	{
		// The superclass' implementation guarantees that other is the same
		// class
		if (!super.matchesSameCharacteristic(other))
			return false;
		CharacteristicCode otherCode = ((ComparativeCriterion) (other)).characteristicCode;
		return (characteristicCode == null) ? (otherCode == null) : characteristicCode.equals(otherCode);
	}

	/**
	 * Returns the contents of the fields
	 * 
	 * @return the contents of the fields
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder(super.toString());
		builder.append('{').append(characteristicCode);
		if (unitOfMeasure != null)
			builder.append('[').append(unitOfMeasure).append(']');
		builder.append(isInvert() ? '<' : '>').append(requirement);
		return builder.append('}').toString();
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -3709535917252399768L;
}
