/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import java.io.Serializable;

/**
 * An eligibility criterion that contains representations of a criterion in text
 * and <code>Criterion</code> forms and the difference between them.
 * 
 * @author Tom Bechtold
 * @version 2008-07-01
 */
public class EligibilityCriterion implements Serializable, Cloneable
{
	/**
	 * The library which this eligibility criterion belongs to. Should not
	 * contain spaces as it will be used to lookup externalized text.
	 */
	private String library;

	/**
	 * The non-localized description of this eligibility criterion as authored.
	 */
	private String text;

	/**
	 * The structured logic used to match this eligibility criterion against a
	 * patient's characteristics or <code>null</code> if this eligibility
	 * criterion is unstructured.
	 */
	private Criterion criterion;

	/**
	 * The non-localized difference between the criterion's text and the
	 * structured logic or <code>null</code> if either <code>criterion</code> is
	 * <code>null</code> or there is no semantic difference between
	 * <code>text</code> and <code>criterion</code>. This text will be used to
	 * qualify the matching result.
	 */
	private String qualifier;

	/**
	 * The available types.
	 * <UL>
	 * <LI><code>NOTMATCHED</code>: Not matched; e.g., no criterion attached /
	 * unstructured criterion.
	 * <LI><code>REQUIRE</code>: Patient characteristic must match.
	 * <LI><code>EXCLUDE</code>: Patient characteristic must not match.
	 * <LI><code>SUBJECTIVE</code>: Patient characteristic is not matched but is
	 * documented if it doesn't (Alert).
	 * <LI><code>VOLUNTARY</code>: Patient characteristic is not matched and is
	 * documented (Allow).
	 * </UL>
	 */
	public enum Type
	{
		NOTMATCHED
		{
			@Override
			public String toString()
			{
				return "Not matched";
			}
		},
		REQUIRE
		{
			@Override
			public String toString()
			{
				return "Require";
			}
		},
		EXCLUDE
		{
			@Override
			public String toString()
			{
				return "Exclude";
			}
		},
		SUBJECTIVE
		{
			@Override
			public String toString()
			{
				return "Alert";
			}
		},
		VOLUNTARY
		{
			@Override
			public String toString()
			{
				return "Allow";
			}
		}
	}

	/**
	 * The type, guaranteed to be non-<code>null</code>
	 */
	private Type type = Type.NOTMATCHED;

	/**
	 * @return the library
	 */
	public String getLibrary()
	{
		return library;
	}

	/**
	 * @param library
	 *            the library to set
	 */
	public void setLibrary(String library)
	{
		this.library = library;
	}

	/**
	 * @return the text
	 */
	public String getText()
	{
		return text;
	}

	/**
	 * @param text
	 *            the text to set
	 */
	public void setText(String text)
	{
		this.text = text;
	}

	/**
	 * Returns the eligibility criteria/on.
	 * 
	 * @return the eligibility criteria/on
	 */
	public Criterion getCriterion()
	{
		return criterion;
	}

	/**
	 * @param criterion
	 *            the criterion to set
	 */
	public void setCriterion(Criterion criterion)
	{
		this.criterion = criterion;
	}

	/**
	 * @return the qualifier
	 */
	public String getQualifier()
	{
		return qualifier;
	}

	/**
	 * @param qualifier
	 *            the qualifier to set
	 */
	public void setQualifier(String qualifier)
	{
		this.qualifier = qualifier;
	}

	/**
	 * @return the type, guaranteed to be non-<code>null</code>
	 */
	public Type getType()
	{
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setType(Type type) throws IllegalArgumentException
	{
		if (type == null)
			throw new IllegalArgumentException("must specify parameter");
		this.type = type;
	}

	/**
	 * Returns a deep clone
	 * 
	 * @return a deep clone
	 */
	@Override
	public Object clone()
	{
		// Create deep clone of the criterion
		try
		{
			EligibilityCriterion eligibilityCriterion = (EligibilityCriterion) super.clone();
			if (criterion != null)
				eligibilityCriterion.criterion = (Criterion) (criterion.clone());
			return eligibilityCriterion;
		}
		catch (CloneNotSupportedException cloneNotSupportedException)
		{
			// This should never happen as we are clonable
			throw new InternalError(cloneNotSupportedException.getMessage());
		}
	}

	/**
	 * Returns whether each field of this object is equal to another object.
	 * 
	 * @return whether each field of this object is equal to another object; if
	 *         <code>obj</code> is <code>null</code> then return
	 *         <code>false</code>.
	 */
	@Override
	public boolean equals(Object obj)
	{
		if ((obj == null) || !(obj instanceof EligibilityCriterion))
			return false;
		EligibilityCriterion other = (EligibilityCriterion) (obj);
		// Compare string and enum fields
		if ((library != other.library) || (text != other.text) || (qualifier != other.qualifier)
				|| (type != other.type))
			return false;
		// Compare criterion
		return (criterion == null) ? (other.criterion == null) : criterion.equals(other.criterion);
	}

	/**
	 * Serial version UID for serializable class
	 */
	private static final long serialVersionUID = -6578856457234827594L;
}
