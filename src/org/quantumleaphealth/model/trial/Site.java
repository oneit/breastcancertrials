/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * A site that delivers clinical trials to patients.
 * 
 * @author Tom Bechtold
 * @version 2009-02-03
 */
@Entity
public class Site implements Serializable
{

	/**
	 * Unique identifier
	 */
	@Id
	private Long id;

	/**
	 * Name
	 */
	private String name;

	/**
	 * City
	 */
	private String city;

	/**
	 * Postal Code (Zip Code)
	 */
	private String postalCode;

	/**
	 * The geocoding
	 */
	private Geocoding geocoding;

	/**
	 * Political Subunit (State/Province)
	 */
	private String politicalSubUnitName;

	/**
	 * Country code
	 */
	private String countryCode;

	/**
	 * Instantiates an empty site.
	 */
	public Site()
	{
	}

	/**
	 * @return the id
	 */
	public Long getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * Returns the name.
	 * 
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * Sets the name.
	 * 
	 * @param name
	 *            the name to set; empty string is stored as <code>null</code>
	 */
	public void setName(String name)
	{
		this.name = trim(name);
	}

	/**
	 * Returns the city.
	 * 
	 * @return the city
	 */
	public String getCity()
	{
		return city;
	}

	/**
	 * Sets the city.
	 * 
	 * @param city
	 *            the city to set; empty string is stored as <code>null</code>
	 */
	public void setCity(String city)
	{
		this.city = trim(city);
	}

	/**
	 * Returns the postal code/zip.
	 * 
	 * @return the postal code/zip
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * Sets the postal code/zip.
	 * 
	 * @param postalCode
	 *            the postal code/zip to set; empty string is stored as
	 *            <code>null</code>
	 */
	public void setPostalCode(String postalCode)
	{
		this.postalCode = trim(postalCode);
	}

	/**
	 * @return the geocoding
	 */
	public Geocoding getGeocoding()
	{
		return geocoding;
	}

	/**
	 * @param the
	 *            geocoding
	 */
	public void setGeocoding(Geocoding geocoding)
	{
		this.geocoding = geocoding;
	}

	/**
	 * Returns the political subunit (state/province)
	 * 
	 * @return the political subunit (state/province)
	 */
	public String getPoliticalSubUnitName()
	{
		return politicalSubUnitName;
	}

	/**
	 * Sets the political subunit (state/province)
	 * 
	 * @param politicalSubUnitName
	 *            the political subunit (state/province) to set; empty string is
	 *            stored as <code>null</code>
	 */
	public void setPoliticalSubUnitName(String politicalSubUnitName)
	{
		this.politicalSubUnitName = trim(politicalSubUnitName);
	}

	/**
	 * Returns the country code.
	 * 
	 * @return the country code
	 */
	public String getCountryCode()
	{
		return countryCode;
	}

	/**
	 * Sets the country code.
	 * 
	 * @param country
	 *            name the country code to set; empty string is stored as
	 *            <code>null</code>
	 */
	public void setCountryCode(String countryCode)
	{
		this.countryCode = trim(countryCode);
	}

	/**
	 * Returns a trimmed string or <code>null</code> if the string is empty
	 * 
	 * @param string
	 *            the string to trim
	 * @return a trimmed string or <code>null</code> if the string is empty
	 */
	private static String trim(String string)
	{
		if (string == null)
			return null;
		string = string.trim();
		return (string.length() == 0) ? null : string;
	}

	/**
	 * Returns comparison of id and name
	 * 
	 * @return <code>true</code> if the ids are equal or if both ids are
	 *         <code>null</code> and the names are equal
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (super.equals(obj))
			return true;
		if (!(obj instanceof Site))
			return false;
		Site site = (Site) obj;
		if (id != null)
			return id.equals(site.id);
		if (site.id != null)
			return false;
		return (name == null) ? false : name.equals(site.name);
	}

	/**
	 * Returns the id's hash or name's hash or superclass' if id/name is not set
	 * 
	 * @return the id's hash or name's hash or superclass' if id and name are
	 *         <code>null</code>
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		if (id != null)
			return id.hashCode();
		if (name != null)
			return name.hashCode();
		return super.hashCode();
	}

	/**
	 * Version ID for serializable class
	 */
	private static final long serialVersionUID = 8333577333749490704L;
}
