package org.quantumleaphealth.model.trial;

import java.io.Serializable;

import javax.persistence.Embeddable;

import org.quantumleaphealth.model.trial.TrialEvent.PrimaryKey;

/**
 * This is the entity class for the trial_category composite key.
 * @author wgweis
 */
@Embeddable
public class Trial_CategoryPK implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9005867707266691669L;
	
	/**
	 * The trialid key is used to join to the trial table on the trial table id field.
	 */
	private long trialid;
	
	/**
	 * The category this trial, pointed to by the trialid field, belongs to.
	 */
	private short category;
	
	/**
	 * default constructor
	 */
	public Trial_CategoryPK()
	{
	}
	
	/**
	 * set both fields using this constructor
	 * @param trialid 
	 * @param category
	 */
	public Trial_CategoryPK( long trialid, short category )
	{
		this.trialid = trialid;
		this.category = category;
	}

	/**
	 * @param trialid the trialid to set
	 */
	public void setTrialid(long trialid) 
	{
		this.trialid = trialid;
	}
	
	/**
	 * @return the trialid
	 */
	public long getTrialid() 
	{
		return trialid;
	}
	
	/**
	 * @param category the category to set
	 */
	public void setCategory(short category) 
	{
		this.category = category;
	}
	
	/**
	 * @return the category
	 */
	public short getCategory() 
	{
		return category;
	}
	
	/**
	 * Returns whether this unique identifier
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other)
	{
		if (this == other)
			return true;
		if (!(other instanceof Trial_CategoryPK))
			return false;
		Trial_CategoryPK otherKey = (Trial_CategoryPK) (other);
		return (otherKey.trialid == this.trialid && otherKey.category == this.category);
	}

	/**
	 * Returns the the trialId hash code combined with the event date
	 * 
	 * @see java.lang.Object#hashCode()
	 * @see java.lang.Long#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return (int) trialid >>> 32 | (int) category;
	}
}
