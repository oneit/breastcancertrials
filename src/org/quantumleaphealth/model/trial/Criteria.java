/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import java.util.ArrayList;
import java.util.List;

/**
 * Combines two or more criterion/criteria and can be applied to a patient's
 * health record to determine a match for a protocol.
 * 
 * @author Tom Bechtold
 * @version 2008-02-19
 */
public class Criteria implements Criterion
{

	/**
	 * The operator that may be applied to a criteria's children.
	 * <OL>
	 * <LI>AND: each child must be <code>true</code> in order for this criteria
	 * to be <code>true</code>
	 * <LI>OR: at least one child must be <code>true</code> in order for this
	 * criteria to be <code>true</code>
	 * <LI>IF: if the first child is <code>true</code> then the second child
	 * must be <code>true</code> in order for this criteria to be
	 * <code>true</code>; if the first child is <code>false</code> then this
	 * criteria is <code>true</code>
	 * </OL>
	 */
	public enum Operator
	{
		AND, OR, IF
	};

	/**
	 * Operator that is applied to this criteria's children. Default is
	 * <code>Operator.AND</code>
	 */
	private Operator operator = Operator.AND;

	/**
	 * Children that comprise this criteria.
	 */
	private List<Criterion> children = new ArrayList<Criterion>();

	/**
	 * Instantiate an empty criteria
	 */
	public Criteria()
	{
	}

	/**
	 * Returns the operator.
	 * 
	 * @return the operator; guaranteed not to be <code>null</code>
	 */
	public Operator getOperator()
	{
		return operator;
	}

	/**
	 * Sets the operator.
	 * 
	 * @param operator
	 *            the operator to set
	 * @throws IllegalArgumentException
	 *             if <code>operator</code> is <code>null</code>
	 */
	public void setOperator(Operator operator) throws IllegalArgumentException
	{
		if (operator == null)
			throw new IllegalArgumentException("Must specify operator");
		this.operator = operator;
	}

	/**
	 * Returns the children
	 * 
	 * @return the children; guaranteed not to be <code>null</code>
	 */
	public List<Criterion> getChildren()
	{
		return children;
	}

	/**
	 * Sets the children.
	 * 
	 * @param children
	 *            the children to add
	 * @throws IllegalArgumentException
	 *             if <code>children</code> is <code>null</code>
	 */
	public void setChildren(List<Criterion> children) throws IllegalArgumentException
	{
		if (children == null)
			throw new IllegalArgumentException("Children may not be null");
		this.children = children;
	}

	/**
	 * @return the number of children
	 */
	public int getCount()
	{
		return children.size();
	}

	/**
	 * Returns the criterion of the first child or <code>null</code> if there
	 * are no children
	 * 
	 * @return the criterion of the first child or <code>null</code> if there
	 *         are no children
	 * @see org.quantumleaphealth.model.trial.Matchable#getFirstCriterion()
	 */
	public AbstractCriterion getFirstCriterion()
	{
		// Return the criterion of the first child
		return (children.size() == 0) || (children.get(0) == null) ? null : children.get(0).getFirstCriterion();
	}

	/**
	 * Returns a deep clone of this object's children.
	 * 
	 * @return a deep clone of this object's children
	 */
	@Override
	public Object clone()
	{
		try
		{
			// Create the clone and insert cloned children (e.g., deep clone).
			Criteria clone = (Criteria) (super.clone());
			List<Criterion> cloneChildren = new ArrayList<Criterion>(getCount());
			for (int index = 0; index < getCount(); index++)
				cloneChildren.add((Criterion) (children.get(index).clone()));
			clone.setChildren(cloneChildren);
			return clone;
		}
		catch (CloneNotSupportedException cloneNotSupportedException)
		{
			// Should never happen
			throw new RuntimeException(cloneNotSupportedException);
		}
	}

	/**
	 * Inverts the operator and children. This method performs the following
	 * transformations depending upon the <code>operator</code>:
	 * <ul>
	 * <li><code>NOT(a AND b) = NOT(a) OR NOT(b)</code>
	 * <li><code>NOT(a OR b) = NOT(a) AND NOT(b)</code>
	 * <li><code>NOT(IF a THEN b) = NOT((a AND b) OR NOT(a))
	 *      = NOT(a AND b) AND a = (NOT(a) OR NOT(b)) AND a = a AND NOT(b)</code>
	 * </ul>
	 */
	public void invert()
	{
		switch (operator)
		{
		case AND:
		case OR:
			// Flip the operator and invert the children
			operator = (operator == Operator.OR) ? Operator.AND : Operator.OR;
			for (Criterion matchable : children)
				matchable.invert();
			break;
		case IF:
			// Convert to AND and flip second child: NOT(IF a THEN b) = a AND
			// NOT(b)
			if ((children == null) || (children.size() != 2))
				throw new IllegalStateException("IF/THEN criteria children cannot be inverted");
			operator = Operator.AND;
			children.get(1).invert();
			break;
		default:
			throw new IllegalStateException("Criteria cannot be inverted");
		}
	}

	/**
	 * Serial version UID for serializable class
	 */
	private static final long serialVersionUID = 4483774452550186698L;
}
