/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * A postal code and its geocoding.
 * 
 * @author Tom Bechtold
 * @version 2008-06-05
 */
@Entity
public class PostalCodeGeocoding implements Serializable
{
	/**
	 * Unique identifier
	 */
	@Id
	private Long id;

	/**
	 * Postal Code (Zip Code)
	 */
	private int postalCode;
	/**
	 * The geocoding
	 */
	private Geocoding geocoding;

	/**
	 * @return the id
	 */
	public Long getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * @return the postalCode
	 */
	public int getPostalCode()
	{
		return postalCode;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 */
	public void setPostalCode(int postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * @return the geocoding
	 */
	public Geocoding getGeocoding()
	{
		return geocoding;
	}

	/**
	 * @param geocoding
	 *            the geocoding to set
	 */
	public void setGeocoding(Geocoding geocoding)
	{
		this.geocoding = geocoding;
	}

	/**
	 * Returns comparison of id
	 * 
	 * @return <code>true</code> if the ids are equal
	 * @param other
	 *            the other object to compare
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other)
	{
		if (super.equals(other))
			return true;
		if ((other == null) || !(other instanceof PostalCodeGeocoding))
			return false;
		PostalCodeGeocoding postalCode = (PostalCodeGeocoding) other;
		return ((id == null) || (postalCode.id == null)) ? false : id.equals(postalCode.id);
	}

	/**
	 * Returns the id's hash or superclass' if id is not set
	 * 
	 * @return the id's hash or superclass' if id is <code>null</code>
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return (id == null) || (id.longValue() == 0l) ? super.hashCode() : id.hashCode();
	}

	/**
	 * Version ID for serializable class
	 */
	private static final long serialVersionUID = 3666870253509692792L;
}
