/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import java.util.Arrays;

import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * A criterion that is compared to a patient's treatment procedure
 * characteristic. The superclass' fields store the following:
 * <ul>
 * <li><code>parent</code>: the type of procedure (e.g., surgery, radiation)</li>
 * <li><code>setOperation</code>, <code>requirement</code>, <code>isOther</code>
 * : the location</li>
 * <li><code>invert</code>: inverts an inclusion to an exclusion</li>
 * </ul>
 * 
 * @author Tom Bechtold
 * @version 2008-02-22
 */
public class TreatmentProcedureCriterion extends AssociativeCriterion
{

	/**
	 * The interventions of the procedure or <code>null</code> if not applicable
	 */
	private CharacteristicCode[] interventions;

	/**
	 * The attributes of the procedure
	 */
	private CharacteristicCode[] qualifiers;

	/**
	 * The maximum elapsed time allowed between the procedure and enrolling or
	 * <code>0</code> if not applicable
	 */
	private int maximumElapsedTime;

	/**
	 * The units of measure for <code>maximumElapsedTime</code> or
	 * <code>null</code> if not relevant
	 */
	private CharacteristicCode maximumElapsedTimeUnits;

	/**
	 * @return the interventions of the procedure or <code>null</code> if not
	 *         applicable
	 */
	public CharacteristicCode[] getInterventions()
	{
		return interventions;
	}

	/**
	 * @param interventions
	 *            the interventions of the procedure or <code>null</code> if not
	 *            applicable
	 */
	public void setInterventions(CharacteristicCode[] interventions)
	{
		this.interventions = interventions;
	}

	/**
	 * @return the qualifiers
	 */
	public CharacteristicCode[] getQualifiers()
	{
		return qualifiers;
	}

	/**
	 * @param qualifiers
	 *            the qualifiers to set
	 */
	public void setQualifiers(CharacteristicCode[] qualifiers)
	{
		this.qualifiers = qualifiers;
	}

	/**
	 * @return the maximumElapsedTime
	 */
	public int getMaximumElapsedTime()
	{
		return maximumElapsedTime;
	}

	/**
	 * @param maximumElapsedTime
	 *            the maximumElapsedTime to set
	 */
	public void setMaximumElapsedTime(int maximumElapsedTime)
	{
		this.maximumElapsedTime = maximumElapsedTime;
	}

	/**
	 * @return the maximumElapsedTimeUnits
	 */
	public CharacteristicCode getMaximumElapsedTimeUnits()
	{
		return maximumElapsedTimeUnits;
	}

	/**
	 * @param maximumElapsedTimeUnits
	 *            the maximumElapsedTimeUnits to set
	 */
	public void setMaximumElapsedTimeUnits(CharacteristicCode maximumElapsedTimeUnits)
	{
		this.maximumElapsedTimeUnits = maximumElapsedTimeUnits;
	}

	/**
	 * Returns the contents of the fields
	 * 
	 * @return the contents of the fields
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		// Remove close-bracket from superclass
		StringBuilder builder = new StringBuilder(super.toString());
		builder.deleteCharAt(builder.length() - 1);
		if ((interventions != null) && (interventions.length > 0))
			builder.append(",locations=").append(Arrays.toString(interventions));
		if ((qualifiers != null) && (qualifiers.length > 0))
			builder.append(",qualifiers=").append(Arrays.toString(qualifiers));
		if (maximumElapsedTime > 0)
		{
			builder.append(",maxElapsedTime=");
			if (maximumElapsedTimeUnits != null)
				builder.append(maximumElapsedTimeUnits);
		}
		return builder.append('}').toString();
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = -6688127366757205248L;
}
