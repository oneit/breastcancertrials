/**
 * (c) 2010 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * This entity represents an open, close, or update event for a trial. The
 * primary identifier is a composite of a foreign key to the trial and an event
 * date.
 * 
 * @author Tom Maple
 * @version 2010-01-26
 */
@Entity
public class TrialEvent implements Serializable
{

	/**
	 * Combines the trial's id and the event date into a composite primary
	 * identifier.
	 */
	@Embeddable
	public static class PrimaryKey implements Serializable
	{

		private static final long serialVersionUID = -2860481675526500556L;

		/**
		 * Trial's id
		 */
		@Column(name = "trial_id")
		private Long trialId;

		/**
		 * The date the event of the trial occurred.
		 */
		@Temporal(TemporalType.DATE)
		private Date eventDate;

		/**
		 * Returns whether this unique identifier
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object other)
		{
			if (this == other)
				return true;
			if (!(other instanceof PrimaryKey))
				return false;
			PrimaryKey otherKey = (PrimaryKey) (other);
			return (otherKey.trialId.equals(this.trialId)) && (otherKey.eventDate.equals(this.eventDate));
		}

		/**
		 * Returns the the trialId hash code combined with the event date
		 * 
		 * @see java.lang.Object#hashCode()
		 * @see java.lang.Long#hashCode()
		 */
		@Override
		public int hashCode()
		{
			return trialId.hashCode() >>> 32 | (int) (eventDate.getTime());
		}
	}

	/**
	 * Instantiates an empty trial event.
	 */
	public TrialEvent()
	{
	}

	/**
	 * Primary identifier, composed of foreign keys to <code>Trial</code> table
	 * and event date.
	 */
	@EmbeddedId
	private PrimaryKey id;

	/**
	 * The type of the event: 'O'pen, 'C'losed', or 'U'pdated.
	 */
	private String eventType;

	/**
	 * Trial's primary identifier as set by NCI/Cancer.gov
	 */
	private String primaryId;

	public PrimaryKey getId()
	{
		return id;
	}

	public Long getTrialId()
	{
		return id.trialId;
	}

	public void setEventDate(Long newTrialId)
	{
		this.id.trialId = newTrialId;
	}

	public Date getEventDate()
	{
		return id.eventDate;
	}

	public void setEventDate(Date newEventDate)
	{
		this.id.eventDate = newEventDate;
	}

	public String getEventType()
	{
		return eventType;
	}

	public void setEventType(String eventType)
	{
		this.eventType = eventType;
	}

	public String getPrimaryId()
	{
		return primaryId;
	}

	public void setPrimaryId(String newPrimaryId)
	{
		this.primaryId = newPrimaryId;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return id.hashCode();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		return id.equals(((TrialEvent) obj).id);
	}

	private static final long serialVersionUID = 1412051695352025126L;

	public static final String OPENED = "O";
	public static final String CLOSED = "C";
	public static final String UPDATED = "U";

}
