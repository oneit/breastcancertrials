/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.quantumleaphealth.model.screener.ScreenerGroup;

/**
 * Instantiation of a trial at a site. This entity represents the join table of
 * the trial and site that contains contact and invitation information. This
 * entity's primary identifier is a composite of foreign keys to the trial and
 * the site. The data behind this entity comes from a view called "trial_site"
 * that coalesces the <code>contact</code> field from a registered contact and a
 * private contact.
 * 
 * @author Tom Bechtold
 * @version 2008-12-13
 */
@Entity
@Table(name = "trial_site")
public class TrialSite implements Serializable
{

	/**
	 * Combines the trials's and site's unique identifiers (e.g., foreign keys)
	 * into a composite primary identifier.
	 */
	@Embeddable
	public static class PrimaryKey implements Serializable
	{
		/**
		 * Trial unique identifier
		 */
		@Column(name = "trial_id")
		public long trialId;
		/**
		 * Site unique identifier
		 */
		@Column(name = "site_id")
		public long siteId;

		/**
		 * Returns whether this unique identifier
		 * 
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object other)
		{
			if (this == other)
				return true;
			if (!(other instanceof PrimaryKey))
				return false;
			PrimaryKey otherKey = (PrimaryKey) (other);
			return (otherKey.trialId == trialId) && (otherKey.siteId == siteId);
		}

		/**
		 * Returns the hash code of the boolean or of those of the id's
		 * 
		 * @see java.lang.Object#hashCode()
		 * @see java.lang.Long#hashCode()
		 */
		@Override
		public int hashCode()
		{
			return (int) (trialId ^ (trialId >>> 32)) | (int) (siteId ^ (siteId >>> 32));
		}

		/**
		 * Version ID for serializable class
		 */
		private static final long serialVersionUID = 2260166004476208670L;
	}

	/**
	 * Primary identifier, composed of foreign keys of <code>trial</code> and
	 * <code>site</code>.
	 * 
	 * @see #trial
	 * @see #site
	 */
	@EmbeddedId
	private PrimaryKey id;

	/**
	 * Trial. The persistence annotation identifies the foreign key column.
	 */
	@ManyToOne
	@JoinColumn(name = "trial_id", insertable = false, updatable = false)
	private Trial trial;

	/**
	 * Site. The persistence annotation identifies the foreign key column.
	 */
	@ManyToOne
	@JoinColumn(name = "site_id", insertable = false, updatable = false)
	private Site site;

	/**
	 * Contact. The first choice is the BCT contact; second choice is the
	 * registered contact
	 */
	@ManyToOne
	private Contact contact;

	/**
	 * Principal investigator
	 */
	@ManyToOne
	private Contact principalInvestigator;

	/**
	 * Group responsible for accruing patients to this trial at this site or
	 * <code>null</code> if none
	 */
	@ManyToOne
	private ScreenerGroup screenerGroup;

	/**
	 * Notification email address
	 */
	private String notificationEmailAddress;

	/**
	 * The date/time the data was last modified
	 */
	private Date lastModified;

	/**
	 * Instantiates an empty site.
	 */
	public TrialSite()
	{
	}

	/**
	 * @return the primary id
	 */
	public PrimaryKey getId()
	{
		return id;
	}

	/**
	 * @return the trial
	 */
	public Trial getTrial()
	{
		return trial;
	}

	/**
	 * @param trial
	 *            the trial to set
	 */
	public void setTrial(Trial trial)
	{
		this.trial = trial;
	}

	/**
	 * @return the site
	 */
	public Site getSite()
	{
		return site;
	}

	/**
	 * @param site
	 *            the site to set
	 */
	public void setSite(Site site)
	{
		this.site = site;
	}

	/**
	 * @return the contact
	 */
	public Contact getContact()
	{
		return contact;
	}

	/**
	 * @param contact
	 *            the contact to set
	 */
	public void setContact(Contact contact)
	{
		this.contact = contact;
	}

	/**
	 * @return the principal investigator
	 */
	public Contact getPrincipalInvestigator()
	{
		return principalInvestigator;
	}

	/**
	 * @param principalInvestigator
	 *            the principal investigator to set
	 */
	public void setPrincipalInvestigator(Contact principalInvestigator)
	{
		this.principalInvestigator = principalInvestigator;
	}

	/**
	 * @return the group responsible for accruing patients to this trial at this
	 *         site or <code>null</code> if none
	 */
	public ScreenerGroup getScreenerGroup()
	{
		return screenerGroup;
	}

	/**
	 * @param screenerGroup
	 *            the group responsible for accruing patients to this trial at
	 *            this site or <code>null</code> if none
	 */
	public void setScreenerGroup(ScreenerGroup screenerGroup)
	{
		this.screenerGroup = screenerGroup;
	}

	/**
	 * Returns the notification email address
	 * 
	 * @return the notification email address
	 */
	public String getNotificationEmailAddress()
	{
		return notificationEmailAddress;
	}

	/**
	 * @param notificationEmailAddress
	 *            the notification email address to set
	 */
	public void setNotificationEmailAddress(String notificationEmailAddress)
	{
		this.notificationEmailAddress = notificationEmailAddress;
	}

	/**
	 * @return the date/time the data was last modified
	 */
	public Date getLastModified()
	{
		return lastModified;
	}

	/**
	 * @param lastModified
	 *            the date/time the data was last modified
	 */
	public void setLastModified(Date lastModified)
	{
		this.lastModified = lastModified;
	}

	/**
	 * Version ID for serializable class
	 */
	private static final long serialVersionUID = 3832671618664617950L;

	/**
	 * Returns whether its primary key equals another's or the trials and sites
	 * are equal. This implementation assumes that there is only one object for
	 * each trial-site relationship.
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 * @see #id
	 */
	@Override
	public boolean equals(Object other)
	{
		if (this == other)
			return true;
		if (!(other instanceof TrialSite))
			return false;
		TrialSite otherTrialSite = (TrialSite) (other);
		// If ids are not available then test trial and site individually
		if ((id != null) && (otherTrialSite.id != null))
			return id.equals(otherTrialSite.id);
		// If either trial or site is not known then we cannot compare
		return (trial != null) && trial.equals(otherTrialSite.trial) && (site != null)
				&& site.equals(otherTrialSite.site);
	}

	/**
	 * Returns the primary key's hashcode
	 * 
	 * @see java.lang.Object#hashCode()
	 * @see #id
	 */
	@Override
	public int hashCode()
	{
		return (id == null) ? super.hashCode() : id.hashCode();
	}
}
