/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

/**
 * A single criterion that can be applied to a patient's health characteristic
 * to determine a match.
 * 
 * @author Tom Bechtold
 * @version 2008-06-30
 */
public abstract class AbstractCriterion implements Criterion
{
	/**
	 * Whether or not the logic is inverted, e.g., <code>NOT</code>
	 */
	private boolean invert;

	/**
	 * Flips the <code>invert</code> field
	 */
	public void invert()
	{
		invert = !invert;
	}

	/**
	 * Returns the invert.
	 * 
	 * @return the invert
	 */
	public boolean isInvert()
	{
		return invert;
	}

	/**
	 * @param invert
	 *            the invert to set
	 */
	public void setInvert(boolean invert)
	{
		this.invert = invert;
	}

	/**
	 * Returns this object
	 */
	public AbstractCriterion getFirstCriterion()
	{
		return this;
	}

	/**
	 * Returns <code>true</code> if this criterion matches the same patient
	 * characteristic as another. This method simply compares the classes for
	 * equality; subclasses should implement their own comparison.
	 * 
	 * @param other
	 *            the other criterion to compare
	 * @return <code>true</code> if this criterion matches the same patient
	 *         characteristic as <code>other</code>.
	 */
	public boolean matchesSameCharacteristic(AbstractCriterion other)
	{
		return (other != null) && other.getClass().equals(getClass());
	}

	/**
	 * Clones the object.
	 */
	public Object clone()
	{
		try
		{
			return super.clone();
		}
		catch (CloneNotSupportedException cloneNotSupportedException)
		{
			// Should never happen as we are cloneable
			throw new RuntimeException(cloneNotSupportedException);
		}
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = -7224314110190144237L;
}
