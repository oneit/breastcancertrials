/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import java.util.ArrayList;

/**
 * A trial's eligibility criteria.
 * 
 * @author Tom Bechtold
 * @version 2008-06-09
 */
public class EligibilityCriteria extends ArrayList<EligibilityCriterion>
{
	/**
	 * The last date/time this protocol was modified
	 */
	private long lastModified;

	/*
	 * The source of eligibility criteria
	 */
	public enum Source
	{
		UNASSIGNED, SPONSOR, REGISTRY, DATAENTRY
	}

	/**
	 * The source of the eligibility criteria, guaranteed to be non-
	 * <code>null</code>
	 */
	private Source sourceEligibilityCriteria = Source.UNASSIGNED;

	/**
	 * @return last modified date/time
	 */
	public long getLastModified()
	{
		return lastModified;
	}

	/**
	 * Sets the last modified date/time
	 * 
	 * @param lastModified
	 *            the last modified date/time to set
	 */
	public void setLastModified(long lastModified)
	{
		this.lastModified = lastModified;
	}

	/**
	 * @return source of eligibility criteria
	 */
	public Source getSourceEligibilityCriteria()
	{
		return sourceEligibilityCriteria;
	}

	/**
	 * Sets the source of eligibility criteria
	 * 
	 * @param sourceEligibilityCriteria
	 *            the source of eligibility criteria to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setSourceEligibilityCriteria(Source sourceEligibilityCriteria) throws IllegalArgumentException
	{
		if (sourceEligibilityCriteria == null)
			throw new IllegalArgumentException("must specify parameter");
		this.sourceEligibilityCriteria = sourceEligibilityCriteria;
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = 6143284510807253680L;
}
