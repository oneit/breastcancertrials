/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial.test;

import org.quantumleaphealth.screen.AttributeAssociativeMatchable;
import org.testng.annotations.Test;

/**
 * Unit tests for the <code>AttributeAssociativeMatchable</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-07
 * @see AttributeAssociativeMatchable
 */
@Test(description = "Multiple-choice associative matchable", groups = { "screen", "composite" })
public class TestCriterion
{

	public void testToBeImplemented()
	{
		throw new AssertionError("To be implemented");
	}

}
