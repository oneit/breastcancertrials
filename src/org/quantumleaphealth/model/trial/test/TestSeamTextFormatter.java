package org.quantumleaphealth.model.trial.test;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Reader;
import java.io.StringReader;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.Formatter;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.NullFragmenter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.util.Version;
import org.jboss.seam.text.SeamTextLexer;
import org.jboss.seam.text.SeamTextParser;
import org.testng.annotations.Test;

import org.apache.lucene.analysis.charfilter.HTMLStripCharFilter;

/**
 * Found that this tag was throwing an invalid character exception if '=' was followed immediately by '<'.
 * 
 * @author wgweis
 *
 */
@Test(description = "Test s:formattedText tag", groups = {"composite" })
public class TestSeamTextFormatter 
{
	private static String formattedSearchString = "\"groups\"";
	
    /**
     * The simpleHTMLFormatter instance variable will do for most tags, such as <h:outputText> or pure HTML.
     */
	private static final SimpleHTMLFormatter simpleHTMLFormatter =  new SimpleHTMLFormatter("<span class=\"highlight\">", "</span>");

	
    @Test() public void testValidTextFormatting() 
    {
    	try
    	{
    		Reader s = new StringReader("All participants will undergo blood sample collection, which will be used to measure circulating tumor cells with the following techniques:\n\n" +
    				"= <span>Experimental</span> technique using stem cell marker retinaldehyde dehydrogenase\n" +
    				"=Standard technique using CellSearch¨");
        
    		SeamTextLexer lexer = new SeamTextLexer(s);
    		SeamTextParser parser = new SeamTextParser(lexer);
    		parser.startRule();
    		System.out.println(parser);
    		assert(true);
    	}
    	catch (Exception e)
    	{
    		assert(false) : "Failed with exception " + e.toString();
    	}
    }
    
    
    /**
     * Note the space after '='
     */
    @Test() public void testInvlidValidTextFormatting() 
    {
    	try
    	{
    		Reader s = new StringReader("All participants will undergo blood sample collection, which will be used to measure circulating tumor cells with the following techniques:\n\n" +
    				"=<span>Experimental</span> technique using stem cell marker retinaldehyde dehydrogenase\n" +
    				"=Standard technique using CellSearch¨");
        
    		SeamTextLexer lexer = new SeamTextLexer(s);
    		SeamTextParser parser = new SeamTextParser(lexer);
    		parser.startRule();
    		System.out.println(parser);
    		assert(false) : "This should have failed on the call to startRule().";
    	}
    	catch (Exception e)
    	{
    		assert(true);
    	}
    }
    
	/**
	 * Main method to get highlighted text for different fields and text.
	 * 
	 * @param fieldName
	 * @param text
	 * @return formatted text if a match, original text if no match (highlighting returns a null if nothing in the text can be highlighted).
	 */
	private String getHighlightedSearchString(String fieldName, String text, Formatter formatter)
	{
		try 
		{
		    QueryParser parser = new QueryParser(Version.LUCENE_46, fieldName, new StandardAnalyzer(Version.LUCENE_46));
			parser.setAllowLeadingWildcard(true);
			parser.setDefaultOperator(Operator.AND);

			
			Query query = parser.parse(formattedSearchString);
			
		    TokenStream tokens = new StandardAnalyzer(Version.LUCENE_46).tokenStream(fieldName, new StringReader(text));
		    QueryScorer scorer = new QueryScorer(query, fieldName);
		    
		    Highlighter highlighter = new Highlighter(formatter, scorer);
		    highlighter. setTextFragmenter ( new NullFragmenter ( ) ) ; // get the whole string as a token, as none of the strings is large.
		    String result = highlighter.getBestFragment(tokens, text);
			return result == null || result.trim().length() == 0? text: result;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return text;
		}
	}
    
	@Test() public void testHTMLFormatting()
    {
    	String text = "Participants will be randomly assigned to 1 of 2 groups:\n\n" +
		 "*Group 1: Acupuncture*\n\n" +
		 "=Acupuncture treatment for 30 minutes, twice a week for 6 weeks\n" +
		 "=Standard lymphedema treatment\n\n" + 
		 "*Group 2: Wait-list*\n\n" +
		 "=Wait list for 6 weeks (no Acupuncture treatment)\n" +
		 "=Standard lymphedema treatment\n\n" + 
		 "*Participants in 2 will receive Acupuncture treatment as in 1, after 6 weeks.*";
    	
    	try
    	{
	        Reader s = new StringReader(text);
	        
			SeamTextLexer lexer = new SeamTextLexer(s);
			SeamTextParser seamTextParser = new SeamTextParser(lexer);
			seamTextParser.startRule();

			// this is what we are actually going to match on.
			String seamTextLexerOutput = seamTextParser.toString();
			

    		FileOutputStream fos = new FileOutputStream(new File("/temp/testReader.txt"));

			fos.write(seamTextLexerOutput.getBytes());
			fos.write("+++++++++++++++++++++++++".getBytes());

			
    		HTMLStripCharFilter testReader = new HTMLStripCharFilter(new StringReader(seamTextLexerOutput));
			
    		int ch;
    		while ((ch = testReader.read()) > 0)
    		{
    			fos.write(ch);
    		}
    		fos.close();
    		assert(true);

    	}
    	catch (Exception e)
    	{
    		e.printStackTrace();
    		assert(false);
    	}
    }

}
