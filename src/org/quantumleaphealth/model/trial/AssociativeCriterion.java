/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import java.util.Arrays;

import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Matches a patient characteristic that is associated with a set. Note that
 * this class does not support the "otherwise-not-specified" (also known as
 * "other") member in a parent-child relationship. Because "other" is defined in
 * the context of the specified members, adding a member to a set would change
 * the definition of what "other" represents. Therefore objects of this class
 * would break if the set of members changed over time -- unless the members of
 * the set were stored in this class.
 * 
 * @author Tom Bechtold
 * @version 2008-05-23
 */
public class AssociativeCriterion extends AbstractCriterion
{
	/**
	 * Types of association between sets
	 */
	public enum SetOperation
	{
		/**
		 * Any-Or: if any answer is a choice within the requirement set, e.g.,
		 * "Requirement contains any of choice1, choice2, etc."
		 */
		INTERSECT,
		/**
		 * All-And: each requirement is selected as a choice, e.g.,
		 * "Choice1 and choice2 and ... etc. is contained within requirement"
		 */
		SUPERSET,
		/**
		 * Only-And/Or: if each answer is a choice within the requirement set,
		 * e.g.,
		 * "Requirement contains choice1 and/or choice2 and/or ... etc. ONLY"
		 */
		SUBSET
	}

	/**
	 * The parent of the patient characteristics
	 */
	private CharacteristicCode parent;

	/**
	 * How the patient's characteristics are compared against the requirement,
	 * guaranteed to be non-<code>null</code>.
	 */
	private SetOperation setOperation = SetOperation.INTERSECT;

	/**
	 * The set of characteristics that the patient is compared to or
	 * <code>null</code> to represent <tt>any</tt> whose parent is
	 * <code>parent</code>
	 */
	private CharacteristicCode[] requirement;

	/**
	 * @return the parent of the patient characteristics
	 */
	public CharacteristicCode getParent()
	{
		return parent;
	}

	/**
	 * @param parent
	 *            the parent of the patient characteristics
	 */
	public void setParent(CharacteristicCode parent)
	{
		this.parent = parent;
	}

	/**
	 * @return how the patient's characteristics are compared against the
	 *         requirement, guaranteed to be non-<code>null</code>
	 */
	public SetOperation getSetOperation()
	{
		return setOperation;
	}

	/**
	 * @param setOperation
	 *            how the patient's characteristics are compared against the
	 *            requirement
	 * @throws IllegalArgumentException
	 *             if <code>setOperation</code> is <code>null</code>
	 */
	public void setSetOperation(SetOperation setOperation) throws IllegalArgumentException
	{
		if (setOperation == null)
			throw new IllegalArgumentException("must specify set operation");
		this.setOperation = setOperation;
	}

	/**
	 * @return the set of characteristics that the patient is compared to or
	 *         <code>null</code> to represent <tt>any</tt> whose parent is
	 *         <code>parent</code>
	 */
	public CharacteristicCode[] getRequirement()
	{
		return requirement;
	}

	/**
	 * @param requirement
	 *            the set of characteristics that the patient is compared to or
	 *            <code>null</code> to represent <tt>any</tt> whose parent is
	 *            <code>parent</code>
	 */
	public void setRequirement(CharacteristicCode[] requirement)
	{
		this.requirement = requirement;
	}

	/**
	 * Returns <code>true</code> if this criterion matches the same patient
	 * characteristic as another. This method adds to the superclass'
	 * implementation a comparison of the <code>parent</code> characteristic.
	 * 
	 * @param other
	 *            the other criterion to compare
	 * @return <code>true</code> if this criterion matches the same parent
	 *         characteristic as <code>other</code>.
	 * @see org.quantumleaphealth.model.trial.AbstractCriterion#matchesSameCharacteristic(org.quantumleaphealth.model.trial.AbstractCriterion)
	 */
	@Override
	public boolean matchesSameCharacteristic(AbstractCriterion other)
	{
		// The superclass' implementation guarantees that other is the same
		// class
		if (!super.matchesSameCharacteristic(other))
			return false;
		CharacteristicCode otherParent = ((AssociativeCriterion) (other)).parent;
		return (parent == null) ? (otherParent == null) : parent.equals(otherParent);
	}

	/**
	 * Returns the contents of the fields
	 * 
	 * @return the contents of the fields
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder(super.toString());
		builder.append("{parent=").append(parent);
		builder.append(",setOperation=").append(setOperation);
		builder.append(",requirement=").append(requirement == null ? "any" : Arrays.toString(requirement));
		if (isInvert())
			builder.append(",not");
		return builder.append('}').toString();
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = 4645699279572424954L;
}
