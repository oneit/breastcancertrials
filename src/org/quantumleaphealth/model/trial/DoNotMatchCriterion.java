package org.quantumleaphealth.model.trial;

import org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes;
import org.quantumleaphealth.ontology.CharacteristicCode;

public class DoNotMatchCriterion  extends AbstractCriterion
{
	private static final long serialVersionUID = 567414254877490371L;
	
	/**
	 * AFFIRMATIVE or NEGATIVE
	 */
	private CharacteristicCode characteristicCode = BreastCancerCharacteristicCodes.DONOTMATCH;

	/**
	 * @return the patient characteristic
	 */
	public CharacteristicCode getCharacteristicCode()
	{
		return characteristicCode;
	}

	/**
	 * @param characteristicCode
	 *            the patient characteristic to set
	 */
	public void setCharacteristicCode(CharacteristicCode characteristicCode)
	{
		this.characteristicCode = characteristicCode;
	}

	/**
	 * Returns <code>true</code> if this criterion matches the same patient
	 * characteristic as another. This method adds to the superclass'
	 * implementation a comparison of the <code>characteristicCode</code>.
	 * 
	 * @param other
	 *            the other criterion to compare
	 * @return <code>true</code> if this criterion matches the same
	 *         characteristic as <code>other</code>.
	 * @see org.quantumleaphealth.model.trial.AbstractCriterion#matchesSameCharacteristic(org.quantumleaphealth.model.trial.AbstractCriterion)
	 */
	@Override
	public boolean matchesSameCharacteristic(AbstractCriterion other)
	{
		// The superclass' implementation guarantees that other is the same
		// class
		if (!super.matchesSameCharacteristic(other))
			return false;
		CharacteristicCode otherCode = ((DoNotMatchCriterion) (other)).characteristicCode;
		return (characteristicCode == null) ? (otherCode == null) : characteristicCode.equals(otherCode);
	}

	/**
	 * Returns the contents of the fields
	 * 
	 * @return the contents of the fields
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder(super.toString());
		builder.append("{characteristic=").append(characteristicCode);
		if (isInvert())
			builder.append(",not");
		return builder.append('}').toString();
	}
}
