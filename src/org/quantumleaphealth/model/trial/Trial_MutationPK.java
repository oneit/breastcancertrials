package org.quantumleaphealth.model.trial;

import java.io.Serializable;

import javax.persistence.Embeddable;

import org.quantumleaphealth.model.trial.TrialEvent.PrimaryKey;

/**
 * This is the entity class for the trial_mutation composite key.
 * @author wgweis
 */
@Embeddable
public class Trial_MutationPK implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -9005867707266691669L;
	
	/**
	 * The trialid key is used to join to the trial table on the trial table id field.
	 */
	private long trialid;
	
	/**
	 * The Mutation this trial, pointed to by the trialid field, belongs to.
	 */
	private short mutation;
	
	/**
	 * default constructor
	 */
	public Trial_MutationPK()
	{
	}
	
	/**
	 * set both fields using this constructor
	 * @param trialid 
	 * @param mutation
	 */
	public Trial_MutationPK( long trialid, short mutation )
	{
		this.trialid = trialid;
		this.mutation = mutation;
	}

	/**
	 * @param trialid the trialid to set
	 */
	public void setTrialid(long trialid) 
	{
		this.trialid = trialid;
	}
	
	/**
	 * @return the trialid
	 */
	public long getTrialid() 
	{
		return trialid;
	}
	
	/**
	 * @param mutation the mutation to set
	 */
	public void setMutation(short mutation) 
	{
		this.mutation = mutation;
	}
	
	/**
	 * @return the mutation
	 */
	public short getMutation() 
	{
		return mutation;
	}
	
	/**
	 * Returns whether this unique identifier
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other)
	{
		if (this == other)
			return true;
		if (!(other instanceof Trial_MutationPK))
			return false;
		Trial_MutationPK otherKey = (Trial_MutationPK) (other);
		return (otherKey.trialid == this.trialid && otherKey.mutation == this.mutation);
	}

	/**
	 * Returns the the trialId hash code combined with the event date
	 * 
	 * @see java.lang.Object#hashCode()
	 * @see java.lang.Long#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return (int) trialid >>> 32 | (int) mutation;
	}
}
