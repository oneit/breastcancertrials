package org.quantumleaphealth.model.trial;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * This is the entity class for the trial_category table.  This table
 * allows a trial to be put into multiple categories.
 *  
 * @author wgweis
 */
@Entity
public class Trial_Category 
{
	
	@Id
	private Trial_CategoryPK trial_CategoryPK;

	/**
	 * The date this record was created.
	 */
	private Date created;
	
	/**
	 * The date this record was modified.  Not currently used, but there for consistency.
	 */
	private Date modified;
	
	/**
	 * @param trial_CategoryPK the trial_CategoryPK to set
	 * The trial_category table uses a composite key.
	 */
	public void setTrial_CategoryPK(Trial_CategoryPK trial_CategoryPK) 
	{
		this.trial_CategoryPK = trial_CategoryPK;
	}

	/**
	 * @return the trial_CategoryPK
	 */
	public Trial_CategoryPK getTrial_CategoryPK() 
	{
		return trial_CategoryPK;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) 
	{
		this.created = created;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() 
	{
		return created;
	}

	/**
	 * @param modified the modified to set
	 */
	public void setModified(Date modified) 
	{
		this.modified = modified;
	}

	/**
	 * @return the modified
	 */
	public Date getModified() 
	{
		return modified;
	}
}
