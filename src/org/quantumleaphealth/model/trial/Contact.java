/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.quantumleaphealth.model.Person;

/**
 * Contact person for a site
 * 
 * @author Tom Bechtold
 * @version 2009-02-03
 */
@Entity
public class Contact implements Person, Serializable
{

	/**
	 * Unique identifier
	 */
	@Id
	private Long id;

	/**
	 * Last name
	 */
	private String surName;

	/**
	 * First name
	 */
	private String givenName;

	/**
	 * Middle initial
	 */
	private String middleInitial;

	/**
	 * Prefix
	 */
	private String prefix;

	/**
	 * Generation suffix
	 */
	private String generationSuffix;

	/**
	 * Professional suffix
	 */
	private String professionalSuffix;

	/**
	 * Title
	 */
	private String title;

	/**
	 * Telephone number
	 */
	private String phone;

	/**
	 * Email address
	 */
	private String email;

	/**
	 * @return the id
	 */
	public Long getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * @return the surname
	 * @see org.quantumleaphealth.model.Person#getSurName()
	 */
	public String getSurName()
	{
		return surName;
	}

	/**
	 * @param surName
	 *            the last name to set; empty string is stored as
	 *            <code>null</code>
	 */
	public void setSurName(String surName)
	{
		this.surName = trim(surName);
	}

	/**
	 * @return the given name
	 * @see org.quantumleaphealth.model.Person#getGivenName()
	 */
	public String getGivenName()
	{
		return givenName;
	}

	/**
	 * @param givenName
	 *            the given name to set; empty string is stored as
	 *            <code>null</code>
	 */
	public void setGivenName(String givenName)
	{
		this.givenName = trim(givenName);
	}

	/**
	 * @return the middleInitial
	 * @see org.quantumleaphealth.model.Person#getMiddleInitial()
	 */
	public String getMiddleInitial()
	{
		return middleInitial;
	}

	/**
	 * @param middleInitial
	 *            the middleInitial to set; empty string is stored as
	 *            <code>null</code>
	 */
	public void setMiddleInitial(String middleInitial)
	{
		this.middleInitial = trim(middleInitial);
	}

	/**
	 * @return the prefix
	 * @see org.quantumleaphealth.model.Person#getPrefix()
	 */
	public String getPrefix()
	{
		return prefix;
	}

	/**
	 * @param prefix
	 *            the prefix to set; empty string is stored as <code>null</code>
	 */
	public void setPrefix(String prefix)
	{
		this.prefix = trim(prefix);
	}

	/**
	 * @return the generation suffix
	 * @see org.quantumleaphealth.model.Person#getGenerationSuffix()
	 */
	public String getGenerationSuffix()
	{
		return generationSuffix;
	}

	/**
	 * @param generationSuffix
	 *            the generation suffix to set; empty string is stored as
	 *            <code>null</code>
	 */
	public void setGenerationSuffix(String generationSuffix)
	{
		this.generationSuffix = trim(generationSuffix);
	}

	/**
	 * @return the professional suffix
	 * @see org.quantumleaphealth.model.Person#getProfessionalSuffix()
	 */
	public String getProfessionalSuffix()
	{
		return professionalSuffix;
	}

	/**
	 * @param professionalSuffix
	 *            the professional suffix to set; empty string is stored as
	 *            <code>null</code>
	 */
	public void setProfessionalSuffix(String professionalSuffix)
	{
		this.professionalSuffix = trim(professionalSuffix);
	}

	/**
	 * @return the title
	 * @see org.quantumleaphealth.model.Person#getTitle()
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param title
	 *            the title to set; empty string is stored as <code>null</code>
	 */
	public void setTitle(String title)
	{
		this.title = trim(title);
	}

	/**
	 * @return the telephone number
	 * @see org.quantumleaphealth.model.Person#getPhone()
	 */
	public String getPhone()
	{
		return phone;
	}

	/**
	 * @param phone
	 *            the telephone number to set; empty string is stored as
	 *            <code>null</code>
	 */
	public void setPhone(String phone)
	{
		this.phone = trim(phone);
	}

	/**
	 * @return the email address
	 * @see org.quantumleaphealth.model.Person#getEmail()
	 */
	public String getEmail()
	{
		return email;
	}

	/**
	 * @param email
	 *            the email address to set; empty string is stored as
	 *            <code>null</code>
	 */
	public void setEmail(String email)
	{
		this.email = trim(email);
	}

	/**
	 * Returns a trimmed string or <code>null</code> if the string is empty
	 * 
	 * @param string
	 *            the string to trim
	 * @return a trimmed string or <code>null</code> if the string is empty
	 */
	private static String trim(String string)
	{
		if (string == null)
			return null;
		string = string.trim();
		return (string.length() == 0) ? null : string;
	}

	/**
	 * Returns comparison of id and phone number
	 * 
	 * @return <code>true</code> if the ids are equal or if both ids are
	 *         <code>null</code> and the phone numbers are equal
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (super.equals(obj))
			return true;
		if (!(obj instanceof Contact))
			return false;
		Contact contact = (Contact) obj;
		if (id != null)
			return id.equals(contact.id);
		if (contact.id != null)
			return false;
		return (phone == null) ? false : phone.equals(contact.phone);
	}

	/**
	 * Returns the id's hash or phone number's hash or superclass' if id/phone
	 * number is not set
	 * 
	 * @return the id's hash or phone number's hash or superclass' if id and
	 *         phone number are <code>null</code>
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		if (id != null)
			return id.hashCode();
		if (phone != null)
			return phone.hashCode();
		return super.hashCode();
	}

	/**
	 * Version ID for serializable class
	 */
	private static final long serialVersionUID = -300863619550358683L;
}
