/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Matches a single patient characteristic.
 * 
 * @author Tom Bechtold
 * @version 2008-02-22
 */
public class BooleanCriterion extends AbstractCriterion
{

	/**
	 * The patient characteristic
	 */
	private CharacteristicCode characteristicCode;

	/**
	 * @return the patient characteristic
	 */
	public CharacteristicCode getCharacteristicCode()
	{
		return characteristicCode;
	}

	/**
	 * @param characteristicCode
	 *            the patient characteristic to set
	 */
	public void setCharacteristicCode(CharacteristicCode characteristicCode)
	{
		this.characteristicCode = characteristicCode;
	}

	/**
	 * Returns <code>true</code> if this criterion matches the same patient
	 * characteristic as another. This method adds to the superclass'
	 * implementation a comparison of the <code>characteristicCode</code>.
	 * 
	 * @param other
	 *            the other criterion to compare
	 * @return <code>true</code> if this criterion matches the same
	 *         characteristic as <code>other</code>.
	 * @see org.quantumleaphealth.model.trial.AbstractCriterion#matchesSameCharacteristic(org.quantumleaphealth.model.trial.AbstractCriterion)
	 */
	@Override
	public boolean matchesSameCharacteristic(AbstractCriterion other)
	{
		// The superclass' implementation guarantees that other is the same
		// class
		if (!super.matchesSameCharacteristic(other))
			return false;
		CharacteristicCode otherCode = ((BooleanCriterion) (other)).characteristicCode;
		return (characteristicCode == null) ? (otherCode == null) : characteristicCode.equals(otherCode);
	}

	/**
	 * Returns the contents of the fields
	 * 
	 * @return the contents of the fields
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder(super.toString());
		builder.append("{characteristic=").append(characteristicCode);
		if (isInvert())
			builder.append(",not");
		return builder.append('}').toString();
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -6185985686564273461L;
}
