/**
 * (c) 2018 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial; 

public class TrialResponse
	{
		/**
		 * The trial being described
		 */
		private  Trial trial;
		private  String currentStatus;
		private  String versionComment;
		private byte[] eligibility;
		private String postDate;


		/**
		 * 
		 */
		public TrialResponse() {
			super();
		}
		/**
		 * Instantiate an descriptive trial object
		 * 
		 * @param trial
		 *            the trial
		 * @throws IllegalArgumentException
		 *             if parameter or its primary id is empty
		 */
		public TrialResponse(Trial trial, String currentStatus, String versionComment, byte[] eligibility, String postDate) throws IllegalArgumentException
		{
			if ((trial == null) || (trial.getId() == null) // || (trial.getId().longValue() <= 0)
					|| (trim(trial.getPrimaryID()) == null))
				throw new IllegalArgumentException("trial's id and/or primaryID not specified");
			this.trial = trial;
			this.currentStatus = currentStatus;
			this.versionComment = versionComment;
			this.eligibility = eligibility;
			this.postDate = postDate;
		}

		/**
		 * @return the postDate
		 */
		public String getPostDate() {
			return postDate;
		}


		/**
		 * @param postDate the postDate to set
		 */
		public void setPostDate(String postDate) {
			this.postDate = postDate;
		}
		
		/**
		 * @return the trial
		 */
		public Trial getTrial() {
			return trial;
		}

		/**
		 * @return the currentStatus
		 */
		public String getCurrentStatus() {
			return currentStatus;
		}

		/**
		 * @return the versionComment
		 */
		public String getVersionComment() {
			return versionComment;
		}
		
		
		
		/**
		 * @param trial the trial to set
		 */
		public void setTrial(Trial trial) {
			this.trial = trial;
		}


		/**
		 * @param currentStatus the currentStatus to set
		 */
		public void setCurrentStatus(String currentStatus) {
			this.currentStatus = currentStatus;
		}


		/**
		 * @param versionComment the versionComment to set
		 */
		public void setVersionComment(String versionComment) {
			this.versionComment = versionComment;
		}

		/**
		 * @return the eligibility
		 */
		public byte[] getEligibility() {
			return eligibility;
		}


		/**
		 * @param eligibility the eligibility to set
		 */
		public void setEligibility(byte[] eligibility) {
			this.eligibility = eligibility;
		}


		private static String trim(String string)
		{
			if (string == null)
				return null;
			string = string.trim();
			return (string.length() == 0) ? null : string;
		}
		
	}