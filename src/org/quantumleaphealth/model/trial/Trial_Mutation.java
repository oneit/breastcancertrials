package org.quantumleaphealth.model.trial;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * This is the entity class for the trial_category table.  This table
 * allows a trial to be put into multiple categories.
 *  
 * @author wgweis
 */
@Entity
public class Trial_Mutation 
{
	
	@Id
	private Trial_MutationPK trial_MutationPK;

	/**
	 * The date this record was created.
	 */
	private Date created;
	
	/**
	 * The date this record was modified.  Not currently used, but there for consistency.
	 */
	private Date modified;
	
	/**
	 * @param trial_MutationPK the trial_MutationPK to set
	 * The trial_mutation table uses a composite key.
	 */
	public void setTrial_MutationPK(Trial_MutationPK trial_MutationPK) 
	{
		this.trial_MutationPK = trial_MutationPK;
	}

	/**
	 * @return the trial_MutationPK
	 */
	public Trial_MutationPK getTrial_MutationPK() 
	{
		return trial_MutationPK;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) 
	{
		this.created = created;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() 
	{
		return created;
	}

	/**
	 * @param modified the modified to set
	 */
	public void setModified(Date modified) 
	{
		this.modified = modified;
	}

	/**
	 * @return the modified
	 */
	public Date getModified() 
	{
		return modified;
	}
}
