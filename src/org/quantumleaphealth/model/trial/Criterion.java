/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import java.io.Serializable;

/**
 * Allowed to match to a patient's health characteristics.
 * 
 * @author Tom Bechtold
 * @version 2008-02-19
 */
public interface Criterion extends Serializable, Cloneable
{

	/**
	 * Returns the first abstract criterion or <code>null</code> if there is no
	 * first criterion.
	 * 
	 * @return the first abstract criterion or <code>null</code> if there is no
	 *         first criterion
	 */
	public AbstractCriterion getFirstCriterion();

	/**
	 * Inverts the logic.
	 */
	public void invert();

	/**
	 * Clones the object.
	 * 
	 * @throws CloneNotSupportedException
	 *             if the class does not support cloning
	 */
	public Object clone() throws CloneNotSupportedException;
}
