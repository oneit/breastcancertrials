/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.quantumleaphealth.xml.DecodingException;
import org.quantumleaphealth.xml.DecodingExceptionListener;


/**
 * Encapsulates a protocol's data. Because this class annotates the
 * <code>id</code> column on the <code>getId()</code> getter method, the access
 * type for persistence will be getter methods instead of fields. This
 * persistence access type allows for the transient field
 * <code>eligibilityCriteria</code> to be marshaled into XML format for
 * persisting.
 * 
 * @author Tom Bechtold
 * @version 2009-02-03
 */
@Entity
public class Trial implements Serializable
{
	/**
	 * Generated ID for serialization. 
	 */
	private static final long serialVersionUID = -1421252283048563863L;


	/*
	 * The phase of a protocol
	 */
	public enum Phase
	{
		NA, O, I, I_II, II, II_III, III, III_IV, IV
	}

	public enum Mutation
	{
		AKT("AKT","AKT"),
		ALK("ALK","ALK"),
		AR("AR","AR"),
		BARD1("BARD1","BARD1"),
		BRCA1_2("BRCA1_2","BRCA1/2 (tumor)"),
		BRIP1("BRIP1","BRIP1"),
		CD205("CD205","CD205"),
		CD70("CD70","CD70"),
		CHEK2("CHEK2","CHEK2 or CHEK1"),
		ERBB2("ERBB2","HER2/ERBB2"),
		ESR1("ESR1","ESR1"),
		FGFR("FGFR","FGFR"),
		HLA("HLA","HLA"),
		MET("MET","MET or C-Met"),
		NTRK("NTRK","NTRK"),
		PALB2("PALB2","PALB2"),
		PIK3CA("PIK3CA","PIK3CA or PI3K"),
		PTEN("PTEN","PTEN"),
		RAD51("RAD51","RAD51"),
		RAF("RAF","RAF (including BRAF)"),
		RAS("RAS","RAS (KRAS or NRAS)"),
		RB("RB","RB"),
		ROS1("ROS1","ROS1"),
		TP53("TP53","TP53"),
		DMMR("DMMR","dMMR/MSI-H")
		;
		
		private final String mutationName;
		private final String mutationDisplayName;
		
	    private static final Map<String, Mutation> lookup = new HashMap<String, Mutation>();

	    static {
	        for (Mutation d : Mutation.values()) {
	            lookup.put(d.getMutationName(), d);
	        }
	    }
		
		private Mutation(String name,  String mutationDisplayName)
		{
			this.mutationName = name;
			this.mutationDisplayName = mutationDisplayName;			
		}
		
		public String getMutationName()
		{
			return this.mutationName;
		}
		
		public String getMutationDisplayName()
		{
			return this.mutationDisplayName;
		}
		
		public static Mutation get(String abbreviation) {
	        return lookup.get(abbreviation);
	    }
		
	}
	
	/*
	 * The type of a protocol.  The webCategory string parameter is how the category is passed
     * in by the urlrewriter.xml.  Currently the same for all the original primary categories
     * ( up to TREATMENT_OTHER ).
	 */
	public enum Type
	{
		OTHER("OTHER", "", ""),						                							// type 0 
		PREVENTION("PREVENTION", "", 							                				// type 1
				"Looking for ways to stop breast cancer from developing for the first time. "), 
		DIAGNOSIS("Diagnosis", "", 									           	// type 2 
				""),
		TREATMENT("TREATMENT", "", ""), 				                						// type 3
		BEHAVIORAL("BEHAVIORAL", "", ""), 		                								// type 4
		LABORATORY("LABORATORY", "", 												// type 5
				"Looking at ways to help patients make decisions about their breast cancer treatment."), 
		DIAGNOSTIC("DIAGNOSTIC", 																// type 6
				"Diagnosing Breast Cancer", "Looking at better ways to understand breast tumors in order to determine the best treatment."),
		EDUCATIONAL("EDUCATIONAL", "", ""), 	                								// type 7
		GENETICS("GENETICS", "", 																// type 8
				"Studies that evaluate how family history and genetic mutations (such as BRCA1/2 and KRAS) affect breast cancer risk and response to therapy."),
		SERVICES("SERVICES", "", ""), 			                								// type 9
		METHODS("METHODS", "", ""), 				                							// type 10
		EPIDEMIOLOGY("EPIDEMIOLOGY", "", 		                								// type 11
				"These studies collect information to look for connections between breast cancer and patient background, lifestyle, or medical history."), 
		PSYCHOSOCIAL("PSYCHOSOCIAL", "", ""), 		                							// type 12
		SCREENING("SCREENING", "",  			                								// type 13
				"Looking for more sensitive ways to find new cancers by mammogram, ultrasound, MRI, and other imaging methods."),
		SUPPORTIVE("SUPPORTIVE", "",  			                								// type 14
				"Looking at ways to improve emotional support for breast cancer patients and their families through education and counseling."),
		TISSUE("TISSUE", "", ""), 					                							// type 15
		TREATMENT_SURGERY("TREATMENT_SURGERY", "", ""), 				                		// type 16
		TREATMENT_CHEMOTHERAPY("TREATMENT_CHEMOTHERAPY", 									// type 17
				"Chemotherapy for breast cancer is used to kill cancer cells in the breast or in other parts of the body. " +
				"Neoadjuvant chemotherapy for breast cancer is given prior to surgery. Adjuvant chemotherapy is given after surgery.", ""),
		TREATMENT_RADIATIONONCOLOGY("TREATMENT_RADIATIONONCOLOGY",                  		// type 18
				"Radiation therapy for breast cancer is used to kill any cancer cells that may not have been removed during surgery. " +
				"It is also used to kill metastatic cells. Clinical trials are investigating different ways to deliver breast cancer radiation therapy.", ""),
		TREATMENT_HORMONE("TREATMENT_HORMONE",  					                		// type 19
				"Hormone therapy for breast cancer is used to block the hormones that fuel tumors that are estrogen receptor-positive (ER+) and/or progesterone receptor-positive (PR+).", ""),
		TREATMENT_BIOLOGICAL("TREATMENT_BIOLOGICAL", 	                					// type 20
				"Targeted therapies work by attacking specific molecules in cancer cells. Targeted therapy is being studied in early stage and advanced breast cancer.", ""), 
		TREATMENT_BISPHOSPHONATE("TREATMENT_BISPHOSPHONATE", "", ""),                 			// type 21
		TREATMENT_VACCINE("TREATMENT_VACCINE", "", ""), 				                		// type 22
		TREATMENT_OTHER("TREATMENT_OTHER", "", ""),						                		// type 23
		ALTERNATIVE("ALTERNATIVE", 															// type 24
				"Complementary and Integrative Medicine may have a role in breast cancer care after a breast cancer diagnosis.  " +
				"Clinical trials are investigating which CIM therapies are effective during treatment or can improve quality of life for breast cancer survivors.", 
				"Studies that evaluate activities such as massage and acupuncture, or diet supplements and vitamins that may accompany standard treatment."), 								                	
		NOVISITSREQUIRED("NOVISITSREQUIRED", "", ""),						                	// type 25
		TREATMENT_NEOADJUVANT("TREATMENT_NEOADJUVANT",  									// type 26
				"Neoadjuvant therapy for breast cancer is given before surgery. It can include targeted therapy, hormone therapy and/or chemotherapy for breast cancer.", ""),					               
		TREATMENT_BIOLOGICAL_ADVANCED_DISEASE("TREATMENT_BIOLOGICAL_ADVANCED_DISEASE", 		// type 27
				"Targeted therapies attack specific molecules in cancer cells. For example, researchers are developing targeted therapies " +
				"for both HER2-positive and triple negative breast cancer (ER-negative, PR-negative, and HER2-negative).", ""), 
		TREATMENT_BIOLOGICAL_EARLY_STAGE("TREATMENT_BIOLOGICAL_EARLY_STAGE",           		// type 28
				"A targeted therapy for breast cancer attacks specific molecules in cancer cells. Researchers are developing targeted therapies for early stage breast cancer to reduce the risk of a cancer recurrence.", ""), 
		BRCA1_2("BRCA1_2", "", ""),																// type 29
		METASTATIC("METASTATIC", "", ""),														// type 30
		HIGH_RISK("HIGH_RISK", "", ""),															// type 31
		ANTI_HER2_THERAPY("ANTI_HER2_THERAPY", "", ""),											// type 32
		LYMPHEDEMA("LYMPHEDEMA", "", 															// type 33
				"Studies designed to prevent or treat swelling of the arm caused by breast cancer surgery."),
		SURGERY_RECONSTRUCTION("SURGERY_RECONSTRUCTION", "", ""),								// type 34
		FERTILITY_PRESERVATION("FERTILITY_PRESERVATION", "", 									// type 35
				"Studies focused on ways to prevent loss of fertility associated with breast cancer treatment and ways to help children cope with a parent’s breast cancer."),
		YOUNG_SURVIVORS("YOUNG_SURVIVORS", "", ""),												// type 36
		PARP("PARP", "", ""),																	// type 37
		ACTIVITIES("ACTIVITIES", "", 															// type 38
				"Studies designed to evaluate activities such as exercise, yoga, diet, and counseling for their ability to prevent a recurrence (return of cancer after treatment) or improve patient quality of life."),
			
		MANAGING_SIDE_EFFECTS("MANAGING_SIDE_EFFECTS", "", 										// type 39
				"Looking at ways to reduce unpleasant side effects arising from treatment such as pain, cognitive issues, sleep disturbances, depression, vaginal dryness and tingling in the hands and feet (neuropathy)."),

		PREVENTING_RECURRENCE("PREVENTING_RECURRENCE", "", 										// type 40
				"Looking at ways to stop breast cancer from coming back in people who have already been treated."),
		CDK4_6_INHIBITORS("CDK4_6_INHIBITORS", "", ""),                                       // type 41
		LOBULAR_BREAST_CANCER("LOBULAR_BREAST_CANCER", "", ""),                                 // type 42
		OTHER_TARGETED_THERAPY("OTHER_TARGETED_THERAPY", "", ""),                               // type 43				
		RESPONSE_TO_TREATMENT("RESPONSE_TO_TREATMENT", "", ""),									// type 44
		TARGETED_THERAPY_MUTATIONS("TARGETED_THERAPY_MUTATIONS", "", ""),						// type 45
		BRAIN_METS("BRAIN_METS", "", ""),									// type 46
		LEPTOMENINGEAL_DISEASE("LEPTOMENINGEAL_DISEASE", "", ""),						// type 47
		TARGETED_THERAPY_ADC("TARGETED_THERAPY_ADC", "", ""),						// type 48
		BREAST_CANCER_SURVIVOR("BREAST_CANCER_SURVIVOR", "", "")						// type 49
		;
		


		private String webCategory;
		private String webDescription;
		private String altText;
		
		private Type( String webCategory, String webDescription, String altText)
		{
			this.webCategory = webCategory;
			this.webDescription = webDescription;
			this.setAltText(altText);
		}
		
		public String getWebCategory()
		{
			return webCategory;
		}
		
		public String getWebDescription()
		{
			return webDescription;
		}

		public String getAltText() 
		{
			return altText;
		}

		public void setAltText(String altText) 
		{
			this.altText = altText;
		}
	}

	/**
	 * java.lang.enum has pretty good lookup by type name.  The purpose of this map is to allow
	 * reverse lookup by webcategory.
	 */
	private static final HashMap<String, Type> typeWebCategories = new HashMap<String, Type>();
	static
	{
		for ( Type type: Type.values() )
		{
			typeWebCategories.put( type.getWebCategory(), type );
		}
	}
	
	public static HashMap<String, Type> getTypeWebCategories()
	{
		return typeWebCategories;
	}
	
	/**
	 * Secondary Category.  The string parameter is how the secondary category is passed
     * in by the urlrewriter.xml.  We may have to add this to the primary category as well.
	 */
	public enum SecondaryType
	{
		OTHER("OTHER"), ALTERNATIVE("CAM"), NOVISITSREQUIRED("NVR");
		
		private String webCategory;
		
		private SecondaryType( String webCategory )
		{
			this.webCategory = webCategory;
		}
		
		public String getWebCategory()
		{
			return webCategory;
		}
	}

	String someThing = SecondaryType.ALTERNATIVE.webCategory;

	/**
	 * The order in which trial types are sorted alphabetically in English.
	 */
	private static final Type[] TRIALTYPE_ORDER_ENGLISH = new Type[] { 
		Type.BEHAVIORAL,
		Type.TREATMENT_CHEMOTHERAPY,
			Type.TREATMENT_SURGERY, 
			Type.SURGERY_RECONSTRUCTION,
			Type.TREATMENT_RADIATIONONCOLOGY, 
			Type.TREATMENT_HORMONE,
			Type.TREATMENT_BIOLOGICAL, 
			Type.TREATMENT_BISPHOSPHONATE, 
			Type.TREATMENT_VACCINE, 
			Type.TREATMENT_OTHER,
			Type.TREATMENT, 
			Type.PREVENTION, 
			Type.DIAGNOSTIC, 
			Type.SUPPORTIVE, 
			Type.PSYCHOSOCIAL, 
			Type.BEHAVIORAL,
			Type.GENETICS, 
			Type.EDUCATIONAL, 
			Type.EPIDEMIOLOGY, 
			Type.SCREENING, 
			Type.DIAGNOSIS, 
			Type.LABORATORY,
			Type.METHODS, 
			Type.SERVICES, 
			Type.TISSUE,
			Type.ACTIVITIES};
	
	/**
	 * The order in which trial types should sort on the browse_trials page.
	 */
	public static final Type[] TRIALTYPE_ORDER_BROWSE_TRIALS = new Type[] {
		Type.TREATMENT_BIOLOGICAL,
		//Type.TREATMENT_BIOLOGICAL_ADVANCED_DISEASE,
		//Type.TREATMENT_BIOLOGICAL_EARLY_STAGE,
		Type.TREATMENT_BISPHOSPHONATE,
		Type.TREATMENT_CHEMOTHERAPY,
		Type.TREATMENT_HORMONE,
		Type.TREATMENT_NEOADJUVANT,
		Type.TREATMENT_RADIATIONONCOLOGY,
		Type.TREATMENT_SURGERY,
		Type.SURGERY_RECONSTRUCTION,
		Type.TREATMENT_VACCINE,
		Type.TREATMENT_OTHER,
		
		Type.ACTIVITIES,
		Type.BEHAVIORAL,
		Type.ALTERNATIVE,
		Type.LABORATORY,
		Type.DIAGNOSIS,
		Type.DIAGNOSTIC,
		Type.GENETICS,
		Type.FERTILITY_PRESERVATION,
		Type.LYMPHEDEMA,
		Type.MANAGING_SIDE_EFFECTS,
		
		// BCT-858: Added No Travel Required category to the Quick View
		Type.NOVISITSREQUIRED,
		
		Type.METHODS,
		Type.TREATMENT,
		Type.RESPONSE_TO_TREATMENT,
		Type.PREVENTION,
		Type.PREVENTING_RECURRENCE,
		Type.PSYCHOSOCIAL,
		Type.SCREENING,
		Type.SERVICES,
		Type.SUPPORTIVE,
		Type.EPIDEMIOLOGY,
		Type.TISSUE
		};

	public static final Type[] TUMOR_TYPE_TRIALS = new Type[] {
		Type.BRCA1_2,
		Type.LOBULAR_BREAST_CANCER
	};
	
	public static final Type[] TREATMENT_TRIALS = new Type[] {
		Type.TREATMENT_BISPHOSPHONATE,
		Type.TREATMENT_CHEMOTHERAPY,
		Type.TREATMENT_HORMONE,
		Type.TREATMENT_NEOADJUVANT,
		Type.TREATMENT_RADIATIONONCOLOGY,
		Type.TREATMENT_SURGERY,
		Type.SURGERY_RECONSTRUCTION,
		Type.TREATMENT_BIOLOGICAL,
		Type.ANTI_HER2_THERAPY,
		Type.CDK4_6_INHIBITORS,
		Type.PARP,
		Type.OTHER_TARGETED_THERAPY,
		Type.TREATMENT_VACCINE,
		Type.TREATMENT_OTHER,
		Type.TARGETED_THERAPY_ADC,
		Type.BREAST_CANCER_SURVIVOR
		//Type.MUTATION_BASED
		};

	public static final Type[] NON_TREATMENT_TRIALS = new Type[] {
		Type.ALTERNATIVE,
		Type.LABORATORY,
		Type.DIAGNOSTIC,
		Type.GENETICS,
		Type.FERTILITY_PRESERVATION,
		Type.LYMPHEDEMA,
		Type.PREVENTION,
		Type.SCREENING,
		Type.SUPPORTIVE,
		Type.EPIDEMIOLOGY,
		Type.RESPONSE_TO_TREATMENT
	};
	
	public static final ArrayList<Type> BROWSE_PAGE_TUMOR_TRIALS = new ArrayList<Type>();
	public static final ArrayList<Type> BROWSE_PAGE_TREATMENT_TRIALS = new ArrayList<Type>();
	public static final ArrayList<Type> BROWSE_PAGE_OTHER_TRIALS = new ArrayList<Type>();
	static
	{
		for ( Type type: TUMOR_TYPE_TRIALS )
		{
			BROWSE_PAGE_TUMOR_TRIALS.add( type );
		}
		for ( Type type: TREATMENT_TRIALS )
		{
			BROWSE_PAGE_TREATMENT_TRIALS.add( type );
		}
		for ( Type type: TRIALTYPE_ORDER_BROWSE_TRIALS )
		{
			if ( !BROWSE_PAGE_TREATMENT_TRIALS.contains( type ) && !BROWSE_PAGE_TUMOR_TRIALS.contains( type ))
			{
				BROWSE_PAGE_OTHER_TRIALS.add( type );
			}
		}
	}
	
	/**
	 * Compare trial types alphabetically in English.
	 */
	public static final Comparator<Type> TRIALTYPE_COMPARATOR_ENGLISH = new Comparator<Type>()
	{
		/**
		 * Compares the types using the english order array.
		 * 
		 * @param type1
		 *            the first type
		 * @param type2
		 *            the second type
		 * @return 0 if both parameters are the same or if both are not found in
		 *         an English-order list; a negative integer if
		 *         <code>type2</code> is <code>null</code> or if
		 *         <code>type1</code> is found in an English-order list before
		 *         <code>type2</code>; a positive integer otherwise
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 * @see #TRIALTYPE_ORDER_ENGLISH
		 */
		public int compare(Type type1, Type type2)
		{
			// Compare for equality or double-null first
			if (type1 == type2)
				return 0;
			// One parameter must be non-null; an unknown type is greater than a
			// known one
			if (type1 == null)
				return 1;
			if (type2 == null)
				return -1;
			// The parameter found first in the English-ordered list wins
			for (Type currentType : TRIALTYPE_ORDER_ENGLISH)
				if (type1 == currentType)
					return -1;
				else if (type2 == currentType)
					return 1;
			// If neither are in list then they are considered equal
			return 0;
		}
	};

	/*
	 * The trial registrars
	 */
	public enum Registry
	{
		// 2016-06-06: Added CTGOV
		UNASSIGNED, PDQ, NLM, CTGOV
	}

	/**
	 * Unique identifier, not maintained by the persistence engine.
	 */
	private Long id;

	/**
	 * The primary identifier
	 */
	private String primaryID;

	/**
	 * The NCT ID from CancerTrials.org
	 */
	private String clinicalTrialID;

	/**
	 * The type, guaranteed to be non-<code>null</code>
	 */
	private Type type = Type.OTHER;

	/**
	 * The secondary category
	 */
	private SecondaryType secondaryType = SecondaryType.OTHER;

	/**
	 * The phase, guaranteed to be non-<code>null</code>
	 */
	private Phase phase = Phase.NA;

	/**
	 * Where a protocol was originally registered, guaranteed to be non-
	 * <code>null</code>
	 */
	private Registry registry = Registry.UNASSIGNED;

	/**
	 * The sponsor
	 */
	private String sponsor;

	/**
	 * Whether or not the trial is accruing patients
	 */
	private boolean open;
	/**
	 * Whether or not the trial is listed on the system
	 */
	private boolean listed;
	
	private boolean earlystage;

	/**
	 * The short name
	 */
	private String name;

	/**
	 * The title
	 */
	private String title;

	/**
	 * The purpose
	 */
	private String purpose;

	/**
	 * The burden
	 */
	private String burden;

	/**
	 * The treatment plan
	 */
	private String treatmentPlan;

	/**
	 * The procedures
	 */
	private String procedures;

	/**
	 * The duration
	 */
	private String duration;

	/**
	 * The follow-up
	 */
	private String followup;

	private Set<Trial_Category> trial_Categories;
	
	private Set<Trial_Mutation> trial_Mutations;
	
	/**
	 * BCT-668:
	 * Field for storing the old primary identifier (referred to as the PDQ Identifier)
	 */
	private String pdqID;
	
	/**
	 * New fields added as part of new summary design
	 */
	
	private String trialpurpose;
	
	private String studymaterial;
	
	private String audience;
	
	/**
	 * Contains a label and a URL.
	 */
	public static class Link implements Serializable
	{
		/**
		 * The label
		 */
		private String label;
		/**
		 * The uniform resource locator
		 */
		private String url;

		/**
		 * Default constructor
		 */
		public Link()
		{
		}

		/**
		 * Store the parameters into instance variables
		 * 
		 * @param label
		 *            the label
		 * @param url
		 *            the uniform resource locator
		 */
		public Link(String label, String url)
		{
			this.label = label;
			this.url = url;
		}

		/**
		 * @param label
		 *            the label
		 */
		public void setLabel(String label)
		{
			this.label = label;
		}

		/**
		 * @return the label
		 */
		public String getLabel()
		{
			return label;
		}

		/**
		 * @param url
		 *            the uniform resource locator
		 */
		public void setUrl(String url)
		{
			this.url = url;
		}

		/**
		 * @return the uniform resource locator
		 */
		public String getUrl()
		{
			return url;
		}

		/**
		 * Version UID for serialization
		 */
		private static final long serialVersionUID = 2776731590978668938L;
	}

	/**
	 * The links, guaranteed to be non-<code>null</code>
	 */
	private List<Link> link = new LinkedList<Link>();

	/**
	 * The research sites, guaranteed to be non-<code>null</code>.
	 */
	private Set<TrialSite> trialSites = new LinkedHashSet<TrialSite>();
	
	
	/**
	 * this field is not mapped to a database field in the trial table.
	 */
	private List<TrialSite> byStateTrialSites = null;

	/**
	 * Eligibility Criteria, guaranteed to be non-<code>null</code>
	 */
	private EligibilityCriteria eligibilityCriteria = new EligibilityCriteria();

	/**
	 * The last date/time this protocol's eligibility criteria was modified or
	 * <code>null</code> if never modified or the eligibility criteria is null
	 */
	private Date eligibilityCriteriaLastModified;

	/**
	 * The last date/time this protocol was batch-matched to patients' histories
	 * or <code>null</code> if never batch-matched
	 */
	private Date lastBatchMatched;

	/**
	 * The last date/time this protocol was modified
	 */
	private Date lastModified;

	/**
	 * The last date this protocol was modified in the registry
	 */
	private Date lastRegistered;
	
	/**
	 * The date this trial was posted to production from the trialCODE swing application.
	 */
	private Date postDate;

	/**
	 * @return the id
	 */
	@Id
	public Long getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * Returns the primaryID.
	 * 
	 * @return the primaryID
	 */
	public String getPrimaryID()
	{
		return primaryID;
	}

	/**
	 * Sets the primaryID.
	 * 
	 * @param primaryID
	 *            the primaryID to set
	 */
	public void setPrimaryID(String primaryID)
	{
		this.primaryID = primaryID;
	}

	/**
	 * @return the clinicalTrialID
	 */
	public String getClinicalTrialID()
	{
		return clinicalTrialID; // == null? "Currently NULL": clinicalTrialID;
	}

	/**
	 * @param clinicalTrialID
	 *            the clinicalTrialID to set
	 */
	public void setClinicalTrialID(String clinicalTrialID)
	{
		this.clinicalTrialID = clinicalTrialID;
	}

	/**
	 * The secondaryType field is no longer being used but will be included for backwards compatibility.
	 * @param secondaryType
	 *            the secondaryType to set
	 */
	public void setSecondaryType(SecondaryType secondaryType)
	{
		this.secondaryType = secondaryType;
	}

	/**
	 * @return the secondaryType
	 */
	@Enumerated
	public SecondaryType getSecondaryType()
	{
		return secondaryType;
	}

	/**
	 * @return the type
	 */
	@Enumerated
	public Type getType()
	{
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setType(Type type) throws IllegalArgumentException
	{
		if (type == null)
			throw new IllegalArgumentException("must specify parameter");
		this.type = type;
	}

	/**
	 * @return the phase
	 */
	@Enumerated
	public Phase getPhase()
	{
		return phase;
	}

	/**
	 * @param phase
	 *            the phase to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setPhase(Phase phase) throws IllegalArgumentException
	{
		if (phase == null)
			throw new IllegalArgumentException("must specify parameter");
		this.phase = phase;
	}

	/**
	 * @return where a protocol was originally registered
	 */
	@Enumerated
	public Registry getRegistry()
	{
		return registry;
	}

	/**
	 * @param registry
	 *            where a protocol was originally registered
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setRegistry(Registry registry) throws IllegalArgumentException
	{
		if (registry == null)
			throw new IllegalArgumentException("must specify parameter");
		this.registry = registry;
	}

	/**
	 * @return the sponsor
	 */
	public String getSponsor()
	{
		return sponsor;
	}

	/**
	 * @param sponsor
	 *            the sponsor
	 */
	public void setSponsor(String sponsor)
	{
		this.sponsor = sponsor;
	}

	/**
	 * @return the earlystage
	 */
	public boolean isEarlystage() {
		return earlystage;
	}

	/**
	 * @param earlystage the earlystage to set
	 */
	public void setEarlystage(boolean earlystage) {
		this.earlystage = earlystage;
	}

	/**
	 * @return whether or not the trial is accruing patients
	 */
	public boolean isOpen()
	{
		return open;
	}

	/**
	 * @param open
	 *            whether or not the trial is accruing patients
	 */
	public void setOpen(boolean open)
	{
		this.open = open;
	}

	/**
	 * @return whether or not the trial is listed on the system
	 */
	public boolean isListed()
	{
		return listed;
	}

	/**
	 * @param listed
	 *            whether or not the trial is listed on the system
	 */
	public void setListed(boolean listed)
	{
		this.listed = listed;
	}

	/**
	 * Returns the short name
	 * 
	 * @return the short name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            the short name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 * @return the purpose
	 */
	public String getPurpose()
	{
		return purpose;
	}

	/**
	 * @param purpose
	 *            the purpose to set
	 */
	public void setPurpose(String purpose)
	{
		this.purpose = purpose;
	}

	/**
	 * @return the burden
	 */
	public String getBurden()
	{
		return burden;
	}

	/**
	 * @param burden
	 *            the burden to set
	 */
	public void setBurden(String burden)
	{
		this.burden = burden;
	}

	/**
	 * @return the treatment plan
	 */
	public String getTreatmentPlan()
	{
		return treatmentPlan;
	}

	/**
	 * @param treatmentPlan
	 *            the treatment plan to set
	 */
	public void setTreatmentPlan(String treatmentPlan)
	{
		this.treatmentPlan = treatmentPlan;
	}

	/**
	 * Returns the procedures.
	 * 
	 * @return the procedures
	 */
	public String getProcedures()
	{
		return procedures;
	}

	/**
	 * Sets the procedures.
	 * 
	 * @param procedures
	 *            the procedures to set
	 */
	public void setProcedures(String procedures)
	{
		this.procedures = procedures;
	}

	/**
	 * @return the duration
	 */
	public String getDuration()
	{
		return duration;
	}

	/**
	 * @param duration
	 *            the duration to set
	 */
	public void setDuration(String duration)
	{
		this.duration = duration;
	}

	/**
	 * Returns the followup.
	 * 
	 * @return the followup
	 */
	public String getFollowup()
	{
		return followup;
	}

	/**
	 * Sets the followup.
	 * 
	 * @param followup
	 *            the followup to set
	 */
	public void setFollowup(String followup)
	{
		this.followup = followup;
	}

	/**
	 * Returns the links. This method is declared transient so the persistence
	 * engine does not save it; rather, the persistence engine will access the
	 * data via <code>getLinkMarshaled</code> and <code>setLinkMarshaled</code>.
	 * 
	 * @return the link
	 * @see #getLinkMarshaled()
	 */
	@Transient
	public List<Link> getLink()
	{
		return link;
	}

	/**
	 * Marshals the links. This method is used by the persistence engine to
	 * persist the <code>link</code> field.
	 * 
	 * @return the marshaled link(s)
	 */
	public byte[] getLinkMarshaled()
	{
		if (link == null)
			return null;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(256);
		XMLEncoder encoder = new XMLEncoder(outputStream);
		encoder.writeObject(link);
		encoder.close();
		return outputStream.toByteArray();
	}

	/**
	 * Unmarshals the links. This method is used by the persistence engine to
	 * persist the <code>link</code> field.
	 * 
	 * @param linkMarshaled
	 *            the marshaled link(s)
	 * @throws DecodingException
	 *             if the bytes cannot be unmarshaled to links
	 */
	@SuppressWarnings( { "unchecked", "cast" })
	public void setLinkMarshaled(byte[] linkMarshaled) throws DecodingException
	{
		// No bytes means an empty object
		if ((linkMarshaled == null) || (linkMarshaled.length == 0))
		{
			link = new LinkedList<Link>();
			return;
		}
		Object linkObject = DecodingExceptionListener.decode(new ByteArrayInputStream(linkMarshaled));
		if ((linkObject == null) || !(linkObject instanceof List))
			throw new DecodingException(1, new RuntimeException("Links for " + id + " with size "
					+ linkMarshaled.length + " cannot be loaded from " + linkObject));
		link = (List<Link>) (linkObject);
	}

	/**
	 * @return the sites where this trial is delivered, guaranteed to be non-
	 *         <code>null</code>
	 */
	@OneToMany(mappedBy = "trial")
	public Set<TrialSite> getTrialSite()
	{
		return trialSites;
	}

	/**
	 * Sets the sites where this trial is delivered. If <code>null</code> then
	 * the field is set to an empty <code>LinkedHashSet</code>.
	 * 
	 * @param trialSite
	 *            the sites where this trial is delivered or <code>null</code>
	 *            for an empty list.
	 */
	public void setTrialSite(Set<TrialSite> trialSites)
	{
		this.trialSites = (trialSites != null) ? trialSites : new LinkedHashSet<TrialSite>();
	}

	/**
	 * Returns the eligibility criteria. This method is declared transient so
	 * the persistence engine does not save it; rather, the persistence engine
	 * will access the data via <code>getEligibilityCriteriaMarshaled</code> and
	 * <code>setEligibilityCriteriaMarshaled</code>.
	 * 
	 * @return the eligibilityCriteria
	 * @see #getEligibilityCriteriaMarshaled()
	 */
	@Transient
	public EligibilityCriteria getEligibilityCriteria()
	{
		return eligibilityCriteria;
	}

	/**
	 * Marshals the eligibility criteria. This method is used by the persistence
	 * engine to persist the <code>eligibilityCriteria</code> field.
	 * 
	 * @return the marshaled eligibility criteria/on
	 */
	public byte[] getEligibilityCriteriaMarshaled()
	{
		if (eligibilityCriteria == null)
			return null;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(256);
		XMLEncoder encoder = new XMLEncoder(outputStream);
		encoder.writeObject(eligibilityCriteria);
		encoder.close();
		return outputStream.toByteArray();
	}

	/**
	 * Unmarshals the eligibility criteria. This method is used by the
	 * persistence engine to persist the <code>eligibilityCriteria</code> field.
	 * 
	 * @param eligibilityCriteriaMarshaled
	 *            the marshaled eligibility criteria
	 * @throws DecodingException
	 *             if the bytes cannot be unmarshaled to eligibility criteria
	 */
	public void setEligibilityCriteriaMarshaled(byte[] eligibilityCriteriaMarshaled) throws DecodingException
	{
		// No bytes means an empty object
		if ((eligibilityCriteriaMarshaled == null) || (eligibilityCriteriaMarshaled.length == 0))
		{
			eligibilityCriteria = new EligibilityCriteria();
			return;
		}
		Object eligibilityCriteriaObject = DecodingExceptionListener.decode(new ByteArrayInputStream(
				eligibilityCriteriaMarshaled));
		if ((eligibilityCriteriaObject == null) || !(eligibilityCriteriaObject instanceof EligibilityCriteria))
			throw new DecodingException(1, new RuntimeException("Trial eligibility for " + id + " with size "
					+ eligibilityCriteriaMarshaled.length + " cannot be loaded from " + eligibilityCriteriaObject));
		eligibilityCriteria = (EligibilityCriteria) (eligibilityCriteriaObject);
	}

	/**
	 * @return the last date/time this protocol's eligibility criteria was
	 *         modified or <code>null</code> if never modified or eligibility
	 *         criteria is null
	 */
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEligibilityCriteriaLastModified()
	{
		return eligibilityCriteriaLastModified;
	}

	/**
	 * @param newEligibilityCriteriaLastModified
	 *            the last date/time this protocol's eligibility criteria was
	 *            modified or <code>null</code> or eligibility criteria is null
	 */
	public void setEligibilityCriteriaLastModified(Date newEligibilityCriteriaLastModified)
	{
		this.eligibilityCriteriaLastModified = newEligibilityCriteriaLastModified;
	}

	/**
	 * @return the last date/time this protocol was batch-matched to patients'
	 *         histories or <code>null</code> if never batch-matched
	 */
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastBatchMatched()
	{
		return lastBatchMatched;
	}

	/**
	 * @param lastBatchMatched
	 *            the last date/time this protocol was batch-matched to
	 *            patients' histories or <code>null</code> if never
	 *            batch-matched
	 */
	public void setLastBatchMatched(Date lastBatchMatched)
	{
		this.lastBatchMatched = lastBatchMatched;
	}

	/**
	 * @return last modified date/time
	 */
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastModified()
	{
		return lastModified;
	}

	/**
	 * Sets the last modified date/time
	 * 
	 * @param lastModified
	 *            the last modified date/time to set
	 */
	public void setLastModified(Date lastModified)
	{
		this.lastModified = lastModified;
	}

	/**
	 * @return last date this protocol was modified in the registry
	 */
	@Temporal(TemporalType.DATE)
	public Date getLastRegistered()
	{
		return lastRegistered;
	}

	/**
	 * Sets the last date this protocol was modified in the registry
	 * 
	 * @param lastRegistered
	 *            the last date this protocol was modified in the registry
	 */
	public void setLastRegistered(Date lastRegistered)
	{
		this.lastRegistered = lastRegistered;
	}

	/**
	 * @param postDate the postDate to set
	 */
	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}

	/**
	 * @return the postDate
	 */
	public Date getPostDate() {
		return postDate;
	}

	/**
	 * @param pdqID the pdqID to set
	 */
	public void setPdqID(String pdqID) {
		this.pdqID = pdqID;
	}

	/**
	 * @return the pdqID
	 */
	public String getPdqID() {
		return pdqID;
	}

	
	/**
	 * @return the trialpurpose
	 */
	public String getTrialpurpose() {
		return trialpurpose;
	}

	/**
	 * @param trialpurpose the trialpurpose to set
	 */
	public void setTrialpurpose(String trialpurpose) {
		this.trialpurpose = trialpurpose;
	}

	/**
	 * @return the studymaterial
	 */
	public String getStudymaterial() {
		return studymaterial;
	}

	/**
	 * @param studymaterial the studymaterial to set
	 */
	public void setStudymaterial(String studymaterial) {
		this.studymaterial = studymaterial;
	}

	/**
	 * @return the audience
	 */
	public String getAudience() {
		return audience;
	}

	/**
	 * @param audience the audience to set
	 */
	public void setAudience(String audience) {
		this.audience = audience;
	}

	/**
	 * Returns whether this trial's identifiers are equal to anothers'. This
	 * method first tests strict equality, then the identifiers, then the
	 * primary identifiers.
	 * 
	 * @return whether this trial's identifiers are equal to anothers'
	 * @param other
	 *            the other trial
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other)
	{
		if (this == other)
			return true;
		if (!(other instanceof Trial))
			return false;
		// Compare unique ids if either are available
		Trial otherTrial = (Trial) (other);
		if ((id != null) && (id.longValue() != 0))
			return id.equals(otherTrial.id);
		if ((otherTrial.id != null) && (otherTrial.id.longValue() != 0))
			return false;
		// Compare primary ids if available
		return (primaryID == null) ? (otherTrial.primaryID == null) : primaryID.equals(otherTrial.primaryID);
	}

	/**
	 * Returns the hashcode of the identifier or primary identifier.
	 * 
	 * @return the hashcode of the identifier or primary identifier
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		if ((id != null) && (id.longValue() != 0))
			return id.hashCode();
		return (primaryID == null) ? super.hashCode() : primaryID.hashCode();
	}

	/**
	 * Returns the class name, memory location, identifier and primary
	 * identifier.
	 * 
	 * @return the class name, memory location, identifier and primary
	 *         identifier.
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return getClass().getName() + "@" + Integer.toHexString(super.hashCode()) + '{' + id + ':' + primaryID + '}';
	}

	
	/**
	 * A list of categories this trial belongs to.  This list includes the primary category.  Fetch is set to eager
	 * as we need this list as part of the Trial object.
	 * @return a list of categories
	 */
	@OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL) // follow the trial for deletes, etc.
	@JoinColumn(name="trialid")
	public Set<Trial_Category> getTrial_Categories() 
	{
		return this.trial_Categories;
	}
	
	/**
	 * The setter used by hibernate to set the trial category set.
	 * @param trial_Categories
	 */
	public void setTrial_Categories(Set<Trial_Category> trial_Categories) 
	{
		this.trial_Categories = trial_Categories;
	}
	
	/**
	 * Given the category determine if this trial belongs to it.
	 * The web Category string is used as this may be different from the enum name.
	 * @param category
	 * @return true if a match is found, false otherwise.
	 */
	public boolean isInCategory( String category )
	{
		if ( getTrial_Categories() == null  && getType() == null )
		{
			return false;
		}

		Type categoryType = getTypeWebCategories().get( category.toUpperCase() );
		
		if ( categoryType == null )
		{
			return false;
		}
		short categoryNdx = ( short ) categoryType.ordinal();
		
		if ( categoryNdx == getType().ordinal() )
		{			
			return true;
		}
		
		if ( this.getTrial_Categories()  == null )
		{
			return false;
		}
		
		for ( Trial_Category trial_Category : this.getTrial_Categories() )
		{
			if ( trial_Category.getTrial_CategoryPK().getCategory() == categoryNdx )
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * A list of mutations this trial belongs to.  Fetch is set to eager
	 * as we need this list as part of the Trial object.
	 * @return a list of mutations
	 */
	@OneToMany(fetch=FetchType.EAGER, cascade = CascadeType.ALL) // follow the trial for deletes, etc.
	@JoinColumn(name="trialid")
	public Set<Trial_Mutation> getTrial_Mutations() 
	{
		return this.trial_Mutations;
	}
	
	/**
	 * The setter used by hibernate to set the trial mutation set.
	 * @param trial_Mutations
	 */
	public void setTrial_Mutations(Set<Trial_Mutation> trial_Mutations) 
	{
		this.trial_Mutations = trial_Mutations;
	}
	
	/**
	 * Given the mutation determine if this trial belongs to it.
	 * @param mutation
	 * @return true if a match is found, false otherwise.
	 */
	public boolean isInMutation( String mutation )
	{
		if ( getTrial_Mutations() == null)
		{
			return false;
		}

		Mutation submittedMutation = Mutation.get( mutation.toUpperCase() );
		
		if ( submittedMutation == null )
		{
			return false;
		}
		short mutationNdx = ( short ) submittedMutation.ordinal();
		
		for ( Trial_Mutation trial_Mutation : this.getTrial_Mutations() )
		{
			if ( trial_Mutation.getTrial_MutationPK().getMutation() == mutationNdx )
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Convenience method for use on trials.xhtml, browse_trials.xhtml and my_trials.xhtml.
	 * These pages display a No Visits required if this is true.
	 * Also, these trial sort above all other trials to be displayed.
	 * @return true if this trial is marked as no visits required, fals otherwise.
	 */
	public boolean inNoVisitsRequired()
	{
		return isInCategory( Type.NOVISITSREQUIRED.getWebCategory() );
	}

	@Transient
	public List<TrialSite> getByStateTrialSites() {
		if (byStateTrialSites == null) {
			byStateTrialSites = new ArrayList<TrialSite>(trialSites);
			Collections.sort(byStateTrialSites, new Comparator<TrialSite>() {
				@Override
				public int compare(TrialSite ts1, TrialSite ts2) {
					if (ts1 == null || ts1.getSite() == null || ts1.getSite().getPoliticalSubUnitName() == null) {
						return 1;
					}

					if (ts2 == null || ts2.getSite() == null || ts2.getSite().getPoliticalSubUnitName() == null) {
						return -1;
					}

					// TODO Auto-generated method stub
					return ts1.getSite().getPoliticalSubUnitName().compareToIgnoreCase(ts2.getSite().getPoliticalSubUnitName());
				}
			});
		}
		return byStateTrialSites;
	}
}
