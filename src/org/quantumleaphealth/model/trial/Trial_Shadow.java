/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.trial;

import java.io.Serializable;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.quantumleaphealth.model.trial.Trial.Phase;
import org.quantumleaphealth.model.trial.Trial.Registry;
import org.quantumleaphealth.model.trial.Trial.SecondaryType;
import org.quantumleaphealth.model.trial.Trial.Type;
import org.quantumleaphealth.xml.DecodingException;

/**
 * Encapsulates a protocol's data. Because this class annotates the
 * <code>id</code> column on the <code>getId()</code> getter method, the access
 * type for persistence will be getter methods instead of fields. This
 * persistence access type allows for the transient field
 * <code>eligibilityCriteria</code> to be marshaled into XML format for
 * persisting.
 * 
 * @author Tom Bechtold
 * @version 2009-02-03
 */
@Entity
public class Trial_Shadow implements Serializable
{
/*
 CREATE TABLE trial_shadow
(
  id integer NOT NULL, -- Pointer to the trial record shadowed
  trialid integer NOT NULL,
  registry smallint NOT NULL, -- Where registered
  open boolean NOT NULL,
  primaryid character varying(22) NOT NULL, -- Primary identifier
  "type" smallint NOT NULL,
  phase smallint NOT NULL,
  sponsor character varying(50),
  "name" character varying(63),
  title character varying(255),
  purpose character varying(1100),
  treatmentplan text,
  procedures character varying(255),
  burden character varying(127),
  duration character varying(50),
  followup character varying(255),
  linkmarshaled bytea, -- XML-encoded link labels/urls
  eligibilitycriteriamarshaled bytea,
  eligibilitycriterialastmodified timestamp with time zone, -- Last timestamp when eligibility criteria was modified
  lastbatchmatched timestamp with time zone, -- Last timestamp when trial_shadow was batch-matched to patients' histories
  lastmodified timestamp with time zone, -- Last modification timestamp of entire record
  lastregistered date, -- Last modification date in registry
  listed boolean NOT NULL, -- Whether or not listed on the website
  clinicaltrialid character varying(22), -- Clinical Trials (NCT) identifier
  secondarytype smallint, -- Trial Secondary Category (Primary is type column)
  storedate timestamp with time zone, -- Date and Time record was copied from the trial table.
  versioncomment character varying(255),
  CONSTRAINT pk_trial_shadow_id PRIMARY KEY (id)
)	
 */
	/**
	 * Unique identifier, not maintained by the persistence engine.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Trial_ShadowID")
	@SequenceGenerator(name = "Trial_ShadowID", sequenceName = "trial_shadow_id_seq", allocationSize = 1)
	private Long id;

	/**
	 * pointer to the trial record this shadow is associated with
	 */
	private Long trialid;
	
	/**
	 * Where a protocol was originally registered, guaranteed to be non-
	 * <code>null</code>
	 */
	private Trial.Registry registry;

	/**
	 * Whether or not the trial is accruing patients
	 */
	private boolean open;

	/**
	 * The primary identifier
	 */
	private String primaryID;

	/**
	 * The NCT ID from CancerTrials.org
	 */
	private String clinicalTrialID;

	/**
	 * The type, guaranteed to be non-<code>null</code>
	 */
	private Trial.Type type;

	/**
	 * The secondary category
	 */
	private Trial.SecondaryType secondaryType;

	/**
	 * The phase, guaranteed to be non-<code>null</code>
	 */
	private Trial.Phase phase;

	/**
	 * The sponsor
	 */
	private String sponsor;

	/**
	 * Whether or not the trial is listed on the system
	 */
	private boolean listed;

	/**
	 * The short name
	 */
	private String name;

	/**
	 * The title
	 */
	private String title;

	/**
	 * The purpose
	 */
	private String purpose;

	/**
	 * The burden
	 */
	private String burden;

	/**
	 * The treatment plan
	 */
	private String treatmentPlan;

	/**
	 * The procedures
	 */
	private String procedures;

	/**
	 * The duration
	 */
	private String duration;

	/**
	 * The follow-up
	 */
	private String followup;

	public byte[] eligibilityCriteriaMarshaled;

	/**
	 * The last date/time this protocol's eligibility criteria was modified or
	 * <code>null</code> if never modified or the eligibility criteria is null
	 */
	private Date eligibilityCriteriaLastModified;

	/**
	 * The last date/time this protocol was batch-matched to patients' histories
	 * or <code>null</code> if never batch-matched
	 */
	private Date lastBatchMatched;

	/**
	 * The last date/time this protocol was modified
	 */
	private Date lastModified;

	/**
	 * The last date this protocol was modified in the registry
	 */
	private Date lastRegistered;

	private byte[] linkmarshaled;
	
	private String versioncomment;
	
	/**
	 * The purpose
	 */
	private String trialpurpose;


	/**
	 * The target
	 */
	private String audience;

	/**
	 * The treatment plan
	 */
	private String studymaterial;
	
	/**
	 * default constructor needed by Hibernate for fetches.
	 */
	public Trial_Shadow()
	{
	}
	
	public Trial_Shadow(Trial trial)
	{
		this.trialid = trial.getId();
		this.burden = trial.getBurden();
		this.clinicalTrialID = trial.getClinicalTrialID();
		this.duration = trial.getDuration();
		this.eligibilityCriteriaLastModified = trial.getEligibilityCriteriaLastModified();
		this.eligibilityCriteriaMarshaled = trial.getEligibilityCriteriaMarshaled();
		this.followup = trial.getFollowup();
		this.lastBatchMatched = trial.getLastBatchMatched();
		this.lastModified = trial.getLastModified();
		this.lastRegistered = trial.getLastRegistered();
		this.linkmarshaled = trial.getLinkMarshaled();
		this.listed = trial.isListed();
		this.name = trial.getName();
		this.open = trial.isOpen();
		this.phase = trial.getPhase();
		this.primaryID = trial.getPrimaryID();
		this.procedures = trial.getProcedures();
		this.purpose = trial.getPurpose();
		this.registry = trial.getRegistry();
		this.secondaryType = trial.getSecondaryType();
		this.sponsor = trial.getSponsor();
		this.title = trial.getTitle();
		this.treatmentPlan = trial.getTreatmentPlan();
		this.type = trial.getType();
		this.trialpurpose = trial.getTrialpurpose();
		this.audience = trial.getAudience();
		this.studymaterial= trial.getStudymaterial();
	}
	
	/**
	 * @return the id
	 */
	public Long getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * @param trialid the trialid to set
	 */
	public void setTrialid(Long trialid) {
		this.trialid = trialid;
	}

	/**
	 * @return the trialid
	 */
	public Long getTrialid() {
		return trialid;
	}

	/**
	 * Returns the primaryID.
	 * 
	 * @return the primaryID
	 */
	public String getPrimaryID()
	{
		return primaryID;
	}

	/**
	 * Sets the primaryID.
	 * 
	 * @param primaryID
	 *            the primaryID to set
	 */
	public void setPrimaryID(String primaryID)
	{
		this.primaryID = primaryID;
	}

	/**
	 * @return the clinicalTrialID
	 */
	public String getClinicalTrialID()
	{
		return clinicalTrialID; // == null? "Currently NULL": clinicalTrialID;
	}

	/**
	 * @param clinicalTrialID
	 *            the clinicalTrialID to set
	 */
	public void setClinicalTrialID(String clinicalTrialID)
	{
		this.clinicalTrialID = clinicalTrialID;
	}

	/**
	 * The secondaryType field is no longer being used but will be included for backwards compatibility.
	 * @param secondaryType
	 *            the secondaryType to set
	 */
	public void setSecondaryType(SecondaryType secondaryType)
	{
		this.secondaryType = secondaryType;
	}

	/**
	 * @return the secondaryType
	 */
	@Enumerated
	public Trial.SecondaryType getSecondaryType()
	{
		return secondaryType;
	}

	/**
	 * @return the type
	 */
	@Enumerated
	public Type getType()
	{
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(Type type) throws IllegalArgumentException
	{
		this.type = type;
	}

	/**
	 * @return the phase
	 */
	@Enumerated
	public Phase getPhase()
	{
		return phase;
	}

	/**
	 * @param phase
	 *            the phase to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setPhase(Phase phase) throws IllegalArgumentException
	{
		this.phase = phase;
	}

	/**
	 * @return where a protocol was originally registered
	 */
	@Enumerated
	public Registry getRegistry()
	{
		return registry;
	}

	/**
	 * @param registry
	 *            where a protocol was originally registered
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setRegistry(Registry registry) throws IllegalArgumentException
	{
		if (registry == null)
			throw new IllegalArgumentException("must specify parameter");
		this.registry = registry;
	}

	/**
	 * @return the sponsor
	 */
	public String getSponsor()
	{
		return sponsor;
	}

	/**
	 * @param sponsor
	 *            the sponsor
	 */
	public void setSponsor(String sponsor)
	{
		this.sponsor = sponsor;
	}

	/**
	 * @return whether or not the trial is accruing patients
	 */
	public boolean isOpen()
	{
		return open;
	}

	/**
	 * @param open
	 *            whether or not the trial is accruing patients
	 */
	public void setOpen(boolean open)
	{
		this.open = open;
	}

	/**
	 * @return whether or not the trial is listed on the system
	 */
	public boolean isListed()
	{
		return listed;
	}

	/**
	 * @param listed
	 *            whether or not the trial is listed on the system
	 */
	public void setListed(boolean listed)
	{
		this.listed = listed;
	}

	/**
	 * Returns the short name
	 * 
	 * @return the short name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            the short name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the title
	 */
	public String getTitle()
	{
		return title;
	}

	/**
	 * @param title
	 *            the title to set
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	/**
	 * @return the purpose
	 */
	public String getPurpose()
	{
		return purpose;
	}

	/**
	 * @param purpose
	 *            the purpose to set
	 */
	public void setPurpose(String purpose)
	{
		this.purpose = purpose;
	}

	/**
	 * @return the burden
	 */
	public String getBurden()
	{
		return burden;
	}

	/**
	 * @param burden
	 *            the burden to set
	 */
	public void setBurden(String burden)
	{
		this.burden = burden;
	}

	/**
	 * @return the treatment plan
	 */
	public String getTreatmentPlan()
	{
		return treatmentPlan;
	}

	/**
	 * @param treatmentPlan
	 *            the treatment plan to set
	 */
	public void setTreatmentPlan(String treatmentPlan)
	{
		this.treatmentPlan = treatmentPlan;
	}

	/**
	 * Returns the procedures.
	 * 
	 * @return the procedures
	 */
	public String getProcedures()
	{
		return procedures;
	}

	/**
	 * Sets the procedures.
	 * 
	 * @param procedures
	 *            the procedures to set
	 */
	public void setProcedures(String procedures)
	{
		this.procedures = procedures;
	}

	/**
	 * @return the duration
	 */
	public String getDuration()
	{
		return duration;
	}

	/**
	 * @param duration
	 *            the duration to set
	 */
	public void setDuration(String duration)
	{
		this.duration = duration;
	}

	/**
	 * Returns the followup.
	 * 
	 * @return the followup
	 */
	public String getFollowup()
	{
		return followup;
	}

	/**
	 * Sets the followup.
	 * 
	 * @param followup
	 *            the followup to set
	 */
	public void setFollowup(String followup)
	{
		this.followup = followup;
	}

	/**
	 * @return the marshaled link(s)
	 */
	public byte[] getLinkMarshaled()
	{
		return this.linkmarshaled;
	}

	/**
	 */
	public void setLinkMarshaled(byte[] linkmarshaled)
	{
		this.linkmarshaled = linkmarshaled;
	}

	/**
	 * @return the marshaled eligibility criteria/on
	 */
	public byte[] getEligibilityCriteriaMarshaled()
	{
		return eligibilityCriteriaMarshaled;
	}

	/**
	 */
	public void setEligibilityCriteriaMarshaled(byte[] eligibilityCriteriaMarshaled) throws DecodingException
	{
		this.eligibilityCriteriaMarshaled = eligibilityCriteriaMarshaled;
	}

	/**
	 * @return the last date/time this protocol's eligibility criteria was
	 *         modified or <code>null</code> if never modified or eligibility
	 *         criteria is null
	 */
	@Temporal(TemporalType.TIMESTAMP)
	public Date getEligibilityCriteriaLastModified()
	{
		return eligibilityCriteriaLastModified;
	}

	/**
	 * @param newEligibilityCriteriaLastModified
	 *            the last date/time this protocol's eligibility criteria was
	 *            modified or <code>null</code> or eligibility criteria is null
	 */
	public void setEligibilityCriteriaLastModified(Date newEligibilityCriteriaLastModified)
	{
		this.eligibilityCriteriaLastModified = newEligibilityCriteriaLastModified;
	}

	/**
	 * @return the last date/time this protocol was batch-matched to patients'
	 *         histories or <code>null</code> if never batch-matched
	 */
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastBatchMatched()
	{
		return lastBatchMatched;
	}

	/**
	 * @param lastBatchMatched
	 *            the last date/time this protocol was batch-matched to
	 *            patients' histories or <code>null</code> if never
	 *            batch-matched
	 */
	public void setLastBatchMatched(Date lastBatchMatched)
	{
		this.lastBatchMatched = lastBatchMatched;
	}

	/**
	 * @return last modified date/time
	 */
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastModified()
	{
		return lastModified;
	}

	/**
	 * Sets the last modified date/time
	 * 
	 * @param lastModified
	 *            the last modified date/time to set
	 */
	public void setLastModified(Date lastModified)
	{
		this.lastModified = lastModified;
	}

	/**
	 * @return last date this protocol was modified in the registry
	 */
	@Temporal(TemporalType.DATE)
	public Date getLastRegistered()
	{
		return lastRegistered;
	}

	/**
	 * Sets the last date this protocol was modified in the registry
	 * 
	 * @param lastRegistered
	 *            the last date this protocol was modified in the registry
	 */
	public void setLastRegistered(Date lastRegistered)
	{
		this.lastRegistered = lastRegistered;
	}

	/**
	 * @param versioncomment the versioncomment to set
	 */
	public void setVersioncomment(String versioncomment) {
		this.versioncomment = versioncomment;
	}

	/**
	 * @return the versioncomment
	 */
	public String getVersioncomment() {
		return versioncomment;
	}
	
	/**
	 * @return the trialpurpose
	 */
	public String getTrialpurpose() {
		return trialpurpose;
	}

	/**
	 * @param trialpurpose the trialpurpose to set
	 */
	public void setTrialpurpose(String trialpurpose) {
		this.trialpurpose = trialpurpose;
	}

	/**
	 * @return the audience
	 */
	public String getAudience() {
		return audience;
	}

	/**
	 * @param audience the audience to set
	 */
	public void setAudience(String audience) {
		this.audience = audience;
	}

	/**
	 * @return the studymaterial
	 */
	public String getStudymaterial() {
		return studymaterial;
	}

	/**
	 * @param studymaterial the studymaterial to set
	 */
	public void setStudymaterial(String studymaterial) {
		this.studymaterial = studymaterial;
	}
}
