package org.quantumleaphealth.model.trial;

public class PatientTrial extends Trial
{

	private static final long serialVersionUID = -5563363797863228791L;

	private Boolean notInterested;

	/**
	 * Indicator for inclusion in the patient's list of favorites.
	 */
	private Boolean favorite = true;

	public void setNotInterested(Boolean notInterested)
	{
		this.notInterested = notInterested;
	}

	public Boolean getNotInterested()
	{
		return notInterested;
	}

	/**
	 * @param favorite
	 *            the favorite to set
	 */
	public void setFavorite(Boolean favorite)
	{
		this.favorite = favorite;
	}

	/**
	 * @return the favorite
	 */
	public Boolean getFavorite()
	{
		return favorite;
	}
}
