package org.quantumleaphealth.model.navigator;

import javax.persistence.Entity;
import java.io.Serializable;
import java.util.Date;

import static javax.persistence.TemporalType.TIMESTAMP;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.Transient;

/**
 * Entity bean for the newsletter_subscriber table.
 * 
 * @author wgweis
 */
@Entity 
public class Navigator implements Serializable 
{
	/**
	 * Unique identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NavigatorID")
	@SequenceGenerator(name = "NavigatorID", sequenceName = "navigator_id_seq", allocationSize = 1)
	private Long id;

	/**
	 * Email or <code>null</code> if none
	 */
	private String principal;
	
	/**
	 * Log in password
	 */
	private String credentials;

	/**
	 * Timestamp for the creation of this record.  This property is for
	 * archiving only and will be set at the database level.
	 */
	@Column(name = "created", updatable = false, insertable = false)
	@Temporal(TIMESTAMP)
	private Date created;
	
	/**
	 * database user id of creator.  This property is for
	 * archiving only and will be set at the database level.
	 */
	@Column(name = "created_by", updatable = false, insertable = false)
	private String created_by;
	
	/**
	 * Timestamp for the modification of this record.
	 */
	@Temporal(TIMESTAMP)
	private Date modified;

	/**
	 * database user id of modifier
	 */
	private String modified_by;
	
	private boolean enabled;
	
	/**
	 * The secret question if credentials are lost
	 */
	private String secretQuestion;
	/**
	 * The secret answer if credentials are lost
	 */
	private String secretAnswer;
	
	/**
	 * When accepted the terms/conditions or <code>null</code> if not accepted
	 * yet
	 */
	private Date termsAcceptedTime;
	
	/**
	 * First name, required for navigator identification purposes, when showing a client, etc.
	 */
	private String firstName;

	/**
	 * Last name, required for navigator identification purposes, when showing a client, etc.
	 */
	private String lastName;
	
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) 
	{
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId() 
	{
		return id;
	}

	/**
	 * @param principal the principal to set
	 */
	public void setPrincipal(String principal) 
	{
		this.principal = principal;
	}

	/**
	 * @return the principal
	 */
	public String getPrincipal() 
	{
		return principal;
	}

	/**
	 * @param credentials the credentials to set
	 */
	public void setCredentials(String credentials) 
	{
		this.credentials = credentials;
	}

	/**
	 * @return the credentials
	 */
	public String getCredentials() 
	{
		return credentials;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) 
	{
		this.created = created;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() 
	{
		return created;
	}

	/**
	 * @param created_by the created_by to set
	 */
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	/**
	 * @return the created_by
	 */
	public String getCreated_by() {
		return created_by;
	}

	/**
	 * @param modified the modified to set
	 */
	public void setModified(Date modified) 
	{
		this.modified = modified;
	}

	/**
	 * @return the modified
	 */
	public Date getModified() 
	{
		return modified;
	}

	/**
	 * @param modified_by the modified_by to set
	 */
	public void setModified_by(String modified_by) 
	{
		this.modified_by = modified_by;
	}

	/**
	 * @return the modified_by
	 */
	public String getModified_by() 
	{
		return modified_by;
	}

	/**
	 * @param enabled the enabled to set
	 */
	public void setEnabled(Boolean enabled) 
	{
		this.enabled = enabled;
	}

	/**
	 * @return the enabled
	 */
	public boolean isEnabled() 
	{
		return enabled;
	}

	/**
	 * @param secretQuestion the secretQuestion to set
	 */
	public void setSecretQuestion(String secretQuestion) 
	{
		this.secretQuestion = secretQuestion;
	}

	/**
	 * @return the secretQuestion
	 */
	public String getSecretQuestion() 
	{
		return secretQuestion;
	}

	/**
	 * @param secretAnswer the secretAnswer to set
	 */
	public void setSecretAnswer(String secretAnswer) 
	{
		this.secretAnswer = secretAnswer;
	}

	/**
	 * @return the secretAnswer
	 */
	public String getSecretAnswer() 
	{
		return secretAnswer;
	}

	/**
	 * Returns whether the terms were accepted. This method return
	 * <code>true</code> if the accepted date is non-<code>null</code>. This
	 * method is declared transient so the persistence engine does not save it;
	 * rather, the persistence engine will access the data via
	 * <code>getTermsAcceptedTime</code> and <code>setTermsAcceptedTime</code>.
	 * 
	 * @return whether the terms were accepted
	 * @see #termsAcceptedTime
	 */
	@Transient
	public boolean isTermsAccepted()
	{
		return termsAcceptedTime != null;
	}

	/**
	 * Sets the terms accepted date. This method sets the date to now if the
	 * parameter is <code>true</code> and the date has not been stored yet.
	 * 
	 * @param termsAcceptedNow
	 *            whether the terms are accepted
	 * @see #termsAcceptedTime
	 */
	public void setTermsAccepted(boolean termsAcceptedNow)
	{
		if (!termsAcceptedNow)
			this.termsAcceptedTime = null;
		else if (this.termsAcceptedTime == null)
			this.termsAcceptedTime = new Date();
	}

	/**
	 * @return when accepted the terms/conditions or <code>null</code> if not
	 *         accepted yet
	 */
	@Temporal(TIMESTAMP)
	public Date getTermsAcceptedTime()
	{
		return termsAcceptedTime;
	}

	/**
	 * @param termsAcceptedTime
	 *            when accepted the terms/conditions or <code>null</code> if not
	 *            accepted yet
	 */
	public void setTermsAcceptedTime(Date termsAcceptedTime)
	{
		this.termsAcceptedTime = termsAcceptedTime;
	}
	
	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) 
	{
		this.firstName = firstName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() 
	{
		return firstName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) 
	{
		this.lastName = lastName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() 
	{
		return lastName;
	}

	/**
	 * @return whether the identifier matches another's
	 * @param other
	 *            the other object
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other)
	{
		if (this == other)
		{
			return true;
		}
		if (!(other instanceof Navigator))
		{
			return false;
		}
		return this.getId().equals(((Navigator) other).getId());
	}
	
	/**
	 * Returns the identifier's hashcode
	 * 
	 * @see java.lang.Object#hashCode()
	 * @see #id
	 */
	@Override
	public int hashCode()
	{
		return (getId() == null) || (getId().longValue() == 0) ? super.hashCode() : getId().hashCode();
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2390886115542753862L;
}
