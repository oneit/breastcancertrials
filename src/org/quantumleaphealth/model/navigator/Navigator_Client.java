package org.quantumleaphealth.model.navigator;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

import org.quantumleaphealth.model.patient.UserPatient;

/**
 * Entity bean for the newsletter_subscriber table.
 * 
 * @author wgweis
 */
@Entity 
public class Navigator_Client implements Serializable
{
	/***
	 * The Navigator_Client composite primary key
	 */
	@EmbeddedId
	private Navigator_ClientPK id;
	
	/**
	 * First Name of Client.
	 */
	private String firstName;

	/**
	 * Last Name of Client.
	 */
	private String lastName;
	
	/**
	 * Given Name of Client.
	 */
	private String givenName;

	/**
	 * Middle Initial of Client.
	 */
	private String middleInitial;
	
	/**
	 * Prefix of Client (Military rank, professor, etc.)
	 */
	private String prefix;
	
	/**
	 * Generational Suffix of Client (Jr., Sr., etc.)
	 */
	private String generationSuffix;
	
	/**
	 * Professional Suffix of client (MBA, MS, etc.)
	 */
	private String professionalSuffix;
	
	/**
	 * Is this client available for an independent login on the conventional
	 * bct login page.  The default value is true.
	 */
	private boolean shared = true;
	
	/**
	 * Whether or not the navigator will be alerted to eligible newly posted
	 * trials for this client
	 */
	private boolean navigatorAlertNewTrialsEnabled;

	@Embeddable
	public static class Navigator_ClientPK implements Serializable
	{
		/**
		 * Foreign key referencing the navigator ID field.
		 */
		@Column(name = "navigator_id")
		private Long navigatorId;
		
		/**
		 * Foreign key referencing the userpatient ID field.
		 */
		@Column(name = "userpatient_id")
		private Long userpatientId;

		/**
		 * default constructor
		 */
		public Navigator_ClientPK() {}
		
		/**
		 * Convenience constructor 
		 * 
		 * @param navigatorId
		 * @param userpatientId
		 */
		public Navigator_ClientPK(Long navigatorId, Long userpatientId)
		{
			this.navigatorId = navigatorId;
			this.userpatientId = userpatientId;
		}
		
		/**
		 * @param navigatorId the navigatorId to set
		 */
		public void setNavigatorId(Long navigatorId) 
		{
			this.navigatorId = navigatorId;
		}

		/**
		 * @return the navigatorId
		 */
		public Long getNavigatorId() 
		{
			return navigatorId;
		}

		/**
		 * @param userpatientId the userpatientId to set
		 */
		public void setUserpatientId(Long userpatientId) 
		{
			this.userpatientId = userpatientId;
		}

		/**
		 * @return the userpatientId
		 */
		public Long getUserpatientId() 
		{
			return userpatientId;
		}

		@Override
		public boolean equals(Object other)
		{
			if (this == other)
			{
				return true;
			}
			if (!(other instanceof Navigator_ClientPK))
			{
				return false;
			}
			Navigator_ClientPK otherPK = (Navigator_ClientPK) other;
			
			return getNavigatorId().equals(otherPK.getNavigatorId()) && getUserpatientId().equals(otherPK.getUserpatientId()) ;
		}
		
		/**
		 * Returns the identifier's hashcode
		 * 
		 * @see java.lang.Object#hashCode()
		 * @see #id
		 */
		@Override
		public int hashCode()
		{
			return (int) (getNavigatorId() ^ (getNavigatorId() >>> 32)) | (int) (getUserpatientId() ^ (getUserpatientId() >>> 32));
		}
		
		/**
		 * 
		 */
		private static final long serialVersionUID = 2335630603053749395L;
	}

	/**
	 * Timestamp for the creation of this record.  This property is for
	 * archiving only and will be set at the database level.
	 */
	@Column(name = "created", updatable = false, insertable = false)
	@Temporal(TIMESTAMP)
	private Date created;
	
	/**
	 * database user id of creator.  This property is for
	 * archiving only and will be set at the database level.
	 */
	@Column(name = "created_by", updatable = false, insertable = false)
	private String created_by;
	
	/**
	 * Timestamp for the modification of this record.
	 */
	@Temporal(TIMESTAMP)
	private Date modified;

	/**
	 * database user id of modifier
	 */
	private String modified_by;

	/**
	 * userpatient. The persistence annotation identifies the foreign key column.
	 */
	@OneToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "userpatient_Id", insertable = false, updatable = false)
	private UserPatient userPatient;
	

	/**
	 * navigator. The persistence annotation identifies the foreign key column.
	 */
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "navigator_Id", insertable = false, updatable = false)
	private Navigator navigator;

	/**
	 * @param id the id to set
	 */
	public void setId(Navigator_ClientPK id) 
	{
		this.id = id;
	}

	/**
	 * The compound primary key for the navigator_client table.
	 * @return the Id
	 */
	public Navigator_ClientPK getId() 
	{
		return id;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) 
	{
		this.firstName = firstName;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() 
	{
		return firstName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) 
	{
		this.lastName = lastName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() 
	{
		return lastName;
	}

	/**
	 * @param givenName the givenName to set
	 */
	public void setGivenName(String givenName) 
	{
		this.givenName = givenName;
	}

	/**
	 * @return the givenName
	 */
	public String getGivenName() 
	{
		return givenName;
	}

	/**
	 * @param middleInitial the middleInitial to set
	 */
	public void setMiddleInitial(String middleInitial) 
	{
		this.middleInitial = middleInitial;
	}

	/**
	 * @return the middleInitial
	 */
	public String getMiddleInitial() 
	{
		return middleInitial;
	}

	/**
	 * @param prefix the prefix to set
	 */
	public void setPrefix(String prefix) 
	{
		this.prefix = prefix;
	}

	/**
	 * @return the prefix
	 */
	public String getPrefix() 
	{
		return prefix;
	}

	/**
	 * @param generationSuffix the generationSuffix to set
	 */
	public void setGenerationSuffix(String generationSuffix) 
	{
		this.generationSuffix = generationSuffix;
	}

	/**
	 * @return the generationSuffix
	 */
	public String getGenerationSuffix() 
	{
		return generationSuffix;
	}

	/**
	 * @param professionalSuffix the professionalSuffix to set
	 */
	public void setProfessionalSuffix(String professionalSuffix) 
	{
		this.professionalSuffix = professionalSuffix;
	}

	/**
	 * @return the professionalSuffix
	 */
	public String getProfessionalSuffix() 
	{
		return professionalSuffix;
	}

	public void setShared(boolean shared) 
	{		
		this.shared = shared;
	}
	
	/**
	 * @return the shared
	 */
	public boolean isShared() 
	{
		return shared;
	}

	/**
	 * @param navigatorAlertNewTrialsEnabled the navigatorAlertNewTrialsEnabled to set
	 */
	public void setNavigatorAlertNewTrialsEnabled(
			boolean navigatorAlertNewTrialsEnabled) {
		this.navigatorAlertNewTrialsEnabled = navigatorAlertNewTrialsEnabled;
	}

	/**
	 * @return the navigatorAlertNewTrialsEnabled
	 */
	public boolean isNavigatorAlertNewTrialsEnabled() {
		return navigatorAlertNewTrialsEnabled;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) 
	{
		this.created = created;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() 
	{
		return created;
	}

	/**
	 * @param created_by the created_by to set
	 */
	public void setCreated_by(String created_by) 
	{
		this.created_by = created_by;
	}

	/**
	 * @return the created_by
	 */
	public String getCreated_by() 
	{
		return created_by;
	}

	/**
	 * @param modified the modified to set
	 */
	public void setModified(Date modified) 
	{
		this.modified = modified;
	}

	/**
	 * @return the modified
	 */
	public Date getModified() 
	{
		return modified;
	}

	/**
	 * @param modified_by the modified_by to set
	 */
	public void setModified_by(String modified_by) 
	{
		this.modified_by = modified_by;
	}

	/**
	 * @return the modified_by
	 */
	public String getModified_by() 
	{
		return modified_by;
	}

	/**
	 * @param userPatient the userPatient to set
	 */
	public void setUserPatient(UserPatient userPatient) 
	{
		this.userPatient = userPatient;
	}

	/**
	 * @return the userPatient
	 */
	public UserPatient getUserPatient() 
	{
		return userPatient;
	}

	/**
	 * @param navigator_Id the navigator_Id to set
	 */
	public void setNavigator(Navigator navigator) 
	{
		this.navigator = navigator;
	}

	/**
	 * @return the navigator_Id
	 */
	public Navigator getNavigator() 
	{
		return navigator;
	}

	/**
	 * @return whether the identifier matches another's
	 * @param other
	 *            the other object
	 * @see java.lang.Object#equals(java.lang.Object)
	 */	
	@Override
	public boolean equals(Object other)
	{
		if (this == other)
		{
			return true;
		}
		if (!(other instanceof Navigator_Client))
		{
			return false;
		}
		
		Navigator_Client otherNavigator_Client = (Navigator_Client) (other);
		
		// If ids are not available then test trial and site individually
		if ((id != null) && (otherNavigator_Client.id != null))
		{	
			return id.equals(otherNavigator_Client.id);
		}	
		return false;
	}
	
	/**
	 * Returns the identifier's hashcode
	 * 
	 * @see java.lang.Object#hashCode()
	 * @see #id
	 */
	@Override
	public int hashCode()
	{
		return (id == null) || (id.getNavigatorId() == 0 || id.getUserpatientId() == 0) ? super.hashCode() : id.hashCode();
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8750406572965299645L;
}
