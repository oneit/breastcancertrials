/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model;

/**
 * Is described with a name, title, telephone number and email address.
 * 
 * @author Tom Bechtold
 * @version Sep 15, 2008
 */
public interface Person
{
	/**
	 * @return the surname
	 */
	public String getSurName();

	/**
	 * @return the given name
	 */
	public String getGivenName();

	/**
	 * @return the middleInitial
	 */
	public String getMiddleInitial();

	/**
	 * @return the prefix
	 */
	public String getPrefix();

	/**
	 * @return the generation suffix
	 */
	public String getGenerationSuffix();

	/**
	 * @return the professional suffix
	 */
	public String getProfessionalSuffix();

	/**
	 * @return the title
	 */
	public String getTitle();

	/**
	 * @return the telephone number
	 */
	public String getPhone();

	/**
	 * @return the email address
	 */
	public String getEmail();
}
