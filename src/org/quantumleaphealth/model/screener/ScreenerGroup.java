/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.screener;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;

/**
 * Represents a group of screeners.
 * 
 * @author Tom Bechtold
 * @version 2008-11-07
 */
@Entity
public class ScreenerGroup implements Serializable
{
	/**
	 * Unique identifier or <code>null</code> if new object.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ScreenerGroupID")
	@SequenceGenerator(name = "ScreenerGroupID", sequenceName = "screenergroup_id_seq", allocationSize = 1)
	private Long id;

	/**
	 * Name
	 */
	private String name;
	/**
	 * Primary postal code for site(s) that are assigned to this group or
	 * <code>null</code> for not applicable.
	 */
	private String postalCode;
	/**
	 * Limitations on patient enrollment in trials assigned to this screener
	 * group or <code>null</code> for none.
	 */
	private String enrollmentLimitation;

	/**
	 * @return the unique identifier or <code>null</code> if new object
	 */
	public Long getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            the unique identifier or <code>null</code> if new object
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            the name
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the primary postal code for site(s) that are assigned to this
	 *         group or <code>null</code> for not applicable
	 */
	public String getPostalCode()
	{
		return postalCode;
	}

	/**
	 * @param postalCode
	 *            the primary postal code for site(s) that are assigned to this
	 *            group or <code>null</code> for not applicable
	 */
	public void setPostalCode(String postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * @return the limitations on patient enrollment in trials assigned to this
	 *         screener group or <code>null</code> for none
	 */
	public String getEnrollmentLimitation()
	{
		return enrollmentLimitation;
	}

	/**
	 * @param enrollmentLimitation
	 *            the limitations on patient enrollment in trials assigned to
	 *            this screener group or <code>null</code> for none
	 */
	public void setEnrollmentLimitation(String enrollmentLimitation)
	{
		this.enrollmentLimitation = enrollmentLimitation;
	}

	/**
	 * @return the identifier's hashcode or the superclass'
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return (id == null) ? super.hashCode() : id.hashCode();
	}

	/**
	 * @return whether the ids match
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof ScreenerGroup))
			return false;
		ScreenerGroup other = (ScreenerGroup) obj;
		return (id == null) ? (other.id == null) : id.equals(other.id);
	}

	/**
	 * @return the superclass' representation plus the instance variables
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder(super.toString());
		builder.append('{');
		builder.append("id=").append(id);
		builder.append(",name=").append(name);
		if (postalCode != null)
			builder.append(",postalCode=").append(postalCode);
		if (enrollmentLimitation != null)
			builder.append(",enrollmentLimitation=").append(enrollmentLimitation);
		return builder.append('}').toString();
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = -5471195364791705410L;
}
