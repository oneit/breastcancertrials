/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient.test;

import java.util.Random;

import org.quantumleaphealth.model.patient.Procedure;
import org.quantumleaphealth.model.patient.YearMonth;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.testng.annotations.Test;

/**
 * Tests for the <code>Procedure</code>. As this class only references the
 * <code>CharacteristicCode</code> it is considered a "true" unit test.
 * 
 * @author Tom Bechtold
 * @version 2008-07-06
 * @see Procedure
 */
@Test(description = "Procedure", groups = { "patient", "unit" })
public class TestProcedure
{

	/**
	 * Random-number generator
	 */
	private final Random generator = new Random();

	/**
	 * Test kind field
	 */
	public void testKind()
	{
		int code = generator.nextInt();
		// Ensure that the holder is empty
		Procedure procedure = new Procedure();
		assert procedure.getKind() == null : "Kind not null " + procedure.getKind();
		// Test the set and get
		procedure.setKind(new CharacteristicCode(code));
		assert new CharacteristicCode(code).equals(procedure.getKind()) : "Characteristic " + code
				+ " not contained in procedure " + procedure;
	}

	/**
	 * Test type field
	 */
	public void testType()
	{
		int code = generator.nextInt();
		// Ensure that the holder is empty
		Procedure procedure = new Procedure();
		assert procedure.getType() == null : "Type not null " + procedure.getType();
		// Test the set and get
		procedure.setType(new CharacteristicCode(code));
		assert new CharacteristicCode(code).equals(procedure.getType()) : "Characteristic " + code
				+ " not contained in procedure " + procedure;
	}

	/**
	 * Test location field
	 */
	public void testLocation()
	{
		int code = generator.nextInt();
		// Ensure that the holder is empty
		Procedure procedure = new Procedure();
		assert procedure.getLocation() == null : "Location not null " + procedure.getLocation();
		// Test the set and get
		procedure.setLocation(new CharacteristicCode(code));
		assert new CharacteristicCode(code).equals(procedure.getLocation()) : "Characteristic " + code
				+ " not contained in procedure " + procedure;
	}

	/**
	 * Test progression field
	 */
	public void testProgression()
	{
		// Ensure that the holder is empty
		Procedure procedure = new Procedure();
		assert !procedure.getProgression() : "Progression set in procedure " + procedure;
		// Test the set and get
		procedure.setProgression(true);
		assert procedure.getProgression() : "Progression not set in procedure " + procedure;
	}

	/**
	 * Test started field
	 */
	public void testStarted()
	{
		// Ensure that the holder is non-null
		Procedure procedure = new Procedure();
		assert procedure.getStarted() != null : "Started is null " + procedure;
		// Test the set and get
		YearMonth yearMonth = new YearMonth();
		procedure.setStarted(yearMonth);
		assert yearMonth == procedure.getStarted() : "Started " + yearMonth + " not stored in procedure " + procedure;
	}

	/**
	 * Test completed field
	 */
	public void testCompleted()
	{
		// Ensure that the holder is non-null
		Procedure procedure = new Procedure();
		assert procedure.getCompleted() != null : "Completed is null " + procedure;
		// Test the set and get
		YearMonth yearMonth = new YearMonth();
		procedure.setCompleted(yearMonth);
		assert yearMonth == procedure.getCompleted() : "Completed " + yearMonth + " not stored in procedure "
				+ procedure;
	}
}
