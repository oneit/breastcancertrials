/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient.test;

import java.util.Random;

import org.quantumleaphealth.model.patient.ValueHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.testng.annotations.Test;

/**
 * Tests for the <code>ValueHistory</code>. As this class only references the
 * <code>CharacteristicCode</code> it is considered a "true" unit test.
 * 
 * @author Tom Bechtold
 * @version 2008-07-06
 * @see ValueHistory
 */
@Test(description = "Value history", groups = { "patient", "unit" })
public class TestValueHistory
{

	/**
	 * Random-number generator
	 */
	private final Random generator = new Random();

	/**
	 * Test empty history
	 */
	public void testEmptyHistory()
	{
		int code = generator.nextInt();
		// Ensure that the characteristic is not in the empty history
		ValueHistory valueHistory = new ValueHistory();
		assert valueHistory.isEmpty() : "History not empty " + valueHistory;
		assert valueHistory.getValue(new CharacteristicCode(code)) == null : "Characteristic " + code
				+ " contained in empty history " + valueHistory;
	}

	/**
	 * Test adding an element
	 */
	public void testAddCharacteristic()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}

		// Ensure that the only characteristic in the history is the one added
		ValueHistory valueHistory = new ValueHistory();
		valueHistory.setValue(new CharacteristicCode(code), new CharacteristicCode(otherCode));
		assert valueHistory.getValue(new CharacteristicCode(code)).equals(new CharacteristicCode(otherCode)) : "Characteristic "
				+ code + " not stored in " + valueHistory;
		assert valueHistory.getValue(new CharacteristicCode(otherCode)) == null : "Other characteristic " + otherCode
				+ " found in " + valueHistory;
		assert valueHistory.size() == 1 : "Number of elements " + valueHistory.size() + " is not one in "
				+ valueHistory;
	}

	/**
	 * Test removing an element
	 */
	public void testRemoveCharacteristic()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}

		// Ensure that the characteristic was removed from the history
		ValueHistory valueHistory = new ValueHistory();
		valueHistory.setValue(new CharacteristicCode(code), new CharacteristicCode(otherCode));
		valueHistory.setValue(new CharacteristicCode(code), null);
		assert valueHistory.getValue(new CharacteristicCode(code)) == null : "Characteristic " + code
				+ " found in removed " + valueHistory;
		assert valueHistory.getValue(new CharacteristicCode(otherCode)) == null : "Other characteristic " + otherCode
				+ " found in removed " + valueHistory;
		assert valueHistory.isEmpty() : "Number of elements " + valueHistory.size() + " is not zero in " + valueHistory;
	}
}
