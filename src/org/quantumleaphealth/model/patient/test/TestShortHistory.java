/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient.test;

import java.util.Random;

import org.quantumleaphealth.model.patient.ShortHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.testng.annotations.Test;

/**
 * Tests for the <code>ShortHistory</code>. As this class only references the
 * <code>CharacteristicCode</code> it is considered a "true" unit test.
 * 
 * @author Tom Bechtold
 * @version 2008-07-06
 * @see ShortHistory
 */
@Test(description = "Short history", groups = { "patient", "unit" })
public class TestShortHistory
{

	/**
	 * Random-number generator
	 */
	private final Random generator = new Random();

	/**
	 * Test empty history
	 */
	public void testEmptyHistory()
	{
		int code = generator.nextInt();
		// Ensure that the characteristic is not in the empty history
		ShortHistory shortHistory = new ShortHistory();
		assert shortHistory.isEmpty() : "History not empty " + shortHistory;
		assert shortHistory.getShort(new CharacteristicCode(code)) == null : "Characteristic " + code
				+ " contained in empty history " + shortHistory;
	}

	/**
	 * Test adding an element
	 */
	public void testAddCharacteristic()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}

		// Ensure that the only characteristic in the history is the one added
		ShortHistory shortHistory = new ShortHistory();
		Short value = Short.valueOf((short) (otherCode & Short.MAX_VALUE));
		shortHistory.setShort(new CharacteristicCode(code), value);
		assert shortHistory.getShort(new CharacteristicCode(code)).equals(value) : "Characteristic " + code
				+ " not stored in " + shortHistory;
		assert shortHistory.getShort(new CharacteristicCode(value.intValue())) == null : "Other characteristic "
				+ value + " found in " + shortHistory;
		assert shortHistory.size() == 1 : "Number of elements " + shortHistory.size() + " is not one in "
				+ shortHistory;
	}

	/**
	 * Test removing an element
	 */
	public void testRemoveCharacteristic()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}
		Short value = Short.valueOf((short) (otherCode & Short.MAX_VALUE));

		// Ensure that the characteristic was removed from the history
		ShortHistory shortHistory = new ShortHistory();
		shortHistory.setShort(new CharacteristicCode(code), value);
		shortHistory.setShort(new CharacteristicCode(code), null);
		assert shortHistory.getShort(new CharacteristicCode(code)) == null : "Characteristic " + code
				+ " found in removed " + shortHistory;
		assert shortHistory.getShort(new CharacteristicCode(otherCode)) == null : "Other characteristic " + otherCode
				+ " found in removed " + shortHistory;
		assert shortHistory.isEmpty() : "Number of elements " + shortHistory.size() + " is not zero in " + shortHistory;
	}
}
