/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient.test;

import java.util.Date;

import org.quantumleaphealth.model.patient.PatientDisposition;
import org.quantumleaphealth.model.patient.PatientDisposition.Disposition;

import static org.quantumleaphealth.screen.test.TestUtils.GENERATOR;
import org.testng.annotations.Test;

/**
 * Tests for the <code>PatientDisposition</code>. This class tests the initial
 * property values of a newly constructed object and the getter/setter methods
 * on property values.
 * 
 * @author Tom Bechtold
 * @version 2009-07-07
 * @see PatientDisposition
 */
@Test(description = "PatientDisposition", groups = { "patient", "unit" })
public class TestPatientDisposition
{

	/**
	 * Test Id field
	 */
	public void testId()
	{
		// Ensure that the holder is empty
		PatientDisposition patientDisposition = new PatientDisposition(null, null, null, 0, 0, null);
		assert patientDisposition.getId() == null : "id not null " + patientDisposition.getId();
		// Test the set and get
		Long id = Long.valueOf(GENERATOR.nextLong());
		patientDisposition.setId(id);
		assert id.equals(patientDisposition.getId()) : "Id " + id + " not contained in invitation "
				+ patientDisposition;
	}

	/**
	 * Test TrialId field
	 */
	public void testTrialId()
	{
		// Ensure that the holder is empty
		PatientDisposition patientDisposition = new PatientDisposition(null, null, null, 0, 0, null);
		assert patientDisposition.getTrialId() == null : "trial id not null " + patientDisposition.getTrialId();
		// Test the set and get
		Long id = Long.valueOf(GENERATOR.nextLong());
		patientDisposition.setTrialId(id);
		assert id.equals(patientDisposition.getTrialId()) : "TrialId " + id + " not contained in " + patientDisposition;
	}

	/**
	 * Test Site Id field
	 */
	public void testSiteId()
	{
		// Ensure that the holder is empty
		PatientDisposition patientDisposition = new PatientDisposition(null, null, null, 0, 0, null);
		assert patientDisposition.getSiteId() == null : "site id not null " + patientDisposition.getSiteId();
		// Test the set and get
		Long id = Long.valueOf(GENERATOR.nextLong());
		patientDisposition.setSiteId(id);
		assert id.equals(patientDisposition.getSiteId()) : "SiteId " + id + " not contained in " + patientDisposition;
	}

	/**
	 * Test Disposition field
	 */
	public void testDisposition()
	{
		// Ensure that the holder is set to default
		PatientDisposition patientDisposition = new PatientDisposition(null, null, null, 0, 0, null);
		assert patientDisposition.getDisposition() == null : "disposition not null: "
				+ patientDisposition.getDisposition();
		// Test the set and get
		Disposition disposition = Disposition.values()[GENERATOR.nextInt(Disposition.values().length - 1) + 1];
		patientDisposition.setDisposition(disposition);
		assert disposition.equals(patientDisposition.getDisposition()) : "disposition " + disposition
				+ " not contained: " + patientDisposition.getDisposition();
	}

	/**
	 * Test Declined field
	 */
	public void testDeclined()
	{
		// Ensure that the holder is empty
		PatientDisposition patientDisposition = new PatientDisposition(null, null, null, 0, 0, null);
		assert patientDisposition.getDeclined() == 0 : "declined not zero " + patientDisposition.getDeclined();
		// Test the set and get
		long declined = GENERATOR.nextLong();
		patientDisposition.setDeclined(declined);
		assert declined == patientDisposition.getDeclined() : "declined " + declined + " not contained: "
				+ patientDisposition.getDeclined();
	}

	/**
	 * Test Ineligible field
	 */
	public void testIneligible()
	{
		// Ensure that the holder is empty
		PatientDisposition patientDisposition = new PatientDisposition(null, null, null, 0, 0, null);
		assert patientDisposition.getIneligible() == 0 : "ineligible not null " + patientDisposition.getIneligible();
		// Test the set and get
		long ineligible = GENERATOR.nextLong();
		patientDisposition.setIneligible(ineligible);
		assert ineligible == patientDisposition.getIneligible() : "ineligible " + ineligible + " not contained: "
				+ patientDisposition.getIneligible();
	}

	/**
	 * Test DispositionDate field
	 */
	public void testDispositionDate()
	{
		// Ensure that the holder is empty
		PatientDisposition patientDisposition = new PatientDisposition(null, null, null, 0, 0, null);
		assert patientDisposition.getDispositionDate() == null : "dispositionDate not null "
				+ patientDisposition.getDispositionDate();
		// Test the set and get
		Date date = new Date(Long.valueOf(Math.abs(GENERATOR.nextLong())));
		patientDisposition.setDispositionDate(date);
		assert date.equals(patientDisposition.getDispositionDate()) : "dispositionDate "
				+ patientDisposition.getDispositionDate() + " not stored as " + date;
	}
}
