/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient.test;

import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.quantumleaphealth.model.patient.Invitation;
import org.quantumleaphealth.model.patient.ScreenedTrial;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.screen.test.TestUtils;
import org.quantumleaphealth.xml.DecodingException;
import org.testng.annotations.Test;

/**
 * Tests for the <code>UserPatient</code>.
 * 
 * @author Tom Bechtold
 * @version 2008-12-28
 * @see UserPatient
 */
@Test(description = "UserPatient", groups = { "patient", "unit" })
public class TestUserPatient
{

	/**
	 * Untrimmed string
	 */
	private static final String NOTTRIMMED = " fooBar  ";
	/**
	 * Trimmed string
	 */
	private static final String TRIMMED = NOTTRIMMED.trim();
	/**
	 * Blank string
	 */
	private static final String BLANK = "  ";

	/**
	 * Test id field
	 */
	public void testId()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getId() == null : userPatient.getId() + " not null ";
		// Test the set and get
		Long id = Long.valueOf(TestUtils.GENERATOR.nextLong());
		userPatient.setId(id);
		assert id.equals(userPatient.getId()) : userPatient.getId() + " not stored as " + id;
	}

	/**
	 * Test countryCode field
	 */
	public void testCountryCode()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getCountryCode() == null : userPatient.getCountryCode() + " not null";
		// Test the set and get on no-trimmed, trimmed, null, blank strings
		userPatient.setCountryCode(TRIMMED);
		assert TRIMMED.equals(userPatient.getCountryCode()) : userPatient.getCountryCode() + " not stored as "
				+ TRIMMED;
		userPatient.setCountryCode(NOTTRIMMED);
		assert TRIMMED.equals(userPatient.getCountryCode()) : userPatient.getCountryCode() + " not stored as "
				+ TRIMMED;
		userPatient.setCountryCode(null);
		assert userPatient.getCountryCode() == null : userPatient.getCountryCode() + " not null";
		userPatient.setCountryCode(BLANK);
		assert userPatient.getCountryCode() == null : userPatient.getCountryCode() + " not null blank";
	}

	/**
	 * Test privatePostalCode field
	 */
	public void testPrivatePostalCode()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getPrivatePostalCode() == null : userPatient.getPrivatePostalCode() + " not null ";
		// Test the set and get
		Long privatePostalCode = Long.valueOf(TestUtils.GENERATOR.nextLong());
		userPatient.setPrivatePostalCode(privatePostalCode);
		assert privatePostalCode.equals(userPatient.getPrivatePostalCode()) : userPatient.getPrivatePostalCode()
				+ " not stored as " + privatePostalCode;
	}

	/**
	 * Test principal field
	 */
	public void testPrincipal()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getPrincipal() == null : userPatient.getPrincipal() + " not null";
		// Test the set and get on no-trimmed, trimmed, null, blank strings
		userPatient.setPrincipal(TRIMMED);
		assert TRIMMED.equals(userPatient.getPrincipal()) : userPatient.getPrincipal() + " not stored as " + TRIMMED;
		userPatient.setPrincipal(NOTTRIMMED);
		assert TRIMMED.equals(userPatient.getPrincipal()) : userPatient.getPrincipal() + " not stored as " + TRIMMED;
		userPatient.setPrincipal(null);
		assert userPatient.getPrincipal() == null : userPatient.getPrincipal() + " not null";
		userPatient.setPrincipal(BLANK);
		assert userPatient.getPrincipal() == null : userPatient.getPrincipal() + " not null blank";
	}

	/**
	 * Test credentials field
	 */
	public void testCredentials()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getCredentials() == null : userPatient.getCredentials() + " not null";
		// Test the set and get on no-trimmed, trimmed, null, blank strings
		userPatient.setCredentials(TRIMMED);
		assert TRIMMED.equals(userPatient.getCredentials()) : userPatient.getCredentials() + " not stored as "
				+ TRIMMED;
		userPatient.setCredentials(NOTTRIMMED);
		assert TRIMMED.equals(userPatient.getCredentials()) : userPatient.getCredentials() + " not stored as "
				+ TRIMMED;
		userPatient.setCredentials(null);
		assert userPatient.getCredentials() == null : userPatient.getCredentials() + " not null";
		userPatient.setCredentials(BLANK);
		assert userPatient.getCredentials() == null : userPatient.getCredentials() + " not null blank";
	}

	/**
	 * Test secretQuestion field
	 */
	public void testSecretQuestion()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getSecretQuestion() == null : userPatient.getSecretQuestion() + " not null";
		// Test the set and get on no-trimmed, trimmed, null, blank strings
		userPatient.setSecretQuestion(TRIMMED);
		assert TRIMMED.equals(userPatient.getSecretQuestion()) : userPatient.getSecretQuestion() + " not stored as "
				+ TRIMMED;
		userPatient.setSecretQuestion(NOTTRIMMED);
		assert TRIMMED.equals(userPatient.getSecretQuestion()) : userPatient.getSecretQuestion() + " not stored as "
				+ TRIMMED;
		userPatient.setSecretQuestion(null);
		assert userPatient.getSecretQuestion() == null : userPatient.getSecretQuestion() + " not null";
		userPatient.setSecretQuestion(BLANK);
		assert userPatient.getSecretQuestion() == null : userPatient.getSecretQuestion() + " not null blank";
	}

	/**
	 * Test secretAnswer field
	 */
	public void testSecretAnswer()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getSecretAnswer() == null : userPatient.getSecretAnswer() + " not null";
		// Test the set and get on no-trimmed, trimmed, null, blank strings
		userPatient.setSecretAnswer(TRIMMED);
		assert TRIMMED.equals(userPatient.getSecretAnswer()) : userPatient.getSecretAnswer() + " not stored as "
				+ TRIMMED;
		userPatient.setSecretAnswer(NOTTRIMMED);
		assert TRIMMED.equals(userPatient.getSecretAnswer()) : userPatient.getSecretAnswer() + " not stored as "
				+ TRIMMED;
		userPatient.setSecretAnswer(null);
		assert userPatient.getSecretAnswer() == null : userPatient.getSecretAnswer() + " not null";
		userPatient.setSecretAnswer(BLANK);
		assert userPatient.getSecretAnswer() == null : userPatient.getSecretAnswer() + " not null blank";
	}

	/**
	 * Test unsuccessfulAuthenticationCount field
	 */
	public void testUnsuccessfulAuthenticationCount()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getUnsuccessfulAuthenticationCount() == 0 : userPatient.getUnsuccessfulAuthenticationCount()
				+ " not 0";
		// Test the set and get
		int unsuccessfulAuthenticationCount = TestUtils.GENERATOR.nextInt();
		userPatient.setUnsuccessfulAuthenticationCount(unsuccessfulAuthenticationCount);
		assert unsuccessfulAuthenticationCount == userPatient.getUnsuccessfulAuthenticationCount() : Integer
				.toString(userPatient.getUnsuccessfulAuthenticationCount())
				+ " not stored as " + unsuccessfulAuthenticationCount;
	}

	/**
	 * Test termsAccepted fields
	 */
	public void testTermsAccepted()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getTermsAcceptedTime() == null : userPatient.getTermsAcceptedTime() + " not null";
		assert !userPatient.isTermsAccepted() : "terms accepted upon creation";
		// Test the set and get
		Date termsAcceptedTime = new Date(TestUtils.GENERATOR.nextLong());
		userPatient.setTermsAcceptedTime(termsAcceptedTime);
		assert termsAcceptedTime.equals(userPatient.getTermsAcceptedTime()) : userPatient.getTermsAcceptedTime()
				+ " not stored as " + termsAcceptedTime;
		assert userPatient.isTermsAccepted() : "terms not accepted after set";
	}

	/**
	 * Test patientHistory field
	 */
	public void testPatientHistory()
	{
		// Ensure that the holder is not empty
		UserPatient userPatient = new UserPatient();
		assert userPatient.getPatientHistory() != null : "null on creation";
		// Test the encoding/decoding
		try
		{
			userPatient.setPatientHistoryMarshaled(userPatient.getPatientHistoryMarshaled());
			userPatient.setPatientHistoryMarshaled(null);
		}
		catch (DecodingException decodingException)
		{
			throw new AssertionError(decodingException);
		}
		// Test setting null
		try
		{
			userPatient.setPatientHistoryMarshaled(null);
		}
		catch (DecodingException decodingException)
		{
			throw new AssertionError(decodingException);
		}
		assert userPatient.getPatientHistory() != null : "null after setting to null";
	}

	/**
	 * Test patientHistoryLastModified field
	 */
	public void testPatientHistoryLastModified()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getPatientHistoryLastModified() == null : userPatient.getPatientHistoryLastModified()
				+ " not null";
		// Test the set and get
		Date patientHistoryLastModified = new Date(TestUtils.GENERATOR.nextLong());
		userPatient.setPatientHistoryLastModified(patientHistoryLastModified);
		assert patientHistoryLastModified.equals(userPatient.getPatientHistoryLastModified()) : userPatient
				.getPatientHistoryLastModified()
				+ " not stored as " + patientHistoryLastModified;
	}

	/**
	 * Test patientHistoryComplete field
	 */
	public void testPatientHistoryComplete()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert !userPatient.isPatientHistoryComplete() : "history complete upon creation";
		// Test the set and get
		userPatient.setPatientHistoryComplete(true);
		assert userPatient.isPatientHistoryComplete() : "history not complete after set";
	}

	/**
	 * Test screenedTrials field
	 */
	public void testScreenedTrials()
	{
		// Ensure that the holder is empty
		UserPatient userPatient = new UserPatient();
		assert userPatient.getScreenedTrials() != null : "null on creation";
		// Test setting null
		try
		{
			userPatient.setScreenedTrials(null);
			throw new AssertionError("set to null");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		// Test set and get
		List<ScreenedTrial> list = new LinkedList<ScreenedTrial>();
		userPatient.setScreenedTrials(list);
		assert userPatient.getScreenedTrials() == list : userPatient.getScreenedTrials() + " not stored as " + list;
	}

	/**
	 * Test screenedTrialsLastModified field
	 */
	public void testScreenedTrialsLastModified()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getScreenedTrialsLastModified() == null : userPatient.getScreenedTrialsLastModified()
				+ " not null";
		// Test the set and get
		Date screenedTrialsLastModified = new Date(TestUtils.GENERATOR.nextLong());
		userPatient.setScreenedTrialsLastModified(screenedTrialsLastModified);
		assert screenedTrialsLastModified.equals(userPatient.getScreenedTrialsLastModified()) : userPatient
				.getScreenedTrialsLastModified()
				+ " not stored as " + screenedTrialsLastModified;
	}

	/**
	 * Test invitations field
	 */
	public void testInvitations()
	{
		// Ensure that the holder is empty
		UserPatient userPatient = new UserPatient();
		assert userPatient.getInvitations() != null : "null on creation";
		// Test setting null
		try
		{
			userPatient.setInvitations(null);
			throw new AssertionError("set to null");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		// Test set and get
		Set<Invitation> list = new LinkedHashSet<Invitation>();
		userPatient.setInvitations(list);
		assert userPatient.getInvitations() == list : userPatient.getInvitations() + " not stored as " + list;
	}

	/**
	 * Test alertNewTrialsEnabled field
	 */
	public void testAlertNewTrialsEnabled()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert !userPatient.isAlertNewTrialsEnabled() : "alert upon creation";
		// Test the set and get
		userPatient.setAlertNewTrialsEnabled(true);
		assert userPatient.isAlertNewTrialsEnabled() : "alert not on after set";
	}

	/**
	 * Test principalInvalidEmailAddress field
	 */
	public void testPrincipalInvalidEmailAddress()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert !userPatient.isPrincipalInvalidEmailAddress() : "invalid upon creation";
		// Test the set and get
		userPatient.setPrincipalInvalidEmailAddress(true);
		assert userPatient.isPrincipalInvalidEmailAddress() : "valid after set";
	}

	/**
	 * Test referral field
	 */
	public void testReferral()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getReferral() == 0 : userPatient.getReferral() + " not 0";
		// Test the set and get
		int referral = TestUtils.GENERATOR.nextInt();
		userPatient.setReferral(referral);
		assert referral == userPatient.getReferral() : Integer.toString(userPatient.getReferral()) + " not stored as "
				+ referral;
	}

	/**
	 * Test referralLink field
	 */
	public void testReferralLink()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getReferralLink() == null : userPatient.getReferralLink() + " not null";
		// Test the set and get on no-trimmed, trimmed, null, blank strings
		userPatient.setReferralLink(TRIMMED);
		assert TRIMMED.equals(userPatient.getReferralLink()) : userPatient.getReferralLink() + " not stored as "
				+ TRIMMED;
		userPatient.setReferralLink(NOTTRIMMED);
		assert TRIMMED.equals(userPatient.getReferralLink()) : userPatient.getReferralLink() + " not stored as "
				+ TRIMMED;
		userPatient.setReferralLink(null);
		assert userPatient.getReferralLink() == null : userPatient.getReferralLink() + " not null";
		userPatient.setReferralLink(BLANK);
		assert userPatient.getReferralLink() == null : userPatient.getReferralLink() + " not null blank";
	}

	/**
	 * Test referralOrganization field
	 */
	public void testReferralOrganization()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getReferralOrganization() == null : userPatient.getReferralOrganization() + " not null";
		// Test the set and get on no-trimmed, trimmed, null, blank strings
		userPatient.setReferralOrganization(TRIMMED);
		assert TRIMMED.equals(userPatient.getReferralOrganization()) : userPatient.getReferralOrganization()
				+ " not stored as " + TRIMMED;
		userPatient.setReferralOrganization(NOTTRIMMED);
		assert TRIMMED.equals(userPatient.getReferralOrganization()) : userPatient.getReferralOrganization()
				+ " not stored as " + TRIMMED;
		userPatient.setReferralOrganization(null);
		assert userPatient.getReferralOrganization() == null : userPatient.getReferralOrganization() + " not null";
		userPatient.setReferralOrganization(BLANK);
		assert userPatient.getReferralOrganization() == null : userPatient.getReferralOrganization()
				+ " not null blank";
	}

	/**
	 * Test referralOther field
	 */
	public void testReferralOther()
	{
		UserPatient userPatient = new UserPatient();
		// Ensure that the holder is empty
		assert userPatient.getReferralOther() == null : userPatient.getReferralOther() + " not null";
		// Test the set and get on no-trimmed, trimmed, null, blank strings
		userPatient.setReferralOther(TRIMMED);
		assert TRIMMED.equals(userPatient.getReferralOther()) : userPatient.getReferralOther() + " not stored as "
				+ TRIMMED;
		userPatient.setReferralOther(NOTTRIMMED);
		assert TRIMMED.equals(userPatient.getReferralOther()) : userPatient.getReferralOther() + " not stored as "
				+ TRIMMED;
		userPatient.setReferralOther(null);
		assert userPatient.getReferralOther() == null : userPatient.getReferralOther() + " not null";
		userPatient.setReferralOther(BLANK);
		assert userPatient.getReferralOther() == null : userPatient.getReferralOther() + " not null blank";
	}
}
