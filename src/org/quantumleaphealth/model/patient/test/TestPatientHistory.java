/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient.test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.quantumleaphealth.model.patient.BinaryHistory;
import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.Procedure;
import org.quantumleaphealth.model.patient.Therapy;
import org.quantumleaphealth.model.patient.ValueHistory;
import org.quantumleaphealth.model.patient.YearMonthHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.testng.annotations.Test;

/**
 * Tests for the <code>PatientHistory</code>. This composite test class
 * references the following classes:
 * <ul>
 * <li><code>CharacteristicCode</code></li>
 * <li><code>BinaryHistory</code></li>
 * <li><code>ValueHistory</code></li>
 * <li><code>YearMonthHistory</code></li>
 * <li><code>Diagnosis</code></li>
 * <li><code>Procedure</code></li>
 * <li><code>Therapy</code></li>
 * </ul>
 * 
 * @author Tom Bechtold
 * @version 2008-07-06
 * @see PatientHistory
 */
@Test(description = "Patient history", groups = { "patient", "composite" })
public class TestPatientHistory
{

	/**
	 * Random-number generator
	 */
	private final Random generator = new Random();

	/**
	 * @return three unique integers
	 */
	private int[] getThreeUniqueIntegers()
	{
		int[] unique = new int[3];
		while ((unique[0] == unique[1]) || (unique[0] == unique[2]) || (unique[1] == unique[2]))
		{
			unique[0] = generator.nextInt();
			unique[1] = generator.nextInt();
			unique[2] = generator.nextInt();
		}
		return unique;
	}

	/**
	 * Test binary history
	 */
	public void testBinaryHistory()
	{
		// Ensure non-null and empty
		PatientHistory patientHistory = new PatientHistory();
		BinaryHistory binaryHistory = patientHistory.getBinaryHistory();
		assert (binaryHistory != null) && binaryHistory.isEmpty() : "New binary history is not empty " + binaryHistory;

		// Test set and get
		binaryHistory = new BinaryHistory();
		int code = generator.nextInt();
		binaryHistory.addCharacteristic(new CharacteristicCode(code));
		patientHistory.setBinaryHistory(binaryHistory);
		assert (binaryHistory == patientHistory.getBinaryHistory()) : "Retrieved binary history "
				+ patientHistory.getBinaryHistory() + " is not the one stored " + binaryHistory;
	}

	/**
	 * Test value history
	 */
	public void testValueHistory()
	{
		// Ensure non-null and empty
		PatientHistory patientHistory = new PatientHistory();
		ValueHistory valueHistory = patientHistory.getValueHistory();
		assert (valueHistory != null) && valueHistory.isEmpty() : "New value history is not empty " + valueHistory;

		// Test set and get
		valueHistory = new ValueHistory();
		int[] unique = getThreeUniqueIntegers();
		valueHistory.setValue(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1]));
		patientHistory.setValueHistory(valueHistory);
		assert (valueHistory == patientHistory.getValueHistory()) : "Retrieved value history "
				+ patientHistory.getValueHistory() + " is not the one stored " + valueHistory;
	}

	/**
	 * Test year/month history
	 */
	public void testYearMonthHistory()
	{
		// Ensure non-null and empty
		PatientHistory patientHistory = new PatientHistory();
		YearMonthHistory yearMonthHistory = patientHistory.getYearMonthHistory();
		assert (yearMonthHistory != null) && yearMonthHistory.isEmpty() : "New year/month history is not empty "
				+ yearMonthHistory;

		// Test set and get
		yearMonthHistory = new YearMonthHistory();
		int[] unique = getThreeUniqueIntegers();
		yearMonthHistory.setShort(new CharacteristicCode(unique[0]), Short
				.valueOf((short) (unique[1] & Short.MAX_VALUE)));
		patientHistory.setYearMonthHistory(yearMonthHistory);
		assert (yearMonthHistory == patientHistory.getYearMonthHistory()) : "Retrieved year/month history "
				+ patientHistory.getYearMonthHistory() + " is not the one stored " + yearMonthHistory;
	}

	/**
	 * Test attribute characteristics
	 */
	public void testAttributeCharacteristics()
	{
		// Ensure non-null and empty
		PatientHistory patientHistory = new PatientHistory();
		Map<CharacteristicCode, Set<CharacteristicCode>> attributeCharacteristics = patientHistory
				.getAttributeCharacteristics();
		assert (attributeCharacteristics != null) && attributeCharacteristics.isEmpty() : "New attribute characteristics is not empty "
				+ attributeCharacteristics;

		// Test set and get
		attributeCharacteristics = new HashMap<CharacteristicCode, Set<CharacteristicCode>>(1, 1.0f);
		int[] unique = getThreeUniqueIntegers();
		attributeCharacteristics.put(new CharacteristicCode(unique[0]), new LinkedHashSet<CharacteristicCode>(Arrays
				.asList(new CharacteristicCode(unique[1]), new CharacteristicCode(unique[2]))));
		patientHistory.setAttributeCharacteristics(attributeCharacteristics);
		assert (attributeCharacteristics == patientHistory.getAttributeCharacteristics()) : "Retrieved attribute characteristics "
				+ patientHistory.getAttributeCharacteristics() + " is not the one stored " + attributeCharacteristics;
	}

	/**
	 * Test adding and removing attributes to characteristics
	 */
	public void testAddRemoveAttribute()
	{
		int[] unique = getThreeUniqueIntegers();

		// Ensure that the attribute is not in the fresh history
		PatientHistory patientHistory = new PatientHistory();
		assert !patientHistory.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1])) : "New attribute characteristics "
				+ patientHistory.getAttributeCharacteristics()
				+ " contains "
				+ unique[1]
				+ " attribute for characteristic " + unique[0];

		// Ensure that the added attribute shows up in the history
		patientHistory.addAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1]));
		assert patientHistory.getAttributeCharacteristics().size() == 1 : "After adding first attribute the map size "
				+ patientHistory.getAttributeCharacteristics().size() + " is not 1";
		assert patientHistory.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1])) : "Attribute characteristic "
				+ patientHistory.getAttributeCharacteristics()
				+ " does not contain "
				+ unique[1]
				+ " attribute for characteristic " + unique[0];

		// Add a second attribute to the same characteristic
		patientHistory.addAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[2]));
		assert patientHistory.getAttributeCharacteristics().size() == 1 : "After adding second attribute the map size "
				+ patientHistory.getAttributeCharacteristics().size() + " is not 1";
		assert patientHistory.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1])) : "Attribute characteristic "
				+ patientHistory.getAttributeCharacteristics()
				+ " does not contain "
				+ unique[1]
				+ " attribute for characteristic " + unique[0];
		assert patientHistory.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[2])) : "Attribute characteristic "
				+ patientHistory.getAttributeCharacteristics()
				+ " does not contain "
				+ unique[2]
				+ " attribute for characteristic " + unique[0];
		// Verify the set stored is correct
		Set<CharacteristicCode> attributes = patientHistory.getAttributeCharacteristics().get(
				new CharacteristicCode(unique[0]));
		assert attributes.containsAll(Arrays.asList(new CharacteristicCode(unique[1]),
				new CharacteristicCode(unique[2]))) : "Attribute set " + attributes
				+ " does not contain both attributes " + unique[1] + " and " + unique[2];

		// Adding a duplicate attribute should not affect anything
		patientHistory.addAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[2]));
		assert patientHistory.getAttributeCharacteristics().size() == 1 : "After adding duplicate attribute the map size "
				+ patientHistory.getAttributeCharacteristics().size() + " is not 1";
		assert patientHistory.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1])) : "Attribute characteristic "
				+ patientHistory.getAttributeCharacteristics()
				+ " does not contain "
				+ unique[1]
				+ " attribute for characteristic " + unique[0];
		assert patientHistory.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[2])) : "Attribute characteristic "
				+ patientHistory.getAttributeCharacteristics()
				+ " does not contain "
				+ unique[2]
				+ " attribute for characteristic " + unique[0];
		attributes = patientHistory.getAttributeCharacteristics().get(new CharacteristicCode(unique[0]));
		assert attributes.containsAll(Arrays.asList(new CharacteristicCode(unique[1]),
				new CharacteristicCode(unique[2]))) : "Attribute set " + attributes
				+ " does not contain both attributes " + unique[1] + " and " + unique[2];

		// Remove latter attribute
		patientHistory.removeAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[2]));
		assert patientHistory.getAttributeCharacteristics().size() == 1 : "After removing first attribute the map size "
				+ patientHistory.getAttributeCharacteristics().size() + " is not 1";
		assert patientHistory.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1])) : "Attribute characteristic "
				+ patientHistory.getAttributeCharacteristics()
				+ " does not contain "
				+ unique[1]
				+ " attribute for characteristic " + unique[0];
		assert !patientHistory.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[2])) : "Attribute characteristic "
				+ patientHistory.getAttributeCharacteristics()
				+ " contains "
				+ unique[2]
				+ " attribute for characteristic " + unique[0];
		// Verify the set stored only has one left
		attributes = patientHistory.getAttributeCharacteristics().get(new CharacteristicCode(unique[0]));
		assert attributes.containsAll(Arrays.asList(new CharacteristicCode(unique[1]))) : "Attribute set " + attributes
				+ " does not contain attribute " + unique[1];

		// Remove former attribute so map is empty
		patientHistory.removeAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1]));
		assert patientHistory.getAttributeCharacteristics().size() == 0 : "After removing second attribute the map size "
				+ patientHistory.getAttributeCharacteristics().size() + " is not 0";
		assert !patientHistory.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1])) : "Attribute characteristic "
				+ patientHistory.getAttributeCharacteristics()
				+ " contains "
				+ unique[1]
				+ " attribute for characteristic " + unique[0];
		assert !patientHistory.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[2])) : "Attribute characteristic "
				+ patientHistory.getAttributeCharacteristics()
				+ " contains "
				+ unique[2]
				+ " attribute for characteristic " + unique[0];
		// Verify the set stored is now null
		attributes = patientHistory.getAttributeCharacteristics().get(new CharacteristicCode(unique[0]));
		assert attributes == null : "Attribute set after removal is not empty for characteristic " + unique[0];
	}

	/**
	 * Test string characteristics
	 */
	public void testStringCharacteristics()
	{
		// Ensure non-null and empty
		PatientHistory patientHistory = new PatientHistory();
		Map<CharacteristicCode, String> stringCharacteristics = patientHistory.getStringCharacteristics();
		assert (stringCharacteristics != null) && stringCharacteristics.isEmpty() : "New string characteristics is not empty "
				+ stringCharacteristics;

		// Test set and get
		stringCharacteristics = new HashMap<CharacteristicCode, String>(1, 1.0f);
		int[] unique = getThreeUniqueIntegers();
		stringCharacteristics.put(new CharacteristicCode(unique[0]), Integer.toString(unique[1]));
		patientHistory.setStringCharacteristics(stringCharacteristics);
		assert (stringCharacteristics == patientHistory.getStringCharacteristics()) : "Retrieved string characteristics "
				+ patientHistory.getStringCharacteristics() + " is not the one stored " + stringCharacteristics;
	}

	/**
	 * Test adding and removing attributes to characteristics
	 */
	public void testAddRemoveString()
	{
		int[] unique = getThreeUniqueIntegers();

		// Ensure that the string is not in the fresh history
		PatientHistory patientHistory = new PatientHistory();
		assert patientHistory.getString(new CharacteristicCode(unique[0])) == null : "New string characteristics "
				+ patientHistory.getStringCharacteristics() + " contains " + unique[0];

		// Ensure that the added string shows up in the history
		patientHistory.setString(new CharacteristicCode(unique[0]), Integer.toString(unique[1]));
		assert patientHistory.getStringCharacteristics().size() == 1 : "After adding first attribute the map size "
				+ patientHistory.getAttributeCharacteristics().size() + " is not 1";
		assert patientHistory.getString(new CharacteristicCode(unique[0])).equals(Integer.toString(unique[1])) : "String characteristics "
				+ patientHistory.getStringCharacteristics()
				+ " does not contain "
				+ unique[1]
				+ " attribute for characteristic " + unique[0];

		// Remove attribute so map is empty
		patientHistory.setString(new CharacteristicCode(unique[0]), null);
		assert patientHistory.getStringCharacteristics().size() == 0 : "After removing string the map size "
				+ patientHistory.getStringCharacteristics().size() + " is not 0";
		assert patientHistory.getString(new CharacteristicCode(unique[0])) == null : "String characteristic "
				+ patientHistory.getStringCharacteristics() + " contains string for characteristic " + unique[0];
	}

	/**
	 * Test diagnoses
	 */
	public void testDiagnoses()
	{
		// Ensure non-null and empty
		PatientHistory patientHistory = new PatientHistory();
		Set<Diagnosis> diagnoses = patientHistory.getDiagnoses();
		assert (diagnoses != null) && diagnoses.isEmpty() : "New diagnoses is not empty " + diagnoses;

		// Test set and get
		diagnoses = new LinkedHashSet<Diagnosis>(1, 1.0f);
		Diagnosis diagnosis = new Diagnosis();
		diagnoses.add(diagnosis);
		patientHistory.setDiagnoses(diagnoses);
		assert (diagnoses == patientHistory.getDiagnoses()) : "Retrieved diagnoses " + patientHistory.getDiagnoses()
				+ " is not the one stored " + diagnoses;
	}

	/**
	 * Test procedures
	 */
	public void testProcedures()
	{
		// Ensure non-null and empty
		PatientHistory patientHistory = new PatientHistory();
		Set<Procedure> procedures = patientHistory.getProcedures();
		assert (procedures != null) && procedures.isEmpty() : "New procedures is not empty " + procedures;

		// Test set and get
		procedures = new LinkedHashSet<Procedure>(1, 1.0f);
		Procedure diagnosis = new Procedure();
		procedures.add(diagnosis);
		patientHistory.setProcedures(procedures);
		assert (procedures == patientHistory.getProcedures()) : "Retrieved procedures "
				+ patientHistory.getProcedures() + " is not the one stored " + procedures;
	}

	/**
	 * Test therapies
	 */
	public void testTherapies()
	{
		// Ensure non-null and empty
		PatientHistory patientHistory = new PatientHistory();
		Set<Therapy> therapies = patientHistory.getTherapies();
		assert (therapies != null) && therapies.isEmpty() : "New therapies is not empty " + therapies;

		// Test set and get
		therapies = new LinkedHashSet<Therapy>(1, 1.0f);
		Therapy diagnosis = new Therapy();
		therapies.add(diagnosis);
		patientHistory.setTherapies(therapies);
		assert (therapies == patientHistory.getTherapies()) : "Retrieved therapies " + patientHistory.getTherapies()
				+ " is not the one stored " + therapies;
	}
}
