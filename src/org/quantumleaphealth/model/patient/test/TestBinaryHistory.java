/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient.test;

import org.quantumleaphealth.model.patient.BinaryHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;
import java.util.Random;
import org.testng.annotations.Test;

/**
 * Tests for the <code>BinaryHistory</code>. As this class only references the
 * <code>CharacteristicCode</code> it is considered a "true" unit test.
 * 
 * @author Tom Bechtold
 * @version 2008-07-06
 * @see BinaryHistory
 */
@Test(description = "Binary history", groups = { "patient", "unit" })
public class TestBinaryHistory
{

	/**
	 * Random-number generator
	 */
	private final Random generator = new Random();

	/**
	 * Test empty history
	 */
	public void testEmptyHistory()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}

		// Ensure that the characteristics are not in the empty history
		BinaryHistory binaryHistory = new BinaryHistory();
		assert binaryHistory.isEmpty() : "History not empty " + binaryHistory;
		assert !binaryHistory.containsCharacteristic(new CharacteristicCode(code)) : "Characteristic " + code
				+ " contained in empty history " + binaryHistory;
		assert !binaryHistory.containsCharacteristic(new CharacteristicCode(otherCode)) : "Characteristic " + otherCode
				+ " contained in empty history " + binaryHistory;
	}

	/**
	 * Test adding an element
	 */
	public void testAddCharacteristic()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}

		// Ensure that the only characteristic in the history is the one added
		BinaryHistory binaryHistory = new BinaryHistory();
		binaryHistory.addCharacteristic(new CharacteristicCode(code));
		assert binaryHistory.containsCharacteristic(new CharacteristicCode(code)) : "Characteristic " + code
				+ " not found in " + binaryHistory;
		assert !binaryHistory.containsCharacteristic(new CharacteristicCode(otherCode)) : "Other characteristic "
				+ otherCode + " found in " + binaryHistory;
		assert binaryHistory.size() == 1 : "Number of elements " + binaryHistory.size() + " is not one in "
				+ binaryHistory;
		assert new CharacteristicCode(code)
				.equals(binaryHistory.toArray(new CharacteristicCode[binaryHistory.size()])[0]) : "First characteristic of history "
				+ binaryHistory + " is not " + code;
	}

	/**
	 * Test removing an element
	 */
	public void testRemoveCharacteristic()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}

		// Ensure that the characteristic was removed from the history
		BinaryHistory binaryHistory = new BinaryHistory();
		binaryHistory.addCharacteristic(new CharacteristicCode(code));
		binaryHistory.removeCharacteristic(new CharacteristicCode(code));
		assert !binaryHistory.containsCharacteristic(new CharacteristicCode(code)) : "Characteristic " + code
				+ " found in removed " + binaryHistory;
		assert !binaryHistory.containsCharacteristic(new CharacteristicCode(otherCode)) : "Other characteristic "
				+ otherCode + " found in removed " + binaryHistory;
		assert binaryHistory.isEmpty() : "Number of elements " + binaryHistory.size() + " is not zero in "
				+ binaryHistory;
	}
}
