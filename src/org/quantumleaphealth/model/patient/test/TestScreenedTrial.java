/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient.test;

import static org.quantumleaphealth.screen.test.TestUtils.GENERATOR;

import java.util.Date;

import org.quantumleaphealth.model.patient.ScreenedTrial;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.screen.test.TestUtils;
import org.testng.annotations.Test;

/**
 * Tests for the <code>ScreenedTrial</code>.
 * 
 * @author Tom Bechtold
 * @version 2008-12-17
 * @see ScreenedTrial
 */
@Test(description = "ScreenedTrial", groups = { "patient", "unit" })
public class TestScreenedTrial
{

	/**
	 * Test Id field
	 */
	public void testId()
	{
		// Ensure that the holder is empty
		ScreenedTrial screenedTrial = new ScreenedTrial();
		assert screenedTrial.getId() == null : "id not null " + screenedTrial.getId();
		// Test the set and get
		Long id = Long.valueOf(GENERATOR.nextLong());
		screenedTrial.setId(id);
		assert id.equals(screenedTrial.getId()) : "Id " + id + " not contained in screenedTrial "
				+ screenedTrial.getId();
	}

	/**
	 * Test informed field
	 */
	public void testInformed()
	{
		// Ensure that the holder is empty
		ScreenedTrial screenedTrial = new ScreenedTrial();
		assert screenedTrial.getInformed() == null : "Informed not null " + screenedTrial.getInformed();
		// Test the set and get
		Date date = new Date(Long.valueOf(Math.abs(GENERATOR.nextLong())));
		screenedTrial.setInformed(date);
		assert date.equals(screenedTrial.getInformed()) : "Informed " + screenedTrial.getInformed() + " not stored as "
				+ date;
	}

	/**
	 * Test lastModified and trialId fields using <code>setTrial()</code>
	 */
	public void testTrial()
	{
		// Test null initial values
		ScreenedTrial screenedTrial = new ScreenedTrial();
		assert screenedTrial.getTrialId() == null : "TrialId not null " + screenedTrial.getTrialId();
		assert screenedTrial.getLastModified() == null : "LastModified not null " + screenedTrial.getLastModified();
		// Test null parameter
		try
		{
			screenedTrial.setTrial(null);
			throw new AssertionError("Set trial to null");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		// Test null trialId
		Trial trial = new Trial();
		try
		{
			screenedTrial.setTrial(trial);
			throw new AssertionError("Set trialId to null");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		// Test setting trialId
		int[] integerIds = TestUtils.getUniqueIntegers(2);
		Long id1 = Long.valueOf(integerIds[0]);
		Long id2 = Long.valueOf(integerIds[1]);
		trial.setId(id1);
		screenedTrial.setTrial(trial);
		assert id1.equals(screenedTrial.getTrialId()) : "TrialId " + screenedTrial.getTrialId() + " not set to " + id1;
		// Test setting lastModified
		Date lastModified = new Date(id2.longValue());
		trial.setLastModified(lastModified);
		screenedTrial.setTrial(trial);
		assert lastModified.equals(screenedTrial.getLastModified()) : "LastModified " + screenedTrial.getLastModified()
				+ " not set to " + lastModified;
		// Test overwriting trial on non-persistent object
		trial = new Trial();
		trial.setId(id2);
		try
		{
			screenedTrial.setTrial(trial);
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
			throw new AssertionError(illegalArgumentException);
		}
		// Test overwriting trial on persistent object
		screenedTrial.setId(id2);
		trial = new Trial();
		trial.setId(id1);
		try
		{
			screenedTrial.setTrial(trial);
			throw new AssertionError("Overwrote trial on persisted instance");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
	}

	/**
	 * Test compareTo()
	 */
	public void testCompareTo()
	{
		// Test other unknown
		ScreenedTrial screenedTrial = new ScreenedTrial();
		assert screenedTrial.compareTo(null) < 0 : "Null is non-negative: " + screenedTrial.compareTo(null);
		// Test equality
		ScreenedTrial other = screenedTrial;
		assert screenedTrial.compareTo(other) == 0 : "Same object is not 0: " + screenedTrial.compareTo(other);
		// Compare class ids if both trial ids are unknown
		other = new ScreenedTrial();
		assert screenedTrial.compareTo(other) == 0 : "Null ids is not 0: " + screenedTrial.compareTo(other);
		int[] integerIds = TestUtils.getUniqueIntegers(2);
		Long id1 = Long.valueOf(integerIds[0]);
		Long id2 = Long.valueOf(integerIds[1]);
		screenedTrial.setId(id1);
		assert screenedTrial.compareTo(other) < 0 : "Null id is non-negative: " + screenedTrial.compareTo(other);
		screenedTrial.setId(null);
		other.setId(id2);
		assert screenedTrial.compareTo(other) > 0 : "Null id is non-positive: " + screenedTrial.compareTo(other);
		screenedTrial.setId(id1);
		assert Math.signum(screenedTrial.compareTo(other)) == Math.signum(id1.compareTo(id2)) : "id comparison "
				+ id1.compareTo(id2) + " is not class comparison " + screenedTrial.compareTo(other);
		// Compare one unknown trial id
		screenedTrial.setId(null);
		other.setId(null);
		Trial trial = new Trial();
		trial.setId(id1);
		screenedTrial.setTrial(trial);
		assert screenedTrial.compareTo(other) < 0 : "Null trial id is non-negative: " + screenedTrial.compareTo(other);
		// Compare same trial ids
		other.setTrial(trial);
		assert screenedTrial.compareTo(other) == 0 : "Same trial ids is not 0: " + screenedTrial.compareTo(other);
		// Compare different trial ids
		trial = new Trial();
		trial.setId(id2);
		other.setTrial(trial);
		assert Math.signum(screenedTrial.compareTo(other)) == Math.signum(id1.compareTo(id2)) : "id comparison "
				+ id1.compareTo(id2) + " is not trial id comparison " + screenedTrial.compareTo(other);
	}

	/**
	 * Test equals()
	 */
	public void testEquals()
	{
		// Different class is not equal
		ScreenedTrial screenedTrial = new ScreenedTrial();
		assert !screenedTrial.equals(null) : "Null is equal";
		assert !screenedTrial.equals(new Object()) : "Other class is equal";
		// Same object is equals
		ScreenedTrial other = screenedTrial;
		assert screenedTrial.equals(other) : "Same object is not equals";
		// Null ids and null trialids is not equal
		other = new ScreenedTrial();
		assert !screenedTrial.equals(other) : "Null ids and null trialIds are equal";
		// One trialid is not equal
		Trial trial = new Trial();
		trial.setId(Long.valueOf(GENERATOR.nextLong()));
		screenedTrial.setTrial(trial);
		assert !screenedTrial.equals(other) : "One null trialIds is equal";
		// Equal trialids with null ids are equal
		other.setTrial(trial);
		assert screenedTrial.equals(other) : "Equal trialIds are equal";
		// One null id is not
		Long id = Long.valueOf(GENERATOR.nextLong());
		screenedTrial.setId(id);
		assert !screenedTrial.equals(other) : "One null id is equal";
		// Equal ids are equal
		other.setId(id);
		assert screenedTrial.equals(other) : "Equal ids are not equal";
	}

	/**
	 * Test hashcode() is equal to the id's hashcode
	 */
	public void testHashcode()
	{
		ScreenedTrial screenedTrial = new ScreenedTrial();
		Long id = Long.valueOf(GENERATOR.nextLong());
		screenedTrial.setId(id);
		assert id.hashCode() == screenedTrial.hashCode() : "Hashcode " + screenedTrial.hashCode()
				+ " not equal to its id's " + id.hashCode();
	}
}
