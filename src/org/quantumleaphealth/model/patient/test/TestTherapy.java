/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient.test;

import java.util.Arrays;
import java.util.Random;

import org.quantumleaphealth.model.patient.Therapy;
import org.quantumleaphealth.model.patient.YearMonth;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.testng.annotations.Test;

/**
 * Tests for the <code>Therapy</code>. As this class only references the
 * <code>CharacteristicCode</code> it is considered a "true" unit test.
 * 
 * @author Tom Bechtold
 * @version 2008-07-06
 * @see Therapy
 */
@Test(description = "Therapy", groups = { "patient", "unit" })
public class TestTherapy
{

	/**
	 * Random-number generator
	 */
	private final Random generator = new Random();

	/**
	 * Test agents field
	 */
	public void testAgents()
	{
		int code = generator.nextInt();
		// Ensure that the holder is empty
		Therapy therapy = new Therapy();
		assert therapy.getAgents() == null : "Agents not null " + Arrays.toString(therapy.getAgents());
		// Test the set and get
		therapy.setAgents(new CharacteristicCode[] { new CharacteristicCode(code) });
		assert (therapy.getAgents() != null) && (therapy.getAgents().length == 1)
				&& new CharacteristicCode(code).equals(therapy.getAgents()[0]) : "Characteristic " + code
				+ " not contained in therapy agents " + Arrays.toString(therapy.getAgents());
	}

	/**
	 * Test type field
	 */
	public void testType()
	{
		int code = generator.nextInt();
		// Ensure that the holder is empty
		Therapy therapy = new Therapy();
		assert therapy.getType() == null : "Type not null " + therapy.getType();
		// Test the set and get
		therapy.setType(new CharacteristicCode(code));
		assert new CharacteristicCode(code).equals(therapy.getType()) : "Characteristic " + code
				+ " not contained in therapy " + therapy;
	}

	/**
	 * Test setting field
	 */
	public void testSetting()
	{
		int code = generator.nextInt();
		// Ensure that the holder is empty
		Therapy therapy = new Therapy();
		assert therapy.getSetting() == null : "Setting not null " + therapy.getSetting();
		// Test the set and get
		therapy.setSetting(new CharacteristicCode(code));
		assert new CharacteristicCode(code).equals(therapy.getSetting()) : "Characteristic " + code
				+ " not contained in therapy " + therapy;
	}

	/**
	 * Test status field
	 */
	public void testStatus()
	{
		int code = generator.nextInt();
		// Ensure that the holder is empty
		Therapy therapy = new Therapy();
		assert therapy.getStatus() == null : "Status not null " + therapy.getStatus();
		// Test the set and get
		therapy.setStatus(new CharacteristicCode(code));
		assert new CharacteristicCode(code).equals(therapy.getStatus()) : "Characteristic " + code
				+ " not contained in therapy " + therapy;
	}

	/**
	 * Test incomplete field
	 */
	public void testIncomplete()
	{
		int code = generator.nextInt();
		// Ensure that the holder is empty
		Therapy therapy = new Therapy();
		assert therapy.getIncomplete() == null : "Incomplete not null " + therapy.getIncomplete();
		// Test the set and get
		therapy.setIncomplete(new CharacteristicCode(code));
		assert new CharacteristicCode(code).equals(therapy.getIncomplete()) : "Characteristic " + code
				+ " not contained in therapy " + therapy;
	}

	/**
	 * Test progression field
	 */
	public void testProgression()
	{
		// Ensure that the holder is empty
		Therapy therapy = new Therapy();
		assert !therapy.isProgression() : "Progression set in therapy " + therapy;
		// Test the set and get
		therapy.setProgression(true);
		assert therapy.isProgression() : "Progression not set in therapy " + therapy;
	}

	/**
	 * Test started field
	 */
	public void testStarted()
	{
		// Ensure that the holder is non-null
		Therapy therapy = new Therapy();
		assert therapy.getStarted() != null : "Started is null " + therapy;
		// Test the set and get
		YearMonth yearMonth = new YearMonth();
		therapy.setStarted(yearMonth);
		assert yearMonth == therapy.getStarted() : "Started " + yearMonth + " not stored in therapy " + therapy;
	}

	/**
	 * Test completed field
	 */
	public void testCompleted()
	{
		// Ensure that the holder is non-null
		Therapy therapy = new Therapy();
		assert therapy.getCompleted() != null : "Completed is null " + therapy;
		// Test the set and get
		YearMonth yearMonth = new YearMonth();
		therapy.setCompleted(yearMonth);
		assert yearMonth == therapy.getCompleted() : "Completed " + yearMonth + " not stored in therapy " + therapy;
	}

	/**
	 * Test length field
	 */
	public void testLength()
	{
		byte length = (byte) (generator.nextInt() & Byte.MAX_VALUE);
		// Ensure that the holder is empty
		Therapy therapy = new Therapy();
		assert therapy.getLength() == 0 : "Length set in therapy " + therapy;
		// Test the set and get
		therapy.setLength(length);
		assert therapy.getLength() == length : "Length not set to " + length + " in therapy " + therapy;
	}
}
