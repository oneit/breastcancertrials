package org.quantumleaphealth.model.patient.test;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREASTMETASTASIS_BRAIN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_BRAIN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_SPINALCORD;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PROGRESSING;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.quantumleaphealth.action.AvatarHistoryConfiguration;
import org.quantumleaphealth.action.PatientInterfaceAction;
import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


/**
 * Test BCT-698 Changes:  Merge spinal cancer with Brain.  Remove from other.  This test should be run
 * individually as the avatar configuration may change again, making this test irrelevant.
 * 
 * @author wgweis
 * @version 06/30/2014
 */
@Test(description = "Avatar Configuration Changes")
public class TestBCT298AvatarConfiguration
{
	AvatarHistoryConfiguration avatarHistoryConfiguration;
	PatientInterfaceAction patientInterfaceAction;
	
	@BeforeTest
	public void beforeTest()
	{
		patientInterfaceAction = new PatientInterfaceAction();
		
		UserPatient userPatient = new UserPatient();
		userPatient.setPatientHistory(new PatientHistory());
		patientInterfaceAction.setPatient(userPatient);
		
		avatarHistoryConfiguration = new AvatarHistoryConfiguration(patientInterfaceAction);
	}
	
	/**
	 * Test empty history
	 * @throws NoSuchMethodException 
	 * @throws SecurityException 
	 * @throws InvocationTargetException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	@Test()
	public void testNoSpinalWhenOther() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException
	{
		Method privateVoidMethod = AvatarHistoryConfiguration.class.getDeclaredMethod("setAllOtherEvidenceOfDisease", (Class<?>[]) null);

		privateVoidMethod.setAccessible(true);
		privateVoidMethod.invoke(avatarHistoryConfiguration, (Object[]) null);

		PatientHistory ph = patientInterfaceAction.getPatient().getPatientHistory();
		assert !ph.containsAttribute(DIAGNOSISAREA, DIAGNOSISAREA_SPINALCORD) : "Contains Spinal Chord Characteristic Code " + DIAGNOSISAREA_SPINALCORD;
		assert !ph.containsAttribute(DIAGNOSISAREA, DIAGNOSISAREA_BRAIN) : "Contains Active Brain Characteristic Code " + DIAGNOSISAREA_BRAIN;
	}
	
	@Test()
	public void testSpinalWhenBrain() throws SecurityException, NoSuchMethodException, IllegalArgumentException, IllegalAccessException, InvocationTargetException
	{
		Method privateVoidMethod = AvatarHistoryConfiguration.class.getDeclaredMethod("setBrainSpinalDiagnosisArea", (Class<?>[]) null);

		privateVoidMethod.setAccessible(true);
		privateVoidMethod.invoke(avatarHistoryConfiguration, (Object[]) null);

		PatientHistory ph = patientInterfaceAction.getPatient().getPatientHistory();
		assert ph.containsAttribute(DIAGNOSISAREA, DIAGNOSISAREA_SPINALCORD) : "Does not contain Spinal Chord Characteristic Code " + DIAGNOSISAREA_SPINALCORD;
		
		boolean hasBrainDiagnosis = false;
		for (Diagnosis d: ph.getDiagnoses())
		{
			if (d.getValueHistory().containsKey(BREASTMETASTASIS_BRAIN) && d.getValueHistory().getValue(BREASTMETASTASIS_BRAIN).equals(PROGRESSING))
			{
				hasBrainDiagnosis = true;
				break;
			}
		}
		assert  hasBrainDiagnosis: "Does not contain Active Brain Characteristic Code " + DIAGNOSISAREA_BRAIN;
	}
}
