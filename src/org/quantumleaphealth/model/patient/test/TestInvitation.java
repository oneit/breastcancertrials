/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient.test;

import java.util.Date;

import org.quantumleaphealth.model.patient.Invitation;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.Site;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.TrialSite;
import static org.quantumleaphealth.screen.test.TestUtils.GENERATOR;
import org.testng.annotations.Test;

/**
 * Tests for the <code>Invitation</code>. This class tests the initial property
 * values of a newly constructed object and the getter/setter methods on
 * property values.
 * 
 * @author Tom Bechtold
 * @version 2009-07-07
 * @see Invitation
 */
@Test(description = "Invitation", groups = { "patient", "unit" })
public class TestInvitation
{

	/**
	 * Test Id field
	 */
	public void testId()
	{
		// Ensure that the holder is empty
		Invitation invitation = new Invitation();
		assert invitation.getId() == null : "id not null " + invitation.getId();
		// Test the set and get
		Long id = Long.valueOf(GENERATOR.nextLong());
		invitation.setId(id);
		assert id.equals(invitation.getId()) : "Id " + id + " not contained in invitation " + invitation;
	}

	/**
	 * Test UserPatient field
	 */
	public void testUserPatient()
	{
		// Ensure that the holder is empty
		Invitation invitation = new Invitation();
		assert invitation.getUserPatient() == null : "UserPatient not null " + invitation.getUserPatient();
		// Test the set and get
		UserPatient userPatient = new UserPatient();
		invitation.setUserPatient(userPatient);
		assert userPatient == invitation.getUserPatient() : "UserPatient " + userPatient
				+ " not contained in invitation " + invitation;
	}

	/**
	 * Test TrialSite field
	 */
	public void testTrialSite()
	{
		// Ensure that the holder is empty
		Invitation invitation = new Invitation();
		assert invitation.getTrialSite() == null : "TrialSite not null " + invitation.getTrialSite();
		assert invitation.getTrialId() == null : "TrialId not null " + invitation.getTrialId();
		assert invitation.getSiteId() == null : "SiteId not null " + invitation.getSiteId();
		// Test the set and get with composite primary id
		Long trialid = Long.valueOf(GENERATOR.nextLong());
		Long siteid = Long.valueOf(GENERATOR.nextLong());
		TrialSite trialSite = new TrialSite();
		Site site = new Site();
		site.setId(siteid);
		trialSite.setSite(site);
		Trial trial = new Trial();
		trial.setId(trialid);
		trialSite.setTrial(trial);
		invitation.setTrialSite(trialSite);
		assert trialSite == invitation.getTrialSite() : "TrialSite " + trialSite + " not contained in invitation "
				+ invitation;
		assert trial == invitation.getTrialSite().getTrial() : "Trial " + trial + " not contained in invitation: "
				+ invitation.getTrialSite().getTrial();
		assert site == invitation.getTrialSite().getSite() : "Site " + site + " not contained in invitation: "
				+ invitation.getTrialSite().getSite();
		// Ensure that the identifiers are set when the trialSite is set
		assert trialid.equals(invitation.getTrialId()) : "TrialId " + trialid + " not contained in invitation: "
				+ invitation.getTrialId();
		assert siteid.equals(invitation.getSiteId()) : "SiteId " + siteid + " not contained in invitation: "
				+ invitation.getSiteId();
	}

	/**
	 * Test PatientName field
	 */
	public void testPatientName()
	{
		// Ensure that the holder is empty
		Invitation invitation = new Invitation();
		assert invitation.getPatientName() == null : "patientName not null " + invitation.getPatientName();
		// Test the set and get
		final String string1 = "foo";
		final String string2 = "bar";
		invitation.setPatientName(string1);
		assert string1.equals(invitation.getPatientName()) : "patientName " + invitation.getPatientName()
				+ " not stored as " + string1;
		invitation.setPatientName(string2);
		assert string2.equals(invitation.getPatientName()) : "patientName " + invitation.getPatientName()
				+ " not stored as " + string2;
	}

	/**
	 * Test PatientContact field
	 */
	public void testPatientContact()
	{
		// Ensure that the holder is empty
		Invitation invitation = new Invitation();
		assert invitation.getPatientContact() == null : "patientContact not null " + invitation.getPatientContact();
		// Test the set and get
		final String string1 = "foo";
		final String string2 = "bar";
		invitation.setPatientContact(string1);
		assert string1.equals(invitation.getPatientContact()) : "patientContact " + invitation.getPatientContact()
				+ " not stored as " + string1;
		invitation.setPatientContact(string2);
		assert string2.equals(invitation.getPatientContact()) : "patientContact " + invitation.getPatientContact()
				+ " not stored as " + string2;
	}

	/**
	 * Test Message field
	 */
	public void testMessage()
	{
		// Ensure that the holder is empty
		Invitation invitation = new Invitation();
		assert invitation.getMessage() == null : "message not null " + invitation.getMessage();
		// Test the set and get
		final String string1 = "foo";
		final String string2 = "bar";
		invitation.setMessage(string1);
		assert string1.equals(invitation.getMessage()) : "message " + invitation.getMessage() + " not stored as "
				+ string1;
		invitation.setMessage(string2);
		assert string2.equals(invitation.getMessage()) : "message " + invitation.getMessage() + " not stored as "
				+ string2;
	}

	/**
	 * Test created field
	 */
	public void testCreated()
	{
		// Ensure that the holder is not empty
		assert new Invitation().getCreated() != null : "created is null ";
	}

	/**
	 * Test sent field
	 */
	public void testSent()
	{
		// Ensure that the holder is empty
		Invitation invitation = new Invitation();
		assert invitation.getSent() == null : "sent not null " + invitation.getSent();
		// Test the set and get
		Date date = new Date(Long.valueOf(Math.abs(GENERATOR.nextLong())));
		invitation.setSent(date);
		assert date.equals(invitation.getSent()) : "Sent " + invitation.getSent() + " not stored as " + date;
	}

	/**
	 * Test viewed field
	 */
	public void testViewed()
	{
		// Ensure that the holder is empty
		Invitation invitation = new Invitation();
		assert invitation.getViewed() == null : "viewed not null " + invitation.getViewed();
		// Test the set and get
		Date date = new Date(Long.valueOf(Math.abs(GENERATOR.nextLong())));
		invitation.setViewed(date);
		assert date.equals(invitation.getViewed()) : "viewed " + invitation.getViewed() + " not stored as " + date;
	}

	/**
	 * Test LastReminder field
	 */
	public void testLastReminder()
	{
		// Ensure that the holder is empty
		Invitation invitation = new Invitation();
		assert invitation.getLastReminder() == null : "lastReminder not null " + invitation.getLastReminder();
		// Test the set and get
		Date date = new Date(Long.valueOf(Math.abs(GENERATOR.nextLong())));
		invitation.setLastReminder(date);
		assert date.equals(invitation.getLastReminder()) : "lastReminder " + invitation.getLastReminder()
				+ " not stored as " + date;
	}

	/**
	 * Test DispositionDate field
	 */
	public void testDispositionDate()
	{
		// Ensure that the holder is empty
		Invitation invitation = new Invitation();
		assert invitation.getDispositionDate() == null : "dispositionDate not null " + invitation.getDispositionDate();
		// Test the set and get
		Date date = new Date(Long.valueOf(Math.abs(GENERATOR.nextLong())));
		invitation.setDispositionDate(date);
		assert date.equals(invitation.getDispositionDate()) : "dispositionDate " + invitation.getDispositionDate()
				+ " not stored as " + date;
	}
}
