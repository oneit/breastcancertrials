/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient.test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.quantumleaphealth.model.patient.BinaryHistory;
import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.ShortHistory;
import org.quantumleaphealth.model.patient.ValueHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.testng.annotations.Test;

/**
 * Tests for the <code>Diagnosis</code>. This composite test class references
 * the following classes:
 * <ul>
 * <li><code>CharacteristicCode</code></li>
 * <li><code>BinaryHistory</code></li>
 * <li><code>ValueHistory</code></li>
 * <li><code>ShortHistory</code></li>
 * </ul>
 * 
 * @author Tom Bechtold
 * @version 2008-07-06
 * @see Diagnosis
 */
@Test(description = "Diagnosis", groups = { "patient", "composite" })
public class TestDiagnosis
{

	/**
	 * Random-number generator
	 */
	private final Random generator = new Random();

	/**
	 * @return three unique integers
	 */
	private int[] getThreeUniqueIntegers()
	{
		int[] unique = new int[3];
		while ((unique[0] == unique[1]) || (unique[0] == unique[2]) || (unique[1] == unique[2]))
		{
			unique[0] = generator.nextInt();
			unique[1] = generator.nextInt();
			unique[2] = generator.nextInt();
		}
		return unique;
	}

	/**
	 * Test binary history
	 */
	public void testBinaryHistory()
	{
		// Ensure non-null and empty
		Diagnosis diagnosis = new Diagnosis();
		BinaryHistory binaryHistory = diagnosis.getBinaryHistory();
		assert (binaryHistory != null) && binaryHistory.isEmpty() : "New binary history is not empty " + binaryHistory;

		// Test set and get
		binaryHistory = new BinaryHistory();
		int code = generator.nextInt();
		binaryHistory.addCharacteristic(new CharacteristicCode(code));
		diagnosis.setBinaryHistory(binaryHistory);
		assert (binaryHistory == diagnosis.getBinaryHistory()) : "Retrieved binary history "
				+ diagnosis.getBinaryHistory() + " is not the one stored " + binaryHistory;
	}

	/**
	 * Test value history
	 */
	public void testValueHistory()
	{
		// Ensure non-null and empty
		Diagnosis diagnosis = new Diagnosis();
		ValueHistory valueHistory = diagnosis.getValueHistory();
		assert (valueHistory != null) && valueHistory.isEmpty() : "New value history is not empty " + valueHistory;

		// Test set and get
		valueHistory = new ValueHistory();
		int[] unique = getThreeUniqueIntegers();
		valueHistory.setValue(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1]));
		diagnosis.setValueHistory(valueHistory);
		assert (valueHistory == diagnosis.getValueHistory()) : "Retrieved value history " + diagnosis.getValueHistory()
				+ " is not the one stored " + valueHistory;
	}

	/**
	 * Test short history
	 */
	public void testShortHistory()
	{
		// Ensure non-null and empty
		Diagnosis diagnosis = new Diagnosis();
		ShortHistory shortHistory = diagnosis.getShortHistory();
		assert (shortHistory != null) && shortHistory.isEmpty() : "New short history is not empty " + shortHistory;

		// Test set and get
		shortHistory = new ShortHistory();
		int[] unique = getThreeUniqueIntegers();
		shortHistory.setShort(new CharacteristicCode(unique[0]), Short.valueOf((short) (unique[1] & Short.MAX_VALUE)));
		diagnosis.setShortHistory(shortHistory);
		assert (shortHistory == diagnosis.getShortHistory()) : "Retrieved short history " + diagnosis.getShortHistory()
				+ " is not the one stored " + shortHistory;
	}

	/**
	 * Test attribute characteristics
	 */
	public void testAttributeCharacteristics()
	{
		// Ensure non-null and empty
		Diagnosis diagnosis = new Diagnosis();
		Map<CharacteristicCode, Set<CharacteristicCode>> attributeCharacteristics = diagnosis
				.getAttributeCharacteristics();
		assert (attributeCharacteristics != null) && attributeCharacteristics.isEmpty() : "New attribute characteristics is not empty "
				+ attributeCharacteristics;

		// Test set and get
		attributeCharacteristics = new HashMap<CharacteristicCode, Set<CharacteristicCode>>(1, 1.0f);
		int[] unique = getThreeUniqueIntegers();
		attributeCharacteristics.put(new CharacteristicCode(unique[0]), new LinkedHashSet<CharacteristicCode>(Arrays
				.asList(new CharacteristicCode(unique[1]), new CharacteristicCode(unique[2]))));
		diagnosis.setAttributeCharacteristics(attributeCharacteristics);
		assert (attributeCharacteristics == diagnosis.getAttributeCharacteristics()) : "Retrieved attribute characteristics "
				+ diagnosis.getAttributeCharacteristics() + " is not the one stored " + attributeCharacteristics;
	}

	/**
	 * Test adding and removing attributes to characteristics
	 */
	public void testAddRemoveAttribute()
	{
		int[] unique = getThreeUniqueIntegers();

		// Ensure that the attribute is not in the fresh history
		Diagnosis diagnosis = new Diagnosis();
		assert !diagnosis.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1])) : "New attribute characteristics "
				+ diagnosis.getAttributeCharacteristics()
				+ " contains "
				+ unique[1]
				+ " attribute for characteristic "
				+ unique[0];

		// Ensure that the added attribute shows up in the history
		diagnosis.addAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1]));
		assert diagnosis.getAttributeCharacteristics().size() == 1 : "After adding first attribute the map size "
				+ diagnosis.getAttributeCharacteristics().size() + " is not 1";
		assert diagnosis.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1])) : "Attribute characteristic "
				+ diagnosis.getAttributeCharacteristics()
				+ " does not contain "
				+ unique[1]
				+ " attribute for characteristic " + unique[0];

		// Add a second attribute to the same characteristic
		diagnosis.addAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[2]));
		assert diagnosis.getAttributeCharacteristics().size() == 1 : "After adding second attribute the map size "
				+ diagnosis.getAttributeCharacteristics().size() + " is not 1";
		assert diagnosis.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1])) : "Attribute characteristic "
				+ diagnosis.getAttributeCharacteristics()
				+ " does not contain "
				+ unique[1]
				+ " attribute for characteristic " + unique[0];
		assert diagnosis.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[2])) : "Attribute characteristic "
				+ diagnosis.getAttributeCharacteristics()
				+ " does not contain "
				+ unique[2]
				+ " attribute for characteristic " + unique[0];
		// Verify the set stored is correct
		Set<CharacteristicCode> attributes = diagnosis.getAttributeCharacteristics().get(
				new CharacteristicCode(unique[0]));
		assert attributes.containsAll(Arrays.asList(new CharacteristicCode(unique[1]),
				new CharacteristicCode(unique[2]))) : "Attribute set " + attributes
				+ " does not contain both attributes " + unique[1] + " and " + unique[2];

		// Adding a duplicate attribute should not affect anything
		diagnosis.addAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[2]));
		assert diagnosis.getAttributeCharacteristics().size() == 1 : "After adding duplicate attribute the map size "
				+ diagnosis.getAttributeCharacteristics().size() + " is not 1";
		assert diagnosis.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1])) : "Attribute characteristic "
				+ diagnosis.getAttributeCharacteristics()
				+ " does not contain "
				+ unique[1]
				+ " attribute for characteristic " + unique[0];
		assert diagnosis.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[2])) : "Attribute characteristic "
				+ diagnosis.getAttributeCharacteristics()
				+ " does not contain "
				+ unique[2]
				+ " attribute for characteristic " + unique[0];
		attributes = diagnosis.getAttributeCharacteristics().get(new CharacteristicCode(unique[0]));
		assert attributes.containsAll(Arrays.asList(new CharacteristicCode(unique[1]),
				new CharacteristicCode(unique[2]))) : "Attribute set " + attributes
				+ " does not contain both attributes " + unique[1] + " and " + unique[2];

		// Remove latter attribute
		diagnosis.removeAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[2]));
		assert diagnosis.getAttributeCharacteristics().size() == 1 : "After removing first attribute the map size "
				+ diagnosis.getAttributeCharacteristics().size() + " is not 1";
		assert diagnosis.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1])) : "Attribute characteristic "
				+ diagnosis.getAttributeCharacteristics()
				+ " does not contain "
				+ unique[1]
				+ " attribute for characteristic " + unique[0];
		assert !diagnosis.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[2])) : "Attribute characteristic "
				+ diagnosis.getAttributeCharacteristics()
				+ " contains "
				+ unique[2]
				+ " attribute for characteristic "
				+ unique[0];
		// Verify the set stored only has one left
		attributes = diagnosis.getAttributeCharacteristics().get(new CharacteristicCode(unique[0]));
		assert attributes.containsAll(Arrays.asList(new CharacteristicCode(unique[1]))) : "Attribute set " + attributes
				+ " does not contain attribute " + unique[1];

		// Remove former attribute so map is empty
		diagnosis.removeAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1]));
		assert diagnosis.getAttributeCharacteristics().size() == 0 : "After removing second attribute the map size "
				+ diagnosis.getAttributeCharacteristics().size() + " is not 0";
		assert !diagnosis.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[1])) : "Attribute characteristic "
				+ diagnosis.getAttributeCharacteristics()
				+ " contains "
				+ unique[1]
				+ " attribute for characteristic "
				+ unique[0];
		assert !diagnosis.containsAttribute(new CharacteristicCode(unique[0]), new CharacteristicCode(unique[2])) : "Attribute characteristic "
				+ diagnosis.getAttributeCharacteristics()
				+ " contains "
				+ unique[2]
				+ " attribute for characteristic "
				+ unique[0];
		// Verify the set stored is now null
		attributes = diagnosis.getAttributeCharacteristics().get(new CharacteristicCode(unique[0]));
		assert attributes == null : "Attribute set after removal is not empty for characteristic " + unique[0];
	}
}
