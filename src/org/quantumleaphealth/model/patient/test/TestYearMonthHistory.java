/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient.test;

import java.util.Random;

import org.quantumleaphealth.model.patient.YearMonthHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.testng.annotations.Test;

/**
 * Tests for the <code>YearMonthHistory</code>. As this class only references
 * the <code>CharacteristicCode</code> it is considered a "true" unit test.
 * 
 * @author Tom Bechtold
 * @version 2008-07-06
 * @see YearMonthHistory
 */
@Test(description = "YearMonth history", groups = { "patient", "unit" })
public class TestYearMonthHistory
{

	/**
	 * Random-number generator
	 */
	private final Random generator = new Random();

	/**
	 * Test empty history
	 */
	public void testEmptyHistory()
	{
		int code = generator.nextInt();
		// Ensure that the characteristic is not in the empty history
		YearMonthHistory yearMonthHistory = new YearMonthHistory();
		assert yearMonthHistory.isEmpty() : "History not empty " + yearMonthHistory;
		assert yearMonthHistory.getShort(new CharacteristicCode(code)) == null : "Characteristic " + code
				+ " contained in empty history " + yearMonthHistory;
	}

	/**
	 * Test adding an element
	 */
	public void testAddShortCharacteristic()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}

		// Ensure that the only characteristic in the history is the one added
		YearMonthHistory yearMonthHistory = new YearMonthHistory();
		Short value = Short.valueOf((short) (otherCode & Short.MAX_VALUE));
		yearMonthHistory.setShort(new CharacteristicCode(code), value);
		assert yearMonthHistory.getShort(new CharacteristicCode(code)).equals(value) : "Characteristic " + code
				+ " not stored in " + yearMonthHistory;
		assert yearMonthHistory.getShort(new CharacteristicCode(value.intValue())) == null : "Other characteristic "
				+ value + " found in " + yearMonthHistory;
		assert yearMonthHistory.size() == 1 : "Number of elements " + yearMonthHistory.size() + " is not one in "
				+ yearMonthHistory;
	}

	/**
	 * Test removing an element
	 */
	public void testRemoveShortCharacteristic()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}
		Short value = Short.valueOf((short) (otherCode & Short.MAX_VALUE));

		// Ensure that the characteristic was removed from the history
		YearMonthHistory yearMonthHistory = new YearMonthHistory();
		yearMonthHistory.setShort(new CharacteristicCode(code), value);
		yearMonthHistory.setShort(new CharacteristicCode(code), null);
		assert yearMonthHistory.getShort(new CharacteristicCode(code)) == null : "Characteristic " + code
				+ " found in removed " + yearMonthHistory;
		assert yearMonthHistory.getShort(new CharacteristicCode(otherCode)) == null : "Other characteristic "
				+ otherCode + " found in removed " + yearMonthHistory;
		assert yearMonthHistory.isEmpty() : "Number of elements " + yearMonthHistory.size() + " is not zero in "
				+ yearMonthHistory;
	}

	/**
	 * Test adding an element
	 */
	public void testAddByteCharacteristic()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}

		// Ensure that the only characteristic in the history is the one added
		YearMonthHistory yearMonthHistory = new YearMonthHistory();
		byte value = (byte) (otherCode & Byte.MAX_VALUE);
		yearMonthHistory.setByte(new CharacteristicCode(code), value);
		assert yearMonthHistory.getByte(new CharacteristicCode(code)) == value : "Characteristic " + code
				+ " not stored in " + yearMonthHistory;
		assert yearMonthHistory.getByte(new CharacteristicCode(otherCode)) == 0 : "Other characteristic " + otherCode
				+ " found in " + yearMonthHistory;
		assert yearMonthHistory.size() == 1 : "Number of elements " + yearMonthHistory.size() + " is not one in "
				+ yearMonthHistory;
	}

	/**
	 * Test removing an element
	 */
	public void testRemoveByteCharacteristic()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}
		byte value = (byte) (otherCode & Byte.MAX_VALUE);

		// Ensure that the characteristic was removed from the history
		YearMonthHistory yearMonthHistory = new YearMonthHistory();
		yearMonthHistory.setByte(new CharacteristicCode(code), value);
		yearMonthHistory.setByte(new CharacteristicCode(code), (byte) 0);
		assert yearMonthHistory.getByte(new CharacteristicCode(code)) == 0 : "Characteristic " + code
				+ " found in removed " + yearMonthHistory;
		assert yearMonthHistory.getByte(new CharacteristicCode(otherCode)) == 0 : "Other characteristic " + otherCode
				+ " found in removed " + yearMonthHistory;
		assert yearMonthHistory.isEmpty() : "Number of elements " + yearMonthHistory.size() + " is not zero in "
				+ yearMonthHistory;
	}
}
