/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient.test;

import java.util.Calendar;
import java.util.Random;

import org.quantumleaphealth.model.patient.YearMonth;
import org.testng.annotations.Test;

/**
 * Tests for the <code>YearMonth</code>.
 * 
 * @author Tom Bechtold
 * @version 2008-07-06
 * @see YearMonth
 */
@Test(description = "YearMonth", groups = { "patient", "unit" })
public class TestYearMonth
{

	/**
	 * Random-number generator
	 */
	private final Random generator = new Random();

	/**
	 * Test kind field
	 */
	public void testYear()
	{
		Short year = Short.valueOf((short) generator.nextInt(Short.MAX_VALUE));
		// Ensure that the holder is empty
		YearMonth yearMonth = new YearMonth();
		assert yearMonth.getYear() == null : "Year not null " + yearMonth.getYear();
		// Test the set and get
		yearMonth.setYear(year);
		assert year.equals(yearMonth.getYear()) : "Year " + year + " not contained in yearMonth " + yearMonth;
	}

	/**
	 * Test type field
	 */
	public void testMonth()
	{
		byte month = (byte) generator.nextInt(Byte.MAX_VALUE);
		// Ensure that the holder is empty
		YearMonth yearMonth = new YearMonth();
		assert yearMonth.getMonth() == 0 : "Month not zero " + yearMonth.getMonth();
		// Test the set and get
		yearMonth.setMonth(month);
		assert month == yearMonth.getMonth() : "Month " + month + " not contained in yearMonth " + yearMonth;
	}

	/**
	 * Test setTime
	 */
	public void testSetTime()
	{
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH) + 1;
		// Ensure that the dates are set correctly
		YearMonth yearMonth = new YearMonth();
		yearMonth.setTime(System.currentTimeMillis());
		assert yearMonth.getYear().intValue() == year : "Year not set to " + year + " in " + yearMonth;
		assert yearMonth.getMonth() == month : "Month not set to " + month + " in " + yearMonth;
	}

	/**
	 * Test isEarlier than null
	 */
	public void testIsEarlierNull()
	{
		// Ensure empty to null/empty comparisons are false
		YearMonth yearMonth = new YearMonth();
		assert !yearMonth.isEarlier(null) : "Empty yearMonth earlier than null";
		assert !yearMonth.isEarlier(new YearMonth()) : "Empty yearMonth earlier than other empty";

		// Ensure valid to null/empty comparisons are false
		Short year = Short.valueOf((short) (generator.nextInt(Short.MAX_VALUE)));
		byte month = (byte) (generator.nextInt(12) + 1);
		yearMonth.setYear(year);
		yearMonth.setMonth(month);
		assert !yearMonth.isEarlier(null) : "Valid yearMonth earlier than null";
		assert !yearMonth.isEarlier(new YearMonth()) : "Valid yearMonth earlier than other empty";
	}

	/**
	 * Test isEarlier than same
	 */
	public void testIsEarlierSame()
	{
		Short year = Short.valueOf((short) (generator.nextInt(Short.MAX_VALUE)));
		byte month = (byte) (generator.nextInt(12) + 1);
		// Ensure empty to null/empty comparisons are false
		YearMonth yearMonth = new YearMonth();
		yearMonth.setYear(year);
		yearMonth.setMonth(month);
		YearMonth other = new YearMonth();
		other.setYear(year);
		other.setMonth(month);
		assert !yearMonth.isEarlier(other) : "YearMonth earlier than same";
	}

	/**
	 * Test isEarlier
	 */
	public void testIsLater()
	{
		Short year = Short.valueOf((short) (generator.nextInt(Short.MAX_VALUE)));
		byte month = (byte) (generator.nextInt(12) + 1);
		// Ensure empty to null/empty comparisons are false
		YearMonth yearMonth = new YearMonth();
		yearMonth.setYear(year);
		yearMonth.setMonth(month);
		// Roll other earlier by one month
		YearMonth other = new YearMonth();
		other.setYear(year);
		if (month == 1)
		{
			other.setMonth((byte) 12);
			other.setYear((short) (year.intValue() - 1));
		}
		else
			other.setMonth((byte) (month - 1));
		assert !yearMonth.isEarlier(other) : "YearMonth " + yearMonth + " is earlier than " + other;
	}

	/**
	 * Test isLater
	 */
	public void testIsEarlier()
	{
		Short year = Short.valueOf((short) (generator.nextInt(Short.MAX_VALUE)));
		byte month = (byte) (generator.nextInt(12) + 1);
		// Ensure empty to null/empty comparisons are false
		YearMonth yearMonth = new YearMonth();
		yearMonth.setYear(year);
		yearMonth.setMonth(month);
		// Roll other later by one month
		YearMonth other = new YearMonth();
		other.setYear(year);
		if (month == 12)
		{
			other.setMonth((byte) 1);
			other.setYear((short) (year.intValue() + 1));
		}
		else
			other.setMonth((byte) (month + 1));
		assert yearMonth.isEarlier(other) : "YearMonth " + yearMonth + " is earlier than " + other;
	}
}
