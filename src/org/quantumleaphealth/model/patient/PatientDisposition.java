/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import static javax.persistence.TemporalType.DATE;

/**
 * Decision of unidentifiable patient applicant to a clinical trial
 * 
 * @author Tom Bechtold
 * @version 2009-07-07
 */
@Entity
public class PatientDisposition implements Serializable
{
	/**
	 * Unique identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PatientDispositionID")
	@SequenceGenerator(name = "PatientDispositionID", sequenceName = "patientdisposition_id_seq", allocationSize = 1)
	private Long id;

	/**
	 * The identifier for the trial
	 */
	private Long trialId;
	/**
	 * The identifier for the site
	 */
	private Long siteId;

	/**
	 * Disposition types
	 */
	public enum Disposition
	{
		ENROLLED, UNREACHABLE, DECLINED, INELIGIBLE
	}

	/**
	 * The result of the screening process or <code>null</code> if no result
	 */
	@Enumerated
	private Disposition disposition;
	/**
	 * The reasons the patient declined in bit-wise format or <code>0</code> for
	 * none
	 */
	private long declined;
	/**
	 * The reasons the patient was ineligible in bit-wise format or
	 * <code>0</code> for none
	 */
	private long ineligible;
	/**
	 * The date of the disposition or <code>null</code> if not disposed yet. To
	 * support unidentifiable records, this field does not persist the time.
	 */
	@Temporal(DATE)
	private Date dispositionDate;

	/**
	 * Default constructor
	 */
	public PatientDisposition()
	{
	}

	/**
	 * Stores parameters into instance variables
	 * 
	 * @param trialId
	 *            the trialId to set
	 * @param siteId
	 *            the siteId to set
	 * @param disposition
	 *            the result of the screening process or <code>null</code> if no
	 *            result
	 * @param declined
	 *            the reasons the patient declined in bit-wise format or
	 *            <code>0</code> for none
	 * @param ineligible
	 *            the reasons the patient was ineligible in bit-wise format or
	 *            <code>0</code> for none
	 * @param dispositionDate
	 *            the date of the disposition or <code>null</code> if not
	 *            disposed yet
	 */
	public PatientDisposition(Long trialId, Long siteId, Disposition disposition, long declined, long ineligible,
			Date dispositionDate)
	{
		this.trialId = trialId;
		this.siteId = siteId;
		this.disposition = disposition;
		this.declined = declined;
		this.ineligible = ineligible;
		this.dispositionDate = dispositionDate;
	}

	/**
	 * @return the unique identifier
	 */
	public Long getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            the unique identifier to set
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * @return the identifier for the trial
	 */
	public Long getTrialId()
	{
		return trialId;
	}

	/**
	 * @param trialId
	 *            the identifier for the trial to set
	 */
	public void setTrialId(Long trialId)
	{
		this.trialId = trialId;
	}

	/**
	 * @return the identifier for the site
	 */
	public Long getSiteId()
	{
		return siteId;
	}

	/**
	 * @param siteId
	 *            the identifier for the site to set
	 */
	public void setSiteId(Long siteId)
	{
		this.siteId = siteId;
	}

	/**
	 * @return the disposition or <code>null</code> if no result
	 */
	public Disposition getDisposition()
	{
		return disposition;
	}

	/**
	 * @param disposition
	 *            the disposition to set or <code>null</code> if no result
	 */
	public void setDisposition(Disposition disposition)
	{
		this.disposition = disposition;
	}

	/**
	 * @return the reasons the patient declined in bit-wise format or
	 *         <code>0</code> for none
	 */
	public long getDeclined()
	{
		return declined;
	}

	/**
	 * @param declined
	 *            reasons the patient declined in bit-wise format or
	 *            <code>0</code> for none
	 */
	public void setDeclined(long declined)
	{
		this.declined = declined;
	}

	/**
	 * @return the reasons the patient was ineligible in bit-wise format or
	 *         <code>0</code> for none
	 */
	public long getIneligible()
	{
		return ineligible;
	}

	/**
	 * @param ineligible
	 *            the reasons the patient was ineligible in bit-wise format or
	 *            <code>0</code> for none
	 */
	public void setIneligible(long ineligible)
	{
		this.ineligible = ineligible;
	}

	/**
	 * @return the date of the disposition or <code>null</code> if not disposed
	 *         yet
	 */
	public Date getDispositionDate()
	{
		return dispositionDate;
	}

	/**
	 * @param dispositionDate
	 *            the date of the disposition or <code>null</code> if not
	 *            disposed yet
	 */
	public void setDispositionDate(Date dispositionDate)
	{
		this.dispositionDate = dispositionDate;
	}

	/**
	 * @return whether the identifier matches another's
	 * @param other
	 *            the other object
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other)
	{
		if (super.equals(other))
			return true;
		if (!(other instanceof PatientDisposition))
			return false;
		return (id != null) && (id.longValue() != 0) && id.equals(((PatientDisposition) (other)).id);
	}

	/**
	 * Returns the identifier's hashcode
	 * 
	 * @see java.lang.Object#hashCode()
	 * @see #id
	 */
	@Override
	public int hashCode()
	{
		return (id == null) || (id.longValue() == 0) ? super.hashCode() : id.hashCode();
	}

	/**
	 * Version ID for serializable class
	 */
	private static final long serialVersionUID = 5586533686538745917L;
}
