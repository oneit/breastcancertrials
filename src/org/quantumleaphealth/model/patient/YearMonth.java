/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Represents the year and month of an event. This serializable JavaBean exposes
 * its data via getters and setters.
 * 
 * @author Tom Bechtold
 * @version 2008-07-06
 */
public class YearMonth implements Serializable
{

	/**
	 * Year or <code>null</code> if not answered yet.
	 */
	private Short year;

	/**
	 * Month or 0 if not answered yet or not known. January == 1, February == 2,
	 * etc.
	 */
	private byte month;

	/**
	 * @return the year or <code>null</code> if not answered yet
	 */
	public Short getYear()
	{
		return year;
	}

	/**
	 * @param year
	 *            the year to set or <code>null</code> if not answered yet
	 */
	public void setYear(Short year)
	{
		this.year = year;
	}

	/**
	 * @return the month or 0 if not answered yet / not known
	 */
	public byte getMonth()
	{
		return month;
	}

	/**
	 * @param month
	 *            the month to set or 0 if not answered yet / not known
	 */
	public void setMonth(byte month)
	{
		this.month = month;
	}

	/**
	 * Set the year and month using UTC milliseconds from the epoch. This method
	 * only supports time after January 1, 1970 (Java epoch).
	 * 
	 * @param millis
	 *            UTC milliseconds from the epoch
	 * @throws IllegalArgumentException
	 *             if <code>millis</code> is not positive
	 */
	public void setTime(long millis) throws IllegalArgumentException
	{
		if (millis <= 0)
			throw new IllegalArgumentException("time must be positive: " + millis);
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTimeInMillis(millis);
		year = new Short((short) calendar.get(Calendar.YEAR));
		month = (byte) (calendar.get(Calendar.MONTH) + 1);
	}

	/**
	 * Returns whether the year and month are identical
	 */
	@Override
	public boolean equals(Object other)
	{
		if ((other == null) || !(other instanceof YearMonth))
			return false;
		YearMonth otherYearMonth = (YearMonth) (other);
		return (otherYearMonth.year == year) && (otherYearMonth.month == month);
	}

	/**
	 * Returns whether this date is definitively earlier than another date. This
	 * method first checks the years; if either is unknown then it returns
	 * <code>false</code>. If the years are the same then it checks the months;
	 * if either is unknown then it returns <code>false</code>.
	 * 
	 * @param other
	 *            the other date
	 * @return whether this date is definitively earlier than another date
	 */
	public boolean isEarlier(YearMonth other)
	{
		if ((other == null) || (year == null) || (other.year == null))
			return false;
		int delta = year.compareTo(other.year);
		if (delta < 0)
			return true;
		if (delta > 0)
			return false;
		return (month > 0) && (other.month > 0) && (month < other.month);
	}

	/**
	 * @return the month (if available), a forward slash, and the year (or ? if
	 *         year not set)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder(7);
		if (month > 0)
			builder.append(month).append('/');
		if (year == null)
			builder.append('?');
		else
			builder.append(year);
		return builder.toString();
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = 4238813091536032004L;
}
