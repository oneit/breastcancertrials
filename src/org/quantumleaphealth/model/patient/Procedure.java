/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient;

import java.io.Serializable;

import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Represents a procedure that is performed on an anatomical site. This
 * serializable JavaBean exposes its data via getters and setters.
 * 
 * @author Tom Bechtold
 * @version 2008-06-11
 */
public class Procedure implements Serializable
{
	/**
	 * Represents the type of procedure
	 */
	private CharacteristicCode kind;
	/**
	 * Represents the type of procedure
	 */
	private CharacteristicCode type;
	/**
	 * Where the procedure was performed
	 */
	private CharacteristicCode location;
	/**
	 * Whether or not there was progression
	 */
	private boolean progression;
	/**
	 * The year and month that the procedure was started, guaranteed to be non-
	 * <code>null</code>.
	 */
	private YearMonth started = new YearMonth();
	/**
	 * The year and month that the procedure was completed, guaranteed to be
	 * non-<code>null</code>.
	 */
	private YearMonth completed = new YearMonth();

	/**
	 * @return the kind
	 */
	public CharacteristicCode getKind()
	{
		return kind;
	}

	/**
	 * @param kind
	 *            the kind to set
	 */
	public void setKind(CharacteristicCode kind)
	{
		this.kind = kind;
	}

	/**
	 * @return the type
	 */
	public CharacteristicCode getType()
	{
		return type;
	}

	/**
	 * @param type
	 *            the type
	 */
	public void setType(CharacteristicCode type)
	{
		this.type = type;
	}

	/**
	 * Returns where the procedure was performed
	 * 
	 * @return where the procedure was performed
	 */
	public CharacteristicCode getLocation()
	{
		return location;
	}

	/**
	 * @param location
	 *            where the procedure was performed
	 */
	public void setLocation(CharacteristicCode location)
	{
		this.location = location;
	}

	/**
	 * Returns the progression
	 * 
	 * @return the progression
	 */
	public boolean getProgression()
	{
		return progression;
	}

	/**
	 * @param progression
	 *            the progression
	 */
	public void setProgression(boolean progression)
	{
		this.progression = progression;
	}

	/**
	 * @return the year and month that the procedure was started, guaranteed to
	 *         be non-<code>null</code>.
	 */
	public YearMonth getStarted()
	{
		return started;
	}

	/**
	 * @param yearMonth
	 *            the year and month that the procedure was started
	 */
	public void setStarted(YearMonth started)
	{
		if (started == null)
			throw new IllegalArgumentException("must specify started");
		this.started = started;
	}

	/**
	 * @return the year and month that the procedure was completed guaranteed to
	 *         be non-<code>null</code>.
	 */
	public YearMonth getCompleted()
	{
		return completed;
	}

	/**
	 * @param yearMonth
	 *            the year and month that the procedure was completed
	 */
	public void setCompleted(YearMonth completed)
	{
		if (completed == null)
			throw new IllegalArgumentException("must specify completed");
		this.completed = completed;
	}

	/**
	 * Returns the values of the fields
	 * 
	 * @return the values of the fields
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder("Procedure={");
		if (kind != null)
			builder.append("kind=").append(type).append(',');
		if (type != null)
			builder.append("type=").append(type).append(',');
		if (location != null)
			builder.append("location=").append(location).append(',');
		if (progression)
			builder.append("progression,");
		if ((started.getMonth() > 0) || ((started.getYear() != null) && (started.getYear().shortValue() > 0)))
			builder.append("started=").append(started.getMonth()).append('/').append(started.getYear()).append(',');
		if ((completed.getMonth() > 0) || ((completed.getYear() != null) && (completed.getYear().shortValue() > 0)))
			builder.append("completed=").append(completed.getMonth()).append('/').append(completed.getYear());
		return builder.append('}').toString();
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = -6938897766149713579L;
}
