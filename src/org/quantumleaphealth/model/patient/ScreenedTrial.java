/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.Transient;

import static javax.persistence.TemporalType.TIMESTAMP;

import org.quantumleaphealth.model.trial.Trial;

/**
 * The identifier of a clinical trial that has been matched to a patient's
 * history.
 * 
 * @author Tom Bechtold
 * @version 2008-12-17
 */
@Entity
public class ScreenedTrial implements Comparable<ScreenedTrial>, Serializable
{
	/**
	 * Unique identifier. Ideally the primary identifier would be a composite of
	 * the userpatient and trial identifiers.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ScreenedTrialID")
	@SequenceGenerator(name = "ScreenedTrialID", sequenceName = "screenedtrial_id_seq", allocationSize = 1)
	private Long id;
	/**
	 * The persisted trial id
	 */
	private Long trialId;
	
	@Column(name="userpatient_id", insertable = false, updatable = false)
	private Long userPatientId;
	
	/**
	 * The date the trial was last modified or <code>null</code> if the trial
	 * has never been modified
	 */
	@Temporal(TIMESTAMP)
	private Date lastModified;
	/**
	 * When the patient was informed of this trial or <code>null</code> if the
	 * patient has not been informed yet
	 */
	@Temporal(TIMESTAMP)
	private Date informed;

	/**
	 * Indicator if the patient is not interested
	 */
	private boolean notInterested;

	/**
	 * Indicator for inclusion in the patient's list of favorites.
	 */
	private boolean favorite;

	/**
	 * An indicator used for the navigator trial alert email
	 */
	@Temporal(TIMESTAMP)
	private Date navigatorInformed;
	
	/**
	 * Empty constructor
	 */
	public ScreenedTrial()
	{
	}

	/**
	 * Stores the identifier and lastModified properties from the parameter
	 * 
	 * @param trial
	 *            the trial
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public ScreenedTrial(Trial trial) throws IllegalArgumentException
	{
		setTrial(trial);
	}

	/**
	 * @return the unique identifier
	 */
	public Long getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            the unique identifier to set
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * Sets the <code>trialId</code> and <code>lastModified</code> properties
	 * 
	 * @param trial
	 *            the trial whose identifier and last modified properties are
	 *            set
	 * @throws IllegalArgumentException
	 *             if the trial or its id is <code>null</code> of if it is
	 *             overwriting a set trial
	 */
	public void setTrial(Trial trial) throws IllegalArgumentException
	{
		if (trial == null)
			throw new IllegalArgumentException("must specify trial");
		if (trial.getId() == null)
			throw new IllegalArgumentException("must specify id for " + trial);
		if ((trialId != null) && !trialId.equals(trial.getId()) && (this.id != null))
			throw new IllegalArgumentException("overwriting persisted screened trial #" + trialId + " with " + trial
					+ " is not allowed");
		this.trialId = trial.getId();
		this.lastModified = trial.getLastModified();
	}

	/**
	 * @return the trial id or <code>null</code> if no trial
	 */
	public Long getTrialId()
	{
		return trialId;
	}

	/**
	 * @return the trial's last modified date or <code>null</code> if the trial
	 *         has not been modified
	 */
	public Date getLastModified()
	{
		return lastModified;
	}

	/**
	 * @return when the patient was informed of this trial or <code>null</code>
	 *         if the patient has not been informed yet
	 */
	public Date getInformed()
	{
		return informed;
	}

	/**
	 * @param informed
	 *            when the patient was informed of this trial or
	 *            <code>null</code> if the patient has not been informed yet
	 */
	public void setInformed(Date informed)
	{
		this.informed = informed;
	}

	/**
	 * @return the notInterested indicator
	 */
	public boolean isNotInterested()
	{
		return notInterested;
	}

	/**
	 * @param notInterested
	 *            the notInterested indicator to set
	 */
	public void setNotInterested(boolean notInterested)
	{
		this.notInterested = notInterested;
	}

	/**
	 * @return the favorite indicator
	 */
	public boolean isFavorite()
	{
		return favorite;
	}

	/**
	 * @param favorite
	 *            the favorite indicator to set
	 */
	public void setFavorite(boolean favorite)
	{
		this.favorite = favorite;
	}

	/**
	 * @param navigatorInformed the navigatorInformed to set
	 */
	public void setNavigatorInformed(Date navigatorInformed) {
		this.navigatorInformed = navigatorInformed;
	}

	/**
	 * @return the navigatorInformed
	 */
	public Date getNavigatorInformed() {
		return navigatorInformed;
	}

	/**
	 * Returns how the trials' identifiers compare. Known data is sorted before
	 * unknown data.
	 * 
	 * @return a negative integer if <code>other</code> is <code>null</code> or
	 *         if the identifiers of <code>other</code> are <code>null</code>; 0
	 *         if the <code>id</code> or <code>trialId</code> properties are
	 *         equal; a positive integer otherwise
	 * @param other
	 *            the other screened trial
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(ScreenedTrial other)
	{
		if (this == other)
			return 0;
		// Known is sorted before unknown
		if (other == null)
			return -1;
		// If both trial id's unknown then compare class ids
		if ((trialId == null) && (other.trialId == null))
		{
			// If class ids are unknown then they are considered the same
			if ((id == null) && (other.id == null))
				return 0;
			if (other.id == null)
				return -1;
			if (id == null)
				return 1;
			return id.compareTo(other.id);
		}
		if (other.trialId == null)
			return -1;
		if (trialId == null)
			return 1;
		return trialId.compareTo(other.trialId);
	}

	/**
	 * @return whether the unique identifier (or trial identifier if not
	 *         available) matches another's
	 * @param other
	 *            the other object
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other)
	{
		if (super.equals(other))
			return true;
		if (!(other instanceof ScreenedTrial))
			return false;
		ScreenedTrial screenedTrial = (ScreenedTrial) (other);
		if (id != null)
			return id.equals(screenedTrial.id);
		return (trialId != null) && trialId.equals(screenedTrial.trialId);
	}

	/**
	 * Returns the id's hashcode or the superclass'
	 * 
	 * @see java.lang.Object#hashCode()
	 * @see #id
	 */
	@Override
	public int hashCode()
	{
		return (id != null) ? id.hashCode() : super.hashCode();
	}


	/**
	 * @param userPatientId the userPatientId to set
	 */
	public void setUserPatientId(Long userPatientId) {
		this.userPatientId = userPatientId;
	}

	/**
	 * @return the userPatientId
	 */
	public Long getUserPatientId() {
		return userPatientId;
	}


	/**
	 * Version ID for serializable class
	 */
	private static final long serialVersionUID = 8144937529195195020L;
}
