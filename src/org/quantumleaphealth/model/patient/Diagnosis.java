/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.quantumleaphealth.ontology.AttributeCharacteristicHolder;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Represents a patient's diagnosis. This serializable JavaBean exposes its data
 * via characteristic codes.
 * 
 * @author Tom Bechtold
 * @version 2008-07-06
 */
public class Diagnosis implements AttributeCharacteristicHolder, Serializable
{
	/**
	 * Binary characteristics, guaranteed to be non-<code>null</code>
	 */
	private BinaryHistory binaryHistory = new BinaryHistory();
	/**
	 * Value characteristics, guaranteed to be non-<code>null</code>
	 */
	private ValueHistory valueHistory = new ValueHistory();
	/**
	 * Attribute characteristics, guaranteed to be non-<code>null</code>
	 */
	private Map<CharacteristicCode, Set<CharacteristicCode>> attributeCharacteristics = new HashMap<CharacteristicCode, Set<CharacteristicCode>>();
	/**
	 * Short characteristics, guaranteed to be non-<code>null</code>
	 */
	private ShortHistory shortHistory = new ShortHistory();

	/**
	 * @return the binary characteristics
	 */
	public BinaryHistory getBinaryHistory()
	{
		return binaryHistory;
	}

	/**
	 * @param binaryHistory
	 *            the binary characteristics to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setBinaryHistory(BinaryHistory binaryHistory) throws IllegalArgumentException
	{
		if (binaryHistory == null)
			throw new IllegalArgumentException("parameter not specified");
		this.binaryHistory = binaryHistory;
	}

	/**
	 * @return the value characteristics
	 */
	public ValueHistory getValueHistory()
	{
		return valueHistory;
	}

	/**
	 * @param valueHistory
	 *            the value characteristics to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setValueHistory(ValueHistory valueHistory) throws IllegalArgumentException
	{
		if (valueHistory == null)
			throw new IllegalArgumentException("parameter not specified");
		this.valueHistory = valueHistory;
	}

	/**
	 * @return the attributeCharacteristics
	 */
	public Map<CharacteristicCode, Set<CharacteristicCode>> getAttributeCharacteristics()
	{
		return attributeCharacteristics;
	}

	/**
	 * @param attributeCharacteristics
	 *            the attributeCharacteristics to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setAttributeCharacteristics(Map<CharacteristicCode, Set<CharacteristicCode>> attributeCharacteristics)
			throws IllegalArgumentException
	{
		if (attributeCharacteristics == null)
			throw new IllegalArgumentException("parameter not specified");
		this.attributeCharacteristics = attributeCharacteristics;
	}

	/**
	 * Adds an attribute to a characteristic
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to add the attribute to
	 * @param attributeCode
	 *            the code of the attribute to add to the characteristic
	 * @see org.quantumleaphealth.ontology.AttributeCharacteristicHolder#addAttribute(org.quantumleaphealth.ontology.CharacteristicCode,
	 *      org.quantumleaphealth.ontology.CharacteristicCode)
	 */
	public void addAttribute(CharacteristicCode characteristicCode, CharacteristicCode attributeCode)
	{
		if ((characteristicCode == null) || (attributeCode == null))
			throw new IllegalArgumentException("characteristic or attribute not specified");
		Set<CharacteristicCode> set = attributeCharacteristics.get(characteristicCode);
		if (set == null)
		{
			set = new HashSet<CharacteristicCode>();
			attributeCharacteristics.put(characteristicCode, set);
		}
		set.add(attributeCode);
	}

	/**
	 * Removes an attribute from a characteristic
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to remove the attribute from
	 * @param attributeCode
	 *            the code of the attribute to remove from the characteristic
	 */
	public void removeAttribute(CharacteristicCode characteristicCode, CharacteristicCode attributeCode)
	{
		if ((characteristicCode == null) || (attributeCode == null))
			throw new IllegalArgumentException("characteristic or attribute not specified");
		// Remove the attribute from the set if not already removed
		Set<CharacteristicCode> set = attributeCharacteristics.get(characteristicCode);
		if (set == null)
			return;
		set.remove(attributeCode);
		// Remove the entire set if no more attributes
		if (set.size() == 0)
			attributeCharacteristics.remove(characteristicCode);
	}

	/**
	 * Returns whether a characteristic contains an attribute
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to find the attribute for
	 * @param attributeCode
	 *            the code of the attribute to find within the characteristic
	 * @return whether a characteristic contains an attribute
	 * @see org.quantumleaphealth.ontology.AttributeCharacteristicHolder#containsAttribute(org.quantumleaphealth.ontology.CharacteristicCode,
	 *      org.quantumleaphealth.ontology.CharacteristicCode)
	 */
	public boolean containsAttribute(CharacteristicCode characteristicCode, CharacteristicCode attributeCode)
	{
		if (characteristicCode == null)
			throw new IllegalArgumentException("characteristic not specified");
		Set<CharacteristicCode> set = attributeCharacteristics.get(characteristicCode);
		return (attributeCode != null) && (set != null) && set.contains(attributeCode);
	}

	/**
	 * Unsupported as diagnoses do not contain string characteristics
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to set the string for
	 * @param string
	 *            the string to set to the characteristic
	 * @see org.quantumleaphealth.ontology.StringCharacteristicHolder#setString(org.quantumleaphealth.ontology.CharacteristicCode,
	 *      java.lang.String)
	 */
	public void setString(CharacteristicCode characteristicCode, String string) throws UnsupportedOperationException
	{
		throw new UnsupportedOperationException("string characteristics are not supported in a diagnosis");
	}

	/**
	 * Unsupported as diagnoses do not contain string characteristics
	 * 
	 * @param characteristicCode
	 *            ignored
	 * @return <code>null</code>
	 * @see org.quantumleaphealth.ontology.StringCharacteristicHolder#getString(org.quantumleaphealth.ontology.CharacteristicCode)
	 */
	public String getString(CharacteristicCode characteristicCode) throws UnsupportedOperationException
	{
		return null;
	}

	/**
	 * @return the short characteristics, guaranteed to be non-<code>null</code>
	 */
	public ShortHistory getShortHistory()
	{
		return shortHistory;
	}

	/**
	 * @param shortHistory
	 *            the short characteristics to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setShortHistory(ShortHistory shortHistory) throws IllegalArgumentException
	{
		if (shortHistory == null)
			throw new IllegalArgumentException("parameter not specified");
		this.shortHistory = shortHistory;
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = -4027878848335172532L;
}
