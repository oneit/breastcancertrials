/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient;

import java.util.HashMap;

import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.ontology.ShortCharacteristicHolder;

/**
 * Holds non-<code>null</code> short characteristics.
 * 
 * @author Tom Bechtold
 * @version 2008-05-27
 */
public class ShortHistory extends HashMap<CharacteristicCode, Short> implements ShortCharacteristicHolder
{
	/**
	 * Calls superclass' default constructor.
	 */
	public ShortHistory()
	{
		super();
	}

	/**
	 * Sets a short value for a characteristic. This method does nothing if
	 * <code>characteristicCode</code> is <code>null</code>
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to set the short value for
	 * @param shortValue
	 *            the short to set to the characteristic or <code>null</code> to
	 *            remove any value
	 * @see org.quantumleaphealth.ontology.ShortCharacteristicHolder#setShort(org.quantumleaphealth.ontology.CharacteristicCode,
	 *      java.lang.Short)
	 */
	public void setShort(CharacteristicCode characteristicCode, Short shortValue)
	{
		if (characteristicCode == null)
			return;
		if (shortValue == null)
			remove(characteristicCode);
		else
			put(characteristicCode, shortValue);
	}

	/**
	 * Returns the short value that is set in a characteristic
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic
	 * @return the short value that is set in a characteristic or
	 *         <code>null</code> if no value is set
	 * @see org.quantumleaphealth.ontology.ShortCharacteristicHolder#getShort(org.quantumleaphealth.ontology.CharacteristicCode)
	 */
	public Short getShort(CharacteristicCode characteristicCode)
	{
		return (characteristicCode == null) ? null : get(characteristicCode);
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = 315025708756942465L;
}
