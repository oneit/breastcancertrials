/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient;

import java.util.HashSet;

import org.quantumleaphealth.ontology.BinaryCharacteristicHolder;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Holds non-<code>null</code> binary characteristics.
 * 
 * @author Tom Bechtold
 * @version 2008-05-22
 */
public class BinaryHistory extends HashSet<CharacteristicCode> implements BinaryCharacteristicHolder
{
	/**
	 * Calls superclass' default constructor.
	 */
	public BinaryHistory()
	{
		super();
	}

	/**
	 * Adds a non-<code>null</code> binary characteristic. This method does
	 * nothing if <code>characteristicCode</code> is <code>null</code>.
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic code to add
	 * @see org.quantumleaphealth.ontology.BinaryCharacteristicHolder#addCharacteristic(org.quantumleaphealth.ontology.CharacteristicCode)
	 */
	public void addCharacteristic(CharacteristicCode characteristicCode)
	{
		if (characteristicCode != null)
			add(characteristicCode);
	}

	/**
	 * Removes a non-<code>null</code> binary characteristic. This method does
	 * nothing if <code>characteristicCode</code> is <code>null</code>.
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to remove
	 * @see org.quantumleaphealth.ontology.BinaryCharacteristicHolder#removeCharacteristic(org.quantumleaphealth.ontology.CharacteristicCode)
	 */
	public void removeCharacteristic(CharacteristicCode characteristicCode)
	{
		if (characteristicCode != null)
			remove(characteristicCode);
	}

	/**
	 * Returns whether the non-<code>null</code> characteristic is present
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to find
	 * @return whether <code>characteristicCode</code> is non-<code>null</code>
	 *         and the characteristic is present
	 * @see org.quantumleaphealth.ontology.BinaryCharacteristicHolder#containsCharacteristic(org.quantumleaphealth.ontology.CharacteristicCode)
	 */
	public boolean containsCharacteristic(CharacteristicCode characteristicCode)
	{
		return characteristicCode == null ? false : contains(characteristicCode);
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = -4339153725352142623L;
}
