/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient;

import java.util.HashMap;

import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.ontology.ValueCharacteristicHolder;

/**
 * Holds non-<code>null</code> value characteristics.
 * 
 * @author Tom Bechtold
 * @version 2008-05-22
 */
public class ValueHistory extends HashMap<CharacteristicCode, CharacteristicCode> implements ValueCharacteristicHolder
{
	/**
	 * Calls superclass' default constructor.
	 */
	public ValueHistory()
	{
		super();
	}

	/**
	 * Sets a characteristic's value. This method does nothing if
	 * <code>characteristicCode</code> is <code>null</code>
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to set the value for
	 * @param valueCode
	 *            the code of the value to set to the characteristic or
	 *            <code>null</code> to remove any value
	 * @see org.quantumleaphealth.ontology.ValueCharacteristicHolder#setValue(org.quantumleaphealth.ontology.CharacteristicCode,
	 *      org.quantumleaphealth.ontology.CharacteristicCode)
	 */
	public void setValue(CharacteristicCode characteristicCode, CharacteristicCode valueCode)
	{
		if (characteristicCode == null)
			return;
		if (valueCode == null)
			remove(characteristicCode);
		else
			put(characteristicCode, valueCode);
	}

	/**
	 * Returns the value that is set in a characteristic.
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic
	 * @return the value that is set in a characteristic or <code>null</code> if
	 *         no value is set or <code>characteristicCode</code> is
	 *         <code>null</code>
	 * @see org.quantumleaphealth.ontology.ValueCharacteristicHolder#getValue(org.quantumleaphealth.ontology.CharacteristicCode)
	 */
	public CharacteristicCode getValue(CharacteristicCode characteristicCode)
	{
		return (characteristicCode == null) ? null : get(characteristicCode);
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = 5486568035082016342L;
}
