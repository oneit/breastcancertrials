/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.beans.XMLEncoder;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import javax.persistence.Transient;

import org.quantumleaphealth.xml.DecodingException;
import org.quantumleaphealth.xml.DecodingExceptionListener;

/**
 * Represents a user who is a patient. Because this class annotates the
 * <code>id</code> column on the <code>getId()</code> getter method, the access
 * type for persistence will be getter methods instead of fields. This
 * persistence access type allows for the transient field <code>history</code>
 * to be marshaled into XML format for persisting.
 * 
 * @author Tom Bechtold
 * @version 2009-03-12
 */
@Entity
public class UserPatient implements Serializable
{
    public static enum UserType { USER_PATIENT, AVATAR_PROFILE, READ_ONLY_PROFILE };

	/**
	 * Unique identifier or <code>null</code> if new object.
	 */
	private Long id;

	/**
	 * Country of residence or <code>null</code> if none
	 */
	private String countryCode;

	/**
	 * First three digits of postal code or <code>null</code> if not entered or
	 * not applicable. Note: JavaServerFaces convertNumber validation does not
	 * work on <code>Short</code> and <code>Integer</code>, so use
	 * <code>Long</code>
	 */
	private Long privatePostalCode;

	/**
	 * Credentials or <code>null</code> if none
	 */
	private String principal;
	/**
	 * Credentials or <code>null</code> if none
	 */
	private String credentials;
	/**
	 * The secret question if credentials are lost
	 */
	private String secretQuestion;
	/**
	 * The secret answer if credentials are lost
	 */
	private String secretAnswer;
	/**
	 * The number of unsuccessful authentication attempts since the most recent
	 * successful authentication.
	 */
	private int unsuccessfulAuthenticationCount;

	/**
	 * When accepted the terms/conditions or <code>null</code> if not accepted
	 * yet
	 */
	private Date termsAcceptedTime;

	/**
	 * The patient's history, guaranteed to be non-<code>null</code>
	 */
	private PatientHistory patientHistory = new PatientHistory();

	/**
	 * Date/time history was last updated or <code>null</code> if never updated
	 */
	private Date patientHistoryLastModified;

	/**
	 * Whether the history is complete
	 */
	private boolean patientHistoryComplete;

	/**
	 * The screened trials in order of <code>trial.id</code>, guaranteed to be
	 * non-<code>null</code>
	 */
	private List<ScreenedTrial> screenedTrials = new LinkedList<ScreenedTrial>();

	/**
	 * Date/time first screened trials were matched or <code>null</code> if
	 * never matched
	 */
	private Date screenedTrialsLastModified;

	/**
	 * Invited trial sites, guaranteed to be non-<code>null</code>
	 */
	private Set<Invitation> invitations = new LinkedHashSet<Invitation>();

	/**
	 * Whether or not the patient will be alerted to eligible newly posted
	 * trials
	 */
	private boolean alertNewTrialsEnabled;
	/**
	 * Whether or not the patient's principal is a valid email address
	 */
	private boolean principalInvalidEmailAddress;

	/**
	 * Referral (multiple choices stored as bits) or <code>0</code> for none.
	 * Each bit represents whether a choice is selected.
	 * <ol>
	 * <li>My doctor or nurse
	 * <li>A breast cancer support group
	 * <li>A radio announcement
	 * <li>A friend or family member
	 * <li>An internet link from a search engine or other internet site
	 * <li>A local community or national organization
	 * <li>Another breast cancer patient
	 * <li>Other
	 * </ol>
	 */
	private int referral = 0;

	/**
	 * Referral link or <code>null</code> if not answered.
	 */
	private String referralLink = null;

	/**
	 * Referral organization or <code>null</code> if not answered.
	 */
	private String referralOrganization = null;

	/**
	 * Referral other or <code>null</code> if not answered.
	 */
	private String referralOther = null;
	/**
	 * Preferred language.
	 */
	private String preferredLanguage = Locale.ENGLISH.getLanguage();

	private UserType usertype;

	private AvatarHistory avatarHistory = new AvatarHistory();
	
	/**
     * A full description of this avatar.
     */
	private String avatarProfileDescription = null;
	
	/**
     * A smaller description for this avatar for use as the title
     * in lists and links.
     */
	private String avatarProfileTitle = null;
	
	/**
	 * The name of the UTF-8 character set that every Java implementation must
	 * provide
	 */
	public static final String UTF8 = "UTF-8";

	private Integer characteristicBitmap;
	
	/**
	 * @return the id
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "UserPatientID")
	@SequenceGenerator(name = "UserPatientID", sequenceName = "userpatient_id_seq", allocationSize = 1)
	public Long getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * @return the country code
	 */
	public String getCountryCode()
	{
		return countryCode;
	}

	/**
	 * Store country code; empty string is stored as <code>null</code>
	 * 
	 * @param countryCode
	 *            the country code to set
	 */
	public void setCountryCode(String countryCode)
	{
		this.countryCode = trim(countryCode);
	}

	/**
	 * @return the first three digits of postal code or <code>null</code> if not
	 *         entered or not applicable
	 */
	public Long getPrivatePostalCode()
	{
		return privatePostalCode;
	}

	/**
	 * @param privatePostalCode
	 *            the first three digits of postal code to set or
	 *            <code>null</code> if not entered or not applicable
	 */
	public void setPrivatePostalCode(Long privatePostalCode)
	{
		this.privatePostalCode = privatePostalCode;
	}

	/**
	 * @return the principal
	 */
	public String getPrincipal()
	{
		return principal;
	}

	/**
	 * Store principal; empty string is stored as <code>null</code>
	 * 
	 * @param principal
	 *            the principal to set
	 */
	public void setPrincipal(String principal)
	{
		this.principal = trim(principal);
	}

	/**
	 * @return the credentials
	 */
	public String getCredentials()
	{
		return credentials;
	}

	/**
	 * Store credentials; empty string is stored as <code>null</code>
	 * 
	 * @param credentials
	 *            the credentials to set
	 */
	public void setCredentials(String credentials)
	{
		this.credentials = trim(credentials);
	}

	/**
	 * @return the secret question if credentials are lost
	 */
	public String getSecretQuestion()
	{
		return secretQuestion;
	}

	/**
	 * Store secret question; empty string is stored as <code>null</code>
	 * 
	 * @param secretQuestion
	 *            the secret question if credentials are lost
	 */
	public void setSecretQuestion(String secretQuestion)
	{
		this.secretQuestion = trim(secretQuestion);
	}

	/**
	 * @return the secret answer if credentials are lost
	 */
	public String getSecretAnswer()
	{
		return secretAnswer;
	}

	/**
	 * Store secret answer; empty string is stored as <code>null</code>
	 * 
	 * @param secretAnswer
	 *            the secret answer if credentials are lost
	 */
	public void setSecretAnswer(String secretAnswer)
	{
		this.secretAnswer = trim(secretAnswer);
	}

	/**
	 * @return the number of unsuccessful authentication attempts since the most
	 *         recent successful authentication
	 */
	public int getUnsuccessfulAuthenticationCount()
	{
		return unsuccessfulAuthenticationCount;
	}

	/**
	 * @param unsuccessfulAuthenticationCount
	 *            the number of unsuccessful authentication attempts since the
	 *            most recent successful authentication
	 */
	public void setUnsuccessfulAuthenticationCount(int unsuccessfulAuthenticationCount)
	{
		this.unsuccessfulAuthenticationCount = unsuccessfulAuthenticationCount;
	}

	/**
	 * Returns whether the terms were accepted. This method return
	 * <code>true</code> if the accepted date is non-<code>null</code>. This
	 * method is declared transient so the persistence engine does not save it;
	 * rather, the persistence engine will access the data via
	 * <code>getTermsAcceptedTime</code> and <code>setTermsAcceptedTime</code>.
	 * 
	 * @return whether the terms were accepted
	 * @see #termsAcceptedTime
	 */
	@Transient
	public boolean isTermsAccepted()
	{
		return termsAcceptedTime != null;
	}

	/**
	 * Sets the terms accepted date. This method sets the date to now if the
	 * parameter is <code>true</code> and the date has not been stored yet.
	 * 
	 * @param termsAcceptedNow
	 *            whether the terms are accepted
	 * @see #termsAcceptedTime
	 */
	public void setTermsAccepted(boolean termsAcceptedNow)
	{
		if (!termsAcceptedNow)
			this.termsAcceptedTime = null;
		else if (this.termsAcceptedTime == null)
			this.termsAcceptedTime = new Date();
	}

	/**
	 * @return when accepted the terms/conditions or <code>null</code> if not
	 *         accepted yet
	 */
	@Temporal(TIMESTAMP)
	public Date getTermsAcceptedTime()
	{
		return termsAcceptedTime;
	}

	/**
	 * @param termsAcceptedTime
	 *            when accepted the terms/conditions or <code>null</code> if not
	 *            accepted yet
	 */
	public void setTermsAcceptedTime(Date termsAcceptedTime)
	{
		this.termsAcceptedTime = termsAcceptedTime;
	}

	/**
	 * Returns the patient history. This method is declared transient so the
	 * persistence engine does not save it; rather, the persistence engine will
	 * access the data via <code>getPatientHistoryMarshaled</code> and
	 * <code>setPatientHistoryMarshaled</code>.
	 * 
	 * @return the patient history
	 * @see #getPatientHistoryMarshaled()
	 */
	@Transient
	public PatientHistory getPatientHistory()
	{
		patientHistory.setUserType(getUsertype());
		return patientHistory;
	}

	@Transient
	public void setPatientHistory( PatientHistory patientHistory )
	{
		this.patientHistory = patientHistory;
	}

	/**
	 * Marshals the patient history into an UTF-8 string. This method is used by
	 * the persistence engine to persist the <code>patientHistory</code> field.
	 * 
	 * @return the marshaled patient history in UTF-8 encoding
	 */
	public String getPatientHistoryMarshaled()
	{
		if (patientHistory == null)
			return null;
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(256);
		XMLEncoder encoder = new XMLEncoder(outputStream);
		encoder.writeObject(patientHistory);
		encoder.close();
		try
		{
			return outputStream.toString(UTF8);
		}
		catch (UnsupportedEncodingException unsupportedEncodingException)
		{
			// Should never happen; use platform's charset
			return outputStream.toString();
		}
	}

	/**
	 * Unmarshals the patient history from a UTF-8 encoded string. This method
	 * is used by the persistence engine to persist the
	 * <code>patientHistory</code> field.
	 * 
	 * @param patientHistoryMarshaled
	 *            the marshaled patient history in UTF-8 encoding
	 * @throws DecodingException
	 *             if the bytes cannot be unmarshaled to eligibility criteria
	 */
	public void setPatientHistoryMarshaled(String patientHistoryMarshaled) throws DecodingException
	{
		// Empty bytes means an empty object
		if ((patientHistoryMarshaled == null) || (patientHistoryMarshaled.length() == 0))
		{
			this.patientHistory = new PatientHistory();
			return;
		}
		byte[] patientHistoryBytes = null;
		try
		{
			patientHistoryBytes = patientHistoryMarshaled.getBytes(UTF8);
		}
		catch (UnsupportedEncodingException unsupportedEncodingException)
		{
			// Should never happen as UTF-8 is mandatory for all Java
			// implementations; use platform's default charset
			patientHistoryBytes = patientHistoryMarshaled.getBytes();
		}
		Object patientHistoryObject = DecodingExceptionListener.decode(new ByteArrayInputStream(patientHistoryBytes));
		if ((patientHistoryObject == null) || !(patientHistoryObject instanceof PatientHistory))
			throw new DecodingException(1, new RuntimeException("Patient history for " + id + " with size "
					+ patientHistoryMarshaled.length() + " cannot be loaded from " + patientHistoryObject));
		this.patientHistory = (PatientHistory) (patientHistoryObject);
	}

	/**
	 * @return the date/time history was last updated or <code>null</code> if
	 *         never updated
	 */
	@Temporal(TIMESTAMP)
	public Date getPatientHistoryLastModified()
	{
		return patientHistoryLastModified;
	}

	/**
	 * @param patientHistoryLastModified
	 *            the the date/time history was last updated or
	 *            <code>null</code> if never updated
	 */
	public void setPatientHistoryLastModified(Date patientHistoryLastModified)
	{
		this.patientHistoryLastModified = patientHistoryLastModified;
	}

	/**
	 * @return whether the history is complete
	 */
	public boolean isPatientHistoryComplete()
	{
		return isAvatarProfile() == true ? true: patientHistoryComplete;
	}

	/**
	 * @param patientHistoryComplete
	 *            whether the history is complete
	 */
	public void setPatientHistoryComplete(boolean patientHistoryComplete)
	{
		this.patientHistoryComplete = patientHistoryComplete;
	}

	/**
	 * @return the screened trials in order of <code>trial.id</code>, guaranteed
	 *         to be non-<code>null</code> and cascaded with this
	 *         <code>userPatient</code>
	 */
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "UserPatient_Id", nullable = false)
	@OrderBy("trialId")
	public List<ScreenedTrial> getScreenedTrials()
	{
		return screenedTrials;
	}

	/**
	 * @param screenedTrials
	 *            the screened trials in order of <code>trial.id</code>
	 * @throws IllegalArgumentException
	 *             if parameter is <code>null</code>
	 */
	public void setScreenedTrials(List<ScreenedTrial> screenedTrials) throws IllegalArgumentException
	{
		if (screenedTrials == null)
			throw new IllegalArgumentException("must specify parameter");
		this.screenedTrials = screenedTrials;
	}

	/**
	 * @return the date/time first screened trials were matched or
	 *         <code>null</code> if never matched
	 */
	@Temporal(TIMESTAMP)
	public Date getScreenedTrialsLastModified()
	{
		return screenedTrialsLastModified;
	}

	/**
	 * @param screenedTrialsLastModified
	 *            the date/time first screened trials were matched or
	 *            <code>null</code> if never matched
	 */
	public void setScreenedTrialsLastModified(Date screenedTrialsLastModified)
	{
		this.screenedTrialsLastModified = screenedTrialsLastModified;
	}

	/**
	 * @return invited trial sites, guaranteed to be non-<code>null</code> and
	 *         cascaded with this owning <code>userPatient</code>
	 */
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "userPatient")
	public Set<Invitation> getInvitations()
	{
		return invitations;
	}

	/**
	 * @param invitation
	 *            invited trial sites
	 * @throws IllegalArgumentException
	 *             if parameter is <code>null</code>
	 */
	public void setInvitations(Set<Invitation> invitations)
	{
		if (invitations == null)
			throw new IllegalArgumentException("must specify parameter");
		this.invitations = invitations;
	}

	/**
	 * @return whether or not the patient will be alerted to eligible newly
	 *         posted trials
	 */
	public boolean isAlertNewTrialsEnabled()
	{
		return alertNewTrialsEnabled;
	}

	/**
	 * @param alertNewTrialsEnabled
	 *            whether or not the patient will be alerted to eligible newly
	 *            posted trials
	 */
	public void setAlertNewTrialsEnabled(boolean alertNewTrialsEnabled)
	{
		this.alertNewTrialsEnabled = alertNewTrialsEnabled;
	}

	/**
	 * @return whether or not the patient's principal is a valid email address
	 */
	public boolean isPrincipalInvalidEmailAddress()
	{
		return principalInvalidEmailAddress;
	}

	/**
	 * @param principalInvalidEmailAddress
	 *            whether or not the patient's principal is a valid email
	 *            address
	 */
	public void setPrincipalInvalidEmailAddress(boolean principalInvalidEmailAddress)
	{
		this.principalInvalidEmailAddress = principalInvalidEmailAddress;
	}

	/**
	 * Convenience method
	 * @return whether or not this is a read-only profile (these can appear in publicly accessible 
	 * trial and history profile pages).
	 */
	@Transient
	public boolean isReadOnlyProfile()
	{
		return this.usertype.equals(UserType.READ_ONLY_PROFILE);
	}
	
	/**
	 * Convenience method
	 * @return whether or not this is an avatar profile (these can appear in publicly accessible 
	 * trial and history profile pages).  Also used in the MatchingEngine implementation.
	 */
	@Transient
	public boolean isAvatarProfile()
	{
		return this.usertype == null? false: this.usertype.equals(UserType.AVATAR_PROFILE);
	}


	/**
	 * Convenience method
	 * @return whether or not this is an avatar profile (these can appear in publicly accessible 
	 * trial and history profile pages).  Also used in the MatchingEngine implementation.
	 */
	@Transient
	public boolean isSpecialUserProfile()
	{
		return this.usertype == null? false: this.usertype.equals(UserType.AVATAR_PROFILE) || this.usertype.equals(UserType.READ_ONLY_PROFILE);
	}

	/**
	 * @param usertype the usertype to set
	 */
	public void setUsertype(UserType usertype) 
	{
		this.usertype = usertype;
	}

	/**
	 * @return the usertype
	 */
	public UserType getUsertype() 
	{
		return usertype;
	}

	/**
	 * @param avatarProfileDescription the avatarProfileDescription to set
	 */
	public void setAvatarProfileDescription(String avatarProfileDescription)
	{
		this.avatarProfileDescription = avatarProfileDescription;
	}

	/**
	 * @return the avatarProfileDescription
	 */
	public String getAvatarProfileDescription()
	{
		return avatarProfileDescription;
	}

	/**
	 * @param avatarProfileTitle the avatarProfileTitle to set
	 */
	public void setAvatarProfileTitle(String avatarProfileTitle)
	{
		this.avatarProfileTitle = avatarProfileTitle;
	}

	/**
	 * @return the avatarProfileTitle
	 */
	public String getAvatarProfileTitle()
	{
		return avatarProfileTitle;
	}

	/**
	 * @return the referral
	 */
	public int getReferral()
	{
		return referral;
	}

	/**
	 * @param referral
	 *            the referral to set
	 */
	public void setReferral(int referral)
	{
		this.referral = referral;
	}

	/**
	 * @return the referralLink
	 */
	public String getReferralLink()
	{
		return referralLink;
	}

	/**
	 * Stores referral link; empty string is stored as <code>null</code>
	 * 
	 * @param referralLink
	 *            the referralLink to set
	 */
	public void setReferralLink(String referralLink)
	{
		this.referralLink = trim(referralLink);
	}

	/**
	 * @return the referralOrganization
	 */
	public String getReferralOrganization()
	{
		return referralOrganization;
	}

	/**
	 * Stores referral organization; empty string is stored as <code>null</code>
	 * 
	 * @param referralOrganization
	 *            the referralOrganization to set
	 */
	public void setReferralOrganization(String referralOrganization)
	{
		this.referralOrganization = trim(referralOrganization);
	}

	/**
	 * @return the referralOther
	 */
	public String getReferralOther()
	{
		return referralOther;
	}

	/**
	 * Stores referral other; empty string is stored as <code>null</code>
	 * 
	 * @param referralOther
	 *            the referralOther to set
	 */
	public void setReferralOther(String referralOther)
	{
		this.referralOther = trim(referralOther);
	}

	/**
	 * @return the preferred language
	 */
	public String getPreferredLanguage()
	{
		return this.preferredLanguage;
	}

	/**
	 * @param preferredLanguage
	 *            The preferred language to set
	 */
	public void setPreferredLanguage(String preferredLanguage)
	{
		this.preferredLanguage = preferredLanguage;
	}

	/**
	 * @param avatarHistoryMarshaled the avatarHistoryMarshaled to set
	 */
	public void setAvatarHistoryMarshaled(String avatarHistoryMarshaled) throws DecodingException
	{
		//Empty bytes means an empty object
		if ((avatarHistoryMarshaled == null) || (avatarHistoryMarshaled.length() == 0))
		{
			this.setAvatarHistory(new AvatarHistory());
			return;
		}
		byte[] avatarHistoryBytes = null;
		try
		{
			avatarHistoryBytes = avatarHistoryMarshaled.getBytes(UTF8);
		}
		catch (UnsupportedEncodingException unsupportedEncodingException)
		{
			// Should never happen as UTF-8 is mandatory for all Java
			// implementations; use platform's default charset
			avatarHistoryBytes = avatarHistoryMarshaled.getBytes();
		}
		Object avatarHistoryObject = DecodingExceptionListener.decode(new ByteArrayInputStream(avatarHistoryBytes));
		if ((avatarHistoryObject == null) || !(avatarHistoryObject instanceof AvatarHistory))
		{	
			throw new DecodingException(1, new RuntimeException("Avatar history for " + id + " with size "
					+ avatarHistoryMarshaled.length() + " cannot be loaded from " + avatarHistoryObject));
		}	
		this.setAvatarHistory((AvatarHistory) (avatarHistoryObject));
	}

	public String getAvatarHistoryMarshaled()
	{
		if (getAvatarHistory() == null)
			return null;
		
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream(256);
		XMLEncoder encoder = new XMLEncoder(outputStream);
		encoder.writeObject(getAvatarHistory());
		encoder.close();
		try
		{
			return outputStream.toString(UTF8);
		}
		catch (UnsupportedEncodingException unsupportedEncodingException)
		{
			// Should never happen; use platform's charset
			return outputStream.toString();
		}
	}

	/**
	 * @param avatarHistory the avatarHistory to set
	 */
	@Transient
	public void setAvatarHistory(AvatarHistory avatarHistory) 
	{
		this.avatarHistory = avatarHistory;
	}

	/**
	 * @return the avatarHistory
	 */
	@Transient
	public AvatarHistory getAvatarHistory() 
	{
		return avatarHistory;
	}

	/**
	 * @param characteristicBitmap the characteristicBitmap to set
	 */
	public void setCharacteristicBitmap(Integer characteristicBitmap) 
	{
		this.characteristicBitmap = characteristicBitmap;
	}

	/**
	 * @return the characteristicBitmap
	 */
	public Integer getCharacteristicBitmap() 
	{
		return characteristicBitmap;
	}

	/**
	 * Returns the values of the fields
	 * 
	 * @return the values of the fields
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder("UserPatient={");
		if (id != null)
			builder.append('#').append(id).append(',');
		if (principal != null)
			builder.append("principal=").append(principal).append(',');
		if (credentials != null)
			builder.append("credentials=").append(credentials).append(',');
		if (secretQuestion != null)
			builder.append("secretQuestion=").append(secretQuestion).append(',');
		if (secretAnswer != null)
			builder.append("secretAnswer=").append(secretAnswer).append(',');
		if (termsAcceptedTime != null)
			builder.append("termsAcceptedTime=").append(termsAcceptedTime).append(',');
		if (countryCode != null)
			builder.append("countryCode=").append(countryCode).append(',');
		if ((privatePostalCode != null) && (privatePostalCode.shortValue() > 0))
			builder.append("privatePostalCode=").append(privatePostalCode).append(',');
		if (termsAcceptedTime != null)
			builder.append("termsAcceptedTime=").append(termsAcceptedTime).append(',');
		if (patientHistoryLastModified != null)
			builder.append("patientHistoryLastModified=").append(patientHistoryLastModified).append(',');
		if (patientHistoryComplete)
			builder.append("patientHistoryComplete,");
		if (alertNewTrialsEnabled)
			builder.append("alertNewTrialsEnabled,");
		if (screenedTrialsLastModified != null)
			builder.append("screenedTrialsLastModified=").append(screenedTrialsLastModified).append(',');
		builder.append("history=").append(patientHistory);
		if (!screenedTrials.isEmpty())
			builder.append(",trials=").append(screenedTrials.size());
		if (!invitations.isEmpty())
			builder.append(",invitations=").append(invitations.size());
		return builder.append('}').toString();
	}

	/**
	 * Returns a unique identifier for the patient, guaranteed to be non-
	 * <code>null</code>. The priority is the <code>id</code>, the
	 * <code>termsAcceptedTime</code> and finally the <code>hashCode()</code>.
	 * 
	 * @return a unique identifier for the patient, guaranteed to be non-
	 *         <code>null</code>
	 */
	@Transient
	public Long getUniqueId()
	{
		if (id != null)
			return id;
		if (termsAcceptedTime != null)
			return Long.valueOf(termsAcceptedTime.getTime());
		return Long.valueOf(hashCode());
	}

	/**
	 * @return the id's hash or the superclass' if id is <code>null</code>
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return (id == null) ? super.hashCode() : id.hashCode();
	}

	/**
	 * @return <code>true</code> if the objects are equal or their ids are equal
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (!(obj instanceof UserPatient))
			return false;
		UserPatient other = (UserPatient) obj;
		if (id == null)
		{
			if (other.id != null)
				return false;
		}
		else if (!id.equals(other.id))
			return false;
		return true;
	}

	/**
	 * Returns a trimmed string or <code>null</code> if the string is empty
	 * 
	 * @param string
	 *            the string to trim
	 * @return a trimmed string or <code>null</code> if the string is empty
	 */
	private static String trim(String string)
	{
		if (string == null)
			return null;
		string = string.trim();
		return (string.length() == 0) ? null : string;
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = 2008413461209640037L;
	
	/**
	 * For Testing Purposes Only.  use the pgAdmin tool to select a userpatient.
	 * Highlight and copy the patienthistorymarshalled field and save it in a file which is assigned to the
	 * file reference variable below.
	 * @param argv not used.
	 */
	public static void main( String...argv )
	{
		File file = new File("/temp/patientHistory.xml");
		try
		{
			System.err.println( "Starting Test." );
			FileReader fr = new FileReader(file);
			char[] buf = new char[ (int) file.length() ];
			fr.read( buf );
			fr.close();
			
			UserPatient testPatient = new UserPatient();
			testPatient.setPatientHistoryMarshaled( new String( buf ) );
			PatientHistory ph = testPatient.getPatientHistory();
			Set<Diagnosis> diagnosis =  ph.getDiagnoses();
			System.err.println( "Diagnosis has " + diagnosis.size() + " members");
			for ( Diagnosis d: diagnosis )
			{
			    System.err.println( d.getValueHistory() );
			    System.err.println( d.getBinaryHistory() );
			    System.err.println( d.getShortHistory() );
			    System.err.println( d.getAttributeCharacteristics() );
			    System.err.println( "==================" );
			}
			
			System.err.println( "Finishing Test." );
		}
		catch ( Exception e )
		{
			e.printStackTrace();
		}
	}
}
