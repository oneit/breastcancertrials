package org.quantumleaphealth.model.patient;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.quantumleaphealth.ontology.AttributeCharacteristicHolder;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * This class contains all the avatar configurations.  These are saved separately from
 * the PatientHistory in the userpatient.avatarhistory text field.
 * 
 * @author wgweis
 */
public class AvatarHistory implements AttributeCharacteristicHolder, Serializable
{
	/**
	 * Year-month characteristics, guaranteed to be non-<code>null</code>
	 */
	private YearMonthHistory yearMonthHistory = new YearMonthHistory();
	
	/**
	 * for serialization 
	 */
	private static final long serialVersionUID = 3941973377152161717L;
	
	/**
	 * Attribute characteristics, guaranteed to be non-<code>null</code>
	 */
	private Map<CharacteristicCode, Set<CharacteristicCode>> attributeCharacteristics = new HashMap<CharacteristicCode, Set<CharacteristicCode>>();

	/**
	 * Check box indicating that we are to treat StageIII as No Evidence of Local Spread, rather than giving an avatar pass for this avatar. 
	 */
	private boolean noLocalSpreadOfDisease = false;
	
	/**
	 * @return the attributeCharacteristics
	 */
	public Map<CharacteristicCode, Set<CharacteristicCode>> getAttributeCharacteristics()
	{
		return attributeCharacteristics;
	}

	/**
	 * @param attributeCharacteristics
	 *            the attributeCharacteristics to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setAttributeCharacteristics(Map<CharacteristicCode, Set<CharacteristicCode>> attributeCharacteristics)
			throws IllegalArgumentException
	{
		if (attributeCharacteristics == null)
			throw new IllegalArgumentException("parameter not specified");
		this.attributeCharacteristics = attributeCharacteristics;
	}

	/**
	 * Add an attribute.
	 */
	@Override
	public void addAttribute(CharacteristicCode characteristicCode, CharacteristicCode attributeCode)
	{
		if ((characteristicCode == null) || (attributeCode == null))
			throw new IllegalArgumentException("characteristic or attribute not specified");
		Set<CharacteristicCode> set = attributeCharacteristics.get(characteristicCode);
		if (set == null)
		{
			set = new HashSet<CharacteristicCode>();
			attributeCharacteristics.put(characteristicCode, set);
		}
		set.add(attributeCode);
	}

	/**
	 * Removes an attribute from a characteristic
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to add the attribute to
	 * @param attributeCode
	 *            the code of the attribute to add to the characteristic
	 */
	@Override
	public void removeAttribute(CharacteristicCode characteristicCode, CharacteristicCode attributeCode)
	{
		if ((characteristicCode == null) || (attributeCode == null))
			throw new IllegalArgumentException("characteristic or attribute not specified");
		// Remove the attribute from the set if not already removed
		Set<CharacteristicCode> set = attributeCharacteristics.get(characteristicCode);
		if (set == null)
			return;
		set.remove(attributeCode);
		// Remove the entire set if no more attributes
		if (set.size() == 0)
			attributeCharacteristics.remove(characteristicCode);
	}
	
	public boolean containsAttribute(CharacteristicCode characteristicCode, CharacteristicCode attributeCode)
	{
		if (characteristicCode == null)
			throw new IllegalArgumentException("characteristic not specified");
		Set<CharacteristicCode> set = attributeCharacteristics.get(characteristicCode);
		return (attributeCode != null) && (set != null) && set.contains(attributeCode);
	}

	@Override
	public void setString(CharacteristicCode characteristicCode, String string) 
	{
		//not used.
	}

	@Override
	public String getString(CharacteristicCode characteristicCode) 
	{
		// not used.
		return null;
	}

	/**
	 * @param yearMonthHistory the yearMonthHistory to set
	 */
	public void setYearMonthHistory(YearMonthHistory yearMonthHistory) 
	{
		this.yearMonthHistory = yearMonthHistory;
	}

	/**
	 * @return the yearMonthHistory
	 */
	public YearMonthHistory getYearMonthHistory() 
	{
		return yearMonthHistory;
	}

	/**
	 * @return empty set if nothing found, otherwise set of avatar stage codes.
	 */
	public Set<CharacteristicCode> getAvatarStageCharacteristics()
	{
		Set<CharacteristicCode> stages = null;
		if (getAttributeCharacteristics() != null)
		{
			stages = getAttributeCharacteristics().get(STAGE);
		}
		return stages == null? new HashSet<CharacteristicCode>(): stages;
	}

	/**
	 * @param noLocalSpreadOfDisease the noLocalSpreadOfDisease to set
	 */
	public void setNoLocalSpreadOfDisease(boolean noLocalSpreadOfDisease) 
	{
		this.noLocalSpreadOfDisease = noLocalSpreadOfDisease;
	}

	/**
	 * @return the noLocalSpreadOfDisease
	 */
	public boolean isNoLocalSpreadOfDisease() 
	{
		return noLocalSpreadOfDisease;
	}
}
