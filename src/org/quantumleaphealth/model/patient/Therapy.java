/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient;

import java.io.Serializable;
import java.util.Arrays;

import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Represents a therapy. This serializable JavaBean exposes its data via getters
 * and setters.
 * 
 * @author Tom Bechtold
 * @version 2008-06-10
 */
public class Therapy implements Serializable
{
	/**
	 * Type of therapy
	 */
	private CharacteristicCode type;
	/**
	 * One or more agents used in the treatment.
	 */
	private CharacteristicCode[] agents;
	/**
	 * The year and month that the procedure was started, guaranteed to be
	 * non-null.
	 */
	private YearMonth started = new YearMonth();
	/**
	 * The setting or <code>null</code> if not answered yet.
	 */
	private CharacteristicCode setting;
	/**
	 * The current status or <code>null</code> if not answered yet.
	 */
	private CharacteristicCode status;
	/**
	 * The reason for incompletion or <code>null</code> if not answered yet or
	 * not applicable.
	 */
	private CharacteristicCode incomplete;
	/**
	 * The year and month that the procedure was completed, guaranteed to be
	 * non-null.
	 */
	private YearMonth completed = new YearMonth();
	/**
	 * The therapy length or <code>0</code> if not answered yet or not relevant.
	 * Each bit represents whether a choice is selected and depends upon the
	 * therapy type. Temporal type:
	 * <ol>
	 * <li>1 month or less
	 * <li>2-3 months
	 * <li>4-12 months
	 * <li>1 year or more
	 * </ol>
	 * Cyclical type:
	 * <ol>
	 * <li>1 cycle
	 * <li>2 cycles
	 * <li>3 cycles
	 * <li>More than 3 cycles
	 * </ol>
	 */
	private byte length;
	/**
	 * Whether metastases progressed
	 */
	private boolean progression;

	/**
	 * @return the type
	 */
	public CharacteristicCode getType()
	{
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(CharacteristicCode type)
	{
		this.type = type;
	}

	/**
	 * @return the agents
	 */
	public CharacteristicCode[] getAgents()
	{
		return agents;
	}

	/**
	 * @param agents
	 *            the agents
	 */
	public void setAgents(CharacteristicCode[] agents)
	{
		this.agents = agents;
	}

	/**
	 * @return the year and month that the procedure was started
	 */
	public YearMonth getStarted()
	{
		return started;
	}

	/**
	 * @param yearMonth
	 *            the year and month that the procedure was started
	 */
	public void setStarted(YearMonth started)
	{
		this.started = (started == null) ? new YearMonth() : started;
	}

	/**
	 * @return the setting or <code>null</code> if not answered yet
	 */
	public CharacteristicCode getSetting()
	{
		return setting;
	}

	/**
	 * @param setting
	 *            the setting to set or <code>null</code> if not answered yet
	 */
	public void setSetting(CharacteristicCode setting)
	{
		this.setting = setting;
	}

	/**
	 * @return the status or <code>null</code> if not answered yet
	 */
	public CharacteristicCode getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *            the status to set or <code>null</code> if not answered yet
	 */
	public void setStatus(CharacteristicCode status)
	{
		this.status = status;
	}

	/**
	 * @return the reason for incompletion or <code>null</code> if not answered
	 *         yet
	 */
	public CharacteristicCode getIncomplete()
	{
		return incomplete;
	}

	/**
	 * @param incomplete
	 *            the reason for incompletion or <code>null</code> if not
	 *            answered yet
	 */
	public void setIncomplete(CharacteristicCode incomplete)
	{
		this.incomplete = incomplete;
	}

	/**
	 * @return the year and month that the procedure was completed
	 */
	public YearMonth getCompleted()
	{
		return completed;
	}

	/**
	 * @param yearMonth
	 *            the year and month that the procedure was completed
	 */
	public void setCompleted(YearMonth completed)
	{
		this.completed = (completed == null) ? new YearMonth() : completed;
	}

	/**
	 * @return the therapy length or <code>0</code> if not answered yet
	 */
	public byte getLength()
	{
		return length;
	}

	/**
	 * @param length
	 *            the therapy length or <code>0</code> if not answered yet
	 */
	public void setLength(byte length)
	{
		this.length = length;
	}

	/**
	 * @return the progression
	 */
	public boolean isProgression()
	{
		return progression;
	}

	/**
	 * @param progression
	 *            the progression to set
	 */
	public void setProgression(boolean progression)
	{
		this.progression = progression;
	}

	/**
	 * Returns the values of the fields
	 * 
	 * @return the values of the fields
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder("Therapy={");
		if (agents != null)
			builder.append("agents=").append(Arrays.toString(agents)).append(',');
		if ((started.getMonth() > 0) || ((started.getYear() != null) && (started.getYear().shortValue() > 0)))
			builder.append("started=").append(started.getMonth()).append('/').append(started.getYear()).append(',');
		if (setting != null)
			builder.append("setting=").append(setting).append(',');
		if (status != null)
			builder.append("status=").append(status).append(',');
		if (incomplete != null)
			builder.append("incomplete=").append(incomplete).append(',');
		if ((completed.getMonth() > 0) || ((completed.getYear() != null) && (completed.getYear().shortValue() > 0)))
			builder.append("completed=").append(completed.getMonth()).append('/').append(completed.getYear()).append(
					',');
		if (length > 0)
			builder.append("length=").append(length).append(',');
		if (progression)
			builder.append("progression").append(progression);
		return builder.append('}').toString();
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = 2865614829316531875L;
}
