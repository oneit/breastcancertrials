/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;
import static javax.persistence.TemporalType.TIMESTAMP;
import javax.persistence.Transient;

import org.quantumleaphealth.model.trial.TrialSite;

/**
 * Invitation for a site to view a patient's health history
 * 
 * @author Tom Bechtold
 * @version 2009-07-07
 */
@Entity
public class Invitation implements Serializable
{
	/**
	 * Unique identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "InvitationID")
	@SequenceGenerator(name = "InvitationID", sequenceName = "invitation_id_seq", allocationSize = 1)
	private Long id;

	/**
	 * The patient
	 */
	@ManyToOne
	private UserPatient userPatient;

	/**
	 * The identifier for the trial
	 * 
	 * @see #setTrialSite(TrialSite)
	 */
	@Column(name = "TrialSite_Trial_id")
	private Long trialId;
	/**
	 * The identifier for the site
	 * 
	 * @see #setTrialSite(TrialSite)
	 */
	@Column(name = "TrialSite_Site_id")
	private Long siteId;
	/**
	 * The trial site. This field will not be managed by Java Persistence.
	 * 
	 * @see #setTrialSite(TrialSite)
	 */
	@Transient
	private TrialSite trialSite;

	/**
	 * The patient's name
	 */
	private String patientName;
	/**
	 * How to contact the patient, e.g., email address and/or telephone number
	 */
	private String patientContact;
	/**
	 * Message
	 */
	private String message;

	/**
	 * The date the invitation was created, guaranteed to be non-
	 * <code>null</code>
	 */
	@Temporal(TIMESTAMP)
	private Date created;
	/**
	 * The date the invitation was sent or <code>null</code> if not sent yet
	 */
	@Temporal(TIMESTAMP)
	private Date sent;
	/**
	 * The date the invitation was viewed or <code>null</code> if not viewed yet
	 */
	@Temporal(TIMESTAMP)
	private Date viewed;
	/**
	 * The date the most recent reminder of the invitation was sent or
	 * <code>null</code> if never reminded
	 */
	@Temporal(TIMESTAMP)
	private Date lastReminder;

	/**
	 * The date the invitation was disposed or <code>null</code> if not disposed
	 * yet
	 */
	@Temporal(TIMESTAMP)
	private Date dispositionDate;

	/**
	 * The date the invitation was disposed or <code>null</code> if not disposed
	 * yet
	 */
	@Temporal(TIMESTAMP)
	private Date helpDeskNotified;

	/**
	 * Set creation date to now
	 */
	public Invitation()
	{
		created = new Date();
	}

	/**
	 * @return the unique identifier
	 */
	public Long getId()
	{
		return id;
	}

	/**
	 * @param id
	 *            the unique identifier to set
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * @return the patient
	 */
	public UserPatient getUserPatient()
	{
		return userPatient;
	}

	/**
	 * @param userPatient
	 *            the patient to set
	 */
	public void setUserPatient(UserPatient userPatient)
	{
		this.userPatient = userPatient;
	}

	/**
	 * @return the identifier for the trial
	 */
	public Long getTrialId()
	{
		return trialId;
	}

	/**
	 * @return the identifier for the site
	 */
	public Long getSiteId()
	{
		return siteId;
	}

	/**
	 * @return the trial site
	 */
	public TrialSite getTrialSite()
	{
		return trialSite;
	}

	/**
	 * @param trialSite
	 *            the trial site to set
	 */
	public void setTrialSite(TrialSite trialSite)
	{
		this.trialSite = trialSite;
		// Identifier precedence is the unique id then the objects' ids
		trialId = null;
		siteId = null;
		if (trialSite == null)
			return;
		if (trialSite.getId() != null)
		{
			trialId = Long.valueOf(trialSite.getId().trialId);
			siteId = Long.valueOf(trialSite.getId().siteId);
			return;
		}
		if (trialSite.getTrial() != null)
			trialId = trialSite.getTrial().getId();
		if (trialSite.getSite() != null)
			siteId = trialSite.getSite().getId();
	}

	/**
	 * @return the patient's name
	 */
	public String getPatientName()
	{
		return patientName;
	}

	/**
	 * @param patientName
	 *            the patient's name, stored as <code>null</code> if empty
	 */
	public void setPatientName(String patientName)
	{
		this.patientName = trim(patientName);
	}

	/**
	 * @return how to contact the patient, e.g., email address and/or telephone
	 *         number
	 */
	public String getPatientContact()
	{
		return patientContact;
	}

	/**
	 * @param patientContact
	 *            how to contact the patient, e.g., email address and/or
	 *            telephone number; stored as <code>null</code> if empty
	 */
	public void setPatientContact(String patientContact)
	{
		this.patientContact = trim(patientContact);
	}

	/**
	 * @return the message
	 */
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *            the message to set, stored as <code>null</code> if empty
	 */
	public void setMessage(String message)
	{
		this.message = trim(message);
	}

	/**
	 * @return the creation date, guaranteed to be non-<code>null</code>
	 */
	public Date getCreated()
	{
		return created;
	}

	/**
	 * @return the date the invitation was sent or <code>null</code> if not sent
	 *         yet
	 */
	public Date getSent()
	{		
		return sent;
	}

	/**
	 * @param sent
	 *            the date the invitation was sent or <code>null</code> if not
	 *            sent yet
	 */
	public void setSent(Date sent)
	{
		this.sent = sent;
	}

	/**
	 * @return the date the invitation was viewed or <code>null</code> if not
	 *         viewed yet
	 */
	public Date getViewed()
	{
		return viewed;
	}

	/**
	 * @param viewed
	 *            date the invitation was viewed or <code>null</code> if not
	 *            viewed yet
	 */
	public void setViewed(Date viewed)
	{
		this.viewed = viewed;
	}

	/**
	 * @return the date the most recent reminder of the invitation was sent or
	 *         <code>null</code> if never reminded
	 */
	public Date getLastReminder()
	{
		return lastReminder;
	}

	/**
	 * @param lastReminder
	 *            the date the most recent reminder of the invitation was sent
	 *            or <code>null</code> if never reminded
	 */
	public void setLastReminder(Date lastReminder)
	{
		this.lastReminder = lastReminder;
	}

	/**
	 * @return the date the invitation was disposed or <code>null</code> if not
	 *         disposed yet
	 */
	public Date getDispositionDate()
	{
		return dispositionDate;
	}

	/**
	 * @param dispositionDate
	 *            the date the invitation was disposed or <code>null</code> if
	 *            not disposed yet
	 */
	public void setDispositionDate(Date dispositionDate)
	{
		this.dispositionDate = dispositionDate;
	}

	/**
	 * @param helpDeskNotified the helpDeskNotified to set
	 */
	public void setHelpDeskNotified(Date helpDeskNotified)
	{
		this.helpDeskNotified = helpDeskNotified;
	}

	/**
	 * @return the helpDeskNotified
	 */
	public Date getHelpDeskNotified()
	{
		return helpDeskNotified;
	}

	/**
	 * @return whether the identifier matches another's
	 * @param other
	 *            the other object
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other)
	{
		if (super.equals(other))
			return true;
		if (!(other instanceof Invitation))
			return false;
		return (id != null) && (id.longValue() != 0) && id.equals(((Invitation) (other)).id);
	}

	/**
	 * Returns the identifier's hashcode
	 * 
	 * @see java.lang.Object#hashCode()
	 * @see #id
	 */
	@Override
	public int hashCode()
	{
		return (id == null) || (id.longValue() == 0) ? super.hashCode() : id.hashCode();
	}

	/**
	 * @param string
	 *            the string to trim
	 * @return the trimmed string or <code>null</code> if the string is empty
	 */
	private static String trim(String string)
	{
		if (string == null)
			return null;
		string = string.trim();
		return (string.length() == 0) ? null : string;
	}

	/**
	 * Version ID for serializable class
	 */
	private static final long serialVersionUID = 4990217291733461258L;
}
