/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.model.patient;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.*;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.quantumleaphealth.ontology.AttributeCharacteristicHolder;
import org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.xml.DecodingException;

/**
 * Represents a patient's history. This serializable JavaBean exposes its data
 * via characteristic codes, diagnoses, procedures and therapies.
 * 
 * @author Tom Bechtold
 * @version 2008-07-01
 */
public class PatientHistory implements AttributeCharacteristicHolder, Serializable
{
    /**
     * UserPatient History type.  So far there are only two:  normal and avatar.
     */
    private UserPatient.UserType userType = UserPatient.UserType.USER_PATIENT;

	/**
	 * Binary characteristics, guaranteed to be non-<code>null</code>
	 */
	private BinaryHistory binaryHistory = new BinaryHistory();
	/**
	 * Value characteristics, guaranteed to be non-<code>null</code>
	 */
	private ValueHistory valueHistory = new ValueHistory();
	/**
	 * Attribute characteristics, guaranteed to be non-<code>null</code>
	 */
	private Map<CharacteristicCode, Set<CharacteristicCode>> attributeCharacteristics = new HashMap<CharacteristicCode, Set<CharacteristicCode>>();
	/**
	 * String characteristics, guaranteed to be non-<code>null</code>
	 */
	private Map<CharacteristicCode, String> stringCharacteristics = new HashMap<CharacteristicCode, String>(4);
	/**
	 * Year-month characteristics, guaranteed to be non-<code>null</code>
	 */
	private YearMonthHistory yearMonthHistory = new YearMonthHistory();

	/**
	 * Diagnoses, guaranteed to be non-<code>null</code>
	 */
	private Set<Diagnosis> diagnoses = new HashSet<Diagnosis>(1, 1f);

	/**
	 * Procedures, guaranteed to be non-<code>null</code>
	 */
	private Set<Procedure> procedures = new HashSet<Procedure>(1, 1f);

	/**
	 * Therapies, guaranteed to be non-<code>null</code>.
	 */
	private Set<Therapy> therapies = new HashSet<Therapy>(1, 1f);

	/**
	 * @return the binary characteristics
	 */
	public BinaryHistory getBinaryHistory()
	{
		return binaryHistory;
	}

	/**
	 * @param binaryHistory
	 *            the binary characteristics to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setBinaryHistory(BinaryHistory binaryHistory) throws IllegalArgumentException
	{
		if (binaryHistory == null)
			throw new IllegalArgumentException("parameter not specified");
		this.binaryHistory = binaryHistory;
	}

	/**
	 * @return the value characteristics
	 */
	public ValueHistory getValueHistory()
	{
		return valueHistory;
	}

	/**
	 * @param valueHistory
	 *            the value characteristics to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setValueHistory(ValueHistory valueHistory) throws IllegalArgumentException
	{
		if (valueHistory == null)
			throw new IllegalArgumentException("parameter not specified");
		this.valueHistory = valueHistory;
	}

	/**
	 * @return the attributeCharacteristics
	 */
	public Map<CharacteristicCode, Set<CharacteristicCode>> getAttributeCharacteristics()
	{
		return attributeCharacteristics;
	}

	/**
	 * @param attributeCharacteristics
	 *            the attributeCharacteristics to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setAttributeCharacteristics(Map<CharacteristicCode, Set<CharacteristicCode>> attributeCharacteristics)
			throws IllegalArgumentException
	{
		if (attributeCharacteristics == null)
			throw new IllegalArgumentException("parameter not specified");
		this.attributeCharacteristics = attributeCharacteristics;
	}

	/**
	 * Adds an attribute to a characteristic
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to add the attribute to
	 * @param attributeCode
	 *            the code of the attribute to add to the characteristic
	 * @see org.quantumleaphealth.ontology.AttributeCharacteristicHolder#addAttribute(org.quantumleaphealth.ontology.CharacteristicCode,
	 *      org.quantumleaphealth.ontology.CharacteristicCode)
	 */
	public void addAttribute(CharacteristicCode characteristicCode, CharacteristicCode attributeCode)
	{
		if ((characteristicCode == null) || (attributeCode == null))
			throw new IllegalArgumentException("characteristic or attribute not specified");
		Set<CharacteristicCode> set = attributeCharacteristics.get(characteristicCode);
		if (set == null)
		{
			set = new HashSet<CharacteristicCode>();
			attributeCharacteristics.put(characteristicCode, set);
		}
		set.add(attributeCode);
	}

	/**
	 * Removes an attribute from a characteristic
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to add the attribute to
	 * @param attributeCode
	 *            the code of the attribute to add to the characteristic
	 */
	public void removeAttribute(CharacteristicCode characteristicCode, CharacteristicCode attributeCode)
	{
		if ((characteristicCode == null) || (attributeCode == null))
			throw new IllegalArgumentException("characteristic or attribute not specified");
		// Remove the attribute from the set if not already removed
		Set<CharacteristicCode> set = attributeCharacteristics.get(characteristicCode);
		if (set == null)
			return;
		set.remove(attributeCode);
		// Remove the entire set if no more attributes
		if (set.size() == 0)
			attributeCharacteristics.remove(characteristicCode);
	}

	/**
	 * Returns whether a characteristic contains an attribute
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to find the attribute for
	 * @param attributeCode
	 *            the code of the attribute to find within the characteristic
	 * @return whether a characteristic contains an attribute
	 * @see org.quantumleaphealth.ontology.AttributeCharacteristicHolder#containsAttribute(org.quantumleaphealth.ontology.CharacteristicCode,
	 *      org.quantumleaphealth.ontology.CharacteristicCode)
	 */
	public boolean containsAttribute(CharacteristicCode characteristicCode, CharacteristicCode attributeCode)
	{
		if (characteristicCode == null)
			throw new IllegalArgumentException("characteristic not specified");
		Set<CharacteristicCode> set = attributeCharacteristics.get(characteristicCode);
		return (attributeCode != null) && (set != null) && set.contains(attributeCode);
	}

	/**
	 * @return the string characteristics, guaranteed to be non-
	 *         <code>null</code>
	 */
	public Map<CharacteristicCode, String> getStringCharacteristics()
	{
		return stringCharacteristics;
	}

	/**
	 * @param stringCharacteristics
	 *            the string characteristics to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setStringCharacteristics(Map<CharacteristicCode, String> stringCharacteristics)
			throws IllegalArgumentException
	{
		if (stringCharacteristics == null)
			throw new IllegalArgumentException("parameter not specified");
		this.stringCharacteristics = stringCharacteristics;
	}

	/**
	 * Sets a string value for a characteristic
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to set the string for
	 * @param string
	 *            the string to set to the characteristic or <code>null</code>
	 *            to remove any string
	 * @see org.quantumleaphealth.ontology.StringCharacteristicHolder#setString(org.quantumleaphealth.ontology.CharacteristicCode,
	 *      java.lang.String)
	 */
	public void setString(CharacteristicCode characteristicCode, String string)
	{
		if (string == null)
			stringCharacteristics.remove(characteristicCode);
		else
			stringCharacteristics.put(characteristicCode, string);
	}

	/**
	 * Returns the string that is set in a characteristic
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic
	 * @return the string that is set in a characteristic or <code>null</code>
	 *         if no string is set
	 * @see org.quantumleaphealth.ontology.StringCharacteristicHolder#getString(org.quantumleaphealth.ontology.CharacteristicCode)
	 */
	public String getString(CharacteristicCode characteristicCode)
	{
		return stringCharacteristics.get(characteristicCode);
	}

	/**
	 * @return the year-month characteristics, guaranteed to be non-
	 *         <code>null</code>
	 */
	public YearMonthHistory getYearMonthHistory()
	{
		return yearMonthHistory;
	}

	/**
	 * @param yearMonthHistory
	 *            the year-month characteristics to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setYearMonthHistory(YearMonthHistory yearMonthHistory) throws IllegalArgumentException
	{
		if (yearMonthHistory == null)
			throw new IllegalArgumentException("parameter not specified");
		this.yearMonthHistory = yearMonthHistory;
	}

	/**
	 * @return the diagnoses, guaranteed to be non-<code>null</code>
	 */
	public Set<Diagnosis> getDiagnoses()
	{
		return diagnoses;
	}

	/**
	 * @param diagnoses
	 *            the diagnoses to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setDiagnoses(Set<Diagnosis> diagnoses) throws IllegalArgumentException
	{
		if (diagnoses == null)
			throw new IllegalArgumentException("parameter not specified");
		this.diagnoses = diagnoses;
	}

	/**
	 * @return the procedures, guaranteed to be non-<code>null</code>
	 */
	public Set<Procedure> getProcedures()
	{
		return procedures;
	}

	/**
	 * @param procedures
	 *            the procedures to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setProcedures(Set<Procedure> procedures) throws IllegalArgumentException
	{
		if (procedures == null)
			throw new IllegalArgumentException("parameter not specified");
		this.procedures = procedures;
	}

	/**
	 * @return the therapies, guaranteed to be non-<code>null</code>
	 */
	public Set<Therapy> getTherapies()
	{
		return therapies;
	}

	/**
	 * @param therapies
	 *            the therapies to set
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setTherapies(Set<Therapy> therapies) throws IllegalArgumentException
	{
		if (therapies == null)
			throw new IllegalArgumentException("parameter not specified");
		this.therapies = therapies;
	}

	/**
	 * set the user the type.  There is no getter for this.  @see isAvatar()
	 * @param userType
	 */
    public void setUserType( UserPatient.UserType userType )
    {
    	this.userType = userType;
    }

    /**
     * Does this patient history belong to an avatar.  It needs to be defined here
     * because all the matchable classes use the history rather than the UserPatient object.
     * 
     * @return true if this is an avatar history, false otherwise.
     */
    public boolean isAvatar()
    {
    	return userType == null? false: userType.equals( UserPatient.UserType.AVATAR_PROFILE );
    }

	/*** Methods used by avatar logic ***/
	private Diagnosis findStageDiagnosis()
	{
		for (Diagnosis diagnosis : getDiagnoses())
		{
			if (LEFT_BREAST.equals(diagnosis.getValueHistory().getValue(ANATOMIC_SITE))  || RIGHT_BREAST.equals(diagnosis.getValueHistory().getValue(ANATOMIC_SITE)))
			{
				return diagnosis;
			}
		}
		return null;
	}

	private Diagnosis findBreastCancerDiagnosis()
	{
		for (Diagnosis diagnosis : getDiagnoses())
		{
			if (diagnosis.getAttributeCharacteristics().get(DIAGNOSISBREAST) != null)
			{
				return diagnosis;
			}
		}
		return null;
	}

	public String getPatientHistoryAsString(PatientHistory patientHistory)
	{
		UserPatient userPatient = new UserPatient();
		userPatient.setPatientHistory(patientHistory);
		return userPatient.getPatientHistoryMarshaled();
	}
	
	public static PatientHistory getPatientHistoryFromString(String patientHistory)
	{
		UserPatient userPatient = new UserPatient();
		try 
		{
			userPatient.setPatientHistoryMarshaled(patientHistory);
		} 
		catch (DecodingException e) 
		{
			e.printStackTrace();
		}
		return userPatient.getPatientHistory();
	}

	public CharacteristicCode getStage()
	{
		return findStageDiagnosis() == null? null: findStageDiagnosis().getValueHistory().getValue(STAGE);
	}
	
	public void injectStageHistory(CharacteristicCode stage)
	{
		if (stage == null)
		{
			return;
		}
		
		Diagnosis diagnosis = findStageDiagnosis();
		if (diagnosis == null)
		{
			diagnosis = new Diagnosis();
			diagnosis.getValueHistory().setValue(ANATOMIC_SITE, LEFT_BREAST);
			getDiagnoses().add(diagnosis);
		}
		diagnosis.getValueHistory().put(STAGE, stage);
		
		// STAGE_0 is the equivalent of ductal insitu.
		if (stage.equals(STAGE_0))
		{
			if (diagnosis.getAttributeCharacteristics().get(DIAGNOSISBREAST) == null)
			{
				diagnosis.getAttributeCharacteristics().put(DIAGNOSISBREAST, new HashSet<CharacteristicCode>());
				diagnosis.getAttributeCharacteristics().get(DIAGNOSISBREAST).add(DUCTAL_INSITU);
			}
		}
	}
	
	/*** End of Methods used for avatar eligibility profile searches ***/

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = -7202496977302796752L;
}
