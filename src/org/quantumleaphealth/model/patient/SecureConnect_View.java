package org.quantumleaphealth.model.patient;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;

@Entity
public class SecureConnect_View
{
//  SELECT i.id, '' AS source, i.sent, i.viewed, i.helpdesknotified, t.primaryid, s.name
	
	/**
	 * Unique identifier
	 */
	@Id
	private Long id;

	private String source;
	
	/**
	 * The date the invitation was sent or <code>null</code> if not sent yet
	 */
	@Temporal(TIMESTAMP)
	private Date sent;
	
	/**
	 * The date the invitation was viewed or <code>null</code> if not viewed yet
	 */
	@Temporal(TIMESTAMP)
	private Date viewed;
	
	/**
	 * The date the invitation was disposed or <code>null</code> if not disposed
	 * yet
	 */
	@Temporal(TIMESTAMP)
	private Date helpDeskNotified;

	/**
	 * Name
	 */
	private String name;

	/**
	 * The primary identifier
	 */
	private String primaryId;

	/**
	 * @param id the id to set
	 */
	public void setId(Long id)
	{
		this.id = id;
	}

	/**
	 * @return the id
	 */
	public Long getId()
	{
		return id;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source)
	{
		this.source = source;
	}

	/**
	 * @return the source
	 */
	public String getSource()
	{
		return source;
	}

	/**
	 * @param sent the sent to set
	 */
	public void setSent(Date sent)
	{
		this.sent = sent;
	}

	/**
	 * @return the sent
	 */
	public Date getSent()
	{
		return sent;
	}

	/**
	 * @param viewed the viewed to set
	 */
	public void setViewed(Date viewed)
	{
		this.viewed = viewed;
	}

	/**
	 * @return the viewed
	 */
	public Date getViewed()
	{
		return viewed;
	}

	/**
	 * @param helpDeskNotified the helpDeskNotified to set
	 */
	public void setHelpDeskNotified(Date helpDeskNotified)
	{
		this.helpDeskNotified = helpDeskNotified;
	}

	/**
	 * @return the helpDeskNotified
	 */
	public Date getHelpDeskNotified()
	{
		return helpDeskNotified;
	}

	/**
	 * @param primaryID the primaryID to set
	 */
	public void setPrimaryId(String primaryId)
	{
		this.primaryId = primaryId;
	}

	/**
	 * @return the primaryID
	 */
	public String getPrimaryId()
	{
		return primaryId;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name)
	{
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName()
	{
		return name;
	}
}
