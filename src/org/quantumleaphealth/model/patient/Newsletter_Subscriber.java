package org.quantumleaphealth.model.patient;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Temporal;

/**
 * Entity bean for the newsletter_subscriber table.
 * 
 * @author wgweis
 */
@Entity
public class Newsletter_Subscriber implements Serializable
{
	/**
	 * Unique identifier
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "Newsletter_SubscriberID")
	@SequenceGenerator(name = "Newsletter_SubscriberID", sequenceName = "newsletter_subscriber_id_seq", allocationSize = 1)
	private Long id;

	/**
	 * Email or <code>null</code> if none
	 */
	private String principal;

	/**
	 * Is the User subscribed or not.  The default value for a new record is true.
	 */
	private boolean subscribed = true;
	
	/**
	 * Whether or not the patient's principal is a valid email address
	 */
	private boolean principalInvalidEmailAddress;
	
	/**
	 * Timestamp for the creation of this record.  This property is for
	 * archiving only and will be set at the database level.
	 */
	@Column(name = "created", updatable = false, insertable = false)
	@Temporal(TIMESTAMP)
	private Date created;
	
	/**
	 * database user id of creator.  This property is for
	 * archiving only and will be set at the database level.
	 */
	@Column(name = "created_by", updatable = false, insertable = false)
	private String created_by;
	
	/**
	 * Timestamp for the modification of this record.
	 */
	@Temporal(TIMESTAMP)
	private Date modified;

	/**
	 * database user id of modifier
	 */
	private String modified_by;

	/**
	 * @param principal the principal to set
	 */
	public void setPrincipal(String principal) 
	{
		this.principal = principal;
	}

	/**
	 * @return the principal
	 */
	public String getPrincipal() 
	{
		return principal;
	}

	/**
	 * @param subscribed the subscribed to set
	 */
	public void setSubscribed(boolean subscribed) 
	{
		this.subscribed = subscribed;
	}

	/**
	 * @return the subscribed
	 */
	public boolean isSubscribed() 
	{
		return subscribed;
	}

	/**
	 * @param principalInvalidEmailAddress the principalInvalidEmailAddress to set
	 */
	public void setPrincipalInvalidEmailAddress(boolean principalInvalidEmailAddress) 
	{
		this.principalInvalidEmailAddress = principalInvalidEmailAddress;
	}

	/**
	 * @return the principalInvalidEmailAddress
	 */
	public boolean isPrincipalInvalidEmailAddress() 
	{
		return principalInvalidEmailAddress;
	}

	/**
	 * @param created the created to set
	 */
	public void setCreated(Date created) 
	{
		this.created = created;
	}

	/**
	 * @return the created
	 */
	public Date getCreated() 
	{
		return created;
	}

	/**
	 * @param created_by the created_by to set
	 */
	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	/**
	 * @return the created_by
	 */
	public String getCreated_by() {
		return created_by;
	}

	/**
	 * @param modified the modified to set
	 */
	public void setModified(Date modified) 
	{
		this.modified = modified;
	}

	/**
	 * @return the modified
	 */
	public Date getModified() 
	{
		return modified;
	}

	/**
	 * @param modified_by the modified_by to set
	 */
	public void setModified_by(String modified_by) 
	{
		this.modified_by = modified_by;
	}

	/**
	 * @return the modified_by
	 */
	public String getModified_by() 
	{
		return modified_by;
	}

	/**
	 * @return whether the identifier matches another's
	 * @param other
	 *            the other object
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other)
	{
		if (this == other)
		{
			return true;
		}
		if (!(other instanceof Newsletter_Subscriber))
		{
			return false;
		}
		return this.id.equals(((Newsletter_Subscriber) other).id);
	}
	
	/**
	 * Returns the identifier's hashcode
	 * 
	 * @see java.lang.Object#hashCode()
	 * @see #id
	 */
	@Override
	public int hashCode()
	{
		return (id == null) || (id.longValue() == 0) ? super.hashCode() : id.hashCode();
	}
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7757459465508922819L;
}
