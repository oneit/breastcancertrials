package org.quantumleaphealth.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TrialResource implements Serializable {
	
	private static final long serialVersionUID = -2038522576671073938L;

	@XmlElement
	private Long identifier;
	
	@XmlElement
	private String briefTitle;
	
	@XmlElement
	private String officialTitle;
	
	@XmlElement
	private String purpose;
	
	@XmlElement
	private String treatmentPlan;
	
	@XmlElement
	private String procedures;
	
	@XmlElement
	private String duration;
	
	@XmlElement
	private String followup;
	
	@XmlElement
	private String sponsor;
	
	@XmlElement
	private String primaryId;
	
	@XmlElement
	private String burden;
	
	@XmlElement
	private String phase;
	
	@XmlElement
	private Date postDate;
	
	@XmlElement
	private Date lastModifiedDate;

	@XmlElementWrapper(name="links")
	@XmlElement
	private List<TrialResourceLink> links = new ArrayList<TrialResourceLink>();
	
	@XmlElementWrapper(name="sites")
	@XmlElement
	private List<TrialResourceSite> sites = new ArrayList<TrialResourceSite>();

	@XmlElementWrapper(name="categories")
	@XmlElement
	private List<String> categories = new ArrayList<String>();

	public List<TrialResourceLink> getLinks() {
		return links;
	}

	public void setLinks(List<TrialResourceLink> links) {
		this.links = links;
	}

	public List<TrialResourceSite> getSites() {
		return sites;
	}

	public void setSites(List<TrialResourceSite> sites) {
		this.sites = sites;
	}
	
	public List<String> getCategories() {
		return categories;
	}

	public Long getIdentifier() {
		return identifier;
	}

	public void setIdentifier(Long identifier) {
		this.identifier = identifier;
	}

	public String getBriefTitle() {
		return briefTitle;
	}

	public void setBriefTitle(String briefTitle) {
		this.briefTitle = briefTitle;
	}

	public String getOfficialTitle() {
		return officialTitle;
	}

	public void setOfficialTitle(String officialTitle) {
		this.officialTitle = officialTitle;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getTreatmentPlan() {
		return treatmentPlan;
	}

	public void setTreatmentPlan(String treatmentPlan) {
		this.treatmentPlan = treatmentPlan;
	}

	public String getProcedures() {
		return procedures;
	}

	public void setProcedures(String procedures) {
		this.procedures = procedures;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getFollowup() {
		return followup;
	}

	public void setFollowup(String followup) {
		this.followup = followup;
	}

	public String getSponsor() {
		return sponsor;
	}

	public void setSponsor(String sponsor) {
		this.sponsor = sponsor;
	}

	public String getPrimaryId() {
		return primaryId;
	}

	public void setPrimaryId(String primaryId) {
		this.primaryId = primaryId;
	}

	public String getBurden() {
		return burden;
	}

	public void setBurden(String burden) {
		this.burden = burden;
	}

	public String getPhase() {
		return phase;
	}

	public void setPhase(String phase) {
		this.phase = phase;
	}

	public Date getPostDate() {
		return postDate;
	}

	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}
	
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}

	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
}
