package org.quantumleaphealth.model;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class TrialResourceSite {
	
	private String facilityName;
	private String city;
	private String politicalSubUnitName;
	private Integer zipCode;
	private String principalInvestigator;
	private String contactName;
	private String contactPhone;
	private String contactEmail;
	private int distance;
	
	public String getFacilityName() {
		return facilityName;
	}
	
	public void setFacilityName(String facilityName) {
		this.facilityName = facilityName;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	
	public String getPoliticalSubUnitName() {
		return politicalSubUnitName;
	}
	
	public void setPoliticalSubUnitName(String politicalSubUnitName) {
		this.politicalSubUnitName = politicalSubUnitName;
	}
	
	public Integer getZipCode() {
		return zipCode;
	}
	
	public void setZipCode(Integer zipCode) {
		this.zipCode = zipCode;
	}

	public String getPrincipalInvestigator() {
		return principalInvestigator;
	}

	public void setPrincipalInvestigator(String principalInvestigator) {
		this.principalInvestigator = principalInvestigator;
	}
	
	public String getContactName() {
		return contactName;
	}
	
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}
	
	public String getContactPhone() {
		return contactPhone;
	}
	
	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}
	
	public String getContactEmail() {
		return contactEmail;
	}
	
	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public int getDistance() {
		return distance;
	}

	public void setDistance(int distance) {
		this.distance = distance;
	}
}
