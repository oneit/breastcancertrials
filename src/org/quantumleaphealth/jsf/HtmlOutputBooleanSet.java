/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.jsf;

import java.io.IOException;
import java.text.DateFormatSymbols;
import java.util.LinkedList;
import java.util.ResourceBundle;

import javax.el.ValueExpression;
import javax.faces.FacesException;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

import org.quantumleaphealth.action.FacadeShort;
import org.quantumleaphealth.action.FacadeYearMonth;
import org.quantumleaphealth.action.FacadeYearMonthCharacteristic;

/**
 * Renders text of either single-selection or multiple-selection property. The
 * renderer functionality is bundled into this class because there will only be
 * one renderer for this component. The rendered text is a concatenation of
 * localized strings, each representing an on bit of the component's value.
 * 
 * @author Tom Bechtold
 * @version 2008-08-12
 */
public class HtmlOutputBooleanSet extends UIOutput
{

	// ************************ Expression Language Functions *********** /
	/**
	 * A blank string
	 */
	private static final String BLANK = "";
	/**
	 * A space
	 */
	private static final String SPACE = " ";
	/**
	 * The months in English, e.g., "January", "February", etc.
	 * 
	 * @see DateFormatSymbols#getMonths()
	 */
	private static final String[] MONTHS = new DateFormatSymbols().getMonths();

	/**
	 * Returns whether a bit is turned on in a number
	 * 
	 * @param number
	 *            the number whose bits will be examined
	 * @param bit
	 *            the bit to examine
	 * @return <code>true</code> if the number's bit is on
	 */
	public static boolean isSelectedBooleanSet(Number number, byte bit)
	{
		long value = (number == null) ? 0 : number.longValue();
		return (value & 1 << bit) != 0;
	}

	/**
	 * @return the non-localized string representation of a year and (optional)
	 *         month or <code>null</code> if year not provided
	 * @param facadeYearMonthCharacteristic
	 *            the holder of the year and month
	 * @param emptyYear
	 *            replaces the year if the year is not available
	 */
	public static String formatYearMonthCharacteristic(FacadeYearMonthCharacteristic facadeYearMonthCharacteristic,
			String emptyYear)
	{
		return formatYearMonth(coalesceShort(facadeYearMonthCharacteristic, emptyYear),
				facadeYearMonthCharacteristic == null ? -1 : facadeYearMonthCharacteristic.getByteValue());
	}

	/**
	 * @param facadeYearMonththe
	 *            holder of the year and month
	 * @param emptyYear
	 *            replaces the year if the year is not available
	 * @return the non-localized string representation of a year and (optional)
	 *         month or <code>null</code> if year not provided
	 */
	public static String formatYearMonth(FacadeYearMonth facadeYearMonth, String emptyYear)
	{
		return formatYearMonth(coalesceShort(facadeYearMonth, emptyYear), facadeYearMonth == null ? -1
				: facadeYearMonth.getMonth());
	}

	/**
	 * @param facadeShort
	 *            holds the string representation of a short value
	 * @param emptyString
	 *            the alternative if the string representation is not found
	 * @return <code>facadeShort.getShortValue()</code> if available,
	 *         <code>emptyString</code> otherwise
	 */
	public static String coalesceShort(FacadeShort facadeShort, String emptyString)
	{
		return (facadeShort == null) || (facadeShort.getShortValue() == null) ? emptyString : facadeShort
				.getShortValue();
	}

	/**
	 * @return the non-localized string representation of a year and (optional)
	 *         month or <code>null</code> if year not provided
	 * @param year
	 *            the year to display
	 * @param month
	 *            the month to display or non-positive if not displayed
	 */
	private static String formatYearMonth(String year, byte month)
	{
		// Validate year
		if (year != null)
			year = year.trim();
		if ((year == null) || (year.length() == 0))
			return null;
		StringBuilder builder = new StringBuilder(year);

		// Insert optional month in front; ensure it fits in array
		String monthString = null;
		if ((month > 0) && (month < MONTHS.length))
			monthString = MONTHS[month - 1];
		else if (month > 0)
			monthString = Byte.toString(month) + '-';
		if (monthString != null)
		{
			if (builder.length() > 0)
				builder.insert(0, ' ');
			builder.insert(0, monthString);
		}
		return builder.toString();
	}

	/**
	 * Returns a sentence where the <code>list</code> elements are separated by
	 * <code>separator</code> except for the last two elements, which are
	 * separated by <code>conjunction</code>.
	 * 
	 * @return sentence listing the items or <code>""</code> if
	 *         <code>number</code> is not positive
	 * @param number
	 *            the component's numeric value
	 * @param bundle
	 *            the resource bundle to use
	 * @param prefix
	 *            the prefix to prepend to the bit for the key to look up
	 *            localized string
	 * @param separator
	 *            the separator between two elements except the final two
	 * @param conjunction
	 *            the separator between the final two elements
	 * @return
	 */
	public static String formatBooleanSet(Number number, ResourceBundle bundle, String prefix, String separator,
			String conjunction, String emptyValue)
	{
		long value = (number == null) ? 0 : number.longValue();
		return (value == 0) ? emptyValue : buildSentence(buildList(value, bundle, prefix), separator, conjunction);
	}

	/**
	 * Returns a list of localized strings that correspond to the on bits of
	 * this component's value using the pattern. The order of the list is from
	 * bit 1 to highest-order bit, and then bit #0 is added last if set.
	 * 
	 * @return a list of localized strings, guaranteed to be non-
	 *         <code>null</code>
	 * @param number
	 *            the component's numeric value
	 * @param bundle
	 *            the resource bundle to use
	 * @param prefix
	 *            the prefix to prepend to the bit for the key to look up
	 *            localized string
	 */
	private static String[] buildList(long number, ResourceBundle bundle, String prefix)
	{
		if (number <= 0)
			return new String[0];
		// Build list from bit #1 to highest-order bit
		LinkedList<String> list = new LinkedList<String>();
		long bit = 1l;
		long mask = 2l;
		while (number > 1l)
		{
			if ((number & mask) != 0l)
			{
				list.add(lookupLocalizedString(bundle, prefix, bit));
				number -= mask;
			}
			bit++;
			mask <<= 1l;
		}
		// Add bit #0
		if (number == 1l)
			list.add(lookupLocalizedString(bundle, prefix, 0l));
		// Convert to array
		return list.toArray(new String[list.size()]);
	}

	/**
	 * Returns a sentence where the <code>list</code> elements are separated by
	 * <code>separator</code> except for the last two elements, which are
	 * separated by <code>conjunction</code>.
	 * 
	 * @param list
	 *            the list of items to build
	 * @param separator
	 *            the separator between two elements except the final two
	 * @param conjunction
	 *            the separator between the final two elements
	 * @return sentence listing the items or <code>null</code> if
	 *         <code>list</code> is <code>null</code>
	 */
	private static String buildSentence(String[] list, String separator, String conjunction)
	{
		if (list == null)
			return null;
		if (list.length == 0)
			return BLANK;
		if (list.length == 1)
			return list[0];
		// Validate separator and conjunction
		if (separator == null)
			separator = SPACE;
		if (conjunction == null)
			conjunction = SPACE;
		StringBuilder builder = new StringBuilder(list[0]);
		for (int index = 1; index < list.length - 1; index++)
			builder.append(separator).append(list[index]);
		builder.append(conjunction).append(list[list.length - 1]);
		return builder.toString();
	}

	/**
	 * Returns the localized text from the <code>resourceBundle</code> that is
	 * found using the key <code>prefix + bit</code>.
	 * 
	 * @param resourceBundle
	 *            contains localized text
	 * @param prefix
	 *            the prefix for the key to lookup the text
	 * @param bit
	 *            the number to append to the prefix
	 * @return the localized text or the key if the map, resource bundle or text
	 *         could not be found
	 */
	private static String lookupLocalizedString(ResourceBundle resourceBundle, String prefix, long bit)
	{
		String key = prefix + bit;
		try
		{
			return resourceBundle.getString(key);
		}
		catch (Exception exception)
		{
			// Return the key instead
			// TODO: Log a warning message
			return key;
		}
	}

	// ************************ Component Methods *********************** /

	/**
	 * Instantiate this object with <code>null</code> renderer type as it will
	 * render itself.
	 */
	public HtmlOutputBooleanSet()
	{
		super();
		setRendererType(null);
	}

	/**
	 * Returns <code>null</code> as this component does not belong to a family.
	 * 
	 * @return <code>null</code>
	 * @see javax.faces.component.UIInput#getFamily()
	 */
	@Override
	public String getFamily()
	{
		return null;
	}

	private String baseName;

	/**
	 * Return the value of the <code>baseName</code> property. Contents: Name of
	 * the resource bundle to look up localized strings.
	 */
	public String getBaseName()
	{
		if (null != this.baseName)
		{
			return this.baseName;
		}
		ValueExpression _ve = getValueExpression("baseName");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>baseName</code> property.
	 */
	public void setBaseName(String baseName)
	{
		this.baseName = baseName;
	}

	private ResourceBundle resourceBundle;

	/**
	 * Return the value of the <code>resourceBundle</code> property. Contents:
	 * Contains localized strings.
	 */
	public ResourceBundle getResourceBundle()
	{
		if (null != this.resourceBundle)
		{
			return this.resourceBundle;
		}
		ValueExpression _ve = getValueExpression("resourceBundle");
		if (_ve != null)
		{
			return (ResourceBundle) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>baseName</code> property.
	 */
	public void setResourceBundle(ResourceBundle resourceBundle)
	{
		this.resourceBundle = resourceBundle;
	}

	private String prefix;

	/**
	 * Return the value of the <code>prefix</code> property. Contents: Prefix of
	 * the key used to lookup localized value.
	 */
	public String getPrefix()
	{
		if (null != this.prefix)
		{
			return this.prefix;
		}
		ValueExpression _ve = getValueExpression("prefix");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>prefix</code> property.
	 */
	public void setPrefix(String prefix)
	{
		this.prefix = prefix;
	}

	private String separator;

	/**
	 * Return the value of the <code>separator</code> property. Contents: Text
	 * that separates two localized values.
	 */
	public String getSeparator()
	{
		if (null != this.separator)
		{
			return this.separator;
		}
		ValueExpression _ve = getValueExpression("separator");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>separator</code> property.
	 */
	public void setSeparator(String separator)
	{
		this.separator = separator;
	}

	private String conjunction;

	/**
	 * Return the value of the <code>conjunction</code> property. Contents:
	 * Separates the last from second-to-last word in list.
	 */
	public String getConjunction()
	{
		if (null != this.conjunction)
		{
			return this.conjunction;
		}
		ValueExpression _ve = getValueExpression("conjunction");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>conjunction</code> property.
	 */
	public void setConjunction(String conjunction)
	{
		this.conjunction = conjunction;
	}

	private String emptyValue;

	/**
	 * Return the value of the <code>emptyValue</code> property. Contents: Text
	 * that denotes an empty value.
	 */
	public String getEmptyValue()
	{
		if (null != this.emptyValue)
		{
			return this.emptyValue;
		}
		ValueExpression _ve = getValueExpression("emptyValue");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>emptyValue</code> property.
	 */
	public void setEmptyValue(String emptyValue)
	{
		this.emptyValue = emptyValue;
	}

	private String emptyClass;

	/**
	 * Return the value of the <code>emptyClass</code> property. Contents: Class
	 * that denotes an empty value.
	 */
	public String getEmptyClass()
	{
		if (null != this.emptyClass)
		{
			return this.emptyClass;
		}
		ValueExpression _ve = getValueExpression("emptyClass");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>emptyClass</code> property.
	 */
	public void setEmptyClass(String emptyClass)
	{
		this.emptyClass = emptyClass;
	}

	private String subjectiveClass;

	/**
	 * Return the value of the <code>subjectiveClass</code> property. Contents:
	 * Class that denotes an subjective value.
	 */
	public String getSubjectiveClass()
	{
		if (null != this.subjectiveClass)
		{
			return this.subjectiveClass;
		}
		ValueExpression _ve = getValueExpression("subjectiveClass");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>subjectiveClass</code> property.
	 */
	public void setSubjectiveClass(String subjectiveClass)
	{
		this.subjectiveClass = subjectiveClass;
	}

	private String dir;

	/**
	 * Return the value of the <code>dir</code> property. Contents: Direction
	 * indication for text that does not inherit directionality. Valid values
	 * are "LTR" (left-to-right) and "RTL" (right-to-left).
	 */
	public String getDir()
	{
		if (null != this.dir)
		{
			return this.dir;
		}
		ValueExpression _ve = getValueExpression("dir");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>dir</code> property.
	 */
	public void setDir(String dir)
	{
		this.dir = dir;
	}

	private String lang;

	/**
	 * Return the value of the <code>lang</code> property. Contents: Code
	 * describing the language used in the generated markup for this component.
	 */
	public String getLang()
	{
		if (null != this.lang)
		{
			return this.lang;
		}
		ValueExpression _ve = getValueExpression("lang");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>lang</code> property.
	 */
	public void setLang(String lang)
	{
		this.lang = lang;
	}

	private Object[] _values;

	/**
	 * Stores each local variable into the component's state
	 * 
	 * @return the component's state including local variables
	 * @param _context
	 *            the current faces context
	 * @see javax.faces.component.UIInput#saveState(javax.faces.context.FacesContext)
	 */
	@Override
	public Object saveState(FacesContext _context)
	{
		if (_values == null)
		{
			_values = new Object[11];
		}
		_values[0] = super.saveState(_context);
		_values[1] = baseName;
		_values[2] = resourceBundle;
		_values[3] = prefix;
		_values[4] = separator;
		_values[5] = conjunction;
		_values[6] = emptyValue;
		_values[7] = emptyClass;
		_values[8] = subjectiveClass;
		_values[9] = dir;
		_values[10] = lang;
		return _values;
	}

	/**
	 * Loads each local variable from the a saved state
	 * 
	 * @param _context
	 *            current faces context
	 * @param _state
	 *            object that saves component's state
	 * @see javax.faces.component.UIInput#restoreState(javax.faces.context.FacesContext,
	 *      java.lang.Object)
	 */
	@Override
	public void restoreState(FacesContext _context, Object _state)
	{
		_values = (Object[]) _state;
		super.restoreState(_context, _values[0]);
		this.baseName = (String) _values[1];
		this.resourceBundle = (ResourceBundle) _values[2];
		this.prefix = (String) _values[3];
		this.separator = (String) _values[4];
		this.conjunction = (String) _values[5];
		this.emptyValue = (String) _values[6];
		this.emptyClass = (String) _values[7];
		this.subjectiveClass = (String) _values[8];
		this.dir = (String) _values[9];
		this.lang = (String) _values[10];
	}

	// ************************ Renderer Methods *********************** /

	/**
	 * Writes the localized text corresponding to the prefix/id and it's value's
	 * on bits. If the value is non-positive then write the emptyValue
	 * 
	 * @see javax.faces.component.UIComponentBase#encodeEnd(javax.faces.context.FacesContext)
	 */
	@Override
	public void encodeEnd(FacesContext context) throws IOException
	{
		ResponseWriter writer = context.getResponseWriter();
		assert (writer != null);
		// If not empty (e.g., positive) then write sentence with localized
		// text, separator and conjunction
		Object value = getValue();
		long number = (value != null) && (value instanceof Number) ? ((Number) (value)).longValue() : 0l;
		if (number > 0)
		{
			// The resourceBundle parameter takes priority over the baseName
			ResourceBundle bundle = getResourceBundle();
			if (bundle == null)
			{
				String resourceBundleBaseName = getBaseName();
				try
				{
					bundle = ResourceBundle.getBundle(resourceBundleBaseName);
				}
				catch (Exception exception)
				{
					throw new FacesException("Cannot load resource bundle " + resourceBundleBaseName, exception);
				}
			}
			// If the prefix is not explicitly specified then use the id
			String keyPrefix = getPrefix();
			if (keyPrefix == null)
				keyPrefix = getId();
			// If subjective bit is set and subjective class is given then
			// enclose in span element
			String styleClass = ((number & 1l) != 0) ? getSubjectiveClass() : null;
			if (styleClass != null)
			{
				writer.startElement("span", this);
				writer.writeAttribute("class", styleClass, "class");
			}
			writer.write(buildSentence(buildList(number, bundle, keyPrefix), separator, conjunction));
			if (styleClass != null)
				writer.endElement("span");
			return;
		}
		// Otherwise write empty text with optional style class within an HTML
		// span element
		String text = getEmptyValue();
		if (text == null)
			return;
		String styleClass = getEmptyClass();
		if (styleClass != null)
		{
			writer.startElement("span", this);
			writer.writeAttribute("class", styleClass, "class");
		}
		writer.write(text);
		if (styleClass != null)
			writer.endElement("span");
	}
}
