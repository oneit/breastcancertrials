/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.jsf;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.el.ValueExpression;
import javax.faces.FacesException;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.component.UISelectItem;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.convert.Converter;
import javax.faces.model.SelectItem;

/**
 * Represents a set of boolean characteristics stored as bit locations in a
 * numeric value and represented by related HTML <code>input</code> elements.
 * Depending upon the <code>multiple</code> parameter, this class renders the
 * set of booleans either as single-selection <code>radio</code> buttons or
 * multiple-selection <code>checkbox</code>es.
 * <p>
 * This class differs from the JavaServer Pages <code>UISelectOne</code>
 * reference implementation in the following manners:
 * <ol>
 * <li>The renderer functionality is bundled into this class because there will
 * only be an HTML renderer for this component.</li>
 * <li>Unlike <code>UISelectOne</code>, this class does not create a validation
 * exception if the user does not select a child item.</li>
 * <li>In order to avoid the above-mentioned match-validation functionality of
 * <code>UISelectOne.validateValue()</code>, this class extends
 * <code>UIInput</code>.</li>
 * <li>In order to replicate the functionality of
 * <code>javax.faces.component.html.HtmlSelectOneRadio</code> this class copies
 * verbatim all of its methods except for <code>getRenderType</code>, which
 * returns <code>null</code> since this class does its own rendering.</li>
 * <li>Decoding depends upon whether radio buttons or check boxes are rendered.
 * If radio buttons, the <code>requestParameterMap</code> is decoded by itself
 * since the child radio elements will generate only one request parameter
 * value. If check boxes, the <code>requestParameterValuesMap</code> is parsed
 * for bit locations which are then turned on in the submitted value.</li>
 * <li>This class renders completely differently from
 * <code>com.sun.faces.renderkit.html_basic.RadioRenderer</code>. Rather than
 * forcing its child radio elements into an HTML layout, this class formats its
 * children exactly where they are laid out. This allows flexible positioning,
 * nested radio groups and non-radio elements to reside inside this component.</li>
 * <li>Because it does not lay out its children in an HTML table like
 * <code>com.sun.faces.renderkit.html_basic.RadioRenderer</code>, there is no
 * need for <code>layout</code> or <code>border</code> attributes.</li>
 * <li>For radio button group this class supports the notion of
 * "no selection value", which is the string value assigned to the component if
 * no child radio buttons are selected. This functionality is important so that
 * the underlying model is set to something even if the user does not select a
 * radio button. A converter (specified by the user or inferred from the
 * component's value) is used to convert the string to the appropriate class.
 * </ol>
 * 
 * @author Tom Bechtold
 * @version 2008-10-31
 * 
 */
public class HtmlSelectBooleanSet extends UIInput
{

	// ************************ Component Methods *********************** /

	/**
	 * Instantiate this object with <code>null</code> renderer type as it will
	 * render itself.
	 */
	public HtmlSelectBooleanSet()
	{
		super();
		setRendererType(null);
	}

	/**
	 * Returns <code>null</code> as this component does not belong to a family.
	 * 
	 * @return <code>null</code>
	 * @see javax.faces.component.UIInput#getFamily()
	 */
	@Override
	public String getFamily()
	{
		return null;
	}

	private String accesskey;

	/**
	 * Return the value of the <code>accesskey</code> property. Contents: Access
	 * key that, when pressed, transfers focus to this element.
	 */
	public String getAccesskey()
	{
		if (null != this.accesskey)
		{
			return this.accesskey;
		}
		ValueExpression _ve = getValueExpression("accesskey");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>accesskey</code> property.
	 */
	public void setAccesskey(String accesskey)
	{
		this.accesskey = accesskey;
	}

	private String dir;

	/**
	 * Return the value of the <code>dir</code> property. Contents: Direction
	 * indication for text that does not inherit directionality. Valid values
	 * are "LTR" (left-to-right) and "RTL" (right-to-left).
	 */
	public String getDir()
	{
		if (null != this.dir)
		{
			return this.dir;
		}
		ValueExpression _ve = getValueExpression("dir");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>dir</code> property.
	 */
	public void setDir(String dir)
	{
		this.dir = dir;
	}

	private boolean disabled = false;
	private boolean disabled_set = false;

	/**
	 * Return the value of the <code>disabled</code> property. Contents: Flag
	 * indicating that this element must never receive focus or be included in a
	 * subsequent submit. A value of false causes no attribute to be rendered,
	 * while a value of true causes the attribute to be rendered as
	 * disabled="disabled".
	 */
	public boolean isDisabled()
	{
		if (this.disabled_set)
		{
			return this.disabled;
		}
		ValueExpression _ve = getValueExpression("disabled");
		if (_ve != null)
		{
			Object _result = _ve.getValue(getFacesContext().getELContext());
			if (_result == null)
			{
				return false;
			}
			else
			{
				return ((Boolean) _result).booleanValue();
			}
		}
		else
		{
			return this.disabled;
		}
	}

	/**
	 * Set the value of the <code>disabled</code> property.
	 */
	public void setDisabled(boolean disabled)
	{
		this.disabled = disabled;
		this.disabled_set = true;
	}

	private String disabledClass;

	/**
	 * Return the value of the <code>disabledClass</code> property. Contents:
	 * CSS style class to apply to the rendered label on disabled options.
	 */
	public String getDisabledClass()
	{
		if (null != this.disabledClass)
		{
			return this.disabledClass;
		}
		ValueExpression _ve = getValueExpression("disabledClass");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>disabledClass</code> property.
	 */
	public void setDisabledClass(String disabledClass)
	{
		this.disabledClass = disabledClass;
	}

	private String enabledClass;

	/**
	 * Return the value of the <code>enabledClass</code> property. Contents: CSS
	 * style class to apply to the rendered label on enabled options.
	 */
	public String getEnabledClass()
	{
		if (null != this.enabledClass)
		{
			return this.enabledClass;
		}
		ValueExpression _ve = getValueExpression("enabledClass");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>enabledClass</code> property.
	 */
	public void setEnabledClass(String enabledClass)
	{
		this.enabledClass = enabledClass;
	}

	private String label;

	/**
	 * Return the value of the <code>label</code> property. Contents: A
	 * localized user presentable name for this component.
	 */
	public String getLabel()
	{
		if (null != this.label)
		{
			return this.label;
		}
		ValueExpression _ve = getValueExpression("label");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>label</code> property.
	 */
	public void setLabel(String label)
	{
		this.label = label;
	}

	private String lang;

	/**
	 * Return the value of the <code>lang</code> property. Contents: Code
	 * describing the language used in the generated markup for this component.
	 */
	public String getLang()
	{
		if (null != this.lang)
		{
			return this.lang;
		}
		ValueExpression _ve = getValueExpression("lang");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>lang</code> property.
	 */
	public void setLang(String lang)
	{
		this.lang = lang;
	}

	private boolean multiple = false;
	private boolean multiple_set = false;

	/**
	 * Return the value of the <code>multiple</code> property. Contents: whether
	 * or not multiple choices are supported.
	 */
	public boolean isMultiple()
	{
		if (this.multiple_set)
		{
			return this.multiple;
		}
		ValueExpression _ve = getValueExpression("multiple");
		if (_ve != null)
		{
			Object _result = _ve.getValue(getFacesContext().getELContext());
			if (_result == null)
			{
				return false;
			}
			else
			{
				return ((Boolean) _result).booleanValue();
			}
		}
		else
		{
			return this.multiple;
		}
	}

	/**
	 * Set the value of the <code>multiple</code> property.
	 */
	public void setMultiple(boolean multiple)
	{
		this.multiple = multiple;
		this.multiple_set = true;
	}

	private String onblur;

	/**
	 * Return the value of the <code>onblur</code> property. Contents:
	 * Javascript code executed when this element loses focus.
	 */
	public String getOnblur()
	{
		if (null != this.onblur)
		{
			return this.onblur;
		}
		ValueExpression _ve = getValueExpression("onblur");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>onblur</code> property.
	 */
	public void setOnblur(String onblur)
	{
		this.onblur = onblur;
	}

	private String onchange;

	/**
	 * Return the value of the <code>onchange</code> property. Contents:
	 * Javascript code executed when this element loses focus and its value has
	 * been modified since gaining focus.
	 */
	public String getOnchange()
	{
		if (null != this.onchange)
		{
			return this.onchange;
		}
		ValueExpression _ve = getValueExpression("onchange");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>onchange</code> property.
	 */
	public void setOnchange(String onchange)
	{
		this.onchange = onchange;
	}

	private String onclick;

	/**
	 * Return the value of the <code>onclick</code> property. Contents:
	 * Javascript code executed when a pointer button is clicked over this
	 * element.
	 */
	public String getOnclick()
	{
		if (null != this.onclick)
		{
			return this.onclick;
		}
		ValueExpression _ve = getValueExpression("onclick");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>onclick</code> property.
	 */
	public void setOnclick(String onclick)
	{
		this.onclick = onclick;
	}

	private String ondblclick;

	/**
	 * Return the value of the <code>ondblclick</code> property. Contents:
	 * Javascript code executed when a pointer button is double clicked over
	 * this element.
	 */
	public String getOndblclick()
	{
		if (null != this.ondblclick)
		{
			return this.ondblclick;
		}
		ValueExpression _ve = getValueExpression("ondblclick");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>ondblclick</code> property.
	 */
	public void setOndblclick(String ondblclick)
	{
		this.ondblclick = ondblclick;
	}

	private String onfocus;

	/**
	 * Return the value of the <code>onfocus</code> property. Contents:
	 * Javascript code executed when this element receives focus.
	 */
	public String getOnfocus()
	{
		if (null != this.onfocus)
		{
			return this.onfocus;
		}
		ValueExpression _ve = getValueExpression("onfocus");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>onfocus</code> property.
	 */
	public void setOnfocus(String onfocus)
	{
		this.onfocus = onfocus;
	}

	private String onkeydown;

	/**
	 * Return the value of the <code>onkeydown</code> property. Contents:
	 * Javascript code executed when a key is pressed down over this element.
	 */
	public String getOnkeydown()
	{
		if (null != this.onkeydown)
		{
			return this.onkeydown;
		}
		ValueExpression _ve = getValueExpression("onkeydown");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>onkeydown</code> property.
	 */
	public void setOnkeydown(String onkeydown)
	{
		this.onkeydown = onkeydown;
	}

	private String onkeypress;

	/**
	 * Return the value of the <code>onkeypress</code> property. Contents:
	 * Javascript code executed when a key is pressed and released over this
	 * element.
	 */
	public String getOnkeypress()
	{
		if (null != this.onkeypress)
		{
			return this.onkeypress;
		}
		ValueExpression _ve = getValueExpression("onkeypress");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>onkeypress</code> property.
	 */
	public void setOnkeypress(String onkeypress)
	{
		this.onkeypress = onkeypress;
	}

	private String onkeyup;

	/**
	 * Return the value of the <code>onkeyup</code> property. Contents:
	 * Javascript code executed when a key is released over this element.
	 */
	public String getOnkeyup()
	{
		if (null != this.onkeyup)
		{
			return this.onkeyup;
		}
		ValueExpression _ve = getValueExpression("onkeyup");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>onkeyup</code> property.
	 */
	public void setOnkeyup(String onkeyup)
	{
		this.onkeyup = onkeyup;
	}

	private String onmousedown;

	/**
	 * Return the value of the <code>onmousedown</code> property. Contents:
	 * Javascript code executed when a pointer button is pressed down over this
	 * element.
	 */
	public String getOnmousedown()
	{
		if (null != this.onmousedown)
		{
			return this.onmousedown;
		}
		ValueExpression _ve = getValueExpression("onmousedown");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>onmousedown</code> property.
	 */
	public void setOnmousedown(String onmousedown)
	{
		this.onmousedown = onmousedown;
	}

	private String onmousemove;

	/**
	 * Return the value of the <code>onmousemove</code> property. Contents:
	 * Javascript code executed when a pointer button is moved within this
	 * element.
	 */
	public String getOnmousemove()
	{
		if (null != this.onmousemove)
		{
			return this.onmousemove;
		}
		ValueExpression _ve = getValueExpression("onmousemove");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>onmousemove</code> property.
	 */
	public void setOnmousemove(String onmousemove)
	{
		this.onmousemove = onmousemove;
	}

	private String onmouseout;

	/**
	 * Return the value of the <code>onmouseout</code> property. Contents:
	 * Javascript code executed when a pointer button is moved away from this
	 * element.
	 */
	public String getOnmouseout()
	{
		if (null != this.onmouseout)
		{
			return this.onmouseout;
		}
		ValueExpression _ve = getValueExpression("onmouseout");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>onmouseout</code> property.
	 */
	public void setOnmouseout(String onmouseout)
	{
		this.onmouseout = onmouseout;
	}

	private String onmouseover;

	/**
	 * Return the value of the <code>onmouseover</code> property. Contents:
	 * Javascript code executed when a pointer button is moved onto this
	 * element.
	 */
	public String getOnmouseover()
	{
		if (null != this.onmouseover)
		{
			return this.onmouseover;
		}
		ValueExpression _ve = getValueExpression("onmouseover");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>onmouseover</code> property.
	 */
	public void setOnmouseover(String onmouseover)
	{
		this.onmouseover = onmouseover;
	}

	private String onmouseup;

	/**
	 * Return the value of the <code>onmouseup</code> property. Contents:
	 * Javascript code executed when a pointer button is released over this
	 * element.
	 */
	public String getOnmouseup()
	{
		if (null != this.onmouseup)
		{
			return this.onmouseup;
		}
		ValueExpression _ve = getValueExpression("onmouseup");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>onmouseup</code> property.
	 */
	public void setOnmouseup(String onmouseup)
	{
		this.onmouseup = onmouseup;
	}

	private String onselect;

	/**
	 * Return the value of the <code>onselect</code> property. Contents:
	 * Javascript code executed when text within this element is selected by the
	 * user.
	 */
	public String getOnselect()
	{
		if (null != this.onselect)
		{
			return this.onselect;
		}
		ValueExpression _ve = getValueExpression("onselect");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>onselect</code> property.
	 */
	public void setOnselect(String onselect)
	{
		this.onselect = onselect;
	}

	private boolean readonly = false;
	private boolean readonly_set = false;

	/**
	 * Return the value of the <code>readonly</code> property. Contents: Flag
	 * indicating that this component will prohibit changes by the user. The
	 * element may receive focus unless it has also been disabled. A value of
	 * false causes no attribute to be rendered, while a value of true causes
	 * the attribute to be rendered as readonly="readonly".
	 */
	public boolean isReadonly()
	{
		if (this.readonly_set)
		{
			return this.readonly;
		}
		ValueExpression _ve = getValueExpression("readonly");
		if (_ve != null)
		{
			Object _result = _ve.getValue(getFacesContext().getELContext());
			if (_result == null)
			{
				return false;
			}
			else
			{
				return ((Boolean) _result).booleanValue();
			}
		}
		else
		{
			return this.readonly;
		}
	}

	/**
	 * Set the value of the <code>readonly</code> property.
	 */
	public void setReadonly(boolean readonly)
	{
		this.readonly = readonly;
		this.readonly_set = true;
	}

	private String style;

	/**
	 * Return the value of the <code>style</code> property. Contents: CSS
	 * style(s) to be applied when this component is rendered.
	 */
	public String getStyle()
	{
		if (null != this.style)
		{
			return this.style;
		}
		ValueExpression _ve = getValueExpression("style");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>style</code> property.
	 */
	public void setStyle(String style)
	{
		this.style = style;
	}

	private String styleClass;

	/**
	 * Return the value of the <code>styleClass</code> property. Contents:
	 * Space-separated list of CSS style class(es) to be applied when this
	 * element is rendered. This value must be passed through as the "class"
	 * attribute on generated markup.
	 */
	public String getStyleClass()
	{
		if (null != this.styleClass)
		{
			return this.styleClass;
		}
		ValueExpression _ve = getValueExpression("styleClass");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>styleClass</code> property.
	 */
	public void setStyleClass(String styleClass)
	{
		this.styleClass = styleClass;
	}

	private String tabindex;

	/**
	 * Return the value of the <code>tabindex</code> property. Contents:
	 * Position of this element in the tabbing order for the current document.
	 * This value must be an integer between 0 and 32767.
	 */
	public String getTabindex()
	{
		if (null != this.tabindex)
		{
			return this.tabindex;
		}
		ValueExpression _ve = getValueExpression("tabindex");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>tabindex</code> property.
	 */
	public void setTabindex(String tabindex)
	{
		this.tabindex = tabindex;
	}

	private String title;

	/**
	 * Return the value of the <code>title</code> property. Contents: Advisory
	 * title information about markup elements generated for this component.
	 */
	public String getTitle()
	{
		if (null != this.title)
		{
			return this.title;
		}
		ValueExpression _ve = getValueExpression("title");
		if (_ve != null)
		{
			return (String) _ve.getValue(getFacesContext().getELContext());
		}
		else
		{
			return null;
		}
	}

	/**
	 * Set the value of the <code>title</code> property.
	 */
	public void setTitle(String title)
	{
		this.title = title;
	}

	private Object[] _values;

	/**
	 * Stores each local variable into the component's state
	 * 
	 * @return the component's state including local variables
	 * @param _context
	 *            the current faces context
	 * @see javax.faces.component.UIInput#saveState(javax.faces.context.FacesContext)
	 */
	@Override
	public Object saveState(FacesContext _context)
	{
		if (_values == null)
		{
			_values = new Object[31];
		}
		_values[0] = super.saveState(_context);
		_values[1] = accesskey;
		_values[2] = dir;
		_values[3] = this.disabled ? Boolean.TRUE : Boolean.FALSE;
		_values[4] = this.disabled_set ? Boolean.TRUE : Boolean.FALSE;
		_values[5] = disabledClass;
		_values[6] = enabledClass;
		_values[7] = label;
		_values[8] = lang;
		_values[9] = this.multiple ? Boolean.TRUE : Boolean.FALSE;
		_values[10] = this.multiple_set ? Boolean.TRUE : Boolean.FALSE;
		_values[11] = onblur;
		_values[12] = onchange;
		_values[13] = onclick;
		_values[14] = ondblclick;
		_values[15] = onfocus;
		_values[16] = onkeydown;
		_values[17] = onkeypress;
		_values[18] = onkeyup;
		_values[19] = onmousedown;
		_values[20] = onmousemove;
		_values[21] = onmouseout;
		_values[22] = onmouseover;
		_values[23] = onmouseup;
		_values[24] = onselect;
		_values[25] = this.readonly ? Boolean.TRUE : Boolean.FALSE;
		_values[26] = this.readonly_set ? Boolean.TRUE : Boolean.FALSE;
		_values[27] = style;
		_values[28] = styleClass;
		_values[29] = tabindex;
		_values[30] = title;
		return _values;
	}

	/**
	 * Loads each local variable from the a saved state
	 * 
	 * @param _context
	 *            current faces context
	 * @param _state
	 *            object that saves component's state
	 * @see javax.faces.component.UIInput#restoreState(javax.faces.context.FacesContext,
	 *      java.lang.Object)
	 */
	@Override
	public void restoreState(FacesContext _context, Object _state)
	{
		_values = (Object[]) _state;
		super.restoreState(_context, _values[0]);
		this.accesskey = (String) _values[1];
		this.dir = (String) _values[2];
		this.disabled = ((Boolean) _values[3]).booleanValue();
		this.disabled_set = ((Boolean) _values[4]).booleanValue();
		this.disabledClass = (String) _values[5];
		this.enabledClass = (String) _values[6];
		this.label = (String) _values[7];
		this.lang = (String) _values[8];
		this.multiple = ((Boolean) _values[9]).booleanValue();
		this.multiple_set = ((Boolean) _values[10]).booleanValue();
		this.onblur = (String) _values[11];
		this.onchange = (String) _values[12];
		this.onclick = (String) _values[13];
		this.ondblclick = (String) _values[14];
		this.onfocus = (String) _values[15];
		this.onkeydown = (String) _values[16];
		this.onkeypress = (String) _values[17];
		this.onkeyup = (String) _values[18];
		this.onmousedown = (String) _values[19];
		this.onmousemove = (String) _values[20];
		this.onmouseout = (String) _values[21];
		this.onmouseover = (String) _values[22];
		this.onmouseup = (String) _values[23];
		this.onselect = (String) _values[24];
		this.readonly = ((Boolean) _values[25]).booleanValue();
		this.readonly_set = ((Boolean) _values[26]).booleanValue();
		this.style = (String) _values[27];
		this.styleClass = (String) _values[28];
		this.tabindex = (String) _values[29];
		this.title = (String) _values[30];
	}

	// ************************ Renderer Methods *********************** /

	/**
	 * HTML read-only attribute
	 */
	private static final String READONLY_ATTR = "readonly";
	/**
	 * HTML class attribute
	 */
	private static final String CLASS_ATTR = "class";
	/**
	 * HTML disabled attribute
	 */
	private static final String DISABLED_ATTR = "disabled";
	/**
	 * HTML type attribute
	 */
	private static final String TYPE_ATTR = "type";
	/**
	 * HTML checked attribute
	 */
	private static final String CHECKED_ATTR = "checked";
	/**
	 * HTML radio attribute
	 */
	private static final String INPUT_TYPE_RADIO = "radio";
	/**
	 * HTML check box attribute
	 */
	private static final String INPUT_TYPE_CHECK_BOX = "checkbox";
	/**
	 * HTML id attribute
	 */
	private static final String ID_ATTR = "id";
	/**
	 * HTML name attribute
	 */
	private static final String NAME_ATTR = "name";
	/**
	 * HTML value attribute
	 */
	private static final String VALUE_ATTR = "value";
	/**
	 * HTML label element
	 */
	private static final String LABEL_ELEM = "label";
	/**
	 * HTML for attribute
	 */
	private static final String FOR_ATTR = "for";
	/**
	 * HTML input element
	 */
	private static final String INPUT_ELEM = "input";
	/**
	 * HTML non-breaking space element
	 */
	private static final String NBSP_ENTITY = "&#160;";

	/**
	 * <p>
	 * An array of all passthrough attributes for the
	 * <code>Standard HTML RenderKit</code>
	 */
	private static final String[] PASSTHROUGH_ATTRIBUTES = { "accept", "accesskey", "dir", "lang", "onblur",
			"onchange", "onclick", "ondblclick", "onfocus", "onkeydown", "onkeypress", "onkeyup", "onmousedown",
			"onmousemove", "onmouseout", "onmouseover", "onmouseup", "onselect", "tabindex", "title" };

	/**
	 * Set the bits of the numeric submitted value to reflect the request
	 * parameter values. This method does nothing if the component is read-only
	 * or disabled. It then sets the bits for those request parameter values
	 * that match the <code>component</code>'s clientId. If no request parameter
	 * values are found then zero is stored. The value is coerced into the
	 * appropriate subclass of <code>java.lang.Number</code>.
	 * 
	 * @param context
	 *            a non-null reference to the Faces context
	 * @throws IllegalArgumentException
	 *             if <code>context</code> is <code>null</code>
	 * @throws javax.faces.FacesException
	 *             if a bit location cannot be parsed from a request parameter
	 *             value
	 * @see javax.faces.component.UIInput#decode(javax.faces.context.FacesContext)
	 */
	@Override
	public void decode(FacesContext context) throws FacesException
	{
		// Validate parameter
		verifyNonNull(context, "context");

		// If the component is disabled or read-only, simply return
		Map<String, Object> attributes = getAttributes();
		if (Boolean.TRUE.equals(attributes.get("disabled")) || Boolean.TRUE.equals(attributes.get("readonly")))
			return;

		// Set each bit whose location is submitted
		long mask = 0;
		String requestParameterValues[] = context.getExternalContext().getRequestParameterValuesMap().get(
				getClientId(context));
		if (requestParameterValues != null)
			for (String requestParameter : requestParameterValues)
				try
				{
					mask |= 1 << Long.parseLong(requestParameter);
				}
				catch (NumberFormatException numberFormatException)
				{
					// Request parameter is not a long: wrap into faces
					// exception
					throw new FacesException("Cannot store bit location " + requestParameter + " for component "
							+ getClientId(context));
				}
		// Coerce the result to the same type as this component's value and
		// submit it
		Class valueClass = getValue() == null ? Integer.class : getValue().getClass();
		setSubmittedValue(context.getApplication().getExpressionFactory().coerceToType(new Long(mask), valueClass));
	}

	/**
	 * Returns <code>true</code> always so this class will render its own
	 * children.
	 * 
	 * @return <code>true</code>
	 */
	@Override
	public boolean getRendersChildren()
	{
		return true;
	}

	/**
	 * Encodes child <code>UIISelectItem</code> and <code>UIISelectItems</code>
	 * as radio buttons; other children encode themselves. This method lays out
	 * the children exactly as they are specified.
	 * 
	 * @param context
	 *            a non-null reference to the Faces context
	 * @throws IllegalArgumentException
	 *             if <code>context</code> is <code>null</code>
	 * @throws IOException
	 *             if any i/o error occurs during rendering
	 */
	@Override
	public void encodeChildren(FacesContext context) throws IOException
	{
		// Validate parameters
		verifyNonNull(context, "context");
		// Get the object's value from the submitted value first, then the
		// regular value
		Converter converter = getConverter(context);
		Object currentValue = getSubmittedValue();
		if (currentValue == null)
			currentValue = getValue();
		List<UIComponent> children = getChildren();

		// Render each child exactly as it is laid out
		for (UIComponent child : children)
		{
			// UISelectItem child is rendered by this class
			if (child instanceof UISelectItem)
			{
				UISelectItem uiSelectItem = (UISelectItem) child;
				Object itemValue = uiSelectItem.getValue();
				SelectItem selectItem = null;
				if (itemValue == null)
				{
					// create SelectItem with the value extracted from child
					// component.
					if (uiSelectItem.getItemLabel() == null)
					{
						selectItem = new SelectItem();
						selectItem.setValue(uiSelectItem.getItemValue());
						selectItem.setDisabled(uiSelectItem.isItemDisabled());
					}
					else
						selectItem = new SelectItem(uiSelectItem.getItemValue(), uiSelectItem.getItemLabel(),
								uiSelectItem.getItemDescription(), uiSelectItem.isItemDisabled());
				}
				else if (itemValue instanceof SelectItem)
				{
					// Return the item directly if it is already a SelectItem
					selectItem = (SelectItem) (itemValue);
				}
				else
					throw new FacesException("The UISelectItem component is invalid."
							+ " Value of the child should be either null or an instance of SelectItem.");
				renderOption(context, selectItem, uiSelectItem.getClientId(context), currentValue, converter);
			}
			else if (child instanceof UISelectItems)
			{
				// UISelectItems child is rendered by this class without any
				// formatting
				UISelectItems uiSelectItems = (UISelectItems) (child);
				String itemId = uiSelectItems.getClientId(context);
				for (SelectItem selectItem : getSelectItems(uiSelectItems))
					renderOption(context, selectItem, itemId, currentValue, converter);
			}
			else
				renderChild(context, child);
		}
	}

	/**
	 * Returns the assigned converter or a converter that matches the
	 * component's value.
	 * 
	 * @param context
	 *            a non-null reference to the faces context
	 * @return the associated converter.
	 */
	protected Converter getConverter(FacesContext context)
	{
		// if the converter exists return it
		Converter converter = getConverter();
		if (converter != null)
		{
			return converter;
		}
		// Try to find the converter using the value binding
		ValueExpression valueExpression = getValueExpression("value");
		if ((valueExpression == null) || (context == null))
			return null;
		Class valueType = valueExpression.getType(context.getELContext());
		// No converter needed for String type or Object Class
		if (valueType == null || String.class.equals(valueType) || Object.class.equals(valueType))
			return null;
		return context.getApplication().createConverter(valueType);
	}

	/**
	 * Render an HTML radio/check box input element with some pass-through
	 * attributes. If the <code>multiple</code> attribute is <code>false</code>
	 * then a radio button is rendered, otherwise a check box is rendered.
	 * 
	 * @param context
	 *            the faces context
	 * @param selectItem
	 *            the selectItem to render
	 * @param itemId
	 *            the item's id
	 * @param currentValue
	 *            the current value of this component
	 * @param converter
	 *            the converter used to convert the value
	 * @throws IOException
	 *             if the response writer cannot write
	 * @throws FacesException
	 *             if the item's value does not represent a bit in component's
	 *             value
	 */
	private void renderOption(FacesContext context, SelectItem selectItem, String itemId, Object currentValue,
			Converter converter) throws IOException, FacesException
	{
		ResponseWriter writer = context.getResponseWriter();
		assert (writer != null);

		// Disable the radio button if the attribute is set.
		Map<String, Object> attributes = getAttributes();
		boolean componentDisabled = Boolean.valueOf(String.valueOf(attributes.get(DISABLED_ATTR)));

		// Render either a radio button or a check box
		writer.startElement(INPUT_ELEM, this);
		writer.writeAttribute(TYPE_ATTR, multiple ? INPUT_TYPE_CHECK_BOX : INPUT_TYPE_RADIO, TYPE_ATTR);
		// Get the formatted value from the converter
		String convertedValue = getConvertedStringValue(context, selectItem.getValue(), converter);
		writer.writeAttribute(VALUE_ATTR, convertedValue, VALUE_ATTR);
		// Element is checked if its value's bit is on in this component's
		// current value
		// Coerce to the same class (String if not known)
		Object coercedValue = context.getApplication().getExpressionFactory().coerceToType(selectItem.getValue(),
				(currentValue != null) ? currentValue.getClass() : String.class);
		if ((currentValue != null) && (currentValue instanceof Number) && (coercedValue != null)
				&& (coercedValue instanceof Number))
		{
			if (((1 << ((Number) coercedValue).intValue()) & ((Number) currentValue).intValue()) != 0)
				writer.writeAttribute(CHECKED_ATTR, Boolean.TRUE, null);
		}
		else
			throw new FacesException("Cannot check numeric element " + itemId + " using '" + coercedValue + "' for '"
					+ currentValue + '\'');
		// The name of the request parameter is this component's id
		writer.writeAttribute(NAME_ATTR, getClientId(context), "clientId");
		writer.writeAttribute(ID_ATTR, itemId, ID_ATTR);

		// Apply HTML 4.x pass-through attributes
		boolean isXhtml = writer.getContentType().equals("application/xhtml+xml");
		for (String attributeName : PASSTHROUGH_ATTRIBUTES)
		{
			Object value = attributes.get(attributeName);
			if (value != null)
				// Prepend the XML prefix to the lang attribute
				writer.writeAttribute(isXhtml && "lang".equals(attributeName) ? "xml:" + attributeName : attributeName,
						value, attributeName);
		}
		// Apply readonly & disabled attributes using XHTML boolean semantics
		Object value = attributes.get(READONLY_ATTR);
		if (value != null)
		{
			Boolean bool = (value instanceof Boolean) ? (Boolean) value : Boolean.valueOf(value.toString());
			if (bool)
				writer.writeAttribute(READONLY_ATTR, true, READONLY_ATTR);
		}
		// Don't render the disabled attribute twice if the 'parent'
		// component is already marked disabled.
		if (!componentDisabled && selectItem.isDisabled())
			writer.writeAttribute(DISABLED_ATTR, true, DISABLED_ATTR);

		// End the input element and write the optional label
		writer.endElement(INPUT_ELEM);
		String itemLabel = selectItem.getLabel();
		if (itemLabel != null)
		{
			writer.write(NBSP_ENTITY);
			writer.startElement(LABEL_ELEM, this);
			writer.writeAttribute(FOR_ATTR, itemId, FOR_ATTR);
			// Apply relevant class if available to the label
			String labelClass = (String) attributes
					.get((componentDisabled || selectItem.isDisabled()) ? "disabledClass" : "enabledClass");
			if (labelClass != null)
				writer.writeAttribute(CLASS_ATTR, labelClass, "labelClass");
			if (selectItem.isEscape())
				writer.writeText(itemLabel, this, LABEL_ELEM);
			else
				writer.write(itemLabel);
			writer.endElement(LABEL_ELEM);
		}
	}

	/**
	 * Ask a child component to render itself or render its children for it.
	 * 
	 * @param context
	 *            a non-null reference to the faces context
	 * @param child
	 *            the child component to render
	 * @throws IOException
	 *             if any i/o error occurs.
	 */
	private void renderChild(FacesContext context, UIComponent child) throws IOException
	{
		// Do nothing if the child doesn't need to be rendered.
		if ((context == null) || (child == null) || !child.isRendered())
			return;
		// Ask the child to render itself
		child.encodeBegin(context);
		if (child.getRendersChildren())
		{
			child.encodeChildren(context);
		}
		else
			// Render its children for it
			for (UIComponent grandchild : child.getChildren())
				renderChild(context, grandchild);
		child.encodeEnd(context);
	}

	/**
	 * Return the converted string representation of the given object value.
	 * This method uses the supplied converter; if not available then the value
	 * is simply converted to a string or the empty string if <code>null</code>.
	 * 
	 * @param context
	 *            a non-null reference to the faces context
	 * @param value
	 *            the value to convert to string from
	 * @return the string form of the given value
	 */
	private String getConvertedStringValue(FacesContext context, Object value, Converter converter)
	{
		// If converter not available then get value's string representation
		if (converter == null)
			return (value == null) ? "" : value.toString();
		else
			return converter.getAsString(context, this, value);
	}

	/**
	 * Return the list of <code>SelectItem</code> found in a
	 * <code>SelectItems</code>.
	 * 
	 * @param uiSelectItems
	 *            the items to convert
	 * @return the list of member items
	 * @throws FacesException
	 *             if a list of <code>SelectItem</code> objects cannot be
	 *             extracted from the parameter
	 */
	private List<SelectItem> getSelectItems(UISelectItems uiSelectItems)
	{
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		Object value = uiSelectItems.getValue();
		// Extract the select items from the component's value.
		if (value instanceof SelectItem)
		{
			selectItems.add((SelectItem) value);
		}
		else if (value instanceof SelectItem[])
		{
			selectItems.addAll(Arrays.asList((SelectItem[]) value));
		}
		else
		{
			throw new FacesException("The UISelectItems component is invalid."
					+ " Value of the child can only be an instance of SelectItem" + " or array of SelectItem.");
		}
		return selectItems;
	}

	/**
	 * Verifies that an object is not <code>null</code>.
	 * 
	 * @param object
	 *            the object to verify
	 * @param name
	 *            the name that represents <code>object</code>
	 * @throws IllegalArgumentException
	 *             if <code>object</code> is <code>null</code>.
	 */
	private static void verifyNonNull(Object object, String name)
	{
		if (object == null)
			throw new IllegalArgumentException("Parameter argument '" + name + "' can not be null");
	}
}
