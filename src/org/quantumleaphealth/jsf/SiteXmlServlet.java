/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.jsf;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jboss.seam.contexts.Context;
import org.jboss.seam.contexts.Contexts;
import org.quantumleaphealth.model.trial.Geocoding;
import org.quantumleaphealth.model.trial.Site;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.TrialSite;
import org.quantumleaphealth.screen.MatchingEngine;

/**
 * Generates XML-formatted site data for a trial. This class deviates each
 * site's geocode that is shared.
 * 
 * @author Tom Bechtold
 * @version 2009-01-09
 */
public class SiteXmlServlet extends HttpServlet
{
	/**
	 * Character encoding for the response
	 */
	private static final String ENCODING = "UTF-8";
	/**
	 * The name for site's without one
	 */
	private static final String SITENAME_MISSING = "Private practice";

	/**
	 * The maximum distance to deviate a geocode
	 */
	private static final double MAXIMUM_DEVIATION_MILES = 0.5d;
	/**
	 * The number of statute miles in a degree of latitude
	 */
	private static final double MILES_PER_DEGREE = 69.04d;

	/**
	 * Constructs an XML-formatted list of sites for a trial. This trial's
	 * identifier is found in the request's name. This method returns a 404
	 * response if the matching engine or the trial is not available. Sites
	 * whose geocoding is unknown are irrelevant.
	 * 
	 * @param request
	 *            the request
	 * @param response
	 *            the response
	 * @throws ServletException
	 *             not thrown
	 * @throws IOException
	 *             if the response cannot be written
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
	 *      javax.servlet.http.HttpServletResponse)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		// If trial not available then send 404 (not found) response
		Trial trial = null;
		try
		{
			trial = getTrial(request);
		}
		catch (Throwable throwable)
		{
			log("cannot find trial for " + (request == null ? "null" : request.getRequestURI()), throwable);
		}
		if (trial == null)
		{
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return;
		}
		// Served page is cachable xml
		response.setContentType("text/xml");
		response.setCharacterEncoding(ENCODING);
		response.setHeader("Cache-Control", "public");
		PrintWriter writer = response.getWriter();
		// Include the request's local name and trial in a comment
		writer.print("<?xml version=\"1.0\" encoding=\"");
		writer.print(ENCODING);
		writer.print("\"?><!-- ");
		writer.print(request.getRequestURI());
		writer.print(": #");
		writer.print(trial.getId());
		writer.print(" (");
		writer.print(trial.getPrimaryID());
		writer.print(") ");
		writer.print(trial.getTrialSite().size());
		writer.println(" --><sites>");
		for (TrialSite trialSite : trial.getTrialSite())
		{
			// Sites whose geocoding is unknown are irrelevant
			Site site = (trialSite == null) || (trialSite.getSite() == null) ? null : trialSite.getSite();
			if ((site == null) || (site.getId() == null))
				continue;
			Geocoding geocoding = site.getGeocoding();
			if ((geocoding == null) || Double.isNaN(geocoding.getLatitude()) || Double.isNaN(geocoding.getLongitude()))
				continue;
			// If geocode is shared then deviate it using the hashcode of its
			// name
			double latitude = geocoding.getLatitude();
			double longitude = geocoding.getLongitude();
			boolean shared = false;
			for (TrialSite otherTrialSite : trial.getTrialSite())
				if ((otherTrialSite != null) && (otherTrialSite.getSite() != null)
						&& !site.equals(otherTrialSite.getSite())
						&& geocoding.equals(otherTrialSite.getSite().getGeocoding()))
				{
					shared = true;
					break;
				}
			if (shared)
			{
				// If a geocode is shared among sites for a particular trial
				// then deviate its geocoding so the markers do not obscure each
				// other
				// A particular site's hash is always the same so the deviation
				// will always be the same
				// If the site's name is known then use its hash to determine
				// the angle; otherwise use its identifier
				// Dividing a hash by the minimum integer value will yield a
				// number between -1 and 1, thus the angle will be between -180
				// and 180 degrees
				double angleRadians = Math.PI
						* ((double) ((site.getName() == null) ? site.getId() : site.getName()).hashCode())
						/ ((double) Integer.MIN_VALUE);
				// Convert the deviation to degrees of latitude
				latitude += Math.sin(angleRadians) * MAXIMUM_DEVIATION_MILES / MILES_PER_DEGREE;
				longitude += Math.cos(angleRadians) * MAXIMUM_DEVIATION_MILES / MILES_PER_DEGREE;
			}
			writer.print("<site id=\"");
			writer.print(site.getId());
			writer.print("\" lat=\"");
			writer.print(Double.toString(latitude));
			writer.print("\" lng=\"");
			writer.print(Double.toString(longitude));
			writer.print("\" name=\"");
			// Escape quotes and ampersands in the site's name
			if (site.getName() != null)
				writer.write(site.getName().replace('"', '\'').replace("&", "&amp;"));
			else
				writer.write(SITENAME_MISSING);
			writer.println("\" />");
		}
		writer.println("</sites>");
		// Do not close the writer as it screws up HTTP keep-alive
		writer.flush();
	}

	/**
	 * @return the last modified date of the trial specified in the request or
	 *         <code>-1</code> if not known
	 * @param request
	 *            the servlet request
	 * @see javax.servlet.http.HttpServlet#getLastModified(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	protected long getLastModified(HttpServletRequest request)
	{
		try
		{
			Trial trial = getTrial(request);
			if ((trial != null) && (trial.getLastRegistered() != null))
				return trial.getLastRegistered().getTime();
		}
		catch (Throwable throwable)
		{
		}
		return -1;
	}

	/**
	 * Returns the trial specified in the request. This method extracts the
	 * requested trial's identifier from the servlet's path and loads the
	 * matching engine from Seam's application context.
	 * 
	 * @return the trial specified in the request or <code>null</code> if not
	 *         found
	 * @param request
	 *            the servlet request
	 * @throws IllegalArgumentException
	 *             if the <code>request</code> is <code>null</code>
	 * @throws IllegalStateException
	 *             if the matching engine could not be found in the Seam context
	 */
	private Trial getTrial(HttpServletRequest request) throws IllegalArgumentException, IllegalStateException
	{
		// The identifier is found in between the last forward slash and the dot
		// after the servlet path (path info)
		if (request == null)
			throw new IllegalArgumentException("No request");
		String identifierString = request.getPathInfo();
		if (identifierString == null)
			return null;
		int index = identifierString.lastIndexOf('/');
		if (index >= 0)
			identifierString = identifierString.substring(index + 1);
		index = identifierString.indexOf('.');
		if (index > 0)
			identifierString = identifierString.substring(0, index);

		// Get engine from Seam application context
		Context applicationContext = Contexts.getApplicationContext();
		Object engineObject = (applicationContext == null) ? null : applicationContext.get("engine");
		if ((engineObject == null) || !(engineObject instanceof MatchingEngine))
			throw new IllegalStateException("Could not find engine in request " + request + " and Seam context "
					+ applicationContext);
		try
		{
			return ((MatchingEngine) (engineObject)).getTrial(Long.valueOf(Long.parseLong(identifierString)));
		}
		catch (NumberFormatException numberFormatException)
		{
		}
		return null;
	}

	/**
	 * Version uid for serializable class
	 */
	private static final long serialVersionUID = 6308794543707817451L;
}
