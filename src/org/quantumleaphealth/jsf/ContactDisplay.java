/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.jsf;

import org.quantumleaphealth.model.Person;

/**
 * Contains static methods for displaying person information and test-only
 * functionality.
 * 
 * @author Tom Bechtold
 * @version 2009-01-26
 */
public class ContactDisplay
{
	/**
	 * A single space
	 */
	private static final String SPACE = " ";
	/**
	 * The server names which denote a test server, in lower-case
	 */
	private static final String[] TEST_SERVER = { "test", "deploy", "localhost" };

	/**
	 * @param person
	 *            the person to display
	 * @return a person's full name or <code>null</code> if no person
	 */
	public static String getFullName(Person person)
	{
		if (person == null)
			return null;
		StringBuilder builder = new StringBuilder();
		addTrimmedString(builder, SPACE, person.getPrefix());
		addTrimmedString(builder, SPACE, person.getGivenName());
		addTrimmedString(builder, SPACE, person.getMiddleInitial());
		addTrimmedString(builder, SPACE, person.getSurName());
		addTrimmedString(builder, SPACE, person.getGenerationSuffix());
		addTrimmedString(builder, SPACE, person.getProfessionalSuffix());
		return builder.length() == 0 ? null : builder.toString();
	}

	/**
	 * @param builder
	 *            the builder to append the prefix and string to
	 * @param prefix
	 *            the prefix to append if the builder already has code or
	 *            <code>null</code> for none
	 * @param string
	 *            the string to append to
	 */
	private static void addTrimmedString(StringBuilder builder, String prefix, String string)
	{
		if (string != null)
		{
			string = string.trim();
			if (string.length() == 0)
				string = null;
		}
		if ((builder == null) || (string == null))
			return;
		if ((prefix != null) && (builder.length() > 0))
			builder.append(prefix);
		builder.append(string);
	}

	/**
	 * @param object1
	 *            the first object
	 * @param object2
	 *            the second object
	 * @return the concatenated string of the two object's
	 *         <code>toString()</code> methods
	 */
	public static String concat(Object object1, Object object2)
	{
		StringBuilder builder = new StringBuilder();
		if (object1 != null)
			builder.append(object1);
		if (object2 != null)
			builder.append(object2);
		return builder.toString();
	}

	/**
	 * @param url
	 *            contains the server name
	 * @return whether <code>url</code> contains a test server name
	 */
	public static boolean isTestServer(String url)
	{
		if (url == null)
			return false;
		url = url.toLowerCase();
		for (String server : TEST_SERVER)
			if (url.indexOf(server) >= 0)
				return true;
		return false;
	}

	/**
	 * @param servername
	 *            contains the server name
	 * @port
	 *        	  request port           
	 * @return proper URL for fetching the resource.
	 */
	public static String getFetchURL(String server, String port)
	{
		if (server == null || port == null) // then we're screwed
		{
			return "http://localhost:8080";
		}
		else if (server.toLowerCase().contains("localhost"))
		{
			return "http://" + server + ":" + port;
		}
		// else this is test or production and using SSL
		return "https://" + server;
	}

	/**
	 * create breadcrumb text for the passed in URL.
	 * @param server
	 * @param port
	 * @param Url
	 * @return the formatted text (should not be escaped)
	 */
	public static String getBreadCrumb(String server, String port, String Url)
	{
		StringBuilder breadCrumbs = new StringBuilder();
		Url = Url.replaceFirst(".*bct_nation\\/", "");
		
		String[] crumbs = Url.split("\\/");
		
		String breadcrumb_url = getFetchURL(server, port) + "/bct_nation";
		breadCrumbs.append("<a href=\"" + breadcrumb_url + "/home.seam\">Home</a>&nbsp;&nbsp;/&nbsp;&nbsp;");
		for (int i = 0; i < crumbs.length - 1; i++)
		{
			breadcrumb_url += "/" + crumbs[i];
			breadCrumbs.append(crumbs[i] + "</a>&nbsp;&nbsp;/&nbsp;&nbsp;");
		}
		
		if (crumbs.length > 0)
		{
			breadCrumbs.append(crumbs[crumbs.length - 1]);
		}
		return breadCrumbs.toString();
	}
}
