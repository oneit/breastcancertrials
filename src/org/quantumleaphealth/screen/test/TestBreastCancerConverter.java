/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import java.util.LinkedList;
import java.util.List;

import org.quantumleaphealth.model.trial.ComparativeCriterion;
import org.quantumleaphealth.model.trial.Criteria;
import org.quantumleaphealth.model.trial.Criterion;
import org.quantumleaphealth.model.trial.TreatmentProcedureCriterion;
import org.quantumleaphealth.model.trial.TreatmentRegimenCriterion;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.*;
import org.quantumleaphealth.screen.AttributeAssociativeMatchable;
import org.quantumleaphealth.screen.BreastCancerConverter;
import org.quantumleaphealth.screen.CriteriaMatchable;
import org.quantumleaphealth.screen.Matchable;
import org.quantumleaphealth.screen.TreatmentRadiationProcedureMatchable;
import org.quantumleaphealth.screen.TreatmentRegimenMatchable;
import org.quantumleaphealth.screen.TreatmentSurgeryProcedureMatchable;
import org.quantumleaphealth.screen.UnsupportedCriterionException;
import org.testng.annotations.Test;

/**
 * Unit tests for the <code>AttributeAssociativeMatchable</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-07
 * @see AttributeAssociativeMatchable
 */
@Test(description = "Multiple-choice associative matchable", groups = { "screen", "composite" })
public class TestBreastCancerConverter
{

	/**
	 * Test criteria
	 */
	public void testCriteria()
	{
		BreastCancerConverter converter = new BreastCancerConverter();
		// Test childless
		try
		{
			converter.convert(new Criteria());
			throw new AssertionError("Converted childless criteria");
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
		}

		// Test two children
		Criteria criteria = new Criteria();
		List<Criterion> children = new LinkedList<Criterion>();
		ComparativeCriterion comparativeCriterion = new ComparativeCriterion();
		comparativeCriterion.setCharacteristicCode(CREATININE);
		children.add(comparativeCriterion);
		comparativeCriterion = new ComparativeCriterion();
		comparativeCriterion.setCharacteristicCode(CREATININE);
		children.add(comparativeCriterion);
		criteria.setChildren(children);
		criteria.setOperator(Criteria.Operator.AND);
		try
		{
			Matchable result = converter.convert(criteria);
			assert (result instanceof CriteriaMatchable) : "Converted criteria is not criteria matchable" + result;
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
			throw new AssertionError(unsupportedCriterionException);
		}
	}

	/**
	 * Test TreatmentProcedureCriterion
	 */
	public void testTreatmentProcedureCriterion()
	{
		BreastCancerConverter converter = new BreastCancerConverter();
		try
		{
			// Test surgery
			TreatmentProcedureCriterion criterion = new TreatmentProcedureCriterion();
			criterion.setParent(SURGERY);
			assert converter.convert(criterion) instanceof TreatmentSurgeryProcedureMatchable : "Surgery not converted";
			// Test radiation
			criterion = new TreatmentProcedureCriterion();
			criterion.setParent(RADIATION);
			assert converter.convert(criterion) instanceof TreatmentRadiationProcedureMatchable : "Radiation not converted";
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
			throw new AssertionError(unsupportedCriterionException);
		}
	}

	public void testTreatmentRegimenCriterion()
	{
		BreastCancerConverter converter = new BreastCancerConverter();
		try
		{
			// Test chemo
			TreatmentRegimenCriterion criterion = new TreatmentRegimenCriterion();
			criterion.setParent(CHEMOTHERAPY);
			assert converter.convert(criterion) instanceof TreatmentRegimenMatchable : "Chemo not converted";
			// Test endo
			criterion = new TreatmentRegimenCriterion();
			criterion.setParent(ENDOCRINE);
			assert converter.convert(criterion) instanceof TreatmentRegimenMatchable : "Endocrine not converted";
			// Test bio
			criterion = new TreatmentRegimenCriterion();
			criterion.setParent(BIOLOGICAL);
			assert converter.convert(criterion) instanceof TreatmentRegimenMatchable : "Biological not converted";
			// Test bis
			criterion = new TreatmentRegimenCriterion();
			criterion.setParent(BISPHOSPHONATE);
			assert converter.convert(criterion) instanceof TreatmentRegimenMatchable : "Bisphos not converted";
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
			throw new AssertionError(unsupportedCriterionException);
		}

	}
}
