/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;

import org.quantumleaphealth.model.patient.AvatarHistory;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.screen.DeclarativeMatchingEngine;
import org.quantumleaphealth.screen.EligibilityProfile;
import org.quantumleaphealth.screen.PatientHistoryMatchingIterator;
import org.testng.annotations.Test;

/**
 * Tests the <code>DeclarativeMatchingEngine</code>
 * 
 * @author Tom Bechtold
 * @version 2008-10-09
 * @see DeclarativeMatchingEngine
 */
@Test(description = "Declarative matching engine", groups = { "screen", "composite" })
public class TestDeclarativeMatchingEngine extends TestUtils
{

	/**
	 * Test getMatchingIterator returns an empty iterator when the trials have
	 * no criteria.
	 */
	public void testGetMatchingIteratorForEmptyTrials()
	{
		// Build list of two empty trials
		LinkedList<Trial> list = new LinkedList<Trial>();
		list.add(new Trial());
		list.add(new Trial());
		DeclarativeMatchingEngine engine = new DeclarativeMatchingEngine(list);

		// Verify that both blank trials are not returned for a blank history
		PatientHistory patientHistory = new PatientHistory();
		AvatarHistory avatarHistory = new AvatarHistory();
		PatientHistoryMatchingIterator iterator = engine.getMatchingIterator(patientHistory, avatarHistory);
		assert iterator != null : "Iterator null";
		assert iterator.nextMatch() == null : "Iterator NOT empty";
	}

	/**
	 * Test getEligibilityProfile returns not pass results for empty trials and
	 * an empty patient
	 */
	public void testGetEligibilityProfileForEmptyTrials()
	{
		// Build list of two empty trials
		int[] unique = getUniqueIntegers(2);
		unique[0] = Math.abs(unique[0]);
		unique[1] = Math.abs(unique[1]);
		LinkedList<Trial> list = new LinkedList<Trial>();
		Trial trial = new Trial();
		trial.setId(Long.valueOf(unique[0]));
		list.add(trial);
		trial = new Trial();
		trial.setId(Long.valueOf(unique[1]));
		list.add(trial);
		DeclarativeMatchingEngine engine = new DeclarativeMatchingEngine(list);

		// Verify that both blank trials do not pass a blank history
		PatientHistory patientHistory = new PatientHistory();
		EligibilityProfile eligibilityProfile = engine.getEligibilityProfile(Long.valueOf(unique[0]), patientHistory,
				null);
		assert eligibilityProfile != null : "First profile null";
		assert !eligibilityProfile.isMatchDefinitive() : "First trial matched";
		eligibilityProfile = engine.getEligibilityProfile(Long.valueOf(unique[1]), patientHistory, null);
		assert eligibilityProfile != null : "Second profile null";
		assert !eligibilityProfile.isMatchDefinitive() : "Second trial matched";
	}

	/**
	 * Test getTrialIterator in sorted order
	 */
	public void testGetTrialIterator()
	{
		// Build list of two trials
		int[] unique = getUniqueIntegers(2);
		unique[0] = Math.abs(unique[0]);
		unique[1] = Math.abs(unique[1]);
		LinkedList<Trial> list = new LinkedList<Trial>();
		Trial trial = new Trial();
		trial.setId(Long.valueOf(unique[0]));
		list.add(trial);
		trial = new Trial();
		trial.setId(Long.valueOf(unique[1]));
		list.add(trial);

		// Verify that the trials were sorted by ID
		DeclarativeMatchingEngine engine = new DeclarativeMatchingEngine(list);
		Iterator<Trial> iterator = engine.getTrialIterator();
		assert iterator != null : "Iterator null";
		assert iterator.hasNext() : "Iterator empty";
		Long id1 = iterator.next().getId();
		assert iterator.hasNext() : "Iterator only has one";
		Long id2 = iterator.next().getId();
		assert !iterator.hasNext() : "Iterator more than two";
		assert id1.intValue() == Math.min(unique[0], unique[1]) : "Unordered first trial";
		assert id2.intValue() == Math.max(unique[0], unique[1]) : "Unordered second trial";
	}

	/**
	 * Test getTrialCount correct count
	 */
	public void testTrialCount()
	{
		// Build list of two trials
		LinkedList<Trial> list = new LinkedList<Trial>();
		list.add(new Trial());
		list.add(new Trial());
		DeclarativeMatchingEngine engine = new DeclarativeMatchingEngine(list);
		assert engine.getTrialCount() == 2 : "Count is not two: " + engine.getTrialCount();
	}

	/**
	 * Test getLastModified latest date
	 */
	public void testLastModified()
	{
		// Build list of two trials with different last modified dates
		int[] unique = getUniqueIntegers(2);
		unique[0] = Math.abs(unique[0]);
		unique[1] = Math.abs(unique[1]);
		LinkedList<Trial> list = new LinkedList<Trial>();
		Trial trial = new Trial();
		trial.setLastModified(new Date(unique[0]));
		list.add(trial);
		trial = new Trial();
		trial.setLastModified(new Date(unique[1]));
		list.add(trial);
		DeclarativeMatchingEngine engine = new DeclarativeMatchingEngine(list);
		assert engine.getLastModified().getTime() == Math.max(unique[0], unique[1]);
	}

	/**
	 * Test getLastRegistered latest date
	 */
	public void testLastRegistered()
	{
		// Build list of two trials with different last modified dates
		int[] unique = getUniqueIntegers(2);
		unique[0] = Math.abs(unique[0]);
		unique[1] = Math.abs(unique[1]);
		LinkedList<Trial> list = new LinkedList<Trial>();
		Trial trial = new Trial();
		trial.setLastRegistered(new Date(unique[0]));
		list.add(trial);
		trial = new Trial();
		trial.setLastRegistered(new Date(unique[1]));
		list.add(trial);
		DeclarativeMatchingEngine engine = new DeclarativeMatchingEngine(list);
		assert engine.getLastRegistered().getTime() == Math.max(unique[0], unique[1]);
	}
}
