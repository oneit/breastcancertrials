/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.patient.UserPatient.UserType;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISBREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;
import org.quantumleaphealth.screen.BreastDiagnosisAttributeAssociativeMatchable;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;

/**
 * Tests <code>BreastDiagnosisAttributeAssociativeMatchable</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-07
 * @see BreastDiagnosisAttributeAssociativeMatchable
 */
@Test(description = "Breast diagnosis matchable", groups = { "screen", "composite" })
public class TestBreastDiagnosisAttributeAssociativeMatchable extends TestUtils
{

	public void testSubjective()
	{
		Diagnosis subjective = new Diagnosis();
		subjective.addAttribute(DIAGNOSISBREAST, UNSURE);
		Diagnosis objective = new Diagnosis();
		objective.addAttribute(DIAGNOSISBREAST, new CharacteristicCode(GENERATOR.nextInt()));

		// Test one subjective diagnosis
		PatientHistory history = new PatientHistory();
		history.getDiagnoses().add(subjective);
		int[] diagnosisCode = new int[] { GENERATOR.nextInt() };
		testMatchDescriptive( diagnosisCode, false, history, Result.PASS_SUBJECTIVE, false);
		// repeat the test for an avatar
		testMatchDescriptive( diagnosisCode, false, history, Result.PASS_SUBJECTIVE, true);
		
		// Test one objective, one subjective
		history = new PatientHistory();
		history.getDiagnoses().add(objective);
		history.getDiagnoses().add(subjective);
		diagnosisCode = new int[] { GENERATOR.nextInt() };
		testMatchDescriptive(diagnosisCode, false, history, Result.PASS_SUBJECTIVE, false);
		// repeat the test for an avatar
		testMatchDescriptive(diagnosisCode, false, history, Result.PASS_SUBJECTIVE, true);

		// Test inverted one subjective diagnosis
		history = new PatientHistory();
		history.getDiagnoses().add(subjective);
		diagnosisCode = new int[] { GENERATOR.nextInt() };
		testMatchDescriptive(diagnosisCode, true, history, Result.FAIL_SUBJECTIVE, false);
		// repeat the test for an avatar
		testMatchDescriptive(diagnosisCode, true, history, Result.FAIL_SUBJECTIVE, true);

		// Test inverted one objective, one subjective
		history = new PatientHistory();
		history.getDiagnoses().add(objective);
		history.getDiagnoses().add(subjective);
		diagnosisCode = new int[] { GENERATOR.nextInt() };
		testMatchDescriptive(diagnosisCode, true, history, Result.FAIL_SUBJECTIVE, false);
		// repeat the test for an avatar
		testMatchDescriptive(diagnosisCode, true, history, Result.FAIL_SUBJECTIVE, true);
	}

	public void testObjective()
	{
		int[] unique = getUniqueIntegers(2);
		Diagnosis objective0 = new Diagnosis();
		objective0.addAttribute(DIAGNOSISBREAST, new CharacteristicCode(unique[0]));
		Diagnosis objective1 = new Diagnosis();
		objective1.addAttribute(DIAGNOSISBREAST, new CharacteristicCode(unique[1]));

		// Test one objective diagnosis
		PatientHistory history = new PatientHistory();
		history.getDiagnoses().add(objective0);
		testMatchDescriptive(new int[] { unique[0] }, false, history, Result.PASS, false);
		// repeat the test for an avatar
		testMatchDescriptive(new int[] { unique[0] }, false, history, Result.PASS, true);
	

		// Test two objective diagnoses
		history = new PatientHistory();
		history.getDiagnoses().add(objective0);
		history.getDiagnoses().add(objective1);
		testMatchDescriptive(new int[] { unique[0] }, false, history, Result.PASS, false);
		// repeat the test for an avatar
		testMatchDescriptive(new int[] { unique[0] }, false, history, Result.PASS, true);
		
		// Test one non-matching objective diagnosis
		history = new PatientHistory();
		history.getDiagnoses().add(objective0);
		testMatchDescriptive(new int[] { unique[1] }, false, history, Result.FAIL, false);
		
		// repeat the test for an avatar
		testMatchDescriptive(new int[] { unique[1] }, false, history, Result.FAIL, true);
		
		// Test zero objective diagnosis
		history = new PatientHistory();
		testMatchDescriptive(new int[] { unique[0] }, false, history, Result.FAIL, false);
		// repeat the test for an avatar.  The avatar should get a tentative pass for this.
		testMatchDescriptive(new int[] { unique[0] }, false, history, Result.AVATAR_SUBJECTIVE, true);
		
		// Invert one objective diagnosis
		history = new PatientHistory();
		history.getDiagnoses().add(objective0);
		testMatchDescriptive(new int[] { unique[0] }, true, history, Result.FAIL, false);
		// repeat the test for an avatar
		testMatchDescriptive(new int[] { unique[0] }, true, history, Result.FAIL, true);
		
		// Invert one non-matching objective diagnosis
		history = new PatientHistory();
		history.getDiagnoses().add(objective0);
		testMatchDescriptive(new int[] { unique[1] }, true, history, Result.PASS, false);
		// repeat the test for an avatar
		testMatchDescriptive(new int[] { unique[1] }, true, history, Result.PASS, true);		
	}

	/**
	 * Tests matchDescriptive by passing a criterion and a patient history
	 * 
	 * @param requirement
	 *            the criterion's requirement
	 * @param invert
	 *            the criterion's invert
	 * @param value
	 *            the patient history's value stored in its value history under
	 *            parent
	 * @param expectedResult
	 *            the expected result
	 * @throws AssertionError
	 *             if the result of <code>matchDescriptive</code> is not
	 *             <code>expectedResult</code>
	 */
	private void testMatchDescriptive(int[] requirement, boolean invert, PatientHistory patientHistory,
			Result expectedResult, boolean isAvatar) throws AssertionError
	{
		if ( isAvatar )
		{
			patientHistory.setUserType( UserPatient.UserType.AVATAR_PROFILE );
		}
		// Create criterion
		AssociativeCriterion associativeCriterion = new AssociativeCriterion();
		associativeCriterion.setParent(DIAGNOSISBREAST);
		if ((requirement != null) && (requirement.length > 0))
		{
			CharacteristicCode[] requirementArray = new CharacteristicCode[requirement.length];
			for (int index = 0; index < requirementArray.length; index++)
				requirementArray[index] = new CharacteristicCode(requirement[index]);
			associativeCriterion.setRequirement(requirementArray);
		}
		associativeCriterion.setInvert(invert);

		// Test matchable.matchDescriptive
		BreastDiagnosisAttributeAssociativeMatchable matchable = new BreastDiagnosisAttributeAssociativeMatchable(
				associativeCriterion);
		Result result = matchable.matchDescriptive(patientHistory);
		assert result == expectedResult : "Matching " + patientHistory.getDiagnoses() + " to " + associativeCriterion
				+ " resulted in " + result + " instead of " + expectedResult;
	}
}
