/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.BooleanCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANATOMIC_SITE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BILATERAL_SYNCHRONOUS;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LEFT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RIGHT_BREAST;
import org.quantumleaphealth.screen.BilateralBooleanMatchable;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;

/**
 * Tests <code>BilateralBooleanMatchable</code>.
 * 
 * @author Tom Bechtold
 * @version 2008-07-07
 * @see BilateralBooleanMatchable
 */
@Test(description = "Syncronous bilateral matchable", groups = { "screen", "composite" })
public class TestBilateralBooleanMatchable
{

	/**
	 * Test invalid arguments in constructors
	 */
	public void testConstructors()
	{
		// Test null criterion and parent
		try
		{
			new BilateralBooleanMatchable(null);
			throw new AssertionError("Null criterion accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		try
		{
			new BilateralBooleanMatchable(new BooleanCriterion());
			throw new AssertionError("Null code accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		try
		{
			BooleanCriterion booleanCriterion = new BooleanCriterion();
			booleanCriterion.setCharacteristicCode(RIGHT_BREAST);
			new BilateralBooleanMatchable(booleanCriterion);
			throw new AssertionError("Wrong code accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
	}

	/**
	 * Test isCharacteristic method
	 */
	public void testIsCharacteristic()
	{
		// No diagnoses
		testIsCharacteristic(false, false, Result.FAIL, false);
		// Left only
		testIsCharacteristic(true, false, Result.FAIL, false);
		// Right only
		testIsCharacteristic(false, true, Result.FAIL, false);
		// Left and right
		testIsCharacteristic(true, true, Result.PASS, false);
		
		// now repeat the tests as an avatar
		// No diagnoses
		testIsCharacteristic(false, false, Result.AVATAR_SUBJECTIVE, true);
		// Left only
		testIsCharacteristic(true, false, Result.FAIL, true);
		// Right only
		testIsCharacteristic(false, true, Result.FAIL, true);
		// Left and right
		testIsCharacteristic(true, true, Result.PASS, true);
	}

	/**
	 * @param left
	 *            whether the left breast diagnosis is populated
	 * @param right
	 *            whether the right breast diagnosis is populated
	 * @param expectedResult
	 *            the expected result
	 */
	private void testIsCharacteristic(boolean left, boolean right, Result expectedResult, boolean isAvatar)
	{
		// Construct history with diagnoses
		PatientHistory patientHistory = new PatientHistory();
		if ( isAvatar )
		{
			patientHistory.setUserType( UserPatient.UserType.AVATAR_PROFILE );
		}
		
		if (left)
			addDiagnosis(patientHistory, LEFT_BREAST);
		if (right)
			addDiagnosis(patientHistory, RIGHT_BREAST);
		// Construct criterion
		BooleanCriterion booleanCriterion = new BooleanCriterion();
		booleanCriterion.setCharacteristicCode(BILATERAL_SYNCHRONOUS);

		// Create and test matchable
		BilateralBooleanMatchable bilateralBooleanMatchable = new BilateralBooleanMatchable(booleanCriterion);
		Result result = bilateralBooleanMatchable.matchDescriptive(patientHistory);
		assert result == expectedResult : "Left " + left + " and " + right + " matching to " + booleanCriterion
				+ " resulted in " + result + " instead of " + expectedResult;
	}

	/**
	 * Add a diagnosis to a history
	 * 
	 * @param patientHistory
	 *            the patient history
	 * @param location
	 *            the location of the diagnosis
	 */
	private void addDiagnosis(PatientHistory patientHistory, CharacteristicCode location)
	{
		if ((patientHistory == null) || (location == null))
			return;
		Diagnosis diagnosis = new Diagnosis();
		diagnosis.getValueHistory().put(ANATOMIC_SITE, location);
		patientHistory.getDiagnoses().add(diagnosis);
	}
}
