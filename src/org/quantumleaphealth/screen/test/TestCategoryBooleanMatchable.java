/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.BooleanCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREAST_CANCER;
import org.quantumleaphealth.screen.CategoryBooleanMatchable;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;

/**
 * Tests <code>CategoryBooleanMatchable</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-08
 * @see CategoryBooleanMatchable
 */
@Test(description = "Category boolean matchable", groups = { "screen", "composite" })
public class TestCategoryBooleanMatchable extends TestUtils
{

	/**
	 * Test isCharacteristic category found
	 */
	public void testIsCharacteristicFound()
	{
		testGetValue(Boolean.TRUE, Result.PASS, false);
		
		// repeat the test for an avatar
		testGetValue(Boolean.TRUE, Result.PASS, true);
	}

	/**
	 * Test isCharacteristic category not found
	 */
	public void testIsCharacteristicNotFound()
	{
		testGetValue(Boolean.FALSE, Result.FAIL, false);
		
		// repeat the test for an avatar
		testGetValue(Boolean.FALSE, Result.FAIL, true);
	}

	/**
	 * Test isCharacteristic category missing
	 */
	public void testIsCharacteristicMissing()
	{
		testGetValue(null, Result.FAIL, false);
		
		// repeat the test for an avatar
		testGetValue(null, Result.AVATAR_SUBJECTIVE, true);
	}

	/**
	 * Test getValue method not found in diagnosis valuehistory
	 */
	private void testGetValue(Boolean matching, Result expectedResult, boolean isAvatar)
	{
		int[] unique = getUniqueIntegers(2);
		// Create criterion with random requirement
		BooleanCriterion booleanCriterion = new BooleanCriterion();
		booleanCriterion.setCharacteristicCode(new CharacteristicCode(unique[0]));

		// Populate diagnosis history with either matching or non-matching value
		PatientHistory patientHistory = new PatientHistory();
		if ( isAvatar )
		{
			patientHistory.setUserType( UserPatient.UserType.AVATAR_PROFILE );
		}
		if (matching != null)
			patientHistory.getValueHistory().put(BREAST_CANCER,
					new CharacteristicCode(unique[matching.booleanValue() ? 0 : 1]));

		// Assert match
		Result result = new CategoryBooleanMatchable(booleanCriterion).matchDescriptive(patientHistory);
		assert expectedResult.equals(result) : "Expected " + expectedResult + ", got " + result;
	}
}
