/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.BooleanCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANATOMIC_SITE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LEFT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RIGHT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.INFLAMMATORY;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AFFIRMATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.NEGATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;
import org.quantumleaphealth.screen.InflammatoryBooleanMatchable;
import org.quantumleaphealth.screen.Matchable.Result;
import static org.quantumleaphealth.screen.Matchable.Result.*;
import org.testng.annotations.Test;

/**
 * Tests for the <code>InflammatoryBooleanMatchable</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-08
 * @see InflammatoryBooleanMatchable
 */
@Test(description = "Inflammatory boolean matchable", groups = { "screen", "composite" })
public class TestInflammatoryBooleanMatchable
{

	/**
	 * Test invalid arguments in constructors
	 */
	public void testConstructors()
	{
		// Test null criterion and parent
		try
		{
			new InflammatoryBooleanMatchable(null);
			throw new AssertionError("Null criterion accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		try
		{
			new InflammatoryBooleanMatchable(new BooleanCriterion());
			throw new AssertionError("Null code accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		try
		{
			BooleanCriterion booleanCriterion = new BooleanCriterion();
			booleanCriterion.setCharacteristicCode(AFFIRMATIVE);
			new InflammatoryBooleanMatchable(booleanCriterion);
			throw new AssertionError("Wrong code accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
	}

	/**
	 * Test if diagnosis available but inflammatory is not
	 */
	public void testNotAnswered()
	{
		Diagnosis diagnosis = new Diagnosis();
		diagnosis.getValueHistory().setValue(ANATOMIC_SITE, LEFT_BREAST);
		PatientHistory patientHistory = new PatientHistory();
		patientHistory.getDiagnoses().add(diagnosis);
		BooleanCriterion booleanCriterion = new BooleanCriterion();
		booleanCriterion.setCharacteristicCode(INFLAMMATORY);
		// Inverting a not-answered result would usually imply PASS, but not for
		// inflammatory
		booleanCriterion.setInvert(true);
		assert FAIL.equals(new InflammatoryBooleanMatchable(booleanCriterion).matchDescriptive(patientHistory)) : "Unanswered diagnosis passed inverted criterion";
	}

	/**
	 * Test matchDescriptive with unsure answer
	 */
	public void testMatchDescriptiveSubjective()
	{
		// Test each combination of left/right not inverted
		testMatchDescriptive(AFFIRMATIVE, UNSURE, false, PASS_SUBJECTIVE, false);
		testMatchDescriptive(NEGATIVE, UNSURE, false, PASS_SUBJECTIVE, false);
		testMatchDescriptive(null, UNSURE, false, PASS_SUBJECTIVE, false);
		testMatchDescriptive(UNSURE, AFFIRMATIVE, false, PASS_SUBJECTIVE, false);
		testMatchDescriptive(UNSURE, NEGATIVE, false, PASS_SUBJECTIVE, false);
		testMatchDescriptive(UNSURE, null, false, PASS_SUBJECTIVE, false);
		testMatchDescriptive(UNSURE, UNSURE, false, PASS_SUBJECTIVE, false);

		// Test each combination of left/right inverted
		testMatchDescriptive(AFFIRMATIVE, UNSURE, true, FAIL_SUBJECTIVE, false);
		testMatchDescriptive(NEGATIVE, UNSURE, true, FAIL_SUBJECTIVE, false);
		testMatchDescriptive(null, UNSURE, true, FAIL_SUBJECTIVE, false);
		testMatchDescriptive(UNSURE, AFFIRMATIVE, true, FAIL_SUBJECTIVE, false);
		testMatchDescriptive(UNSURE, NEGATIVE, true, FAIL_SUBJECTIVE, false);
		testMatchDescriptive(UNSURE, null, true, FAIL_SUBJECTIVE, false);
		testMatchDescriptive(UNSURE, UNSURE, true, FAIL_SUBJECTIVE, false);
	}

	/**
	 * Test matchDescriptive with unsure answer for an avatar
	 */
	public void testMatchDescriptiveSubjectiveAvatar()
	{
		// Test each combination of left/right not inverted
		testMatchDescriptive(AFFIRMATIVE, UNSURE, false, PASS_SUBJECTIVE, true);
		testMatchDescriptive(NEGATIVE, UNSURE, false, PASS_SUBJECTIVE, true);
		testMatchDescriptive(null, UNSURE, false, PASS_SUBJECTIVE, true);
		testMatchDescriptive(UNSURE, AFFIRMATIVE, false, PASS_SUBJECTIVE, true);
		testMatchDescriptive(UNSURE, NEGATIVE, false, PASS_SUBJECTIVE, true);
		testMatchDescriptive(UNSURE, null, false, PASS_SUBJECTIVE, true);
		testMatchDescriptive(UNSURE, UNSURE, false, PASS_SUBJECTIVE, true);

		// Test each combination of left/right inverted
		testMatchDescriptive(AFFIRMATIVE, UNSURE, true, FAIL_SUBJECTIVE, true);
		testMatchDescriptive(NEGATIVE, UNSURE, true, FAIL_SUBJECTIVE, true);
		testMatchDescriptive(null, UNSURE, true, FAIL_SUBJECTIVE, true);
		testMatchDescriptive(UNSURE, AFFIRMATIVE, true, FAIL_SUBJECTIVE, true);
		testMatchDescriptive(UNSURE, NEGATIVE, true, FAIL_SUBJECTIVE, true);
		testMatchDescriptive(UNSURE, null, true, FAIL_SUBJECTIVE, true);
		testMatchDescriptive(UNSURE, UNSURE, true, FAIL_SUBJECTIVE, true);
	}

	/**
	 * Test matchDescriptive with affirmative/negative answer
	 */
	public void testMatchDescriptiveObjective()
	{
		// No diagnoses
		testMatchDescriptive(null, null, false, FAIL, false);
		testMatchDescriptive(null, null, true, PASS, false);

		// Use the diagnosed side
		testMatchDescriptive(AFFIRMATIVE, null, false, PASS, false);
		testMatchDescriptive(NEGATIVE, null, false, FAIL, false);
		testMatchDescriptive(null, AFFIRMATIVE, false, PASS, false);
		testMatchDescriptive(null, NEGATIVE, false, FAIL, false);
		// Use the diagnosed side inverted
		testMatchDescriptive(AFFIRMATIVE, null, true, FAIL, false);
		testMatchDescriptive(NEGATIVE, null, true, PASS, false);
		testMatchDescriptive(null, AFFIRMATIVE, true, FAIL, false);
		testMatchDescriptive(null, NEGATIVE, true, PASS, false);

		// Equal diagnoses pass through
		testMatchDescriptive(AFFIRMATIVE, AFFIRMATIVE, false, PASS, false);
		testMatchDescriptive(NEGATIVE, NEGATIVE, false, FAIL, false);
		testMatchDescriptive(AFFIRMATIVE, AFFIRMATIVE, true, FAIL, false);
		testMatchDescriptive(NEGATIVE, NEGATIVE, true, PASS, false);

		// Unequal diagnoses are subjective
		testMatchDescriptive(AFFIRMATIVE, NEGATIVE, false, PASS_SUBJECTIVE, false);
		testMatchDescriptive(NEGATIVE, AFFIRMATIVE, false, PASS_SUBJECTIVE, false);
		testMatchDescriptive(AFFIRMATIVE, NEGATIVE, true, FAIL_SUBJECTIVE, false);
		testMatchDescriptive(NEGATIVE, AFFIRMATIVE, true, FAIL_SUBJECTIVE, false);
	}

	/**
	 * Test matchDescriptive with affirmative/negative answer for an avatar.
	 */
	public void testMatchDescriptiveObjectiveAvatar()
	{
		// No diagnoses
		testMatchDescriptive(null, null, false, AVATAR_SUBJECTIVE, true);
		testMatchDescriptive(null, null, true, AVATAR_SUBJECTIVE, true);

		// Use the diagnosed side
		testMatchDescriptive(AFFIRMATIVE, null, false, PASS, true);
		testMatchDescriptive(NEGATIVE, null, false, FAIL, true);
		testMatchDescriptive(null, AFFIRMATIVE, false, PASS, true);
		testMatchDescriptive(null, NEGATIVE, false, FAIL, true);
		// Use the diagnosed side inverted
		testMatchDescriptive(AFFIRMATIVE, null, true, FAIL, true);
		testMatchDescriptive(NEGATIVE, null, true, PASS, true);
		testMatchDescriptive(null, AFFIRMATIVE, true, FAIL, true);
		testMatchDescriptive(null, NEGATIVE, true, PASS, true);

		// Equal diagnoses pass through
		testMatchDescriptive(AFFIRMATIVE, AFFIRMATIVE, false, PASS, true);
		testMatchDescriptive(NEGATIVE, NEGATIVE, false, FAIL, true);
		testMatchDescriptive(AFFIRMATIVE, AFFIRMATIVE, true, FAIL, true);
		testMatchDescriptive(NEGATIVE, NEGATIVE, true, PASS, true);

		// Unequal diagnoses are subjective
		testMatchDescriptive(AFFIRMATIVE, NEGATIVE, false, PASS_SUBJECTIVE, true);
		testMatchDescriptive(NEGATIVE, AFFIRMATIVE, false, PASS_SUBJECTIVE, true);
		testMatchDescriptive(AFFIRMATIVE, NEGATIVE, true, FAIL_SUBJECTIVE, true);
		testMatchDescriptive(NEGATIVE, AFFIRMATIVE, true, FAIL_SUBJECTIVE, true);
	}

	/**
	 * Tests matchDescriptive
	 * 
	 * @param left
	 *            the value of the inflammatory characteristic for the left
	 *            breast or <code>null</code> if left breast not diagnosed
	 * @param right
	 *            the value of the inflammatory characteristic for the right
	 *            breast or <code>null</code> if right breast not diagnosed
	 * @param invert
	 *            whether the criterion is inverted
	 * @param expectedResult
	 *            the expected result
	 */
	private void testMatchDescriptive(CharacteristicCode left, CharacteristicCode right, boolean invert,
			Result expectedResult, boolean isAvatar)
	{
		// Construct history with diagnoses
		PatientHistory patientHistory = new PatientHistory();
		if ( isAvatar )
		{
			patientHistory.setUserType(UserPatient.UserType.AVATAR_PROFILE);
		}
		
		if (left != null)
			addDiagnosis(patientHistory, LEFT_BREAST, left);
		if (right != null)
			addDiagnosis(patientHistory, RIGHT_BREAST, right);
		// Construct criterion
		BooleanCriterion booleanCriterion = new BooleanCriterion();
		booleanCriterion.setCharacteristicCode(INFLAMMATORY);
		booleanCriterion.setInvert(invert);

		// Create and test matchable
		InflammatoryBooleanMatchable inflammatoryBooleanMatchable = new InflammatoryBooleanMatchable(booleanCriterion);
		Result result = inflammatoryBooleanMatchable.matchDescriptive(patientHistory);
		assert result == expectedResult : "Left " + left + " and " + right + " with invert " + invert + " resulted in "
				+ result + " instead of " + expectedResult;
	}

	/**
	 * Add an inflammatory diagnosis to a history
	 * 
	 * @param patientHistory
	 *            the patient history
	 * @param location
	 *            the anatomic site
	 * @param value
	 *            the value to store for the inflammatory characteristic
	 */
	private void addDiagnosis(PatientHistory patientHistory, CharacteristicCode location, CharacteristicCode value)
	{
		if ((patientHistory == null) || (location == null) || (value == null))
			return;
		Diagnosis diagnosis = new Diagnosis();
		diagnosis.getValueHistory().setValue(ANATOMIC_SITE, location);
		diagnosis.getValueHistory().setValue(INFLAMMATORY, value);
		patientHistory.getDiagnoses().add(diagnosis);
	}
}
