/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import java.util.Arrays;
import java.util.List;

import org.quantumleaphealth.model.trial.EligibilityCriterion.Type;
import org.quantumleaphealth.screen.CriterionProfile;
import org.quantumleaphealth.screen.Matchable.CharacteristicResult;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;

/**
 * Unit tests for the <code>CriterionProfile</code>. While this class calls the
 * Type and Result classes they are used only as getter/setter, so this test is
 * regarded as a "pure" unit test.
 * 
 * @author Tom Bechtold
 * @version 2008-07-08
 * @see CriterionProfile
 */
@Test(description = "Criterion profile data holding", groups = { "screen", "unit" })
public class TestCriterionProfile
{

	/**
	 * Test the setting via the constructor works in the getters
	 */
	public void testGetters()
	{
		List<CharacteristicResult> results = Arrays.asList(new CharacteristicResult("foobar", Result.FAIL_SUBJECTIVE));
		CriterionProfile criterionProfile = new CriterionProfile(Result.PASS_SUBJECTIVE, Type.EXCLUDE, "foo", "bar",
				results);
		assert Result.PASS_SUBJECTIVE.equals(criterionProfile.getResult()) : "Unexpected result "
				+ criterionProfile.getResult();
		assert Type.EXCLUDE.equals(criterionProfile.getType()) : "Unexpected type " + criterionProfile.getType();
		assert "foo".equals(criterionProfile.getText()) : "Unexpected text " + criterionProfile.getText();
		assert "bar".equals(criterionProfile.getQualifier()) : "Unexpected qualifier "
				+ criterionProfile.getQualifier();
		assert (criterionProfile.getCharacteristicResults() != null)
				&& (criterionProfile.getCharacteristicResults().size() == 1)
				&& (criterionProfile.getCharacteristicResults().get(0) != null)
				&& "foobar".equals(criterionProfile.getCharacteristicResults().get(0).getCharacteristic())
				&& Result.FAIL_SUBJECTIVE.equals(criterionProfile.getCharacteristicResults().get(0).getResult()) : "Unexpected characteristic results"
				+ criterionProfile.getCharacteristicResults();
	}
}
