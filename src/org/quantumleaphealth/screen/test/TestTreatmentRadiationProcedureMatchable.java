/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANATOMIC_SITE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISBREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LEFT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.MOSTRECENT;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RADIATION;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RADIATION_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RADIATION_BREAST_IPSILATERAL;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.OVARIAN_RADIOTHERAPY;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.THRESHOLD;

import java.util.Calendar;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.Procedure;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.patient.YearMonth;
import org.quantumleaphealth.model.trial.TreatmentProcedureCriterion;
import org.quantumleaphealth.model.trial.AssociativeCriterion.SetOperation;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.screen.TreatmentRadiationProcedureMatchable;
import org.quantumleaphealth.screen.UnsupportedCriterionException;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;

/**
 * Tests <code>TreatmentRadiationProcedureMatchable</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 * @see TreatmentRadiationProcedureMatchable
 */
@Test(description = "Treatment radiation procedure matchable", groups = { "screen", "composite" })
public class TestTreatmentRadiationProcedureMatchable
{

	public void matchAvatarRecentIpsilateralRadiation()
	{
		matchRecentIpsilateralRadiation(true, true, Result.PASS);
	}

	public void matchNormalRecentIpsilateralRadiation()
	{
		matchRecentIpsilateralRadiation(false, true, Result.PASS);
	}

	public void noMatchAvatarRecentIpsilateralRadiation()
	{
		matchRecentIpsilateralRadiation(true, false, Result.AVATAR_SUBJECTIVE);
	}

	public void noMatchNormalRecentIpsilateralRadiation()
	{
		matchRecentIpsilateralRadiation(false, false, Result.FAIL);
	}

	/**
	 * Tests a recent ipsilateral radiation
	 */
	private void matchRecentIpsilateralRadiation(boolean isAvatar, boolean doMatch, Result ExpectedResult)
	{
		// Left-breast radiation performed one month ago
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		Short year = Short.valueOf((short) calendar.get(Calendar.YEAR));
		YearMonth procedureYearMonth = new YearMonth();
		procedureYearMonth.setYear(year);
		procedureYearMonth.setMonth((byte) (calendar.get(Calendar.MONTH) + 1));
		Procedure recentLeftBreastRadiation = new Procedure();
		recentLeftBreastRadiation.setKind(RADIATION);
		recentLeftBreastRadiation.setLocation(LEFT_BREAST);
		recentLeftBreastRadiation.setType(RADIATION_BREAST);
		recentLeftBreastRadiation.setStarted(procedureYearMonth);
		// Diagnosis year is same as procedure
		YearMonth diagnosisYearMonth = new YearMonth();
		diagnosisYearMonth.setYear(year);
		
		PatientHistory history = new PatientHistory();
		if (isAvatar)
		{
			history.setUserType(UserPatient.UserType.AVATAR_PROFILE);
		}
		
		// Recent diagnosis is left breast (ipsilateral)
		Diagnosis diagnosis = new Diagnosis();
		diagnosis.getValueHistory().setValue(ANATOMIC_SITE, LEFT_BREAST);
		
		if ( doMatch == true )
		{	
			history.getYearMonthHistory().put(DIAGNOSISBREAST, procedureYearMonth);
			history.getProcedures().add(recentLeftBreastRadiation);
			history.getDiagnoses().add(diagnosis);
		}	

		// Criterion specifies any ipsilateral radiation for this diagnosis
		TreatmentProcedureCriterion recentIpsilateralRadiationCriterion = new TreatmentProcedureCriterion();
		recentIpsilateralRadiationCriterion.setParent(RADIATION);
		recentIpsilateralRadiationCriterion.setRequirement(new CharacteristicCode[] { RADIATION_BREAST_IPSILATERAL });
		recentIpsilateralRadiationCriterion.setSetOperation(SetOperation.INTERSECT);
		recentIpsilateralRadiationCriterion.setQualifiers(new CharacteristicCode[] { MOSTRECENT });

		// History should match pass since most recent is true
		try
		{
			TreatmentRadiationProcedureMatchable matchable = new TreatmentRadiationProcedureMatchable(
					recentIpsilateralRadiationCriterion);
			Result result = matchable.matchDescriptive(history);
			assert (result == ExpectedResult) : "Expected " + ExpectedResult + " result, got " + result;
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
			throw new AssertionError(unsupportedCriterionException);
		}
	}

	/**
	 * Tests ovary radiation.
	 * 
	 * @author Tom Maple
	 */
	public void matchAvatarOvaryRadiation()
	{
		matchOvaryRadiation(true, true, Result.PASS);		
	}
	
	public void matchNormalOvaryRadiation()
	{
		matchOvaryRadiation(false, true, Result.PASS);		
	}

	public void noMatchAvatarOvaryRadiation()
	{
		matchOvaryRadiation(true, false, Result.AVATAR_SUBJECTIVE);		
	}
	
	public void noMatchNormalOvaryRadiation()
	{
		matchOvaryRadiation(false, false, Result.FAIL);		
	}

	private void matchOvaryRadiation(boolean isAvatar, boolean doMatch, Result ExpectedResult)
	{
		// Ovary radiation performed
		Procedure ovaryRadiation = new Procedure();
		ovaryRadiation.setKind(RADIATION);
		ovaryRadiation.setLocation(OVARIAN_RADIOTHERAPY);
		PatientHistory history = new PatientHistory();
		if (isAvatar)
		{
			history.setUserType(UserPatient.UserType.AVATAR_PROFILE);
		}
		
		Diagnosis diagnosis = new Diagnosis();
		diagnosis.getValueHistory().setValue(ANATOMIC_SITE, OVARIAN_RADIOTHERAPY);
		
		if ( doMatch )
		{	
			history.getProcedures().add(ovaryRadiation);
			history.getDiagnoses().add(diagnosis);
		}

		// Criterion specifies any ovarian radiation
		TreatmentProcedureCriterion ovarianRadiationCriterion = new TreatmentProcedureCriterion();
		ovarianRadiationCriterion.setParent(RADIATION);
		ovarianRadiationCriterion.setRequirement(new CharacteristicCode[] { OVARIAN_RADIOTHERAPY });
		ovarianRadiationCriterion.setSetOperation(SetOperation.INTERSECT);

		// History should match pass since ovary radiation performed
		try
		{
			TreatmentRadiationProcedureMatchable matchable = new TreatmentRadiationProcedureMatchable(
					ovarianRadiationCriterion);
			Result result = matchable.matchDescriptive(history);
			assert (result == ExpectedResult) : "Expected " + ExpectedResult + " result, got " + result;
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
			throw new AssertionError(unsupportedCriterionException);
		}
	}
}
