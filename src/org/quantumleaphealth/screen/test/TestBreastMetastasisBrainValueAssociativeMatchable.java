/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREASTMETASTASIS_BRAIN;
import org.quantumleaphealth.screen.BreastMetastasisBrainValueAssociativeMatchable;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;

/**
 * Tests <code>BreastMetastasisBrainValueAssociativeMatchable</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-08
 * @see BreastMetastasisBrainValueAssociativeMatchable
 */
@Test(description = "Breast metastasis to brain matchable", groups = { "screen", "composite" })
public class TestBreastMetastasisBrainValueAssociativeMatchable extends TestUtils
{

	/**
	 * Test invalid arguments in constructors
	 */
	public void testConstructors()
	{
		// Test null criterion and parent
		try
		{
			new BreastMetastasisBrainValueAssociativeMatchable(null);
			throw new AssertionError("Null criterion accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		try
		{
			new BreastMetastasisBrainValueAssociativeMatchable(new AssociativeCriterion());
			throw new AssertionError("Null parent accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		try
		{
			AssociativeCriterion associativeCriterion = new AssociativeCriterion();
			associativeCriterion.setParent(BREASTMETASTASIS_BRAIN);
			new BreastMetastasisBrainValueAssociativeMatchable(associativeCriterion);
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
			throw new AssertionError(illegalArgumentException);
		}
	}

	/**
	 * Test getValue method found in diagnosis valuehistory
	 */
	public void testGetValueFound()
	{
		testGetValue(Boolean.TRUE, Result.PASS, false);
		
		// repeat test for an avatar
		testGetValue(Boolean.TRUE, Result.PASS, true);
	}

	/**
	 * Test getValue method not found in diagnosis valuehistory
	 */
	public void testGetValueNotFound()
	{
		testGetValue(Boolean.FALSE, Result.FAIL, false);
		
		// repeat test for an avatar
		testGetValue(Boolean.FALSE, Result.FAIL, true);
	}

	/**
	 * Test getValue method missing in diagnosis valuehistory
	 */
	public void testGetValueMissing()
	{
		testGetValue(null, Result.FAIL, false);
		
		// repeat test for an avatar
		testGetValue(null, Result.AVATAR_SUBJECTIVE, true);
	}

	/**
	 * Test getValue method not found in diagnosis valuehistory
	 */
	private void testGetValue(Boolean matching, Result expectedResult, boolean isAvatar)
	{
		int[] unique = getUniqueIntegers(3);
		// Create criterion with two random requirements
		AssociativeCriterion associativeCriterion = new AssociativeCriterion();
		associativeCriterion.setParent(BREASTMETASTASIS_BRAIN);
		associativeCriterion.setRequirement(new CharacteristicCode[] { new CharacteristicCode(unique[0]),
				new CharacteristicCode(unique[1]) });

		// Populate diagnosis history with either matching or non-matching value
		PatientHistory patientHistory = new PatientHistory();
		if ( isAvatar )
		{
			patientHistory.setUserType( UserPatient.UserType.AVATAR_PROFILE );
		}
		
		if (matching != null)
		{
			Diagnosis diagnosis = new Diagnosis();
			diagnosis.getValueHistory().put(BREASTMETASTASIS_BRAIN,
					new CharacteristicCode(unique[matching.booleanValue() ? 0 : 2]));
			patientHistory.getDiagnoses().add(diagnosis);
		}

		// Assert match
		Result result = new BreastMetastasisBrainValueAssociativeMatchable(associativeCriterion)
				.matchDescriptive(patientHistory);
		assert expectedResult.equals(result) : "Expected " + expectedResult + ", got " + result;
	}
}
