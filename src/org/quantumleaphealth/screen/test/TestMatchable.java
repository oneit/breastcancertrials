/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.quantumleaphealth.screen.Matchable;
import org.quantumleaphealth.screen.Matchable.CharacteristicResult;
import org.quantumleaphealth.screen.Matchable.Result;
import static org.quantumleaphealth.screen.Matchable.Result.PASS;
import static org.quantumleaphealth.screen.Matchable.Result.PASS_SUBJECTIVE;
import static org.quantumleaphealth.screen.Matchable.Result.AVATAR_SUBJECTIVE;
import static org.quantumleaphealth.screen.Matchable.Result.FAIL_SUBJECTIVE;
import static org.quantumleaphealth.screen.Matchable.Result.FAIL;
import org.testng.annotations.Test;

/**
 * Unit tests for the <code>Matchable</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-07
 * @see Matchable
 */
@Test(description = "Matchable", groups = { "screen", "unit" })
public class TestMatchable
{

	/**
	 * Tests the members of the Result enumeration
	 */
	public void testResultEnumeration()
	{
		List<Result> results = new LinkedList<Result>(Arrays.asList(Result.values()));
		assert (results != null) && (results.size() == 5) : "Result enumeration is not length 5";
		assert results.remove(PASS) : "Pass not found in enumeration";
		assert results.remove(PASS_SUBJECTIVE) : "Pass-subjective not found in enumeration";
		assert results.remove(AVATAR_SUBJECTIVE) : "Pass-subjective not found in enumeration";
		assert results.remove(FAIL_SUBJECTIVE) : "Fail-subjective not found in enumeration";
		assert results.remove(FAIL) : "Fail not found in enumeration";
		assert results.isEmpty() : "Unexpected members of Result: " + results;
	}

	/**
	 * Test inverting each member of Result enumeration
	 */
	public void testResultInvert()
	{
		assert FAIL.equals(PASS.invert()) : "Inverting pass is not fail " + PASS.invert();
		assert FAIL_SUBJECTIVE.equals(PASS_SUBJECTIVE.invert()) : "Inverting pass-subjective is not fail-subjective "
				+ PASS_SUBJECTIVE.invert();
		assert AVATAR_SUBJECTIVE.equals(AVATAR_SUBJECTIVE.invert()) : "Inverting avatar_subjective is not avatar_subjective "
			+ AVATAR_SUBJECTIVE.invert();
		assert PASS_SUBJECTIVE.equals(FAIL_SUBJECTIVE.invert()) : "Inverting fail-subjective is not pass-subjective "
				+ FAIL_SUBJECTIVE.invert();
		assert PASS.equals(FAIL.invert()) : "Inverting fail is not pass " + FAIL.invert();
	}

	/**
	 * Test Result.better()
	 */
	public void testResultBetter()
	{
		// Test pass
		testResultBetter(PASS, null, PASS);
		testResultBetter(PASS, PASS, PASS);
		testResultBetter(PASS, PASS_SUBJECTIVE, PASS);
		testResultBetter(PASS, FAIL_SUBJECTIVE, PASS);
		testResultBetter(PASS, FAIL, PASS);
		testResultBetter(PASS, AVATAR_SUBJECTIVE, PASS);
		
		// Test pass-subjective
		testResultBetter(PASS_SUBJECTIVE, null, PASS_SUBJECTIVE);
		testResultBetter(PASS_SUBJECTIVE, PASS, PASS);
		testResultBetter(PASS_SUBJECTIVE, PASS_SUBJECTIVE, PASS_SUBJECTIVE);
		testResultBetter(PASS_SUBJECTIVE, FAIL_SUBJECTIVE, PASS_SUBJECTIVE);
		testResultBetter(PASS_SUBJECTIVE, FAIL, PASS_SUBJECTIVE);
		testResultBetter(PASS_SUBJECTIVE, AVATAR_SUBJECTIVE, PASS_SUBJECTIVE);
		
		// Test fail-subjective
		testResultBetter(FAIL_SUBJECTIVE, null, FAIL_SUBJECTIVE);
		testResultBetter(FAIL_SUBJECTIVE, PASS, PASS);
		testResultBetter(FAIL_SUBJECTIVE, PASS_SUBJECTIVE, PASS_SUBJECTIVE);
		testResultBetter(FAIL_SUBJECTIVE, FAIL_SUBJECTIVE, FAIL_SUBJECTIVE);
		testResultBetter(FAIL_SUBJECTIVE, FAIL, FAIL_SUBJECTIVE);
		testResultBetter(FAIL_SUBJECTIVE, AVATAR_SUBJECTIVE, FAIL_SUBJECTIVE);
		
		// Test fail
		testResultBetter(FAIL, null, FAIL);
		testResultBetter(FAIL, PASS, PASS);
		testResultBetter(FAIL, PASS_SUBJECTIVE, PASS_SUBJECTIVE);
		testResultBetter(FAIL, FAIL_SUBJECTIVE, FAIL_SUBJECTIVE);
		testResultBetter(FAIL, FAIL, FAIL);
		testResultBetter(FAIL, AVATAR_SUBJECTIVE, FAIL);

		// Test AVATAR_SUBJECTIVE
		testResultBetter(AVATAR_SUBJECTIVE, null, AVATAR_SUBJECTIVE);
		testResultBetter(AVATAR_SUBJECTIVE, PASS, PASS);
		testResultBetter(AVATAR_SUBJECTIVE, PASS_SUBJECTIVE, PASS_SUBJECTIVE);
		testResultBetter(AVATAR_SUBJECTIVE, FAIL_SUBJECTIVE, FAIL_SUBJECTIVE);
		testResultBetter(AVATAR_SUBJECTIVE, FAIL, FAIL);
		testResultBetter(AVATAR_SUBJECTIVE, AVATAR_SUBJECTIVE, AVATAR_SUBJECTIVE);
		
		// Test both nulls
		assert null == Matchable.better(null, null) : "Better of two nulls is not null " + Matchable.better(null, null);
	}

	/**
	 * Tests Result.better()
	 * 
	 * @param result1
	 *            the first result
	 * @param result2
	 *            the second result
	 * @param resultExpected
	 *            the expected result of <code>result1.better(result2)</code>
	 * @throws AssertionError
	 *             if the expected result is not <code>expectedResult</code>
	 */
	private void testResultBetter(Result result1, Result result2, Result resultExpected) throws AssertionError
	{
		// Test both instance method and static method
		assert resultExpected.equals(result1.better(result2)) : result1 + " better " + result2 + " != "
				+ resultExpected + " (" + result1.better(result2) + ')';
		assert resultExpected.equals(Matchable.better(result1, result2)) : result1 + " Matchable.better " + result2
				+ " != " + resultExpected + " (" + Matchable.better(result1, result2) + ')';
	}

	/**
	 * Test Result.worse()
	 */
	public void testResultWorse()
	{
		// Test pass
		testResultWorse(PASS, null, null);
		testResultWorse(PASS, PASS, PASS);
		testResultWorse(PASS, PASS_SUBJECTIVE, PASS_SUBJECTIVE);
		testResultWorse(PASS, FAIL_SUBJECTIVE, FAIL_SUBJECTIVE);
		testResultWorse(PASS, FAIL, FAIL);
		testResultWorse(PASS, AVATAR_SUBJECTIVE, PASS);
		
		// Test pass-subjective
		testResultWorse(PASS_SUBJECTIVE, null, null);
		testResultWorse(PASS_SUBJECTIVE, PASS, PASS_SUBJECTIVE);
		testResultWorse(PASS_SUBJECTIVE, PASS_SUBJECTIVE, PASS_SUBJECTIVE);
		testResultWorse(PASS_SUBJECTIVE, FAIL_SUBJECTIVE, FAIL_SUBJECTIVE);
		testResultWorse(PASS_SUBJECTIVE, FAIL, FAIL);
		testResultWorse(PASS_SUBJECTIVE, AVATAR_SUBJECTIVE, PASS_SUBJECTIVE);

		// Test fail-subjective
		testResultWorse(FAIL_SUBJECTIVE, null, null);
		testResultWorse(FAIL_SUBJECTIVE, PASS, FAIL_SUBJECTIVE);
		testResultWorse(FAIL_SUBJECTIVE, PASS_SUBJECTIVE, FAIL_SUBJECTIVE);
		testResultWorse(FAIL_SUBJECTIVE, FAIL_SUBJECTIVE, FAIL_SUBJECTIVE);
		testResultWorse(FAIL_SUBJECTIVE, FAIL, FAIL);
		testResultWorse(FAIL_SUBJECTIVE, AVATAR_SUBJECTIVE, FAIL_SUBJECTIVE);

		// Test fail
		testResultWorse(FAIL, null, null);
		testResultWorse(FAIL, PASS, FAIL);
		testResultWorse(FAIL, PASS_SUBJECTIVE, FAIL);
		testResultWorse(FAIL, FAIL_SUBJECTIVE, FAIL);
		testResultWorse(FAIL, FAIL, FAIL);
		testResultWorse(FAIL, AVATAR_SUBJECTIVE, FAIL);
		
		// Test Avatar_Subjective
		testResultWorse(AVATAR_SUBJECTIVE, null, null);
		testResultWorse(AVATAR_SUBJECTIVE, PASS, PASS);
		testResultWorse(AVATAR_SUBJECTIVE, PASS_SUBJECTIVE, PASS_SUBJECTIVE);
		testResultWorse(AVATAR_SUBJECTIVE, FAIL_SUBJECTIVE, FAIL_SUBJECTIVE);
		testResultWorse(AVATAR_SUBJECTIVE, FAIL, FAIL);
		testResultWorse(AVATAR_SUBJECTIVE, AVATAR_SUBJECTIVE, AVATAR_SUBJECTIVE);

		// Test both nulls
		assert null == Matchable.worse(null, null) : "Worse of two nulls is not null " + Matchable.worse(null, null);
	}

	/**
	 * Tests Result.worse()
	 * 
	 * @param result1
	 *            the first result
	 * @param result2
	 *            the second result
	 * @param resultExpected
	 *            the expected result of <code>result1.worse(result2)</code>
	 * @throws AssertionError
	 *             if the expected result is not <code>expectedResult</code>
	 */
	private void testResultWorse(Result result1, Result result2, Result resultExpected) throws AssertionError
	{
		// Test both instance method and static method
		if (result1 != null)
			assert resultExpected == result1.worse(result2) : result1 + " worse " + result2 + " != " + resultExpected
					+ " (" + result1.worse(result2) + ')';
		assert resultExpected == Matchable.worse(result1, result2) : result1 + " Matchable.worse " + result2 + " != "
				+ resultExpected + " (" + Matchable.worse(result1, result2) + ')';
	}

	/**
	 * Test the default and full constructors for characteristic result
	 */
	public void testCharacteristicResultConstructors()
	{
		// Default constructor
		CharacteristicResult characteristicResult = new CharacteristicResult();
		assert (characteristicResult != null) && (characteristicResult.getCharacteristic() == null) : "Empty constructor has non-null characteristic "
				+ characteristicResult.getCharacteristic();
		assert characteristicResult.getResult() == null : "Empty constructor has non-null result "
				+ characteristicResult.getResult();
		// Full constructor
		characteristicResult = new CharacteristicResult("foobar", Result.PASS_SUBJECTIVE);
		assert "foobar".equals(characteristicResult.getCharacteristic()) : "Constructor did not store characteristic as 'foobar' "
				+ characteristicResult.getCharacteristic();
		assert PASS_SUBJECTIVE.equals(characteristicResult.getResult()) : "Constructor did not store pass-subjective result "
				+ characteristicResult.getResult();
	}

	/**
	 * Test get/set characteristic result
	 */
	public void testCharacteristicResultGetSet()
	{
		CharacteristicResult characteristicResult = new CharacteristicResult();
		characteristicResult.setCharacteristic("foobar");
		assert "foobar".equals(characteristicResult.getCharacteristic()) : "Set did not store characteristic as 'foobar' "
				+ characteristicResult.getCharacteristic();
		characteristicResult.setResult(PASS_SUBJECTIVE);
		assert PASS_SUBJECTIVE.equals(characteristicResult.getResult()) : "Set did not store pass-subjective result "
				+ characteristicResult.getResult();
	}

	public void testCharacteristicResultMatches()
	{
		// Tests isBetter true with matching characteristics
		CharacteristicResult characteristicResult = new CharacteristicResult("foo", FAIL_SUBJECTIVE);
		CharacteristicResult otherResult = new CharacteristicResult("foo", PASS_SUBJECTIVE);
		assert characteristicResult.matches(otherResult, true) : "Characteristics "
				+ characteristicResult.getCharacteristic() + " and " + otherResult.getCharacteristic()
				+ " do not match";
		assert PASS_SUBJECTIVE.equals(characteristicResult.getResult()) : "Matched characteristic was not upgraded to pass-subjective "
				+ characteristicResult.getResult();

		// Tests isBetter true with non-matching characteristics
		characteristicResult = new CharacteristicResult("foo", FAIL_SUBJECTIVE);
		otherResult = new CharacteristicResult("bar", PASS_SUBJECTIVE);
		assert !characteristicResult.matches(otherResult, true) : "Characteristics "
				+ characteristicResult.getCharacteristic() + " and " + otherResult.getCharacteristic() + " match";
		assert FAIL_SUBJECTIVE.equals(characteristicResult.getResult()) : "Unmatched characteristic was updated to "
				+ characteristicResult.getResult();

		// Tests isBetter false with matching characteristics
		characteristicResult = new CharacteristicResult("foo", PASS_SUBJECTIVE);
		otherResult = new CharacteristicResult("foo", FAIL_SUBJECTIVE);
		assert characteristicResult.matches(otherResult, false) : "Characteristics "
				+ characteristicResult.getCharacteristic() + " and " + otherResult.getCharacteristic()
				+ " do not match";
		assert FAIL_SUBJECTIVE.equals(characteristicResult.getResult()) : "Matched characteristic was not downgraded to fail-subjective "
				+ characteristicResult.getResult();

		// Tests isBetter false with non-matching characteristics
		characteristicResult = new CharacteristicResult("foo", PASS_SUBJECTIVE);
		otherResult = new CharacteristicResult("bar", FAIL_SUBJECTIVE);
		assert !characteristicResult.matches(otherResult, false) : "Characteristics "
				+ characteristicResult.getCharacteristic() + " and " + otherResult.getCharacteristic() + " match";
		assert PASS_SUBJECTIVE.equals(characteristicResult.getResult()) : "Unmatched characteristic was updated to "
				+ characteristicResult.getResult();
	}
}
