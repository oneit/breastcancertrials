/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import org.quantumleaphealth.screen.CriterionProfile;
import org.quantumleaphealth.screen.EligibilityProfile;
import org.testng.annotations.Test;

/**
 * Unit tests for <code>EligibilityProfile</code>
 * 
 * @author Tom Bechtold
 * @version 2008-10-09
 * @see EligibilityProfile
 */
@Test(description = "Eligibility profile", groups = { "screen", "unit" })
public class TestEligibilityProfile extends TestUtils
{

	/**
	 * Test constructor
	 */
	public void testConstructor()
	{
		// Test parameters can be retrieved
		String description = "Hello World";
		assert new EligibilityProfile(true, description).getDescription() == description : "Could not retrieve description";
		assert new EligibilityProfile(true, null).isMatchDefinitive() : "Could not retrieve match";
	}

	/**
	 * Test adding and retrieving a criterion profile
	 */
	public void testAddCriterionProfile()
	{
		// Ensure list is empty
		EligibilityProfile eligibilityProfile = new EligibilityProfile(true, null);
		assert eligibilityProfile.getCriteria() != null : "Null list";
		assert eligibilityProfile.getCriteria().isEmpty() : "Non-empty list";

		// Ensure added profile is retrieved
		eligibilityProfile.addCriterionProfile(new CriterionProfile(null, null, "foo", "bar", null));
		assert eligibilityProfile.getCriteria().size() == 1 : "List does not have one element";
		assert "foo".equals(eligibilityProfile.getCriteria().get(0).getText())
				&& "bar".equals(eligibilityProfile.getCriteria().get(0).getQualifier()) : "List element could not be retrieved";
	}

	/**
	 * Test compareTo
	 */
	public void testCompareTo()
	{
		// -1 if null
		EligibilityProfile profile1 = new EligibilityProfile(true, null);
		assert profile1.compareTo(null) < 0 : "Null comparison";

		// 0 if both descriptions null
		EligibilityProfile profile2 = new EligibilityProfile(true, null);
		assert profile1.compareTo(profile2) == 0 : "Neither description set";

		profile1 = new EligibilityProfile(true, "a");
		assert profile1.compareTo(profile2) < 0 : "Description a < null";
		profile2 = new EligibilityProfile(true, "b");
		assert profile1.compareTo(profile2) < 0 : "Description a < b";
		profile1 = new EligibilityProfile(true, null);
		assert profile1.compareTo(profile2) > 0 : "Description null > b";
	}
}
