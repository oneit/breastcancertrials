/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import org.quantumleaphealth.model.trial.BooleanCriterion;
import org.quantumleaphealth.screen.UnsupportedCriterionException;
import org.testng.annotations.Test;

/**
 * Unit tests for the <code>UnsupportedCriterionException</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-08
 * @see UnsupportedCriterionException
 */
@Test(description = "Unsupported criterion exception", groups = { "screen", "unit" })
public class TestUnsupportedCriterionException
{

	/**
	 * Test constructor
	 */
	public void testConstructor()
	{
		// Empty
		UnsupportedCriterionException exception = new UnsupportedCriterionException();
		assert null == exception.getCause() : "Cause not null in empty exception" + exception.getCause();
		assert null == exception.getCriterion() : "Criterion not null in empty exception" + exception.getCriterion();
		// Criterion specified
		exception = new UnsupportedCriterionException(new BooleanCriterion());
		assert null != exception.getCriterion() : "Criterion null in exception";
	}

}
