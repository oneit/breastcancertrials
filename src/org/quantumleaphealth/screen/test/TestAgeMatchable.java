/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AGE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BIRTHDATE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.YEAR;

import java.util.Calendar;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.ComparativeCriterion;
import org.quantumleaphealth.model.patient.UserPatient.UserType;
import org.quantumleaphealth.screen.AgeMatchable;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;

/**
 * Tests for the <code>AgeMatchable</code>. This composite test relies upon the
 * following classes:
 * <ul>
 * <li><code>PatientHistory</code></li>
 * <li><code>YearMonthHistory</code></li>
 * <li><code>ComparativeCriterion</code></li>
 * <li><code>Matchable.Result</code></li>
 * </ul>
 * 
 * @author Tom Bechtold
 * @version 2008-07-07
 * @see AgeMatchable
 */
@Test(description = "Age", groups = { "screen", "composite" })
public class TestAgeMatchable extends TestUtils
{

	/**
	 * The current four-digit year
	 */
	private final int currentYear = Calendar.getInstance().get(Calendar.YEAR);
	/**
	 * Maximum age
	 */
	private final static int MAXIMUM_AGE = 120;

	/**
	 * Test age is equal to minimum requirement
	 */
	public void matchMinimumAgeEqual()
	{
		int requirement = GENERATOR.nextInt(MAXIMUM_AGE) + 1;
		match(false, requirement, requirement, true);
	}

	/**
	 * Test age is one greater than minimum requirement
	 */
	public void matchMinimumAgeOneGreater()
	{
		int requirement = GENERATOR.nextInt(MAXIMUM_AGE) + 1;
		match(false, requirement, requirement + 1, true);
	}

	/**
	 * Test age is at least two greater than minimum requirement
	 */
	public void matchMinimumAgeMuchGreater()
	{
		int requirement = GENERATOR.nextInt(MAXIMUM_AGE) + 1;
		match(false, requirement, requirement + GENERATOR.nextInt(MAXIMUM_AGE) + 2, true);
	}

	/**
	 * Test age is one less than minimum requirement
	 */
	public void matchMinimumAgeOneLess()
	{
		int requirement = GENERATOR.nextInt(MAXIMUM_AGE) + 1;
		match(false, requirement, requirement - 1, false);
	}

	/**
	 * Test age is at least two less than minimum requirement
	 */
	public void matchMinimumAgeMuchLess()
	{
		int requirement = GENERATOR.nextInt(MAXIMUM_AGE) + 2;
		match(false, requirement, Math.max(requirement - GENERATOR.nextInt(MAXIMUM_AGE) - 2, 0), false);
	}

	/**
	 * Test age is equal to maximum requirement
	 */
	public void matchMaximumAgeEqual()
	{
		int requirement = GENERATOR.nextInt(MAXIMUM_AGE) + 1;
		match(true, requirement, requirement, true);
	}

	/**
	 * Test age is one greater than maximum requirement. This test asserts
	 * <code>true</code> to cover the special edge condition of the matchable --
	 * unknown month and day of birth.
	 */
	public void matchMaximumAgeOneGreater()
	{
		int requirement = GENERATOR.nextInt(MAXIMUM_AGE) + 1;
		match(true, requirement, requirement + 1, true);
	}

	/**
	 * Test age is at least two greater than maximum requirement
	 */
	public void matchMaximumAgeMuchGreater()
	{
		int requirement = GENERATOR.nextInt(MAXIMUM_AGE) + 1;
		match(true, requirement, requirement + GENERATOR.nextInt(MAXIMUM_AGE) + 2, false);
	}

	/**
	 * Test age is one less than maximum requirement
	 */
	public void matchMaximumAgeOneLess()
	{
		int requirement = GENERATOR.nextInt(MAXIMUM_AGE) + 1;
		match(true, requirement, requirement - 1, true);
	}

	/**
	 * Test age is at least two less than maximum requirement
	 */
	public void matchMaximumAgeMuchLess()
	{
		int requirement = GENERATOR.nextInt(MAXIMUM_AGE) + 1;
		match(true, requirement, Math.max(requirement - GENERATOR.nextInt(MAXIMUM_AGE) - 2, 0), true);
	}

	/**
	 * Test age requirement criterion against patient age
	 * 
	 * @param maximum
	 *            <code>true</code> if the age requirement is a maximum,
	 *            <code>false</code> if a minimum
	 * @param requirement
	 *            the required age
	 * @param birthDateYearsAgo
	 *            the number of years ago the patient was born
	 * @param asserted
	 *            the asserted result; <code>PASS</code> if <code>true</code> or
	 *            <code>FAIL</code> if <code>false</code>
	 * @throws AssertionError
	 *             if the matching result does not equal <code>asserted</code>
	 */
	private void match(boolean maximum, int requirement, int birthDateYearsAgo, boolean asserted)
	{
		// Populate history with age
		PatientHistory history = new PatientHistory();
		history.getYearMonthHistory().setShort(BIRTHDATE, Short.valueOf((short) (currentYear - birthDateYearsAgo)));

		// Create criterion and pass to matchable
		ComparativeCriterion ageCriterion = new ComparativeCriterion();
		ageCriterion.setCharacteristicCode(AGE);
		// Matchable only supports year unit of measure
		ageCriterion.setUnitOfMeasure(YEAR);
		ageCriterion.setRequirement(requirement);
		ageCriterion.setInvert(maximum);
		AgeMatchable ageMatchable = new AgeMatchable(ageCriterion);
		Result result = ageMatchable.matchDescriptive(history);
		assert (result != null) && result.equals(asserted ? Result.PASS : Result.FAIL) : "Expected " + asserted
				+ " result, got " + result + " for maximum " + maximum + ", requirement " + requirement
				+ ", years ago " + birthDateYearsAgo;
	}
	
	public void matchAvatar()
	{
		// Populate history with age
		PatientHistory history = new PatientHistory();
		history.setUserType(UserType.AVATAR_PROFILE);

		// Create criterion and pass to matchable
		ComparativeCriterion ageCriterion = new ComparativeCriterion();
		ageCriterion.setCharacteristicCode(AGE);
		// Matchable only supports year unit of measure
		ageCriterion.setUnitOfMeasure(YEAR);
		ageCriterion.setRequirement(40);
		AgeMatchable ageMatchable = new AgeMatchable(ageCriterion);
		Result result = ageMatchable.matchDescriptive(history);
		assert (result != null) && result.equals( Result.AVATAR_SUBJECTIVE ) : "Expected " + Result.AVATAR_SUBJECTIVE
				+ " result, got " + result + " for avatar with no age defined.";
	}

}
