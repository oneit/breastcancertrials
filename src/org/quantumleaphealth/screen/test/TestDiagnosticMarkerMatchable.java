/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.patient.ValueHistory;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.screen.DiagnosisMarkerMatchable;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANATOMIC_SITE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.METASTATIC;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.METASTATIC_BREAST_CANCER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREAST_CANCER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.HER2NEUDIAGNOSIS;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.HER2NEUDIAGNOSIS_POSITIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.HER2NEUDIAGNOSIS_NEGATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.HER2NEUDIAGNOSIS_KNOWN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.HER2NEUDIAGNOSIS_INDETERMINATE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.HER2NEUDIAGNOSIS_NOTTESTED;

/**
 * Unit tests for the <code>DiagnosticMarkerMatchable</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-02
 * @see DiagnosisMarkerMatchable
 */
@Test(description = "Diagnostic markers", groups = { "unit" })
public class TestDiagnosticMarkerMatchable
{

	/**
	 * Tests a positive HER2 marker in a metatstatic diagnosis
	 */
	public void matchHer2MetastaticPositive()
	{
		// Populate history with positive diagnosis in metastatic location
		Diagnosis diagnosis = new Diagnosis();
		ValueHistory valueHistory = diagnosis.getValueHistory();
		valueHistory.put(ANATOMIC_SITE, METASTATIC_BREAST_CANCER);
		valueHistory.put(HER2NEUDIAGNOSIS, HER2NEUDIAGNOSIS_POSITIVE);
		PatientHistory history = new PatientHistory();
		history.getDiagnoses().add(diagnosis);
		history.getValueHistory().setValue(BREAST_CANCER, METASTATIC);

		// Create criterion and pass to matchable
		AssociativeCriterion associativeCriterion = new AssociativeCriterion();
		associativeCriterion.setParent(HER2NEUDIAGNOSIS);
		associativeCriterion.setRequirement(new CharacteristicCode[] { HER2NEUDIAGNOSIS_POSITIVE });
		DiagnosisMarkerMatchable diagnosisMarkerMatchable = new DiagnosisMarkerMatchable(associativeCriterion,
				HER2NEUDIAGNOSIS_POSITIVE, HER2NEUDIAGNOSIS_NEGATIVE, HER2NEUDIAGNOSIS_KNOWN,
				HER2NEUDIAGNOSIS_INDETERMINATE, HER2NEUDIAGNOSIS_NOTTESTED);

		Result result = diagnosisMarkerMatchable.matchDescriptive(history);
		assert (result == Result.PASS) : "Expected pass result, got " + result;
		
		// repeat the test for an avatar
		history.setUserType( UserPatient.UserType.AVATAR_PROFILE );
		result = diagnosisMarkerMatchable.matchDescriptive(history);
		assert (result == Result.PASS) : "Expected pass result, got " + result;
		
		// repeat the avatar test with an empty history with nothing defined.
		history = new PatientHistory();
		history.setUserType( UserPatient.UserType.AVATAR_PROFILE );
		result = diagnosisMarkerMatchable.matchDescriptive(history);
		assert (result == Result.AVATAR_SUBJECTIVE) : "Expected pass result, got " + result;
	}
}
