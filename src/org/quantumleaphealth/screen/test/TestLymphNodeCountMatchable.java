/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.ComparativeCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANATOMIC_SITE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LYMPH_NODE_COUNT;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LEFT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RIGHT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AFFIRMATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.NEGATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;
import static org.quantumleaphealth.screen.Matchable.Result.FAIL;
import static org.quantumleaphealth.screen.Matchable.Result.PASS;
import static org.quantumleaphealth.screen.Matchable.Result.PASS_SUBJECTIVE;
import org.quantumleaphealth.screen.LymphNodeCountMatchable;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;

/**
 * Tests <code>LymphNodeCountMatchable</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 * @see LymphNodeCountMatchable
 */
@Test(description = "Lymph node count matchable", groups = { "screen", "composite" })
public class TestLymphNodeCountMatchable
{

	/**
	 * Test invalid arguments in constructors
	 */
	public void testConstructors()
	{
		// Test null criterion and parent
		try
		{
			new LymphNodeCountMatchable(null);
			throw new AssertionError("Null criterion accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		try
		{
			new LymphNodeCountMatchable(new ComparativeCriterion());
			throw new AssertionError("Null code accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		try
		{
			ComparativeCriterion comparativeCriterion = new ComparativeCriterion();
			comparativeCriterion.setCharacteristicCode(AFFIRMATIVE);
			new LymphNodeCountMatchable(comparativeCriterion);
			throw new AssertionError("Wrong code accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
	}

	/**
	 * Test matchDescriptive with unsure answer
	 */
	public void testMatchDescriptiveSubjective()
	{
		// Test each combination of left/right not inverted
		testMatchDescriptive(AFFIRMATIVE, 2, UNSURE, 0, 1, false, PASS);
		testMatchDescriptive(NEGATIVE, 0, UNSURE, 0, 1, false, PASS_SUBJECTIVE);
		testMatchDescriptive(null, 0, UNSURE, 0, 1, false, PASS_SUBJECTIVE);
		testMatchDescriptive(UNSURE, 0, AFFIRMATIVE, 2, 1, false, PASS);
		testMatchDescriptive(UNSURE, 0, NEGATIVE, 0, 1, false, PASS_SUBJECTIVE);
		testMatchDescriptive(UNSURE, 0, null, 0, 1, false, PASS_SUBJECTIVE);
		testMatchDescriptive(UNSURE, 0, UNSURE, 0, 1, false, PASS_SUBJECTIVE);

		// Test each combination of left/right maximum
		testMatchDescriptive(AFFIRMATIVE, 2, UNSURE, 0, 1, true, FAIL);
		testMatchDescriptive(NEGATIVE, 0, UNSURE, 0, 1, true, PASS_SUBJECTIVE);
		testMatchDescriptive(null, 0, UNSURE, 0, 1, true, PASS_SUBJECTIVE);
		testMatchDescriptive(UNSURE, 0, AFFIRMATIVE, 2, 1, true, FAIL);
		testMatchDescriptive(UNSURE, 0, NEGATIVE, 0, 1, true, PASS_SUBJECTIVE);
		testMatchDescriptive(UNSURE, 0, null, 0, 1, true, PASS_SUBJECTIVE);
		testMatchDescriptive(UNSURE, 0, UNSURE, 0, 1, true, PASS_SUBJECTIVE);
	}

	/**
	 * Test matchDescriptive with affirmative/negative answers
	 */
	public void testMatchDescriptiveObjective()
	{
		// No diagnoses
		testMatchDescriptive(null, 0, null, 0, 1, false, FAIL);
		testMatchDescriptive(null, 0, null, 0, 2, true, PASS);

		// Use the diagnosed side
		testMatchDescriptive(AFFIRMATIVE, 2, null, 0, 1, false, PASS);
		testMatchDescriptive(NEGATIVE, 0, null, 0, 1, false, FAIL);
		testMatchDescriptive(null, 0, AFFIRMATIVE, 2, 1, false, PASS);
		testMatchDescriptive(null, 0, NEGATIVE, 0, 1, false, FAIL);
		// Use the diagnosed side inverted
		testMatchDescriptive(AFFIRMATIVE, 2, null, 0, 1, true, FAIL);
		testMatchDescriptive(NEGATIVE, 0, null, 0, 1, true, PASS);
		testMatchDescriptive(null, 0, AFFIRMATIVE, 2, 1, true, FAIL);
		testMatchDescriptive(null, 0, NEGATIVE, 0, 1, true, PASS);

		// Equal diagnoses pass through
		testMatchDescriptive(AFFIRMATIVE, 2, AFFIRMATIVE, 2, 1, false, PASS);
		testMatchDescriptive(NEGATIVE, 0, NEGATIVE, 0, 1, false, FAIL);
		testMatchDescriptive(AFFIRMATIVE, 2, AFFIRMATIVE, 2, 1, true, FAIL);
		testMatchDescriptive(NEGATIVE, 0, NEGATIVE, 0, 1, true, PASS);

		// Unequal diagnoses override each other
		testMatchDescriptive(AFFIRMATIVE, 2, NEGATIVE, 0, 1, false, PASS);
		testMatchDescriptive(NEGATIVE, 0, AFFIRMATIVE, 2, 1, false, PASS);
		testMatchDescriptive(AFFIRMATIVE, 2, NEGATIVE, 0, 1, true, FAIL);
		testMatchDescriptive(NEGATIVE, 0, AFFIRMATIVE, 2, 1, true, FAIL);
	}

	/**
	 * Test matchDescriptive with affirmative answers but the count is blank (0)
	 */
	public void testMatchDescriptiveAffirmativeButCountBlank()
	{
		// Diagnoses with right breast positive but count left blank
		testMatchDescriptive(NEGATIVE, 0, AFFIRMATIVE, 0, 1, false, PASS);
		testMatchDescriptive(NEGATIVE, 0, AFFIRMATIVE, 0, 2, false, PASS_SUBJECTIVE);
		testMatchDescriptive(NEGATIVE, 0, AFFIRMATIVE, 0, 0, true, FAIL);
	}

	/**
	 * Tests matchDescriptive
	 * 
	 * @param left
	 *            the value of the lymph node characteristic for the left breast
	 *            or <code>null</code> if left breast not diagnosed
	 * @param leftCount
	 *            the count to store for the left lymph node characteristic
	 * @param right
	 *            the value of the lymph node characteristic for the right
	 *            breast or <code>null</code> if right breast not diagnosed
	 * @param rightCount
	 *            the count to store for the right lymph node characteristic
	 * @param limit
	 *            the criterion limit
	 * @param maximum
	 *            whether the criterion is inverted
	 * @param expectedResult
	 *            the expected result
	 */
	private void testMatchDescriptive(CharacteristicCode left, int leftCount, CharacteristicCode right, int rightCount,
			int limit, boolean maximum, Result expectedResult)
	{
		// Construct history with diagnoses
		PatientHistory patientHistory = new PatientHistory();
		if (left != null)
			addDiagnosis(patientHistory, LEFT_BREAST, left, leftCount);
		if (right != null)
			addDiagnosis(patientHistory, RIGHT_BREAST, right, rightCount);
		// Construct criterion
		ComparativeCriterion comparativeCriterion = new ComparativeCriterion();
		comparativeCriterion.setCharacteristicCode(LYMPH_NODE_COUNT);
		comparativeCriterion.setRequirement(limit);
		comparativeCriterion.setInvert(maximum);

		// Create and test matchable
		LymphNodeCountMatchable lymphNodeCountMatchable = new LymphNodeCountMatchable(comparativeCriterion);
		Result result = lymphNodeCountMatchable.matchDescriptive(patientHistory);
		assert result == expectedResult : "Left " + left + '(' + leftCount + ") and " + right + '(' + rightCount
				+ ") limit " + limit + " invert " + maximum + " resulted in " + result + " instead of "
				+ expectedResult;
	}

	/**
	 * Add a lymph node count diagnosis to a history
	 * 
	 * @param patientHistory
	 *            the patient history
	 * @param location
	 *            the anatomic site
	 * @param value
	 *            the value to store for the lymph node characteristic
	 * @param count
	 *            the count to store for the lymph node characteristic
	 */
	private void addDiagnosis(PatientHistory patientHistory, CharacteristicCode location, CharacteristicCode value,
			int count)
	{
		if ((patientHistory == null) || (location == null) || (value == null))
			return;
		Diagnosis diagnosis = new Diagnosis();
		diagnosis.getValueHistory().setValue(ANATOMIC_SITE, location);
		diagnosis.getValueHistory().setValue(LYMPH_NODE_COUNT, value);
		if (count > 0)
			diagnosis.getShortHistory().setShort(LYMPH_NODE_COUNT, Short.valueOf((short) count));
		patientHistory.getDiagnoses().add(diagnosis);
	}
}
