/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.model.trial.AssociativeCriterion.SetOperation;
import static org.quantumleaphealth.model.trial.AssociativeCriterion.SetOperation.*;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.screen.AttributeAssociativeMatchable;
import org.quantumleaphealth.screen.Matchable.CharacteristicResult;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;

/**
 * Tests for <code>AttributeAssociativeMatchable</code>. This composite test
 * relies upon the following classes:
 * <ul>
 * <li><code>PatientHistory</code></li>
 * <li><code>AssociativeCriterion</code></li>
 * <li><code>CharacteristicCode</code></li>
 * <li><code>Matchable</code></li>
 * </ul>
 * 
 * @author Tom Bechtold
 * @version 2008-07-07
 * @see AttributeAssociativeMatchable
 */
@Test(description = "Multiple-choice associative matchable", groups = { "screen", "composite" })
public class TestAttributeAssociativeMatchable extends TestUtils
{

	/**
	 * Test invalid arguments in constructors
	 */
	public void testConstructors()
	{
		// Test null criterion and parent
		try
		{
			new AttributeAssociativeMatchable(null);
			throw new AssertionError("Null criterion accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		try
		{
			new AttributeAssociativeMatchable(new AssociativeCriterion());
			throw new AssertionError("Null parent accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
	}

	/**
	 * Test matchDescriptive
	 */
	public void testMatchDescriptive()
	{
		// Create aggregator to map 0 -> 1+2
		int[] unique = getUniqueIntegers(4);

		Map<CharacteristicCode, CharacteristicCode[]> aggregators = new HashMap<CharacteristicCode, CharacteristicCode[]>(
				1, 1.0f);
		aggregators.put(new CharacteristicCode(unique[0]), new CharacteristicCode[] {
				new CharacteristicCode(unique[1]), new CharacteristicCode(unique[2]) });
		// Test no aggregators, matching
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, INTERSECT, new CharacteristicCode(
				unique[0]), null, Result.PASS);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, SUBSET, new CharacteristicCode(
				unique[0]), null, Result.PASS);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, SUPERSET, new CharacteristicCode(
				unique[0]), null, Result.PASS);
		// Test no aggregators, nonmatching
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, INTERSECT, new CharacteristicCode(
				unique[1]), null, Result.FAIL);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, SUBSET, new CharacteristicCode(
				unique[1]), null, Result.FAIL);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, SUPERSET, new CharacteristicCode(
				unique[1]), null, Result.FAIL);
		// Test aggregators, matching
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, INTERSECT, new CharacteristicCode(
				unique[1]), aggregators, Result.PASS);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, SUBSET, new CharacteristicCode(
				unique[1]), aggregators, Result.PASS);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, SUPERSET, new CharacteristicCode(
				unique[1]), aggregators, Result.FAIL);
		// Test aggregators, nonmatching
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, INTERSECT, new CharacteristicCode(
				unique[3]), aggregators, Result.FAIL);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, SUBSET, new CharacteristicCode(
				unique[3]), aggregators, Result.FAIL);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, SUPERSET, new CharacteristicCode(
				unique[3]), aggregators, Result.FAIL);
	}

	/**
	 * Test matchDescriptive inverted
	 */
	public void testMatchDescriptiveInvert()
	{
		// Create aggregator to map 0 -> 1+2
		int[] unique = getUniqueIntegers(4);

		Map<CharacteristicCode, CharacteristicCode[]> aggregators = new HashMap<CharacteristicCode, CharacteristicCode[]>(
				1, 1.0f);
		aggregators.put(new CharacteristicCode(unique[0]), new CharacteristicCode[] {
				new CharacteristicCode(unique[1]), new CharacteristicCode(unique[2]) });
		// Test no aggregators, matching inverted
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, INTERSECT, new CharacteristicCode(
				unique[0]), null, Result.FAIL);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, SUBSET, new CharacteristicCode(
				unique[0]), null, Result.FAIL);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, SUPERSET, new CharacteristicCode(
				unique[0]), null, Result.FAIL);
		// Test no aggregators, nonmatching inverted
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, INTERSECT, new CharacteristicCode(
				unique[1]), null, Result.PASS);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, SUBSET, new CharacteristicCode(
				unique[1]), null, Result.PASS);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, SUPERSET, new CharacteristicCode(
				unique[1]), null, Result.PASS);
		// Test aggregators, matching inverted
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, INTERSECT, new CharacteristicCode(
				unique[1]), aggregators, Result.FAIL);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, SUBSET, new CharacteristicCode(
				unique[1]), aggregators, Result.FAIL);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, SUPERSET, new CharacteristicCode(
				unique[1]), aggregators, Result.PASS);
		// Test aggregators, nonmatching inverted
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, INTERSECT, new CharacteristicCode(
				unique[3]), aggregators, Result.PASS);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, SUBSET, new CharacteristicCode(
				unique[3]), aggregators, Result.PASS);
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, SUPERSET, new CharacteristicCode(
				unique[3]), aggregators, Result.PASS);
	}

	/**
	 * Test intersect operation
	 */
	public void testIntersect()
	{
		int[] unique = getUniqueIntegers(3);
		// Null requirement means "any"
		// Test any value is found
		assert AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[0]) },
				null, INTERSECT, false) : "Any matching value not found";
		assert AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[0]) },
				null, INTERSECT, true) : "Any matching value with other not found";
		// Test any other value is found or not found
		assert AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] {}, null, INTERSECT, true) : "Any other value not found";
		assert !AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] {}, null, INTERSECT, false) : "No other value is found";

		// Requirement has first code
		LinkedHashSet<CharacteristicCode> requirement = new LinkedHashSet<CharacteristicCode>(1, 1f);
		requirement.add(new CharacteristicCode(unique[0]));

		// Test single value found
		assert AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[0]) },
				requirement, INTERSECT, false) : "Single matching value not found";
		// Test multiple value found
		assert AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[0]),
				new CharacteristicCode(unique[1]) }, requirement, INTERSECT, false) : "Multiple matching value not found";
		// Test single value not found
		assert !AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[1]) },
				requirement, INTERSECT, false) : "Single non-matching value found";
		// Test multiple value not found
		assert !AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[1]),
				new CharacteristicCode(unique[2]) }, requirement, INTERSECT, false) : "Multiple non-matching value found";
	}

	/**
	 * Test subset operation
	 */
	public void testSubset()
	{
		// Build single (0) and multiple (0+1) requirements
		int[] unique = getUniqueIntegers(3);
		LinkedHashSet<CharacteristicCode> singleRequirement = new LinkedHashSet<CharacteristicCode>(1, 1f);
		singleRequirement.add(new CharacteristicCode(unique[0]));
		LinkedHashSet<CharacteristicCode> multipleRequirement = new LinkedHashSet<CharacteristicCode>(singleRequirement);
		multipleRequirement.add(new CharacteristicCode(unique[1]));

		// Passing results
		// Single value is found in multiple requirement
		assert AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[0]) },
				multipleRequirement, SUBSET, false) : "Single matching value not found in multiple";
		// Multiple value is found in multiple requirement
		assert AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[0]),
				new CharacteristicCode(unique[1]) }, multipleRequirement, SUBSET, false) : "Multiple matching value not found in multiple";
		// Single value is found in single requirement
		assert AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[0]) },
				singleRequirement, SUBSET, false) : "Single matching value not found in single";

		// Failing results
		// Single value is not found in multiple requirement
		assert !AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[2]) },
				multipleRequirement, SUBSET, false) : "Single non-matching value found in multiple";
		// Multiple value is not found in multiple requirement
		assert !AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[0]),
				new CharacteristicCode(unique[2]) }, multipleRequirement, SUBSET, false) : "Multiple non-matching value found in multiple";
		// Single value is not found in single requirement
		assert !AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[1]) },
				singleRequirement, SUBSET, false) : "Single non-matching value found in single";
		// Multiple matching value is not found in single requirement
		assert !AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[0]),
				new CharacteristicCode(unique[1]) }, singleRequirement, SUBSET, false) : "Multiple matching value found in single";
	}

	/**
	 * Test superset operation
	 */
	public void testSuperset()
	{
		// Build single (0) and multiple (0+1) requirements
		int[] unique = getUniqueIntegers(3);
		LinkedHashSet<CharacteristicCode> singleRequirement = new LinkedHashSet<CharacteristicCode>(1, 1f);
		singleRequirement.add(new CharacteristicCode(unique[0]));
		LinkedHashSet<CharacteristicCode> multipleRequirement = new LinkedHashSet<CharacteristicCode>(singleRequirement);
		multipleRequirement.add(new CharacteristicCode(unique[1]));

		// Passing results
		// Multiple value is found in multiple requirement
		assert AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[0]),
				new CharacteristicCode(unique[1]) }, multipleRequirement, SUPERSET, false) : "Multiple matching value not found in multiple";
		// Single value is found in single requirement
		assert AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[0]) },
				singleRequirement, SUPERSET, false) : "Single matching value not found in single";

		// Failing results
		// Single matching value is not found in multiple requirement
		assert !AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[0]) },
				multipleRequirement, SUPERSET, false) : "Single matching value found in multiple";
		// Multiple non-matching value is not found in multiple requirement
		assert !AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[0]),
				new CharacteristicCode(unique[2]) }, multipleRequirement, SUPERSET, false) : "Multiple non-matching value found in multiple";
		// Single non-matching value is not found in single requirement
		assert !AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[1]) },
				singleRequirement, SUPERSET, false) : "Single non-matching value found in single";
		// Multiple non-matching matching value is not found in single
		// requirement
		assert !AttributeAssociativeMatchable.isMatch(new CharacteristicCode[] { new CharacteristicCode(unique[1]),
				new CharacteristicCode(unique[2]) }, singleRequirement, SUPERSET, false) : "Multiple non-matching value found in single";
	}

	/**
	 * Test the characteristic's result is same as definitive result
	 */
	public void testCharacteristicResult()
	{
		int[] unique = getUniqueIntegers(3);
		// Test matching
		Result expectedResult = Result.PASS;
		List<CharacteristicResult> characteristicResults = testMatchDescriptive(GENERATOR.nextInt(),
				new int[] { unique[0] }, false, INTERSECT, new CharacteristicCode(unique[0]), null, expectedResult);
		assert (characteristicResults != null) && (characteristicResults.size() == 1)
				&& (expectedResult == characteristicResults.get(0).getResult()) : "Characteristic result "
				+ characteristicResults + " is not " + expectedResult;
		// Test nonmatching
		expectedResult = Result.FAIL;
		characteristicResults = testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, INTERSECT,
				new CharacteristicCode(unique[1]), null, expectedResult);
		assert (characteristicResults != null) && (characteristicResults.size() == 1)
				&& (expectedResult == characteristicResults.get(0).getResult()) : "Characteristic result "
				+ characteristicResults + " is not " + expectedResult;
	}

	/**
	 * Tests matchDescriptive by passing a criterion and a patient history
	 * 
	 * @param parent
	 *            the criterion's parent
	 * @param requirement
	 *            the criterion's requirement
	 * @param invert
	 *            the criterion's invert
	 * @param setOperation
	 *            the criterion's set operation
	 * @param value
	 *            the patient history's value stored in its value history under
	 *            parent
	 * @param aggregators
	 *            optional aggregators to expand the requirement
	 * @param expectedResult
	 *            the expected result
	 * @return the characteristic result from the matchable
	 * @throws AssertionError
	 *             if the result of <code>matchDescriptive</code> is not
	 *             <code>expectedResult</code>
	 */
	private List<CharacteristicResult> testMatchDescriptive(int parent, int[] requirement, boolean invert,
			SetOperation setOperation, CharacteristicCode value,
			Map<CharacteristicCode, CharacteristicCode[]> aggregators, Result expectedResult) throws AssertionError
	{
		// Create criterion
		AssociativeCriterion associativeCriterion = new AssociativeCriterion();
		associativeCriterion.setParent(new CharacteristicCode(parent));
		if ((requirement != null) && (requirement.length > 0))
		{
			CharacteristicCode[] requirementArray = new CharacteristicCode[requirement.length];
			for (int index = 0; index < requirementArray.length; index++)
				requirementArray[index] = new CharacteristicCode(requirement[index]);
			associativeCriterion.setRequirement(requirementArray);
		}
		associativeCriterion.setSetOperation(setOperation);
		associativeCriterion.setInvert(invert);

		// Populate patient history with optional value
		PatientHistory patientHistory = new PatientHistory();
		if (value != null)
			patientHistory.addAttribute(new CharacteristicCode(parent), value);

		// Test matchable.matchDescriptive
		AttributeAssociativeMatchable valueAssociativeMatchable = new AttributeAssociativeMatchable(
				associativeCriterion, aggregators);
		Result result = valueAssociativeMatchable.matchDescriptive(patientHistory);
		assert result == expectedResult : "Matching " + value + " to " + associativeCriterion + " resulted in "
				+ result + " instead of " + expectedResult;
		return valueAssociativeMatchable.getCharacteristicResults(patientHistory, null);
	}
}
