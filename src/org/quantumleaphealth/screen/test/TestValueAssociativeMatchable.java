/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;
import org.quantumleaphealth.screen.ValueAssociativeMatchable;
import org.quantumleaphealth.screen.Matchable.CharacteristicResult;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;

/**
 * Tests <code>ValueAssociativeMatchable</code> This composite test class
 * references the following classes:
 * <ul>
 * <li><code>CharacteristicCode</code></li>
 * <li><code>PatientHistory</code></li>
 * <li><code>AssociativeCriterion</code></li>
 * </ul>
 * 
 * @author Tom Bechtold
 * @version 2008-07-07
 * @see ValueAssociativeMatchable
 */
@Test(description = "Single-choice associative matchable", groups = { "screen", "composite" })
public class TestValueAssociativeMatchable extends TestUtils
{
	/**
	 * Test invalid arguments in constructors
	 */
	public void testConstructors()
	{
		// Test null criterion and parent
		try
		{
			new ValueAssociativeMatchable(null);
			throw new AssertionError("Null criterion accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		try
		{
			new ValueAssociativeMatchable(new AssociativeCriterion());
			throw new AssertionError("Null parent accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
	}

	public void testAvatarSubjective()
	{
		testSubjective(true);
	}

	public void testNormalSubjective()
	{
		testSubjective(false);
	}

	/**
	 * Test matchDescriptive with subjective
	 */
	private void testSubjective(boolean isAvatar)
	{
		// Create aggregator to map 0 -> 1+2
		int[] unique = getUniqueIntegers(3);
		Map<CharacteristicCode, CharacteristicCode[]> aggregators = new HashMap<CharacteristicCode, CharacteristicCode[]>(
				1, 1.0f);
		aggregators.put(new CharacteristicCode(unique[0]), new CharacteristicCode[] {
				new CharacteristicCode(unique[1]), new CharacteristicCode(unique[2]) });
		// Test no aggregators, subjective matching
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, UNSURE, null, Result.PASS_SUBJECTIVE, isAvatar);
		// Test aggregators, subjective matching
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, UNSURE, aggregators,
				Result.PASS_SUBJECTIVE, isAvatar);
	}

	public void testAvatarSubjectiveInvert()
	{
		testSubjectiveInvert(true);
	}

	public void testNormalSubjectiveInvert()
	{
		testSubjectiveInvert(false);
	}
	
	/**
	 * Test matchDescriptive with subjective inverted
	 */
	private void testSubjectiveInvert(boolean isAvatar)
	{
		// Create aggregator to map 0 -> 1+2
		int[] unique = getUniqueIntegers(3);
		Map<CharacteristicCode, CharacteristicCode[]> aggregators = new HashMap<CharacteristicCode, CharacteristicCode[]>(
				1, 1.0f);
		aggregators.put(new CharacteristicCode(unique[0]), new CharacteristicCode[] {
				new CharacteristicCode(unique[1]), new CharacteristicCode(unique[2]) });
		// Test no aggregators, subjective matching
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, UNSURE, null, Result.FAIL_SUBJECTIVE, isAvatar);
		// Test aggregators, subjective matching
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, UNSURE, aggregators, Result.FAIL_SUBJECTIVE, isAvatar);
	}

	public void testAvatarObjective()
	{
		testObjective(true);
	}

	public void testNormalObjective()
	{
		testObjective(false);
	}

	/**
	 * Test matchDescriptive with objective
	 */
	private void testObjective(boolean isAvatar)
	{
		// Create aggregator to map 0 -> 1+2
		int[] unique = getUniqueIntegers(4);
		Map<CharacteristicCode, CharacteristicCode[]> aggregators = new HashMap<CharacteristicCode, CharacteristicCode[]>(
				1, 1.0f);
		aggregators.put(new CharacteristicCode(unique[0]), new CharacteristicCode[] {
				new CharacteristicCode(unique[1]), new CharacteristicCode(unique[2]) });
		// Test no aggregators, matching
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, new CharacteristicCode(unique[0]),
				null, Result.PASS, isAvatar);
		// Test no aggregators, nonmatching
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, new CharacteristicCode(unique[1]),
				null, Result.FAIL, isAvatar);
		// Test aggregators, matching
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, new CharacteristicCode(unique[1]),
				aggregators, Result.PASS, isAvatar);
		// Test aggregators, nonmatching
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, new CharacteristicCode(unique[3]),
				aggregators, Result.FAIL, isAvatar);
	}

	public void testAvatarObjectiveInvert()
	{
		testObjectiveInvert(true);
	}

	public void testNormalObjectiveInvert()
	{
		testObjectiveInvert(false);
	}

	/**
	 * Test matchDescriptive with objective inverted
	 */
	private void testObjectiveInvert(boolean isAvatar)
	{
		// Create aggregator to map 0 -> 1+2
		int[] unique = getUniqueIntegers(4);
		Map<CharacteristicCode, CharacteristicCode[]> aggregators = new HashMap<CharacteristicCode, CharacteristicCode[]>(
				1, 1.0f);
		aggregators.put(new CharacteristicCode(unique[0]), new CharacteristicCode[] {
				new CharacteristicCode(unique[1]), new CharacteristicCode(unique[2]) });
		// Test no aggregators, matching inverted
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, new CharacteristicCode(unique[0]),
				null, Result.FAIL, isAvatar);
		// Test no aggregators, nonmatching inverted
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, new CharacteristicCode(unique[1]),
				null, Result.PASS, isAvatar);
		// Test aggregators, matching inverted
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, new CharacteristicCode(unique[1]),
				aggregators, Result.FAIL, isAvatar);
		// Test aggregators, nonmatching inverted
		testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, true, new CharacteristicCode(unique[3]),
				aggregators, Result.PASS, isAvatar);
	}

	public void testAvatarCharacteristicResult()
	{
		testCharacteristicResult(true);
	}
	
	public void testNormalCharacteristicResult()
	{
		testCharacteristicResult(false);
	}
	
	/**
	 * Test the characteristic's result is same as definitive result
	 */
	private void testCharacteristicResult(boolean isAvatar)
	{
		int[] unique = getUniqueIntegers(2);
		// Test matching
		Result expectedResult = Result.PASS;
		List<CharacteristicResult> characteristicResults = testMatchDescriptive(GENERATOR.nextInt(),
				new int[] { unique[0] }, false, new CharacteristicCode(unique[0]), null, expectedResult, isAvatar);
		assert (characteristicResults != null) && (characteristicResults.size() == 1)
				&& (expectedResult == characteristicResults.get(0).getResult()) : "Characteristic result "
				+ characteristicResults + " is not " + expectedResult;
		// Test nonmatching
		expectedResult = Result.FAIL;
		characteristicResults = testMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false,
				new CharacteristicCode(unique[1]), null, expectedResult, isAvatar);
		assert (characteristicResults != null) && (characteristicResults.size() == 1)
				&& (expectedResult == characteristicResults.get(0).getResult()) : "Characteristic result "
				+ characteristicResults + " is not " + expectedResult;
	}

	/**
	 * Tests matchDescriptive by passing a criterion and a patient history
	 * 
	 * @param parent
	 *            the criterion's parent
	 * @param requirement
	 *            the criterion's requirement
	 * @param invert
	 *            the criterion's invert
	 * @param value
	 *            the patient history's value stored in its value history under
	 *            parent
	 * @param aggregators
	 *            optional aggregators to expand the requirement
	 * @param expectedResult
	 *            the expected result
	 * @return the characteristic result from the matchable
	 * @throws AssertionError
	 *             if the result of <code>matchDescriptive</code> is not
	 *             <code>expectedResult</code>
	 */
	private List<CharacteristicResult> testMatchDescriptive(int parent, int[] requirement, boolean invert,
			CharacteristicCode value, Map<CharacteristicCode, CharacteristicCode[]> aggregators, Result expectedResult, boolean isAvatar)
			throws AssertionError
	{
		// Create criterion
		AssociativeCriterion associativeCriterion = new AssociativeCriterion();
		associativeCriterion.setParent(new CharacteristicCode(parent));
		if ((requirement != null) && (requirement.length > 0))
		{
			CharacteristicCode[] requirementArray = new CharacteristicCode[requirement.length];
			for (int index = 0; index < requirementArray.length; index++)
				requirementArray[index] = new CharacteristicCode(requirement[index]);
			associativeCriterion.setRequirement(requirementArray);
		}
		associativeCriterion.setInvert(invert);

		// Populate patient history with optional value
		PatientHistory patientHistory = new PatientHistory();
		if ( isAvatar )
		{
			patientHistory.setUserType( UserPatient.UserType.AVATAR_PROFILE );
		}
		
		if (value != null)
			patientHistory.getValueHistory().setValue(new CharacteristicCode(parent), value);

		// Test matchable.matchDescriptive
		ValueAssociativeMatchable valueAssociativeMatchable = new ValueAssociativeMatchable(associativeCriterion,
				aggregators);
		Result result = valueAssociativeMatchable.matchDescriptive(patientHistory);
		assert result == expectedResult : "Matching " + value + " to " + associativeCriterion + " resulted in "
				+ result + " instead of " + expectedResult;
		return valueAssociativeMatchable.getCharacteristicResults(patientHistory, null);
	}
	
	public void testAvatarNoMatch()
	{
		int[] unique = getUniqueIntegers(2);
		testNoMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, null, Result.AVATAR_SUBJECTIVE, true);
	}

	public void testNormalNoMatch()
	{
		int[] unique = getUniqueIntegers(2);
		testNoMatchDescriptive(GENERATOR.nextInt(), new int[] { unique[0] }, false, null, Result.FAIL, false);
	}

	private List<CharacteristicResult> testNoMatchDescriptive(int parent, int[] requirement, boolean invert,
			Map<CharacteristicCode, CharacteristicCode[]> aggregators, Result expectedResult, boolean isAvatar)
			throws AssertionError
	{
		// Create criterion
		AssociativeCriterion associativeCriterion = new AssociativeCriterion();
		associativeCriterion.setParent(new CharacteristicCode(parent));
		if ((requirement != null) && (requirement.length > 0))
		{
			CharacteristicCode[] requirementArray = new CharacteristicCode[requirement.length];
			for (int index = 0; index < requirementArray.length; index++)
				requirementArray[index] = new CharacteristicCode(requirement[index]);
			associativeCriterion.setRequirement(requirementArray);
		}
		associativeCriterion.setInvert(invert);

		// Populate patient history with optional value
		PatientHistory patientHistory = new PatientHistory();
		if ( isAvatar )
		{
			patientHistory.setUserType( UserPatient.UserType.AVATAR_PROFILE );
		}
		
		// Test matchable.matchDescriptive
		ValueAssociativeMatchable valueAssociativeMatchable = new ValueAssociativeMatchable(associativeCriterion,
				aggregators);
		Result result = valueAssociativeMatchable.matchDescriptive(patientHistory);
		assert result == expectedResult : "Matching empty history to " + associativeCriterion + " resulted in "
				+ result + " instead of " + expectedResult;
		return valueAssociativeMatchable.getCharacteristicResults(patientHistory, null);
	}

}
