/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import org.quantumleaphealth.screen.UnsupportedCharacteristicMatchable;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;

/**
 * Unit tests for the <code>UnsupportedCharacteristicMatchable</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 * @see UnsupportedCharacteristicMatchable
 */
@Test(description = "Unsupported matchable", groups = { "screen", "unit" })
public class TestUnsupportedCharacteristicMatchable
{

	/**
	 * Test MatchDescriptive
	 */
	public void testMatchDescriptive()
	{
		Result result = new UnsupportedCharacteristicMatchable().matchDescriptive(null);
		assert Result.PASS_SUBJECTIVE.equals(result) : "Descriptive result not pass-subjective: " + result;
	}

	/**
	 * Test GetCharacteristicResults
	 */
	public void testGetCharacteristicResults()
	{
		assert null == new UnsupportedCharacteristicMatchable().getCharacteristicResults(null, null) : "Results not null";
	}
}
