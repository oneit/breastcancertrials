/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import java.util.List;
import java.util.ResourceBundle;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.Criteria.Operator;

import static org.quantumleaphealth.model.trial.Criteria.Operator.*;
import org.quantumleaphealth.screen.CriteriaMatchable;
import org.quantumleaphealth.screen.Matchable;
import org.quantumleaphealth.screen.UnsupportedCriterionException;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;

/**
 * Unit tests for <code>CriteriaMatchable</code>. While this class references
 * patient history and Matchable, they are used as <code>null</code> and
 * statically, thus this test is considered a "pure" unit test rather than
 * composite.
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 * @see CriteriaMatchable
 */
@Test(description = "Criteria matchable", groups = { "screen", "unit" })
public class TestCriteriaMatchable
{

	/**
	 * Test constructors with null arguments
	 */
	public void testConstructorNull()
	{
		// Test null parameters
		try
		{
			new CriteriaMatchable(null, null);
			throw new AssertionError("Accepted null parameters");
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
		}
		// Test null operator
		try
		{
			new CriteriaMatchable(null, new Matchable[] { new StaticMatchable(null) });
			throw new AssertionError("Accepted empty operator");
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
		}
		// Test null children
		try
		{
			new CriteriaMatchable(AND, null);
			throw new AssertionError("Accepted null children");
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
		}
		// Test empty child
		try
		{
			new CriteriaMatchable(AND, new StaticMatchable[] { null });
			throw new AssertionError("Accepted empty children");
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
		}
	}

	/**
	 * Test constructor arguments with if operator
	 */
	public void testConstructorIf()
	{
		// Test one parameter
		try
		{
			new CriteriaMatchable(IF, new Matchable[] { new StaticMatchable(Result.PASS_SUBJECTIVE) });
			throw new AssertionError("Accepted one child");
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
		}
		// Test three parameters
		try
		{
			new CriteriaMatchable(IF, new Matchable[] { new StaticMatchable(Result.PASS_SUBJECTIVE),
					new StaticMatchable(Result.PASS_SUBJECTIVE), new StaticMatchable(Result.PASS_SUBJECTIVE) });
			throw new AssertionError("Accepted three children");
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
		}
		// Test two parameters
		try
		{
			new CriteriaMatchable(IF, new Matchable[] { new StaticMatchable(Result.PASS_SUBJECTIVE),
					new StaticMatchable(Result.PASS_SUBJECTIVE) });
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
			throw new AssertionError(unsupportedCriterionException);
		}
	}

	/**
	 * Test matchDescriptive AND: worst of two children's results
	 */
	public void testMatchDescriptiveAnd()
	{
		// Child order does not matter
		// first child is PASS
		testMatchDescriptive(Result.PASS, AND, Result.PASS, Result.PASS, false);
		testMatchDescriptive(Result.PASS, AND, Result.PASS_SUBJECTIVE, Result.PASS_SUBJECTIVE, false);
		testMatchDescriptive(Result.PASS, AND, Result.FAIL_SUBJECTIVE, Result.FAIL_SUBJECTIVE, false);
		testMatchDescriptive(Result.PASS, AND, Result.FAIL, Result.FAIL, false);
		testMatchDescriptive(Result.PASS, AND, Result.AVATAR_SUBJECTIVE, Result.PASS, false);
		
		// first child is PASS_SUBJECTIVE
		testMatchDescriptive(Result.PASS_SUBJECTIVE, AND, Result.PASS_SUBJECTIVE, Result.PASS_SUBJECTIVE, false);
		testMatchDescriptive(Result.PASS_SUBJECTIVE, AND, Result.FAIL_SUBJECTIVE, Result.FAIL_SUBJECTIVE, false);
		testMatchDescriptive(Result.PASS_SUBJECTIVE, AND, Result.FAIL, Result.FAIL, false);
		testMatchDescriptive(Result.PASS_SUBJECTIVE, AND, Result.AVATAR_SUBJECTIVE, Result.PASS_SUBJECTIVE, false);

		// first child is FAIL_SUBJECTIVE
		testMatchDescriptive(Result.FAIL_SUBJECTIVE, AND, Result.FAIL_SUBJECTIVE, Result.FAIL_SUBJECTIVE, false);
		testMatchDescriptive(Result.FAIL_SUBJECTIVE, AND, Result.FAIL, Result.FAIL, false);
		testMatchDescriptive(Result.FAIL_SUBJECTIVE, AND, Result.AVATAR_SUBJECTIVE, Result.FAIL_SUBJECTIVE, false);

		// first child is FAIL
		testMatchDescriptive(Result.FAIL, AND, Result.FAIL, Result.FAIL, false);
		testMatchDescriptive(Result.FAIL, AND, Result.AVATAR_SUBJECTIVE, Result.FAIL, false);

		// first child is AVATAR.  for these tests, all patient histories are from an avatar
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, AND, Result.PASS, Result.PASS, true);
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, AND, Result.PASS_SUBJECTIVE, Result.PASS_SUBJECTIVE, true);
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, AND, Result.FAIL_SUBJECTIVE, Result.FAIL_SUBJECTIVE, true);
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, AND, Result.FAIL, Result.FAIL, true);
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, AND, Result.AVATAR_SUBJECTIVE, Result.AVATAR_SUBJECTIVE, true);
	}

	/**
	 * Test matchDescriptive OR: best of two children's results
	 */
	public void testMatchDescriptiveOr()
	{
		// Child order does not matter
		// first child is PASS
		testMatchDescriptive(Result.PASS, OR, Result.PASS, Result.PASS, false);
		testMatchDescriptive(Result.PASS, OR, Result.PASS_SUBJECTIVE, Result.PASS, false);
		testMatchDescriptive(Result.PASS, OR, Result.FAIL_SUBJECTIVE, Result.PASS, false);
		testMatchDescriptive(Result.PASS, OR, Result.FAIL, Result.PASS, false);
		testMatchDescriptive(Result.PASS, OR, Result.AVATAR_SUBJECTIVE, Result.PASS, false);
		
		// first child is PASS_SUBJECTIVE
		testMatchDescriptive(Result.PASS_SUBJECTIVE, OR, Result.PASS_SUBJECTIVE, Result.PASS_SUBJECTIVE, false);
		testMatchDescriptive(Result.PASS_SUBJECTIVE, OR, Result.FAIL_SUBJECTIVE, Result.PASS_SUBJECTIVE, false);
		testMatchDescriptive(Result.PASS_SUBJECTIVE, OR, Result.FAIL, Result.PASS_SUBJECTIVE, false);
		testMatchDescriptive(Result.PASS_SUBJECTIVE, OR, Result.AVATAR_SUBJECTIVE, Result.PASS_SUBJECTIVE, false);
		
		// first child is FAIL_SUBJECTIVE
		testMatchDescriptive(Result.FAIL_SUBJECTIVE, OR, Result.FAIL_SUBJECTIVE, Result.FAIL_SUBJECTIVE, false);
		testMatchDescriptive(Result.FAIL_SUBJECTIVE, OR, Result.FAIL, Result.FAIL_SUBJECTIVE, false);
		testMatchDescriptive(Result.FAIL_SUBJECTIVE, OR, Result.AVATAR_SUBJECTIVE, Result.FAIL_SUBJECTIVE, false);
		
		// first child is FAIL
		testMatchDescriptive(Result.FAIL, OR, Result.FAIL, Result.FAIL, false);
		testMatchDescriptive(Result.FAIL, OR, Result.AVATAR_SUBJECTIVE, Result.FAIL, false);
		
		// first child is AVATAR_SUBJECTIVE
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, OR, Result.PASS, Result.PASS, true);
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, OR, Result.PASS_SUBJECTIVE, Result.PASS_SUBJECTIVE, true);
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, OR, Result.FAIL_SUBJECTIVE, Result.FAIL_SUBJECTIVE, true);
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, OR, Result.FAIL, Result.FAIL, true);
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, OR, Result.AVATAR_SUBJECTIVE, Result.AVATAR_SUBJECTIVE, true);
	}

	/**
	 * Test matchDescriptive IF/THEN
	 */
	public void testMatchDescriptiveIfThen()
	{
		// Child order does matter; test all permutations
		// IF child is PASS use THEN child
		testMatchDescriptive(Result.PASS, IF, Result.PASS, Result.PASS, false);
		testMatchDescriptive(Result.PASS, IF, Result.PASS_SUBJECTIVE, Result.PASS_SUBJECTIVE, false);
		testMatchDescriptive(Result.PASS, IF, Result.FAIL_SUBJECTIVE, Result.FAIL_SUBJECTIVE, false);
		testMatchDescriptive(Result.PASS, IF, Result.FAIL, Result.FAIL, false);
		testMatchDescriptive(Result.PASS, IF, Result.AVATAR_SUBJECTIVE, Result.AVATAR_SUBJECTIVE, false);
		
		// IF child is PASS_SUBJECTIVE use better of two
		testMatchDescriptive(Result.PASS_SUBJECTIVE, IF, Result.PASS, Result.PASS_SUBJECTIVE, false);
		testMatchDescriptive(Result.PASS_SUBJECTIVE, IF, Result.PASS_SUBJECTIVE, Result.PASS_SUBJECTIVE, false);
		testMatchDescriptive(Result.PASS_SUBJECTIVE, IF, Result.FAIL_SUBJECTIVE, Result.PASS_SUBJECTIVE, false);
		testMatchDescriptive(Result.PASS_SUBJECTIVE, IF, Result.FAIL, Result.PASS_SUBJECTIVE, false);
		testMatchDescriptive(Result.PASS_SUBJECTIVE, IF, Result.AVATAR_SUBJECTIVE, Result.PASS_SUBJECTIVE, true);
		
		// IF child is FAIL_SUBJECTIVE
		testMatchDescriptive(Result.FAIL_SUBJECTIVE, IF, Result.PASS, Result.FAIL_SUBJECTIVE, false);
		testMatchDescriptive(Result.FAIL_SUBJECTIVE, IF, Result.PASS_SUBJECTIVE, Result.FAIL_SUBJECTIVE, false);
		testMatchDescriptive(Result.FAIL_SUBJECTIVE, IF, Result.FAIL_SUBJECTIVE, Result.FAIL_SUBJECTIVE, false);
		testMatchDescriptive(Result.FAIL_SUBJECTIVE, IF, Result.FAIL, Result.FAIL_SUBJECTIVE, false);
		testMatchDescriptive(Result.FAIL_SUBJECTIVE, IF, Result.AVATAR_SUBJECTIVE, Result.FAIL_SUBJECTIVE, true);
		
		// IF child is FAIL; automatic pass
		testMatchDescriptive(Result.FAIL, IF, Result.PASS, Result.PASS, false);
		testMatchDescriptive(Result.FAIL, IF, Result.PASS_SUBJECTIVE, Result.PASS, false);
		testMatchDescriptive(Result.FAIL, IF, Result.FAIL_SUBJECTIVE, Result.PASS, false);
		testMatchDescriptive(Result.FAIL, IF, Result.FAIL, Result.PASS, false);
		testMatchDescriptive(Result.FAIL, IF, Result.AVATAR_SUBJECTIVE, Result.PASS, true);

		// If child is AVATAR_SUBJECTIVE
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, IF, Result.PASS, Result.AVATAR_SUBJECTIVE, true);
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, IF, Result.PASS_SUBJECTIVE, Result.AVATAR_SUBJECTIVE, true);
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, IF, Result.FAIL_SUBJECTIVE, Result.AVATAR_SUBJECTIVE, true);
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, IF, Result.FAIL, Result.AVATAR_SUBJECTIVE, true);
		testMatchDescriptive(Result.AVATAR_SUBJECTIVE, IF, Result.AVATAR_SUBJECTIVE, Result.AVATAR_SUBJECTIVE, true);
	}

	/**
	 * Test matchDescriptive
	 * 
	 * @param operator
	 *            the operator for the criteria matchable
	 * @param descriptive1
	 *            the static result for the first matchable child
	 * @param descriptive2
	 *            the static result for the second matchable child
	 * @param expectedResult
	 *            the expected result of the criteria matchable
	 * @throws AssertionError
	 *             if the criteria is unsupported or the result is not expected
	 */
	private void testMatchDescriptive(Result descriptive1, Operator operator, Result descriptive2, Result expectedResult, boolean isAvatar)
			throws AssertionError
	{
		// Pass two dummy matchables
		try
		{
			CriteriaMatchable criteriaMatchable = new CriteriaMatchable(operator, new Matchable[] {
					new StaticMatchable(descriptive1), new StaticMatchable(descriptive2) });
			
			PatientHistory history = new PatientHistory();
			if ( isAvatar )
			{
				history.setUserType( UserPatient.UserType.AVATAR_PROFILE );
			}
			Result result = criteriaMatchable.matchDescriptive( history );
			assert result == expectedResult : "Expected " + expectedResult + ", got " + result;
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
			// Must be bad operator
			throw new AssertionError(unsupportedCriterionException);
		}
	}

	/**
	 * Performs static matching results.
	 */
	private static class StaticMatchable extends Matchable
	{
		/**
		 * Static descriptive result
		 */
		private final Result matchDescriptive;

		/**
		 * Stores the parameter
		 * 
		 * @param matchDescriptive
		 *            static descriptive result
		 */
		private StaticMatchable(Result matchDescriptive) throws UnsupportedCriterionException
		{
			if (matchDescriptive == null)
				throw new UnsupportedCriterionException(null, "null result");
			this.matchDescriptive = matchDescriptive;
		}

		@Override
		public List<CharacteristicResult> getCharacteristicResults(PatientHistory h, ResourceBundle rB)
		{
			return null;
		}

		@Override
		public Result matchDescriptive(PatientHistory history)
		{
			return matchDescriptive;
		}

		/**
		 * Serializion uid
		 */
		private static final long serialVersionUID = 919053526647554930L;
	}
}
