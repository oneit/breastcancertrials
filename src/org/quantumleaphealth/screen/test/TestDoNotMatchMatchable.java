/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AFFIRMATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.NEGATIVE;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient.UserType;
import org.quantumleaphealth.screen.DoNotMatchMatchable;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;


/**
 * Tests for the <code>DoNotMatchMatchable</code>.
 * 
 * @author Warren Weis
 * @version 2014-03-17
 * @see DoNotMatchMatchable
 */
@Test(description = "DoNotMatch", groups = { "screen", "composite" })
public class TestDoNotMatchMatchable extends TestUtils
{
	public void testDefaultMatch()
	{
		PatientHistory history = new PatientHistory();
		DoNotMatchMatchable dnmm = new DoNotMatchMatchable();
		Result result = dnmm.matchDescriptive(history);
		assert Result.FAIL.equals(result);
	}

	public void testSetMatch()
	{
		PatientHistory history = new PatientHistory();
		DoNotMatchMatchable dnmm = new DoNotMatchMatchable(AFFIRMATIVE);
		Result result = dnmm.matchDescriptive(history);
		assert Result.FAIL.equals(result);
	}
	
	public void testSetPassMatch()
	{
		PatientHistory history = new PatientHistory();
		DoNotMatchMatchable dnmm = new DoNotMatchMatchable(NEGATIVE);
		Result result = dnmm.matchDescriptive(history);
		assert Result.PASS.equals(result);
	}

	public void testDefaultMatchAvatar()
	{
		PatientHistory history = new PatientHistory();
		history.setUserType(UserType.AVATAR_PROFILE);
		DoNotMatchMatchable dnmm = new DoNotMatchMatchable();
		Result result = dnmm.matchDescriptive(history);
		assert Result.FAIL.equals(result);
	}

	public void testSetMatchAvatar()
	{
		PatientHistory history = new PatientHistory();
		history.setUserType(UserType.AVATAR_PROFILE);
		DoNotMatchMatchable dnmm = new DoNotMatchMatchable(AFFIRMATIVE);
		Result result = dnmm.matchDescriptive(history);
		assert Result.FAIL.equals(result);
	}
	
	public void testSetPassMatchAvatar()
	{
		PatientHistory history = new PatientHistory();
		history.setUserType(UserType.AVATAR_PROFILE);
		DoNotMatchMatchable dnmm = new DoNotMatchMatchable(NEGATIVE);
		Result result = dnmm.matchDescriptive(history);
		assert Result.PASS.equals(result);
	}

}
