/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANATOMIC_SITE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_0;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_I;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_II;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIA;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIB;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIC;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_III;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIIA;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIIB;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIIC;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_I_III;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LEFT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RIGHT_BREAST;
import static org.quantumleaphealth.screen.Matchable.Result.*;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.screen.StageMatchable;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;

/**
 * Tests <code>StageMatchable</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 * @see StageMatchable
 */
@Test(description = "Stage matchable", groups = { "screen", "composite" })
public class TestStageMatchable
{

	/**
	 * Test invalid arguments in constructors
	 */
	public void testConstructors()
	{
		// Test bad parent
		try
		{
			AssociativeCriterion associativeCriterion = new AssociativeCriterion();
			associativeCriterion.setParent(UNSURE);
			new StageMatchable(associativeCriterion);
			throw new AssertionError("Wrong code accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
	}

	public void testAvatartMatchDescriptiveSubjective()
	{
		testMatchDescriptiveSubjective( true );
		// test null/null:  this should return a result of AVATAR_SUBJECTIVE
		testMatchDescriptive(null, null, new CharacteristicCode[] { STAGE_0 }, false, AVATAR_SUBJECTIVE, true);
	}
	
	public void testNormalMatchDescriptiveSubjective()
	{
		testMatchDescriptiveSubjective( false );
		testMatchDescriptive(null, null, new CharacteristicCode[] { STAGE_0 }, false, FAIL, false);
	}

	/**
	 * Test matchDescriptive with unsure answer
	 */
	private void testMatchDescriptiveSubjective(boolean isAvatar)
	{
		// Test each combination of left/right not inverted; pass takes
		// preference
		testMatchDescriptive(STAGE_0, UNSURE, new CharacteristicCode[] { STAGE_0 }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(STAGE_I, UNSURE, new CharacteristicCode[] { STAGE_0 }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(STAGE_I, UNSURE, new CharacteristicCode[] { STAGE_I }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(STAGE_II, UNSURE, new CharacteristicCode[] { STAGE_0 }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(STAGE_II, UNSURE, new CharacteristicCode[] { STAGE_II }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(STAGE_III, UNSURE, new CharacteristicCode[] { STAGE_0 }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(STAGE_III, UNSURE, new CharacteristicCode[] { STAGE_III }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(UNSURE, STAGE_0, new CharacteristicCode[] { STAGE_0 }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(UNSURE, STAGE_I, new CharacteristicCode[] { STAGE_0 }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(UNSURE, STAGE_I, new CharacteristicCode[] { STAGE_I }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(UNSURE, STAGE_II, new CharacteristicCode[] { STAGE_0 }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(UNSURE, STAGE_II, new CharacteristicCode[] { STAGE_II }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(UNSURE, STAGE_III, new CharacteristicCode[] { STAGE_0 }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(UNSURE, STAGE_III, new CharacteristicCode[] { STAGE_III }, false, PASS_SUBJECTIVE, isAvatar);

		// Subjective aggregated Stage II,III
		testMatchDescriptive(STAGE_II, null, new CharacteristicCode[] { STAGE_IIA }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(STAGE_II, null, new CharacteristicCode[] { STAGE_IIB }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(STAGE_II, null, new CharacteristicCode[] { STAGE_IIC }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(STAGE_III, null, new CharacteristicCode[] { STAGE_IIIA }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(STAGE_III, null, new CharacteristicCode[] { STAGE_IIIB }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(STAGE_III, null, new CharacteristicCode[] { STAGE_IIIC }, false, PASS_SUBJECTIVE, isAvatar);
		// Subjective aggregated Stage II,III non-matching
		testMatchDescriptive(STAGE_II, null, new CharacteristicCode[] { STAGE_IIIC }, false, FAIL, isAvatar);
		testMatchDescriptive(STAGE_III, null, new CharacteristicCode[] { STAGE_IIC }, false, FAIL, isAvatar);
	}

	public void testAvatarMatchDescriptiveObjective()
	{
		testMatchDescriptiveObjective( true );
	}
	
	public void testNormalMatchDescriptiveObjective()
	{
		testMatchDescriptiveObjective( false );		
	}

	/**
	 * Test matchDescriptive with objective answer
	 */
	private void testMatchDescriptiveObjective(boolean isAvatar)
	{
		// Test each combination of left/right not inverted; pass takes
		// preference
		testMatchDescriptive(STAGE_0, null, new CharacteristicCode[] { STAGE_0 }, false, PASS, isAvatar);
		testMatchDescriptive(STAGE_I, null, new CharacteristicCode[] { STAGE_0 }, false, FAIL, isAvatar);
		testMatchDescriptive(STAGE_I, null, new CharacteristicCode[] { STAGE_I }, false, PASS, isAvatar);
		testMatchDescriptive(STAGE_II, null, new CharacteristicCode[] { STAGE_0 }, false, FAIL, isAvatar);
		testMatchDescriptive(STAGE_II, null, new CharacteristicCode[] { STAGE_II }, false, PASS, isAvatar);
		testMatchDescriptive(STAGE_III, null, new CharacteristicCode[] { STAGE_0 }, false, FAIL, isAvatar);
		testMatchDescriptive(STAGE_III, null, new CharacteristicCode[] { STAGE_III }, false, PASS, isAvatar);
		testMatchDescriptive(null, STAGE_0, new CharacteristicCode[] { STAGE_0 }, false, PASS, isAvatar);
		testMatchDescriptive(null, STAGE_I, new CharacteristicCode[] { STAGE_0 }, false, FAIL, isAvatar);
		testMatchDescriptive(null, STAGE_I, new CharacteristicCode[] { STAGE_I }, false, PASS, isAvatar);
		testMatchDescriptive(null, STAGE_II, new CharacteristicCode[] { STAGE_0 }, false, FAIL, isAvatar);
		testMatchDescriptive(null, STAGE_II, new CharacteristicCode[] { STAGE_II }, false, PASS, isAvatar);
		testMatchDescriptive(null, STAGE_III, new CharacteristicCode[] { STAGE_0 }, false, FAIL, isAvatar);
		testMatchDescriptive(null, STAGE_III, new CharacteristicCode[] { STAGE_III }, false, PASS, isAvatar);

		// Objective aggregated Stage I-III
		testMatchDescriptive(STAGE_I, null, new CharacteristicCode[] { STAGE_I_III }, false, PASS, isAvatar);
		testMatchDescriptive(STAGE_II, null, new CharacteristicCode[] { STAGE_I_III }, false, PASS, isAvatar);
		testMatchDescriptive(STAGE_III, null, new CharacteristicCode[] { STAGE_I_III }, false, PASS, isAvatar);
	}

	/**
	 * Tests matchDescriptive
	 * 
	 * @param left
	 *            the value of the stage characteristic for the left breast or
	 *            <code>null</code> if left breast not diagnosed
	 * @param right
	 *            the value of the stage characteristic for the right breast or
	 *            <code>null</code> if right breast not diagnosed
	 * @param invert
	 *            whether the criterion is inverted
	 * @param expectedResult
	 *            the expected result
	 */
	private void testMatchDescriptive(CharacteristicCode left, CharacteristicCode right,
			CharacteristicCode[] requirement, boolean invert, Result expectedResult, boolean isAvatar)
	{
		// Construct history with diagnoses
		PatientHistory patientHistory = new PatientHistory();
		if ( isAvatar )
		{
			patientHistory.setUserType(UserPatient.UserType.AVATAR_PROFILE);
		}
		if (left != null)
			addDiagnosis(patientHistory, LEFT_BREAST, left);
		if (right != null)
			addDiagnosis(patientHistory, RIGHT_BREAST, right);
		// Construct criterion
		AssociativeCriterion associativeCriterion = new AssociativeCriterion();
		associativeCriterion.setParent(STAGE);
		associativeCriterion.setRequirement(requirement);
		associativeCriterion.setInvert(invert);

		// Create and test matchable
		StageMatchable stageMatchable = new StageMatchable(associativeCriterion);
		Result result = stageMatchable.matchDescriptive(patientHistory);
		assert result == expectedResult : "Left " + left + " and " + right + " with criterion " + associativeCriterion
				+ " resulted in " + result + " instead of " + expectedResult;
	}

	/**
	 * Add an stage diagnosis to a history
	 * 
	 * @param patientHistory
	 *            the patient history
	 * @param location
	 *            the anatomic site
	 * @param value
	 *            the value to store for the stage characteristic
	 */
	private void addDiagnosis(PatientHistory patientHistory, CharacteristicCode location, CharacteristicCode value)
	{
		if ((patientHistory == null) || (location == null) || (value == null))
			return;
		Diagnosis diagnosis = new Diagnosis();
		diagnosis.getValueHistory().setValue(ANATOMIC_SITE, location);
		diagnosis.getValueHistory().setValue(STAGE, value);
		patientHistory.getDiagnoses().add(diagnosis);
	}
}
