/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import java.util.Calendar;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.Procedure;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.patient.YearMonth;
import org.quantumleaphealth.model.trial.TreatmentProcedureCriterion;
import org.quantumleaphealth.model.trial.AssociativeCriterion.SetOperation;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.screen.TreatmentSurgeryProcedureMatchable;
import org.quantumleaphealth.screen.UnsupportedCriterionException;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.SURGERY;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LEFT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISBREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.MASTECTOMY_THERAPEUTIC;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.MASTECTOMY;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.WEEK;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.MOSTRECENT;

/**
 * Composite tests for the <code>TreatmentSurgeryProcedureMatchable</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 * @see TreatmentSurgeryProcedureMatchable
 */
@Test(description = "Surgery", groups = { "screen", "composite" })
public class TestTreatmentSurgeryProcedureMatchable
{

	/**
	 * Tests a recent mastectomy
	 */
	public void matchRecentMastectomy()
	{
		// Left mastectomy performed one month ago
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		Short year = Short.valueOf((short) calendar.get(Calendar.YEAR));
		YearMonth procedureYearMonth = new YearMonth();
		procedureYearMonth.setYear(year);
		procedureYearMonth.setMonth((byte) (calendar.get(Calendar.MONTH) + 1));
		Procedure recentLeftTheraputicMastectomy = new Procedure();
		recentLeftTheraputicMastectomy.setKind(SURGERY);
		recentLeftTheraputicMastectomy.setLocation(LEFT_BREAST);
		recentLeftTheraputicMastectomy.setType(MASTECTOMY_THERAPEUTIC);
		recentLeftTheraputicMastectomy.setStarted(procedureYearMonth);
		// Diagnosis year is same as procedure
		YearMonth diagnosisYearMonth = new YearMonth();
		diagnosisYearMonth.setYear(year);
		PatientHistory history = new PatientHistory();
		history.getYearMonthHistory().put(DIAGNOSISBREAST, procedureYearMonth);
		history.getProcedures().add(recentLeftTheraputicMastectomy);

		// Criterion specifies any mastectomy for this diagnosis within 12 weeks
		TreatmentProcedureCriterion recentMastectomyCriterion = new TreatmentProcedureCriterion();
		recentMastectomyCriterion.setParent(SURGERY);
		recentMastectomyCriterion.setRequirement(new CharacteristicCode[] { MASTECTOMY });
		recentMastectomyCriterion.setSetOperation(SetOperation.INTERSECT);
		recentMastectomyCriterion.setQualifiers(new CharacteristicCode[] { MOSTRECENT });
		recentMastectomyCriterion.setMaximumElapsedTime(12);
		recentMastectomyCriterion.setMaximumElapsedTimeUnits(WEEK);

		// History should match subjective-pass since one month is within twelve
		// weeks range
		try
		{
			TreatmentSurgeryProcedureMatchable matchable = new TreatmentSurgeryProcedureMatchable(
					recentMastectomyCriterion);
			Result result = matchable.matchDescriptive(history);
			assert (result == Result.PASS_SUBJECTIVE) : "Expected " + Result.PASS_SUBJECTIVE + " result, got " + result;
		
			history.setUserType( UserPatient.UserType.AVATAR_PROFILE );
			Result avtar_result = matchable.matchDescriptive(history);
			assert (avtar_result == Result.PASS_SUBJECTIVE) : "Expected " + Result.PASS_SUBJECTIVE + " result, got " + avtar_result;

		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
			throw new AssertionError(unsupportedCriterionException);
		}
	}
}
