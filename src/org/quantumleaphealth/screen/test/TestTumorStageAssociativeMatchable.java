/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANATOMIC_SITE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LEFT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RIGHT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.TUMOR_STAGE_FINDING;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;
import org.quantumleaphealth.screen.TumorStageAssociativeMatchable;
import org.quantumleaphealth.screen.Matchable.Result;
import static org.quantumleaphealth.screen.Matchable.Result.*;

import org.testng.annotations.Test;

/**
 * Tests <code>TumorStageAssociativeMatchable</code>
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 * @see TumorStageAssociativeMatchable
 */
@Test(description = "Tumor stage associative matchable", groups = { "screen", "composite" })
public class TestTumorStageAssociativeMatchable extends TestUtils
{

	/**
	 * Test invalid arguments in constructors
	 */
	public void testConstructors()
	{
		// Test bad parent
		try
		{
			AssociativeCriterion associativeCriterion = new AssociativeCriterion();
			associativeCriterion.setParent(UNSURE);
			new TumorStageAssociativeMatchable(associativeCriterion);
			throw new AssertionError("Wrong code accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
	}

	public void testAvatarMatchDescriptiveSubjective()
	{
		testMatchDescriptiveSubjective( true );
	}
	
	public void testNormalMatchDescriptiveSubjective()
	{
		testMatchDescriptiveSubjective( false );
	}

	/**
	 * Test matchDescriptive with unsure answer
	 */
	private void testMatchDescriptiveSubjective( boolean isAvatar )
	{
		int[] unique = getUniqueIntegers(3);
		// Test left/right unique not inverted; subjective takes preference
		testMatchDescriptive(unique[0], UNSURE.getCode(), new int[] { unique[0] }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(UNSURE.getCode(), unique[0], new int[] { unique[0] }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(unique[0], UNSURE.getCode(), new int[] { unique[0], unique[1], unique[2] }, false,	PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(unique[0], UNSURE.getCode(), new int[] { unique[2] }, false, PASS_SUBJECTIVE, isAvatar);
		testMatchDescriptive(UNSURE.getCode(), unique[0], new int[] { unique[2] }, false, PASS_SUBJECTIVE, isAvatar);
		// Test left/right unique inverted; subjective takes preference
		testMatchDescriptive(unique[0], UNSURE.getCode(), new int[] { unique[0] }, true, FAIL_SUBJECTIVE, isAvatar);
		testMatchDescriptive(UNSURE.getCode(), unique[0], new int[] { unique[0] }, true, FAIL_SUBJECTIVE, isAvatar);
		testMatchDescriptive(unique[0], UNSURE.getCode(), new int[] { unique[0], unique[1], unique[2] }, true, FAIL_SUBJECTIVE, isAvatar);
		testMatchDescriptive(unique[0], UNSURE.getCode(), new int[] { unique[2] }, true, FAIL_SUBJECTIVE, isAvatar);
		testMatchDescriptive(UNSURE.getCode(), unique[0], new int[] { unique[2] }, true, FAIL_SUBJECTIVE, isAvatar);
	}

	public void testAvatarMatchDescriptiveObjective()
	{
		testMatchDescriptiveObjective( true );
	}

	public void tesNormaltMatchDescriptiveObjective()
	{
		testMatchDescriptiveObjective( false );
	}

	/**
	 * Test matchDescriptive with definitive answer
	 */
	public void testMatchDescriptiveObjective( boolean isAvatar )
	{
		int[] unique = getUniqueIntegers(3);
		// Test left/right unique not inverted
		testMatchDescriptive(unique[0], -1, new int[] { unique[0] }, false, PASS, isAvatar);
		testMatchDescriptive(-1, unique[0], new int[] { unique[0] }, false, PASS, isAvatar);
		testMatchDescriptive(unique[0], -1, new int[] { unique[0], unique[1], unique[2] }, false, PASS, isAvatar);
		testMatchDescriptive(unique[0], -1, new int[] { unique[2] }, false, isAvatar? AVATAR_SUBJECTIVE: FAIL, isAvatar);
		testMatchDescriptive(-1, unique[0], new int[] { unique[2] }, false, isAvatar? AVATAR_SUBJECTIVE: FAIL, isAvatar);
		// Test left/right unique inverted; subjective takes preference
		testMatchDescriptive(unique[0], -1, new int[] { unique[0] }, true, FAIL, isAvatar);
		testMatchDescriptive(-1, unique[0], new int[] { unique[0] }, true, FAIL, isAvatar);
		testMatchDescriptive(unique[0], -1, new int[] { unique[0], unique[1], unique[2] }, true, FAIL, isAvatar);
		testMatchDescriptive(unique[0], -1, new int[] { unique[2] }, true, isAvatar? AVATAR_SUBJECTIVE: PASS, isAvatar);
		testMatchDescriptive(-1, unique[0], new int[] { unique[2] }, true, isAvatar? AVATAR_SUBJECTIVE: PASS, isAvatar);
	}

	/**
	 * Tests matchDescriptive
	 * 
	 * @param left
	 *            the value of the tumor stage characteristic for the left
	 *            breast or <code>null</code> if left breast not diagnosed
	 * @param right
	 *            the value of the tumor stage characteristic for the right
	 *            breast or <code>null</code> if right breast not diagnosed
	 * @param invert
	 *            whether the criterion is inverted
	 * @param expectedResult
	 *            the expected result
	 */
	private void testMatchDescriptive(int left, int right, int[] requirement, boolean invert, Result expectedResult, boolean isAvatar)
	{
		// Construct history with diagnoses
		PatientHistory patientHistory = new PatientHistory();
		if ( isAvatar )
		{
			patientHistory.setUserType(UserPatient.UserType.AVATAR_PROFILE);
		}
		
		if (left >= 0)
			addDiagnosis(patientHistory, LEFT_BREAST, new CharacteristicCode(left));
		if (right >= 0)
			addDiagnosis(patientHistory, RIGHT_BREAST, new CharacteristicCode(right));
		// Construct criterion
		AssociativeCriterion associativeCriterion = new AssociativeCriterion();
		associativeCriterion.setParent(TUMOR_STAGE_FINDING);
		CharacteristicCode[] requirementCharacteristic = null;
		if ((requirement != null) && (requirement.length > 0))
		{
			requirementCharacteristic = new CharacteristicCode[requirement.length];
			for (int index = 0; index < requirement.length; index++)
				requirementCharacteristic[index] = new CharacteristicCode(requirement[index]);
		}
		associativeCriterion.setRequirement(requirementCharacteristic);
		associativeCriterion.setRequirement(requirementCharacteristic);
		associativeCriterion.setInvert(invert);

		// Create and test matchable
		TumorStageAssociativeMatchable tumorStageAssociativeMatchable = new TumorStageAssociativeMatchable(
				associativeCriterion);
		Result result = tumorStageAssociativeMatchable.matchDescriptive(patientHistory);
		assert result == expectedResult : "Left " + left + " and " + right + " with criterion " + associativeCriterion
				+ " resulted in " + result + " instead of " + expectedResult;
	}

	/**
	 * Add an tumor stage diagnosis to a history
	 * 
	 * @param patientHistory
	 *            the patient history
	 * @param location
	 *            the anatomic site
	 * @param value
	 *            the value to store for the tumor stage characteristic
	 */
	private void addDiagnosis(PatientHistory patientHistory, CharacteristicCode location, CharacteristicCode value)
	{
		if ((patientHistory == null) || (location == null) || (value == null))
			return;
		Diagnosis diagnosis = new Diagnosis();
		diagnosis.getValueHistory().setValue(ANATOMIC_SITE, location);
		diagnosis.getValueHistory().setValue(TUMOR_STAGE_FINDING, value);
		patientHistory.getDiagnoses().add(diagnosis);
	}
}
