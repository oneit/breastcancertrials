/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANATOMIC_SITE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISBREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LEFT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.CHEMOTHERAPY;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DOXORUBICIN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.CYCLOPHOSPHAMIDE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PACLITAXEL;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.CLASS_ANTHRACYCLINE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.CLASS_ANTHRACYCLINE_SET;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.CLASS_TAXANE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.MOSTRECENT;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.COMPLETED;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ADJUVANT;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ENDOCRINE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.TAMOXIFEN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PROGRESSING;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.CONCURRENT;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.INCOMPLETE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RECURRENT_LOCALLY;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ADVERSE_DRUG_EFFECT;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;

import java.util.Calendar;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.Therapy;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.patient.YearMonth;
import org.quantumleaphealth.model.trial.Criteria;
import org.quantumleaphealth.model.trial.TreatmentRegimenCriterion;
import org.quantumleaphealth.model.trial.AssociativeCriterion.SetOperation;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.screen.BreastCancerConverter;
import org.quantumleaphealth.screen.Matchable;
import org.quantumleaphealth.screen.TreatmentRegimenMatchable;
import org.quantumleaphealth.screen.UnsupportedCriterionException;
import org.quantumleaphealth.screen.Matchable.Result;
import org.testng.annotations.Test;

/**
 * Tests <code>TreatmentRegimenMatchable</code>
 * 
 * @author Tom Bechtold
 * @version 2009-07-09
 * @see TreatmentRegimenMatchable
 */
@Test(description = "Treatment regimen matchable", groups = { "screen", "composite" })
public class TestTreatmentRegimenMatchable
{

	/**
	 * Test constructor
	 */
	public void testConstructor()
	{
		// Test null criterion and parent
		try
		{
			new TreatmentRegimenMatchable(null, null, (byte) 0);
			throw new AssertionError("Null criterion accepted");
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
			throw new AssertionError(unsupportedCriterionException);
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		try
		{
			new TreatmentRegimenMatchable(new TreatmentRegimenCriterion(), null, (byte) 0);
			throw new AssertionError("Null code accepted");
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
			throw new AssertionError(unsupportedCriterionException);
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
	}

	/**
	 * Tests a recent regimen
	 */
	public void matchRecentChemotherapy()
	{
		// Chemotherapy lasted four months and finished one month ago
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		Short year = Short.valueOf((short) calendar.get(Calendar.YEAR));
		YearMonth completedYearMonth = new YearMonth();
		completedYearMonth.setYear(year);
		completedYearMonth.setMonth((byte) (calendar.get(Calendar.MONTH) + 1));
		calendar.add(Calendar.MONTH, -4);
		year = Short.valueOf((short) calendar.get(Calendar.YEAR));
		YearMonth startedYearMonth = new YearMonth();
		startedYearMonth.setYear(year);
		startedYearMonth.setMonth((byte) (calendar.get(Calendar.MONTH) + 1));
		Therapy recentChemotherapy = new Therapy();
		recentChemotherapy.setAgents(new CharacteristicCode[] { CLASS_ANTHRACYCLINE_SET[0] });
		recentChemotherapy.setSetting(ADJUVANT);
		recentChemotherapy.setType(CHEMOTHERAPY);
		recentChemotherapy.setStarted(startedYearMonth);
		recentChemotherapy.setCompleted(completedYearMonth);
		recentChemotherapy.setStatus(COMPLETED);
		// Diagnosis year is same as started
		YearMonth diagnosisYearMonth = new YearMonth();
		diagnosisYearMonth.setYear(year);
		PatientHistory history = new PatientHistory();
		history.getYearMonthHistory().put(DIAGNOSISBREAST, diagnosisYearMonth);
		history.getTherapies().add(recentChemotherapy);
		// Recent diagnosis is left breast
		Diagnosis diagnosis = new Diagnosis();
		diagnosis.getValueHistory().setValue(ANATOMIC_SITE, LEFT_BREAST);
		history.getDiagnoses().add(diagnosis);

		// Criterion specifies therapy for most recent diagnosis
		TreatmentRegimenCriterion treatmentRegimenCriterion = new TreatmentRegimenCriterion();
		treatmentRegimenCriterion.setParent(CHEMOTHERAPY);
		treatmentRegimenCriterion.setRequirement(CLASS_ANTHRACYCLINE_SET);
		treatmentRegimenCriterion.setSetOperation(SetOperation.INTERSECT);
		treatmentRegimenCriterion.setQualifiers(new CharacteristicCode[] { MOSTRECENT });

		// History should match pass since regimen started after diagnosis year
		try
		{
			TreatmentRegimenMatchable matchable = new TreatmentRegimenMatchable(treatmentRegimenCriterion, null,
					(byte) (1 << 4));
			Result result = matchable.matchDescriptive(history);
			assert (result == Result.PASS) : "Expected " + Result.PASS + " result, got " + result;
			
			history.setUserType(UserPatient.UserType.AVATAR_PROFILE);
			result = matchable.matchDescriptive(history);
			assert (result == Result.PASS) : "Expected " + Result.PASS + " result, got " + result;
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
			throw new AssertionError(unsupportedCriterionException);
		}
	}

	/**
	 * Tests that a regimen matches to ANDed two criteria
	 */
	public void matchChemotherapyAnd()
	{
		// Chemotherapy regimen contained two drugs given 12-24 months ago
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.YEAR, -1);
		Short year = Short.valueOf((short) calendar.get(Calendar.YEAR));
		YearMonth completedYearMonth = new YearMonth();
		completedYearMonth.setYear(year);
		completedYearMonth.setMonth((byte) (calendar.get(Calendar.MONTH) + 1));
		calendar.add(Calendar.YEAR, -1);
		year = Short.valueOf((short) calendar.get(Calendar.YEAR));
		YearMonth startedYearMonth = new YearMonth();
		startedYearMonth.setYear(year);
		startedYearMonth.setMonth((byte) (calendar.get(Calendar.MONTH) + 1));
		Therapy recentChemotherapy = new Therapy();
		recentChemotherapy.setAgents(new CharacteristicCode[] { DOXORUBICIN, CYCLOPHOSPHAMIDE, PACLITAXEL });
		recentChemotherapy.setSetting(ADJUVANT);
		recentChemotherapy.setType(CHEMOTHERAPY);
		recentChemotherapy.setStarted(startedYearMonth);
		recentChemotherapy.setCompleted(completedYearMonth);
		recentChemotherapy.setStatus(COMPLETED);
		// Diagnosis year is same as started
		YearMonth diagnosisYearMonth = new YearMonth();
		diagnosisYearMonth.setYear(year);
		PatientHistory history = new PatientHistory();
		history.getYearMonthHistory().put(DIAGNOSISBREAST, diagnosisYearMonth);
		history.getTherapies().add(recentChemotherapy);
		// Recent diagnosis is left breast
		Diagnosis diagnosis = new Diagnosis();
		diagnosis.getValueHistory().setValue(ANATOMIC_SITE, LEFT_BREAST);
		history.getDiagnoses().add(diagnosis);

		// Criterion is ANDed criteria, each specifying one adjuvant drug class:
		// anthracycline, taxane
		Criteria criteria = new Criteria();
		criteria.setOperator(Criteria.Operator.AND);
		TreatmentRegimenCriterion treatmentRegimenCriterion = new TreatmentRegimenCriterion();
		treatmentRegimenCriterion.setParent(CHEMOTHERAPY);
		treatmentRegimenCriterion.setCount(1);
		treatmentRegimenCriterion.setRequirement(new CharacteristicCode[] { CLASS_ANTHRACYCLINE });
		treatmentRegimenCriterion.setSetOperation(SetOperation.INTERSECT);
		treatmentRegimenCriterion.setSettings(new CharacteristicCode[] { ADJUVANT });
		criteria.getChildren().add(treatmentRegimenCriterion);
		treatmentRegimenCriterion = new TreatmentRegimenCriterion();
		treatmentRegimenCriterion.setParent(CHEMOTHERAPY);
		treatmentRegimenCriterion.setCount(1);
		treatmentRegimenCriterion.setRequirement(new CharacteristicCode[] { CLASS_TAXANE });
		treatmentRegimenCriterion.setSetOperation(SetOperation.INTERSECT);
		treatmentRegimenCriterion.setSettings(new CharacteristicCode[] { ADJUVANT });
		criteria.getChildren().add(treatmentRegimenCriterion);

		// History should match pass since regimen contains both classes
		try
		{
			Matchable matchable = new BreastCancerConverter().convert(criteria);
			Result result = matchable.matchDescriptive(history);
			assert (result == Result.PASS) : "Expected " + Result.PASS + " result, got " + result;
			
			history.setUserType(UserPatient.UserType.AVATAR_PROFILE);
			result = matchable.matchDescriptive(history);
			assert (result == Result.AVATAR_SUBJECTIVE) : "Expected " + Result.AVATAR_SUBJECTIVE + " result, got " + result;
			
			// now nothing defined
			PatientHistory emptyHistory = new PatientHistory();
			emptyHistory.setUserType(UserPatient.UserType.AVATAR_PROFILE);
			result = matchable.matchDescriptive(emptyHistory);
			assert (result == Result.AVATAR_SUBJECTIVE) : "Incomplete treatment expected " + Result.PASS_SUBJECTIVE
			+ " result, got " + result;
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
			throw new AssertionError(unsupportedCriterionException);
		}
	}

	/**
	 * Tests that a progression regimen matches to objective and subjective
	 * therapies
	 */
	public void matchProgression()
	{
		// Tamoxifen lasted five years and finished one month ago
		CharacteristicCode[] tamoxifenAgent = new CharacteristicCode[] { TAMOXIFEN };
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.MONTH, -1);
		Short year = Short.valueOf((short) calendar.get(Calendar.YEAR));
		YearMonth completedYearMonth = new YearMonth();
		completedYearMonth.setYear(year);
		completedYearMonth.setMonth((byte) (calendar.get(Calendar.MONTH) + 1));
		calendar.add(Calendar.YEAR, -5);
		year = Short.valueOf((short) calendar.get(Calendar.YEAR));
		YearMonth startedYearMonth = new YearMonth();
		startedYearMonth.setYear(year);
		startedYearMonth.setMonth((byte) (calendar.get(Calendar.MONTH) + 1));
		Therapy tamoxifen = new Therapy();
		tamoxifen.setAgents(tamoxifenAgent);
		tamoxifen.setSetting(ADJUVANT);
		tamoxifen.setType(ENDOCRINE);
		tamoxifen.setStarted(startedYearMonth);
		tamoxifen.setCompleted(completedYearMonth);
		tamoxifen.setStatus(COMPLETED);
		// Diagnosis year is same as started
		YearMonth diagnosisYearMonth = new YearMonth();
		diagnosisYearMonth.setYear(year);
		PatientHistory history = new PatientHistory();
		history.getYearMonthHistory().put(DIAGNOSISBREAST, diagnosisYearMonth);
		history.getTherapies().add(tamoxifen);
		// Recent diagnosis is left breast
		Diagnosis diagnosis = new Diagnosis();
		diagnosis.getValueHistory().setValue(ANATOMIC_SITE, LEFT_BREAST);
		history.getDiagnoses().add(diagnosis);

		// Criterion specifies therapy with progression
		TreatmentRegimenCriterion treatmentRegimenCriterion = new TreatmentRegimenCriterion();
		treatmentRegimenCriterion.setParent(ENDOCRINE);
		treatmentRegimenCriterion.setRequirement(tamoxifenAgent);
		treatmentRegimenCriterion.setSetOperation(SetOperation.INTERSECT);
		treatmentRegimenCriterion.setQualifiers(new CharacteristicCode[] { PROGRESSING });
		TreatmentRegimenMatchable matchable;
		try
		{
			matchable = new TreatmentRegimenMatchable(treatmentRegimenCriterion, null, (byte) (0));
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
			throw new AssertionError(unsupportedCriterionException);
		}

		// Completed therapy fails
		Result result = matchable.matchDescriptive(history);
		assert (result == Result.FAIL) : "Completed treatment expected " + Result.FAIL + " result, got " + result;
		// Concurrent therapy fails
		tamoxifen.setStatus(CONCURRENT);
		result = matchable.matchDescriptive(history);
		assert (result == Result.FAIL) : "Concurrent treatment expected " + Result.FAIL + " result, got " + result;
		// Incomplete therapy without reason fails
		tamoxifen.setStatus(INCOMPLETE);
		tamoxifen.setLength((byte) (1));
		tamoxifen.setIncomplete(null);
		result = matchable.matchDescriptive(history);
		assert (result == Result.FAIL) : "Incomplete treatment expected " + Result.FAIL + " result, got " + result;
		// Adverse effect reason fails
		tamoxifen.setIncomplete(ADVERSE_DRUG_EFFECT);
		result = matchable.matchDescriptive(history);
		assert (result == Result.FAIL) : "Incomplete treatment expected " + Result.FAIL + " result, got " + result;
		// Recurrent reason passes
		tamoxifen.setIncomplete(RECURRENT_LOCALLY);
		result = matchable.matchDescriptive(history);
		assert (result == Result.PASS) : "Incomplete treatment expected " + Result.PASS + " result, got " + result;
		// Unsure reason passes subjectively
		tamoxifen.setIncomplete(UNSURE);
		result = matchable.matchDescriptive(history);
		assert (result == Result.PASS_SUBJECTIVE) : "Incomplete treatment expected " + Result.PASS_SUBJECTIVE
				+ " result, got " + result;
		
		history.setUserType(UserPatient.UserType.AVATAR_PROFILE);
		result = matchable.matchDescriptive(history);
		assert (result == Result.PASS_SUBJECTIVE) : "Incomplete treatment expected " + Result.PASS_SUBJECTIVE
		+ " result, got " + result;
		
		// now nothing defined
		PatientHistory emptyHistory = new PatientHistory();
		emptyHistory.setUserType(UserPatient.UserType.AVATAR_PROFILE);
		result = matchable.matchDescriptive(emptyHistory);
		assert (result == Result.AVATAR_SUBJECTIVE) : "Incomplete treatment expected " + Result.PASS_SUBJECTIVE
		+ " result, got " + result;
	}
}
