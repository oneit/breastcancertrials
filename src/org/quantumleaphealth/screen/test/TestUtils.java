/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import java.util.Random;

/**
 * Utility methods for test classes
 * 
 * @author Tom Bechtold
 * @version 2008-12-12
 */
public abstract class TestUtils
{
	/**
	 * Random-number generator
	 */
	public static final Random GENERATOR = new Random();

	/**
	 * @return unique positive integers
	 * @param length
	 *            the number of integers to return
	 */
	public static int[] getUniqueIntegers(int length)
	{
		if (length <= 0)
			return null;
		int[] unique = new int[length];
		regenerate: do
		{
			// Generate all new positive random numbers
			for (int index = 0; index < unique.length; index++)
				unique[index] = GENERATOR.nextInt(Integer.MAX_VALUE - 1) + 1;
			// If any two are equal then regenerate
			for (int leftIndex = 0; leftIndex < unique.length - 1; leftIndex++)
				for (int rightIndex = leftIndex + 1; rightIndex < unique.length; rightIndex++)
					if (unique[leftIndex] == unique[rightIndex])
						continue regenerate;
			break;
		} while (true);
		return unique;
	}
}
