/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen.test;

import java.util.List;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.BooleanCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.screen.BooleanMatchable;
import org.quantumleaphealth.screen.Matchable.CharacteristicResult;
import org.quantumleaphealth.screen.Matchable.Result;
import static org.quantumleaphealth.screen.Matchable.Result.AVATAR_SUBJECTIVE;
import static org.quantumleaphealth.screen.Matchable.Result.FAIL;
import static org.quantumleaphealth.screen.Matchable.Result.PASS;
import org.testng.annotations.Test;

/**
 * Tests <code>BooleanMatchable</code>. This composite test relies upon the
 * following classes:
 * <ul>
 * <li><code>PatientHistory</code></li>
 * <li><code>BooleanCriterion</code></li>
 * <li><code>CharacteristicCode</code></li>
 * <li><code>Matchable</code></li>
 * </ul>
 * 
 * @author Tom Bechtold
 * @version 2008-07-07
 * @see BooleanMatchable
 */
@Test(description = "Boolean matchable", groups = { "screen", "composite" })
public class TestBooleanMatchable extends TestUtils
{

	/**
	 * Test invalid arguments in constructors
	 */
	public void testConstructors()
	{
		// Test null criterion and parent
		try
		{
			new BooleanMatchable(null);
			throw new AssertionError("Null criterion accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
		try
		{
			new BooleanMatchable(new BooleanCriterion());
			throw new AssertionError("Null code accepted");
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
		}
	}

	public void testMatchDescriptive()
	{
		// Found
		testMatchDescriptive(GENERATOR.nextInt(), false, true, PASS, false);
		// Not found
		testMatchDescriptive(GENERATOR.nextInt(), false, false, FAIL, false);
		// Found inverted
		testMatchDescriptive(GENERATOR.nextInt(), true, true, FAIL, false);
		// Not found inverted
		testMatchDescriptive(GENERATOR.nextInt(), true, false, PASS, false);

		// repeat the previous four tests for avatars.
		// Found
		testMatchDescriptive(GENERATOR.nextInt(), false, true, PASS, true);
		// Not found
		testMatchDescriptive(GENERATOR.nextInt(), false, false, AVATAR_SUBJECTIVE, true);
		// Found inverted
		testMatchDescriptive(GENERATOR.nextInt(), true, true, FAIL, true);
		// Not found inverted
		testMatchDescriptive(GENERATOR.nextInt(), true, false, AVATAR_SUBJECTIVE, true);

	}

	/**
	 * Ensure the singleton result is the same as in matchDescriptive
	 */
	public void testCharacteristicResults()
	{
		// Found
		List<CharacteristicResult> results = testMatchDescriptive(GENERATOR.nextInt(), false, true, PASS, false);
		assert (results != null) && (results.size() == 1) && PASS.equals(results.get(0).getResult()) : "Result not pass:"
				+ results;
		// Not found
		results = testMatchDescriptive(GENERATOR.nextInt(), false, false, FAIL, false);
		assert (results != null) && (results.size() == 1) && FAIL.equals(results.get(0).getResult()) : "Result not fail:"
				+ results;
		
		// Repeat the two tests for avatars
		results = testMatchDescriptive(GENERATOR.nextInt(), false, true, PASS, true);
		assert (results != null) && (results.size() == 1) && PASS.equals(results.get(0).getResult()) : "Result not pass:"
				+ results;
		// Not found
		results = testMatchDescriptive(GENERATOR.nextInt(), false, false, AVATAR_SUBJECTIVE, true);
		assert (results != null) && (results.size() == 1) && AVATAR_SUBJECTIVE.equals(results.get(0).getResult()) : "Result not fail:"
				+ results;
	}

	/**
	 * Test matchDescriptive method
	 * 
	 * @param characteristicCode
	 *            the criterion's code
	 * @param invert
	 *            the criterion's invert
	 * @param populated
	 *            whether the patient history is populated with the
	 *            characteristic
	 * @param expectedResult
	 *            the expected result
	 * @return the characteristic result from the matchable
	 * @throws AssertionError
	 *             if the expected result does not match the result
	 */
	private List<CharacteristicResult> testMatchDescriptive(int characteristicCode, boolean invert, boolean populated,
			Result expectedResult, boolean isAvatar) throws AssertionError
	{
		// Construct history with boolean characteristic
		PatientHistory patientHistory = new PatientHistory();
		if (populated)
			patientHistory.getBinaryHistory().add(new CharacteristicCode(characteristicCode));
		
		if ( isAvatar )
		{
			patientHistory.setUserType( UserPatient.UserType.AVATAR_PROFILE );
		}
		// Construct criterion
		BooleanCriterion booleanCriterion = new BooleanCriterion();
		booleanCriterion.setCharacteristicCode(new CharacteristicCode(characteristicCode));
		booleanCriterion.setInvert(invert);

		// Create and test matchable
		BooleanMatchable booleanMatchable = new BooleanMatchable(booleanCriterion);
		Result result = booleanMatchable.matchDescriptive(patientHistory);
		assert result == expectedResult : "Matching " + characteristicCode + " to " + booleanCriterion
				+ " resulted in " + result + " instead of " + expectedResult;
		return booleanMatchable.getCharacteristicResults(patientHistory, null);
	}
}
