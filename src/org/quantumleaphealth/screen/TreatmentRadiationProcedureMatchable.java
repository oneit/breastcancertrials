/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISBREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LEFT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RIGHT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RADIATION;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RADIATION_AGGREGATORS;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RADIATION_BREAST_CONTRALATERAL;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RADIATION_BREAST_IPSILATERAL;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.Procedure;
import org.quantumleaphealth.model.patient.YearMonth;
import org.quantumleaphealth.model.trial.TreatmentProcedureCriterion;
import org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.ontology.OntologyUtils;

/**
 * Matches a patient's radiation procedure history to an eligibility criterion.
 * 
 * @author Tom Bechtold
 * @version 2008-07-02
 */
public class TreatmentRadiationProcedureMatchable extends DiagnosisMatchable
{
	/**
	 * The criterion
	 */
	private final TreatmentProcedureCriterion criterion;
	/**
	 * The required locations or <code>null</code> for any
	 */
	private final Set<CharacteristicCode> requirement;
	/**
	 * Whether or not to limit therapies to current ones. <code>true</code>
	 * means limit to current therapies; <code>false</code> means limit to
	 * completed therapies; <code>null</code> means do not limit.
	 */
	private final Boolean concurrent;
	/**
	 * Whether or not only procedures started after the most recent diagnosis
	 * apply
	 */
	private final boolean mostRecent;

	/**
	 * The string to print when a localized description cannot be found
	 */
	private static final String UNKNOWN_KEY = "???";

	/**
	 * Validates and stores the requirements into instance variables.
	 * 
	 * @param radiationCriterion
	 *            the criterion
	 * @throws IllegalArgumentException
	 *             if <code>criterion</code> is <code>null</code>
	 * @throws UnsupportedCriterionException
	 *             if the procedure type is not radiation or a qualifier is not
	 *             supported
	 */
	public TreatmentRadiationProcedureMatchable(TreatmentProcedureCriterion radiationCriterion)
			throws IllegalArgumentException, UnsupportedCriterionException
	{
		// Validate parameter
		if ((radiationCriterion == null) || !RADIATION.equals(radiationCriterion.getParent()))
			throw new UnsupportedCriterionException(radiationCriterion, "not radiation");
		// Store instance variable
		this.criterion = radiationCriterion;
		LinkedList<CharacteristicCode> qualifiers = (radiationCriterion.getQualifiers() == null) ? new LinkedList<CharacteristicCode>()
				: new LinkedList<CharacteristicCode>(Arrays.asList(radiationCriterion.getQualifiers()));
		boolean isCompleted = qualifiers.remove(BreastCancerCharacteristicCodes.COMPLETED);
		boolean isConcurrent = qualifiers.remove(BreastCancerCharacteristicCodes.CONCURRENT);
		concurrent = isConcurrent ? Boolean.TRUE : (isCompleted ? Boolean.FALSE : null);
		mostRecent = qualifiers.remove(BreastCancerCharacteristicCodes.MOSTRECENT);
		// Report any unsupported qualifiers (breast cancer is ignored because
		// patient history only concerns breast cancer)
		qualifiers.remove(BreastCancerCharacteristicCodes.BREAST_CANCER);
		if (qualifiers.size() > 0)
			throw new UnsupportedCriterionException(radiationCriterion, "qualifiers " + qualifiers);
		// Expand the requirement using the radiation aggregator
		requirement = OntologyUtils.expand(criterion.getRequirement(), RADIATION_AGGREGATORS);
	}

	/**
	 * Returns whether the procedure matches the criterion. This method
	 * considers all criterion fields except <code>invert</code>:
	 * <ul>
	 * <li><code>interventions</code>: The procedure's <code>type</code> is a
	 * member</li>
	 * <li><code>diagnosisYearMonth</code>: The procedure's <code>started</code>
	 * is not earlier</li>
	 * <li><code>concurrent</code>: The procedure's <code>completed</code> date
	 * is not available</li>
	 * <li><code>not concurrent</code>: The procedure's <code>completed</code>
	 * date is available</li>
	 * <li><code>requirement</code>: The procedure's <code>location</code> is a
	 * member, after transforming <code>LEFT_BREAST</code> and
	 * <code>RIGHT_BREAST</code> to <code>RADIATION_BREAST_IPSILATERAL</code>
	 * and <code>RADIATION_BREAST_CONTRALATERAL</code> based upon the values of
	 * the <code>left</code> and <code>right</code> parameters.</li>
	 * <li><code></code></li>
	 * <li><code></code></li>
	 * </ul>
	 * 
	 * @param procedure
	 *            a procedure
	 * @param diagnosisYearMonth
	 *            the year/month of diagnosis or <code>null</code> if not
	 *            available or if criterion does not specify most recent
	 * @param left
	 *            patient is metastatic or the left breast was diagnosed in
	 *            recent diagnosis
	 * @param right
	 *            patient is metastatic or the right breast was diagnosed in
	 *            recent diagnosis
	 * @return whether the procedure matches the criterion
	 */
	private boolean getResult(Procedure procedure, YearMonth diagnosisYearMonth, boolean left, boolean right)
	{
		// If no procedure then fail
		if (procedure == null)
			return false;

		// Match interventions to procedure type
		if ((criterion.getInterventions() != null) && (criterion.getInterventions().length > 0)
				&& (procedure.getType() != null) && !procedure.getType().isMember(criterion.getInterventions()))
			return false;

		// Filter most recent diagnosis: compare therapy start year to diagnosis
		// year
		if ((diagnosisYearMonth != null) && (procedure.getStarted() != null)
				&& procedure.getStarted().isEarlier(diagnosisYearMonth))
			return false;

		// Filter concurrency to procedure completion
		boolean procedureConcurrent = (procedure.getCompleted() == null)
				|| (procedure.getCompleted().getYear() == null)
				|| (procedure.getCompleted().getYear().shortValue() <= 0);
		if (Boolean.TRUE.equals(concurrent) && !procedureConcurrent)
			return false;
		if (Boolean.FALSE.equals(concurrent) && procedureConcurrent)
			return false;

		// Filter out location using requirement after converting laterality
		CharacteristicCode location = procedure.getLocation();
		if (LEFT_BREAST.equals(location))
			location = left ? RADIATION_BREAST_IPSILATERAL : RADIATION_BREAST_CONTRALATERAL;
		else if (RIGHT_BREAST.equals(location))
			location = right ? RADIATION_BREAST_IPSILATERAL : RADIATION_BREAST_CONTRALATERAL;
		// Empty requirement matches to all locations
		return (requirement == null) || requirement.isEmpty() || ((location != null) && requirement.contains(location));
	}

	/**
	 * @param history
	 *            the patient's history
	 * @return how each radiation procedure matches the criterion or
	 *         <code>null</code> if history not available
	 */
	private Map<Procedure, Result> getResults(PatientHistory history)
	{
		// If no history then return null
		if (history == null)
			return null;
		// Determine recent malignancy date if specified and left/right
		// diagnoses
		YearMonth diagnosis = mostRecent ? history.getYearMonthHistory().get(DIAGNOSISBREAST) : null;
		boolean left = getDiagnosis(history, LEFT_BREAST) != null;
		boolean right = getDiagnosis(history, RIGHT_BREAST) != null;
		if (isMetastatic(history))
		{
			left = true;
			right = true;
		}

		// Create a copy of the list and prune items that do not match
		Set<Procedure> procedures = history.getProcedures();
		Map<Procedure, Result> results = new LinkedHashMap<Procedure, Result>(procedures.size());
		for (Procedure procedure : procedures)
		{	
			if (RADIATION.equals(procedure.getKind()))
			{	
				results.put(procedure, getResult(procedure, diagnosis, left, right) ? Result.PASS : Result.FAIL);
			}	
		}	
		// Return the pruned list
		return results;
	}

	/**
	 * Returns how the patient's radiation procedures match to the requirement.
	 * 
	 * @param history
	 *            the patient's history
	 * @return how the patient's radiation procedures match to the requirement
	 * @see org.quantumleaphealth.screen.Matchable#matchDescriptive(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	private Result getResult(Map<Procedure, Result> results, PatientHistory history)
	{
		if ( ( results == null || results.size() <= 0 ) && history != null && history.isAvatar() )
		{
			return Result.AVATAR_SUBJECTIVE;
		}
			
		// If any procedure is applicable then fail
		if (results != null)
		{	
			for (Map.Entry<Procedure, Result> result : results.entrySet())
			{	
				if (Result.PASS == result.getValue())
				{	
					return criterion.isInvert() ? Result.FAIL : Result.PASS;
				}
			}
		}	
		return criterion.isInvert() ? Result.PASS : Result.FAIL;
	}

	/**
	 * Returns how the patient's radiation procedures match to the requirement.
	 * 
	 * @param history
	 *            the patient's history
	 * @return how the patient's radiation procedures match to the requirement
	 * @see org.quantumleaphealth.screen.Matchable#matchDescriptive(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	public Result matchDescriptive(PatientHistory history)
	{
		return getResult(getResults(history), history);
	}

	/**
	 * Localized description(s) of how each radiation procedure matches the
	 * criterion.
	 * 
	 * @param history
	 *            the patient's history
	 * @param resourceBundle
	 *            contains localized descriptions of locations and types
	 * @return localized description(s) of how each radiation procedure matches
	 *         the criterion
	 * @see org.quantumleaphealth.screen.Matchable#getCharacteristic(org.quantumleaphealth.model.patient.PatientHistory,
	 *      java.util.ResourceBundle)
	 */
	public List<CharacteristicResult> getCharacteristicResults(PatientHistory history, ResourceBundle resourceBundle)
	{
		// Get results for each procedure
		Map<Procedure, Result> results = getResults(history);
		Result result = getResult(results, history);
		
		if ((results == null) || results.isEmpty())
			return Arrays.asList(new CharacteristicResult("No radiation history", result));
		// Add each procedure to the applicable or unapplicable lists
		StringBuilder applicable = new StringBuilder();
		StringBuilder unapplicable = new StringBuilder();
		for (Map.Entry<Procedure, Result> entry : results.entrySet())
		{
			StringBuilder builder = entry.getValue() == Result.PASS ? applicable : unapplicable;
			if (builder.length() > 0)
				builder.append(", ");
			Procedure procedure = entry.getKey();
			if (procedure.getType() != null)
				builder.append(getString(resourceBundle, RADIATION.toString() + '.' + procedure.getType().toString()))
						.append(' ');
			builder.append("radiotherapy to ");
			if (procedure.getLocation() != null)
				builder.append(getString(resourceBundle, RADIATION.toString() + '.'
						+ procedure.getLocation().toString()));
			else
				builder.append("site" + UNKNOWN_KEY);
			append(builder, " started ", procedure.getStarted());
			append(builder, " and completed ", procedure.getCompleted());
		}
		if (applicable.length() != 0)
			applicable.insert(0, "Applicable procedure(s): ");
		if (unapplicable.length() != 0)
		{
			unapplicable.insert(0, "Unapplicable procedure(s): ");
			if (applicable.length() != 0)
				unapplicable.insert(0, "; ");
		}
		return Arrays.asList(new CharacteristicResult(applicable.append(unapplicable).toString(), result));
	}

	/**
	 * Returns a localized string
	 * 
	 * @param resourceBundle
	 *            contains localized strings
	 * @param key
	 *            the key used to find the string
	 * @return a localized string or <tt>???</tt><code>key</code><tt>???</tt> if
	 *         <code>key</code> is not found in <code>resourceBundle</code>
	 */
	private static String getString(ResourceBundle resourceBundle, String key)
	{
		if ((resourceBundle == null) || (key == null))
			return UNKNOWN_KEY;
		try
		{
			return resourceBundle.getString(key);
		}
		catch (MissingResourceException missingResourceException)
		{
			return UNKNOWN_KEY + key + UNKNOWN_KEY;
		}
	}

	/**
	 * Appends a description of a date. This method only prints dates with a
	 * valid year. It will also print any valid month.
	 * 
	 * @param builder
	 *            the builder to append to
	 * @param prefix
	 *            the optional prefix to append before the description
	 * @param yearMonth
	 *            the date to append or <code>null</code> to not append anything
	 */
	private static void append(StringBuilder builder, String prefix, YearMonth yearMonth)
	{
		if ((builder == null) || (yearMonth == null) || (yearMonth.getYear() == null)
				|| (yearMonth.getYear().shortValue() < 0))
			return;
		if (prefix != null)
			builder.append(prefix);
		if (yearMonth.getMonth() > 0)
			builder.append(yearMonth.getMonth()).append('/');
		builder.append(yearMonth.getYear());
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = -1606041995241069487L;
}
