/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import java.io.Serializable;
import java.util.List;

import org.quantumleaphealth.model.trial.EligibilityCriterion.Type;
import org.quantumleaphealth.screen.Matchable.CharacteristicResult;
import org.quantumleaphealth.screen.Matchable.Result;

/**
 * Result of a descriptive match between an eligibility criterion and a patient
 * characteristic.
 * 
 * @author Tom Bechtold
 * @version 2008-09-26
 */
public class CriterionProfile implements Serializable
{

	/**
	 * The result of matching the criterion to the patient's history or
	 * <code>null</code> if undetermined (e.g., the matching engine does not
	 * support the criterion).
	 */
	private final Result result;

	/**
	 * The type of criterion
	 */
	private final Type type;

	/**
	 * The localized text of the criterion
	 */
	private final String text;

	/**
	 * The localized qualifier or <code>null</code> if none
	 */
	private final String qualifier;

	/**
	 * The localized patient characteristics and their results that factor into
	 * the match or <code>null</code> for none
	 */
	private final List<CharacteristicResult> characteristicResults;

	/**
	 * Store parameters into instance variables.
	 * 
	 * @param result
	 *            the result of matching the criterion to the patient's history
	 *            or <code>null</code> if undetermined
	 * @param type
	 *            the type of criterion
	 * @param text
	 *            the localized text of the criterion
	 * @param qualifier
	 *            the localized qualifier or <code>null</code> if none
	 * @param characteristicResults
	 *            the localized patient characteristics and their results that
	 *            factor into the match or <code>null</code> for none
	 */
	public CriterionProfile(Result result, Type type, String text, String qualifier,
			List<CharacteristicResult> characteristicResults)
	{
		this.result = result;
		this.type = type;
		this.text = text;
		this.qualifier = qualifier;
		this.characteristicResults = characteristicResults;
	}

	/**
	 * @return the result of matching the criterion to the patient's history or
	 *         <code>null</code> if undetermined
	 */
	public Result getResult()
	{
		return result;
	}

	/**
	 * @return the type of criterion
	 */
	public Type getType()
	{
		return type;
	}

	/**
	 * @return the localized text of the criterion
	 */
	public String getText()
	{
		return text;
	}

	/**
	 * @return the localized qualifier or <code>null</code> if none
	 */
	public String getQualifier()
	{
		return qualifier;
	}

	/**
	 * @return the localized patient characteristics and their results that
	 *         factor into the match or <code>null</code> for none
	 */
	public List<CharacteristicResult> getCharacteristicResults()
	{
		return characteristicResults;
	}

	public boolean isWarning()
	{
		return (result == Result.FAIL) || (result == Result.FAIL_SUBJECTIVE);
	}

	/**
	 * Version uid for serializable class
	 */
	private static final long serialVersionUID = -898799536698046942L;
}
