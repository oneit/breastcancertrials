/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANATOMIC_SITE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREAST_CANCER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.METASTATIC;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IV;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Contains convenience methods to extract diagnosis information from a
 * patient's history.
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 */
public abstract class DiagnosisMatchable extends Matchable
{
	/**
	 * Returns the diagnosis.
	 * 
	 * @param history
	 *            the patient's history
	 * @param location
	 *            the location
	 * @return the diagnosis or <code>null</code> if there is no diagnosis
	 */
	public static Diagnosis getDiagnosis(PatientHistory history, CharacteristicCode location)
	{
		if ((history == null) || (location == null))
			return null;
		// Find the location within the diagnosis history
		for (Diagnosis diagnosis : history.getDiagnoses())
			if (location.equals(diagnosis.getValueHistory().getValue(ANATOMIC_SITE)))
				return diagnosis;
		return null;
	}

	/**
	 * @return whether the patient history indicates metastatic breast cancer
	 * @param history
	 *            the patient's history
	 */
	public static boolean isMetastatic(PatientHistory history)
	{
		//ignore metastatic for avatars unless stage IV is set or no stage is set.
		if (history != null && 
			history.isAvatar() && 
			history.getStage() != null &&
			!STAGE_IV.equals(history.getStage()))
		{
			return false;
		}
		return (history != null) && (METASTATIC.equals(history.getValueHistory().getValue(BREAST_CANCER)));
	}

	/**
	 * Returns the value characteristic in a diagnosis
	 * 
	 * @param diagnosis
	 *            the diagnosis
	 * @param characteristic
	 *            the diagnosis' value characteristic
	 * @return the value characteristic in a diagnosis <code>null</code> if
	 *         there is no diagnosis or no characteristic
	 */
	public static CharacteristicCode getDiagnosisValue(Diagnosis diagnosis, CharacteristicCode characteristic)
	{
		return ((diagnosis == null) || (characteristic == null)) ? null : diagnosis.getValueHistory().getValue(
				characteristic);
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = 356445628866309307L;
}
