package org.quantumleaphealth.screen;

import java.util.List;
import java.util.ResourceBundle;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * A simple implementation of the Matchable interface allowing an unconditional no match for a trial.
 * 
 * @author wgweis 3/17/2014
 *
 */
public class DoNotMatchMatchable extends Matchable
{
	/**
	 * For serialization 
	 */
	private static final long serialVersionUID = 4079044097151623821L;
	
	CharacteristicCode doMatch;

	public DoNotMatchMatchable()
	{
		this(BreastCancerCharacteristicCodes.DONOTMATCH);
	}
	
	public DoNotMatchMatchable(CharacteristicCode doMatch)
	{
		this.doMatch = doMatch;
	}

	/**
	 * This method is not relevant for this matchable type.
	 */
	@Override
	public List<CharacteristicResult> getCharacteristicResults(PatientHistory history, ResourceBundle resourceBundle) 
	{
		return null;
	}

	/**
	 * The match will always fail if this trial has a Matchable of this type and the characteristic code is set to NEGATIVE.
	 */
	@Override
	public Result matchDescriptive(PatientHistory history) 
	{
		if (BreastCancerCharacteristicCodes.DONOTMATCH.equals(doMatch))
		{
			return Result.FAIL;
		}
		return Result.PASS;
	}
	
	@Override
	public String toString()
	{
		return "DoNotMatchMatchable with Do Not Match set to " + (BreastCancerCharacteristicCodes.AFFIRMATIVE.equals(doMatch)? "TRUE" : "NEGATIVE");
	}
}
