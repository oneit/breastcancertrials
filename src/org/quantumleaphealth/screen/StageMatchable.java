/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LEFT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RIGHT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_0;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_I;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_II;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIA;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIB;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIC;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_III;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIIA;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIIB;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIIC;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_III_SET;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_II_SET;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IV;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_I_III;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.model.trial.AssociativeCriterion.SetOperation;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Matches a patient's cancer stage diagnosis characteristics to stage
 * eligibility criterion. This class offers two complex features on top of the
 * usual matching algorithm:
 * <ol>
 * <li>handling criterion-characteristic mismatch.
 * <li>handling bilateral idiosyncrasies
 * </ol>
 * <p>
 * Criterion-Characteristic Mismatch
 * </p>
 * The patient characteristic allows only major cancer stages, e.g., Stage 0, I,
 * II, III, IV. The eligibility criterion allows sub-stages such as Stage IIA,
 * IIB, etc. This class uses subjective matching to support this mismatch. If a
 * particular criterion is coded for any of the major stages then it is mapped
 * to an <i>objective</i> set. Match results will either be pass or fail. If a
 * particular criterion is coded for a sub-stage then that sub-stage is mapped
 * to a <i>subjective</i> set. Match results will be either subjective or fail.
 * For example, a criterion specifying Stage IIB is mapped as a subjective Stage
 * II. If a patient indicates Stage II then the match returns a subjective
 * (rather than pass or fail) match.
 * <p>
 * Bilateral idiosyncrasies
 * </p>
 * To support combinations of left breast and right breast diagnoses that may
 * have dissimilar stages, this class weighs heavily towards subjective results.
 * If the left diagnosis matches the criterion but the right one fails then this
 * class will return a subjective match. The intention is to reduce
 * false-negative matches as much as possible.
 * 
 * @author Tom Bechtold
 * @version 2008-11-10
 */
public class StageMatchable extends DiagnosisMatchable
{

	/** 
	 * BreastCancerCharacteristics codes could be part of a 1.5 enum, now, but they are not.  
	 * This is a hack to get around the current implementation.
	 */
	public static final HashMap<CharacteristicCode, String> StageDescriptive = new HashMap<CharacteristicCode, String>();
	{
		StageDescriptive.put(UNSURE, "\"I'm not sure\"");
		StageDescriptive.put(STAGE_0, "DCIS");
		StageDescriptive.put(STAGE_I, "STAGE I");
		StageDescriptive.put(STAGE_II, "STAGE II");
		StageDescriptive.put(STAGE_IIA, "STAGE IIA");
		StageDescriptive.put(STAGE_IIB, "STAGE IIB");
		StageDescriptive.put(STAGE_IIC, "STAGE IIC");
		StageDescriptive.put(STAGE_III, "STAGE III");
		StageDescriptive.put(STAGE_IIIA, "STAGE IIIA");
		StageDescriptive.put(STAGE_IIIB, "STAGE IIIB");;
		StageDescriptive.put(STAGE_IIIC, "STAGE IIIC");
		StageDescriptive.put(STAGE_IV, "STAGE IV");
		StageDescriptive.put(STAGE_I_III, "STAGE I - III");
	}
	
	/**
	 * Whether or not to invert a pass/fail result
	 */
	private final boolean invert;
	/**
	 * The criterion's requirement that can be directly applied to the patient's
	 * characteristic. The choices that are supported by the patient's
	 * characteristic:
	 * <ol>
	 * <li>Stage 0
	 * <li>Stage I
	 * <li>Stage II
	 * <li>Stage III
	 * <li>Stage IV
	 * </ol>
	 * For avatars gradations of Stage II and Stage are also allowed.
	 * <ol>
	 * <li>Stage IIA
	 * <li>Stage IIB
	 * <li>Stage IIC
	 * <li>Stage IIIA
	 * <li>Stage IIIB
	 * <li>Stage IIIC
	 * </ol>
	 */
	private final Set<CharacteristicCode> objective;
	/**
	 * The criterion's requirement that can not be directly applied to the
	 * patient's characteristic. The criterion's requirement maps cancer
	 * sub-stages to their major stages:
	 * <ol>
	 * <li>Stage IIA,B,C: Stage II
	 * <li>Stage IIIA, B, C: Stage III
	 * </ol>
	 * In addition, this set should contain the "I'm not sure" element. This
	 * mask represents those cancer stages that the patient does not answer,
	 * such as Stage IIA, Stage IIB etc.
	 */
	private final Set<CharacteristicCode> subjective;

	/**
	 * Validates the parameter and calculates the instance variables. This
	 * constructor sets the members of the two sets depending upon what is in
	 * the requirement.
	 * 
	 * @param stageCriterion
	 *            the stage eligibility criterion
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code> or is not a stage
	 *             criterion or the set operator is not <code>INTERSECT</code>
	 *             or a stage requirement is not supported
	 */
	public StageMatchable(AssociativeCriterion stageCriterion) throws IllegalArgumentException
	{
		if ((stageCriterion == null) || !STAGE.equals(stageCriterion.getParent())
				|| (stageCriterion.getSetOperation() != SetOperation.INTERSECT)
				|| (stageCriterion.getRequirement() == null))
			throw new IllegalArgumentException("criterion is not for stage: " + stageCriterion);
		this.invert = stageCriterion.isInvert();
		// Assign requirements into either those that the patient can answer or
		// those that the patient can not answer.
		// Therefore map Stage II sub-stages (A, B, C) to Stage II; likewise for
		// Stage III
		objective = new HashSet<CharacteristicCode>(stageCriterion.getRequirement().length);
		subjective = new HashSet<CharacteristicCode>(stageCriterion.getRequirement().length);
		// A patient answer of "I'm not sure" is always subjective.
		subjective.add(UNSURE);
		for (CharacteristicCode requirement : stageCriterion.getRequirement())
		{
			if (STAGE_0.equals(requirement) || STAGE_I.equals(requirement) || STAGE_II.equals(requirement)
					|| STAGE_III.equals(requirement) || STAGE_IV.equals(requirement))
				objective.add(requirement);
			else if (STAGE_I_III.equals(requirement))
			{
				// Add each member of the aggregator: I, II, III
				objective.add(STAGE_I);
				objective.add(STAGE_II);
				objective.add(STAGE_III);
				objective.addAll(Arrays.asList(STAGE_II_SET));
				objective.addAll(Arrays.asList(STAGE_III_SET));
			}
			else if (requirement.isMember(STAGE_II_SET))
				subjective.add(STAGE_II);
			else if (requirement.isMember(STAGE_III_SET))
				subjective.add(STAGE_III);
			else
				throw new IllegalArgumentException("Cannot interpret stage " + requirement);
		}
	}

	/**
	 * Returns how the patient's diagnoses match to the stage requirement. This
	 * method first matches metastatic (Stage IV) patients. If non-metastatic
	 * and the two lateral results are available yet unequal then the result is
	 * <code>PASS_SUBJECTIVE</code>. The result is post-processed by
	 * <code>invertResult()</code>.
	 * 
	 * @return how the patient's diagnoses match to the stage requirement
	 * @param history
	 *            the patient's history
	 * @see org.quantumleaphealth.screen.Matchable#match(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	public Result matchDescriptive(PatientHistory history)
	{
		// Validate parameter and check metastatic status
		if (history == null)
			return Result.FAIL;
		if (isMetastatic(history))
			return invertResult(match(STAGE_IV));

		// If left and right breast results differ then return PASS_SUBJECTIVE
		Result left = match(getStage(history, LEFT_BREAST));
		Result right = match(getStage(history, RIGHT_BREAST));
		if (left == null)
			return invertResult(right);
		if (right == null)
			return invertResult(left);
		if (left != right)
			return Result.PASS_SUBJECTIVE;
		return invertResult(left);
	}

	/**
	 * Returns how a stage diagnosis matches the requirement. This method
	 * supports objective stages (such as 0, I, II, III) and subjective stages
	 * (IIA, IIB, IIIA). Objective takes precedence over subjective.
	 * 
	 * @param stage
	 *            the stage diagnosis or 0 for no diagnosis
	 * @return how a stage diagnosis matches the requirement or
	 *         <code>null</code> for no diagnosis
	 */
	private Result match(CharacteristicCode stage)
	{
		// If no stage data then return null
		if (stage == null)
			return null;
		// Pass result takes precedence over Subjective result
		if (objective.contains(stage))
			return Result.PASS;
		if (subjective.contains(stage))
			return Result.PASS_SUBJECTIVE;
		// If not pass or subjective then return failure
		return Result.FAIL;
	}

	/**
	 * Returns the stage for a lateral diagnosis.
	 * 
	 * @param history
	 *            the patient's history
	 * @param location
	 *            the lateral location
	 * @return the stage for a lateral diagnosis or <code>null</code> if there
	 *         is no diagnosis
	 */
	private CharacteristicCode getStage(PatientHistory history, CharacteristicCode location)
	{
		return getDiagnosisValue(getDiagnosis(history, location), STAGE);
	}

	/**
	 * Returns the post-processed result depending upon the invert. This method
	 * fails if the result is not known and doesn't invert if the result is
	 * pass-subjective.
	 * 
	 * @param result
	 *            the result to post-process
	 * @return <code>FAIL</code> if <code>result</code> is <code>null</code>,
	 *         unchanged if <code>result</code> is <code>PASS_SUBJECTIVE</code>,
	 *         <code>result.invert()</code> if <code>invert</code> is
	 *         <code>true</code>, unchanged otherwise
	 */
	private Result invertResult(Result result)
	{
		if (result == null)
			return Result.FAIL;
		if (result == Result.PASS_SUBJECTIVE)
			return result;
		if (invert)
			return result.invert();
		return result;
	}

	/**
	 * Localized description(s) of the stage characteristics. This method
	 * examines each lateral diagnosis.
	 * 
	 * @param history
	 *            the patient's history
	 * @param resourceBundle
	 *            unused
	 * @return localized description(s) of the stage characteristics
	 * @see org.quantumleaphealth.screen.Matchable#getCharacteristic(org.quantumleaphealth.model.patient.PatientHistory,
	 *      java.util.ResourceBundle)
	 */
	public List<CharacteristicResult> getCharacteristicResults(PatientHistory history, ResourceBundle resourceBundle)
	{
		// Check metastatic
		if (isMetastatic(history))
			return Arrays.asList(new CharacteristicResult("Stage IV", matchDescriptive(history)));
		StringBuilder builder = new StringBuilder();
		addCharacteristicDiagnosis(builder, getStage(history, LEFT_BREAST), true);
		addCharacteristicDiagnosis(builder, getStage(history, RIGHT_BREAST), false);
		// If none then display none
		if (builder.length() == 0)
			builder.append("Stage not available");
		return Arrays.asList(new CharacteristicResult(builder.toString(), matchDescriptive(history)));
	}

	/**
	 * Adds a localized description for a lateral diagnosis. This method adds
	 * either
	 * <ul>
	 * <li>"I'm not sure"</li>
	 * <li>Stage n</li>
	 * </ul>
	 * It does nothing if the <code>builder</code> or <code>stage</code> are
	 * <code>null</code>.
	 * 
	 * @param builder
	 *            the builder to add the description to
	 * @param stage
	 *            the stage
	 * @param left
	 *            whether the diagnosis is for left breast or right
	 */
	private static void addCharacteristicDiagnosis(StringBuilder builder, CharacteristicCode stage, boolean left)
	{
		if (builder == null)
			return;
		String description = null;
		if (StageDescriptive.containsKey(stage))
		{
			description = StageDescriptive.get(stage);
		}
		else
			return;
		if (builder.length() != 0)
			builder.append("; ");
		builder.append(left ? "Left breast: " : "Right breast: ");
		builder.append(description);
	}

	/**
	 * Verion uid for serializable class
	 */
	private static final long serialVersionUID = 2042794975591370696L;
}
