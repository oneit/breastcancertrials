/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AVATAR_DIAGNOSISAREA_NONE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.quantumleaphealth.model.patient.AvatarHistory;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.EligibilityCriterion;
import org.quantumleaphealth.model.trial.EligibilityCriterion.Type;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.screen.Matchable.Result;

/**
 * Screens cached eligibility against a patient's history. This class converts
 * eligibility criteria from declarative into imperative form and makes it
 * available for two types of matching:
 * <ul>
 * <li>Definitive: pass or fail result for the criteria as a whole</li>
 * <li>Descriptive: a subjective result for each criterion</li>
 * </ul>
 * <i>Note: a criterion in <b>declarative</b> form specifies what to match; the
 * same criterion in <b>imperative</b> form describes how to match it against a
 * patient's history.</i>
 * 
 * @author Tom Bechtold
 * @version 2009-03-10
 */
public class DeclarativeMatchingEngine implements MatchingEngine, Serializable
{
	/**
	 * Logger for unsupported criteria
	 */
	private final Log log = LogFactory.getLog(DeclarativeMatchingEngine.class);
	/**
	 * The mapping between declarative and imperative criteria. To promote reuse
	 * this assignment should be changed to use a factory pattern, e.g.,
	 * <code>ConverterFactory.getInstance()</code>
	 */
	private static final DeclarativeToImperativeConverter CONVERTER = new BreastCancerConverter();

	/**
	 * The imperative form of the declarative <code>EligibilityCriterion</code>
	 * class.
	 */
	private class ImperativeCriterion implements Serializable
	{
		/**
		 * The type
		 */
		private final EligibilityCriterion.Type type;
		/**
		 * The text
		 */
		private final String text;
		/**
		 * The qualifier
		 */
		private final String qualifier;
		/**
		 * The criterion represented in imperative form
		 */
		private final Matchable matchable;

		/**
		 * Extract instance variables from parameter. If the criterion is not
		 * supported then log a warning message.
		 * 
		 * @param eligibilityCriterion
		 *            the declarative eligibility criterion
		 * @param primaryID
		 *            the trial's primary identifier; optionally used for
		 *            logging unsupported criteria
		 */
		private ImperativeCriterion(EligibilityCriterion eligibilityCriterion, String primaryID)
		{
			if (eligibilityCriterion == null)
				throw new IllegalArgumentException("must specify eligibility criterion");
			this.type = eligibilityCriterion.getType();
			this.text = eligibilityCriterion.getText();
			this.qualifier = eligibilityCriterion.getQualifier();
			Matchable convertedMatchable = null;
			if ((this.type != null) && (eligibilityCriterion.getCriterion() != null))
				try
				{
					convertedMatchable = CONVERTER.convert(eligibilityCriterion.getCriterion());
				}
				catch (UnsupportedCriterionException unsupportedCriterionException)
				{
					log.warn("trial " + primaryID + " is skipping unsupported " + this.type.toString().toLowerCase()
							+ " criterion " + unsupportedCriterionException.getMessage());
				}
			this.matchable = convertedMatchable;
		}

		private Result matchResult(PatientHistory patientHistory)
		{
			// Only consider inclusion and exclusion
			if (!Type.REQUIRE.equals(type) && !Type.EXCLUDE.equals(type))
				return Result.PASS;
			
			if (matchable == null)
				return Result.PASS;

			// Treatment is ignored for matching purposes for Avatars.
			if (matchable.avatarIgnorable(patientHistory))
			{	
				return Result.AVATAR_SUBJECTIVE;
			}
			return matchable.matchDescriptive(patientHistory);
		}
		
		/**
		 * Returns the eligibility profile for this criterion
		 * 
		 * @param patientHistory
		 *            the patient's history to match
		 * @return the eligibility profile for this criterion
		 */
		private CriterionProfile getCriterionProfile(PatientHistory patientHistory)
		{
			// Unsupported criterion has null result and characteristic
			if (matchable == null)
				return new CriterionProfile(null, type, text, qualifier, null);
			
			Result result = null;
			// Apply subjectivity or voluntary to result
			// Treatment matches are not considered for avatars.
			if (matchable.avatarIgnorable(patientHistory))
			{
				result = Result.AVATAR_SUBJECTIVE;
			}
			else
			{	
				result = matchable.matchDescriptive(patientHistory);
			}	
			if (type != null)
				switch (type)
				{
				case SUBJECTIVE:
					if (result == Result.FAIL)
						result = Result.FAIL_SUBJECTIVE;
					else if (result == Result.PASS)
						result = Result.PASS_SUBJECTIVE;
					break;
				case VOLUNTARY:
					result = Result.PASS;
					break;
				}
			// Return descriptive result
			return new CriterionProfile(result, type, text, qualifier, matchable.getCharacteristicResults(
					patientHistory, PROFILEBUNDLE));
		}

		/**
		 * Version UID for serializable class
		 */
		private static final long serialVersionUID = -257779623388375409L;
	}

	/**
	 * Contains localized text for characteristics in eligibility profile
	 */
	private static final ResourceBundle PROFILEBUNDLE;
	static
	{
		ResourceBundle bundle = null;
		try
		{
			bundle = ResourceBundle.getBundle("EligibilityCriteria");
		}
		catch (MissingResourceException missingResourceException)
		{
		}
		PROFILEBUNDLE = bundle;
	}

	/**
	 * The trials that this engine screens patients' histories for sorted in
	 * ascending order on <code>Trial.id</code>, guaranteed to be non-
	 * <code>null</code>
	 */
	private final List<Trial> sortedTrialData;

	/**
	 * @return the profilebundle
	 */
	public static ResourceBundle getProfilebundle() 
	{
		return PROFILEBUNDLE;
	}
	
	/**
	 * Associates a declarative trial with its imperative eligibility criteria
	 */
	private class ImperativeTrial implements Serializable
	{
		/**
		 * The represented trial
		 */
		private final Trial trial;
		/**
		 * The eligibility criteria represented in imperative form
		 */
		private final List<ImperativeCriterion> imperativeCriteria = new LinkedList<ImperativeCriterion>();

		/**
		 * Stores the declarative instance variable and converts its eligibility
		 * criteria into imperative form.
		 * 
		 * @param trial
		 *            the trial whose data is stored and converted
		 */
		public ImperativeTrial(Trial trial)
		{
			// Validate and store scalar fields
			if (trial == null)
				throw new IllegalArgumentException("trial not specified");
			this.trial = trial;
			String primaryID = trial.getPrimaryID();
			// Convert each criterion from declarative to imperative form
			for (EligibilityCriterion eligibilityCriterion : trial.getEligibilityCriteria())
				imperativeCriteria.add(new ImperativeCriterion(eligibilityCriterion, primaryID));
		}

		/**
		 * Returns whether a patient's history matches the trial's eligibility.
		 * 
		 * @param patientHistory
		 *            the patient's history to match
		 * @return whether a patient's history matches the trial's eligibility
		 */
		public boolean matchDefinitive(PatientHistory patientHistory)
		{
			boolean nonAvatarResultFound = false;
			
			// If null or empty criteria, no match.
			if (imperativeCriteria == null || imperativeCriteria.size() < 1)
				return false;

			// No criterion may fail
			for (ImperativeCriterion imperativeCriterion : imperativeCriteria)
			{	
				Result result = imperativeCriterion.matchResult( patientHistory );

				// this won't do anything unless the debug variable in the ReportOutput class is set to true.
				ReportOutput.addOutput("  Match result is " + (result == null? Result.FAIL: result) + " for imperativeCriterion " + imperativeCriterion.text + System.getProperty("line.separator"));

				if ( result == null || result == Result.FAIL )
				{	
					return false;
				}
				
				if ( result != Result.AVATAR_SUBJECTIVE )
				{
					nonAvatarResultFound = true;
				}
			}	
			return nonAvatarResultFound ? true: false;
		}

		/**
		 * Returns the result of a descriptive match with a patient's history.
		 * 
		 * @param patientHistory
		 *            the patient's history to match
		 * @param description
		 *            describes the profile or <code>null</code> if not
		 *            available
		 * @return the result of a descriptive match with a patient's history
		 */
		public EligibilityProfile getEligibilityProfile(PatientHistory patientHistory, String description)
		{
			EligibilityProfile eligibilityProfile = new EligibilityProfile(matchDefinitive(patientHistory), description);
			// Add a descriptive match result for each criterion regardless of
			// type
			for (ImperativeCriterion imperativeCriterion : imperativeCriteria)
				eligibilityProfile.addCriterionProfile(imperativeCriterion.getCriterionProfile(patientHistory));
			return eligibilityProfile;
		}

		/**
		 * Version UID for serializable class
		 */
		private static final long serialVersionUID = -6622663983332095936L;
	}

	/**
	 * The trials that this engine screens patients' histories for sorted in
	 * ascending order on <code>Trial.id</code>, guaranteed to be non-
	 * <code>null</code>. Because it is accessed sequentially it is implemented
	 * as a linked list.
	 */
	private final List<ImperativeTrial> trials;

	/**
	 * The date and time that the trials were last modified or <code>null</code>
	 * if unknown
	 */
	private final Date lastModified;
	/**
	 * The date when the trials were last modified in the registry or
	 * <code>null</code> if unknown
	 */
	private final Date lastRegistered;
	/**
	 * The date and time when the trials were loaded from persistent storage,
	 * guaranteed to be non-<code>null</code>
	 */
	private final Date loaded;

	/**
	 * Iterates over trials that match a patient's history sorted in ascending
	 * order on <code>Trial.id</code>.
	 */
	private class DeclarativePatientMatchingIterator implements PatientHistoryMatchingIterator, Serializable
	{
		/**
		 * Iterates over all trials
		 */
		private final Iterator<ImperativeTrial> iterator;
		/**
		 * The patient's history
		 */
		private final PatientHistory patientHistory;
		
		/**
		 * Avatar configuration to be used for matching multiple stages.
		 */
		private final AvatarHistory avatarHistory;

		private final String patientHistoryBackup; 

		/**
		 * Stores the parameters into instance variables
		 * 
		 * @param patientHistory
		 *            the patient's history
		 * @param trials
		 *            iterates over all trials
		 * @throws IllegalArgumentException
		 *             if either parameter is <code>null</code>
		 */
		private DeclarativePatientMatchingIterator(PatientHistory patientHistory, List<ImperativeTrial> trials, AvatarHistory avatarHistory)
				throws IllegalArgumentException
		{
			if (patientHistory == null)
				throw new IllegalArgumentException("patient history must be specified");
			if (trials == null)
				throw new IllegalArgumentException("list of trials must be specified");
			this.patientHistory = patientHistory;
			this.avatarHistory = avatarHistory;
			this.iterator = trials.iterator();
			this.patientHistoryBackup = patientHistory.getPatientHistoryAsString(patientHistory);
		}

		/**
		 * Returns the next trial that matches the patient's history in
		 * ascending order of <code>Trial.id</code> or <code>null</code> if
		 * there are no more matching trials
		 * 
		 * @return the next trial that matches the patient's history or
		 *         <code>null</code> if there are no more matching trials
		 * @see org.quantumleaphealth.screen.PatientHistoryMatchingIterator#nextMatch()
		 */
		public Trial nextMatch()
		{
			while (iterator.hasNext())
			{
				ImperativeTrial imperativeTrial = iterator.next();
				if (patientHistory.isAvatar() && avatarHistory != null && !avatarHistory.getAvatarStageCharacteristics().isEmpty())
				{
					for (CharacteristicCode stage : avatarHistory.getAvatarStageCharacteristics()) 
					{
						ReportOutput.addOutput("Avatar Checking for stage " + (StageMatchable.StageDescriptive.containsKey(stage) ? StageMatchable.StageDescriptive.get(stage): stage));
						PatientHistory avatarPatientHistory = PatientHistory.getPatientHistoryFromString(patientHistoryBackup);
						avatarPatientHistory.setUserType(UserPatient.UserType.AVATAR_PROFILE);
						avatarPatientHistory.injectStageHistory(stage);
						
						if (avatarHistory.containsAttribute(DIAGNOSISAREA, AVATAR_DIAGNOSISAREA_NONE))
						{
							avatarPatientHistory.addAttribute(DIAGNOSISAREA, AVATAR_DIAGNOSISAREA_NONE);
						}
						
						if (imperativeTrial.matchDefinitive(avatarPatientHistory)) 
						{
							return imperativeTrial.trial;
						}
					}
				}
				// if we get here, then either this is a normal patient or this is an avatar with no stages defined.
				else if (imperativeTrial.matchDefinitive(patientHistory))
				{	
					return imperativeTrial.trial;
				}	
			}
			// At the end
			return null;
		}

		/**
		 * Version UID for serializable class
		 */
		private static final long serialVersionUID = -3177882478255167233L;
	}

	/**
	 * Compares trials' identifiers.
	 */
	private static final Comparator<Trial> TRIALIDENTIFIERCOMPARATOR = new Comparator<Trial>()
	{
		/**
		 * @return the difference between the trials' identifiers
		 * @param trial1
		 *            the first trial
		 * @param trial2
		 *            the second trial
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Trial trial1, Trial trial2)
		{
			return (int) (((trial1 == null) || (trial1.getId() == null) ? 0l : trial1.getId()) - ((trial2 == null)
					|| (trial2.getId() == null) ? 0l : trial2.getId()));
		}
	};

	/**
	 * Loads imperative trial eligibility into the engine using the current
	 * date/time.
	 * 
	 * @param trialData
	 *            the trials to load
	 */
	public DeclarativeMatchingEngine(List<Trial> trialData )
	{
		// Call overloaded constructor
		this(trialData, null );
	}

	/**
	 * Loads imperative trial eligibility into the engine with a particular load
	 * date/time.
	 * 
	 * @param trialData
	 *            the trials to load
	 * @param loaded
	 *            the date and time when the trials were loaded from persistent
	 *            storage or <code>null</code> for the current date/time
	 */
	public DeclarativeMatchingEngine(List<Trial> trialData, Date loaded )
	{
		this.loaded = loaded == null ? new Date() : loaded;
		// Ensure the list is not null, sort by identifier and restrict
		// reordering
		if (trialData == null)
			trialData = Collections.emptyList();
		else
			Collections.sort(trialData, TRIALIDENTIFIERCOMPARATOR);
		sortedTrialData = Collections.unmodifiableList(trialData);
		this.trials = new LinkedList<ImperativeTrial>();
		Date lastModified = null;
		Date lastRegistered = null;
		for (Trial trial : sortedTrialData)
		{
			trials.add(new ImperativeTrial(trial));
			if ((lastModified == null)
					|| ((trial.getLastModified() != null) && trial.getLastModified().after(lastModified)))
				lastModified = trial.getLastModified();
			if ((lastRegistered == null)
					|| ((trial.getLastRegistered() != null) && trial.getLastRegistered().after(lastRegistered)))
				lastRegistered = trial.getLastRegistered();
		}
		this.lastModified = lastModified;
		this.lastRegistered = lastRegistered;
	}

	/**
	 * Returns an iterator of trials that match a patient's history sorted in
	 * ascending order on <code>Trial.id</code>. This method creates an instance
	 * of the inner class with the patient's history and the loaded trial
	 * eligibility.
	 * 
	 * @return an iterator of trials that match a patient's history sorted in
	 *         ascending order on <code>Trial.id</code>, guaranteed to be non-
	 *         <code>null</code>
	 * @param patientHistory
	 *            the patient's history
	 * @see org.quantumleaphealth.screen.MatchingEngine#getMatchingIterator(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	public PatientHistoryMatchingIterator getMatchingIterator(PatientHistory patientHistory, AvatarHistory avatarHistory)
	{
		return new DeclarativePatientMatchingIterator(patientHistory, trials, avatarHistory);
	}

	/**
	 * Returns the result of a descriptive match between a trial and a patient
	 * history
	 * 
	 * @param trialIdentifier
	 *            the trial's unique identifier
	 * @param patientHistory
	 *            the patient's history
	 * @param description
	 *            describes the profile or <code>null</code> if not available
	 * @return the result of a descriptive match between a trial and a patient
	 *         history or <code>null</code> if the identifier does not match a
	 *         loaded trial
	 * @see org.quantumleaphealth.screen.MatchingEngine#getEligibilityProfile(java.lang.Long,
	 *      org.quantumleaphealth.model.patient.PatientHistory)
	 */
	public EligibilityProfile getEligibilityProfile(Long trialIdentifier, PatientHistory patientHistory,
			String description)
	{
		// Validate parameters
		if ((trialIdentifier == null) || (patientHistory == null))
			return null;
		for (ImperativeTrial imperativeTrial : trials)
			if (trialIdentifier.equals(imperativeTrial.trial.getId()))
				return imperativeTrial.getEligibilityProfile(patientHistory, description);
		// Identifier does not match a loaded trial
		return null;
	}

	/**
	 * @return the loaded trials sorted by <code>id</code>, guaranteed to be
	 *         non-<code>null</code>
	 * @see org.quantumleaphealth.screen.MatchingEngine#getTrialIterator()
	 */
	public Iterator<Trial> getTrialIterator()
	{
		return sortedTrialData.iterator();
	}

	/**
	 * @return the trial or <code>null</code> if trial not found
	 * @param trialIdentifier
	 *            the trial's unique identifier
	 * @see org.quantumleaphealth.screen.MatchingEngine#getTrial(java.lang.Long)
	 */
	public Trial getTrial(Long trialIdentifier)
	{
		// Validate parameter
		if (trialIdentifier == null)
			return null;
		// Use binary search to find the trial
		Trial key = new Trial();
		key.setId(trialIdentifier);
		int index = Collections.binarySearch(sortedTrialData, key, TRIALIDENTIFIERCOMPARATOR);
		return (index < 0) ? null : sortedTrialData.get(index);
	}

	/**
	 * @return the number of loaded trials
	 * @see org.quantumleaphealth.screen.MatchingEngine#getTrialCount()
	 */
	public int getTrialCount()
	{
		return sortedTrialData.size();
	}

	/**
	 * @return the date and time when the trials were last modified or
	 *         <code>null</code> if unknown
	 * @see org.quantumleaphealth.screen.MatchingEngine#getLastModified()
	 */
	public Date getLastModified()
	{
		return lastModified;
	}

	/**
	 * @return the date when the trials were last modified in the registry or
	 *         <code>null</code> if unknown
	 * @see org.quantumleaphealth.screen.MatchingEngine#getLastRegistered()
	 */
	public Date getLastRegistered()
	{
		return lastRegistered;
	}

	/**
	 * @return the date and time when the trials were loaded from persistent
	 *         storage, guaranteed to be non-<code>null</code>
	 * @see org.quantumleaphealth.screen.MatchingEngine#getLoaded()
	 */
	public Date getLoaded()
	{
		return loaded;
	}

	private Hashtable<String, Integer> categoryCounts = null;
	private ArrayList <String> categoryCountsKeys = null;
	
	private Hashtable<String, Integer> getCategoryCounts()
	{
		if ( categoryCounts == null )
		{
			categoryCounts = new Hashtable<String, Integer>();
			for ( Trial trial: sortedTrialData )
			{
				if ( !categoryCounts.containsKey( trial.getType().name() ))
				{
					categoryCounts.put( trial.getType().name(), 1 );
				}
				else
				{
					categoryCounts.put( trial.getType().name(), categoryCounts.get( trial.getType().name() ) + 1 );
				}
			}
		}
		return categoryCounts;
	}

	@Override
	public List<String> getCategoryCountsKeys()
	{
		if ( categoryCountsKeys == null )
		{
			categoryCountsKeys = new ArrayList<String>();
			for ( String key : getCategoryCounts().keySet() )
			{
				categoryCountsKeys.add( key );
			}
			Collections.sort(categoryCountsKeys);
		}
		return categoryCountsKeys;
	}

	@Override
	public Integer getCategoryCountEntry( String key )
	{
		return getCategoryCounts().get( key );
	}

	/**
	 * Version uid for serializable class
	 */
	private static final long serialVersionUID = 5632288404882103598L;
}
