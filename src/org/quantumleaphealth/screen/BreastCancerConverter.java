/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANDROGENRECEPTOR;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANDROGENRECEPTOR_NEGATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANDROGENRECEPTOR_POSITIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.MEASURABLE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PDL1;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PDL1_NEGATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PDL1_POSITIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PI3K;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PI3K_NEGATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PI3K_POSITIVE;

import java.io.Serializable;
import java.util.LinkedList;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.*;

import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.model.trial.BooleanCriterion;
import org.quantumleaphealth.model.trial.ComparativeCriterion;
import org.quantumleaphealth.model.trial.Criteria;
import org.quantumleaphealth.model.trial.Criterion;
import org.quantumleaphealth.model.trial.DoNotMatchCriterion;
import org.quantumleaphealth.model.trial.TreatmentProcedureCriterion;
import org.quantumleaphealth.model.trial.TreatmentRegimenCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Maps declarative eligibility criteria to imperative matchable objects for
 * breast cancer criteria.
 * 
 * @author Tom Bechtold
 * @version 2008-07-08
 */
public class BreastCancerConverter implements DeclarativeToImperativeConverter, Serializable
{

	/**
	 * List of unsupported comparative criteria TODO: Implement Gail and Claus
	 * once prevention form is ready
	 */
	private static final CharacteristicCode[] UNSUPPORTED_COMPARATIVE_CRITERION = { CREATININE, CREATININE_CLEARANCE,
			BILIRUBIN, NEUTROPHIL, PLATELET_COUNT, GAIL_SCORE, CLAUS_MODEL };

	/**
	 * The list of characteristic codes of associative criteria that are found
	 * in the patient's <code>valueHistory</code> field and do not use
	 * aggregators.
	 */
	private static final CharacteristicCode[] VALUEHISTORY = { GENDER, MENOPAUSAL, MENOPAUSE, BRCA1TEST, BRCA2TEST,
			ECOG_ZUBROD, LYMPHEDEMA};

	/**
	 * The list of characteristic codes of associative criteria that are found
	 * in the patient's <code>attributeHistory</code> field and do not use
	 * aggregators.
	 */
	private static final CharacteristicCode[] ATTRIBUTEHISTORY = { HISPANIC, RACE, PRIMARYCANCER, HEMATOPOIETIC, AUTOIMMUNE, PULMONARY,
			CARDIOVASCULAR, RENAL, NEUROPSYCHIATRIC, HORMONAL, GYNECOLOGIC, DIAGNOSISAREA };
	// agg: BREASTMETASTASIS_BRAIN

	/**
	 * The list of characteristic codes of boolean criteria that are found in
	 * the patient's <code>binaryHistory</code> field.
	 */
	private static final CharacteristicCode[] BINARYHISTORY = { PARTICIPATION_CURRENT, HISPANIC, ASHKENAZI_JEWISH, HIV,
			DIABETES, OSTEOPOROSIS, PREGNANCY, NURSING };
	/**
	 * The list of characteristic codes of boolean criteria that are stored in
	 * the patient's <code>valueHistory</code> map under the
	 * <code>BREAST_CANCER</code> key.
	 */
	private static final CharacteristicCode[] BINARYCATEGORY = { SURVIVOR, RECURRENT_LOCALLY, BILATERAL_ASYNCHRONOUS };

	/**
	 * The choices of treatment length (1-based bitwise mask) that objectively
	 * qualify a chemotherapy regimen. Incomplete chemotherapy regimes whose
	 * length matches this mask will be counted objectively; those that do not
	 * match will be counted subjectively. This value is set to 4 or more
	 * cycles, e.g., the fourth choice presented to the patient.
	 */
	private static final byte THRESHOLD_CHEMOTHERAPY = 1 << 4;

	/**
	 * Converts a criterion from declarative to imperative form so that it may
	 * be matched to a patient's history. This method supports the following
	 * subclasses:
	 * <ul>
	 * <li><code>Criteria</code>
	 * <li><code>TreatmentRegimenCriterion</code>
	 * <li><code>AssociativeCriterion</code>
	 * <li><code>BooleanCriterion</code>
	 * <li><code>ComparativeCriterion</code>
	 * </ul>
	 * 
	 * @param criterion
	 *            the declarative criterion to convert
	 * @return an imperative criterion
	 * @see org.quantumleaphealth.screen.DeclarativeToImperativeConverter#createMatchable(org.quantumleaphealth.model.trial.Criterion)
	 * @throws UnsupportedCriterionException
	 *             if <code>criterion</code> is <code>null</code> or it is a
	 *             criteria but has wrong number of children or the criterion is
	 *             not supported
	 */
	public Matchable convert(Criterion criterion) throws UnsupportedCriterionException
	{
		if (criterion == null)
			throw new UnsupportedCriterionException();

		if (criterion instanceof Criteria)
		{
			Criteria criteria = (Criteria) (criterion);
			if (criteria.getChildren() == null)
				throw new UnsupportedCriterionException(criteria, "no children");
			LinkedList<Matchable> children = new LinkedList<Matchable>();
			for (Criterion child : criteria.getChildren())
				children.add(convert(child));
			return new CriteriaMatchable(criteria.getOperator(), children.toArray(new Matchable[children.size()]));
		}
		else if (criterion instanceof TreatmentProcedureCriterion)
		{
			TreatmentProcedureCriterion treatmentProcedureCriterion = (TreatmentProcedureCriterion) (criterion);
			if (SURGERY.equals(treatmentProcedureCriterion.getParent()))
				return new TreatmentSurgeryProcedureMatchable(treatmentProcedureCriterion);
			if (RADIATION.equals(treatmentProcedureCriterion.getParent()))
				return new TreatmentRadiationProcedureMatchable(treatmentProcedureCriterion);
		}
		else if (criterion instanceof TreatmentRegimenCriterion)
		{
			TreatmentRegimenCriterion treatmentRegimenCriterion = (TreatmentRegimenCriterion) (criterion);
			CharacteristicCode type = treatmentRegimenCriterion.getParent();
			if (CHEMOTHERAPY.equals(type))
				return new TreatmentRegimenMatchable(treatmentRegimenCriterion, CHEMOTHERAPY_AGGREGATORS,
						THRESHOLD_CHEMOTHERAPY);
			if (ENDOCRINE.equals(type))
				return new TreatmentRegimenMatchable(treatmentRegimenCriterion, ENDOCRINE_AGGREGATORS, (byte) 0);
			if(BIOLOGICAL.equals(type))
				return new TreatmentRegimenMatchable(treatmentRegimenCriterion, BIOLOGICAL_AGGREGATORS, (byte) 0);
			if (BISPHOSPHONATE.equals(type))
				return new TreatmentRegimenMatchable(treatmentRegimenCriterion, null, (byte) 0);
		}
		else if (criterion instanceof AssociativeCriterion)
		{
			AssociativeCriterion associativeCriterion = (AssociativeCriterion) (criterion);
			CharacteristicCode characteristicCode = associativeCriterion.getParent();
			// Diagnostic criteria have their own classes
			if (ESTROGENRECEPTOR.equals(characteristicCode))
				return new DiagnosisMarkerMatchable(associativeCriterion, ESTROGENRECEPTOR_POSITIVE,
						ESTROGENRECEPTOR_NEGATIVE, ESTROGENRECEPTOR_KNOWN, ESTROGENRECEPTOR_INDETERMINATE,
						ESTROGENRECEPTOR_NOTTESTED);
			if (PROGESTERONERECEPTOR.equals(characteristicCode))
				return new DiagnosisMarkerMatchable(associativeCriterion, PROGESTERONERECEPTOR_POSITIVE,
						PROGESTERONERECEPTOR_NEGATIVE, PROGESTERONERECEPTOR_KNOWN, PROGESTERONERECEPTOR_INDETERMINATE,
						PROGESTERONERECEPTOR_NOTTESTED);
			if (HER2NEUDIAGNOSIS.equals(characteristicCode))
				return new DiagnosisMarkerMatchable(associativeCriterion, HER2NEUDIAGNOSIS_POSITIVE,
						HER2NEUDIAGNOSIS_NEGATIVE, HER2NEUDIAGNOSIS_KNOWN, HER2NEUDIAGNOSIS_INDETERMINATE,
						HER2NEUDIAGNOSIS_NOTTESTED);
			
			if (PDL1.equals(characteristicCode))
				return new DiagnosisNewMarkerMatchable(associativeCriterion, PDL1_POSITIVE,
						PDL1_NEGATIVE);
			if (ANDROGENRECEPTOR.equals(characteristicCode))
				return new DiagnosisNewMarkerMatchable(associativeCriterion, ANDROGENRECEPTOR_POSITIVE,
						ANDROGENRECEPTOR_NEGATIVE);
			if (PI3K.equals(characteristicCode))
				return new DiagnosisNewMarkerMatchable(associativeCriterion, PI3K_POSITIVE,
						PI3K_NEGATIVE);
			// Breast diagnosis, stage, tumor stage and brain mets have their
			// own classes
			if (DIAGNOSISBREAST.equals(associativeCriterion.getParent()))
				return new BreastDiagnosisAttributeAssociativeMatchable(associativeCriterion);
			if (STAGE.equals(associativeCriterion.getParent()))
				return new StageMatchable(associativeCriterion);
			if (TUMOR_STAGE_FINDING.equals(associativeCriterion.getParent()))
				return new TumorStageAssociativeMatchable(associativeCriterion);
			if (BREASTMETASTASIS_BRAIN.equals(associativeCriterion.getParent()))
				return new BreastMetastasisBrainValueAssociativeMatchable(associativeCriterion);
			// Non-aggregated value-stored criteria
			if (characteristicCode.isMember(VALUEHISTORY))
				return new ValueAssociativeMatchable(associativeCriterion);
			// Aggregated value-stored criteria
			if (HORMONE_REPLACEMENT_THERAPY.equals(characteristicCode))
				return new ValueAssociativeMatchable(associativeCriterion, HORMONE_REPLACEMENT_THERAPY_AGGREGATORS);
			// Non-aggregated attribute-stored criteria
			if (characteristicCode.isMember(ATTRIBUTEHISTORY))
				return new AttributeAssociativeMatchable(associativeCriterion);
			// Aggregated attribute-stored criteria
			if (DIGESTIVE.equals(characteristicCode))
				return new AttributeAssociativeMatchable(associativeCriterion, DIGESTIVE_AGGREGATORS);
			// Unsupported criteria
			// TODO: Implement ala DIAGNOSISBREAST once prevention form is
			// ready; TB 2008-06-20
			// TODO: Split out Lobular Carcinoma In Situ and Atypical Ductal
			// Hyperplasia from breast cancer types?
			if (PRIOR_BREAST_DISEASE.equals(characteristicCode))
				return new UnsupportedCharacteristicMatchable();
		}
		else if (criterion instanceof BooleanCriterion)
		{
			BooleanCriterion booleanCriterion = (BooleanCriterion) (criterion);
			CharacteristicCode characteristicCode = booleanCriterion.getCharacteristicCode();
			// Most criteria are found in the patient's binary history
			if (characteristicCode == null)
				throw new UnsupportedCriterionException(criterion);
			if (characteristicCode.isMember(BINARYHISTORY))
				return new BooleanMatchable(booleanCriterion);
			if (characteristicCode.isMember(BINARYCATEGORY))
				return new CategoryBooleanMatchable(booleanCriterion);
			if (BILATERAL_SYNCHRONOUS.equals(characteristicCode))
				return new BilateralBooleanMatchable(booleanCriterion);
			if (INFLAMMATORY.equals(characteristicCode))
				return new InflammatoryBooleanMatchable(booleanCriterion);
			if (MEASURABLE.equals(characteristicCode))
				return new BooleanMatchable(booleanCriterion);
		}
		else if (criterion instanceof DoNotMatchCriterion)
		{
			return new DoNotMatchMatchable();
		}
		else if (criterion instanceof ComparativeCriterion)
		{
			ComparativeCriterion comparativeCriterion = (ComparativeCriterion) (criterion);
			// Laboratory criteria are not supported in patient history
			// TODO: Remove Gail and Claus scores from UNSUPPORTED array when
			// prevention form is ready; TB 2008-06-20
			CharacteristicCode characteristicCode = comparativeCriterion.getCharacteristicCode();
			for (CharacteristicCode unsupported : UNSUPPORTED_COMPARATIVE_CRITERION)
				if (unsupported.equals(characteristicCode))
					return new UnsupportedCharacteristicMatchable();
			// Each declarative comparative criterion has its own imperative
			// class
			if (AGE.equals(characteristicCode))
				return new AgeMatchable(comparativeCriterion);
			if (LYMPH_NODE_COUNT.equals(characteristicCode))
				return new LymphNodeCountMatchable(comparativeCriterion);
		}
		// Not supported
		throw new UnsupportedCriterionException(criterion);
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -8890531875772258142L;
}
