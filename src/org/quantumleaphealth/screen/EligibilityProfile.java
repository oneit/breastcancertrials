/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * Result of a descriptive match between a trial and a patient's history.
 * 
 * @author Tom Bechtold
 * @version 2008-10-09
 */
public class EligibilityProfile implements Comparable<EligibilityProfile>, Serializable
{
	/**
	 * Whether the trial definitively matches the patient's history
	 */
	private final boolean matchDefinitive;
	/**
	 * Describes this profile or <code>null</code> if none available
	 */
	private final String description;
	/**
	 * Results of descriptive matching of each eligibility criterion
	 */
	private final List<CriterionProfile> criteria = new LinkedList<CriterionProfile>();

	/**
	 * Store parameters into instance variables.
	 * 
	 * @param matchDefinitive
	 *            whether the trial matches the patient's history
	 * @param description
	 *            describes this profile or <code>null</code> if none available
	 */
	public EligibilityProfile(boolean matchDefinitive, String description)
	{
		this.matchDefinitive = matchDefinitive;
		this.description = description;
	}

	/**
	 * @return whether the trial definitively matches the patient's history
	 */
	public boolean isMatchDefinitive()
	{
		return matchDefinitive;
	}

	/**
	 * @return the description or <code>null</code> if none available
	 */
	public String getDescription()
	{
		return description;
	}

	/**
	 * @return results of descriptive matching of each eligibility criterion
	 */
	public List<CriterionProfile> getCriteria()
	{
		return criteria;
	}

	/**
	 * Adds a descriptive match between a criterion and patient history to the
	 * list
	 * 
	 * @param criterionProfile
	 *            match between a criterion and patient history
	 */
	public void addCriterionProfile(CriterionProfile criterionProfile)
	{
		if (criterionProfile == null)
			throw new IllegalArgumentException("must specify criterion profile");
		criteria.add(criterionProfile);
	}

	/**
	 * @return how the descriptions compare or 0 if both descriptions are
	 *         unknown
	 * @param other
	 *            the other eligibility profile
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(EligibilityProfile other)
	{
		// Known data is always sorted before unknown
		if (other == null)
			return -1;
		if ((description == null) && (other.description == null))
			return 0;
		if (other.description == null)
			return -1;
		if (description == null)
			return 1;
		return description.trim().compareTo(other.description.trim());
	}

	/**
	 * Version uid for serializable class
	 */
	private static final long serialVersionUID = -3034033421766483912L;
}
