/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.Therapy;
import org.quantumleaphealth.model.patient.YearMonth;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.*;

import org.quantumleaphealth.model.trial.TreatmentRegimenCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.ontology.OntologyUtils;

/**
 * Enables a limited requirement to be matchable against a patient's treatment
 * history.
 * 
 * @author Tom Bechtold
 * @version 2009-07-09
 */
public class TreatmentRegimenMatchable extends Matchable
{
	/**
	 * The criterion
	 */
	private final TreatmentRegimenCriterion criterion;
	/**
	 * The agents to match to or <code>null</code> to match to any agent
	 */
	private final Set<CharacteristicCode> agents;
	/**
	 * Whether or not to limit therapies to current ones. <code>true</code>
	 * means limit to current therapies; <code>false</code> means limit to
	 * completed therapies; <code>null</code> means do not limit.
	 */
	private final Boolean concurrent;
	/**
	 * Whether or not to limit therapies to ones with tumor progression
	 */
	private final boolean progression;
	/**
	 * Whether or not to limit therapies to recent diagnosis
	 */
	private final boolean mostRecent;
	/**
	 * The treatment lengths (bit-wise mask) for a therapy to count. Therapies
	 * that match this mask will be counted objectively; those that do not will
	 * be counted subjectively.
	 */
	private final int threshold;

	/**
	 * The minimum number of weeks for each choice of treatment length. The
	 * patient's choices are:
	 * <ol>
	 * <li>1 month or less
	 * <li>2-3 months
	 * <li>4-12 months
	 * <li>1 year or more
	 * </ol>
	 * Bit #0 is reserved for "I'm not sure".
	 */
	private static final int[] LENGTHTEMPORAL_WEEKS = { 0, 4, 13, 52 };

	/**
	 * The string to use if the localized text cannot be found in the resource
	 * bundle
	 */
	private static final String UNKNOWN_KEY = "???";

	/**
	 * Validates and stores the requirements into instance variables.
	 * 
	 * @param criterion
	 *            the criterion that is matched to patients' histories
	 * @throws IllegalArgumentException
	 *             if <code>criterion</code> is <code>null</code>
	 * @throws UnsupportedCriterionException
	 *             if the therapy type is not chemo, endocrine or bisphosphonate
	 *             or a qualifier is not supported
	 */
	public TreatmentRegimenMatchable(TreatmentRegimenCriterion criterion,
			Map<CharacteristicCode, CharacteristicCode[]> aggregators, byte thresholdCycles)
			throws IllegalArgumentException, UnsupportedCriterionException
	{
		// Validate parameter
		if ((criterion == null) || (criterion.getParent() == null))
			throw new IllegalArgumentException("must specify criterion and its parent");
		this.criterion = criterion;

		// Expand agents using aggregators
		agents = OntologyUtils.expand(criterion.getRequirement(), aggregators);
		// Store qualifiers instance variables; conversion to list so we can
		// check for unsupported qualifiers
		CharacteristicCode[] set = criterion.getQualifiers();
		LinkedList<CharacteristicCode> qualifiers = (set == null) ? new LinkedList<CharacteristicCode>()
				: new LinkedList<CharacteristicCode>(Arrays.asList(set));
		// Concurrent's value has three options: concurrent (true), completed
		// (false) or neither (null)
		boolean isConcurrent = qualifiers.remove(CONCURRENT);
		boolean isCompleted = qualifiers.remove(COMPLETED);
		concurrent = isConcurrent ? Boolean.TRUE : (isCompleted ? Boolean.FALSE : null);
		progression = qualifiers.remove(PROGRESSING);
		mostRecent = qualifiers.remove(MOSTRECENT);
		if (qualifiers.size() > 0)
			throw new UnsupportedCriterionException(criterion, "qualifiers " + qualifiers);

		// Threshold (bit-wise mask) is fixed if given and calculated otherwise
		if (thresholdCycles > 0)
			threshold = thresholdCycles;
		else
		{
			// Determine minimum number of weeks for a regimen to count
			int requiredWeeks = criterion.getThreshold();
			CharacteristicCode unitOfMeasure = criterion.getThresholdUnitOfMeasure();
			if (unitOfMeasure == null)
				requiredWeeks = 0;
			else if (MONTH.equals(unitOfMeasure))
				requiredWeeks *= 4;
			else if (YEAR.equals(unitOfMeasure))
				requiredWeeks *= 52;
			// Build bitwise mask by comparing requirement to patient choices
			// (1-based)
			int requiredMask = 0;
			for (int index = 0; index < LENGTHTEMPORAL_WEEKS.length; index++)
				if (LENGTHTEMPORAL_WEEKS[index] >= requiredWeeks)
					requiredMask |= 1 << (index + 1);
			threshold = requiredMask;
		}
	}

	/**
	 * Compares objective and subjective counts of filtered therapy history to
	 * minimum or maximum requirement.
	 * 
	 * @return how the patient history matches to the therapy requirement
	 * @param history
	 *            the patient's history
	 * @see org.quantumleaphealth.screen.Matchable#matchDescriptive(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	public Result matchDescriptive(PatientHistory history)
	{
		return getResult(getResults(history), history);
	}

	/**
	 * Compares objective and subjective counts of filtered therapy history to
	 * minimum or maximum requirement.
	 * 
	 * @return how the patient history matches to the therapy requirement
	 * @param results
	 *            the result for each therapy
	 */
	private Result getResult(Map<Therapy, Result> results, PatientHistory history)
	{
		if (results == null)  // put the avatar thing here.
		{	
			if ( history != null && history.isAvatar() )
			{
				return Result.AVATAR_SUBJECTIVE;
			}
			return Result.FAIL;
		}	
		int objective = 0, subjective = 0;
		for (Map.Entry<Therapy, Result> result : results.entrySet())
			if (result.getValue() == Result.PASS)
				objective++;
			else if (result.getValue() != Result.FAIL)
				subjective++;

		// Compare counts to requirement
		if (criterion.isInvert())
		{
			// Maximum of 0 (none): fail takes precedence
			if (criterion.getCount() == 0)
			{
				if (objective > 0)
					return Result.FAIL;
				if (subjective > 0)
					return Result.FAIL_SUBJECTIVE;
				return Result.PASS;
			}
			// Maximum of n: treat edge condition as subjective by increasing
			// limit
			if (objective > (criterion.getCount() + 1))
				return Result.FAIL;
			if (objective == (criterion.getCount() + 1))
				return Result.FAIL_SUBJECTIVE;
			if ((objective + subjective) > criterion.getCount())
				return Result.FAIL_SUBJECTIVE;
			
			return history.isAvatar()? Result.AVATAR_SUBJECTIVE : Result.PASS;
		}
		// Minimum limit less than one makes no sense
		int count = Math.max(criterion.getCount(), 1);
		if (objective >= count)
			return Result.PASS;
		if ((objective + subjective) >= count)
			return Result.PASS_SUBJECTIVE;
		
		return history.isAvatar()? Result.AVATAR_SUBJECTIVE : Result.FAIL;
	}

	/**
	 * Returns how each therapy matches the requirements.
	 * 
	 * @param history
	 *            the patient's history
	 * @return the therapies and their matching results or <code>null</code> if
	 *         <code>history</code> is <code>null</code>
	 */
	public Map<Therapy, Result> getResults(PatientHistory history)
	{
		if (history == null)
			return null;
		Set<Therapy> therapies = history.getTherapies();
		Map<Therapy, Result> results = new LinkedHashMap<Therapy, Result>(therapies.size(), 1.0f);

		// Setup threshold for start year if most recent diagnosis requirement
		// is set
		YearMonth diagnosisYearMonth = mostRecent ? history.getYearMonthHistory().get(DIAGNOSISBREAST) : null;

		// Consider only therapies of the same type
		for (Therapy therapy : therapies)
		{	
			if (criterion.getParent().equals(therapy.getType()))
			{	
				results.put(therapy, getResult(therapy, diagnosisYearMonth));
			}
		}	
		return results;
	}

	/**
	 * Returns how a therapy matches the criterion. This method tests each
	 * criterion field except <code>invert</code>:
	 * <ul>
	 * <li><code>agents</code> and <code>setOperation</code>: matched to therapy
	 * <code>agents</code></li>
	 * <li><code>settings</code>: matched to therapy <code>setting</code></li>
	 * <li><code>progression</code>: matched to therapy <code>progression</code>
	 * and subjectively to <code>incomplete</code></li>
	 * <li><code>mostrecent</code>: compares <code>diagnosisYearMonth</code> to
	 * therapy <code>started</code></li>
	 * <li><code>concurrent</code>: matched to therapy <code>status</code></li>
	 * <li><code>threshold</code>: matched to therapy <code>length</code>
	 * subjectively</li>
	 * </ul>
	 * 
	 * @param therapy
	 *            the therapy
	 * @param diagnosisYearMonth
	 *            the diagnosis date or <code>null</code> if criterion does not
	 *            specify most recent diagnosis
	 * @return how a therapy matches the criterion
	 */
	private Result getResult(Therapy therapy, YearMonth diagnosisYearMonth)
	{
		// Validate parameter
		if (therapy == null)
			return Result.FAIL;

		// If none of the following conditions apply then the result passes
		Result result = Result.PASS;

		// Match agents and settings
		if (!AttributeAssociativeMatchable.isMatch(therapy.getAgents(), agents, criterion.getSetOperation(), false))
			return Result.FAIL;
		if ((criterion.getSettings() != null) && (criterion.getSettings().length != 0)
				&& ((therapy.getSetting() == null) || !therapy.getSetting().isMember(criterion.getSettings())))
			return Result.FAIL;

		// Progression checks boolean field absolutely; incomplete field
		// supports subjectivity through UNSURE answer
		if (progression)
		{
			// TODO: As of July 9, 2009, progression is not asked on the patient
			// form, so do not include it in algorithm
			// if (!therapy.isProgression()) return Result.FAIL;
			// The definition of progression for this algorithm is progression
			// during the treatment, not afterwards
			CharacteristicCode incomplete = therapy.getIncomplete();
			if (UNSURE.equals(incomplete))
				result = result.worse(Result.PASS_SUBJECTIVE);
			else if (!RECURRENT_LOCALLY.equals(therapy.getIncomplete()))
				return Result.FAIL;
		}

		// Most recent diagnosis: compare therapy start year to diagnosis year
		// Filter most recent diagnosis: compare therapy start year to diagnosis
		// year
		if ((diagnosisYearMonth != null) && (therapy.getStarted() != null)
				&& therapy.getStarted().isEarlier(diagnosisYearMonth))
			return Result.FAIL;

		// Skip therapies depending upon concurrency
		boolean therapyConcurrent = CONCURRENT.equals(therapy.getStatus());
		if (Boolean.TRUE.equals(concurrent) && !therapyConcurrent)
			return Result.FAIL;
		if (Boolean.FALSE.equals(concurrent) && therapyConcurrent)
			return Result.FAIL;

		// If the treatment did not complete normally and the length does not
		// match then count it subjectively
		if (!COMPLETED.equals(therapy.getStatus()) && CHEMOTHERAPY.equals(therapy.getType())  && (((1 << therapy.getLength()) & threshold) == 0))
			result = result.worse(Result.PASS_SUBJECTIVE);

		return result;
	}

	/**
	 * Localized description(s) of how each therapy matches the criterion.
	 * 
	 * @param history
	 *            the patient's history
	 * @param resourceBundle
	 *            contains localized descriptions
	 * @return localized description(s) of how each therapy matches the
	 *         criterion
	 * @see org.quantumleaphealth.screen.Matchable#getCharacteristic(org.quantumleaphealth.model.patient.PatientHistory,
	 *      java.util.ResourceBundle)
	 */
	public List<CharacteristicResult> getCharacteristicResults(PatientHistory history, ResourceBundle resourceBundle)
	{
		// Get results for each therapy
		Map<Therapy, Result> results = getResults(history);
		Result result = getResult(results, history);
		if ((results == null) || results.isEmpty())
			return Arrays.asList(new CharacteristicResult("No "
					+ getString(resourceBundle, criterion.getParent().toString()), result));

		// Add each therapy to the applicable, temporal or unapplicable lists
		StringBuilder applicable = new StringBuilder();
		StringBuilder temporal = new StringBuilder();
		StringBuilder unapplicable = new StringBuilder();
		for (Map.Entry<Therapy, Result> entry : results.entrySet())
		{
			StringBuilder builder = null;
			if (entry.getValue() == null)
				throw new IllegalStateException("Unknown therapy match result");
			switch (entry.getValue())
			{
			case PASS:
				builder = applicable;
				break;
			case PASS_SUBJECTIVE:
				builder = temporal;
				break;
			case FAIL:
				builder = unapplicable;
				break;
			default:
				throw new IllegalStateException("Invalid therapy match result " + entry.getValue());
			}
			if (builder.length() > 0)
				builder.append(", ");
			Therapy therapy = entry.getKey();
			if ((therapy.getAgents() != null) && (therapy.getAgents().length != 0))
			{
				builder.append(getString(resourceBundle, criterion.getParent().toString() + '.'
						+ therapy.getAgents()[0].toString()));
				for (int index = 1; index < therapy.getAgents().length; index++)
					builder.append('+').append(
							getString(resourceBundle, criterion.getParent().toString() + '.'
									+ therapy.getAgents()[index].toString()));
			}
			if (therapy.getSetting() != null)
				builder.append(" in ").append(
						getString(resourceBundle, SETTING.toString() + '.' + therapy.getSetting())).append(" setting");
			append(builder, " started ", therapy.getStarted());
			append(builder, " and completed ", therapy.getCompleted());
		}
		if (applicable.length() != 0)
			applicable.insert(0, "Applicable therapy(s): ");
		if (temporal.length() != 0)
			applicable.append((applicable.length() != 0 ? "; Temporal therapy(s): " : "Temporal therapy(s): ")
					+ temporal);
		if (unapplicable.length() != 0)
			applicable.append((applicable.length() != 0 ? "; Unapplicable therapy(s): " : "Unapplicable therapy(s): ")
					+ unapplicable);
		return Arrays.asList(new CharacteristicResult(applicable.toString(), result));
	}

	/**
	 * Returns a localized string
	 * 
	 * @param resourceBundle
	 *            contains localized strings
	 * @param key
	 *            the key used to find the string
	 * @return a localized string or <tt>???</tt><code>key</code><tt>???</tt> if
	 *         <code>key</code> is not found in <code>resourceBundle</code>
	 */
	private static String getString(ResourceBundle resourceBundle, String key)
	{
		if ((resourceBundle == null) || (key == null))
			return UNKNOWN_KEY;
		try
		{
			return resourceBundle.getString(key);
		}
		catch (MissingResourceException missingResourceException)
		{
			return UNKNOWN_KEY + key + UNKNOWN_KEY;
		}
	}

	/**
	 * Appends a description of a date. This method only prints dates with a
	 * valid year. It will also print any valid month.
	 * 
	 * @param builder
	 *            the builder to append to
	 * @param prefix
	 *            the optional prefix to append before the description
	 * @param yearMonth
	 *            the date to append or <code>null</code> to not append anything
	 */
	private static void append(StringBuilder builder, String prefix, YearMonth yearMonth)
	{
		if ((builder == null) || (yearMonth == null) || (yearMonth.getYear() == null)
				|| (yearMonth.getYear().shortValue() < 0))
			return;
		if (prefix != null)
			builder.append(prefix);
		if (yearMonth.getMonth() > 0)
			builder.append(yearMonth.getMonth()).append('/');
		builder.append(yearMonth.getYear());
	}

	/**
	 * Version uid for serializable class
	 */
	private static final long serialVersionUID = 2584544097235435867L;
}
