/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AVATAR_STAGEIII_LOCAL_SPREAD_OF_DISEASE_SET;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AVATAR_STAGEIV_SPREAD_OF_DISEASE_SET;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_0;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_I;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_II;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIA;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIB;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIC;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_III;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIIA;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIIB;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IIIC;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE_IV;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AVATAR_NOLOCAL_SPREAD_DISEASE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AVATAR_DIAGNOSISAREA_NONE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AVATAR_DIAGNOSISAREA_ANY;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ASSOCIATIVE_ATTRIBUTE_IGNORABLE;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.model.trial.AssociativeCriterion.SetOperation;
import org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Enables a set of characteristics to be matchable against a single value in a
 * patient's history. TODO: Patient language for characteristics?
 * 
 * @author Tom Bechtold
 * @version 2008-07-07
 */
public class AttributeAssociativeMatchable extends ValueAssociativeMatchable
{
	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -848162923633265963L;

	/**
	 * Store the instance variable using no aggregators
	 * 
	 * @param criterion
	 *            the criterion to represent
	 * @throws IllegalArgumentException
	 *             if <code>criterion</code> is <code>null</code>
	 */
	public AttributeAssociativeMatchable(AssociativeCriterion criterion) throws IllegalArgumentException
	{
		super(criterion);
	}

	/**
	 * Store the instance variable and expand the requirement using aggregators.
	 * 
	 * @param criterion
	 *            the criterion to represent
	 * @param aggregators
	 *            optional aggregators that may make up the criterion's
	 *            requirement
	 * @throws IllegalArgumentException
	 *             if <code>criterion</code> is <code>null</code>
	 */
	public AttributeAssociativeMatchable(AssociativeCriterion criterion,
			Map<CharacteristicCode, CharacteristicCode[]> aggregators) throws IllegalArgumentException
	{
		super(criterion, aggregators);
	}

	/**
	 * Returns how the patient's attribute characteristic matches to the
	 * associative requirement. This method's algorithm then depends upon the
	 * <code>comparator</code>:
	 * <ul>
	 * <li><code>SUBSET</code>: <code>true</code> if each characteristic in the
	 * characteristic is also present in the requirement
	 * <li><code>SUPERSET</code>: <code>true</code> if each characteristic in
	 * the requirement is also present in the characteristic
	 * <li><code>INTERSECT</code> (default): <code>true</code> if any
	 * characteristic is present in both the characteristic and the requirement.
	 * </ul>
	 * Finally, the invert is applied to the result.
	 * 
	 * @return how the characteristic matches the requirement
	 * @see org.quantumleaphealth.screen.Matchable#match(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	public Result matchDescriptive(PatientHistory history)
	{
		if ( history != null && history.isAvatar() && !avatarContainsAnyCharacteristic(history, isOther(history)))
		{	
			return Result.AVATAR_SUBJECTIVE;
		}
		
		// Unsure and Other are acceptable choices for some attribute characteristics
		if (history != null && isIgnorable(history))
		{
			return Result.PASS;
		}
		// Other is specified if an "other" string is stored for the parent
		// characteristic
		return (criterion.isInvert() ^ isMatch(getAttribute(history), requirement, isOther(history))) ? Result.PASS
				: Result.FAIL;
	}

	/**
	 * Patient attribute characteristics of UNSURE or SKIP are acceptable for some attribute requirements such as
	 * Race/Ethnicity or the Hispanic/Latino Question
	 * 
	 * Modification:  normal matching rules are to apply for SKIP and UNSURE on Hispanic and race/ethnicity.
	 * @see org.quantumleaphealth.ontology#BreastCancerCharacteristicCodes
	 *  
	 * @param history
	 * @return true if this is the case, false otherwise
	 */
	private boolean isIgnorable(PatientHistory history)
	{
		if (ASSOCIATIVE_ATTRIBUTE_IGNORABLE.containsKey(this.criterion.getParent()))
		{
			Set<CharacteristicCode> patient_attributes = history.getAttributeCharacteristics().get(this.criterion.getParent());
			if (patient_attributes == null)
			{
				return true;
			}
			
			for (CharacteristicCode code: ASSOCIATIVE_ATTRIBUTE_IGNORABLE.get(this.criterion.getParent()))
			{
				if (patient_attributes.contains(code))
				{
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * 
	 *  Local Spread of Disease is defined for stage III patients.  There are three values possible:
	 *  DIAGNOSISAREA(27653) = {INFRACLAVICULAR(229743), CHESTWALL(205076), SUPERCLAVICULAR(229730)} 
	 * 
	 *  Evidence of Disease is defined for Stage IV patients.  The following values are defined:
	 *  
	 * 	7.	Avatar rules for Stage IV :
	 *		i.	Always check �Stage IV Evidence of Disease� on the Avatar configuration tab:
	 *			i.	If one or more sites are checked, standard matching rules apply
	 *			ii.	If �NONE� is checked, consider the Avatar to be a Stage IV patient with no �current� evidence of disease; standard matching rules apply
	 *			iii.	If   �no� sites are selected, ignore �evidence of disease� as a field (see Rule 5)
	 *
	 *	8.	Avatar rules for Stage III
	 *		i.	If Stage III is selected, consider �Local Spread� either on the Diagnosis Tab or Avatar Configuration Tab:
	 *			a.	Diagnosis Tab
	 *				i.	Value selected for �local spread� (supra/infraclavicular nodes or chest wall): Normal matching to selected value
	 *				ii.	No value selected: Avatar Pass
	 *			b.	Avatar Configuration Tab
	 *				i.	If Check Box is selected for �no local spread,� CTMatch interprets as a �negative value� for local spread and proceeds per normal rules for matching. 
	 *				ii.	If Check Box is selected for �no local spread,� CTMatch does not provide an Avatar Pass as described in 8aii, even if no values are selected in the Diagnosis Tab.
	 *		ii.	Ignore �Stage IV Evidence of Disease� on the Avatar configuration tab, unless Stage IV is also selected
	 * 
	 * @param history
	 * @param isOther
	 * @return return whether or not the requirement being examined falls under Evidence of Disease.
	 */
	private boolean avatarContainsAnyCharacteristic(PatientHistory history, boolean isOther)
	{
		// get the list 
		Set<CharacteristicCode> characteristics = getAttribute( history );
		if ( characteristics == null )
		{
			return false;
		}
		
		if ((requirement == null) || requirement.isEmpty())
		{
			return characteristics.size() != 0 || isOther;
		}
		
		CharacteristicCode stage = history.getStage();
		for (CharacteristicCode requirementCode: requirement)
		{
			if (DIAGNOSISAREA.equals(criterion.getParent()) && stage != null && criterion != null && criterion.getParent() != null)
			{
				if (stage.equals(STAGE_0) ||
					stage.equals(STAGE_I) ||
					stage.equals(STAGE_II) ||
					stage.equals(STAGE_IIA) ||
					stage.equals(STAGE_IIB) ||
					stage.equals(STAGE_IIC))
				{
					continue;
				}
				else if (stage.equals(STAGE_III) || 
						 stage.equals(STAGE_IIIA) || 
						 stage.equals(STAGE_IIIB) || 
						 stage.equals(STAGE_IIIC))
				{
					if (history.containsAttribute(DIAGNOSISAREA, AVATAR_NOLOCAL_SPREAD_DISEASE))
					{
						return true;  // no local spread will be defined so that trials requiring none will match, while trials requiring something will fail.
					}
					// Stage III ignores all stage IV evidence of disease.
					else if (requirementCode.isMember(AVATAR_STAGEIV_SPREAD_OF_DISEASE_SET))
					{
						continue;
					}
				}
				else if (stage.equals(STAGE_IV))
				{
					// i.	If one or more sites are checked, standard matching rules apply
					// ii.	If NONE is checked, consider the Avatar to be a Stage IV patient with no current evidence of disease; standard matching rules apply
					// iii.	If   no sites are selected, ignore evidence of disease as a field (see Rule 5)
					if (history.containsAttribute(DIAGNOSISAREA, AVATAR_DIAGNOSISAREA_NONE))
					{
						return true;  // no evidence of disease will be defined so that trials requiring none will match, while trials requiring something will fail.
					}

					if (history.containsAttribute(DIAGNOSISAREA, AVATAR_DIAGNOSISAREA_ANY))
					{
						return false;  // always matches a site with evidence of disease requirement.
					}

					// some local spread of disease are ignored for stage IV
					if (requirementCode.isMember(AVATAR_STAGEIII_LOCAL_SPREAD_OF_DISEASE_SET))
					{
						continue;
					}
					else
					{
						// otherwise normal matching rules apply
						return true;
					}
				}
			}	

			if (characteristics.contains(requirementCode )) 
			{
				return true;
			}
		}
		return false;
	}
	
	/**
	 * @param history
	 *            the patient's history
	 * @return the patient's attribute that is stored in its attribute history
	 *         under the criterion's characteristic or <code>null</code> if none
	 */
	protected Set<CharacteristicCode> getAttribute(PatientHistory history)
	{
		return history == null ? null : history.getAttributeCharacteristics().get(criterion.getParent());
	}

	/**
	 * @param history
	 *            the patient's history
	 * @return whether or not an "other" string attribute is stored in the
	 *         patient's history under the criterion's characteristic
	 */
	protected boolean isOther(PatientHistory history)
	{
		return (history == null) ? false : history.getStringCharacteristics().containsKey(criterion.getParent());
	}

	public String toString()
	{
		return "AttributeAssociativeMatchable Parent = " + 
			BreastCancerCharacteristicCodes.getCharacteristicCodeDescriber(criterion.getParent().getCode()) + 
			", requirement set is " + BreastCancerCharacteristicCodes.getCharactersticCodeArrayDescriber(this.requirement);
	}
	/**
	 * Returns how the patient's attribute characteristic matches to the
	 * associative requirement. This method's algorithm then depends upon the
	 * <code>comparator</code>:
	 * <ul>
	 * <li><code>SUBSET</code>: <code>true</code> if each characteristic in the
	 * characteristic is also present in the requirement
	 * <li><code>SUPERSET</code>: <code>true</code> if each characteristic in
	 * the requirement is also present in the characteristic
	 * <li><code>INTERSECT</code> (default): <code>true</code> if any
	 * characteristic is present in both the characteristic and the requirement
	 * 
	 * @param characteristics
	 *            the characteristics
	 * @param requirement
	 *            the requirement
	 * @param isOther
	 *            whether an "other" value was set
	 * @return how the patient's attribute characteristic matches to the
	 *         associative requirement.
	 */
	protected boolean isMatch(Set<CharacteristicCode> characteristics, Set<CharacteristicCode> requirement,
			boolean isOther)
	{
		// Convert set to array
		CharacteristicCode[] array = (characteristics == null) ? null : new LinkedList<CharacteristicCode>(
				characteristics).toArray(new CharacteristicCode[characteristics.size()]);
		return isMatch(array, requirement, criterion.getSetOperation(), isOther);
	}

	/**
	 * Returns how the patient's attribute characteristic matches to the
	 * associative requirement. This method's algorithm then depends upon the
	 * <code>comparator</code>:
	 * <ul>
	 * <li><code>SUBSET</code>: <code>true</code> if each characteristic in the
	 * characteristic is also present in the requirement
	 * <li><code>SUPERSET</code>: <code>true</code> if each characteristic in
	 * the requirement is also present in the characteristic
	 * <li><code>INTERSECT</code> (default): <code>true</code> if any
	 * characteristic is present in both the characteristic and the requirement
	 * 
	 * @param characteristics
	 *            the characteristics
	 * @param requirement
	 *            the requirement
	 * @param isOther
	 *            whether an "other" value was set
	 * @return how the patient's attribute characteristic matches to the
	 *         associative requirement.
	 */
	public static boolean isMatch(CharacteristicCode[] characteristics, Set<CharacteristicCode> requirement,
			SetOperation setOperation, boolean isOther)
	{
		// Empty requirements implies "any", which must use INTERSECT logic: at
		// least one characteristic (including other)
		if ((requirement == null) || requirement.isEmpty())
			return ((characteristics != null) && (characteristics.length != 0)) || isOther;
		switch (setOperation)
		{
		case SUBSET:
			if (characteristics != null)
				for (CharacteristicCode characteristic : characteristics)
					if (!requirement.contains(characteristic))
						return false;
			return true;
		case SUPERSET:
			for (CharacteristicCode req : requirement)
				if ((characteristics == null) || !req.isMember(characteristics))
					return false;
			return true;
		case INTERSECT:
		default:
			if (characteristics != null)
				for (CharacteristicCode characteristic : characteristics)
					if (requirement.contains(characteristic))
						return true;
			return false;
		}
	}

	/**
	 * Localized description of the characteristic
	 * 
	 * @param history
	 *            the patient's history
	 * @param resourceBundle
	 *            contains localized descriptions
	 * @return localized description of the characteristic
	 * @see org.quantumleaphealth.screen.Matchable#getCharacteristic(org.quantumleaphealth.model.patient.PatientHistory,
	 *      java.util.ResourceBundle)
	 */
	public List<CharacteristicResult> getCharacteristicResults(PatientHistory history, ResourceBundle resourceBundle)
	{
		Set<CharacteristicCode> attributes = getAttribute(history);
		StringBuilder builder = new StringBuilder();
		if ((attributes == null) || attributes.isEmpty())
			builder.append("No/t ").append(getString(criterion.getParent().toString(), resourceBundle));
		else
			for (CharacteristicCode attribute : attributes)
			{
				if (builder.length() > 0)
					builder.append(", ");
				builder
						.append(getString(criterion.getParent().toString() + '.' + attribute.toString(), resourceBundle));
			}
		return Arrays.asList(new CharacteristicResult(builder.toString(), matchDescriptive(history)));
	}
}
