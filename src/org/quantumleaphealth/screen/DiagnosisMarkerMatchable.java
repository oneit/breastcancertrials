/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.model.trial.AssociativeCriterion.SetOperation;
import org.quantumleaphealth.ontology.CharacteristicCode;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREAST_CANCER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.METASTATIC_BREAST_CANCER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LEFT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RIGHT_BREAST;

/**
 * Matches a patient's marker diagnosis characteristics to an eligibility
 * criterion. Dissimilar bilateral diagnoses result in a pass-subjective result.
 * 
 * @author Tom Bechtold
 * @version 2008-07-08
 */
public class DiagnosisMarkerMatchable extends DiagnosisMatchable
{
	/**
	 * The type of marker
	 */
	private final CharacteristicCode parent;
	/**
	 * The elements of the criterion's requirement that can be directly applied
	 * to the patient's characteristic. The choices that are supported by the
	 * patient's characteristic:
	 * <ul>
	 * <li>Positive
	 * <li>Negative
	 * <li>Known (Positive and Negative)
	 * </ul>
	 */
	private final Set<CharacteristicCode> objective;
	/**
	 * The unclear/indeterminate characteristic that results in a subjective
	 * result.
	 */
	private final CharacteristicCode indeterminate;
	/**
	 * The not tested characteristic that results in a subjective result.
	 */
	private final CharacteristicCode notTested;
	/**
	 * Whether or not to invert a pass/fail result
	 */
	private final boolean invert;

	/**
	 * Validates the parameter and calculates the instance variables. This
	 * constructor sets the bits of the objective mask depending upon what is
	 * set in the requirement.
	 * 
	 * @param markerCriterion
	 *            the marker eligibility criterion
	 * @param positive
	 *            represents a positive marker test result
	 * @param negative
	 *            represents a negative marker test result
	 * @param known
	 *            represents a known (positive or negative) marker test result
	 * @param indeterminate
	 *            represents an indeterminate marker test result
	 * @param notTested
	 *            represents an untested status
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code> or the set operator is
	 *             not <code>INTERSECT</code> or a requirement is not supported
	 */
	public DiagnosisMarkerMatchable(AssociativeCriterion markerCriterion, CharacteristicCode positive,
			CharacteristicCode negative, CharacteristicCode known, CharacteristicCode indeterminate,
			CharacteristicCode notTested) throws IllegalArgumentException
	{
		if ((markerCriterion == null) || (markerCriterion.getSetOperation() != SetOperation.INTERSECT)
				|| (markerCriterion.getParent() == null) || (markerCriterion.getRequirement() == null))
			throw new IllegalArgumentException("criterion is not for marker diagnosis: " + markerCriterion);
		if ((positive == null) || (negative == null) || (known == null) || (indeterminate == null)
				|| (notTested == null))
			throw new IllegalArgumentException("characteristic codes not specified");
		this.parent = markerCriterion.getParent();
		this.invert = markerCriterion.isInvert();
		this.indeterminate = indeterminate;
		this.notTested = notTested;

		// For each requirement populate the set that corresponds to the
		// patient's characteristic
		objective = new HashSet<CharacteristicCode>(1, 1f);
		for (CharacteristicCode requirement : markerCriterion.getRequirement())
		{
			if (known.equals(requirement))
			{
				objective.add(positive);
				objective.add(negative);
			}
			else if (positive.equals(requirement) || negative.equals(requirement))
				objective.add(requirement);
			else
				throw new IllegalArgumentException("Cannot interpret marker " + requirement);
		}
	}

	/**
	 * Returns how the patient's diagnoses match to the diagnostic marker
	 * requirement. This method first matches metastatic (Stage IV) patients. It
	 * then returns the better lateral result, post-processed by
	 * <code>invertResult()</code>.
	 * 
	 * @param history
	 *            the patient's history
	 * @return how the patient's diagnosis matches the diagnostic marker
	 *         criterion
	 * @see org.quantumleaphealth.screen.Matchable#matchDescriptive(org.quantumleaphealth.model.patient.PatientHistory)
	 * @see #invertResult(org.quantumleaphealth.screen.Matchable.Result)
	 */
	@Override
	public Result matchDescriptive(PatientHistory history)
	{
		// Validate parameter and check metastatic status
		if (history == null)
			return Result.FAIL;
		if (isMetastatic(history))
		{	
			return invertResult(match(getMarker(history, METASTATIC_BREAST_CANCER)));
		}
				
		// Return the better lateral result
		Result left = match(getMarker(history, LEFT_BREAST));
		Result right = match(getMarker(history, RIGHT_BREAST));
		
		// avatar check.  If nothing was defined return AVATAR_SUBJECTIVE.  isMetastic will return 
		// positivie only if something was defined.
		if ( left == null && right == null && history != null && history.isAvatar() )
		{
			return Result.AVATAR_SUBJECTIVE;
		}
		return invertResult(left == null ? right : left.better(right));
	}

	/**
	 * Returns the marker for a diagnosis.
	 * 
	 * @param history
	 *            the patient's history
	 * @param location
	 *            the location
	 * @return the marker for a diagnosis or <code>null</code> if there is no
	 *         diagnosis
	 */
	private CharacteristicCode getMarker(PatientHistory history, CharacteristicCode location)
	{
		return getDiagnosisValue(getDiagnosis(history, location), parent);
	}

	/**
	 * Returns if a patient's characteristic passes objectively, subjectively or
	 * fails.
	 * 
	 * @param status
	 *            the patient's marker status characteristic
	 * @return <code>PASS</code> if the status characteristic matches the
	 *         objective set, <code>PASS_SUBJECTIVE</code> if the status
	 *         characteristic matches the untested, <code>FAIL_SUBJECTIVE</code>
	 *         if the status characteristic matches the indeterminate or unsure,
	 *         <code>FAIL</code> if it doesn't match either, or
	 *         <code>null</code> if the status characteristic is not available
	 */
	private Result match(CharacteristicCode characteristicCode)
	{
		if (characteristicCode == null)
			return null;
		// Preference: pass, subjective-pass, fail
		if (objective.contains(characteristicCode))
			return Result.PASS;
		if (notTested.equals(characteristicCode))
			return Result.PASS_SUBJECTIVE;
		if (indeterminate.equals(characteristicCode))
			return Result.FAIL_SUBJECTIVE;
		if (UNSURE.equals(characteristicCode))
			return Result.FAIL_SUBJECTIVE;
		return Result.FAIL;
	}

	/**
	 * Returns the post-processed result depending upon the invert. This method
	 * fails if the result is not known and doesn't invert if the result is
	 * pass-subjective.
	 * 
	 * @param result
	 *            the result to post-process
	 * @return <code>FAIL</code> if <code>result</code> is <code>null</code>,
	 *         unchanged if <code>result</code> is <code>PASS_SUBJECTIVE</code>,
	 *         <code>result.invert()</code> if <code>invert</code> is
	 *         <code>true</code>, unchanged otherwise
	 */
	private Result invertResult(Result result)
	{
		if (result == null)
			return Result.FAIL;
		if (result == Result.PASS_SUBJECTIVE)
			return result;
		if (invert)
			return result.invert();
		return result;
	}

	/**
	 * Localized description(s) of the diagnostic marker characteristics. This
	 * method first returns the metastatic status for metastatic patients. If
	 * non-metastatic it combines each lateral diagnosis.
	 * 
	 * @param history
	 *            the patient's history
	 * @param resourceBundle
	 *            unused
	 * @return localized description(s) of the diagnostic marker characteristics
	 * @see org.quantumleaphealth.screen.Matchable#getCharacteristic(org.quantumleaphealth.model.patient.PatientHistory,
	 *      java.util.ResourceBundle)
	 */
	@Override
	public List<CharacteristicResult> getCharacteristicResults(PatientHistory history, ResourceBundle resourceBundle)
	{
		// Check metastatic
		LinkedList<CharacteristicResult> characteristicResults = new LinkedList<CharacteristicResult>();
		if (isMetastatic(history))
		{
			CharacteristicCode markerDiagnosis = getMarker(history, METASTATIC_BREAST_CANCER);
			String characteristic = (markerDiagnosis == null) ? (getString(parent.toString(), resourceBundle) + " not available")
					: getString(parent.toString() + '.' + markerDiagnosis.toString(), resourceBundle);
			characteristicResults.add(new CharacteristicResult(characteristic, matchDescriptive(history)));
			return characteristicResults;
		}
		CharacteristicCode markerDiagnosis = getMarker(history, LEFT_BREAST);
		if (markerDiagnosis != null)
			characteristicResults.add(new CharacteristicResult("Left breast: "
					+ getString(parent.toString() + '.' + markerDiagnosis.toString(), resourceBundle),
					invertResult(match(markerDiagnosis))));
		markerDiagnosis = getMarker(history, RIGHT_BREAST);
		if (markerDiagnosis != null)
			characteristicResults.add(new CharacteristicResult("Right breast: "
					+ getString(parent.toString() + '.' + markerDiagnosis.toString(), resourceBundle),
					invertResult(match(markerDiagnosis))));
		// If none then display none
		if (characteristicResults.size() == 0)
			characteristicResults.add(new CharacteristicResult(getString(parent.toString(), resourceBundle)
					+ " not available", matchDescriptive(history)));
		return characteristicResults;
	}

	/**
	 * Returns the localized text
	 * 
	 * @param key
	 *            the key to lookup the text
	 * @param resourceBundle
	 *            contains localized text
	 * @return the localized text
	 */
	private static final String getString(String key, ResourceBundle resourceBundle)
	{
		try
		{
			return resourceBundle.getString(key);
		}
		catch (Exception exception)
		{
			return "???" + key + "???";
		}
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = 424635887543477685L;
}
