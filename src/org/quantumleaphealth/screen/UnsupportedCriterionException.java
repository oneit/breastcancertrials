/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import org.quantumleaphealth.model.trial.Criterion;

/**
 * Contains a criterion that is not supported.
 * 
 * @author Tom Bechtold
 * @version 2008-02-19
 */
public class UnsupportedCriterionException extends Exception
{
	/**
	 * The unsupported criterion or <code>null</code> for none
	 */
	private final Criterion criterion;

	/**
	 * Stores <code>null</code> in the instance variables
	 */
	public UnsupportedCriterionException()
	{
		this(null);
	}

	/**
	 * Stores the unsupported criterion without a detailed message.
	 * 
	 * @param criterion
	 *            the unsupported criterion or <code>null</code> for none
	 */
	public UnsupportedCriterionException(Criterion criterion)
	{
		this(criterion, null);
	}

	/**
	 * Stores the instance variables.
	 * 
	 * @param criterion
	 *            the unsupported criterion or <code>null</code> for none
	 * @param message
	 *            a detailed message or <code>null</code> for none
	 */
	public UnsupportedCriterionException(Criterion criterion, String message)
	{
		super(message);
		this.criterion = criterion;
	}

	/**
	 * @return the unsupported criterion
	 */
	public Criterion getCriterion()
	{
		return criterion;
	}

	/**
	 * Returns the string representation of the unsupported criterion along with
	 * the detail message, if any.
	 * 
	 * @return the string representation of the unsupported criterion and the
	 *         detail message or <code>null</code> if not available
	 * @see java.lang.Throwable#getMessage()
	 */
	@Override
	public String getMessage()
	{
		String detailMessage = super.getMessage();
		if (criterion == null)
			return detailMessage;
		return (detailMessage == null) ? criterion.toString() : detailMessage + ": " + criterion.toString();
	}

	/**
	 * Version uid for serializable class
	 */
	private static final long serialVersionUID = 5280236361704883710L;
}
