/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISBREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DUCTAL_INSITU;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;

import java.util.HashSet;
import java.util.Set;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.screen.Matchable.Result;

/**
 * Enables a set of breast diagnosis type characteristics to be matchable
 * against multiple values and multiple diagnoses in a patient's history. This
 * class combines all breast diagnoses into a union that is matched by the
 * superclass.
 * 
 * @author Tom Bechtold
 * @version 2008-06-20
 */
public class BreastDiagnosisAttributeAssociativeMatchable extends AttributeAssociativeMatchable
{

	/**
	 * Store the instance variable using no aggregators
	 * 
	 * @param criterion
	 *            the criterion to represent
	 * @throws IllegalArgumentException
	 *             if <code>criterion</code> is <code>null</code>
	 */
	public BreastDiagnosisAttributeAssociativeMatchable(AssociativeCriterion criterion) throws IllegalArgumentException
	{
		super(criterion);
		if (!DIAGNOSISBREAST.equals(criterion.getParent()))
			throw new IllegalArgumentException("criterion is not for breast diagnosis: " + criterion);
	}

	/**
	 * @param history
	 *            the patient's history
	 * @return the union of the patient's attributes that are stored in its
	 *         attribute history under its diagnoses or <code>null</code> if
	 *         none
	 * @see org.quantumleaphealth.screen.AttributeAssociativeMatchable#getAttribute(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	@Override
	protected Set<CharacteristicCode> getAttribute(PatientHistory history)
	{
		if (history == null)
			return null;
		// Find the location within the diagnosis history
		Set<CharacteristicCode> breastDiagnoses = new HashSet<CharacteristicCode>(1);
		for (Diagnosis diagnosis : history.getDiagnoses())
		{
			Set<CharacteristicCode> breastDiagnosis = diagnosis.getAttributeCharacteristics().get(DIAGNOSISBREAST);
			if (breastDiagnosis != null)
				breastDiagnoses.addAll(breastDiagnosis);
		}
		return breastDiagnoses.isEmpty() ? null : breastDiagnoses;
	}

	/**
	 * @return false as breast diagnoses does not support the "other"
	 *         characteristic
	 * @see org.quantumleaphealth.screen.AttributeAssociativeMatchable#isOther(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	@Override
	protected boolean isOther(PatientHistory history)
	{
		return false;
	}

	private boolean isAvatarIgnorable()
	{
		for (CharacteristicCode code: criterion.getRequirement())
		{
			if (DUCTAL_INSITU.equals(code))
			{
				return false;
			}
		}
		return true;
	}
	
	/**
	 * Returns how the patient's attribute characteristic matches to the
	 * associative requirement. This method first tests for subjective matching,
	 * then calls the superclass for objective matching. This method's algorithm
	 * then depends upon the <code>comparator</code>:
	 * <ul>
	 * <li><code>SUBSET</code>: <code>true</code> if each characteristic in the
	 * characteristic is also present in the requirement
	 * <li><code>SUPERSET</code>: <code>true</code> if each characteristic in
	 * the requirement is also present in the characteristic
	 * <li><code>INTERSECT</code> (default): <code>true</code> if any
	 * characteristic is present in both the characteristic and the requirement.
	 * </ul>
	 * Finally, the invert is applied to the result.
	 * 
	 * @return how the characteristic matches the requirement
	 * @see org.quantumleaphealth.screen.AttributeAssociativeMatchable#matchDescriptive(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	@Override
	public Result matchDescriptive(PatientHistory history)
	{
		// Check for subjective first
		Set<CharacteristicCode> attributes = getAttribute(history);
		if ( attributes == null && history != null && history.isAvatar() && isAvatarIgnorable())
		{
			return Result.AVATAR_SUBJECTIVE;
		}
		
		if ((attributes != null) && attributes.contains(UNSURE))
		{	
			return criterion.isInvert() ? Result.FAIL_SUBJECTIVE : Result.PASS_SUBJECTIVE;
		}	

		// Other is specified if an "other" string is stored for the parent
		// characteristic
		// Copied from super.
		return (criterion.isInvert() ^ isMatch(getAttribute(history), requirement, isOther(history))) ? Result.PASS
				: Result.FAIL;
//
//		return super.matchDescriptive(history);
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = 1242538416879088546L;
}
