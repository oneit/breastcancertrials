/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import org.quantumleaphealth.model.trial.Criterion;

/**
 * Converts an eligibility criterion from declarative to imperative form. A
 * declarative criterion simply specifies the relevant patient characteristic
 * and screening requirement. An imperative criterion describes how the patient
 * data is fetched and the actual matching algorithm used.
 * 
 * @author Tom Bechtold
 * @version 2008-02-19
 */
public interface DeclarativeToImperativeConverter
{

	/**
	 * Converts a criterion from declarative to imperative form so that it may
	 * be matched to a patient's history.
	 * 
	 * @param declarative
	 *            the declarative criterion to convert
	 * @return an imperative criterion
	 * @throws UnsupportedCriterionException
	 *             if the criterion is not supported
	 */
	Matchable convert(Criterion declarative) throws UnsupportedCriterionException;
}
