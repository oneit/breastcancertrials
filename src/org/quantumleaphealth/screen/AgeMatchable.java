/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AGE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BIRTHDATE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.YEAR;

import java.text.NumberFormat;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.ComparativeCriterion;

/**
 * Matches a patient's year of birth characteristic to an age eligibility
 * criterion.
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 */
public class AgeMatchable extends Matchable
{
	/**
	 * The requirement
	 */
	private final double requirement;
	/**
	 * Whether or not the requirement is a maximum
	 */
	private final boolean maximum;

	/**
	 * The number of milliseconds in a year using 365.25 days/year.
	 */
	private static final double MILLISINYEAR = 1000d * 60d * 60d * 24d * (((365d * 4d) + 1d) / 4d);
	/**
	 * The year on January 1st when <code>System.currentTimeMillis()</code>
	 * returned zero.
	 */
	private static final double YEAR0 = 1970d;
	/**
	 * Formats age as integer.
	 */
	private static final NumberFormat FORMATTER = NumberFormat.getIntegerInstance();

	/**
	 * Validates and store the instance variable
	 * 
	 * @param ageCriterion
	 *            the age eligibility criterion
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code> or is not an age
	 *             criterion or does not use years as its unit of measure
	 */
	public AgeMatchable(ComparativeCriterion ageCriterion) throws IllegalArgumentException
	{
		if ((ageCriterion == null) || !AGE.equals(ageCriterion.getCharacteristicCode())
				|| !YEAR.equals(ageCriterion.getUnitOfMeasure()))
			throw new IllegalArgumentException("criterion is not for age in years: " + ageCriterion);
		requirement = ageCriterion.getRequirement();
		maximum = ageCriterion.isInvert();
	}

	/**
	 * Returns <code>PASS</code> if the patient's year of birth characteristic
	 * matches to the age requirement or <code>FAIL</code> otherwise. Because
	 * the patient's birth month and day are unspecified, this method only
	 * considers the birth year. Therefore there will be some false-positive
	 * matches for patients near the requirement -- but no false-negative
	 * matches. If age is unavailable then this method always returns
	 * <code>FAIL</code>.
	 * 
	 * @return <code>PASS</code> if the patient's year of birth characteristic
	 *         matches to the age requirement or <code>FAIL</code> otherwise
	 * @param history
	 *            the patient's history
	 * @see org.quantumleaphealth.screen.Matchable#match(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	public Result matchDescriptive(PatientHistory history)
	{
		// Round the age down; no age means automatic failure
		double ageRoundedDown = getAgeRoundedDown(history);
		if (Double.isNaN(ageRoundedDown))
		{
			if ( history.isAvatar() )  // continue matching if this is an avatar and age is not defined.
			{
				return Result.AVATAR_SUBJECTIVE;
			}
			return Result.FAIL;
		}	
		// Add one to maximum requirement to account for any birth month/day
		// during the year
		if (maximum)
			return (ageRoundedDown <= (requirement + 1d)) ? Result.PASS : Result.FAIL;
		return (ageRoundedDown >= requirement) ? Result.PASS : Result.FAIL;
	}

	/**
	 * @param history
	 *            the patient's history
	 * @return the patient's age rounded down or <code>Double.NaN</code> if the
	 *         birth date is not available
	 */
	private double getAgeRoundedDown(PatientHistory history)
	{
		Short yearOfBirth = (history == null) ? null : history.getYearMonthHistory().getShort(BIRTHDATE);
		return yearOfBirth == null ? Double.NaN : Math.floor((YEAR0 + (System.currentTimeMillis() / MILLISINYEAR))
				- yearOfBirth.shortValue());
	}

	/**
	 * Returns the localized patient's age characteristic and matching result
	 * 
	 * @returns the localized patient's age characteristic or
	 *          <code>unknown age</code> if the birth date is not available
	 * @param history
	 *            the patient's history
	 * @param resourceBundle
	 *            unused
	 * @see org.quantumleaphealth.screen.Matchable#getCharacteristic(org.quantumleaphealth.model.patient.PatientHistory,
	 *      java.util.ResourceBundle)
	 */
	public List<CharacteristicResult> getCharacteristicResults(PatientHistory history, ResourceBundle resourceBundle)
	{
		double ageRoundedDown = getAgeRoundedDown(history);
		return Arrays.asList(new CharacteristicResult(Double.isNaN(ageRoundedDown) ? "unknown age" : (FORMATTER
				.format(ageRoundedDown) + " years old"), matchDescriptive(history)));
	}

	/**
	 * Verion uid for serializable class
	 */
	private static final long serialVersionUID = 7011844344952056348L;
}
