/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.quantumleaphealth.model.patient.AvatarHistory;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.Trial;

/**
 * Screens a patient's history against trial data.
 * 
 * @author Tom Bechtold
 * @version 2009-03-10
 */
public interface MatchingEngine
{
	/**
	 * @return an iterator of trials that match a patient's history sorted in
	 *         ascending order on <code>Trial.id</code>, guaranteed to be non-
	 *         <code>null</code>
	 * @param patientHistory
	 *            the patient's history
	 */
	PatientHistoryMatchingIterator getMatchingIterator(PatientHistory patientHistory, AvatarHistory avatarHistory);

	/**
	 * @param trialIdentifier
	 *            the trial's unique identifier
	 * @param patientHistory
	 *            the patient's history
	 * @param description
	 *            describes the profile or <code>null</code> if not available
	 * @return the result of a descriptive match between a trial and a patient
	 *         history or <code>null</code> if the identifier does not match a
	 *         loaded trial
	 */
	EligibilityProfile getEligibilityProfile(Long trialIdentifier, PatientHistory patientHistory, String description);

	/**
	 * @return the loaded trials, guaranteed to be non-<code>null</code>
	 */
	Iterator<Trial> getTrialIterator();

	/**
	 * @return the trial or <code>null</code> if trial not found
	 * @param trialIdentifier
	 *            the trial's unique identifier
	 */
	Trial getTrial(Long trialIdentifier);

	/**
	 * @return the number of loaded trials
	 */
	int getTrialCount();

	/**
	 * @return the date and time when the trial eligibility criteria were last
	 *         modified or <code>null</code> if unknown
	 */
	Date getLastModified();

	/**
	 * @return the date when the trials were last modified in the registry or
	 *         <code>null</code> if unknown
	 */
	Date getLastRegistered();

	/**
	 * @return the date and time when the trials were loaded from persistent
	 *         storage, guaranteed to be non-<code>null</code>
	 */
	Date getLoaded();
	
	List<String> getCategoryCountsKeys();

	Integer getCategoryCountEntry( String key );
}
