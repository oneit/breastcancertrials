/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.Criteria;
import org.quantumleaphealth.model.trial.Criteria.Operator;

/**
 * Enables a set of criteria to be matchable against a patient's history. This
 * class is the imperative form of <code>Criteria</code>.
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 */
public class CriteriaMatchable extends Matchable
{
	/**
	 * The matchable children of this criteria
	 */
	private final Matchable[] children;

	/**
	 * The operator for the children
	 */
	private final Operator operator;

	/**
	 * Validate and store each parameter as an instance variable.
	 * 
	 * @param operator
	 *            the operator to apply to the children
	 * @param children
	 *            the children
	 * @throws UnsupportedCriterionException
	 *             if either paramater is <code>null</code> or it has no
	 *             children or any child is <code>null</code> or it has other
	 *             than two children if <code>IF</code> operator
	 */
	public CriteriaMatchable(Operator operator, Matchable[] children) throws UnsupportedCriterionException
	{
		if ((children == null) || (children.length == 0))
			throw new UnsupportedCriterionException(null, "criteria must contain at least one child");
		if (operator == null)
			throw new UnsupportedCriterionException(null, "operator must be specified");
		for (int index = 0; index < children.length; index++)
			if (children[index] == null)
				throw new UnsupportedCriterionException(null, "Child " + index + " not specified");
		if ((operator == Criteria.Operator.IF) && (children.length != 2))
			throw new UnsupportedCriterionException(null, "criteria must contain exactly two children");
		this.operator = operator;
		this.children = children;
	}

	/**
	 * Returns how the children match to a patient's history. This method's uses
	 * different philosophy depending upon the operator:
	 * <ul>
	 * <li><code>AND</code>: worst result among children (pessimistic)</li>
	 * <li><code>OR</code>: best result among children (optimistic)</li>
	 * <li><code>IF</code>: if the first child fails then pass; if the first
	 * child passes then use second child's result; otherwise the first is
	 * subjective -- ignore the second result and return the first</li>
	 * 
	 * @return whether the children match to a patient's history
	 * @see org.quantumleaphealth.screen.Matchable#matchDefinitive(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	public Result matchDescriptive(PatientHistory history)
	{
		if (operator == null)
			return Result.FAIL;
		switch (operator)
		{
		case IF:
			// If first one fails then pass
			if (children.length < 2)
				return Result.FAIL;

            // Treatment matches are not considered for avatars.  We are not even going to check the condition.
			if (children[0].avatarIgnorable(history))
			{
				ReportOutput.addOutput("   IF: first result is Cyan Avatar Ignored for " + children[0]);
				return Result.AVATAR_SUBJECTIVE;
			}
 
			Result firstResult = children[0].matchDescriptive(history);
			ReportOutput.addOutput("   IF: first result is " + (firstResult == null? Result.FAIL: firstResult) + " for " + children[0].toString());
			if ((firstResult == null) || (firstResult == Result.FAIL))
				return Result.PASS;
			
            // If we passed the condition then we still have to check to ensure that the THEN part of the IF
			// can be tested for an avatar as well.
			if (children[1].avatarIgnorable(history))
			{
				ReportOutput.addOutput("   IF: second result is Cyan Avatar Ignored for " + children[1]);
				return Result.AVATAR_SUBJECTIVE;
			}
			
			// If first one passes then use second4
			Result secondResult = children[1].matchDescriptive(history);
			ReportOutput.addOutput("   IF: second result is " + (secondResult == null? Result.FAIL: secondResult) + " for " + children[1]);
			if (firstResult == Result.PASS)
				return secondResult;
			
			// Otherwise first one is subjective -- ignore the second
			return firstResult;
		case AND:
			// Return worst child result
			Result result = (history != null && history.isAvatar())? Result.AVATAR_SUBJECTIVE: Result.PASS;
			for (Matchable matchable : children)
			{				
				// Treatment matches are not considered for avatars.
				if (matchable.avatarIgnorable(history))
				{
					ReportOutput.addOutput("   AND: result is Cyan Avatar Ignored for " + matchable);
					continue;
				}
				result = worse(result, matchable.matchDescriptive(history));
				ReportOutput.addOutput("   AND: result is " + (result == null? Result.FAIL: result) + ", for matchable " + matchable);				
			}	
			return result;
		case OR:
			// Return best child result
			result = null;
			for (Matchable matchable : children)
			{
				// Treatment matches are not considered for avatars.
				if (matchable.avatarIgnorable(history))
				{
					ReportOutput.addOutput("  	OR: result is Cyan Avatar Ignored for " + matchable);
					result = Result.AVATAR_SUBJECTIVE;
					continue;
				}
				result = better(result, matchable.matchDescriptive(history));
				ReportOutput.addOutput("   OR: result is " + (result == null? Result.FAIL: result) + "  for " + matchable);				
			}
			return result;
		default:
			// TODO: throw exception?
			return Result.FAIL;
		}
	}

	/**
	 * Returns localized description(s) of the characteristic(s) and their
	 * results that are relevant to the children criteria. This method ensures
	 * that characteristics are not duplicated in the list by updating the
	 * results of existing characteristics.
	 * 
	 * @param history
	 *            the patient's history
	 * @param resourceBundle
	 *            contains localized descriptions
	 * @return localized description(s) of the characteristic(s) and their
	 *         results that are relevant to this criteria or <code>null</code>
	 *         for none
	 * @see org.quantumleaphealth.screen.Matchable#getCharacteristic(org.quantumleaphealth.model.patient.PatientHistory,
	 *      java.util.ResourceBundle)
	 */
	public List<CharacteristicResult> getCharacteristicResults(PatientHistory history, ResourceBundle resourceBundle)
	{
		if ((children == null) || (children.length == 0))
			return null;
		LinkedList<CharacteristicResult> characteristicResults = new LinkedList<CharacteristicResult>();
		for (Matchable child : children)
		{
			List<CharacteristicResult> childCharacteristicResults = child.getCharacteristicResults(history,
					resourceBundle);
			if (childCharacteristicResults == null)
				continue;
			// Update the criteria list by either updating results of matching
			// characteristics or adding new results
			childResultLoop: for (CharacteristicResult childCharacteristicElement : childCharacteristicResults)
			{
				for (CharacteristicResult characteristicElement : characteristicResults)
					// For OR operations use the best result for matching
					// characteristics; otherwise use the worse result
					if (characteristicElement.matches(childCharacteristicElement, operator == Operator.OR))
						continue childResultLoop;
				characteristicResults.add(childCharacteristicElement);
			}
		}
		return (characteristicResults.size() == 0) ? null : characteristicResults;
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = 621006054055196043L;
}
