/**
 * (c) 2008 Quantum Leap Healthcare Collaborative
 * All Rights Reserved
 */
package org.quantumleaphealth.screen;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.METASTATIC_BREAST_CANCER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LEFT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RIGHT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.INFLAMMATORY;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AFFIRMATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.NEGATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;

import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.BooleanCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Match to whether any of the patient's <code>diagnoses</code> list contains
 * affirmative value for inflammatory.
 * 
 * @author Tom Bechtold
 * @version 2008-08-11
 */
public class InflammatoryBooleanMatchable extends DiagnosisMatchable
{
	/**
	 * The invert
	 */
	protected final boolean invert;

	/**
	 * Store invert
	 * 
	 * @param booleanCriterion
	 *            the criterion
	 * @throws IllegalArgumentException
	 *             if parameter is <code>null</code> or not inflammatory
	 */
	public InflammatoryBooleanMatchable(BooleanCriterion booleanCriterion) throws IllegalArgumentException
	{
		if ((booleanCriterion == null) || !INFLAMMATORY.equals(booleanCriterion.getCharacteristicCode()))
			throw new IllegalArgumentException("Criterion not inflammatory: " + booleanCriterion);
		this.invert = booleanCriterion.isInvert();
	}

	/**
	 * Returns how the inflammatory criterion matches to a patient history. This
	 * method takes into account bilateral differences and subjectivity:
	 * <ol>
	 * <li>If metastatic diagnosis but inflammatory was not provided then return
	 * <code>FAIL</code></li>
	 * <li>If metastatic diagnosis and inflammatory was <code>UNSURE</code> then
	 * return <code>PASS_SUBJECTIVE</code> (if inverted,
	 * <code>FAIL_SUBJECTIVE</code>)</li>
	 * <li>If metastatic diagnosis compare it to <code>AFFIRMATIVE</code> and
	 * apply the invert</li>
	 * <li>If no lateral diagnoses then return <code>FAIL</code> (if inverted,
	 * <code>PASS</code>)</li>
	 * <li>If a lateral diagnosis was made but inflammatory was not provided
	 * then return <code>FAIL</code></li>
	 * <li>If either diagnosis was <code>UNSURE</code> or the diagnoses are
	 * opposite then return <code>PASS_SUBJECTIVE</code> (if inverted,
	 * <code>FAIL_SUBJECTIVE</code>)</li>
	 * <li>At this point, only one is available or both are the same, so use the
	 * available one</li>
	 * <li>Compare it to <code>AFFIRMATIVE</code> and apply the invert</li>
	 * </ol>
	 * 
	 * @return subjective if either breast is subjective or both breasts differ
	 * @param history
	 *            the patient's history
	 * @see org.quantumleaphealth.screen.Matchable#matchDescriptive(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	@Override
	public Result matchDescriptive(PatientHistory history)
	{
		// Validate history
		if (history == null)
			return Result.FAIL;
		// Metastatic diagnosis
		if (isMetastatic(history))
		{
			// No answer means fail
			CharacteristicCode value = getDiagnosisValue(getDiagnosis(history, METASTATIC_BREAST_CANCER), INFLAMMATORY);
			if (value == null)
			{	
				return history.isAvatar()? Result.AVATAR_SUBJECTIVE: Result.FAIL;
			}	
			// If subjective then return inverted subjective
			if (UNSURE.equals(value))
				return invert ? Result.FAIL_SUBJECTIVE : Result.PASS_SUBJECTIVE;
			// Compare to affirmative and invert result
			return (invert ^ AFFIRMATIVE.equals(value)) ? Result.PASS : Result.FAIL;
		}

		// If no lateral diagnoses then return inverted fail
		Diagnosis left = getDiagnosis(history, LEFT_BREAST);
		Diagnosis right = getDiagnosis(history, RIGHT_BREAST);
		if ((left == null) && (right == null))
		{	
			return history.isAvatar()? Result.AVATAR_SUBJECTIVE: invert ? Result.PASS : Result.FAIL;
		}	

		// If either not answered then fail definitively
		CharacteristicCode leftValue = getDiagnosisValue(left, INFLAMMATORY);
		CharacteristicCode rightValue = getDiagnosisValue(right, INFLAMMATORY);
		if (((left != null) && (leftValue == null)) || ((right != null) && (rightValue == null)))
			return Result.FAIL;

		// If either subjective or both are unequal then return inverted
		// subjective
		if (UNSURE.equals(leftValue) || UNSURE.equals(rightValue)
				|| ((leftValue != null) && (rightValue != null) && !leftValue.equals(rightValue)))
			return invert ? Result.FAIL_SUBJECTIVE : Result.PASS_SUBJECTIVE;

		// If left is not diagnosed then the right must be; compare to
		// affirmative and invert result
		return (invert ^ AFFIRMATIVE.equals(leftValue == null ? rightValue : leftValue)) ? Result.PASS : Result.FAIL;
	}

	/**
	 * @return the localized characteristic and matching result
	 * @param history
	 *            the patient's history
	 * @param resourceBundle
	 *            not used
	 * @see org.quantumleaphealth.screen.Matchable#getCharacteristicResults(org.quantumleaphealth.model.patient.PatientHistory,
	 *      java.util.ResourceBundle)
	 */
	@Override
	public List<CharacteristicResult> getCharacteristicResults(PatientHistory history, ResourceBundle resourceBundle)
	{
		// If history not available then return
		if (history == null)
			return Arrays.asList(new CharacteristicResult("history not available", matchDescriptive(history)));
		// Add left and right descriptions
		StringBuilder stringBuilder = new StringBuilder();
		addDiagnosis(stringBuilder, history, METASTATIC_BREAST_CANCER, "metastatic");
		addDiagnosis(stringBuilder, history, LEFT_BREAST, "left breast");
		addDiagnosis(stringBuilder, history, RIGHT_BREAST, "right breast");
		if (stringBuilder.length() == 0)
			stringBuilder.append("no inflammatory diagnoses");
		return Arrays.asList(new CharacteristicResult(stringBuilder.toString(), matchDescriptive(history)));
	}

	/**
	 * Append the localized description of the patient's inflammatory
	 * characteristic to a string builder.
	 * 
	 * @param stringBuilder
	 *            the builder to append to
	 * @param history
	 *            the patient's history
	 * @param location
	 *            the diagnosis' location
	 * @param name
	 *            the localized name of the diagnosis
	 */
	private static void addDiagnosis(StringBuilder stringBuilder, PatientHistory history, CharacteristicCode location,
			String name)
	{
		CharacteristicCode value = getDiagnosisValue(getDiagnosis(history, location), INFLAMMATORY);
		if ((stringBuilder == null) || (value == null))
			return;

		// Add conjunction and available prefix
		if (stringBuilder.length() > 0)
			stringBuilder.append("; ");
		if (name != null)
			stringBuilder.append(name).append(' ');
		// Translate values into localized strings
		if (AFFIRMATIVE.equals(value))
			stringBuilder.append("inflammatory");
		else if (NEGATIVE.equals(value))
			stringBuilder.append("not inflammatory");
		else if (UNSURE.equals(value))
			stringBuilder.append("inflammatory not sure");
		else
			stringBuilder.append("inflammatory unavailable");
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = 8727366325545556860L;
}
