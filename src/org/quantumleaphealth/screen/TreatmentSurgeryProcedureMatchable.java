/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.*;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.Procedure;
import org.quantumleaphealth.model.patient.YearMonth;
import org.quantumleaphealth.model.trial.TreatmentProcedureCriterion;
import org.quantumleaphealth.model.trial.AssociativeCriterion.SetOperation;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Enables a set of surgical requirements to be matchable against a patient's
 * history. This class maps a declarative set of surgical procedures into a
 * boolean hierarchy of imperative criteria.
 * <p>
 * For each requirement an imperative criterion is created. First the
 * requirement is analyzed to see if it is an aggregator or has bilateral
 * components. If so, the requirement is converted into a boolean set of all
 * relevant surgical procedures. For example, a requirement of
 * "bilateral breast surgery" will be expanded into left-breast mastectomy and
 * right-breast mastectomy OR left-breast lumpectomy and right-breast lumpectomy
 * OR ... etc.
 * </p>
 * 
 * During matching the patient's history is scanned for the surgical procedure
 * that the criterion specifies. If it is found then any optional qualifiers are
 * applied:
 * <ol>
 * <li>Most recent: the surgical procedure is not earlier than the most recent
 * cancer diagnosis. <strong>Note: this logic does not match the laterality or
 * type of procedure to the recent diagnosis. For example, if the most recent
 * diagnosis was for the left breast, any prophylactic surgery on the right
 * breast or other organ performed after the diagnosis date will be considered
 * for this malignancy. A better implementation of the logic would match at
 * least the laterality of the diagnosis to the procedures.</strong></li>
 * <li>Earliest objective date: the surgical procedure is not earlier than the
 * current date minus the maximum elapsed time (adjusted for some lee-way by a
 * precision factor)</li>
 * <li>Earliest subjective-pass date: if the surgical procedure is earlier than
 * the current date minus the maximum subjective-pass time then the result is
 * subjective-fail. Otherwise the result is subjective-pass. This functionality
 * is meant to alert the user that a maximum-elapsed qualifier might soon make
 * the patient ineligible for the trial. It uses a more serious result
 * (subjective-fail) if the patient is closer to disqualification.</li>
 * </ol>
 * <p>
 * The overall result is found by taking the best result of matching each
 * surgical procedure. Finally, the result is inverted if specified by the
 * declarative criterion.
 * </p>
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 */
public class TreatmentSurgeryProcedureMatchable extends Matchable
{
	/**
	 * The hierarchical imperative requirements for surgical procedures
	 */
	private final ProcedureMatchable procedureMatchable;
	/**
	 * Whether or not the overall criterion is inverted
	 */
	private final boolean invert;
	/**
	 * Whether or not the matching procedures must be for the recent cancer
	 * diagnosis
	 */
	private final boolean recentMalignancy;
	/**
	 * The maximum number of weeks allowed between today and procedure or 0 if
	 * not applicable
	 */
	private final int maximumElapsedWeeks;
	/**
	 * The number of milliseconds in a week
	 */
	private final long MILLISECONDS_PER_WEEK = 1000l * 60l * 60l * 24l * 7l;
	/**
	 * The number of weeks to add to the maximum-week limit when testing
	 */
	private final int MAXIMUMWEEKS_PRECISION = 1;
	/**
	 * The number of weeks to subtract from the maximum-week limit to establish
	 * the subjective threshold. Procedures performed earlier than the threshold
	 * will generate a <code>SUBJECTIVE_FAIL</code> match result; those later
	 * will generate a <code>SUBJECTIVE_PASS</code> result.
	 */
	private final int MAXIMUMWEEKS_SUBJECTIVE = 4;

	/**
	 * Encapsulates a procedure's type and location
	 */
	private static class TypeLocation implements Serializable
	{
		/**
		 * The procedure's type
		 */
		private final CharacteristicCode type;
		/**
		 * The procedure's location
		 */
		private final CharacteristicCode location;

		/**
		 * Store the parameters into instance variables
		 * 
		 * @param type
		 *            the procedure's type
		 * @param location
		 *            the procedure's location
		 */
		private TypeLocation(CharacteristicCode type, CharacteristicCode location)
		{
			if ((type == null) || (location == null))
				throw new IllegalArgumentException("must specify both parameters");
			this.type = type;
			this.location = location;
		}

		/**
		 * @param procedure
		 *            a procedure
		 * @return whether a procedure has the same type and location
		 */
		private boolean matches(Procedure procedure)
		{
			if (procedure == null)
				return false;
			return type.equals(procedure.getType()) && location.equals(procedure.getLocation());
		}

		/**
		 * Returns location and type
		 * 
		 * @return location and type
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString()
		{
			return "[" + type + ',' + location + ']';
		}

		/**
		 * Version UID for serialization
		 */
		private static final long serialVersionUID = 6533181900166775950L;
	}

	/**
	 * Encapsulates two procedures associated by laterality.
	 */
	private static class BilateralTypeLocation implements Serializable
	{
		/**
		 * The procedure on the left side or the only procedure if it is not
		 * bilateral in nature
		 */
		private final TypeLocation left;
		/**
		 * The procedure on the right side or <code>null</code> if the procedure
		 * is not bilateral in nature
		 */
		private final TypeLocation right;

		/**
		 * Store the parameters into instance variables
		 * 
		 * @param typeLeft
		 *            the left procedure's type
		 * @param locationLeft
		 *            the left procedure's location
		 * @param typeRight
		 *            the right procedure's type or <code>null</code> for
		 *            unilateral
		 * @param locationRight
		 *            the right procedure's location or <code>null</code> for
		 *            unilateral
		 */
		private BilateralTypeLocation(CharacteristicCode typeLeft, CharacteristicCode locationLeft,
				CharacteristicCode typeRight, CharacteristicCode locationRight)
		{
			this.left = new TypeLocation(typeLeft, locationLeft);
			this.right = (typeRight == null) || (locationRight == null) ? null : new TypeLocation(typeRight,
					locationRight);
		}

		/**
		 * Version UID for serialization
		 */
		private static final long serialVersionUID = 6012477083554841017L;
	}

	/**
	 * An imperative criterion that matches a requirement to a patient's
	 * surgical history.
	 */
	private static abstract class ProcedureMatchable implements Serializable
	{
		/**
		 * @return how the criterion matches to a patient's surgical procedure
		 * @param procedure
		 *            the patient's surgical procedure
		 * @param mostRecent
		 *            the date of the most recent cancer diagnosis or
		 *            <code>null</code> if not a qualifier
		 * @param earliestObjectiveDate
		 *            the earliest date of a qualifying procedure or
		 *            <code>null</code> if not a qualifier
		 * @param earliestSubjectivePassDate
		 *            the earliest date of a subjective qualifying procedure or
		 *            <code>null</code> if not a qualifier
		 */
		protected abstract Result match(Procedure procedure, YearMonth mostRecent, YearMonth earliestObjectiveDate,
				YearMonth earliestSubjectivePassDate, boolean isAvatar);

		/**
		 * Version UID for serializable class
		 */
		private static final long serialVersionUID = 9154862588057296653L;
	}

	/**
	 * A hierarchical set of imperative criteria.
	 */
	private static class ProceduresMatchable extends ProcedureMatchable
	{
		/**
		 * The set of criteria that is owned by this object
		 */
		private final ProcedureMatchable[] procedureMatchables;
		/**
		 * The operator to apply to the set. Note that
		 * <code>SetOperator.SUBSET</code> is not supported.
		 */
		private final SetOperation setOperation;

		/**
		 * Store the parameters into instance variables
		 * 
		 * @param procedureMatchables
		 *            the set of criteria that is owned by this object
		 * @param setOperation
		 *            the operator to apply to the set
		 * @throws IllegalArgumentException
		 *             if either parameter is <code>null</code> or if
		 *             <code>setOperator</code> is
		 *             <code>SetOperator.SUBSET</code>
		 */
		private ProceduresMatchable(ProcedureMatchable[] procedureMatchables, SetOperation setOperation)
				throws IllegalArgumentException
		{
			if (procedureMatchables == null)
				throw new IllegalArgumentException("must specify matchables");
			if (setOperation == null)
				throw new IllegalArgumentException("must specify set operation");
			if (setOperation == SetOperation.SUBSET)
				throw new IllegalArgumentException("set operation SUBSET (e.g., 'only') not supported");
			this.procedureMatchables = procedureMatchables;
			this.setOperation = setOperation;
		}

		/**
		 * Returns the application of the set operator to each criterion's
		 * matching result. This method calls each of its children to get a
		 * matching result and then uses the set operator to combine the
		 * results:
		 * <ul>
		 * <li><code>SetOperation.SUBSET</code> (e.g., only): Not supported
		 * (returns FAIL)</li>
		 * <li><code>SetOperation.SUPERSET</code> (e.g., all): Returns the worst
		 * result from its children</li>
		 * <li><code>SetOperation.INTERSECT</code> (e.g., or, the default):
		 * Returns the best result from its children</li>
		 * </ul>
		 * 
		 * @return the application of the set operator to each criterion's
		 *         matching result
		 * @param procedure
		 *            the patient's surgical procedure
		 * @param mostRecent
		 *            the date of the most recent cancer diagnosis or
		 *            <code>null</code> if not a qualifier
		 * @param earliestObjectiveDate
		 *            the earliest date of a qualifying procedure or
		 *            <code>null</code> if not a qualifier
		 * @param earliestSubjectivePassDate
		 *            the earliest date of a subjective qualifying procedure or
		 *            <code>null</code> if not a qualifier
		 * @see org.quantumleaphealth.screen.TreatmentSurgeryProcedureMatchable.ProcedureMatchable#match(java.util.List,
		 *      org.quantumleaphealth.model.patient.YearMonth,
		 *      org.quantumleaphealth.model.patient.YearMonth,
		 *      org.quantumleaphealth.model.patient.YearMonth)
		 */
		protected Result match(Procedure procedure, YearMonth mostRecent, YearMonth earliestObjectiveDate,
				YearMonth earliestSubjectivePassDate, boolean isAvatar)
		{
			// ONLY: Not supported
			if (setOperation == SetOperation.SUBSET)
				return Result.FAIL;
			// AND: Return worst result
			if (setOperation == SetOperation.SUPERSET)
			{
				Result result = Result.PASS;
				for (ProcedureMatchable procedureMatchable : procedureMatchables)
				{
					//System.out.println("PC MAtchable1"+ procedureMatchable.toString());
					result = result.worse(procedureMatchable.match(procedure, mostRecent, earliestObjectiveDate,
							earliestSubjectivePassDate, isAvatar));
					//System.out.println("PC MAtchable1 Result:"+ result);
				}
				return result;
			}
			// OR (default): Return best result
			Result result = Result.FAIL;
			for (ProcedureMatchable procedureMatchable : procedureMatchables)
			{
				//System.out.println("PC MAtchable2"+ procedureMatchable.toString());
				result = result.better(procedureMatchable.match(procedure, mostRecent, earliestObjectiveDate,
						earliestSubjectivePassDate, isAvatar));
				//System.out.println("PC MAtchable2 Result:"+ result);

			}
			return result;
		}

		/**
		 * Returns list of children separated by operator
		 * 
		 * @return list of children separated by operator
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString()
		{
			if ((procedureMatchables == null) || (procedureMatchables.length == 0))
				return "{}";
			StringBuilder builder = new StringBuilder('{').append(procedureMatchables[0]);
			for (int index = 1; index < procedureMatchables.length; index++)
				builder.append(' ').append(
						setOperation == SetOperation.SUPERSET ? " and "
								: (setOperation == SetOperation.SUBSET ? " and/or " : " or ")).append(
						procedureMatchables[index]);
			return builder.append('}').toString();
		}

		/**
		 * Version UID for serialization
		 */
		private static final long serialVersionUID = 1648716354938102658L;
	}

	/**
	 * An imperative criterion that scans a patient's history for a matching
	 * procedure.
	 */
	private static class ImperativeProcedureMatchable extends ProcedureMatchable
	{
		/**
		 * The required procedure's type and location
		 */
		private final TypeLocation typeLocation;

		/**
		 * Store the parameter into instance variable
		 * 
		 * @param typeLocation
		 *            the required procedure's type and location
		 * @throws IllegalArgumentException
		 *             if the parameter is <code>null</code>
		 */
		private ImperativeProcedureMatchable(TypeLocation typeLocation) throws IllegalArgumentException
		{
			if (typeLocation == null)
				throw new IllegalArgumentException("must specify type and location");
			this.typeLocation = typeLocation;
		}

		/**
		 * Returns whether the required procedure is found within the patient's
		 * history. This method returns <code>FAIL</code> if the procedure is
		 * not available. If found then any provided qualifiers are also
		 * checked:
		 * <ol>
		 * <li>Most recent: the surgical procedure is not earlier than the most
		 * recent cancer diagnosis. TODO: <strong>Note: this logic does not
		 * match the laterality or type of procedure to the recent diagnosis.
		 * For example, if the most recent diagnosis was for the left breast,
		 * any prophylactic surgery on the right breast or other organ performed
		 * after the diagnosis date will be considered for this malignancy. A
		 * better implementation of the logic would match at least the
		 * laterality of the diagnosis to the procedures.</strong></li>
		 * <li>Earliest objective date: the surgical procedure is not earlier
		 * than the current date minus the maximum elapsed time (adjusted for
		 * some lee-way by a precision factor)</li>
		 * <li>Earliest subjective-pass date: if the surgical procedure is
		 * earlier than the current date minus the maximum subjective-pass time
		 * then the result is subjective-fail. Otherwise the result is
		 * subjective-pass. This functionality is meant to alert the user that a
		 * maximum-elapsed qualifier might soon make the patient ineligible for
		 * the trial. It uses a more serious result (subjective-fail) if the
		 * patient is closer to disqualification.</li>
		 * </ol>
		 * 
		 * @return how the criterion matches to a patient's surgical procedure
		 * @param procedure
		 *            the patient's surgical procedure
		 * @param mostRecent
		 *            the date of the most recent cancer diagnosis or
		 *            <code>null</code> if not a qualifier
		 * @param earliestObjectiveDate
		 *            the earliest date of a qualifying procedure or
		 *            <code>null</code> if not a qualifier
		 * @param earliestSubjectivePassDate
		 *            the earliest date of a subjective qualifying procedure or
		 *            <code>null</code> if not a qualifier
		 * @see org.quantumleaphealth.screen.TreatmentSurgeryProcedureMatchable.ProcedureMatchable#match(java.util.List,
		 *      org.quantumleaphealth.model.patient.YearMonth,
		 *      org.quantumleaphealth.model.patient.YearMonth,
		 *      org.quantumleaphealth.model.patient.YearMonth)
		 */
		protected Result match(Procedure procedure, YearMonth mostRecent, YearMonth earliestObjectiveDate,
				YearMonth earliestSubjectivePassDate, boolean isAvatar)
		{
			if ( isAvatar && procedure == null )
			{
				return Result.AVATAR_SUBJECTIVE;
			}
			
			// Check location and type
			if (!typeLocation.matches(procedure))
				return Result.FAIL;

			// Check date if most recent is specified
			YearMonth date = procedure.getStarted();
			if ((mostRecent != null) && ((date == null) || (date.isEarlier(mostRecent))))
				return Result.FAIL;

			// If maximum elapsed time is specified then check both objectively
			// and subjectively
			if ((earliestObjectiveDate != null) && ((date == null) || (date.isEarlier(earliestObjectiveDate))))
				return Result.FAIL;
			if (earliestSubjectivePassDate == null)
				return Result.PASS;
			if ((date == null) || date.isEarlier(earliestSubjectivePassDate))
				return Result.FAIL_SUBJECTIVE;
			return Result.PASS_SUBJECTIVE;
		}

		/**
		 * Returns type and location
		 * 
		 * @return type and location
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString()
		{
			return typeLocation.toString();
		}

		/**
		 * Version UID for serialization
		 */
		private static final long serialVersionUID = -1295588528512871918L;
	}

	/**
	 * Converts a set of declarative criteria into an imperative criterion. This
	 * method converts each declarative criterion into an imperative one and
	 * then bundles the set along with a set operator into an object.
	 * 
	 * @param characteristicCodes
	 *            the required procedures
	 * @param setOperation
	 *            the operation used to combine the matching results
	 * @param bilateral
	 *            whether or not the procedures must be bilateral
	 * @return an imperative criterion that matches to the requirements
	 * @throws UnsupportedCriterionException
	 *             if a required procedure is missing or not supported or if a
	 *             set operator is not supported
	 */
	private static ProcedureMatchable createMatchable(CharacteristicCode[] characteristicCodes,
			SetOperation setOperation) throws UnsupportedCriterionException
	{
		if ((characteristicCodes == null) || (characteristicCodes.length == 0))
			throw new UnsupportedCriterionException(null, "missing codes");
		if (characteristicCodes.length == 1)
			return createMatchable(characteristicCodes[0]);
		ProcedureMatchable[] procedureMatchables = new ProcedureMatchable[characteristicCodes.length];
		for (int index = 0; index < procedureMatchables.length; index++)
			procedureMatchables[index] = createMatchable(characteristicCodes[index]);
		try
		{
			//System.out.println("Size of Matchables: "+procedureMatchables.length);
			return new ProceduresMatchable(procedureMatchables, setOperation);
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
			throw new UnsupportedCriterionException(null, illegalArgumentException.getMessage());
		}
	}

	/**
	 * Converts a declarative criterion into an imperative criterion. If the
	 * declarative criterion is an aggregator then this method expands it into a
	 * set of declarative criteria. If the declarative criterion represents a
	 * procedure that may occur in a bilateral setting then this method expands
	 * the declarative criterion into a set of two imperative criteria and
	 * applies either <code>INTERSECT</code> or <code>SUPERSET</code> to the set
	 * depending upon whether the requirement must be bilateral.
	 * 
	 * @param characteristicCode
	 *            the required procedure
	 * @param bilateral
	 *            whether or not the required procedure is performed in a
	 *            bilateral setting
	 * @return an imperative criterion that represents the declarative criterion
	 * @throws UnsupportedCriterionException
	 *             if the declarative criterion is missing or not supported
	 */
	private static ProcedureMatchable createMatchable(CharacteristicCode characteristicCode)
			throws UnsupportedCriterionException
	{
		if (characteristicCode == null)
			throw new UnsupportedCriterionException(null, "missing code");
		CharacteristicCode[] characteristicCodes = SURGERY_AGGREGATORS.get(characteristicCode);
		if ((characteristicCodes != null) && (characteristicCodes.length > 0))
			if ((characteristicCodes.length == 1) && !characteristicCodes[0].equals(characteristicCode))
			{
				//System.out.println("First One");
				return createMatchable(characteristicCodes[0]);
			}
			else
			{
				//System.out.println("Second One");
				return createMatchable(characteristicCodes, SetOperation.INTERSECT);
			}
		BilateralTypeLocation bilateralTypeLocation = CONVERTER.get(characteristicCode);
		if ((bilateralTypeLocation == null) || (bilateralTypeLocation.left == null))
			throw new UnsupportedCriterionException(null, characteristicCode.toString());
		try
		{
			
			ImperativeProcedureMatchable left = new ImperativeProcedureMatchable(bilateralTypeLocation.left);
			if (bilateralTypeLocation.right == null)
				return left;
			ImperativeProcedureMatchable[] bilateralMatchables = new ImperativeProcedureMatchable[2];
			bilateralMatchables[0] = left;
			bilateralMatchables[1] = new ImperativeProcedureMatchable(bilateralTypeLocation.right);
			return new ProceduresMatchable(bilateralMatchables, SetOperation.INTERSECT);
		}
		catch (IllegalArgumentException illegalArgumentException)
		{
			throw new UnsupportedCriterionException(null, illegalArgumentException.getMessage());
		}
	}

	/**
	 * Associates a required procedure in a criterion to a type/location pair in
	 * a patient's surgery history. If the procedure can be performed in a
	 * bilateral manner then both left and right type/location pairs are
	 * provided. The set of keys constitutes the procedures implied if the "any"
	 * requirement (e.g., <code>null</code>) is specified
	 * 
	 * @see TreatmentProcedureCriterion#getRequirement()
	 * @see Procedure#getType()
	 * @see Procedure#getLocation()
	 */
	private static final Map<CharacteristicCode, BilateralTypeLocation> CONVERTER = new HashMap<CharacteristicCode, BilateralTypeLocation>(
			15, 1.0f);
	static
	{
		CONVERTER.put(SURGERY_BREAST_CONSERVING, new BilateralTypeLocation(SURGERY_BREAST_CONSERVING, LEFT_BREAST,
				SURGERY_BREAST_CONSERVING, RIGHT_BREAST));
		CONVERTER.put(BREAST_REEXCISION, new BilateralTypeLocation(BREAST_REEXCISION, LEFT_BREAST, BREAST_REEXCISION,
				RIGHT_BREAST));
		CONVERTER.put(EXCISIONAL_BIOPSY, new BilateralTypeLocation(EXCISIONAL_BIOPSY, LEFT_BREAST, EXCISIONAL_BIOPSY,
				RIGHT_BREAST));
		CONVERTER.put(MASTECTOMY_BILATERAL, new BilateralTypeLocation(MASTECTOMY_BILATERAL, LEFT_BREAST, null, null));
		CONVERTER.put(MASTECTOMY_UNILATERAL, new BilateralTypeLocation(MASTECTOMY_UNILATERAL, LEFT_BREAST,
				MASTECTOMY_UNILATERAL, RIGHT_BREAST));
		/*CONVERTER.put(MASTECTOMY_PROPHYLACTIC, new BilateralTypeLocation(MASTECTOMY_PROPHYLACTIC, LEFT_BREAST,
				MASTECTOMY_PROPHYLACTIC, RIGHT_BREAST));*/
		CONVERTER.put(SENTINEL_NODE_BIOPSY, new BilateralTypeLocation(SENTINEL_NODE_BIOPSY, LEFT_BREAST,
				SENTINEL_NODE_BIOPSY, RIGHT_BREAST));
		CONVERTER.put(AXILLARY_NODE_DISSECTION, new BilateralTypeLocation(AXILLARY_NODE_DISSECTION, LEFT_AXILLARY_NODE,
				AXILLARY_NODE_DISSECTION, RIGHT_AXILLARY_NODE));
		CONVERTER.put(OOPHORECTOMY, new BilateralTypeLocation(OOPHORECTOMY, LEFT_OVARY, OOPHORECTOMY, RIGHT_OVARY));
		CONVERTER.put(HYSTERECTOMY, new BilateralTypeLocation(HYSTERECTOMY, UTERUS, null, null));
		CONVERTER.put(SURGERY_BRAIN, new BilateralTypeLocation(SURGERY_BRAIN, BRAIN, null, null));
		CONVERTER.put(SURGERY_SPINAL_CORD, new BilateralTypeLocation(SURGERY_SPINAL_CORD, SPINAL_CORD, null, null));
		CONVERTER.put(SURGERY_BONE, new BilateralTypeLocation(SURGERY_BONE, BONE, null, null));
		CONVERTER.put(SURGERY_LIVER, new BilateralTypeLocation(SURGERY_LIVER, LIVER, null, null));
		CONVERTER.put(SURGERY_LUNG, new BilateralTypeLocation(SURGERY_LUNG, LUNG, null, null));
		CONVERTER.put(SURGERY_LYMPH_NODE, new BilateralTypeLocation(SURGERY_LYMPH_NODE, LYMPH_NODE, null, null));
	}

	/**
	 * Calculates the instance variables from the criterion
	 * 
	 * @param criterion
	 *            the surgery criterion
	 * @throws IllegalArgumentException
	 *             if the criterion is not surgery
	 * @throws UnsupportedCriterionException
	 *             if a surgical procedure is not supported or if the set
	 *             operation is <code>SUBSET</code>
	 */
	public TreatmentSurgeryProcedureMatchable(TreatmentProcedureCriterion criterion) throws IllegalArgumentException,
			UnsupportedCriterionException
	{
		// Validate parameter
		if ((criterion == null) || !SURGERY.equals(criterion.getParent()))
			throw new IllegalArgumentException("criterion is not surgery: " + criterion);
		SetOperation setOperation = criterion.getSetOperation();
		if ((setOperation == null) || (setOperation == SetOperation.SUBSET))
			throw new UnsupportedCriterionException(criterion, "set operation " + setOperation);

		// Store instance variables
		invert = criterion.isInvert();
		boolean isMostRecentMalignancy = false;
		//boolean bilateral = false;
		// Ensure that only supported qualifiers are present
		if (criterion.getQualifiers() != null)
			for (CharacteristicCode qualifier : criterion.getQualifiers())
				if (MOSTRECENT.equals(qualifier))
					isMostRecentMalignancy = true;
				//else if (BILATERAL_SYNCHRONOUS.equals(qualifier))
					//bilateral = true;
				else if (qualifier != null)
					throw new UnsupportedCriterionException(criterion, "qualifier " + qualifier);
		recentMalignancy = isMostRecentMalignancy;
		// Convert optional maximum elapsed time using units
		int maximumWeeks = criterion.getMaximumElapsedTime();
		if (maximumWeeks > 0)
		{
			if (MONTH.equals(criterion.getMaximumElapsedTimeUnits()))
				maximumWeeks *= 4;
			else if (!WEEK.equals(criterion.getMaximumElapsedTimeUnits()))
				throw new UnsupportedCriterionException(criterion, "maximum elapsed unit of measure "
						+ criterion.getMaximumElapsedTimeUnits());
		}
		maximumElapsedWeeks = maximumWeeks;

		// If no requirement then use "any site" set
		CharacteristicCode[] requirement = criterion.getRequirement();
		if ((requirement == null) || (requirement.length == 0))
			requirement = CONVERTER.keySet().toArray(new CharacteristicCode[CONVERTER.size()]);
		try
		{
			// Recursively convert surgical procedures from declarative into
			// imperative objects
			procedureMatchable = createMatchable(requirement, setOperation);
		}
		catch (UnsupportedCriterionException unsupportedCriterionException)
		{
			// Add the criterion into recast exception because the
			// createMatchable methods do not have access to the criterion
			throw new UnsupportedCriterionException(criterion, unsupportedCriterionException.getMessage());
		}
	}

	/**
	 * Returns the match results from the hierarchical imperative objects after
	 * passing in the surgical procedure history and calculating cut-off dates
	 * if specified. This method only considers surgical procedures. The
	 * optional cut-off dates that this method supports are:
	 * <ul>
	 * <li>Most recent: the surgical procedure is not earlier than the most
	 * recent cancer diagnosis</li>
	 * <li>Earliest objective date: the surgical procedure is not earlier than
	 * the current date minus the maximum elapsed time (adjusted for some
	 * lee-way by a precision factor)</li>
	 * <li>Earliest subjective-pass date: if the surgical procedure is earlier
	 * than the current date minus the maximum subjective-pass time then the
	 * result is subjective-fail. Otherwise the result is subjective-pass. This
	 * functionality is meant to alert the user that a maximum-elapsed qualifier
	 * might soon make the patient ineligible for the trial. It uses a more
	 * serious result (subjective-fail) if the patient is closer to
	 * disqualification.</li>
	 * </ul>
	 * 
	 * @return the match results from the hierarchical imperative objects or
	 *         <code>null</code> if the <code>history</code> or
	 *         <code>procedureMatchable</code> is <code>null</code>
	 * @param history
	 *            the patient's history
	 */
	private Map<Procedure, Result> getResults(PatientHistory history)
	{
		// Validate parameter and matching logic
		if ((history == null) || (procedureMatchable == null))
			return null;

		// Most recent qualifier: get date of diagnosis if available
		YearMonth diagnosis = recentMalignancy ? history.getYearMonthHistory().get(DIAGNOSISBREAST) : null;

		// Maximum elapsed time qualifier: calculate objective and subjective
		// thresholds
		YearMonth earliestObjectiveDate = null;
		YearMonth earliestSubjectivePassDate = null;
		if (maximumElapsedWeeks > 0)
		{
			// Allow a little bit of lee-way (precision) for absolute cut-off
			long millis = System.currentTimeMillis()
					- ((maximumElapsedWeeks + MAXIMUMWEEKS_PRECISION) * MILLISECONDS_PER_WEEK);
			earliestObjectiveDate = new YearMonth();
			earliestObjectiveDate.setTime(millis);
			// Divide passing date range into SUBJECTIVE_PASS and
			// SUBJECTIVE_FAIL zones
			earliestSubjectivePassDate = new YearMonth();
			earliestSubjectivePassDate.setTime(millis + (MAXIMUMWEEKS_SUBJECTIVE * MILLISECONDS_PER_WEEK));
		}

		// Match surgical procedures: use a new collection that can be pruned as
		// procedures are found, pass in date qualifiers if specified
		HashMap<Procedure, Result> results = new HashMap<Procedure, Result>(history.getProcedures().size(), 1.0f);
		// Only match to surgery procedures
		//Check for Bi lateral Mastectomy by checking the procedures
		List<CharacteristicCode> mastectomyList = Arrays.asList(OLD_MASTECTOMY_SET);
		boolean isBilateral =false;
		YearMonth mastectomyDate = null;
		YearMonth therapeuticDate = null;
		Procedure biLatProcedure = null;
		for (Procedure proc : history.getProcedures())
		{	
			if (SURGERY.equals(proc.getKind()))
			{
				if(mastectomyList.contains(proc.getType()))
				{	
					//System.out.println(" Procedure xm: "+mastectomyDate);
					//System.out.println(" Procedure x: "+proc.getStarted());
					//if(proc.getStarted() != null && convertYearMonthToString(proc.getStarted()).equals(mastectomyDate))
					if(proc.getStarted() != null && mastectomyDate != null)
					{
						if(MASTECTOMY_THERAPEUTIC.equals(proc.getType()))
							therapeuticDate = proc.getStarted();
						//System.out.println(" Procedure : 2");
						isBilateral = true;						
					} else
					{
						//System.out.println(" Procedure : 1");
						mastectomyDate = proc.getStarted();
						if(MASTECTOMY_THERAPEUTIC.equals(proc.getType()))
							therapeuticDate = proc.getStarted();
					}											
				}
			}
		}
		
		if(isBilateral)
		{
			biLatProcedure = new Procedure();
			biLatProcedure.setStarted(therapeuticDate==null?mastectomyDate:therapeuticDate);
			biLatProcedure.setKind(SURGERY);
			biLatProcedure.setType(MASTECTOMY_BILATERAL);
			biLatProcedure.setLocation(LEFT_BREAST);
		}
		for (Procedure procedure : history.getProcedures())
		{	
			if (SURGERY.equals(procedure.getKind()))
			{	
				if(!isBilateral || (isBilateral && !(MASTECTOMY_THERAPEUTIC.equals(procedure.getType())) && !(MASTECTOMY_PROPHYLACTIC.equals(procedure.getType()))))
				{
					//System.out.println("Procedure 111: "+procedure.getType());

					if((MASTECTOMY_THERAPEUTIC.equals(procedure.getType())) || (MASTECTOMY_PROPHYLACTIC.equals(procedure.getType())))
					{
						Procedure mastProcedure = new Procedure();
						mastProcedure.setStarted(procedure.getStarted());
						mastProcedure.setKind(SURGERY);
						mastProcedure.setType(MASTECTOMY_UNILATERAL);
						mastProcedure.setLocation(procedure.getLocation());
						results.put(mastProcedure, procedureMatchable.match(mastProcedure, diagnosis, earliestObjectiveDate, earliestSubjectivePassDate, history.isAvatar()));

					}
					else
						results.put(procedure, procedureMatchable.match(procedure, diagnosis, earliestObjectiveDate, earliestSubjectivePassDate, history.isAvatar()));
				}
			}	
		}
		if(isBilateral)
		{
			//System.out.println(" Bilat Procedure x: "+biLatProcedure.getStarted());
			//System.out.println("Result: "+ procedureMatchable.match(biLatProcedure, diagnosis, earliestObjectiveDate, earliestSubjectivePassDate, history.isAvatar()));
			results.put(biLatProcedure, procedureMatchable.match(biLatProcedure, diagnosis, earliestObjectiveDate, earliestSubjectivePassDate, history.isAvatar()));
		}
		return results;
	}

	/**
	 * Returns the match results from the hierarchical imperative objects after
	 * passing in the surgical procedure history and calculating cut-off dates
	 * if specified. The optional cut-off dates that this method supports are:
	 * <ul>
	 * <li>Most recent: the surgical procedure is not earlier than the most
	 * recent cancer diagnosis</li>
	 * <li>Earliest objective date: the surgical procedure is not earlier than
	 * the current date minus the maximum elapsed time (adjusted for some
	 * lee-way by a precision factor)</li>
	 * <li>Earliest subjective-pass date: if the surgical procedure is earlier
	 * than the current date minus the maximum subjective-pass time then the
	 * result is subjective-fail. Otherwise the result is subjective-pass. This
	 * functionality is meant to alert the user that a maximum-elapsed qualifier
	 * might soon make the patient ineligible for the trial. It uses a more
	 * serious result (subjective-fail) if the patient is closer to
	 * disqualification.</li>
	 * </ul>
	 * 
	 * @return the match result from the hierarchical imperative objects or
	 *         <code>FAIL</code> if results not available
	 * @param history
	 *            the patient's history
	 * @see org.quantumleaphealth.screen.Matchable#matchDescriptive(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	public Result matchDescriptive(PatientHistory history)
	{
		return getResult(getResults(history), history);
	}

	/**
	 * @param results
	 *            the match results for each surgical procedure or
	 *            <code>null</code> if results not available
	 * @return the overall match result or <code>FAIL</code> if results not
	 *         available
	 */
	private Result getResult(Map<Procedure, Result> results, PatientHistory history )
	{
		// If results are not available then fail
		if (results == null)
		{
			if ( history != null && history.isAvatar() )
			{
				return Result.AVATAR_SUBJECTIVE;
			}
			return Result.FAIL;
		}

		// Get the best result.  FAIL is the worst result for a normal patient, AVATAR_SUBJECTIVE is the
		// worst result for an avatar.
//		Result best = history != null && history.isAvatar()? Result.AVATAR_SUBJECTIVE: Result.FAIL;
		Result best = Result.FAIL;
		for (Map.Entry<Procedure, Result> entry : results.entrySet())
			best = best.better(entry.getValue());
		return invert ? best.invert() : best;
	}

	/**
	 * Returns a list of the matching procedures
	 * 
	 * @return a list of the matching procedures
	 * @see org.quantumleaphealth.screen.Matchable#getCharacteristic(org.quantumleaphealth.model.patient.PatientHistory,
	 *      java.util.ResourceBundle)
	 */
	public List<CharacteristicResult> getCharacteristicResults(PatientHistory history, ResourceBundle resourceBundle)
	{
		// Get how matching procedures matched to criteria
		Map<Procedure, Result> results = getResults(history);
		if ((results == null) || results.isEmpty())
			return Arrays.asList(new CharacteristicResult("No surgical procedures", getResult(results, history)));

		// Print each procedure into a string builder corresponding to its
		// result
		Map<Result, StringBuilder> builders = new HashMap<Result, StringBuilder>(Result.values().length, 1.0f);
		for (Result value : Result.values())
			builders.put(value, new StringBuilder());
		for (Map.Entry<Procedure, Result> entry : results.entrySet())
		{
			StringBuilder builder = builders.get(entry.getValue());
			if (builder == null)
				throw new IllegalStateException("Cannot find builder for " + entry.getValue());
			if (builder.length() > 0)
				builder.append(", ");
			// In resource bundle, location/type are specified together
			builder.append(getString(entry.getKey().getLocation().toString() + '.' + entry.getKey().getType(),
					resourceBundle));
			if (entry.getKey().getStarted() != null)
			{
				builder.append(' ');
				if (entry.getKey().getStarted().getMonth() > 0)
				{
					builder.append(entry.getKey().getStarted().getMonth());
					builder.append('/');
				}
				builder.append(entry.getKey().getStarted().getYear());
			}
		}
		// Concatenate all procedures of a particular result using a prefix
		StringBuilder builder = new StringBuilder();
		concatenate(builder, builders, Result.PASS, "Applicable surgeries: ");
		concatenate(builder, builders, Result.FAIL, "Unapplicable surgeries: ");
		concatenate(builder, builders, Result.PASS_SUBJECTIVE, "Surgeries within time-frame: ");
		concatenate(builder, builders, Result.FAIL_SUBJECTIVE, "Surgeries almost out of time-frame: ");
		// Return the concatenated string with the overall result
		return Arrays.asList(new CharacteristicResult(builder.toString(), getResult(results, history)));
	}

	/**
	 * @param key
	 *            a key
	 * @param resourceBundle
	 *            a resource bundle
	 * @return the text in the resource bundle that corresponds to the key
	 */
	private static String getString(String key, ResourceBundle resourceBundle)
	{
		if (key == null)
			return "???";
		try
		{
			return resourceBundle.getString(key);
		}
		catch (Throwable throwable)
		{
			return "???" + key + "???";
		}
	}

	/**
	 * Adds the strings for a result
	 * 
	 * @param builder
	 *            contains all of the strings
	 * @param result
	 *            the result to find within the map
	 * @param map
	 *            contains a string for each result
	 * @param label
	 *            an optional label to prepend to the string
	 */
	private static void concatenate(StringBuilder builder, Map<Result, StringBuilder> map, Result result, String label)
	{
		StringBuilder value = (map == null) || (result == null) || (builder == null) ? null : map.get(result);
		if ((value == null) || (value.length() == 0))
			return;
		if (builder.length() > 0)
			builder.append("; ");
		if (label != null)
			builder.append(label);
		builder.append(value);
		map.remove(result);
	}

	/**
	 * Returns a string representation containing the values of the instance
	 * variables
	 * 
	 * @return a string representation containing the values of the instance
	 *         variables
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder(super.toString());
		builder.append("{matchable=").append(procedureMatchable);
		if (invert)
			builder.append(",inverted");
		if (recentMalignancy)
			builder.append(",recentMalignancy");
		if (maximumElapsedWeeks > 0)
			builder.append(",maximumElapsedWeeks=").append(maximumElapsedWeeks);
		return builder.append('}').toString();
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = 2909135053984286768L;
}
