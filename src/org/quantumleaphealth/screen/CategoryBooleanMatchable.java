/**
 * (c) 2008 Quantum Leap Healthcare Collaborative
 * All Rights Reserved
 */
package org.quantumleaphealth.screen;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREAST_CANCER;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.BooleanCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.screen.Matchable.Result;

/**
 * Match to a value in the patient's <code>valueHistory</code> map under the
 * <code>BREAST_CANCER</code> key, a.k.a. the patient's "category".
 * 
 * @author Tom Bechtold
 * @version 2008-07-08
 */
public class CategoryBooleanMatchable extends BooleanMatchable
{
	/**
	 * Store instance variables
	 * 
	 * @param booleanCriterion
	 *            the criterion
	 * @throws IllegalArgumentException
	 *             if parameter is <code>null</code>
	 */
	public CategoryBooleanMatchable(BooleanCriterion booleanCriterion) throws IllegalArgumentException
	{
		super(booleanCriterion);
	}

	@Override
	public Result matchDescriptive(PatientHistory history)
	{
		if ( history != null && history.isAvatar() && history.getValueHistory().getValue(BREAST_CANCER) == null)
		{
			return Result.AVATAR_SUBJECTIVE;
		}
		// If either invert or characteristic is set then it passes
		return (criterion.isInvert() ^ isCharacteristic(history)) ? Result.PASS : Result.FAIL;
	}

	/**
	 * @return whether the value stored in the patient's history under the
	 *         <code>BREAST_CANCER</code> key equals the criterion's
	 *         <code>characteristicCode</code>.
	 * @param history
	 *            the patient's history
	 * @see org.quantumleaphealth.screen.BooleanMatchable#isCharacteristic(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	@Override
	protected boolean isCharacteristic(PatientHistory history)
	{
		return criterion.getCharacteristicCode().equals(history.getValueHistory().getValue(BREAST_CANCER));
	}

	@Override
	public String toString()
	{
		return super.toString();
	}
	
	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -3298659195713737051L;
}
