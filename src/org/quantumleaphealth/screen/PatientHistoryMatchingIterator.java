/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import org.quantumleaphealth.model.trial.Trial;

/**
 * Returns trials that match a patient's history sorted in ascending order on
 * <code>Trial.id</code>.
 * 
 * @author Tom Bechtold
 * @version 2008-02-19
 * @see org.quantumleaphealth.model.trial.Trial
 */
public interface PatientHistoryMatchingIterator
{

	/**
	 * Returns the next trial that matches the patient's history in ascending
	 * order of <code>Trial.id</code> or <code>null</code> if there are no more
	 * matching trials
	 * 
	 * @return the next trial that matches the patient's history or
	 *         <code>null</code> if there are no more matching trials
	 */
	Trial nextMatch();
}
