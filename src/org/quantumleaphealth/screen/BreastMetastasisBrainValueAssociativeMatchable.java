/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREASTMETASTASIS_BRAIN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREASTMETASTASIS_BRAIN_AGGREGATORS;

/**
 * Matches a patient's cancer stage diagnosis characteristics to stage
 * eligibility criterion.
 * 
 * @author Tom Bechtold
 * @version 2008-06-20
 */
public class BreastMetastasisBrainValueAssociativeMatchable extends ValueAssociativeMatchable
{

	/**
	 * @param criterion
	 * @throws IllegalArgumentException
	 */
	public BreastMetastasisBrainValueAssociativeMatchable(AssociativeCriterion criterion)
			throws IllegalArgumentException
	{
		super(criterion, BREASTMETASTASIS_BRAIN_AGGREGATORS);
		if (!BREASTMETASTASIS_BRAIN.equals(criterion.getParent()))
			throw new IllegalArgumentException("criterion is not for brain mets: " + criterion);
	}

	/**
	 * @return the breast metastases/brain values that are found in the
	 *         patient's diagnoses
	 * @see org.quantumleaphealth.screen.ValueAssociativeMatchable#getValue(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	@Override
	protected CharacteristicCode getValue(PatientHistory history)
	{
		if (history == null)
			return null;
		for (Diagnosis diagnosis : history.getDiagnoses())
			if (diagnosis.getValueHistory().containsKey(BREASTMETASTASIS_BRAIN))
				return diagnosis.getValueHistory().getValue(BREASTMETASTASIS_BRAIN);
		return null;
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = 7810163581908183166L;
}
