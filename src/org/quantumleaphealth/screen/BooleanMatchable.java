/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import java.util.Arrays;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.BooleanCriterion;

/**
 * Enables a characteristic to be matchable against a patient's history.
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 */
public class BooleanMatchable extends Matchable
{
	/**
	 * The criterion
	 */
	protected final BooleanCriterion criterion;

	/**
	 * Validate and store the instance variable
	 * 
	 * @param criterion
	 *            the boolean criterion to represent
	 * @throws IllegalArgumentException
	 *             if parameter is <code>null</code>
	 */
	public BooleanMatchable(BooleanCriterion criterion) throws IllegalArgumentException
	{
		// Validate and store the instance variables
		if ((criterion == null) || (criterion.getCharacteristicCode() == null))
			throw new IllegalArgumentException("must specify criterion and code");
		this.criterion = criterion;
	}

	/**
	 * Returns how the patient's boolean characteristic matches to the
	 * requirement. This method see whether the characteristic is set and
	 * applies the invert.
	 * 
	 * @return how the characteristic matches the requirement
	 * @see org.quantumleaphealth.screen.Matchable#match(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	public Result matchDescriptive(PatientHistory history)
	{
		if ( history != null && history.isAvatar() && !history.getBinaryHistory().containsCharacteristic(criterion.getCharacteristicCode()) )
		{
			return Result.AVATAR_SUBJECTIVE;
		}
		// If either invert or characteristic is set then it passes
		return (criterion.isInvert() ^ isCharacteristic(history)) ? Result.PASS : Result.FAIL;
	}

	/**
	 * @param history
	 *            the patient's history
	 * @return whether the history contains the boolean characteristic
	 */
	protected boolean isCharacteristic(PatientHistory history)
	{
		if(criterion.getCharacteristicCode().getCode() == 1513040)
			return Boolean.TRUE;
		
		return history.getBinaryHistory().containsCharacteristic(criterion.getCharacteristicCode());
	}

	/**
	 * @param history
	 *            the patient's history
	 * @param resourceBundle
	 *            contains localized descriptions
	 * @return localized description of the characteristic and matching result
	 * @see org.quantumleaphealth.screen.Matchable#getCharacteristic(org.quantumleaphealth.model.patient.PatientHistory,
	 *      java.util.ResourceBundle)
	 */
	public List<CharacteristicResult> getCharacteristicResults(PatientHistory history, ResourceBundle resourceBundle)
	{
		String description = null;
		try
		{
			if (resourceBundle != null)
				description = resourceBundle.getString(criterion.getCharacteristicCode().toString());
		}
		catch (MissingResourceException missingResourceException)
		{
		}
		if (description == null)
			description = "criterion not recognized " + criterion.getCharacteristicCode();
		else if (!isCharacteristic(history))
			description = "No/t " + description;
		return Arrays.asList(new CharacteristicResult(description, matchDescriptive(history)));
	}

	@Override
	public String toString()
	{
		if (criterion != null)
		{	
			return criterion.toString();
		}
		return super.toString();
	}
	
	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -3473865585069835841L;
}
