/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.ComparativeCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LYMPH_NODE_COUNT;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.LEFT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RIGHT_BREAST;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AFFIRMATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.NEGATIVE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;

/**
 * An imperative criterion that matches a patient's lymph node characteristics
 * to a count requirement.
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 */
public class LymphNodeCountMatchable extends DiagnosisMatchable
{
	/**
	 * The requirement
	 */
	private final double requirement;
	/**
	 * Whether or not the requirement is a maximum
	 */
	private final boolean maximum;

	/**
	 * Validates and store the instance variables
	 * 
	 * @param lymphCountCriterion
	 *            the lymph node count eligibility criterion
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code> or the criterion is not
	 *             a lymph node count criterion or the criterion's unit of
	 *             measure is not <code>null</code>
	 */
	public LymphNodeCountMatchable(ComparativeCriterion lymphCountCriterion) throws IllegalArgumentException
	{
		if ((lymphCountCriterion == null) || !LYMPH_NODE_COUNT.equals(lymphCountCriterion.getCharacteristicCode())
				|| (lymphCountCriterion.getUnitOfMeasure() != null))
			throw new IllegalArgumentException("criterion is not for absolute lymph node count: " + lymphCountCriterion);
		requirement = lymphCountCriterion.getRequirement();
		maximum = lymphCountCriterion.isInvert();
	}

	/**
	 * Returns how the patient's lymph count characteristic matches to the
	 * requirement. If the requirement is a maximum then use the worse lateral
	 * result; if minimum use the better lateral result.
	 * 
	 * @return how the lymph count characteristic matches the requirement
	 * @param history
	 *            the patient's history
	 * @see org.quantumleaphealth.screen.Matchable#match(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	public Result matchDescriptive(PatientHistory history)
	{
		// Validate history and lateral diagnoses
		if (history == null)
			return Result.FAIL;
		// If maximum then use worse lateral result, if minimum use better
		// lateral result
		Result left = matchDescriptive(getDiagnosis(history, LEFT_BREAST), history);
		Result right = matchDescriptive(getDiagnosis(history, RIGHT_BREAST), history);
		return maximum ? worse(left, right) : better(left, right);
	}

	/**
	 * Returns how a lateral diagnosis matches a lymph node count requirement.
	 * The logic in this method depends upon whether the requirement is a
	 * maximum (e.g., the criterion is inverted). If the laterality has no
	 * diagnosis then a maximum requirement passes and a minimum requirement
	 * fails. If unsure then the result is a subjective pass. Otherwise compare
	 * the number against the requirement.
	 * 
	 * @return how a lateral diagnosis matches a maximum lymph node count
	 *         requirement
	 * @param diagnosis
	 *            the diagnosis
	 */
	private Result matchDescriptive(Diagnosis diagnosis, PatientHistory history)
	{
		// Not diagnosed: maximum requirement means pass, minimum requirement
		// means fail
		if (diagnosis == null)
		{	
			if ( history != null && history.isAvatar() )
			{
				return Result.AVATAR_SUBJECTIVE;
			}
			return maximum ? Result.PASS : Result.FAIL;
		}	
		// Not sure if tested means subjective pass
		CharacteristicCode tested = diagnosis.getValueHistory().getValue(LYMPH_NODE_COUNT);
		if (tested == null && history.isAvatar())
		{
			return Result.AVATAR_SUBJECTIVE;
		}
		
		if (UNSURE.equals(tested))
			return Result.PASS_SUBJECTIVE;
		// Get the count if available
		double count = 0d;
		if (AFFIRMATIVE.equals(tested))
		{
			Short nodeCount = diagnosis.getShortHistory().getShort(LYMPH_NODE_COUNT);
			// BCT-320 If the count is blank,
			// consider this a FAIL if the requirement is a maximum of zero,
			// consider this a PASS if the requirement is a minimum of 1,
			// otherwise consider it a PASS_SUBJECTIVE
			if ((nodeCount == null) || (nodeCount.shortValue() <= 0))
			{
				if (maximum && (requirement == 0))
					return Result.FAIL;
				else if (!maximum && (requirement == 1))
					return Result.PASS;
				else
					return Result.PASS_SUBJECTIVE;
			}
			count = nodeCount.doubleValue();
		}
		// Compare the count to the requirement
		if (maximum)
			return (count <= requirement) ? Result.PASS : Result.FAIL;
		return (count >= requirement) ? Result.PASS : Result.FAIL;
	}

	/**
	 * Localized description(s) and results of the lymph count characteristics.
	 * This method describes each lateral diagnosis.
	 * 
	 * @param history
	 *            the patient's history
	 * @param resourceBundle
	 *            contains localized descriptions
	 * @return localized description(s) and results of the lymph count
	 *         characteristics
	 * @see org.quantumleaphealth.screen.Matchable#getCharacteristicResults(PatientHistory,
	 *      ResourceBundle)
	 */
	public List<CharacteristicResult> getCharacteristicResults(PatientHistory history, ResourceBundle resourceBundle)
	{
		StringBuilder builder = new StringBuilder();
		addDiagnosis(builder, getDiagnosis(history, LEFT_BREAST), "left breast");
		addDiagnosis(builder, getDiagnosis(history, RIGHT_BREAST), "right breast");
		if (builder.length() == 0)
			builder.append("lymph node count not found");
		return Arrays.asList(new CharacteristicResult(builder.toString(), matchDescriptive(history)));
	}

	/**
	 * Append the localized description of the patient's lymph node count
	 * characteristic to a string builder. This method adds either
	 * <ul>
	 * <li>"I'm not sure"</li>
	 * <li>"Not tested"</li>
	 * <li>"Lymph node count: n"</li>
	 * <li><code>null</code> (no diagnosis or none-of-the-above)</li>
	 * </ul>
	 * 
	 * @param stringBuilder
	 *            the builder to append to
	 * @param diagnosis
	 *            the patient's lateral diagnosis
	 * @param name
	 *            the localized name of the diagnosis
	 */
	private static void addDiagnosis(StringBuilder stringBuilder, Diagnosis diagnosis, String name)
	{
		if ((stringBuilder == null) || (diagnosis == null))
			return;
		if (stringBuilder.length() > 0)
			stringBuilder.append("; ");
		if (name != null)
			stringBuilder.append(name).append(' ');
		CharacteristicCode tested = getDiagnosisValue(diagnosis, LYMPH_NODE_COUNT);
		stringBuilder.append("lymph node count ");
		if (UNSURE.equals(tested))
			stringBuilder.append("unsure");
		else if (NEGATIVE.equals(tested))
			stringBuilder.append("0/not tested");
		else if (AFFIRMATIVE.equals(tested))
		{
			// Not sure of the count means subjective pass
			Short nodeCount = diagnosis.getShortHistory().getShort(LYMPH_NODE_COUNT);
			if ((nodeCount == null) || (nodeCount.shortValue() <= 0))
				stringBuilder.append("unsure");
			else
				stringBuilder.append(nodeCount);
		}
		else
			stringBuilder.append("unavailable");
	}

	/**
	 * Verion uid for serializable class
	 */
	private static final long serialVersionUID = 6538797672802273057L;
}
