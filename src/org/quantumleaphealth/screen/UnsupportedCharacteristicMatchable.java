/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import java.util.List;
import java.util.ResourceBundle;

import org.quantumleaphealth.model.patient.PatientHistory;

/**
 * Returns a subjective-pass matching result a <code>null</code> patient
 * characteristic for any patient history. It is an imperative placeholder for a
 * declarative criterion whose relevant patient characteristic(s) cannot be
 * found in the patient's history. Its optimistic-result strategy supports the
 * philosophy of no false-negative matches.
 * <p>
 * Objects from this class can be used for eligibility criteria that might be
 * supported by future or other matching engines.
 * </p>
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 */
public class UnsupportedCharacteristicMatchable extends Matchable
{
	/**
	 * @param history
	 *            ignored since the relevant characteristic cannot be found in
	 *            the history
	 * @return <code>Result.PASS_SUBJECTIVE</code>
	 * @see org.quantumleaphealth.screen.Matchable#matchDescriptive(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	public Result matchDescriptive(PatientHistory history)
	{
		return Result.PASS_SUBJECTIVE;
	}

	/**
	 * @return <code>null</code>
	 * @param history
	 *            ignored since the relevant characteristic cannot be found in
	 *            the history
	 * @param resourceBundle
	 *            ignored
	 * @see org.quantumleaphealth.screen.Matchable#getCharacteristic(org.quantumleaphealth.model.patient.PatientHistory,
	 *      java.util.ResourceBundle)
	 */
	public List<CharacteristicResult> getCharacteristicResults(PatientHistory history, ResourceBundle resourceBundle)
	{
		return null;
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -7854748055412075469L;
}
