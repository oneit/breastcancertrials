/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import java.util.HashSet;
import java.util.Set;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.TUMOR_STAGE_FINDING;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;

/**
 * Matches a patient's tumor stage diagnosis characteristics to an eligibility
 * criterion. This class matches the requirement to a union of the diagnoses'
 * tumor stage characteristics.
 * 
 * @author Tom Bechtold
 * @version 2008-06-20
 */
public class TumorStageAssociativeMatchable extends AttributeAssociativeMatchable
{

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -2705697385378015036L;

	/**
	 * Validates and stores the instance variable
	 * 
	 * @param criterion
	 *            the criterion
	 * @throws IllegalArgumentException
	 *             if the criterion is <code>null</code> or not tumor stage
	 */
	public TumorStageAssociativeMatchable(AssociativeCriterion criterion) throws IllegalArgumentException
	{
		super(criterion);
		if (!TUMOR_STAGE_FINDING.equals(criterion.getParent()))
			throw new IllegalArgumentException("criterion is not for tumor stage: " + criterion);
	}

	/**
	 * @return a union of the diagnoses' tumor stage characteristics
	 * @see org.quantumleaphealth.screen.AttributeAssociativeMatchable#getAttribute(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	@Override
	protected Set<CharacteristicCode> getAttribute(PatientHistory history)
	{
		if (history == null)
			return null;
		// Build a union from each diagnosis
		Set<CharacteristicCode> set = new HashSet<CharacteristicCode>(1);
		for (Diagnosis diagnosis : history.getDiagnoses())
		{
			CharacteristicCode tumorStage = diagnosis.getValueHistory().get(TUMOR_STAGE_FINDING);
			if (tumorStage != null)
				set.add(tumorStage);
		}
		return set.isEmpty() ? null : set;
	}

	/**
	 * Returns how the patient's attribute characteristic matches to the
	 * associative requirement. This method first tests for subjective matching,
	 * then calls the superclass for objective matching. This method's algorithm
	 * then depends upon the <code>comparator</code>:
	 * <ul>
	 * <li><code>SUBSET</code>: <code>true</code> if each characteristic in the
	 * characteristic is also present in the requirement
	 * <li><code>SUPERSET</code>: <code>true</code> if each characteristic in
	 * the requirement is also present in the characteristic
	 * <li><code>INTERSECT</code> (default): <code>true</code> if any
	 * characteristic is present in both the characteristic and the requirement.
	 * </ul>
	 * Finally, the invert is applied to the result.
	 * 
	 * @return how the characteristic matches the requirement
	 * @see org.quantumleaphealth.screen.AttributeAssociativeMatchable#matchDescriptive(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	@Override
	public Result matchDescriptive(PatientHistory history)
	{
		// Check for subjective first
		Set<CharacteristicCode> attributes = getAttribute(history);
		if ((attributes != null) && attributes.contains(UNSURE))
			return criterion.isInvert() ? Result.FAIL_SUBJECTIVE : Result.PASS_SUBJECTIVE;

		if ( attributes == null && history.isAvatar() )
		{
			return Result.AVATAR_SUBJECTIVE;
		}
		
		return super.matchDescriptive(history);
	}
}
