package org.quantumleaphealth.screen;

import java.util.ArrayList;

public class ReportOutput 
{
	private static ArrayList<String> output = new ArrayList<String>();

	private static boolean debug = false;
	
	/**
	 * @param output the output to set
	 */
	private static void setOutput(ArrayList<String> output) 
	{
		ReportOutput.output = output;
	}

	
	public static void addOutput(String msg)
	{
		if (debug == true)
		{	
			output.add(msg);
		}	
	}
	
	/**
	 * To protect the class level buffer from possible outside modification return a copy of it.
	 * 
	 * @return The copy of the current log.
	 */
	public static ArrayList<String> getOutput()
	{
		return new ArrayList<String>(output);
	}
	
	public static void emptyOutput()
	{
		setOutput(new ArrayList<String>());
	}

	/**
	 * @param debug the debug to set
	 */
	public static void setDebug(boolean debug) 
	{
		ReportOutput.debug = debug;
	}
}
