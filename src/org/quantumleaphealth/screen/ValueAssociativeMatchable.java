/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.screen;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.trial.AssociativeCriterion;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.ontology.OntologyUtils;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;

/**
 * Enables a set of characteristics to be matchable against a single value in a
 * patient's history. TODO: Patient language for characteristics?
 * 
 * @author Tom Bechtold
 * @version 2008-07-09
 */
public class ValueAssociativeMatchable extends Matchable
{
	/**
	 * The criterion, guaranteed to be non-<code>null</code>
	 */
	protected final AssociativeCriterion criterion;
	/**
	 * The requirement to compare the patient's value history to
	 */
	protected final Set<CharacteristicCode> requirement;

	/**
	 * Store the instance variable using no aggregators
	 * 
	 * @param criterion
	 *            the criterion to represent
	 * @throws IllegalArgumentException
	 *             if <code>criterion</code> is <code>null</code>
	 */
	public ValueAssociativeMatchable(AssociativeCriterion criterion) throws IllegalArgumentException
	{
		// Call overloaded constructor with no aggregators
		this(criterion, null);
	}

	/**
	 * Store the instance variable and expand the requirement using aggregators.
	 * 
	 * @param criterion
	 *            the criterion to represent
	 * @param aggregators
	 *            optional aggregators that may make up the criterion's
	 *            requirement
	 * @throws IllegalArgumentException
	 *             if <code>criterion</code> is <code>null</code>
	 */
	public ValueAssociativeMatchable(AssociativeCriterion criterion,
			Map<CharacteristicCode, CharacteristicCode[]> aggregators) throws IllegalArgumentException
	{
		// Validate and store the instance variables
		if ((criterion == null) || (criterion.getParent() == null))
			throw new IllegalArgumentException("must specify criterion and its parent code");
		this.criterion = criterion;
		this.requirement = OntologyUtils.expand(criterion.getRequirement(), aggregators);
	}

	/**
	 * Returns how the patient's associative characteristic matches to the
	 * requirement. This method first sees if the patient's characteristic is
	 * subjective (e.g., stored as <code>UNSURE</code>). If so then it returns
	 * <code>PASS_SUBJECTIVE</code> or <code>FAIL_SUBJECTIVE</code> depending
	 * upon the invert. This method's algorithm then compares the value against
	 * the criterion's requirement. First, if the value is null then the match
	 * fails. If the requirement is null (e.g., "any") then the match
	 * automatically passes. Finally the value must be contained in the
	 * requirement.
	 * 
	 * @return how the characteristic matches the requirement
	 * @see org.quantumleaphealth.screen.Matchable#match(org.quantumleaphealth.model.patient.PatientHistory)
	 */
	public Result matchDescriptive(PatientHistory history)
	{
		// First check if the patient is subjective
		CharacteristicCode value = getValue(history);
		
		if ( value == null && history.isAvatar() )
		{
			return Result.AVATAR_SUBJECTIVE;			
		}
		
		if (UNSURE.equals(value))
			return criterion.isInvert() ? Result.FAIL_SUBJECTIVE : Result.PASS_SUBJECTIVE;
		// Intersection test: empty requirement means any member
		boolean match = (value != null) && ((requirement == null) || requirement.contains(value));
		// Apply invert
		return (criterion.isInvert() ^ match) ? Result.PASS : Result.FAIL;
	}

	/**
	 * @param history
	 *            the patient's history
	 * @return the patient's value that is stored in its value history under the
	 *         criterion's parent characteristic
	 */
	protected CharacteristicCode getValue(PatientHistory history)
	{
		return history == null ? null : history.getValueHistory().getValue(criterion.getParent());
	}

	/**
	 * Localized description of the characteristic
	 * 
	 * @param history
	 *            the patient's history
	 * @param resourceBundle
	 *            contains localized descriptions
	 * @return localized description of the characteristic
	 * @see org.quantumleaphealth.screen.Matchable#getCharacteristic(org.quantumleaphealth.model.patient.PatientHistory,
	 *      java.util.ResourceBundle)
	 */
	public List<CharacteristicResult> getCharacteristicResults(PatientHistory history, ResourceBundle resourceBundle)
	{
		// If history does not contain a characteristic for the parent then say
		// "no parent"
		CharacteristicCode value = getValue(history);
		String characteristic = value == null ? "No/t " + getString(criterion.getParent().toString(), resourceBundle)
				: getString(criterion.getParent().toString() + '.' + value.toString(), resourceBundle);
		LinkedList<CharacteristicResult> characteristicResults = new LinkedList<CharacteristicResult>();
		characteristicResults.add(new CharacteristicResult(characteristic, matchDescriptive(history)));
		return characteristicResults;
	}

	/**
	 * Returns the localized text
	 * 
	 * @param key
	 *            the key to lookup the text
	 * @param resourceBundle
	 *            contains localized text
	 * @return the localized text
	 */
	public static final String getString(String key, ResourceBundle resourceBundle)
	{
		try
		{
			return resourceBundle.getString(key);
		}
		catch (Exception exception)
		{
			return "???" + key + "???";
		}
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = 3159702951459936228L;
}
