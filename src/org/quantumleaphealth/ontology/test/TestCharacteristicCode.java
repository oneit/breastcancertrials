/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.ontology.test;

import java.util.Arrays;
import java.util.Random;

import org.quantumleaphealth.ontology.CharacteristicCode;
import org.testng.annotations.Test;

/**
 * Tests for the <code>CharacteristicCode</code> class.
 * 
 * @author Tom Bechtold
 * @version 2008-07-06
 * @see CharacteristicCode
 */
@Test(description = "Characteristic code", groups = { "ontology", "unit" })
public class TestCharacteristicCode
{

	/**
	 * Random-number generator
	 */
	private final Random generator = new Random();

	/**
	 * Test empty constructor
	 */
	public void testConstructor()
	{
		CharacteristicCode characteristicCode = new CharacteristicCode();
		int code = characteristicCode.getCode();
		assert (code == 0) : "Code " + code + " is not zero";
	}

	/**
	 * Test integer constructor
	 */
	public void testConstructorInteger()
	{
		int code = generator.nextInt();
		CharacteristicCode characteristicCode = new CharacteristicCode(code);
		int otherCode = characteristicCode.getCode();
		assert (code == otherCode) : "Code " + code + " is not " + otherCode;
	}

	/**
	 * Test setCode
	 */
	public void testSetGetCode()
	{
		int code = generator.nextInt();
		CharacteristicCode characteristicCode = new CharacteristicCode();
		characteristicCode.setCode(code);
		int otherCode = characteristicCode.getCode();
		assert (code == otherCode) : "Code " + code + " is not " + otherCode;
	}

	/**
	 * Test equals method
	 */
	public void testEquals()
	{
		int code = generator.nextInt();
		CharacteristicCode characteristicCode1 = new CharacteristicCode(code);
		CharacteristicCode characteristicCode2 = new CharacteristicCode(code);
		assert characteristicCode1.equals(characteristicCode2) : "Characteristic " + characteristicCode1
				+ " is not equal to " + characteristicCode2;
	}

	/**
	 * Test not equals method
	 */
	public void testNotEquals()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}
		CharacteristicCode characteristicCode1 = new CharacteristicCode(code);
		CharacteristicCode characteristicCode2 = new CharacteristicCode(otherCode);
		assert !characteristicCode1.equals(characteristicCode2) : "Characteristic " + characteristicCode1
				+ " is equal to " + characteristicCode2;
	}

	/**
	 * Test toString (description). The localization via resource bundle relies
	 * upon this method.
	 */
	public void testToString()
	{
		assert "C".equals(CharacteristicCode.PREFIX) : "Prefix is not C: " + CharacteristicCode.PREFIX;
		int code = generator.nextInt();
		CharacteristicCode characteristicCode = new CharacteristicCode(code);
		String description = characteristicCode.toString();
		assert description.equals("C" + code) : "Description " + description + " is not C" + code;
	}

	/**
	 * Test hashcode. <code>hashcode()</code> is used by HashSet and HashMap
	 * collections.
	 */
	public void testHashcode()
	{
		int code = generator.nextInt();
		CharacteristicCode characteristicCode = new CharacteristicCode(code);
		int hashcode = characteristicCode.hashCode();
		assert (code == hashcode) : "Hashcode " + hashcode + " is not " + code;
	}

	/**
	 * Test isMember found
	 */
	public void testIsMemberFound()
	{
		int code = generator.nextInt();
		CharacteristicCode characteristicCode = new CharacteristicCode(code);
		CharacteristicCode[] set = new CharacteristicCode[] { new CharacteristicCode(code) };
		assert characteristicCode.isMember(set) : "Set " + Arrays.toString(set) + " does not contain "
				+ characteristicCode;
	}

	/**
	 * Test isMember not found
	 */
	public void testIsMemberNotFound()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}
		CharacteristicCode characteristicCode = new CharacteristicCode(code);
		CharacteristicCode[] set = new CharacteristicCode[] { new CharacteristicCode(otherCode) };
		assert !characteristicCode.isMember(set) : "Set " + Arrays.toString(set) + " contains " + characteristicCode;
	}

	/**
	 * Test getIndex found
	 */
	public void testGetIndexFound()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}
		CharacteristicCode characteristicCode = new CharacteristicCode(code);
		CharacteristicCode[] set = new CharacteristicCode[] { new CharacteristicCode(otherCode),
				new CharacteristicCode(code) };
		assert (new CharacteristicCode(code).getIndex(set) == 1) : "The second element of set " + Arrays.toString(set)
				+ " is not " + characteristicCode;
	}

	/**
	 * Test getIndex not found
	 */
	public void testGetIndexNotFound()
	{
		int code = 0;
		int otherCode = 0;
		while (code == otherCode)
		{
			code = generator.nextInt();
			otherCode = generator.nextInt();
		}
		CharacteristicCode characteristicCode = new CharacteristicCode(code);
		CharacteristicCode[] set = new CharacteristicCode[] { new CharacteristicCode(otherCode) };
		assert (characteristicCode.getIndex(set) == -1) : "The set " + Arrays.toString(set) + " contains "
				+ characteristicCode;
	}
}
