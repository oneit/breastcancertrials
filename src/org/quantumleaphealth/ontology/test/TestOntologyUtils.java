/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.ontology.test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.ontology.OntologyUtils;
import org.testng.annotations.Test;

/**
 * Tests for the <code>OntologyUtils</code> class. As this class only references
 * the <code>CharacteristicCode</code> it is considered a "true" unit test.
 * 
 * @author Tom Bechtold
 * @version 2008-07-06
 * @see CharacteristicCode
 * @see OntologyUtils
 */
@Test(description = "Characteristic code", groups = { "ontology", "unit" })
public class TestOntologyUtils
{

	/**
	 * Random-number generator
	 */
	private final Random generator = new Random();

	/**
	 * @return three unique integers
	 */
	private int[] getThreeUniqueIntegers()
	{
		int[] unique = new int[3];
		while ((unique[0] == unique[1]) || (unique[0] == unique[2]) || (unique[1] == unique[2]))
		{
			unique[0] = generator.nextInt();
			unique[1] = generator.nextInt();
			unique[2] = generator.nextInt();
		}
		return unique;
	}

	/**
	 * Test getLongValue with empty array and element
	 */
	public void testGetLongValueArrayCharacteristicEmpty()
	{
		int code = generator.nextInt();
		long result = OntologyUtils.getLongValue(new CharacteristicCode[] {}, new CharacteristicCode(code));
		assert result == 0l : "Empty set returned long value of " + result + " for element " + code;
	}

	/**
	 * Test getLongValue with array and element found
	 */
	public void testGetLongValueArrayCharacteristicFound()
	{
		// Place element second among two different elements
		int[] unique = getThreeUniqueIntegers();
		CharacteristicCode[] set = new CharacteristicCode[] { new CharacteristicCode(unique[0]),
				new CharacteristicCode(unique[1]), new CharacteristicCode(unique[2]) };
		long result = OntologyUtils.getLongValue(set, new CharacteristicCode(unique[1]));
		assert result == 2l : "Set " + Arrays.toString(set) + " returned long value of " + result + " for element "
				+ unique[1];
	}

	/**
	 * Test getLongValue with array and element not found
	 */
	public void testGetLongValueArrayCharacteristicNotFound()
	{
		int[] unique = getThreeUniqueIntegers();
		CharacteristicCode[] set = new CharacteristicCode[] { new CharacteristicCode(unique[0]),
				new CharacteristicCode(unique[1]) };
		long result = OntologyUtils.getLongValue(set, new CharacteristicCode(unique[2]));
		assert result == 0l : "Set " + Arrays.toString(set) + " returned long value of " + result + " for element "
				+ unique[2];
	}

	/**
	 * Test getLongValue with empty array and member array
	 */
	public void testGetLongValueArrayArrayEmpty()
	{
		long result = OntologyUtils.getLongValue(new CharacteristicCode[] {}, new CharacteristicCode[] {});
		assert result == 0l : "Empty set returned long value of " + result + " for empty members";
	}

	/**
	 * Test getLongValue with array and member array found one element
	 */
	public void testGetLongValueArrayArraySingle()
	{
		int[] unique = getThreeUniqueIntegers();
		CharacteristicCode[] set = new CharacteristicCode[] { new CharacteristicCode(unique[0]),
				new CharacteristicCode(unique[1]), new CharacteristicCode(unique[2]) };
		long result = OntologyUtils.getLongValue(set, new CharacteristicCode[] { new CharacteristicCode(unique[1]) });
		assert result == 2l : "Set " + Arrays.toString(set) + " returned long value of " + result
				+ " for singleton member set with element " + unique[1];
	}

	/**
	 * Test getLongValue with array and member array found more than one element
	 */
	public void testGetLongValueArrayArrayMultiple()
	{
		int[] unique = getThreeUniqueIntegers();
		CharacteristicCode[] set = new CharacteristicCode[] { new CharacteristicCode(unique[0]),
				new CharacteristicCode(unique[1]), new CharacteristicCode(unique[2]) };
		long result = OntologyUtils.getLongValue(set, new CharacteristicCode[] { new CharacteristicCode(unique[2]),
				new CharacteristicCode(unique[0]) });
		assert result == 5l : "Set " + Arrays.toString(set) + " returned long value of " + result
				+ " for member set with elements " + unique[2] + " and " + unique[0];
	}

	/**
	 * Test getLongValue with array and member array not found any element
	 */
	public void testGetLongValueArrayArrayNotFound()
	{
		int[] unique = getThreeUniqueIntegers();
		CharacteristicCode[] set = new CharacteristicCode[] { new CharacteristicCode(unique[0]),
				new CharacteristicCode(unique[1]) };
		long result = OntologyUtils.getLongValue(set, new CharacteristicCode[] { new CharacteristicCode(unique[2]) });
		assert result == 0l : "Set " + Arrays.toString(set) + " returned long value of " + result
				+ " for singleton member set with element " + unique[2];
	}

	/**
	 * Test expand with empty array
	 */
	public void testExpandArrayEmpty()
	{
		CharacteristicCode[] nullArray = null;
		CharacteristicCode[] emptyArray = new CharacteristicCode[0];
		Map<CharacteristicCode, CharacteristicCode[]> map = new HashMap<CharacteristicCode, CharacteristicCode[]>(1,
				1.0f);
		map.put(new CharacteristicCode(generator.nextInt()), new CharacteristicCode[] { new CharacteristicCode(
				generator.nextInt()) });

		Set<CharacteristicCode> result = OntologyUtils.expand(nullArray, map);
		assert result == null : "Expanded null array is not null: " + result;
		result = OntologyUtils.expand(emptyArray, map);
		assert result == null : "Expanded empty array is not null: " + result;
	}

	/**
	 * Test expand with empty map
	 */
	public void testExpandMapEmpty()
	{
		CharacteristicCode[] array = new CharacteristicCode[] { new CharacteristicCode(generator.nextInt()) };
		List<CharacteristicCode> arrayAsList = Arrays.asList(array);

		// Null map should not expand
		Set<CharacteristicCode> result = OntologyUtils.expand(array, null);
		assert (result != null) && result.containsAll(arrayAsList) && arrayAsList.containsAll(result) : "Array "
				+ arrayAsList + " was expanded with null map to  " + result;

		// Empty map should not expand
		result = OntologyUtils.expand(array, new HashMap<CharacteristicCode, CharacteristicCode[]>(1, 1.0f));
		assert (result != null) && result.containsAll(arrayAsList) && arrayAsList.containsAll(result) : "Array "
				+ arrayAsList + " was expanded with empty map to  " + result;
	}

	/**
	 * Test expand with no map entry found
	 */
	public void testExpandNone()
	{
		int[] unique = getThreeUniqueIntegers();
		// Create list using first code
		CharacteristicCode[] array = new CharacteristicCode[] { new CharacteristicCode(unique[0]) };
		List<CharacteristicCode> arrayAsList = Arrays.asList(array);
		// Create map with one otherCode entry (e.g., non-matching)
		Map<CharacteristicCode, CharacteristicCode[]> map = new HashMap<CharacteristicCode, CharacteristicCode[]>(1,
				1.0f);
		map.put(new CharacteristicCode(unique[1]), new CharacteristicCode[] {
				new CharacteristicCode(generator.nextInt()), new CharacteristicCode(generator.nextInt()) });
		// Non-matching map should not expand the array
		Set<CharacteristicCode> result = OntologyUtils.expand(array, map);
		assert (result != null) && result.containsAll(arrayAsList) && arrayAsList.containsAll(result) : "Array "
				+ arrayAsList + " was expanded with non-matching map " + map + " to  " + result;
	}

	/**
	 * Test expand with map entry found
	 */
	public void testExpand()
	{
		int[] unique = getThreeUniqueIntegers();
		// Create list using first code0
		CharacteristicCode[] array = new CharacteristicCode[] { new CharacteristicCode(unique[0]) };

		// Create map with one entry to expand the code0 to code1 and code2
		Map<CharacteristicCode, CharacteristicCode[]> map = new HashMap<CharacteristicCode, CharacteristicCode[]>(1,
				1.0f);
		map.put(new CharacteristicCode(unique[0]), new CharacteristicCode[] { new CharacteristicCode(unique[1]),
				new CharacteristicCode(unique[2]) });

		// Map should expand the array
		List<CharacteristicCode> expectedResultAsList = Arrays.asList(new CharacteristicCode(unique[1]),
				new CharacteristicCode(unique[2]));
		Set<CharacteristicCode> result = OntologyUtils.expand(array, map);
		assert (result != null) && result.containsAll(expectedResultAsList) && expectedResultAsList.containsAll(result) : "Array "
				+ Arrays.toString(array)
				+ " was expanded with matching map "
				+ map
				+ " to  "
				+ result
				+ " instead of "
				+ expectedResultAsList;
	}
}
