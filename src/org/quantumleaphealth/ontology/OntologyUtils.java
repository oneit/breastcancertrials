/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.ontology;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Static utility methods for characteristic codes
 * 
 * @author Tom Bechtold
 * @version 2008-06-11
 */
public class OntologyUtils
{
	/**
	 * Returns the bit-wise representation of the position of a member within a
	 * set.
	 * <p>
	 * Example:
	 * <code>getLongValue({Monday, Tuesday, Wednesday, Thursday, Friday}, Tuesday) = 2</code>
	 * 
	 * @param set
	 *            the set
	 * @param member
	 *            the member to find within the set
	 * @return the bit-wise representation of the member within a set or
	 *         <code>0</code> if <code>member</code> is <code>null</code> or not
	 *         found
	 * @throws IllegalArgumentException
	 *             if <code>set</code> is <code>null</code> or larger than
	 *             <code>Long.SIZE</code>
	 * @see Long#SIZE
	 */
	public static long getLongValue(CharacteristicCode[] set, CharacteristicCode member)
			throws IllegalArgumentException
	{
		if (set == null)
			throw new IllegalArgumentException("set not specified");
		if (set.length >= Long.SIZE - 1)
			throw new IllegalArgumentException("set too large: " + set.length);
		if (member == null)
			return 0l;
		for (int index = 0; index < set.length; index++)
			if (member.equals(set[index]))
				return 1l << index;
		return 0l;
	}

	/**
	 * Returns the bit-wise representation of the positions of members within a
	 * set.
	 * <p>
	 * Example:
	 * <code>getLongValue({Monday, Tuesday, Wednesday, Thursday, Friday}, {Tuesday, Wednesday}) = 6</code>
	 * 
	 * @param set
	 *            the set
	 * @param members
	 *            the members to find within the set
	 * @return the bit-wise representation of the positions of members within a
	 *         set or <code>0</code> if <code>members</code> is empty or none
	 *         found within the set
	 * @throws IllegalArgumentException
	 *             if <code>set</code> is <code>null</code> or larger than
	 *             <code>Long.SIZE</code>
	 * @see Long#SIZE
	 */
	public static long getLongValue(CharacteristicCode[] set, CharacteristicCode[] members)
			throws IllegalArgumentException
	{
		if (set == null)
			throw new IllegalArgumentException("set not specified");
		if (set.length >= Long.SIZE - 1)
			throw new IllegalArgumentException("set too large: " + set.length);
		if ((members == null) || (members.length == 0))
			return 0l;
		long value = 0l;
		for (int memberIndex = 0; memberIndex < members.length; memberIndex++)
			if (members[memberIndex] != null)
				for (int index = 0; index < set.length; index++)
					if (members[memberIndex].equals(set[index]))
					{
						value += 1l << index;
						break;
					}
		return value;
	}

	/**
	 * Returns a set expanded by aggregators
	 * 
	 * @param set
	 *            the set to expand
	 * @param aggregators
	 *            the aggregators to expand it with
	 * @return a set expanded by aggregators or <code>null</code> if the set is
	 *         empty
	 */
	public static Set<CharacteristicCode> expand(CharacteristicCode[] set,
			Map<CharacteristicCode, CharacteristicCode[]> aggregators)
	{
		if ((set == null) || (set.length == 0))
			return null;
		LinkedHashSet<CharacteristicCode> linkedHashSet = new LinkedHashSet<CharacteristicCode>(set.length, 1f);
		for (CharacteristicCode member : set)
		{
			CharacteristicCode[] replacements = (aggregators == null) ? null : aggregators.get(member);
			if (replacements != null)
				for (CharacteristicCode replacement : replacements)
					linkedHashSet.add(replacement);
			else
				linkedHashSet.add(member);
		}
		return linkedHashSet;
	}
}
