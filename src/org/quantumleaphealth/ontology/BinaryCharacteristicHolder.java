/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.ontology;

/**
 * Holds characteristics that do not have children or attributes.
 * 
 * @author Tom Bechtold
 * @version 2008-05-15
 */
public interface BinaryCharacteristicHolder
{
	/**
	 * Adds a binary characteristic
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic code to add
	 */
	public void addCharacteristic(CharacteristicCode characteristicCode);

	/**
	 * Removes a binary characteristic
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to remove
	 */
	public void removeCharacteristic(CharacteristicCode characteristicCode);

	/**
	 * Returns whether the characteristic is present
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to find
	 * @return whether the characteristic is present
	 */
	public boolean containsCharacteristic(CharacteristicCode characteristicCode);
}
