/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.ontology;

import java.io.Serializable;

/**
 * References a patient characteristic. This class defines the code with
 * <code>int</code> precision and caches its description.
 * 
 * @author Tom Bechtold
 * @version 2008-08-12
 */
public class CharacteristicCode implements Serializable
{
	/**
	 * The code, defaults to zero
	 */
	private int code = 0;

	/**
	 * The cached description
	 * 
	 * @see #toString()
	 * @see #setCode(int)
	 */
	private String description;

	/**
	 * The prefix for the string representation of the code
	 */
	public static final String PREFIX = "C";

	/**
	 * Default constructor
	 */
	public CharacteristicCode()
	{
		this(0);
	}

	/**
	 * Stores the code
	 * 
	 * @param code
	 *            the code to set
	 */
	public CharacteristicCode(int code)
	{
		setCode(code);
	}

	/**
	 * @return the code
	 */
	public int getCode()
	{
		return code;
	}

	/**
	 * Sets the code and clears the cached description
	 * 
	 * @param code
	 *            the code to set
	 */
	public void setCode(int code)
	{
		this.code = code;
		this.description = null;
	}

	/**
	 * Returns the code
	 * 
	 * @see java.lang.Integer#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return code;
	}

	/**
	 * Returns <code>true</code> if the code matches
	 * 
	 * @param other
	 *            the object to compare equality
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other)
	{
		return (other != null) && (other instanceof CharacteristicCode) && ((CharacteristicCode) (other)).code == code;
	}

	/**
	 * @param code1
	 *            the first code
	 * @param code2
	 *            the second code
	 * @return whether two codes are equal
	 */
	public static boolean equals(CharacteristicCode code1, CharacteristicCode code2)
	{
		return (code1 == null) ? (code2 == null) : code1.equals(code2);
	}

	/**
	 * Returns this object is a member of a set
	 * 
	 * @param set
	 *            the set
	 * @return whether this object is a member of a set
	 */
	public boolean isMember(CharacteristicCode[] set)
	{
		return getIndex(set) >= 0;
	}

	/**
	 * Returns the index of this object in a set
	 * 
	 * @param set
	 *            the set
	 * @return the index of this object in a set or -1 if not a member
	 */
	public int getIndex(CharacteristicCode[] set)
	{
		if (set == null)
			return -1;
		for (int index = 0; index < set.length; index++)
			if (this.equals(set[index]))
				return index;
		return -1;
	}

	/**
	 * Returns the prefix and the code.
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		if (description == null)
			description = PREFIX + Integer.toString(code);
		return description;
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -1221833285984145172L;
}
