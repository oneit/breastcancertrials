/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.ontology;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Characteristic codes used for matching patients to breast cancer trials. This
 * class employs the Unified Medical Language System codes version 2007AC.
 * Rather than storing this information in hard-coded form, criteria could be
 * retrieved from a database and associative relationships from an ontology
 * server.
 * 
 * @author Tom Bechtold
 * @version 2008-06-19
 * @updated 2010-05-04
 */
public class BreastCancerCharacteristicCodes
{
	// BCT-405 internal UMLS code for "Unknown" (we couldn't find a UMLS code)
	public static final CharacteristicCode DONOTMATCH = new CharacteristicCode(99999989);
	
	// Units of measure
	public static final CharacteristicCode WEEK = new CharacteristicCode(439230);
	public static final CharacteristicCode MONTH = new CharacteristicCode(439231);
	public static final CharacteristicCode YEAR = new CharacteristicCode(439234);
	public static final CharacteristicCode MILLIGRAMS_PER_DECILITER = new CharacteristicCode(439269);
	public static final CharacteristicCode MILLILITERS_PER_MINUTE = new CharacteristicCode(439445);
	public static final CharacteristicCode CELLS_PER_MICROLITER = new CharacteristicCode(439378);
	public static final CharacteristicCode CELLS_PER_LITER = new CharacteristicCode(347983);

	// General Health
	public static final CharacteristicCode BIRTHDATE = new CharacteristicCode(421451);
	public static final CharacteristicCode AGE = new CharacteristicCode(1779);
	public static final CharacteristicCode GENDER = new CharacteristicCode(79399);
	public static final CharacteristicCode MALE = new CharacteristicCode(24554);
	public static final CharacteristicCode FEMALE = new CharacteristicCode(15780);
	public static final CharacteristicCode MENOPAUSAL = new CharacteristicCode(1513126);
	public static final CharacteristicCode PREMENOPAUSAL = new CharacteristicCode(279752);
	public static final CharacteristicCode PERIMENOPAUSAL = new CharacteristicCode(677925);
	public static final CharacteristicCode POSTMENOPAUSAL = new CharacteristicCode(232970);
	public static final CharacteristicCode MENOPAUSE = new CharacteristicCode(404559);
	public static final CharacteristicCode MENOPAUSE_NATURAL = new CharacteristicCode(856856);
	public static final CharacteristicCode MENOPAUSE_OOPHRECTOMY = new CharacteristicCode(29936);
	public static final CharacteristicCode MENOPAUSE_RADIATION = new CharacteristicCode(457231);
	public static final CharacteristicCode MENOPAUSE_HORMONE = new CharacteristicCode(19928);
	public static final CharacteristicCode MENOPAUSE_CHEMO = new CharacteristicCode(392920);
	public static final CharacteristicCode HORMONE_REPLACEMENT_THERAPY = new CharacteristicCode(282402);
	public static final CharacteristicCode TREATED = new CharacteristicCode(332154);
	public static final CharacteristicCode COMPLETED = new CharacteristicCode(205197);
	public static final CharacteristicCode CONCURRENT = new CharacteristicCode(205420);
	public static final CharacteristicCode[] HORMONE_REPLACEMENT_THERAPY_SET = { COMPLETED, CONCURRENT };
	public static final Map<CharacteristicCode, CharacteristicCode[]> HORMONE_REPLACEMENT_THERAPY_AGGREGATORS = new HashMap<CharacteristicCode, CharacteristicCode[]>(
			1, 1f);
	static
	{
		HORMONE_REPLACEMENT_THERAPY_AGGREGATORS.put(TREATED, HORMONE_REPLACEMENT_THERAPY_SET);
	}
	public static final CharacteristicCode PARTICIPATION_CURRENT = new CharacteristicCode(679823);
	// Demographics
	public static final CharacteristicCode EDUCATION = new CharacteristicCode(1553770);
	public static final CharacteristicCode EDUCATION_LESS_HIGH_SCHOOL = new CharacteristicCode(1552030);
	public static final CharacteristicCode EDUCATION_HIGH_SCHOOL = new CharacteristicCode(1552031);
	public static final CharacteristicCode EDUCATION_LESS_COLLEGE = new CharacteristicCode(1552032);
	public static final CharacteristicCode EDUCATION_COLLEGE = new CharacteristicCode(1552034);
	public static final CharacteristicCode EDUCATION_POSTGRADUATE = new CharacteristicCode(1552037);
	public static final CharacteristicCode RACE = new CharacteristicCode(34510);
	public static final CharacteristicCode AMERICAN_INDIAN_ALASKAN_NATIVE = new CharacteristicCode(1515945);
	public static final CharacteristicCode ASIAN = new CharacteristicCode(78988);
	public static final CharacteristicCode BLACK = new CharacteristicCode(27567);
	public static final CharacteristicCode HISPANIC = new CharacteristicCode(86409);
	public static final CharacteristicCode PACIFIC_ISLANDER = new CharacteristicCode(1513907);
	public static final CharacteristicCode CAUCASIAN = new CharacteristicCode(7457);
	public static final CharacteristicCode ASHKENAZI_JEWISH = new CharacteristicCode(337704);
	public static final CharacteristicCode RACE_OTHER = new CharacteristicCode(99999995);

	// Other Health Conditions
	public static final CharacteristicCode DISEASE = new CharacteristicCode(12634);
	public static final CharacteristicCode ECOG_ZUBROD = new CharacteristicCode(1520224);
	public static final CharacteristicCode ECOG_ZUBROD_0 = new CharacteristicCode(1518966);
	public static final CharacteristicCode ECOG_ZUBROD_1 = new CharacteristicCode(1518967);
	public static final CharacteristicCode ECOG_ZUBROD_2 = new CharacteristicCode(278956);
	public static final CharacteristicCode ECOG_ZUBROD_3 = new CharacteristicCode(278957);
	public static final CharacteristicCode ECOG_ZUBROD_4 = new CharacteristicCode(278958);
	public static final CharacteristicCode KARNOFSKY = new CharacteristicCode(206065);
	public static final CharacteristicCode PRIMARYCANCER = new CharacteristicCode(1306459);
	public static final CharacteristicCode PRIMARYCANCER_BONE = new CharacteristicCode(279530);
	public static final CharacteristicCode PRIMARYCANCER_CNS = new CharacteristicCode(348374);
	public static final CharacteristicCode PRIMARYCANCER_CERVICAL_INVASIVE = new CharacteristicCode(1334177);
	public static final CharacteristicCode PRIMARYCANCER_CERVICAL_INSITU = new CharacteristicCode(851140);
	public static final CharacteristicCode PRIMARYCANCER_COLON_RECTAL = new CharacteristicCode(9402);
	public static final CharacteristicCode PRIMARYCANCER_HODGKINS = new CharacteristicCode(19829);
	public static final CharacteristicCode PRIMARYCANCER_INTESTINAL = new CharacteristicCode(346627);
	public static final CharacteristicCode PRIMARYCANCER_KIDNEY = new CharacteristicCode(1378703);
	public static final CharacteristicCode PRIMARYCANCER_LEUKEMIA = new CharacteristicCode(23418);
	public static final CharacteristicCode PRIMARYCANCER_LUNG = new CharacteristicCode(242379);
	public static final CharacteristicCode PRIMARYCANCER_LYMPHOMA = new CharacteristicCode(24299);
	public static final CharacteristicCode PRIMARYCANCER_OVARIAN = new CharacteristicCode(1140680);
	public static final CharacteristicCode PRIMARYCANCER_PANCREATIC = new CharacteristicCode(346647);
	public static final CharacteristicCode PRIMARYCANCER_PROSTATE = new CharacteristicCode(376358);
	public static final CharacteristicCode PRIMARYCANCER_BASAL_SQUAMOUS = new CharacteristicCode(7118);
	public static final CharacteristicCode PRIMARYCANCER_MELANOMA = new CharacteristicCode(25202);
	public static final CharacteristicCode PRIMARYCANCER_THYROID = new CharacteristicCode(7115);
	public static final CharacteristicCode PRIMARYCANCER_UTERINE = new CharacteristicCode(153567);
	public static final CharacteristicCode HIV = new CharacteristicCode(19682);
	public static final CharacteristicCode HEMATOPOIETIC = new CharacteristicCode(851353);
	public static final CharacteristicCode HEMATOPOIETIC_ANEMIA = new CharacteristicCode(238644);
	public static final CharacteristicCode HEMATOPOIETIC_BLEEDING_DIATHESIS = new CharacteristicCode(1458140);
	public static final CharacteristicCode AUTOIMMUNE = new CharacteristicCode(4364);
	public static final CharacteristicCode SCLERODERMA = new CharacteristicCode(11644);
	public static final CharacteristicCode SYSTEMIC_LUPUS_ERYTHEMATOSUS = new CharacteristicCode(24141);
	public static final CharacteristicCode PULMONARY = new CharacteristicCode(24115);
	public static final CharacteristicCode PULMONARY_EMBOLISM = new CharacteristicCode(34065);
	public static final CharacteristicCode COPD = new CharacteristicCode(24117);
	public static final CharacteristicCode ASTHMA = new CharacteristicCode(581126);
	public static final CharacteristicCode DIGESTIVE = new CharacteristicCode(12242);
	public static final CharacteristicCode HEPATITIS_B = new CharacteristicCode(19163);
	public static final CharacteristicCode HEPATITIS_C = new CharacteristicCode(19196);
	public static final CharacteristicCode CIRRHOSIS = new CharacteristicCode(23890);
	public static final CharacteristicCode HEPATIC = new CharacteristicCode(23895);
	public static final CharacteristicCode[] HEPATIC_SET = { HEPATITIS_B, HEPATITIS_C, CIRRHOSIS };
	public static final Map<CharacteristicCode, CharacteristicCode[]> DIGESTIVE_AGGREGATORS = new HashMap<CharacteristicCode, CharacteristicCode[]>(
			1, 1f);
	static
	{
		DIGESTIVE_AGGREGATORS.put(HEPATIC, HEPATIC_SET);
	}
	public static final CharacteristicCode DIABETES = new CharacteristicCode(11849);
	public static final CharacteristicCode CARDIOVASCULAR = new CharacteristicCode(7222);
	public static final CharacteristicCode ANGINA = new CharacteristicCode(2962);
	public static final CharacteristicCode ARRHYTHMIA = new CharacteristicCode(3811);
	public static final CharacteristicCode CONGESTIVE_HEART_FAILURE = new CharacteristicCode(18802);
	public static final CharacteristicCode DEEP_VEIN_THROMBOSIS = new CharacteristicCode(149871);
	public static final CharacteristicCode HEART_ATTACK = new CharacteristicCode(27051);
	public static final CharacteristicCode HYPERTENSION = new CharacteristicCode(20538);
	public static final CharacteristicCode RENAL = new CharacteristicCode(22658);
	public static final CharacteristicCode RENAL_FAILURE = new CharacteristicCode(35078);
	public static final CharacteristicCode RENAL_INSUFFICIENCY = new CharacteristicCode(1565489);
	public static final CharacteristicCode NEUROPSYCHIATRIC = new CharacteristicCode(27765);
	public static final CharacteristicCode PERIPHERAL_NEUROPATHY = new CharacteristicCode(31117);
	public static final CharacteristicCode STROKE = new CharacteristicCode(38454);
	public static final CharacteristicCode OSTEOPOROSIS = new CharacteristicCode(29456);
	public static final CharacteristicCode HORMONAL = new CharacteristicCode(14130);
	public static final CharacteristicCode HYPERTHYROIDISM = new CharacteristicCode(20550);
	public static final CharacteristicCode HYPOTHYROIDISM = new CharacteristicCode(20676);
	public static final CharacteristicCode GYNECOLOGIC = new CharacteristicCode(17411);
	public static final CharacteristicCode ENDOMETRIAL_HYPERPLASIA = new CharacteristicCode(14173);
	public static final CharacteristicCode ENDOMETRIOSIS = new CharacteristicCode(14175);
	public static final CharacteristicCode ABNORMAL_VAGINAL_BLEEDING = new CharacteristicCode(578503);
	public static final CharacteristicCode PREGNANCY = new CharacteristicCode(32961);
	public static final CharacteristicCode NURSING = new CharacteristicCode(6147);

	// Diagnosis
	public static final CharacteristicCode CANCER_DIAGNOSIS = new CharacteristicCode(920688);
	public static final CharacteristicCode BREAST_CANCER = new CharacteristicCode(6142);
	public static final CharacteristicCode ANATOMIC_SITE = new CharacteristicCode(1515976);
	public static final CharacteristicCode METASTATIC_BREAST_CANCER = new CharacteristicCode(278488);
	public static final CharacteristicCode LEFT_BREAST = new CharacteristicCode(222601);
	public static final CharacteristicCode RIGHT_BREAST = new CharacteristicCode(222600);
	public static final CharacteristicCode PRIOR_BREAST_DISEASE = new CharacteristicCode(332132);
	public static final CharacteristicCode LOBULAR_INSITU = new CharacteristicCode(279563);
	public static final CharacteristicCode ATYPICAL_DUCTAL_HYPERPLASIA = new CharacteristicCode(442834);
	public static final CharacteristicCode GENETIC_TESTING = new CharacteristicCode(393006);
	public static final CharacteristicCode BRCA1TEST = new CharacteristicCode(1320548);
	public static final CharacteristicCode BRCA1TEST_POSITIVE = new CharacteristicCode(1445905);
	public static final CharacteristicCode BRCA1TEST_NEGATIVE = new CharacteristicCode(1445907);
	public static final CharacteristicCode BRCA2TEST = new CharacteristicCode(1320550);
	public static final CharacteristicCode BRCA2TEST_POSITIVE = new CharacteristicCode(1445909);
	public static final CharacteristicCode BRCA2TEST_NEGATIVE = new CharacteristicCode(1445910);
	public static final CharacteristicCode GAIL_SCORE = new CharacteristicCode(1511297);
	public static final CharacteristicCode CLAUS_MODEL = new CharacteristicCode(1707405);
	public static final CharacteristicCode DIAGNOSISBREAST = new CharacteristicCode(858252);
	public static final CharacteristicCode DUCTAL_INSITU = new CharacteristicCode(7124);
	public static final CharacteristicCode DUCTAL_INVASIVE = new CharacteristicCode(1134719);
	public static final CharacteristicCode LOBULAR_INVASIVE = new CharacteristicCode(279565);
	public static final CharacteristicCode BILATERAL = new CharacteristicCode(238767);
	public static final CharacteristicCode BILATERAL_SYNCHRONOUS = new CharacteristicCode(1515107);
	public static final CharacteristicCode BILATERAL_ASYNCHRONOUS = new CharacteristicCode(205156);
	public static final CharacteristicCode IPSILATERAL = new CharacteristicCode(441989);
	public static final CharacteristicCode CONTRALATERAL = new CharacteristicCode(441988);
	public static final CharacteristicCode SURVIVOR = new CharacteristicCode(242793);
	public static final CharacteristicCode STAGE = new CharacteristicCode(1708231);
	public static final CharacteristicCode STAGE_0 = new CharacteristicCode(475413);
	public static final CharacteristicCode STAGE_I = new CharacteristicCode(475372);
	public static final CharacteristicCode STAGE_II = new CharacteristicCode(475373);
	public static final CharacteristicCode STAGE_IIA = new CharacteristicCode(475387);
	public static final CharacteristicCode STAGE_IIB = new CharacteristicCode(475388);
	public static final CharacteristicCode STAGE_IIC = new CharacteristicCode(475389);
	public static final CharacteristicCode STAGE_III = new CharacteristicCode(475374);
	public static final CharacteristicCode STAGE_IIIA = new CharacteristicCode(475390);
	public static final CharacteristicCode STAGE_IIIB = new CharacteristicCode(475391);
	public static final CharacteristicCode STAGE_IIIC = new CharacteristicCode(475394);
	public static final CharacteristicCode STAGE_IV = new CharacteristicCode(475751);
	public static final CharacteristicCode STAGE_I_III = new CharacteristicCode(796563);
	public static final CharacteristicCode[] STAGE_I_III_SET = { STAGE_I, STAGE_IIA, STAGE_IIB, STAGE_IIC, STAGE_IIIA,
			STAGE_IIIB, STAGE_IIIC };
	public static final CharacteristicCode[] STAGE_II_SET = { STAGE_IIA, STAGE_IIB, STAGE_IIC };
	public static final CharacteristicCode[] STAGE_III_SET = { STAGE_IIIA, STAGE_IIIB, STAGE_IIIC };
	public static final CharacteristicCode[] STAGE_IV_SET = { STAGE_IV };
	public static final Map<CharacteristicCode, CharacteristicCode[]> STAGE_AGGREGATORS = new HashMap<CharacteristicCode, CharacteristicCode[]>(
			3, 1f);
	static
	{
		STAGE_AGGREGATORS.put(STAGE_I_III, STAGE_I_III_SET);
		STAGE_AGGREGATORS.put(STAGE_II, STAGE_II_SET);
		STAGE_AGGREGATORS.put(STAGE_III, STAGE_III_SET);
		STAGE_AGGREGATORS.put(STAGE_IV, STAGE_IV_SET);		
	}
	public static final CharacteristicCode INFLAMMATORY = new CharacteristicCode(278601);
	public static final CharacteristicCode SINGULAR = new CharacteristicCode(205171);
	public static final CharacteristicCode RECURRENT_LOCALLY = new CharacteristicCode(34897);
	public static final CharacteristicCode TUMOR_STAGE_FINDING = new CharacteristicCode(1300486);
	public static final CharacteristicCode TUMOR_STAGE_FINDING_T1 = new CharacteristicCode(1276545);
	public static final CharacteristicCode TUMOR_STAGE_FINDING_T2 = new CharacteristicCode(1276550);
	public static final CharacteristicCode TUMOR_STAGE_FINDING_T3 = new CharacteristicCode(1276551);
	public static final CharacteristicCode LYMPH_NODE_COUNT = new CharacteristicCode(686619);
	public static final CharacteristicCode KNOWN = new CharacteristicCode(205309);
	public static final CharacteristicCode AFFIRMATIVE = new CharacteristicCode(1705108);
	public static final CharacteristicCode NEGATIVE = new CharacteristicCode(1298908);
	public static final CharacteristicCode UNSURE = new CharacteristicCode(87130);
	public static final CharacteristicCode NOTTESTED = new CharacteristicCode(1298808);
	public static final CharacteristicCode ESTROGENRECEPTOR = new CharacteristicCode(1516974);
	public static final CharacteristicCode ESTROGENRECEPTOR_POSITIVE = new CharacteristicCode(279754);
	public static final CharacteristicCode ESTROGENRECEPTOR_NEGATIVE = new CharacteristicCode(279756);
	public static final CharacteristicCode ESTROGENRECEPTOR_LOW = new CharacteristicCode(99999980);
	public static final CharacteristicCode ESTROGENRECEPTOR_INDETERMINATE = new CharacteristicCode(279758);
	public static final CharacteristicCode ESTROGENRECEPTOR_NOTTESTED = NOTTESTED;
	public static final CharacteristicCode ESTROGENRECEPTOR_KNOWN = KNOWN;
	public static final CharacteristicCode[] ESTROGENRECEPTOR_KNOWN_SET = { ESTROGENRECEPTOR_POSITIVE,
			ESTROGENRECEPTOR_NEGATIVE, ESTROGENRECEPTOR_LOW };
	public static final Map<CharacteristicCode, CharacteristicCode[]> ESTROGENRECEPTOR_AGGREGATORS = new HashMap<CharacteristicCode, CharacteristicCode[]>(
			1, 1f);
	static
	{
		ESTROGENRECEPTOR_AGGREGATORS.put(ESTROGENRECEPTOR_KNOWN, ESTROGENRECEPTOR_KNOWN_SET);
	}
	public static final CharacteristicCode PROGESTERONERECEPTOR = new CharacteristicCode(1514471);
	public static final CharacteristicCode PROGESTERONERECEPTOR_POSITIVE = new CharacteristicCode(279759);
	public static final CharacteristicCode PROGESTERONERECEPTOR_NEGATIVE = new CharacteristicCode(279766);
	public static final CharacteristicCode PROGESTERONERECEPTOR_LOW = new CharacteristicCode(99999982);
	public static final CharacteristicCode PROGESTERONERECEPTOR_INDETERMINATE = new CharacteristicCode(279768);
	public static final CharacteristicCode PROGESTERONERECEPTOR_NOTTESTED = NOTTESTED;
	public static final CharacteristicCode PROGESTERONERECEPTOR_KNOWN = KNOWN;
	public static final CharacteristicCode[] PROGESTERONERECEPTOR_KNOWN_SET = { PROGESTERONERECEPTOR_POSITIVE,
			PROGESTERONERECEPTOR_NEGATIVE, PROGESTERONERECEPTOR_LOW };
	public static final Map<CharacteristicCode, CharacteristicCode[]> PROGESTERONERECEPTOR_AGGREGATORS = new HashMap<CharacteristicCode, CharacteristicCode[]>(
			1, 1f);
	static
	{
		PROGESTERONERECEPTOR_AGGREGATORS.put(PROGESTERONERECEPTOR_KNOWN, PROGESTERONERECEPTOR_KNOWN_SET);
	}
	public static final CharacteristicCode HER2NEUDIAGNOSIS = new CharacteristicCode(1512413);
	public static final CharacteristicCode HER2NEUDIAGNOSIS_POSITIVE = new CharacteristicCode(1335447);
	public static final CharacteristicCode HER2NEUDIAGNOSIS_NEGATIVE = new CharacteristicCode(1334932);
	public static final CharacteristicCode HER2NEUDIAGNOSIS_LOW = new CharacteristicCode(99999981);
	public static final CharacteristicCode HER2NEUDIAGNOSIS_INDETERMINATE = new CharacteristicCode(459425);
	public static final CharacteristicCode HER2NEUDIAGNOSIS_NOTTESTED = NOTTESTED;
	public static final CharacteristicCode HER2NEUDIAGNOSIS_KNOWN = KNOWN;
	public static final CharacteristicCode[] HER2NEUDIAGNOSIS_KNOWN_SET = { HER2NEUDIAGNOSIS_POSITIVE,
			HER2NEUDIAGNOSIS_NEGATIVE, HER2NEUDIAGNOSIS_LOW };
	public static final Map<CharacteristicCode, CharacteristicCode[]> HER2NEUDIAGNOSIS_AGGREGATORS = new HashMap<CharacteristicCode, CharacteristicCode[]>(
			1, 1f);
	static
	{
		HER2NEUDIAGNOSIS_AGGREGATORS.put(HER2NEUDIAGNOSIS_KNOWN, HER2NEUDIAGNOSIS_KNOWN_SET);
	}
	
	public static final CharacteristicCode PDL1 = new CharacteristicCode(4722206);
	public static final CharacteristicCode PDL1_POSITIVE = new CharacteristicCode(4284303);
	public static final CharacteristicCode PDL1_NEGATIVE = new CharacteristicCode(4054670);
	
	public static final CharacteristicCode ANDROGENRECEPTOR = new CharacteristicCode(1367578);
	public static final CharacteristicCode ANDROGENRECEPTOR_POSITIVE = new CharacteristicCode(2986463);
	public static final CharacteristicCode ANDROGENRECEPTOR_NEGATIVE = new CharacteristicCode(4528171);
	
	public static final CharacteristicCode PI3K = new CharacteristicCode(1335212);
	public static final CharacteristicCode PI3K_POSITIVE = new CharacteristicCode(1817670);
	public static final CharacteristicCode PI3K_NEGATIVE = new CharacteristicCode(1817669);

	public static final CharacteristicCode DIAGNOSISSPREAD = new CharacteristicCode(332261);
	public static final CharacteristicCode DIAGNOSISAREA = new CharacteristicCode(27653);
	public static final CharacteristicCode SUPERCLAVICULAR = new CharacteristicCode(229730);
	public static final CharacteristicCode INFRACLAVICULAR = new CharacteristicCode(229743);
	public static final CharacteristicCode CHESTWALL = new CharacteristicCode(205076);
	public static final CharacteristicCode DIAGNOSISAREA_BRAIN = new CharacteristicCode(220650);
	public static final CharacteristicCode DIAGNOSISAREA_SPINALCORD = new CharacteristicCode(153646);
	public static final CharacteristicCode DIAGNOSISAREA_BONE = new CharacteristicCode(279530);
	public static final CharacteristicCode DIAGNOSISAREA_LIVER = new CharacteristicCode(345904);
	public static final CharacteristicCode DIAGNOSISAREA_LUNG = new CharacteristicCode(242379);
	public static final CharacteristicCode DIAGNOSISAREA_LYMPHNODE = new CharacteristicCode(596869);
	public static final CharacteristicCode DIAGNOSISAREA_SKIN = new CharacteristicCode(1123023);
	public static final CharacteristicCode DIAGNOSISAREA_OVARIES = new CharacteristicCode(29939);
	public static final CharacteristicCode DIAGNOSISAREA_BREAST = new CharacteristicCode(6141);
	public static final CharacteristicCode BREASTMETASTASIS_BRAIN = new CharacteristicCode(1332624);
	public static final CharacteristicCode UNTREATED = new CharacteristicCode(332155);
	public static final CharacteristicCode PROGRESSING = new CharacteristicCode(205329);
	public static final CharacteristicCode STABLE = new CharacteristicCode(205360);
	public static final CharacteristicCode DIAGNOSISAREA_OTHER_LOCATION = new CharacteristicCode(221562);
	public static final CharacteristicCode DIAGNOSISAREA_EYE = new CharacteristicCode(15392);
	public static final CharacteristicCode DIAGNOSISAREA_LEPTOMENINGES = new CharacteristicCode(228126);
	public static final CharacteristicCode DIAGNOSISAREA_ABDOMINAL_LININGS = new CharacteristicCode(31153);
	public static final CharacteristicCode DIAGNOSISAREA_BLADDER = new CharacteristicCode(5682);
	//public static final CharacteristicCode DIAGNOSISAREA_REPRODUCTIVE = new CharacteristicCode(545953);
	public static final CharacteristicCode DIAGNOSISAREA_GI_TRACT = new CharacteristicCode(17189);
	public static final CharacteristicCode[] BREASTMETASTASIS_BRAIN_TREATED_SET = { PROGRESSING, STABLE };
	public static final Map<CharacteristicCode, CharacteristicCode[]> BREASTMETASTASIS_BRAIN_AGGREGATORS = new HashMap<CharacteristicCode, CharacteristicCode[]>(
			1, 1f);
	static
	{
		BREASTMETASTASIS_BRAIN_AGGREGATORS.put(TREATED, BREASTMETASTASIS_BRAIN_TREATED_SET);
	}
	public static final CharacteristicCode LYMPHEDEMA = new CharacteristicCode(24236);
	public static final CharacteristicCode MEASURABLE = new CharacteristicCode(1513040);

	// Surgical procedure
	public static final CharacteristicCode CANCER_TREATMENT = new CharacteristicCode(920425);
	public static final CharacteristicCode SURGERY = new CharacteristicCode(728940);
	public static final CharacteristicCode SURGERY_BREAST_CONSERVING = new CharacteristicCode(851238);
	public static final CharacteristicCode BREAST_REEXCISION = new CharacteristicCode(1274052);
	public static final CharacteristicCode EXCISIONAL_BIOPSY = new CharacteristicCode(184913);
	public static final CharacteristicCode MASTECTOMY_BILATERAL = new CharacteristicCode(99999994);
	public static final CharacteristicCode MASTECTOMY_UNILATERAL = new CharacteristicCode(99999996);
	public static final CharacteristicCode MASTECTOMY_THERAPEUTIC = new CharacteristicCode(302350);
	public static final CharacteristicCode MASTECTOMY_PROPHYLACTIC = new CharacteristicCode(445202);
	public static final CharacteristicCode SENTINEL_NODE_BIOPSY = new CharacteristicCode(796693);
	public static final CharacteristicCode MASTECTOMY = new CharacteristicCode(24881);
	//public static final CharacteristicCode[] MASTECTOMY_SET = { MASTECTOMY_THERAPEUTIC, MASTECTOMY_PROPHYLACTIC };
	public static final CharacteristicCode[] MASTECTOMY_SET = { MASTECTOMY_UNILATERAL, MASTECTOMY_BILATERAL };
	public static final CharacteristicCode[] OLD_MASTECTOMY_SET = { MASTECTOMY_BILATERAL, MASTECTOMY_THERAPEUTIC, MASTECTOMY_PROPHYLACTIC };
	public static final CharacteristicCode SURGERY_BREAST = new CharacteristicCode(851312);
	public static final CharacteristicCode[] SURGERY_BREAST_SET = { SURGERY_BREAST_CONSERVING, BREAST_REEXCISION,
			EXCISIONAL_BIOPSY, MASTECTOMY_UNILATERAL, MASTECTOMY_BILATERAL };
	public static final Map<CharacteristicCode, CharacteristicCode[]> SURGERY_AGGREGATORS = new HashMap<CharacteristicCode, CharacteristicCode[]>(
			2, 1f);
	static
	{
		SURGERY_AGGREGATORS.put(SURGERY_BREAST, SURGERY_BREAST_SET);
		SURGERY_AGGREGATORS.put(MASTECTOMY, MASTECTOMY_SET);
	}
	public static final CharacteristicCode LEFT_AXILLARY_NODE = new CharacteristicCode(929379);
	public static final CharacteristicCode RIGHT_AXILLARY_NODE = new CharacteristicCode(934583);
	public static final CharacteristicCode AXILLARY_NODE_DISSECTION = new CharacteristicCode(1510998);
	public static final CharacteristicCode LEFT_OVARY = new CharacteristicCode(227874);
	public static final CharacteristicCode RIGHT_OVARY = new CharacteristicCode(227873);
	public static final CharacteristicCode OOPHORECTOMY = new CharacteristicCode(29936);
	public static final CharacteristicCode UTERUS = new CharacteristicCode(42149);
	public static final CharacteristicCode HYSTERECTOMY = new CharacteristicCode(457230);
	public static final CharacteristicCode BRAIN = new CharacteristicCode(6104);
	public static final CharacteristicCode SURGERY_BRAIN = new CharacteristicCode(195775);
	public static final CharacteristicCode SPINAL_CORD = new CharacteristicCode(37925);
	public static final CharacteristicCode SURGERY_SPINAL_CORD = new CharacteristicCode(920347);
	public static final CharacteristicCode BONE = new CharacteristicCode(262950);
	public static final CharacteristicCode SURGERY_BONE = new CharacteristicCode(185131);
	public static final CharacteristicCode LIVER = new CharacteristicCode(23884);
	public static final CharacteristicCode SURGERY_LIVER = new CharacteristicCode(193373);
	public static final CharacteristicCode LUNG = new CharacteristicCode(24109);
	public static final CharacteristicCode SURGERY_LUNG = new CharacteristicCode(38903);
	public static final CharacteristicCode LYMPH_NODE = new CharacteristicCode(24204);
	public static final CharacteristicCode SURGERY_LYMPH_NODE = new CharacteristicCode(193830);
	public static final CharacteristicCode OVARY = new CharacteristicCode(29939);

	// Radiation procedure
	public static final CharacteristicCode RADIATION = new CharacteristicCode(34618);
	public static final CharacteristicCode WHOLE = new CharacteristicCode(444667);
	public static final CharacteristicCode PARTIAL = new CharacteristicCode(728938);
	public static final CharacteristicCode RADIATION_BREAST_IPSILATERAL = new CharacteristicCode(441989);
	public static final CharacteristicCode RADIATION_BREAST_CONTRALATERAL = new CharacteristicCode(441988);
	public static final CharacteristicCode RADIATION_BREAST = new CharacteristicCode(852216);
	public static final CharacteristicCode[] RADIATION_BREAST_SET = { RADIATION_BREAST_IPSILATERAL,
			RADIATION_BREAST_CONTRALATERAL };
	public static final CharacteristicCode THORAX = new CharacteristicCode(817096);
	public static final CharacteristicCode OVARIAN_RADIOTHERAPY = new CharacteristicCode(1504397);
	public static final Map<CharacteristicCode, CharacteristicCode[]> RADIATION_AGGREGATORS = new HashMap<CharacteristicCode, CharacteristicCode[]>(
			1, 1f);
	static
	{
		RADIATION_AGGREGATORS.put(RADIATION_BREAST, RADIATION_BREAST_SET);
	}
	public static final CharacteristicCode THYROID_DISEASE = new CharacteristicCode(40128);
	public static final CharacteristicCode LUNG_DISEASE = new CharacteristicCode(948315);

	// Regimen codes
	public static final CharacteristicCode SETTING = new CharacteristicCode(542559);
	public static final CharacteristicCode PREVENTION = new CharacteristicCode(199176);
	public static final CharacteristicCode NEOADJUVANT = new CharacteristicCode(600558);
	public static final CharacteristicCode ADJUVANT = new CharacteristicCode(677850);
	public static final CharacteristicCode METASTATIC = new CharacteristicCode(36525);
	public static final CharacteristicCode ADVERSE_DRUG_EFFECT = new CharacteristicCode(41755);
	public static final CharacteristicCode BONECANCER_SECONDARY = new CharacteristicCode(153690);
	public static final CharacteristicCode INCOMPLETE = new CharacteristicCode(205257);
	public static final CharacteristicCode MOSTRECENT = new CharacteristicCode(1513491);
	public static final CharacteristicCode THRESHOLD = new CharacteristicCode(449864);

	// Chemotherapy regimen
	public static final CharacteristicCode CHEMOTHERAPY = new CharacteristicCode(392920);
	public static final CharacteristicCode ABRAXANE = new CharacteristicCode(1564985);
	public static final CharacteristicCode DOXORUBICIN = new CharacteristicCode(13089);
	public static final CharacteristicCode FIVE_FLUOROURACIL = new CharacteristicCode(16360);
	public static final CharacteristicCode CYCLOPHOSPHAMIDE = new CharacteristicCode(10583);
	public static final CharacteristicCode LIPOSOMAL_DOXORUBICIN = new CharacteristicCode(279284);
	public static final CharacteristicCode EPIRUBICIN = new CharacteristicCode(14582);
	public static final CharacteristicCode GEMCITABINE = new CharacteristicCode(45093);
	public static final CharacteristicCode IRINOTECAN = new CharacteristicCode(123931);
	public static final CharacteristicCode METHOTREXATE = new CharacteristicCode(25677);
	public static final CharacteristicCode VINORELBINE = new CharacteristicCode(78257);
	public static final CharacteristicCode MITOXANTRONE = new CharacteristicCode(26259);
	public static final CharacteristicCode CARBOPLATIN = new CharacteristicCode(79083);
	public static final CharacteristicCode CISPLATIN = new CharacteristicCode(8838);
	public static final CharacteristicCode PACLITAXEL = new CharacteristicCode(144576);
	public static final CharacteristicCode DOCETAXEL = new CharacteristicCode(246415);
	public static final CharacteristicCode TEMOZOLOMIDE = new CharacteristicCode(76080);
	public static final CharacteristicCode THIOTEPA = new CharacteristicCode(39871);
	public static final CharacteristicCode TOPOTECAN = new CharacteristicCode(146224);
	public static final CharacteristicCode VINCRISTINE = new CharacteristicCode(42679);
	public static final CharacteristicCode VINBLASTINE = new CharacteristicCode(42670);
	public static final CharacteristicCode CAPECITABINE = new CharacteristicCode(671970);
	public static final CharacteristicCode IXABEPILONE = new CharacteristicCode(1135132);
	public static final CharacteristicCode DAUNORUBICIN = new CharacteristicCode(11015);
	public static final CharacteristicCode MITOMYCIN = new CharacteristicCode(2475);
	public static final CharacteristicCode CLASS_ALKYLATING = new CharacteristicCode(2073);
	public static final CharacteristicCode[] CLASS_ALKYLATING_SET = { CYCLOPHOSPHAMIDE, TEMOZOLOMIDE, THIOTEPA };
	public static final CharacteristicCode CLASS_ANTHRACYCLINE = new CharacteristicCode(3234);
	public static final CharacteristicCode[] CLASS_ANTHRACYCLINE_SET = { DOXORUBICIN, LIPOSOMAL_DOXORUBICIN,
			EPIRUBICIN, MITOXANTRONE };
	public static final CharacteristicCode CLASS_PLATINUM = new CharacteristicCode(32207);
	public static final CharacteristicCode[] CLASS_PLATINUM_SET = { CARBOPLATIN, CISPLATIN };
	public static final CharacteristicCode CLASS_TAXANE = new CharacteristicCode(796419);
	public static final CharacteristicCode[] CLASS_TAXANE_SET = { ABRAXANE, PACLITAXEL, DOCETAXEL };
	public static final CharacteristicCode CLASS_VINCA = new CharacteristicCode(1883533);
	public static final CharacteristicCode[] CLASS_VINCA_SET = { VINORELBINE, VINCRISTINE, VINBLASTINE };
	public static final Map<CharacteristicCode, CharacteristicCode[]> CHEMOTHERAPY_AGGREGATORS = new HashMap<CharacteristicCode, CharacteristicCode[]>(
			5, 1f);
	static
	{
		CHEMOTHERAPY_AGGREGATORS.put(CLASS_ALKYLATING, CLASS_ALKYLATING_SET);
		CHEMOTHERAPY_AGGREGATORS.put(CLASS_ANTHRACYCLINE, CLASS_ANTHRACYCLINE_SET);
		CHEMOTHERAPY_AGGREGATORS.put(CLASS_PLATINUM, CLASS_PLATINUM_SET);
		CHEMOTHERAPY_AGGREGATORS.put(CLASS_TAXANE, CLASS_TAXANE_SET);
		CHEMOTHERAPY_AGGREGATORS.put(CLASS_VINCA, CLASS_VINCA_SET);
	}
	public static final CharacteristicCode HALAVEN = new CharacteristicCode(2608038); // Eribulin Mesylate
	public static final CharacteristicCode CHEMOTHERAPY_OTHER = new CharacteristicCode(478584);
	public static final CharacteristicCode CHEMOAGENT_OTHER = new CharacteristicCode(99999992);

	// Targeted/Biological regimen
	public static final CharacteristicCode BIOLOGICAL = new CharacteristicCode(5522);
	public static final CharacteristicCode TRASTUZUMAB = new CharacteristicCode(728747);
	public static final CharacteristicCode KADCYLA = new CharacteristicCode(2935436);
	public static final CharacteristicCode NERATINIB = new CharacteristicCode(2713008);
	public static final CharacteristicCode PERJETA = new CharacteristicCode(3473371);
	public static final CharacteristicCode LAPATINIB = new CharacteristicCode(1506770);
	public static final CharacteristicCode PEMBROLIZUMAB = new CharacteristicCode(3658706);
	public static final CharacteristicCode PALBOCICLIB = new CharacteristicCode(3853822);
	public static final CharacteristicCode RIBOCICLIB = new CharacteristicCode(4045494);
	public static final CharacteristicCode ABEMACICLIB = new CharacteristicCode(3852841);
	public static final CharacteristicCode OLAPARIB = new CharacteristicCode(2316164);
	public static final CharacteristicCode TALAZOPARIB = new CharacteristicCode(4042960);
	public static final CharacteristicCode AFINITOR = new CharacteristicCode(2682856);
	public static final CharacteristicCode BEVACIZUMAB = new CharacteristicCode(796392);
	public static final CharacteristicCode BIOLOGICAL_OTHER = new CharacteristicCode(99999990);
	public static final CharacteristicCode ATEZOLIZUMAB = new CharacteristicCode(4055433);
	public static final CharacteristicCode ALPELISIB = new CharacteristicCode(4055478);
	public static final CharacteristicCode TUCATINIB = new CharacteristicCode(4519167);
	public static final CharacteristicCode SACITUZUMAB = new CharacteristicCode(5244401);
	public static final CharacteristicCode FAMTRASTUZUMAB = new CharacteristicCode(5222084);
	public static final CharacteristicCode DOSTARLIMAB = new CharacteristicCode(507894);


	public static final CharacteristicCode CLASS_ANTIHER2 = new CharacteristicCode(99999998);
	public static final CharacteristicCode[] CLASS_ANTIHER2_SET = { FAMTRASTUZUMAB, TRASTUZUMAB, KADCYLA, NERATINIB, PERJETA, TUCATINIB, LAPATINIB  };
	public static final CharacteristicCode CLASS_IMMUNOTHERAPY = new CharacteristicCode(21083);
	public static final CharacteristicCode[] CLASS_IMMUNOTHERAPY_SET = { PEMBROLIZUMAB, ATEZOLIZUMAB, DOSTARLIMAB };
	public static final CharacteristicCode CLASS_CDK_FOUR_FIVE = new CharacteristicCode(99999999);
	public static final CharacteristicCode[] CLASS_CDK_FOUR_FIVE_SET = { PALBOCICLIB, RIBOCICLIB, ABEMACICLIB };
	public static final CharacteristicCode CLASS_PARP = new CharacteristicCode(1882413);
    public static final CharacteristicCode[] CLASS_PARP_SET = { OLAPARIB, TALAZOPARIB };
	public static final CharacteristicCode[] CLASS_OTHER_SET = { AFINITOR, BEVACIZUMAB , ALPELISIB, SACITUZUMAB};
	public static final Map<CharacteristicCode, CharacteristicCode[]> BIOLOGICAL_AGGREGATORS = new HashMap<CharacteristicCode, CharacteristicCode[]>(
			5, 1f);
	static
	{
		BIOLOGICAL_AGGREGATORS.put(CLASS_ANTIHER2, CLASS_ANTIHER2_SET);
		BIOLOGICAL_AGGREGATORS.put(CLASS_IMMUNOTHERAPY, CLASS_IMMUNOTHERAPY_SET);
		BIOLOGICAL_AGGREGATORS.put(CLASS_CDK_FOUR_FIVE, CLASS_CDK_FOUR_FIVE_SET);
		BIOLOGICAL_AGGREGATORS.put(CLASS_PARP, CLASS_PARP_SET);
		BIOLOGICAL_AGGREGATORS.put(BIOLOGICAL_OTHER, CLASS_OTHER_SET);
		
	}
	// Hormonal/Endocrine regimen
	public static final CharacteristicCode ENDOCRINE = new CharacteristicCode(19928);
	public static final CharacteristicCode RALOXIFENE = new CharacteristicCode(244404);
	public static final CharacteristicCode TOREMIFENE = new CharacteristicCode(76836);
	public static final CharacteristicCode FULVESTRANT = new CharacteristicCode(935916);
	public static final CharacteristicCode TAMOXIFEN = new CharacteristicCode(39286);
	public static final CharacteristicCode ANASTROZOLE = new CharacteristicCode(290883);
	public static final CharacteristicCode EXEMESTANE = new CharacteristicCode(851344);
	public static final CharacteristicCode LETROZOLE = new CharacteristicCode(246421);
	public static final CharacteristicCode LEUPROLIDE = new CharacteristicCode(85272);
	public static final CharacteristicCode ABARELIX = new CharacteristicCode(965390);
	public static final CharacteristicCode BUSERELIN = new CharacteristicCode(6456);
	public static final CharacteristicCode GOSERELIN = new CharacteristicCode(120107);
	public static final CharacteristicCode MEGESTROL_ACETATE = new CharacteristicCode(65879);
	public static final CharacteristicCode CLASS_ANTIESTROGEN = new CharacteristicCode(14930);
	public static final CharacteristicCode[] CLASS_ANTIESTROGEN_SET = { RALOXIFENE, TOREMIFENE, FULVESTRANT, TAMOXIFEN };
	public static final CharacteristicCode CLASS_AROMATASE_INHIBITOR = new CharacteristicCode(593802);
	public static final CharacteristicCode[] CLASS_AROMATASE_INHIBITOR_SET = { ANASTROZOLE, EXEMESTANE, LETROZOLE };
	public static final CharacteristicCode CLASS_OVARIAN_SUPPRESSION = new CharacteristicCode(677922);
	public static final CharacteristicCode[] CLASS_OVARIAN_SUPPRESSION_SET = { LEUPROLIDE, ABARELIX, BUSERELIN,
			GOSERELIN };
	public static final Map<CharacteristicCode, CharacteristicCode[]> ENDOCRINE_AGGREGATORS = new HashMap<CharacteristicCode, CharacteristicCode[]>(
			3, 1f);
	static
	{
		ENDOCRINE_AGGREGATORS.put(CLASS_ANTIESTROGEN, CLASS_ANTIESTROGEN_SET);
		ENDOCRINE_AGGREGATORS.put(CLASS_AROMATASE_INHIBITOR, CLASS_AROMATASE_INHIBITOR_SET);
		ENDOCRINE_AGGREGATORS.put(CLASS_OVARIAN_SUPPRESSION, CLASS_OVARIAN_SUPPRESSION_SET);
	}
	public static final CharacteristicCode ENDOCRINE_OTHER = new CharacteristicCode(99999993);

	// Bisphosphonate regimen
	public static final CharacteristicCode BISPHOSPHONATE = new CharacteristicCode(12544);
	public static final CharacteristicCode RISEDRONATE = new CharacteristicCode(246719);
	public static final CharacteristicCode PAMIDRONATE = new CharacteristicCode(43603);
	public static final CharacteristicCode IBANDRONATE = new CharacteristicCode(379199);
	public static final CharacteristicCode ALENDRONATE = new CharacteristicCode(102118);
	public static final CharacteristicCode ZOLEDRONATE = new CharacteristicCode(392938);
	public static final CharacteristicCode DENOSUMAB_XGEVA = new CharacteristicCode(1690432);
	public static final CharacteristicCode BIOSPHOSPHONATE_OTHER = new CharacteristicCode(99999991);

	// BCT-405: Therapy Type Others
	public static final CharacteristicCode[] CLASS_THERAPY_OTHER_SET = { CHEMOAGENT_OTHER, BIOLOGICAL_OTHER, ENDOCRINE_OTHER, BIOSPHOSPHONATE_OTHER,  CHEMOTHERAPY_OTHER };
	
	// Laboratory values
	public static final CharacteristicCode CREATININE = new CharacteristicCode(201975);
	public static final CharacteristicCode CREATININE_CLEARANCE = new CharacteristicCode(373595);
	public static final CharacteristicCode BILIRUBIN = new CharacteristicCode(201913);
	public static final CharacteristicCode NEUTROPHIL = new CharacteristicCode(948762);
	public static final CharacteristicCode PLATELET_COUNT = new CharacteristicCode(32181);

	// special Avatar codes.  Be careful that these do not conflict with real UML codes
	public static final CharacteristicCode AVATAR_DIAGNOSISAREA_OTHER = new CharacteristicCode(9999);
	public static final CharacteristicCode AVATAR_DIAGNOSISAREA_BRAIN_SPINAL = new CharacteristicCode(9997);

	public static final CharacteristicCode AVATAR_NOLOCAL_SPREAD_DISEASE = new CharacteristicCode(9998);
	public static final CharacteristicCode AVATAR_DIAGNOSISAREA_NONE = new CharacteristicCode(0);
	public static final CharacteristicCode AVATAR_DIAGNOSISAREA_ANY = new CharacteristicCode(-1);


	// If anything is added to the DIAGNOSIS_AREA set add it to one of the two HashSets here as well.  DIAGNOSISAREA_BREAST should not be included in either list??? 
	public static final CharacteristicCode[] AVATAR_STAGEIII_LOCAL_SPREAD_OF_DISEASE_SET = 	{INFRACLAVICULAR, SUPERCLAVICULAR, CHESTWALL};
	
	public static final CharacteristicCode[] AVATAR_STAGEIV_SPREAD_OF_DISEASE_SET = {DIAGNOSISAREA_BRAIN,
																					 DIAGNOSISAREA_SPINALCORD,
																					 DIAGNOSISAREA_BONE,
																					 DIAGNOSISAREA_LIVER,
																					 DIAGNOSISAREA_LUNG,
																					 DIAGNOSISAREA_LYMPHNODE, 
																					 DIAGNOSISAREA_SKIN,
																					 DIAGNOSISAREA_OVARIES, 
																					 DIAGNOSISAREA_OTHER_LOCATION
	};
	
	public static final CharacteristicCode[] HISPANIC_QUESTION_ATTRIBUTE_IGNORABLE = {UNSURE};
	public static final CharacteristicCode[] RACE_ETHNIC_ATTRIBUTE_IGNORABLE = {UNSURE, RACE_OTHER};
	
	public static final Map<CharacteristicCode, CharacteristicCode[]> ASSOCIATIVE_ATTRIBUTE_IGNORABLE = new HashMap<CharacteristicCode, CharacteristicCode[]>();
	static
	{
//		ASSOCIATIVE_ATTRIBUTE_IGNORABLE.put(HISPANIC, HISPANIC_QUESTION_ATTRIBUTE_IGNORABLE);
//		ASSOCIATIVE_ATTRIBUTE_IGNORABLE.put(RACE, RACE_ETHNIC_ATTRIBUTE_IGNORABLE);
	}
	
	private static HashMap<Integer, String> breastCancerCodeMatcher;

	public static String getCharacteristicCodeDescriber(int value)
	{
		if (breastCancerCodeMatcher == null)
		{
			breastCancerCodeMatcher = new HashMap<Integer, String>();
			Class<?> c = BreastCancerCharacteristicCodes.class;
			Field[] fields = c.getDeclaredFields();
			for (Field field: fields)
			{
				Object fieldValue = null;
				try 
				{
					fieldValue = field.get(c);
				} 
				catch (IllegalArgumentException e) 
				{
					// Ignored
					e.printStackTrace();
				} 
				catch (IllegalAccessException e) 
				{
					// Ignored
				}
				
				if (fieldValue instanceof CharacteristicCode)
				{
					breastCancerCodeMatcher.put(((CharacteristicCode) fieldValue).getCode(), field.getName());
				}	
			}
		}
		return breastCancerCodeMatcher.get(value);
	}
	
	public static String getCharactersticCodeArrayDescriber(Set<CharacteristicCode> codes)
	{
		if (codes == null)
		{
			return "";
		}

		StringBuilder describer = new StringBuilder();
		
		for (CharacteristicCode code: codes)
		{
			describer.append(",");
			String codeDescription = getCharacteristicCodeDescriber(code.getCode());
			if (codeDescription != null)
			{	
				describer.append(getCharacteristicCodeDescriber(code.getCode()));
			}	
		}
		return "[" + describer.toString().substring(1) + "]";
	}
}
