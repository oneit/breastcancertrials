/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.ontology;

/**
 * Holds characteristics that have zero or more attributes.
 * 
 * @author Tom Bechtold
 * @version 2008-05-16
 */
public interface AttributeCharacteristicHolder extends StringCharacteristicHolder
{
	/**
	 * Adds an attribute to a characteristic
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to add the attribute to
	 * @param attributeCode
	 *            the code of the attribute to add to the characteristic
	 */
	public void addAttribute(CharacteristicCode characteristicCode, CharacteristicCode attributeCode);

	/**
	 * Removes an attribute from a characteristic
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to add the attribute to
	 * @param attributeCode
	 *            the code of the attribute to add to the characteristic
	 */
	public void removeAttribute(CharacteristicCode characteristicCode, CharacteristicCode attributeCode);

	/**
	 * Returns whether a characteristic contains an attribute
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to find the attribute for
	 * @param attributeCode
	 *            the code of the attribute to find within the characteristic
	 * @return whether a characteristic contains an attribute
	 */
	public boolean containsAttribute(CharacteristicCode characteristicCode, CharacteristicCode attributeCode);
}
