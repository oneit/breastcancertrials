/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.ontology;

/**
 * Holds characteristics that have one short value.
 * 
 * @author Tom Bechtold
 * @version 2008-05-21
 */
public interface ShortCharacteristicHolder
{
	/**
	 * Sets a characteristic's short value
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to set the value for
	 * @param shortValue
	 *            the value to set to the characteristic or <code>null</code> to
	 *            remove any value
	 */
	public void setShort(CharacteristicCode characteristicCode, Short shortValue);

	/**
	 * Returns the value that is set in a characteristic
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic
	 * @return the value that is set in a characteristic or <code>null</code> if
	 *         no value is set
	 */
	public Short getShort(CharacteristicCode characteristicCode);
}
