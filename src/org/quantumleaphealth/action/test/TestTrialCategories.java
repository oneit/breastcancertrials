package org.quantumleaphealth.action.test;

import java.util.HashSet;
import java.util.Set;

import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.Trial_Category;
import org.quantumleaphealth.model.trial.Trial_CategoryPK;
import org.testng.annotations.Test;

public class TestTrialCategories 
{
	
	private static Trial_Category getTestCategory( short category )
	{
		Trial_CategoryPK trialCatPK = new Trial_CategoryPK();
		trialCatPK.setCategory( category );
		Trial_Category trial_Category = new Trial_Category();
		trial_Category.setTrial_CategoryPK( trialCatPK );
		
		return trial_Category;
	}

	@Test
	public void testTrialIsInCategory()
	{
		Trial trial = new Trial();
		
        Set<Trial_Category> hs = new HashSet<Trial_Category>();
        hs.add( getTestCategory( (short) 3 ) );
        hs.add( getTestCategory( (short) 24 ) );
        hs.add( getTestCategory( (short) 25 ) );
        trial.setTrial_Categories( hs );
		
		assert trial.isInCategory("ALTERNATIVE") : "Trial should be in category but is not.";
	}

	@Test
	public void testTrialIsNotInCategory()
	{
		Trial trial = new Trial();
		
        Set<Trial_Category> hs = new HashSet<Trial_Category>();
        hs.add( getTestCategory( (short) 3 ) );
        hs.add( getTestCategory( (short) 25 ) );
        trial.setTrial_Categories( hs );
		
		assert trial.isInCategory( "CAM" ) == false : "Trial should NOT be in category but is.";
	}

	@Test
	public void testTrialIsNoVisitsRequired()
	{
		Trial trial = new Trial();
		
        Set<Trial_Category> hs = new HashSet<Trial_Category>();
        hs.add( getTestCategory( (short) 3 ) );
        hs.add( getTestCategory( (short) 24 ) );
        hs.add( getTestCategory( (short) 25 ) );
        trial.setTrial_Categories( hs );
		
		assert trial.inNoVisitsRequired() : "Trial should be in No Visits Required category but is not.";
	}

	@Test
	public void testTrialIsNotNoVisitsRequired()
	{
		Trial trial = new Trial();
		
        Set<Trial_Category> hs = new HashSet<Trial_Category>();
        hs.add( getTestCategory( (short) 3 ) );
        hs.add( getTestCategory( (short) 24 ) );
        trial.setTrial_Categories( hs );
		
		assert trial.inNoVisitsRequired() == false : "Trial should NOT be in No Visits Required category but is.";
	}
}
