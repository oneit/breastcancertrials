/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action.test;

import org.quantumleaphealth.action.PatientInterfaceAction;
import org.quantumleaphealth.model.patient.ScreenedTrial;
import org.quantumleaphealth.screen.test.TestUtils;
import org.testng.annotations.Test;

/**
 * Tests for the <code>PatientInterfaceAction</code>.
 * 
 * @author Tom Bechtold
 * @version 2008-12-19
 * @see PatientInterfaceAction
 */
@Test(description = "PatientInterfaceAction", groups = { "action", "unit" })
public class TestPatientInterfaceAction
{

	/**
	 * Test initial value of patient property
	 */
	public void testGetPatient()
	{
		assert new PatientInterfaceAction().getPatient() != null : "New interface has null patient";
	}

	/**
	 * Test initial value of history property
	 */
	public void testGetHistory()
	{
		assert new PatientInterfaceAction().getHistory() != null : "New interface has null history facade";
	}

	/**
	 * Test initial value of PostalCode property
	 */
	public void testGetPostalCode()
	{
		assert new PatientInterfaceAction().getPostalCode() == null : "New interface has non-null PostalCode";
	}

	/**
	 * Test null initial value and getter/setter of trialId property
	 */
	public void testTrialId()
	{
		// Initial value is null
		PatientInterfaceAction patientInterfaceAction = new PatientInterfaceAction();
		assert patientInterfaceAction.getTrialId() == null : "New interface has non-null TrialId";
		// Setting random long
		int[] ids = TestUtils.getUniqueIntegers(2);
		Long trialId1 = Long.valueOf(ids[0]);
		Long trialId2 = Long.valueOf(ids[1]);
		patientInterfaceAction.setTrialId(trialId1);
		assert trialId1.equals(patientInterfaceAction.getTrialId()) : "Stored value "
				+ patientInterfaceAction.getTrialId() + " is not " + trialId1;
		// Setting null has no effect
		patientInterfaceAction.setTrialId(null);
		assert trialId1.equals(patientInterfaceAction.getTrialId()) : "Stored value "
				+ patientInterfaceAction.getTrialId() + " after setting null is not " + trialId1;
		// Overwriting
		patientInterfaceAction.setTrialId(trialId2);
		assert trialId2.equals(patientInterfaceAction.getTrialId()) : "Stored value "
				+ patientInterfaceAction.getTrialId() + " after overwriting is not " + trialId2;
	}

	/**
	 * Test null initial value and getter/setter of credentials property
	 */
	public void testCredentials()
	{
		// Initial value is null
		PatientInterfaceAction patientInterfaceAction = new PatientInterfaceAction();
		assert patientInterfaceAction.getCredentials() == null : "New interface has non-null credentials";
		// Empty or blank is converted to null
		patientInterfaceAction.setCredentials("");
		assert patientInterfaceAction.getCredentials() == null : "Stored empty value "
				+ patientInterfaceAction.getCredentials() + " is not null";
		patientInterfaceAction.setCredentials(" ");
		assert patientInterfaceAction.getCredentials() == null : "Stored blank value "
				+ patientInterfaceAction.getCredentials() + " is not null";
		String credentials1 = "foo";
		String credentials2 = "bar";
		patientInterfaceAction.setCredentials(credentials1);
		assert credentials1.equals(patientInterfaceAction.getCredentials()) : "Stored value "
				+ patientInterfaceAction.getCredentials() + " is not " + credentials1;
		patientInterfaceAction.setCredentials(credentials2);
		assert credentials2.equals(patientInterfaceAction.getCredentials()) : "Stored value "
				+ patientInterfaceAction.getCredentials() + " after overwriting is not " + credentials2;
	}

	/**
	 * Test non-null initial value of trials property
	 */
	public void testTrials()
	{
		assert new PatientInterfaceAction().getTrials() != null : "New interface has null trial list";
	}

	/**
	 * Test null initial value of SortedTrialSites property
	 */
	public void testSortedTrialSites()
	{
		assert new PatientInterfaceAction().getSortedTrialSites() == null : "New interface has sorted trial list";
	}

	/**
	 * Test zero initial value of ScreenedTrialCount property, after adding a
	 * screened trial and after setting the patient's history to complete
	 */
	public void testScreenedTrialCount()
	{
		// 0 initial value
		PatientInterfaceAction patientInterfaceAction = new PatientInterfaceAction();
		assert patientInterfaceAction.getScreenedTrialCount() == 0 : "New interface has positive screened trials: "
				+ patientInterfaceAction.getScreenedTrialCount();
		// Incomplete history invalidates any screened trials
		patientInterfaceAction.getPatient().getScreenedTrials().add(new ScreenedTrial());
		assert patientInterfaceAction.getScreenedTrialCount() == 0 : "Incomplete history has positive screened trials: "
				+ patientInterfaceAction.getScreenedTrialCount();
		patientInterfaceAction.getPatient().setPatientHistoryComplete(true);
		assert patientInterfaceAction.getScreenedTrialCount() == 1 : "Complete history does not have one screened trial: "
				+ patientInterfaceAction.getScreenedTrialCount();
	}

	/**
	 * Test true initial value of FirstTime property
	 */
	public void testFirstTime()
	{
		assert new PatientInterfaceAction().isFirstTime() : "New interface is not first time user";
	}

	/**
	 * Test false initial value of TermsAcceptedValid property
	 */
	public void testTermsAcceptedValid()
	{
		assert !new PatientInterfaceAction().isTermsAcceptedValid() : "New interface has accepted terms";
	}

	/**
	 * Test false initial value of LoggedIn property and after patient's
	 * identifier is set
	 */
	public void testLoggedIn()
	{
		// False initial value
		PatientInterfaceAction patientInterfaceAction = new PatientInterfaceAction();
		assert !patientInterfaceAction.isLoggedIn() : "New interface is logged in";
		// True after patient has an id
		patientInterfaceAction.getPatient().setId(Long.valueOf(TestUtils.GENERATOR.nextLong()));
		assert patientInterfaceAction.isLoggedIn() : "Patient with id is not logged in";
	}
}
