package org.quantumleaphealth.action.test;

import org.quantumleaphealth.action.TrialProximity;
import org.quantumleaphealth.action.TrialSiteProximity.DistanceUnit;
import org.quantumleaphealth.model.trial.Trial;
import org.testng.annotations.Test;

@Test(description = "TrialProximity", groups = { "unit" })
public class TestTrialProximity
{
	TrialProximity thisTrialProximity;
	TrialProximity otherTrialProximity;

	Trial thisTrial;
	Trial otherTrial;
	
	private void init()
	{
		//TrialProximity(Trial trial, Geocoding position, DistanceUnit distanceUnit) throws IllegalArgumentException

		thisTrial = new Trial();
		thisTrial.setPrimaryID("TEST0001");
		otherTrial = new Trial();
		otherTrial.setPrimaryID("TEST0002");
		
		thisTrialProximity = new TrialProximity( thisTrial, null, DistanceUnit.MILE );
		otherTrialProximity = new TrialProximity( otherTrial, null, DistanceUnit.MILE ); 
	}
	
	private void setupTest001()
	{
		thisTrialProximity.getTrial().setSecondaryType(Trial.SecondaryType.NOVISITSREQUIRED);
		otherTrialProximity.getTrial().setSecondaryType(Trial.SecondaryType.OTHER);
		thisTrialProximity.setDistance( 400.0 );
		otherTrialProximity.setDistance( 300.0 );
	}

	private void setupTest002()
	{
		thisTrialProximity.getTrial().setSecondaryType(Trial.SecondaryType.OTHER);
		otherTrialProximity.getTrial().setSecondaryType(Trial.SecondaryType.NOVISITSREQUIRED);
		thisTrialProximity.setDistance( 300.0 );
		otherTrialProximity.setDistance( 400.0 );
	}

	private void setupTest003()
	{
		thisTrialProximity.getTrial().setSecondaryType(Trial.SecondaryType.OTHER);
		otherTrialProximity.getTrial().setSecondaryType(Trial.SecondaryType.OTHER);
		thisTrialProximity.setDistance( 300.0 );
		otherTrialProximity.setDistance( Double.NaN );
	}

	private void setupTest004()
	{
		thisTrialProximity.getTrial().setSecondaryType(Trial.SecondaryType.OTHER);
		otherTrialProximity.getTrial().setSecondaryType(Trial.SecondaryType.OTHER);
		thisTrialProximity.setDistance( Double.NaN );
		otherTrialProximity.setDistance( 400.0 );
	}

	private void setupTest005()
	{
		thisTrialProximity.getTrial().setSecondaryType(Trial.SecondaryType.OTHER);
		otherTrialProximity.getTrial().setSecondaryType(Trial.SecondaryType.OTHER);
		thisTrialProximity.setDistance( Double.NaN );
		otherTrialProximity.setDistance( Double.NaN );
	}

	@Test
	public void compareTo()
	{
		init();
		
		// test 001:  this trial is a NOVISITSREQUIRED trial. other is not.
		setupTest001();
		assert (thisTrialProximity.compareTo(otherTrialProximity) == -1 ): "TrialProximity.compareTo failed test 001.";
		
		// test 002:  other trial is a NOVISITSREQUIRED trial. this is not.		
		setupTest002();
		assert (thisTrialProximity.compareTo(otherTrialProximity) ==  1 ): "TrialProximity.compareTo failed test 002.";
		
		// test 003:  both trials are normal trials.  this is not Nan, other is Nan.
		setupTest003();
		assert (thisTrialProximity.compareTo(otherTrialProximity) == -1 ): "TrialProximity.compareTo failed test 003.";

		// test 004:  both trials are normal trials.  this is Nan, other is not.
		setupTest004();
		assert (thisTrialProximity.compareTo(otherTrialProximity) ==  1 ): "TrialProximity.compareTo failed test 004.";

		// test 005:  both trials are normal trials.  both are Nan.
		setupTest005();
		assert (thisTrialProximity.compareTo(otherTrialProximity) ==  1 ): "TrialProximity.compareTo failed test 005.";
	}
	
	/**
	 * So that I can run the debugger.
	 * @param strings Not Used
	 */
	public static void main( String[] argv )
	{
		new TestTrialProximity().compareTo();
	}
}
