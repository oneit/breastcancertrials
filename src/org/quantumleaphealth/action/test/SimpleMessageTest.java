package org.quantumleaphealth.action.test;

import javax.mail.Address;

import org.quantumleaphealth.action.SimpleMessage;
import org.testng.annotations.Test;

/**
 * TestNg class to test avatar modifications made to SimpleMessage
 */
@Test(description = "SimpleMessage", groups = { "action", "unit" })
public class SimpleMessageTest {

  /**
   * Test to see that the email list is parced properly.  The comma shoudl be skipped, as well as the spaces.
   */
  public void getRecipients() 
  {
		String recipients = "401@bct.org wgweis@sbcglobal.net, 401@bct.org, wgweis@sbcglobal.net";

		SimpleMessage test = new SimpleMessage();
		test.setTo( recipients );
		
		Address[] address = test.getRecipients();
		for ( Address adrr: address )
		{
			System.out.println ( "SimpleMessage Would send X" + adrr + "X" );
		}
		assert address != null && address.length == 2;
  }
}
