/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action.test;

import org.quantumleaphealth.action.ScreenerGroupsSiteTrialInvitations;
import org.quantumleaphealth.model.patient.Invitation;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.screener.ScreenerGroup;
import org.quantumleaphealth.model.trial.Site;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.TrialSite;
import org.testng.annotations.Test;

/**
 * Tests for the <code>ScreenerGroupsSiteTrialInvitations</code>.
 * 
 * @author Tom Bechtold
 * @version 2009-05-28
 * @see ScreenerGroupsSiteTrialInvitations
 */
@Test(description = "ScreenerGroupsSiteTrialInvitations", groups = { "action", "unit" })
public class TestScreenerGroupsSiteTrialInvitations
{

	/**
	 * Test populating six invitations for three trials and five patients in one
	 * site in one group
	 */
	public void testGetInvitations()
	{
		// Populate five patients, four trials and one site
		UserPatient[] patients = new UserPatient[5];
		for (int index = 0; index < patients.length; index++)
		{
			patients[index] = new UserPatient();
			patients[index].setId(Long.valueOf(index));
		}
		ScreenerGroup screenerGroup = new ScreenerGroup();
		screenerGroup.setId(Long.valueOf(1l));
		screenerGroup.setName(screenerGroup.getId().toString());
		Site site = new Site();
		site.setId(Long.valueOf(1l));
		site.setName(site.getId().toString());
		TrialSite[] trialSites = new TrialSite[4];
		for (int index = 0; index < trialSites.length; index++)
		{
			Trial trial = new Trial();
			trial.setId(Long.valueOf(index));
			trial.setPrimaryID(trial.getId().toString());
			trialSites[index] = new TrialSite();
			trialSites[index].setSite(site);
			trialSites[index].setTrial(trial);
			trialSites[index].setScreenerGroup(screenerGroup);
		}
		// Add each invitation to the list
		ScreenerGroupsSiteTrialInvitations test = new ScreenerGroupsSiteTrialInvitations();
		test.add(createInvitation(1, patients[0], trialSites[0]));
		test.add(createInvitation(2, patients[1], trialSites[1]));
		test.add(createInvitation(3, patients[2], trialSites[2]));
		test.add(createInvitation(4, patients[3], trialSites[3]));
		test.add(createInvitation(5, patients[0], trialSites[3]));
		test.add(createInvitation(6, patients[4], trialSites[3]));
		// There should be only one group, one site, and four trials
		assert (test.size() == 1) : "Not one screener group: " + test.size();
		assert (test.getFirst().size() == 1) : "Not one site: " + test.getFirst().size();
		assert (test.getFirst().getFirst().size() == 4) : "Not four trials: " + test.getFirst().getFirst().size();
		assert (test.getFirst().getFirst().get(0).size() == 1) : "Not one patient in trial #0: "
				+ test.getFirst().getFirst().get(0).size();
		assert (test.getFirst().getFirst().get(1).size() == 1) : "Not one patient in trial #1: "
				+ test.getFirst().getFirst().get(1).size();
		assert (test.getFirst().getFirst().get(2).size() == 1) : "Not one patient in trial #2: "
				+ test.getFirst().getFirst().get(2).size();
		assert (test.getFirst().getFirst().get(3).size() == 3) : "Not three patients in trial #3: "
				+ test.getFirst().getFirst().get(3).size();
	}

	private static Invitation createInvitation(int id, UserPatient userPatient, TrialSite trialSite)
			throws IllegalArgumentException
	{
		if (userPatient == null)
			throw new IllegalArgumentException("patient");
		if (trialSite == null)
			throw new IllegalArgumentException("trialSite");
		Invitation invitation = new Invitation();
		invitation.setId(Long.valueOf(id));
		invitation.setUserPatient(userPatient);
		invitation.setTrialSite(trialSite);
		invitation.setPatientName(userPatient.getId().toString());
		return invitation;
	}
}
