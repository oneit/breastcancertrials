package org.quantumleaphealth.action;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * This class holds the Page Code desired to be passed to Google Analytics
 * Tracking.
 * 
 * @author Tom Maple
 * @created 19 May 2010
 */
@Name("analyticTracker")
@Scope(ScopeType.SESSION)
@AutoCreate
public class AnalyticTracker
{
	private String pageCode = null;

	public void setPageCode(String pageCode)
	{
		this.pageCode = pageCode;
	}

	public String getPageCode()
	{
		String pageCodeReturned = this.pageCode;
		this.pageCode = null;
		return pageCodeReturned;
	}
}
