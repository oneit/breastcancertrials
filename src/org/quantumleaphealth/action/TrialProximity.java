/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.util.logging.Logger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.seam.text.SeamTextLexer;
import org.jboss.seam.text.SeamTextParser;
import org.quantumleaphealth.action.TrialSiteProximity.DistanceUnit;
import org.quantumleaphealth.model.trial.Geocoding;
import org.quantumleaphealth.model.trial.Site;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.TrialSite;

import antlr.RecognitionException;
import antlr.TokenStreamException;

/**
 * How close a trial's site is.
 * 
 * @author Tom Bechtold
 * @version 2009-11-18
 * 
 * Modifications:
 * @author Warren George Weis
 * @version 2014-05-15
 * 	Modifications to make TrialProxmity subclassable:  made instance variables protected and non-final.
 * 	Modifications to make TrialProximity a virtual proxy for trial:  added indirect getters for some trial fields used on the browse_trials page.
 * 
 */
public class TrialProximity implements Comparable<TrialProximity>, Serializable
{
	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = 7863606466996460834L;

    /**
     * Logger.  Can't be injected as this is not a managed bean.
     */
    private final Log logger = LogFactory.getLog(TrialProximity.class);


	/**
	 * The trial, guaranteed to be non-<code>null</code>
	 */
	protected Trial trial;
	
	/**
	 * The closest site or <code>null</code> if trial has no sites with
	 * geocoding
	 */
	protected Site site;
	
	/**
	 * The distance or <code>Double.NaN</code> if distance is unavailable
	 */
	protected double distance;
	
	/**
	 * The distance unit of measure, guaranteed to be non-<code>null</code>
	 */
	protected DistanceUnit distanceUnit;

	/**
	 * Default constructor for use by the LuceneSearchTrialProximity subclass constructor
	 */
	public TrialProximity() {}
	
	/**
	 * Stores trial and unit into instance variables and finds closest site.
	 * 
	 * @param trial
	 *            the trial
	 * @param position
	 *            the position to find the closest site or <code>null</code> for
	 *            no proximity
	 * @param distanceUnit
	 *            the unit of distance
	 * @throws IllegalArgumentException
	 *             if either <code>trial</code> or <code>distanceUnit</code> is
	 *             <code>null</code>
	 */
	public TrialProximity(Trial trial, Geocoding position, DistanceUnit distanceUnit) throws IllegalArgumentException
	{
		// Validate parameters
		if (trial == null)
			throw new IllegalArgumentException("trial must be specified");
		if (distanceUnit == null)
			throw new IllegalArgumentException("unit must be specified");
		this.trial = trial;
		this.distanceUnit = distanceUnit;
		// Loop over every site with available position
		Site closestSite = null;
		double closestDistance = Double.NaN;
		for (TrialSite trialSite : trial.getTrialSite())
			if (trialSite.getSite() != null)
			{
				Site currentSite = trialSite.getSite();
				if (currentSite.getGeocoding() == null)
					continue;
				// If no distance specified then just find the first site that
				// has a position
				if (position == null)
				{
					closestSite = currentSite;
					break;
				}
				double currentDistance = currentSite.getGeocoding().getDistanceMiles(position);
				if ((closestSite == null) || Double.isNaN(closestDistance)
						|| (!Double.isNaN(currentDistance) && (currentDistance < closestDistance)))
				{
					closestDistance = currentDistance;
					closestSite = currentSite;
				}
			}
		site = closestSite;
		if (Double.isNaN(closestDistance))
		{
			// BCT-453 For trials with no site info, set the distance to -0.1 so
			// they will be at the top of a distance sorted list
			// (it is expected these will be Pharmaceutical trials)
			closestDistance = new Double(-0.1);
		}
		else
		{
			// Convert distance units from miles
			if (DistanceUnit.KILOMETER.equals(distanceUnit))
				closestDistance *= 1.609344d;
			else if (!DistanceUnit.MILE.equals(distanceUnit))
				throw new IllegalArgumentException("distance unit " + distanceUnit + " not supported");
		}
		setDistance(closestDistance);
	}

	/**
	 * @return the trial, guaranteed to be non-<code>null</code>
	 */
	public Trial getTrial()
	{
		return trial;
	}
	
	/**
	 * Virtual proxy getters.  We implement these methods to make TrialProxmity a virtual proxy for its trial instance. 
	 * We will override these in a subclass so that we can return the Lucene highlighted text rather than the actual trial text.
	 * Please see JIRA issue BCT-260 for more information.
	 * 
	 * These are the fields indexed by the lucene indexer for searches:
	 * 	trial.name
	 * 	trial.title
	 *  trial.purpose
	 *  trial.duration
	 *  trial.followup
	 *  trial.sponsor
	 *  trial.burden
	 * 
	 */
	/**
	 * Equivalent of getTrial().getName()
	 */
	public String getTrialName()
	{
		return trial == null? "": trial.getName();
	}

	/**
	 * Equivalent of getTrial().getTitle()
	 */
	public String getTrialTitle()
	{
		return trial == null? "": trial.getTitle();
	}
	
	/**
	 * Equivalent of getTrial().getPurpose()
	 */
	public String getTrialPurpose()
	{
		return trial == null? "": trial.getPurpose();
	}
	
	/**
	 * Equivalent of getTrial().getTreatmentPlan
	 * We have to call the SeamTextParser lexer here, as we are no longer using the <s:format> tag.
	 * Please see {@link #LuceneSearchTrialProxmity.getHighlightedHTMLSearchString}
	 */
	public String getTrialTreatmentPlan()
	{
		String s = trial == null? "": trial.getTreatmentPlan();
	    Reader reader = new StringReader(s == null? "": s);
		SeamTextLexer lexer = new SeamTextLexer(reader);
		SeamTextParser seamTextParser = new SeamTextParser(lexer);
		try 
		{
			seamTextParser.startRule();
		} 
		catch (RecognitionException e) 
		{
			logger.error("Exception parsing text for trial " + trial.getPrimaryID() + ":\n" + s, e);
			return "";
		} 
		catch (TokenStreamException e) 
		{
			logger.error("Exception parsing text for trial " + trial.getPrimaryID() + ":\n" + s, e);
			return "";
		}
		return seamTextParser.toString();
	}
	
	/**
	 * Equivalent of getTrial().getProcedures
	 */
	public String getTrialProcedures()
	{
		return trial == null? "": trial.getProcedures();
	}
	
	/**
	 * Equivalent of getTrial().getDuration()
	 */
	public String getTrialDuration()
	{
		return trial == null? "": trial.getDuration();
	}
	
	/**
	 * Equivalent of getTrial().getFollowup()
	 */
	public String getTrialFollowup()
	{
		return trial == null? "": trial.getFollowup();
	}

	/**
	 * Equivalent of getTrial().getSponsor()
	 */
	public String getTrialSponsor()
	{
		return trial == null? "": trial.getSponsor();
	}

	/**
	 * Equivalent of getTrial().getSponsor()
	 */
	public String getTrialBurden()
	{
		return trial == null? "": trial.getBurden();
	}

	/****************************** End of Virtual Proxy methods ******************************/
	
	/**
	 * @return the closest site or <code>null</code> if trial has no sites
	 */
	public Site getSite()
	{
		return site;
	}

	/**
	 * @return the distance or <code>Double.NaN</code> if trial has no sites
	 */
	public double getDistance()
	{
		return distance;
	}

	/**
	 * @param distance the distance to set
	 */
	public void setDistance(double distance)
	{
		this.distance = distance;
	}

	/**
	 * @return the distance unit of measure, guaranteed to be non-
	 *         <code>null</code>
	 */
	public DistanceUnit getDistanceUnit()
	{
		return distanceUnit;
	}

	/**
	 * @return whether any site's position is available
	 */
	public boolean isAvailable()
	{
		return (site != null);
	}

	/**
	 * @return whether the closest site is known
	 */
	public boolean isKnown()
	{
		return (site != null) && (getDistance() >= 0);
	}

	/**
	 * @return Trial Id formatted with leading zeroes, suitable for inclusion in
	 *         a URL contacting cancer.gov
	 */
	public String getCdrid()
	{
		return (trial != null) ? "CDR"
				+ org.apache.commons.lang.StringUtils.leftPad(String.valueOf(trial.getId()), 10, '0') : null;
	}

	/**
	 * Compares its trial to another's trial for distance, trial type and number
	 * of sites. This method first compares distances. If both distances are
	 * unavailable or distances are equal then compare the trials' types. If
	 * types are equal then compare the trials' number of sites (more sites is
	 * winner).
	 * BCT-496 modification:  trials which require no visits to the trial sites,
	 * such as online surveys or mail only surveys, are ordered at the top by setting
	 * their distance from the patient to zero.
	 * 
	 * @return a negative number if a) its distance is less than the other's or
	 *         b) distances are available or equal and its trial type is
	 *         alphabetically before the others' or c) distances are available
	 *         or equal and types are equal and its number of sites is more than
	 *         the other's; zero if the distances, trial types and number of
	 *         sites are equal or positive otherwise
	 * @param the
	 *            other trial's proximity
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public int compareTo(TrialProximity other) throws IllegalArgumentException
	{
		// Validate parameter
		if (other == null)
			throw new NullPointerException("other trial proximity");

		// case one:  this wins if no visits are required, and other is a normal trial
		if ( this.getTrial().inNoVisitsRequired() == true && other.getTrial().inNoVisitsRequired() == false)
		{
			return -1;
		}
		
		// case two: other wins if no visits required and this is a normal trial
		if ( this.getTrial().inNoVisitsRequired() == false && other.getTrial().inNoVisitsRequired() == true)
		{
			return 1;
		}

		// at this point both are either No Visits Required, or both are not.  Either way the Nan rule now takes precedence.
		// Compare distances; known proximity is always closer than unknown
		boolean isNaN = Double.isNaN( this.getDistance() );
		if (isNaN != Double.isNaN( other.getDistance()) )
		{	
			return isNaN ? 1 : -1;
		}	

		// BCT-496: if both trials have a secondary category of NOVISITSREQUIRED, then this is a "tie".  Go on to alternative sorting.
		if (!isNaN && !( this.getTrial().inNoVisitsRequired() == true && other.getTrial().inNoVisitsRequired() == true ))
		{	
			return (int) Math.signum(this.getDistance() - other.getDistance() );
		}	

		// Compare trial types; trial is guaranteed to be non-null by
		// constructor		
		int trialTypeCompare = Trial.TRIALTYPE_COMPARATOR_ENGLISH.compare(trial.getType(), other.trial.getType());
		
		// If types are equivalent then more sites is closer; sites list is
		// guaranteed to be non-null by constructor
		return (trialTypeCompare != 0) ? trialTypeCompare : (other.trial.getTrialSite().size() - trial.getTrialSite()
				.size());
	}

	/**
	 * Returns whether this proximity contains the same trial as anothers'
	 * 
	 * @return whether this proximity contains the same trial as anothers'
	 * @param other
	 *            the other trial proximity
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other)
	{
		if (super.equals(other))
			return true;
		return (other != null) && (other instanceof TrialProximity) && trial.equals(((TrialProximity) (other)).trial);
	}

	/**
	 * Returns the hash code of the owned trial
	 * 
	 * @return the hash code of the owned trial
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return trial == null ? super.hashCode() : trial.hashCode();
	}

	/**
	 * Returns the class name, memory location, trial, closest site, distance
	 * and unit
	 * 
	 * @return the class name, memory location, trial, closest site, distance
	 *         and unit
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return getClass().getName() + '@' + super.hashCode() + '{' + trial + '@' + site + '='
				+ (Double.isNaN(getDistance()) ? '?' : Double.toString(getDistance()) + distanceUnit) + '}';
	}
}
