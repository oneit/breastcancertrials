/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;

/**
 * Updates the associations of each patient. Instances of this Enterprise
 * JavaBean are available for both local and remote access.
 * 
 * @author Tom Bechtold
 * @version 2009-03-24
 */
@Stateless
@Name("patientsAssociationManager")
public class PatientsAssociationManagerAction implements PatientsAssociationManagerLocal,
		PatientsAssociationManagerRemote, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 139813480953260022L;
	
	/**
	 * Loads the patient data using EJB3 persistence manager
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;
	/**
	 * Manages the patient's associations
	 */
	@EJB
	private PatientAssociationManager patientAssociationManager;
	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log logger;

	/**
	 * Updates all patients' matching trials and removes their obsolete
	 * invitations. This method fetches the list of patient identifiers from the
	 * persistent storage and passes each identifier to the patient manager bean
	 * with the current date/time.
	 * 
	 * @see org.quantumleaphealth.action.PatientsAssociationManager#updateScreenedTrialsAndInvitations()
	 * @see PatientAssociationManager#updateScreenedTrialsAndInvitations(Long,
	 *      Date)
	 */
	@SuppressWarnings( { "unchecked", "cast" })
	public void updateScreenedTrialsAndInvitations()
	{
		// Load patient ids in order
		List<Long> patientIds = (List<Long>) em.createQuery("select u.id from UserPatient u order by u.id")
				.getResultList();
		Date updated = new Date();
		for (Long patientId : patientIds)
			patientAssociationManager.updateScreenedTrialsAndInvitations(patientId, updated);
		logger.debug("Updated screened trials and invitations for #0 offline patients", patientIds.size());
	}
}
