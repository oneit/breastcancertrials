/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.StringReader;
import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeMap;

import javax.annotation.PostConstruct;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Context;
import org.jboss.seam.contexts.Contexts;
import org.jboss.seam.text.SeamTextLexer;
import org.jboss.seam.text.SeamTextParser;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.screen.MatchingEngine;

import antlr.RecognitionException;
import antlr.TokenStreamException;

/**
 * A sorted list of trials that contain parsing errors in one or more text
 * fields. This class inspects each <code>Trial.treatmentPlan</code> field of
 * <code>engine</code> for parsing errors. As a <em>POJO</em> (Plain Old Java
 * Object), Seam stores instances in the default <code>event</code> scope.
 * 
 * @author Tom Bechtold
 * @version 2009-06-23
 * @see MatchingEngine
 * @see Trial#getTreatmentPlan()
 */
@Name("trialsRecognitionException")
public class TrialsRecognitionException extends TreeMap<Trial, RecognitionException>
{
	/**
	 * Compares a trial's primary identifier
	 */
	private static final Comparator<Trial> TRIAL_COMPARATOR = new Comparator<Trial>()
	{
		/**
		 * @return how the trials' primary id's compare or <code>0</code> if
		 *         both trials are equal or <code>null</code>, both trials'
		 *         primary ids are <code>null</code> or both trials' primary ids
		 *         are equal
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		@Override
		public int compare(Trial trial1, Trial trial2)
		{
			// Check for equality and double-null first
			if (trial1 == trial2)
				return 0;
			// Known is always sorted before unknown
			if (trial1 == null)
				return 1;
			if (trial2 == null)
				return -1;
			if (trial1.getPrimaryID() == null)
				return (trial2.getPrimaryID() == null) ? 0 : 1;
			if (trial2.getPrimaryID() == null)
				return -1;
			return trial1.getPrimaryID().compareTo(trial2.getPrimaryID());
		}
	};

	public TrialsRecognitionException()
	{
		super(TRIAL_COMPARATOR);
	}

	/**
	 * Populates itself with parsing errors from trials'
	 * <code>treatmentPlan</code> field
	 */
	@PostConstruct
	public void parse()
	{
		// Get engine from Seam application context
		Context applicationContext = Contexts.getApplicationContext();
		Object engineObject = (applicationContext == null) ? null : applicationContext.get("engine");
		if (!(engineObject instanceof MatchingEngine))
		{
			put(null, new RecognitionException("Could not load matching engine"));
			return;
		}
		MatchingEngine engine = (MatchingEngine) (engineObject);
		// If no trials then do nothing
		if (engine.getTrialCount() == 0)
			return;
		Iterator<Trial> iterator = engine.getTrialIterator();
		while (iterator.hasNext())
		{
			Trial trial = iterator.next();
			// Trial must contain a treatment plan
			String treatmentPlan = trial.getTreatmentPlan();
			if ((treatmentPlan == null) || (treatmentPlan.trim().length() == 0))
			{
				put(trial, new RecognitionException("No treatment plan"));
				continue;
			}
			try
			{
				// This code found in both SeamText validator and SeamText
				// formatter components
				new SeamTextParser(new SeamTextLexer(new StringReader(treatmentPlan))).startRule();
			}
			catch (TokenStreamException tokenStreamException)
			{
				put(trial, new RecognitionException(tokenStreamException.getMessage()));
			}
			catch (RecognitionException recognitionException)
			{
				put(trial, recognitionException);
			}
		}
	}

	/**
	 * UID for serializable class
	 */
	private static final long serialVersionUID = -50820261086804459L;
}
