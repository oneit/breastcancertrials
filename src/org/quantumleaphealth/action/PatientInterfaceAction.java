/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.annotations.intercept.PrePassivate;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.faces.FacesManager;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.LocaleSelector;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.jboss.seam.web.Session;
import org.quantumleaphealth.action.TrialSiteProximity.DistanceUnit;
import org.quantumleaphealth.action.navigator.NavigatorInterface;
import org.quantumleaphealth.model.navigator.Navigator_Client;
import org.quantumleaphealth.model.patient.Invitation;
import org.quantumleaphealth.model.patient.ScreenedTrial;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.PatientTrial;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.screen.EligibilityProfile;
import org.quantumleaphealth.screen.MatchingEngine;

import org.apache.commons.lang.StringEscapeUtils;

/**
 * Navigates a patient through collecting, validating, retrieving and persisting
 * information.
 * 
 * Navigator Revision:  the patientInterface pulls information on clients managed by the 
 * navigator.  Information cannot be pushed the other direction or we get a concurrency exception
 * as both beans are session level.  See also the @SerializedConcurrentAccess  annotation on the NavigatorInterfaceAction bean.
 * The annotation forces access to be serialized to avoid contention problems.
 * 
 * @author Tom Bechtold
 * @version 2009-06-18
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("patientInterface")
public class PatientInterfaceAction implements PatientInterface, Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7535780899873541945L;

	enum HistoryCategoryByPassTypes
	{
		NEW_FIRST_DIAGNOSIS("NewFirstDiagnosis"),
		NEW_RECURRENCE("NewRecurrence"),
		NEW_SECOND_DIAGNOSIS("NewSecondDiagnosis"),
		RECENT_FIRST_DIAGNOSIS("RecentFirstDiagnosis"),
		RECENT_RECURRENCE("RecentRecurrence"),
		RECENT_SECOND_DIAGNOSIS("RecentSecondDiagnosis"),
		MANAGING_COMPLETE("managingComplete"),
		FINISHED_COMPLETE("finishedComplete"),
		HORMONE_COMPLETE("HormoneComplete");
		
		private String byPassType;
		HistoryCategoryByPassTypes(String byPassType)
		{
			this.byPassType = byPassType;
		}
		
		public String getByPassType()
		{
			return byPassType;
		}
	}
	
	static final Map<String, Long> historyCategoryPrimaryBypass = new HashMap<String, Long>();
	{
		historyCategoryPrimaryBypass.put(HistoryCategoryByPassTypes.NEW_FIRST_DIAGNOSIS.getByPassType(), 2L);
		historyCategoryPrimaryBypass.put(HistoryCategoryByPassTypes.NEW_RECURRENCE.getByPassType(), 4L);
		historyCategoryPrimaryBypass.put(HistoryCategoryByPassTypes.NEW_SECOND_DIAGNOSIS.getByPassType(), 8L);
		historyCategoryPrimaryBypass.put(HistoryCategoryByPassTypes.RECENT_FIRST_DIAGNOSIS.getByPassType(), 2L);
		historyCategoryPrimaryBypass.put(HistoryCategoryByPassTypes.RECENT_RECURRENCE.getByPassType(), 4L);
		historyCategoryPrimaryBypass.put(HistoryCategoryByPassTypes.RECENT_SECOND_DIAGNOSIS.getByPassType(), 8L);
	}
	
	static final Map<String, Long> historyCategoryBypass = new HashMap<String, Long>();
	{
		historyCategoryBypass.put(HistoryCategoryByPassTypes.NEW_FIRST_DIAGNOSIS.getByPassType(), 4L);
		historyCategoryBypass.put(HistoryCategoryByPassTypes.NEW_RECURRENCE.getByPassType(), 4L);
		historyCategoryBypass.put(HistoryCategoryByPassTypes.NEW_SECOND_DIAGNOSIS.getByPassType(), 4L);
		historyCategoryBypass.put(HistoryCategoryByPassTypes.RECENT_FIRST_DIAGNOSIS.getByPassType(), 4L);
		historyCategoryBypass.put(HistoryCategoryByPassTypes.RECENT_RECURRENCE.getByPassType(), 4L);
		historyCategoryBypass.put(HistoryCategoryByPassTypes.RECENT_SECOND_DIAGNOSIS.getByPassType(), 4L);
		
		historyCategoryBypass.put(HistoryCategoryByPassTypes.MANAGING_COMPLETE.getByPassType(), 8L);
		historyCategoryBypass.put(HistoryCategoryByPassTypes.FINISHED_COMPLETE.getByPassType(), 16L);
		historyCategoryBypass.put(HistoryCategoryByPassTypes.HORMONE_COMPLETE.getByPassType(), 16L);
	}
	
	/**
	 * Persists the entity beans using EJB3 persistence context
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;
	/**
	 * The matching engine
	 */
	@In
	private MatchingEngine engine;
	/**
	 * Manages the patient's associations
	 */
	@EJB
	private PatientAssociationManager patientAssociationManager;
	/**
	 * The geocoding engine
	 */
	@EJB
	private PostalCodeManager postalCodeManager;
	/**
	 * The messaging engine
	 */
	@EJB
	private MessageSender messageSender;
	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log logger;

	/**
	 * The Google Analytics tracking object
	 */
//	@In(create = true)
//	private AnalyticTracker analyticTracker;

	/**
	 *  The facade avatar Id.
	 */
	@RequestParameter("aid")
	private Long avatarId;

	/**
	 * Possible forwarding URL?
	 */
	@RequestParameter("fw")
	private String forwardingURL;

//	@RequestParameter("navigatorClientId")
//	private Long navigatorClientId;
	
	/** 
	 * Patient Type, if coming from the terms page
	 */
	private String patientType;
	
	/**
	 * The current patient, guaranteed to be non-<code>null</code>.
	 * 
	 * @see #getPatient()
	 * @see #setPatient(UserPatient)
	 */
	private UserPatient patient;
	/**
	 * The user's locale, either en (English) or es (Spanish)
	 */
	private Locale locale = Locale.ENGLISH;
	/**
	 * The user's credentials that is verified against the password
	 */
	private String credentials;
	/**
	 * The email address is verified against the patient's principal field
	 * (email address)
	 */
	private String emailAddressVerify;
	/**
	 * Whether the user is logging in after logging out.
	 */
	private boolean returningUser;
	/**
	 * The facade into the patient's history, guaranteed to be non-
	 * <code>null</code>.
	 */
	private final FacadePatientHistory facadePatientHistory;

	/**
	 * The sorted list of trials using miles as the proximity unit of measure
	 * and grouped by proximity, guaranteed to be non-<code>null</code>.
	 */
	private final TrialProximityGroups trials = new TrialProximityGroups(DistanceUnit.MILE, true);
	/**
	 * The identifier of the trial to return the sites for or <code>null</code>
	 * if not set
	 */
	private Long trialId;
	/**
	 * The sites for the trial referenced by <code>trialId</code> sorted by
	 * closest research site to <code>geocoding</code> or <code>null</code> if
	 * no list generated yet
	 */
	private TrialSiteProximities sortedTrialSites;
	/**
	 * The patient's eligibility profiles sorted by trial primary id and name or
	 * <code>null</code> if no list generated yet
	 */
	private List<EligibilityProfile> eligibilityProfiles;

	/**
	 * The number of milliseconds in a 30-day month.
	 */
	private static final long MILLIS_MONTH = 1000l * 60l * 60l * 24l * 30l;
	/**
	 * The non-localized message when authentication fails
	 */
	private static final String ERRORMESSAGE_UNAUTHENTICATED = "The email address or password you entered is not correct. Please try again";
	/**
	 * The maximum number of unsuccessful authentication attempts.
	 * 
	 * @see #login()
	 */
	private static final int MAXIMUM_UNSUCCESSFULAUTHENTICATIONCOUNT = 5;
	/**
	 * The maximum length of the principal. This must be less than or equal to
	 * the string length defined in the database schema.
	 */
	private static final int LENGTH_PRINCIPAL_MAXIMUM = 64;
	/**
	 * The length of the randomly-generated credentials
	 */
	private static final int LENGTH_RANDOMCREDENTIALS = 6;
	/**
	 * Random number generator
	 */
	private static final Random RANDOM = new Random();
	/**
	 * Nonlocalized warning message if the category is not valid
	 */
	private static final FacesMessage CATEGORY_INVALID = new FacesMessage(FacesMessage.SEVERITY_ERROR,
			"Category not entered", "Please answer the question above.");

	/**
	 * Country code for United States
	 */
	public static final String COUNTRY_UNITEDSTATES = "US";

	private static final Locale SPANISH = new Locale("es");
	
	private AvatarHistoryConfiguration avatarHistoryConfiguration;

	@In(required=false)
	private
	NavigatorInterface navigatorInterface;

	/**
	 * Instantiates the object with blank user and history fields.
	 */
	public PatientInterfaceAction()
	{
		// Store new patient and history objects into guaranteed-non-null
		// facades
		patient = new UserPatient();
		facadePatientHistory = new FacadePatientHistory(patient.getPatientHistory());
		setAvatarHistoryConfiguration(new AvatarHistoryConfiguration(this));
	}

	/**
	 * @return the patient, guaranteed to be non-<code>null</code>.
	 * @see org.quantumleaphealth.action.PatientInterface#getPatient()
	 */
	@BypassInterceptors
	public UserPatient getPatient()
	{
		return patient;
	}

	/**
	 * Update the patient and the facades into its history.
	 * 
	 * @param newPatient
	 *            the new patient or <code>null</code> for a blank patient
	 */
/**WGW Navigator Portal made this public so it can be accessed from NavigatorInterfaceAction **/
	@BypassInterceptors
	public void setPatient(UserPatient newPatient)
	{
		// If patient did not change then do nothing
		if (newPatient == patient)
			return;
		boolean fresh = (newPatient == null);
		patient = fresh ? new UserPatient() : newPatient;

		// Update the characteristic facades
		getFacadePatientHistory().setPatientHistory(patient.getPatientHistory(), fresh);
		setAvatarHistoryConfiguration(new AvatarHistoryConfiguration(this));
		emailAddressVerify = patient.getPrincipal();
	}

	/**
	 * @return the locale
	 */
	@BypassInterceptors
	public Locale getLocale()
	{
		return locale;
	}

	/**
	 * @param locale
	 *            the locale to set
	 */
	public void setLocale(Locale locale)
	{
		this.locale = locale;
	}

	/**
	 * @return the patient's characteristics, guaranteed to be non-
	 *         <code>null</code>.
	 * @see org.quantumleaphealth.action.PatientInterface#getHistory()
	 */
	@BypassInterceptors
	public FacadePatientHistory getHistory()
	{
		return getFacadePatientHistory();
	}

	/**
	 * @return the number of months old a patient's history is or
	 *         <code>-1</code> if the history is incomplete
	 * @see org.quantumleaphealth.action.PatientInterface#getPatientHistoryAgeMonths()
	 */
	@BypassInterceptors
	public int getPatientHistoryAgeMonths()
	{
		// Return -1 if incomplete or not known
		return (!patient.isPatientHistoryComplete() || (patient.getPatientHistoryLastModified() == null)) ? -1
				: (int) Math.floor((System.currentTimeMillis() - patient.getPatientHistoryLastModified().getTime())
						/ MILLIS_MONTH);
	}

	/**
	 * Returns the user's postal code or <code>null</code> if none Note: JSF's
	 * convertNumber only works with <code>Long</code>.
	 * 
	 * @return the user's postal code or <code>null</code> if none
	 * @see org.quantumleaphealth.action.PatientInterface#getPostalCode()
	 */
	public Long getPostalCode()
	{
		int postalCode = trials.getPostalCode();
		return postalCode <= 0 ? null : Long.valueOf(postalCode);
	}

	/**
	 * Stores the user's postal code and clears the list of sorted trials. This
	 * method does nothing if the parameter is <code>null</code> or has not
	 * changed.
	 * 
	 * @param postalCode
	 *            the user's postal code or <code>null</code> if none
	 * @see org.quantumleaphealth.action.PatientInterface#setPostalCode(java.lang.Long)
	 */
	public void setPostalCode(Long postalCode)
	{
		// Do nothing if null or not changing
		if ((postalCode == null) || postalCode.equals(getPostalCode()))
			return;
		// Cache postal code, fetch geocoding and clear the sorted trials so
		// they will be regenerated
		logger.debug("Setting zip code for #0 to #1", patient.getPrincipal(), postalCode);
		trials.setPostalCodeGeocoding(postalCode.intValue(), getPostalCodeManager().getPostalCodeGeocoding(
				postalCode.intValue()).getGeocoding());
		sortedTrialSites = null;
	}

	/**
	 * @return the identifier of the trial to list sites for or
	 *         <code>null</code> if none
	 * @see org.quantumleaphealth.action.PatientInterface#getTrialId()
	 */
	@BypassInterceptors
	public Long getTrialId()
	{
		return trialId;
	}

	/**
	 * Stores the identifier of the trial to list sites for and clears the list
	 * of sorted trial sites. This method does nothing if the parameter is
	 * <code>null</code> or has not changed.
	 * 
	 * @param trialId
	 *            the identifier of the trial to list sites for or
	 *            <code>null</code> if none
	 * @see org.quantumleaphealth.action.PatientInterface#setTrialId(long)
	 */
	@RequestParameter
	public void setTrialId(Long trialId)
	{
		// Do nothing if null or not changing
		if ((trialId == null) || trialId.equals(this.trialId))
			return;
		// Cache and clear the sorted trial sites so they will be regenerated
		this.trialId = trialId;
		sortedTrialSites = null;
	}

	/**
	 * @return the user's credentials or <code>null</code> if new user
	 * @see org.quantumleaphealth.action.PatientInterface#getCredentials()
	 */
	@BypassInterceptors
	public String getCredentials()
	{
		return credentials;
	}

	/**
	 * Trims and stores the credentials.
	 * 
	 * @param credentials
	 *            the user's credentials or <code>null</code> if new user
	 * @see org.quantumleaphealth.action.PatientInterface#setCredentials(java.lang.String)
	 */
	@BypassInterceptors
	public void setCredentials(String credentials)
	{
		if (credentials != null)
		{
			credentials = credentials.trim();
			if (credentials.length() == 0)
				credentials = null;
		}
		this.credentials = credentials;
	}

	/**
	 * @return the emailAddressVerify
	 */
	@BypassInterceptors
	public String getEmailAddressVerify()
	{
		return emailAddressVerify;
	}

	/**
	 * @param emailAddressVerify
	 *            the emailAddressVerify to set
	 */
	@BypassInterceptors
	public void setEmailAddressVerify(String emailAddressVerify)
	{
		this.emailAddressVerify = emailAddressVerify;
	}

	/**
	 * Updates the patient's history timestamp and complete status, updates the
	 * list of matching trials and invitations and persists changes for
	 * returning users. This method first reattaches the patient to the
	 * persistence context (if the patient has been persisted) before updating
	 * its associations.
	 * 
	 * @see org.quantumleaphealth.action.PatientInterface#updateHistory()
	 */
	public void updateHistory()
	{
		logger.debug("Updating history for ##0", patient.getUniqueId());
		if (patient.isAvatarProfile()) 
		{
			getAvatarHistoryConfiguration().updateAvatarHistory(getHistory());
		}
		// Reattach if persisted before changing properties and associations
		merge();
		patient.setPatientHistoryLastModified(new Date());
		// Update screenedTrials and invitations associations; the patient is
		// attached
		// AVATAR: avatar profiles are always considered complete.
		getPatientAssociationManager().updateScreenedTrialsAndInvitations(patient, trials, null, patient.isAvatarProfile() ? true: getFacadePatientHistory().isComplete(), true);
	}

	/**
	 * @return the patient's screened trials sorted by type and closest research
	 *         site, guaranteed to be non-<code>null</code>
	 * @see org.quantumleaphealth.action.PatientInterface#getTrials()
	 */
	@BypassInterceptors
	public TrialProximityGroups getTrials()
	{
		//For the 2012 web rewrite the default sort for my_trials is type.  We are not allowing sort by zip code.
		trials.setGroupedByProximityBoolean(false);
		return trials;
	}

	/**
	 * @return the research sites for a trial sorted by closest site or
	 *         <code>null</code> if no trial specified by
	 *         <code>setTrialId</code>
	 * @see org.quantumleaphealth.action.PatientInterface#getSortedTrialSites()
	 */
	public TrialSiteProximities getSortedTrialSites()
	{
		// Return null if no trial id set
		if (trialId == null)
			return null;

		// Copy and sort list if not cached
		if (sortedTrialSites == null)
		{
			// Return null and invalidate trial id property if requested trial
			// is not found in the screened list
			logger.debug("Finding sites for trial ##0", trialId);
			Trial trial = trials.get(trialId);
			// If trial not found then
			if (trial == null)
			{
				trialId = null;
				return null;
			}
			// Build the list of sites and sort on geocoding
			sortedTrialSites = new TrialSiteProximities(trial, trials.getGeocoding(),
					TrialSiteProximity.DistanceUnit.MILE);
			logger.debug("Sorted #0 sites for trial ##1", sortedTrialSites.size(), trialId);
		}
		return sortedTrialSites;
	}

	/**
	 * @return the number of screened trials for the patient
	 * @see org.quantumleaphealth.action.PatientInterface#getScreenedTrialCount()
	 */
	public int getScreenedTrialCount()
	{
		// If history is incomplete then return zero
		return !patient.isPatientHistoryComplete() ? 0 : patient.getScreenedTrials().size();
	}

	/**
	 * @return the patient's eligibility profiles for all loaded trials
	 * @see org.quantumleaphealth.action.PatientInterface#getEligibilityProfiles()
	 */
	public List<EligibilityProfile> getEligibilityProfiles()
	{
		// Return cached value if available
		if (eligibilityProfiles == null)
		{
			// Use linked list for sequential access, although sorting will be
			// slower
			eligibilityProfiles = new LinkedList<EligibilityProfile>();
			// Iterate over all loaded trials, using trial's primary id and name
			// for the sorted profile description
			Iterator<Trial> trials = getEngine().getTrialIterator();
			while (trials.hasNext())
			{
				Trial trial = trials.next();
				String description = null;
				if ((trial != null) && (trial.getName() != null) && (trial.getPrimaryID() != null))
					description = trial.getPrimaryID() + ' ' + trial.getName();
				eligibilityProfiles.add(getEngine().getEligibilityProfile(trial.getId(), patient.getPatientHistory(),
						description));
			}
			Collections.sort(eligibilityProfiles);
			logger.debug("Sorted profiles for #0", patient.getPrincipal());
		}
		return eligibilityProfiles;
	}

	/**
	 * This method sets the AnalyticTracker pageCode property based on the
	 * patient's record.
	 */
	private void generateAnalyticTrackingCode()
	{
		// BCT-365 add a page code for sending to Google Analytics
		// StringBuffer generatedPageCode = new StringBuffer( 50 );
		// if ( this.isFirstTime() )
		// generatedPageCode.append( "new" );
		// else
		// generatedPageCode.append( "returning" );
		// if ( patient.isPatientHistoryComplete() )
		// generatedPageCode.append( "-completed-" );
		// else
		// generatedPageCode.append( "-not_completed-" );
		// if ( patient.isAlertNewTrialsEnabled() )
		// generatedPageCode.append( "trialAlert" );
		// else
		// generatedPageCode.append( "no_trialAlert" );
		// this.analyticTracker.setPageCode( generatedPageCode.toString() );
	}

	/**
	 * Returns whether the patient is a first-time user, e.g., if not persisted
	 * yet or last login is the same as persisted time
	 * 
	 * @return <code>true</code> if the patient is a first-time user
	 * @see org.quantumleaphealth.action.PatientInterface#isFirstTime()
	 */
	@BypassInterceptors
	public boolean isFirstTime()
	{
		return !returningUser;
	}

	/**
	 * Returns whether the patient accepted the terms/conditions and entered
	 * their location. This method returns <code>true</code> if all of the
	 * following apply:
	 * <ol>
	 * <li>the <code>termsAccepted</code> is <code>true</code></li>
	 * <li>the <code>country</code> is not empty</li>
	 * <li>the <code>country</code> is not <code>COUNTRY_UNITEDSTATES</code> or
	 * the <code>private postal code</code> is positive</li>
	 * </ol>
	 * 
	 * @return <code>true</code> if the terms were accepted
	 * @see org.quantumleaphealth.action.PatientInterface#isTermsAcceptedValid()
	 */
	@BypassInterceptors
	public boolean isTermsAcceptedValid()
	{
		return patient.isTermsAccepted()
				&& (patient.getCountryCode() != null)
				&& (patient.getCountryCode().trim().length() > 0)
				&& (!COUNTRY_UNITEDSTATES.equals(patient.getCountryCode()) || ((patient.getPrivatePostalCode() != null)
						&& (patient.getPrivatePostalCode().shortValue() > 0) && (patient.getPrivatePostalCode()
						.shortValue() < 1000)));
	}

	/**
	 * Validates that the termsAccepted checkbox was checked. This method adds a
	 * non-localized faces messages if the component was not checked.
	 * 
	 * @param facesContext
	 *            the faces context
	 * @param component
	 *            the checkbox component
	 * @param value
	 *            the component's value
	 * @see org.quantumleaphealth.action.PatientInterface#validateTermsAccepted(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object)
	 */
	public void validateTermsAccepted(FacesContext facesContext, UIComponent component, Object value)
	{
		logger.debug("Validating termsAccepted '#0'", value);
		if (!Boolean.TRUE.equals(value))
		{
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"Please check the Terms & Conditions box before clicking the Start button.");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
		}
	}

	/**
	 * Validates that the country code is entered and, if the country is US,
	 * that the private postal code is entered. This method adds a non-localized
	 * faces messages if submissions are missing.
	 * 
	 * @param facesContext
	 *            the faces context
	 * @param component
	 *            the input component
	 * @param value
	 *            the component's value
	 * @see org.quantumleaphealth.action.PatientInterface#validateCountryCode(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object)
	 */
	public void validateCountryCode(FacesContext facesContext, UIComponent component, Object value)
	{
		logger.debug("Validating countryCode '#0'", value);
		String countryCode = (value instanceof String) ? (String) (value) : null;
		if ((countryCode == null) || (countryCode.trim().length() == 0))
		{
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"Please choose a country/continent of residence");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
		}
		if (!COUNTRY_UNITEDSTATES.equals(countryCode))
			return;
		UIComponent privatePostalCodeComponent = component.findComponent("privatePostalCode");
		if ((privatePostalCodeComponent == null) || !(privatePostalCodeComponent instanceof UIInput))
		{
			logger
					.warn("Cannot find private postal code component from country code to validate; skipping postal code validation");
			return;
		}
		// We have to validate on submitted value, not getLocalValue or getValue
		value = ((UIInput) (privatePostalCodeComponent)).getSubmittedValue();
		logger.debug("Validating privatePostalCode '#0'", value);
		String privatePostalCodeString = (value instanceof String) ? (String) (value) : null;
		if ((privatePostalCodeString == null) || (privatePostalCodeString.trim().length() == 0))
		{
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"Please enter the first three digits of your zip code");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
			return;
		}
		try
		{
			int privatePostalCode = Integer.parseInt(privatePostalCodeString);
			if ((privatePostalCode < 0) || (privatePostalCode > 999))
			{
				FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
						"Please enter only the first three digits of your zip code");
				if (component instanceof UIInput)
					((UIInput) (component)).setValid(false);
			}
			// Set the session's postal code if it is not already set
			if (getPostalCode() == null)
				setPostalCode(Long.valueOf(privatePostalCode * 100));
		}
		catch (NumberFormatException numberFormatException)
		{
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"Please enter the first three digits of your zip code as numbers");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
		}
	}

	/**
	 * Validates the principal is entered and, if changed, is a valid email
	 * address. This method invalidates the component and adds a non-localized
	 * faces messages if the submitted value is not available or if the
	 * submitted value has changed (case-insensitive) and the trial alert
	 * service is desired yet the submitted value is not a valid email address.
	 * 
	 * @param facesContext
	 *            the faces context
	 * @param component
	 *            the input component
	 * @param value
	 *            the component's value
	 * @see org.quantumleaphealth.action.PatientInterface#validatePrincipal(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object)
	 */
	public void validatePrincipal(FacesContext facesContext, UIComponent component, Object value)
	{
		// Ensure not empty; not required for JavaServerFaces 1.2 as empty
		// values are not validated; use required=false parameter
		String principal = (value instanceof String) ? (String) (value) : null;
		if ((principal == null) || (principal.trim().length() == 0))
		{
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"Please enter an email address");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
			return;
		}
		principal = principal.trim();

		// If not changing (case-insensitive) then it must be valid
		if ((patient.getPrincipal() != null) && principal.equalsIgnoreCase(patient.getPrincipal()))
		{
			logger.debug("Principal '#0' not changing for patient ##1", principal, patient.getUniqueId());
			return;
		}
		// If principal is changing then ensure its maximum length and that it
		// is unique in the database
		if (principal.length() > LENGTH_PRINCIPAL_MAXIMUM)
		{
			logger.debug("Principal '#0' too long (#1) for patient ##2", principal, principal.length(), patient
					.getUniqueId());
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"This email address is #0 letters too long for our system. Please choose another.",
					principal.length() - LENGTH_PRINCIPAL_MAXIMUM);
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
			return;
		}

		// check to make sure a normal user does not accidentally choose a navigator login.
		try
		{
			getEm().createQuery("select distinct n from Navigator n where LOWER(n.principal)=LOWER(:principal)")
				.setParameter("principal", principal).getSingleResult();
			
			// then the principal already exists as a navigator
			logger.debug("Existing principal '#0' as navigator for patient ##1", principal, patient.getUniqueId());
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
			"This email address is already in our system. Please choose another.");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
			return;

		}
		catch (NoResultException noResultException)
		{
			// passes navigator check
		}
		catch (NonUniqueResultException nonUniqueResultException)
		{
			// More than one result; should never happen
			logger.warn("Duplicate principal '#0' for patient ##1 as navigator", principal, patient.getUniqueId());
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"This email address is already in our system. Please choose another.");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
			return;
		}
		
		try
		{
			getEm().createQuery("select u.id from UserPatient u where LOWER(u.principal)=LOWER(:principal)").setParameter(
					"principal", principal).getSingleResult();
			// Principal already in system as userPatient
			logger.debug("Existing principal '#0' for patient ##1", principal, patient.getUniqueId());
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"This email address is already in our system. Please choose another.");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
			return;
		}
		catch (NoResultException noResultException)
		{
			// Unique in database
		}
		catch (NonUniqueResultException nonUniqueResultException)
		{
			// More than one result; should never happen
			logger.warn("Duplicate principal '#0' for patient ##1", principal, patient.getUniqueId());
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"This email address is already in our system. Please choose another.");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
			return;
		}

		// Get trial alert service status from current record; override if its
		// input component is posting on the same form
		boolean alertNewTrialsEnabled = patient.isAlertNewTrialsEnabled();
		UIComponent alertNewTrialsEnabledComponent = component.findComponent("alertNewTrialsEnabled");
		// Must fetch boolean value as string with getSubmittedValue, not
		// getValue or getLocalValue
		if (alertNewTrialsEnabledComponent instanceof UIInput)
		{
			Object alertNewTrialsEnabledSubmittedValue = ((UIInput) (alertNewTrialsEnabledComponent))
					.getSubmittedValue();
			if (alertNewTrialsEnabledSubmittedValue instanceof String)
			{
				boolean alertNewTrialsEnabledSubmittedBoolean = Boolean
						.parseBoolean((String) (alertNewTrialsEnabledSubmittedValue));
				if (alertNewTrialsEnabledSubmittedBoolean != alertNewTrialsEnabled)
				{
					logger.debug("Overriding trial alert preference for ##0 from #1 to #2", patient.getUniqueId(),
							Boolean.valueOf(alertNewTrialsEnabled), Boolean
									.valueOf(alertNewTrialsEnabledSubmittedBoolean));
					alertNewTrialsEnabled = alertNewTrialsEnabledSubmittedBoolean;
				}
			}
		}
		// Invalidate new principal if it is not an email address and trial
		// alert is desired
		boolean principalInvalidEmailAddress = false;
		try
		{
			InternetAddress.parse(principal, true);
		}
		catch (AddressException addressException)
		{
			principalInvalidEmailAddress = true;
		}
		if (alertNewTrialsEnabled && principalInvalidEmailAddress)
		{
			logger.debug("Invalid email address '#0' for patient ##1 who desires trial alerts", principal, patient
					.getUniqueId());
			FacesMessages
					.instance()
					.addToControl(
							component.getClientId(facesContext),
							Severity.ERROR,
							"You must enter a valid email address in order to receive trial alerts. Please enter another email address or uncheck the trial alert box.");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
			return;
		}
		// Because the principal has now passed validation, update its
		// valid-email flag
		patient.setPrincipalInvalidEmailAddress(principalInvalidEmailAddress);
	}

	/**
	 * Validates the verification email address is entered and matches the
	 * patient's email address. This method invalidates the component and adds a
	 * non-localized Faces messages if the submitted value is not available or
	 * if the submitted value does not match the patient's email address
	 * (principal field).
	 * 
	 * @param facesContext
	 *            The faces context
	 * @param component
	 *            The input component
	 * @param value
	 *            The component's value
	 * @see org.quantumleaphealth.action.PatientInterface#validateEmailAddress(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object)
	 */
	public void validateEmailAddress(FacesContext facesContext, UIComponent component, Object value)
	{
		// Ensure not empty; not required for JavaServerFaces 1.2 as empty
		// values are not validated; use required=false parameter
		String emailAddress = (value instanceof String) ? (String) (value) : null;
		if ((emailAddress == null) || (emailAddress.trim().length() == 0))
		{
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"Please enter a verifying email address");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
			return;
		}
		emailAddress = emailAddress.trim();
		UIComponent usernameComponent = component.findComponent("username");
		
		// return if there is a problem with the username input field.  
		// there is no need to do any further validations.
		if (usernameComponent instanceof UIInput && ((UIInput) (usernameComponent)).isValid() == false)
		{
			return;
		}
		
		// If username component is not available then give up
		if (!(usernameComponent instanceof UIInput))
			return;

		if (!emailAddress.equals(((UIInput) usernameComponent).getValue()))
		{
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"The email address is not the same as the email address entered above.");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
		}
	}

	private boolean isNavigatorClientWithSharingOff(UserPatient retrievedPatient)
	{
		if (getNavigatorInterface() != null && getNavigatorInterface().getNavigatorClient() != null)
		{
			//then the navigator is trying to log in to a client with sharing off.
			return false;
		}
		
		try
		{
			Navigator_Client navigator_Client = 
				(Navigator_Client) getEm().createQuery("select n from Navigator_Client n where n.id.userpatientId = :userpatientId")
					.setParameter("userpatientId", retrievedPatient.getId()).getSingleResult();
			
			if (navigator_Client == null)
			{
				return false;
			}
			return navigator_Client.isShared() == false;
		}
		catch (NoResultException noResultException)
		{
			// This is fine.  It tells us that this is a normal user patient.
			return false;
		}
		catch (NonUniqueResultException nonUniqueResultException)
		{
			// Step 4b: Validate single record
			logger.error("Patient '#0' found in multiple records", nonUniqueResultException, retrievedPatient.getPrincipal());
			FacesMessages
					.instance()
					.add(Severity.ERROR,
							"Your record is not available. A message has been sent to the system administrator to fix the problem.");
			return true;
		}
	}
	
	/**
	 * Validates the credentials are entered and are equal to the other patient
	 * credentials. This method invalidates the component and adds a
	 * non-localized faces messages if the submitted value is not available or
	 * if the submitted value is not equal (case-sensitive) to the other patient
	 * credentials component's submitted value.
	 * 
	 * @param facesContext
	 *            the faces context
	 * @param component
	 *            the input component
	 * @param value
	 *            the component's value
	 * @see org.quantumleaphealth.action.PatientInterface#validateCredentials(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object)
	 */
	public void validateCredentials(FacesContext facesContext, UIComponent component, Object value)
	{
		// Ensure not empty; not required for JavaServerFaces 1.2 as empty
		// values are not validated; use required=false parameter
		String postedCredentials = (value instanceof String) ? (String) (value) : null;
		if ((postedCredentials == null) || (postedCredentials.trim().length() == 0))
		{
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"Please enter a password");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
			return;
		}
		postedCredentials = postedCredentials.trim();

		// If not equal (case-sensitive) then invalidate; must use value or
		// local value, not submitted value
		UIComponent passwordComponent = component.findComponent("password");
		// If password component is not available then give up
		if (!(passwordComponent instanceof UIInput))
			return;
		if (!postedCredentials.equals(((UIInput) (passwordComponent)).getValue()))
		{
			logger.debug("Invalid duplicate credentials for ##0", patient.getUniqueId());
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"The second password entered is not the same as the first. Please try again");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
			return;
		}
	}

	/**
	 * Returns whether the patient answered the category question(s). If the
	 * second <code>category</code> choice is made then a <code>primary</code>
	 * choice must be made. Algorithm:
	 * <ol>
	 * <li><code>category</code> must not be zero, and</li>
	 * <li><code>category</code> must not be 4 or <code>primary</code> must not
	 * be zero</li>
	 * </ol>
	 * This method adds a faces message to the context's <code>category</code>
	 * component if not valid.
	 * 
	 * @return <code>true</code> if the category question(s) were answered
	 * @see org.quantumleaphealth.action.PatientInterface#isCategoryValid()
	 */
	public boolean isCategoryValid()
	{
		boolean categoryValid = getFacadePatientHistory().isCategoryValid();
		if (!categoryValid)
		{
			FacesContext facesContext = FacesContext.getCurrentInstance();
			boolean found = false;
			Iterator<FacesMessage> iterator = facesContext.getMessages();
			while (iterator.hasNext())
				if (CATEGORY_INVALID.getSummary().equals(iterator.next().getSummary()))
				{
					found = true;
					break;
				}
			if (!found)
				facesContext.addMessage(null, CATEGORY_INVALID);
		}
		return categoryValid;
	}
	
	private boolean isNavigator()
	{
		if (patient == null)
		{
			return false;
		}

		try
		{
			getEm().createQuery("select distinct n from Navigator n where LOWER(n.principal)=LOWER(:principal)")
				.setParameter("principal", patient.getPrincipal()).getSingleResult();
		}
		catch (NoResultException noResultException)
		{
			return false;
		}
		catch (NonUniqueResultException nonUniqueResultException)
		{
			logger.error("Non-unique result found in navigator table for principal " + patient.getPrincipal() + ", attempting login as normal userpatient.");
			return false;
		}
		return true;
	}
	
	/**
	 * Authenticates and loads a user whose principal and credentials are stored
	 * in its <code>patient</code> property or loads the patient's secret
	 * question if credentials are not provided and updates its
	 * <code>unsuccessfulAuthenticationCount</code>. This method transforms the
	 * <code>patient</code> property according to the following flow:
	 * <ol>
	 * <li>If the patient is already logged in then exit.</li>
	 * <li>If the patient's principal is unavailable then add a message and
	 * exit.</li>
	 * <li>Load the patient record along with its associated screened trials and
	 * invitations that matches (case-insensitive) the property's principal.</li>
	 * <li>If the record is not found or there is more than one then add a
	 * message and exit.</li>
	 * <li>Set the property's consecutive unsuccessful authentication attempts
	 * to that of the retrieved record. If the number exceeds a threshold then
	 * exit.</li>
	 * <li>If the credentials have been provided then compare to the retrieved
	 * credentials. If there is a match (case-sensitive) then store the
	 * retrieved record into the property and exit. If there is not a match then
	 * add a message, increment the unsuccessful-authentication-count and exit.</li>
	 * <li>If the user cannot receive an email message then update its
	 * <code>principalInvalidEmailAddress</code> property and exit.</li>
	 * <li>Set the property's secret question to the retrieved secret question.
	 * If the secret answer has not been provided then exit.</li>
	 * <li>Compare the secret answer to the retrieved secret answer. If there is
	 * not a match (case-insensitive) then add a message, increment the
	 * unsuccessful-authentication-count and exit. If there is a match then
	 * update the record's credentials with a new random string, reset the
	 * unsuccessful-authentication-count, send a message to the patient with the
	 * new credentials and logout the user. The random string consists of
	 * <code>LENGTH_RANDOMCREDENTIALS</code> upper-case Roman characters and/or
	 * digits.</li>
	 * </ol>
	 * Note that this class does not employ the JBoss seam <code>identity</code>
	 * component because it does not provide extra functionality and its Faces
	 * messages are awkward to handle.
	 * 
	 * @see org.quantumleaphealth.action.PatientInterface#login()
	 */
	public void login()
	{
//		if (this.getNavigatorClientId() != null) used for the find as well.
		if (getNavigatorInterface() != null && getNavigatorInterface().getNavigatorClient() != null)
		{
			UserPatient up = getEm().find(UserPatient.class, getNavigatorInterface().getNavigatorClient().getId().getUserpatientId());

			this.getPatient().setPrincipal(up.getPrincipal());
			this.getPatient().setCredentials(up.getCredentials());
		}
		else if (isNavigator() && getNavigatorInterface().getNavigator() != null)
		{
			getNavigatorInterface().getNavigator().setPrincipal(patient.getPrincipal());
			getNavigatorInterface().getNavigator().setCredentials(patient.getCredentials());
			getNavigatorInterface().login();
			return;
		}
		
		// Step 1: Validate logged in
		if (isLoggedIn())
		{
			return;
		}	

		// Step 2: Validate principal (redundant: should be checked by JSF
		// required attribute)
		String principal = (patient == null) ? null : patient.getPrincipal();
		if ((principal == null) || (principal.length() == 0))
		{
			logger.debug("Blank patient principal");
			FacesMessages.instance().addToControl("username", Severity.ERROR,
					"Please enter your email address and try again");
			return;
		}

		UserPatient retrievedPatient;
		try
		{
			// Step 3: Load record; fetch screened-trial association eagerly;
			// invitation association will be fetched by
			// updateScreenedTrialsAndInvitations()
			// WARNING: Do NOT eagerly fetch both associations in same query as
			// Hibernate returns duplicate screened-trial objects
			// when duplicate (erroneous) invitations exist (e.g., same
			// mechanism as the multiple-bag fetch problem)
			retrievedPatient = (UserPatient) (getEm()
					.createQuery(
							"select distinct u from UserPatient u left join fetch u.screenedTrials st where LOWER(u.principal)=LOWER(:principal)")
					.setParameter("principal", principal).getSingleResult());
			if (Locale.ENGLISH.getLanguage().equals(retrievedPatient.getPreferredLanguage()))
				this.locale = Locale.ENGLISH;
		}
		catch (NoResultException noResultException)
		{
			// Step 4a: Validate single record
			logger.debug("Patient '#0' not found", principal);
			FacesMessages
					.instance()
					.add(
							Severity.ERROR,
							(patient.getCredentials() == null) ? "The email address you entered was not found. Please try again"
									: ERRORMESSAGE_UNAUTHENTICATED);
			return;
		}
		catch (NonUniqueResultException nonUniqueResultException)
		{
			// Step 4b: Validate single record
			logger.error("Patient '#0' found in multiple records", nonUniqueResultException, principal);
			FacesMessages
					.instance()
					.add(Severity.ERROR,
							"Your record is not available. A message has been sent to the system administrator to fix the problem.");
			return;
		}

		// Step 5: Cache the number of consecutive unsuccessful login attempts
		// for navigation purposes and validate
		patient.setUnsuccessfulAuthenticationCount(retrievedPatient.getUnsuccessfulAuthenticationCount());
		if (retrievedPatient.getUnsuccessfulAuthenticationCount() >= MAXIMUM_UNSUCCESSFULAUTHENTICATIONCOUNT)
		{
			logger.debug("Patient '#0' exceeded authentication count", principal);
			FacesMessages
					.instance()
					.add(
							Severity.ERROR,
							"This account has been deactivated because of too many unsuccessful login attempts. Please contact BCT to reset your password.");
			return;
		}

		// Step 6: Compare available credentials case-insensitive
		if (patient.getCredentials() != null)
		{
			if ((retrievedPatient.getCredentials() == null)
					|| !patient.getCredentials().equalsIgnoreCase(retrievedPatient.getCredentials()))
			{
				// Failed: Update the unsuccessful count (both persisted and
				// cached)
				retrievedPatient.setUnsuccessfulAuthenticationCount(retrievedPatient
						.getUnsuccessfulAuthenticationCount() + 1);
				patient.setUnsuccessfulAuthenticationCount(retrievedPatient.getUnsuccessfulAuthenticationCount());
				FacesMessages.instance().add(Severity.ERROR, ERRORMESSAGE_UNAUTHENTICATED);
				logger.debug("Bad patient credentials '#1' for '#0' attempt #2", principal, patient.getCredentials(),
						retrievedPatient.getUnsuccessfulAuthenticationCount());
				return;
			}

			// Passed: reset the unauthenticated count, load the returning user
			// and update matches
			if (retrievedPatient.getUnsuccessfulAuthenticationCount() != 0)
				retrievedPatient.setUnsuccessfulAuthenticationCount(0);
			
			// Check to see if this is a userpatient managed by a navigator.  If so, then sharing for this must be turned on.
			// only check if the navigaotr pid is null, indicating that we logged in using the normal login page.
			if (isNavigatorClientWithSharingOff(retrievedPatient))
			{
				FacesMessages
				.instance()
				.add(Severity.ERROR,
						"This record is being managed by a navigator and is not available. A message has been sent to the system administrator to fix the problem.");
				return;
			}
			
			setPatient(retrievedPatient);
			// updateScreenedTrialsAndInvitations() fetches both the
			// screenedTrials and invitations associations since the patient is
			// now attached
			// AVATAR: avatar profiles are always considered complete.
			getPatientAssociationManager().updateScreenedTrialsAndInvitations(patient, trials, null, patient.isAvatarProfile() ? true: getFacadePatientHistory().isComplete(), true);
			// Flush the entity manager so SQL/debug logging is in chronological
			// order
			getEm().flush();
			logger.info("Authenticated and loaded '#0' as ##1, language '#2'", patient.getPrincipal(), patient.getId(),
					patient.getPreferredLanguage());
			returningUser = true;
			// Set five-digit postal code from three-digit private postal code
			if ((patient.getPrivatePostalCode() != null) && (patient.getPrivatePostalCode().longValue() > 0l))
				setPostalCode(Long.valueOf(100l * patient.getPrivatePostalCode().longValue()));
			// Set preferred language
			if (SPANISH.getLanguage().equals(patient.getPreferredLanguage()))
			{
				this.locale = SPANISH;
				LocaleSelector.instance().setLocale(SPANISH);
			}
			else
			{
				this.locale = Locale.ENGLISH;
				LocaleSelector.instance().setLocale(Locale.ENGLISH);
			}
			LocaleSelector.instance().select();
			
			// may go to the my_trials page if the fw web parameter has been defined.
			this.goToTrial();
			
			return;
		}

		// Step 7: If we cannot read the secret question/answer nor send an
		// email then we cannot authenticate using the secret question/answer
		// Version 1.3.07: We no longer check the secret question before sending
		// out the email for resetting the password.
		// if ((retrievedPatient.getSecretQuestion() == null)
		// || (retrievedPatient.getSecretAnswer() == null)) {
		// // Clear the secret question so navigation knows that it cannot send
		// // an email
		// patient.setSecretQuestion(null);
		// return;
		// }

		InternetAddress internetAddress = null;
		if (!retrievedPatient.isPrincipalInvalidEmailAddress())
			try
			{
				internetAddress = new InternetAddress(retrievedPatient.getPrincipal(), true);
			}
			catch (AddressException addressException)
			{
				retrievedPatient.setPrincipalInvalidEmailAddress(true);
				logger.debug("Patient '#0' cannot receive email with reset password: #1", retrievedPatient
						.getPrincipal(), addressException);
			}
		// Cache the valid status for navigation
		patient.setPrincipalInvalidEmailAddress(retrievedPatient.isPrincipalInvalidEmailAddress());
		if (internetAddress == null)
			return;

		// Step 8: Cache the secret question and display it to the user if
		// answer has not been provided
		// Version 1.3.07: We no longer check the secret question before sending
		// out the email for resetting the password.
		// patient.setSecretQuestion(retrievedPatient.getSecretQuestion());
		// if (patient.getSecretAnswer() == null)
		// return;

		// Version 1.3.07: We no longer check the secret question before sending
		// out the email for resetting the password.
		// Step 9: Authenticate the secret answer and store the result in the
		// unsuccessful count
		// if (!patient.getSecretAnswer().equalsIgnoreCase(
		// retrievedPatient.getSecretAnswer())) {
		// // Failed: Update the unsuccessful count (both persisted and cached)
		// retrievedPatient.setUnsuccessfulAuthenticationCount(retrievedPatient.getUnsuccessfulAuthenticationCount()
		// + 1);
		// patient.setUnsuccessfulAuthenticationCount(retrievedPatient.getUnsuccessfulAuthenticationCount());
		// FacesMessages.instance().add(Severity.ERROR,
		// "The secret answer you entered is not correct. Please try again");
		// logger.debug("Bad patient secret answer '#1' for '#0' attempt #2",
		// principal, patient.getSecretAnswer(),
		// retrievedPatient.getUnsuccessfulAuthenticationCount());
		// return;
		// }

		// Passed: reset and send credentials, reset unsuccessful authentication
		// count, logout session
		char[] random_characters = new char[LENGTH_RANDOMCREDENTIALS];
		// The new credentials uses upper-case Roman characters and digits for
		// readability
		for (int index = 0; index < LENGTH_RANDOMCREDENTIALS; index++)
			random_characters[index] = Character.toUpperCase(Character.forDigit(RANDOM.nextInt(Character.MAX_RADIX),
					Character.MAX_RADIX));
		String newCredentials = new String(random_characters);
		try
		{
			// Update credentials and persisted count
			retrievedPatient.setCredentials(newCredentials);
			if (retrievedPatient.getUnsuccessfulAuthenticationCount() != 0)
			{
				retrievedPatient.setUnsuccessfulAuthenticationCount(0);
			}
			getMessageSender().send(internetAddress, null, "Your BCT password has been reset",
					"Your password on BreastCancerTrials.org has been changed to: " + newCredentials);
			// Update cached count so navigation knows that password reset and
			// sending was successful
			patient.setUnsuccessfulAuthenticationCount(0);
			logger.debug("Reset patient credentials and sent message to '#0'", principal);
		}
		catch (Throwable throwable)
		{
			// If either storage or sending did not work then ensure count
			// represents failure and notify user
			if (patient.getUnsuccessfulAuthenticationCount() <= 0)
				patient.setUnsuccessfulAuthenticationCount(1);
			logger.error("Cannot reset patient ##0's credentials", throwable, retrievedPatient.getId());
			FacesMessages.instance().add(Severity.ERROR,
					"The system could not reset your password. Please contact BCT.");
		}
		// Logout session
		logout();
	}

	/**
	 * Prepare a forwarding URL if the fw query string parameter is defined and if it starts with the URI /bct_nation/mytrials
	 * 
	 * @param request
	 * @return formatted URL
	 */
	private String getForwardingHostName(HttpServletRequest request)
	{
		if (request == null)
		{
			return null;
		}

		if (request.isSecure())
		{
			return "https://" + request.getServerName();
		}
		
		// otherwise the server responds to http requests
		if (request.getServerPort() != 0 && request.getServerPort() != 80)
		{
			// non standard port
			return "http://" + request.getServerName() + ":" + request.getServerPort();
		}

		// standard port 80 
		return "http://" + request.getServerName();
	}
	
	/**
	 * @return whether the patient is represented in the database
	 * @see org.quantumleaphealth.action.PatientInterface#isLoggedIn()
	 */
	@BypassInterceptors
	public boolean isLoggedIn()
	{
		return (patient != null) && (patient.getId() != null);
	}

	/**
	 * Switch the locale to Spanish
	 */
	public void switchToSpanish()
	{
		LocaleSelector.instance().setLocale(SPANISH);
		LocaleSelector.instance().select();
		this.locale = SPANISH;
		if (this.isLoggedIn())
		{
			patient.setPreferredLanguage(this.locale.getLanguage());
			updateUserLanguagePreference();
		}	
	}

	/**
	 * update the database with the current language preference.  We don't want to merge the whole
	 * userpatient object, but we do need to persist the language choice whenever it changes.
	 */
	private void updateUserLanguagePreference()
	{
		Query query = getEm().createQuery("update UserPatient u set preferredLanguage = :preferredlanguage where u.id=:id")
				.setParameter("preferredlanguage", patient.getPreferredLanguage()).setParameter("id", patient.getId());
		query.executeUpdate();
		logger.info("Updated Patient #0 language preference to #1", patient.getPrincipal(), patient.getPreferredLanguage());
	}
	
	/**
	 * Revert the locale back to English
	 */
	public void revertToEnglish()
	{
		LocaleSelector.instance().setLocale(Locale.ENGLISH);
		LocaleSelector.instance().select();
		this.locale = Locale.ENGLISH;
		if (this.isLoggedIn())
		{
			patient.setPreferredLanguage(this.locale.getLanguage());
			updateUserLanguagePreference();
		}	
	}

	/**
	 * Mark this trial as not interested by the patient. Updates both the trial
	 * list in memory as well as persisting the ScreenedTrial table.
	 * 
	 * @param trialId
	 */
	public String notInterested(Long trialId)
	{
		logger.info("Patient is not interested in trial ##0", trialId);
		this.updateScreenedTrialNotInterestedIndicator(trialId, true);
		return "success";
	}

	/**
	 * Mark this trial as now once again interested by the patient.
	 * 
	 * @param trialId
	 */
	public String nowInterested(Long trialId)
	{
		logger.info("Patient is once again interested in trial ##0", trialId);
		this.updateScreenedTrialNotInterestedIndicator(trialId, false);
		return "success";
	}

	/**
	 * Mark this trial as not interested by the patient. Updates both the trial
	 * list in memory as well as persisting the ScreenedTrial table.
	 * 
	 * @param trialId
	 */
	public String addToFavorites(Long trialId)
	{
		logger.info("Patient added trial ##0 to favorites list.", trialId);
		this.updateScreenedTrialFavoriteIndicator(trialId, true);
		return "success";
	}

	/**
	 * Mark this trial as now once again interested by the patient.
	 * 
	 * @param trialId
	 */
	public String removeFromFavorites(Long trialId)
	{
		logger.info("Patient removed trial ##0 from favorites list.", trialId);
		this.updateScreenedTrialFavoriteIndicator(trialId, false);
		return "success";
	}

	/**
	 * @return the avatarHistoryConfiguration
	 */
	public AvatarHistoryConfiguration getAvatarHistoryConfiguration() 
	{
		return avatarHistoryConfiguration;
	}

	private boolean loadAvatarProfile(Long avatarId)
	{
		UserPatient avatar = getEm().find(UserPatient.class, avatarId );
		if ( avatar != null )
		{
			this.patient.setPatientHistory( avatar.getPatientHistory() );
			this.getFacadePatientHistory().setPatientHistory( patient.getPatientHistory(), true );
			return true;
		}
		return false;
	}
	
	public boolean isAvatarProfile()
	{
		return getPatient().isAvatarProfile();
	}
	
	
	/**
	 * @param avatarId the avatarId to set
	 */
	public void setAvatarId(Long avatarId)
	{
		if ( loadAvatarProfile( avatarId ) )
		{
			this.avatarId = avatarId;
		}
	}

	/**
	 * @return the avatarId
	 */
	public Long getAvatarId()
	{
		return avatarId;
	}

//	/**
//	 * @param navigatorClientId the navigatorClientId to set
//	 */
//	public void setNavigatorClientId(Long navigatorClientId) {
//		this.navigatorClientId = navigatorClientId;
//	}
//
//	/**
//	 * @return the navigatorClientId
//	 */
//	public Long getNavigatorClientId() {
//		return navigatorClientId;
//	}

	public void setPatientType(String patientType) 
	{
		this.patientType = patientType;

		if ( historyCategoryBypass.containsKey(patientType))
		{	
			this.getHistory().setCategory(historyCategoryBypass.get(patientType));
			if (historyCategoryPrimaryBypass.containsKey(patientType))
			{
				this.getHistory().setPrimary(historyCategoryPrimaryBypass.get(patientType));
			}
		}	
	}

	public String getPatientType() 
	{
		return patientType;
	}

	/**
	 * @param avatarHistoryConfiguration the avatarHistoryConfiguration to set
	 */
	public void setAvatarHistoryConfiguration(AvatarHistoryConfiguration avatarHistoryConfiguration) 
	{
		this.avatarHistoryConfiguration = avatarHistoryConfiguration;
	}

	/**
	 * Mark the notInterested indicator reference by the passed in trial id.
	 * Updates both the trial list in memory as well as persisting the
	 * ScreenedTrial table.
	 * 
	 * @param trialId
	 *            The trial id to update for the patient.
	 * @param notInterested
	 *            Indicator if the patient is Not Interested.
	 */
	@SuppressWarnings("unchecked")
	private void updateScreenedTrialNotInterestedIndicator(Long trialId, boolean notInterested)
	{
		merge();
		for (ScreenedTrial screenedTrial : patient.getScreenedTrials())
		{
			if (screenedTrial.getTrialId().equals(trialId))
			{
				screenedTrial.setNotInterested(notInterested);
				getEm().merge(screenedTrial);
				break;
			}
		}
		TrialProximity trialProximity = null;
		PatientTrial patientTrial = null;
		for (TrialProximityGroup trialGroup : this.getTrials().getSortedList())
		{
			for (Iterator trialIterator = trialGroup.iterator(); trialIterator.hasNext();)
			{
				trialProximity = (TrialProximity) trialIterator.next();
				patientTrial = (PatientTrial) trialProximity.getTrial();
				if (patientTrial.getId().equals(trialId))
				{
					if (notInterested)
						patientTrial.setNotInterested(Boolean.TRUE);
					else
						patientTrial.setNotInterested(Boolean.FALSE);
					break;
				}
			}
		}
		// Because the invitations association has a transient field, call
		// updateInvitations().
		getPatientAssociationManager().updateInvitations(patient);
	}

	/**
	 * Mark the notInterested indicator reference by the passed in trial id.
	 * Updates both the trial list in memory as well as persisting the
	 * ScreenedTrial table.
	 * 
	 * @param trialId
	 *            The trial id to update for the patient.
	 * @param notInterested
	 *            Indicator if the patient is Not Interested.
	 */
	// @SuppressWarnings("unchecked")
	private void updateScreenedTrialFavoriteIndicator(Long trialId, boolean isFavorite)
	{
		merge();
		for (ScreenedTrial screenedTrial : patient.getScreenedTrials())
		{
			if (screenedTrial.getTrialId().equals(trialId))
			{
				screenedTrial.setFavorite(isFavorite);
				getEm().merge(screenedTrial);
				break;
			}
		}
		TrialProximity trialProximity = null;
		PatientTrial patientTrial = null;
		for (TrialProximityGroup trialGroup : this.getTrials().getSortedList())
		{
			boolean hasFavorites = false;
			for (Iterator<TrialProximity> trialIterator = trialGroup.iterator(); trialIterator.hasNext();)
			{
				trialProximity = (TrialProximity) trialIterator.next();
				patientTrial = (PatientTrial) trialProximity.getTrial();
				if (patientTrial.getId().equals(trialId))
				{
					if (isFavorite)
					{
						patientTrial.setFavorite(Boolean.TRUE);
					}	
					else
					{	
						patientTrial.setFavorite(Boolean.FALSE);
					}	
				}
				
				if (patientTrial.getFavorite())  // set indicator that this trialgroup has favorites
				{
					hasFavorites = true;
				}
			}
			trialGroup.setHasFavorites(hasFavorites);
		}
		// Because the invitations association has a transient field, call
		// updateInvitations().
		getPatientAssociationManager().updateInvitations(patient);
	}

	/**
	 * Persist a new patient user. This method does nothing if the user is
	 * already persisted or the principal and credentials are not available.
	 * 
	 * @see org.quantumleaphealth.action.PatientInterface#persist()
	 */
	public void persist()
	{
		// If already persisted or username or password is empty then do nothing
		if (isLoggedIn())
		{
			logger.warn("Already persisted ##0", patient.getId());
			return;
		}
		if ((patient.getPrincipal() == null) || (patient.getCredentials() == null))
		{
			logger.warn("No authentication to persist");
			return;
		}

		this.generateAnalyticTrackingCode();

		// Seam logs and swallows any persistence exceptions so don't bother
		// catching them
		getEm().persist(patient);
		logger.debug("Persisted '#0' into ##1", patient.getPrincipal(), patient.getId());
	}

	/**
	 * Deletes a user and logs out. It will invalidate this session object.
	 * 
	 * @see org.quantumleaphealth.action.PatientInterface#delete()
	 */
	public void delete()
	{
		// If the user has not been persisted then ignore this step
		if (isLoggedIn())
		{
			logger.debug("Deleting patient ##0", patient.getId());
			// Because we are deleting the patient and its associations, we do
			// not need to re-attach invitations' transient field
			merge();
			getEm().remove(patient);
			getEm().flush();
			logger.debug("Patient ##0 deleted", patient.getId());
		}
		logout();
	}

	/**
	 * Removes the patient's authentication and invitations and logs out.
	 * 
	 * @see org.quantumleaphealth.action.PatientInterface#donate()
	 */
	public void donate()
	{
		// If the user has not been persisted then return
		if (!isLoggedIn())
			return;
		logger.debug("Donating patient ##0", patient.getId());
		// Attach patient to persistence engine
		// Because we are deleting the patient and its associations, we do not
		// need to re-attach invitations' transient field
		merge();
		patient.setPrincipal(null);
		patient.setCredentials(null);
		patient.setSecretQuestion(null);
		patient.setSecretAnswer(null);
		patient.setAlertNewTrialsEnabled(false);
		patient.setPrincipalInvalidEmailAddress(true);
		// Remove invitations but keep screenedTrials
		Iterator<Invitation> invitations = patient.getInvitations().iterator();
		while (invitations.hasNext())
		{
			Invitation invitation = invitations.next();
			invitations.remove();
			if (invitation.getId() != null)
				getEm().remove(invitation);
		}
		getEm().flush();
		logger.debug("Patient donated");
		logout();
	}

	/**
	 * Resets the patient to a new patient and invalidates the session.
	 * 
	 * @see org.quantumleaphealth.action.PatientInterface#logout()
	 */
	public void logout()
	{
		setPatient(null);
		// Reset other instance variables
		credentials = null;
		returningUser = false;
		trials.clear();
		trials.setGroupedByProximityBoolean(false);
		trialId = null;
		sortedTrialSites = null;
		eligibilityProfiles = null;
		
		if (this.getNavigatorInterface() == null || !this.getNavigatorInterface().isLoggedIn())
		{	
			Session.instance().invalidate();
		}	
	}

	/**
	 * Persists changes to an existing user's information that is not the health
	 * history. This method also reattaches the transient field within each
	 * element of the <code>invitations</code> association, since the
	 * <code>screen()</code> method is not called after the patient object is
	 * reattached to the persistence context.
	 * 
	 * @see org.quantumleaphealth.action.PatientInterface#update()
	 */
	public void update()
	{
		merge();
		this.generateAnalyticTrackingCode();
		// Reattach transient fields in associations (just invitations)
		getPatientAssociationManager().updateInvitations(patient);
		Long postalCode = patient.getPrivatePostalCode();
		if (postalCode == null) {
			postalCode = 0L;
		}
		int postalCodeInt = postalCode.intValue() * 100;
		
		trials.setPostalCodeGeocoding(postalCodeInt, getPostalCodeManager().getPostalCodeGeocoding(postalCodeInt)
				.getGeocoding());
	}

	/**
	 * Persists an existing user's information and stores the
	 * persistence-context-attached version into an instance variable so further
	 * changes made by the calling method will also be persisted. This method
	 * does nothing if the user is new (e.g., has not been persisted already).
	 * <strong> Because the <code>invitations</code> association has a transient
	 * field, it is the caller's responsibility to call
	 * <code>updateInvitations()</code> after calling this method if invitations
	 * will be subsequently accessed. If the transient field is not attached
	 * then the <code>invitation</code> objects will be unusable. Note that
	 * <code>screen()</code> calls <code>updateInvitations()</code>. </strong>
	 * 
	 * @see #setPatient(UserPatient)
	 */
	private void merge()
	{
		// If the user is not existing then do nothing
		if (!isLoggedIn())
			return;

		setPatient(getEm().merge(patient));
		getEm().flush();
		logger.debug("Merged patient ##0", patient.getId());
	}

	/**
	 * Do nothing when stateful session bean is destroyed.
	 * 
	 * @see org.quantumleaphealth.action.PatientInterface#destroy()
	 */
	@Remove
	@Destroy
	public void destroy()
	{
	}

	/**
	 * Returns the value of the <code>patient</code> and <code>history</code>
	 * fields
	 * 
	 * @return the value of the <code>patient</code> and <code>history</code>
	 *         fields
	 */
	@Override
	@BypassInterceptors
	public String toString()
	{
		return "PatientInterfaceAction{patient=" + patient + ", history=" + patient.getPatientHistory() + '}';
	}

	/**
	 * @param forwardingURL the forwardingURL to set
	 */
	public void setForwardingURL(String forwardingURL) {
		this.forwardingURL = forwardingURL;
	}

	/**
	 * @return the forwardingURL
	 */
	public String getForwardingURL() 
	{
		return StringEscapeUtils.unescapeHtml(forwardingURL);
	}

	/**
	 * Bypass pages.xml navigation rules if a redirect URL is defined.
	 */
	public void goToTrial()
	{
		if (getForwardingURL() != null && getForwardingURL().toLowerCase().startsWith("/bct_nation/my_trials"))
		{
			HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
			String externalURL = this.getForwardingHostName(request) + getForwardingURL();
			FacesManager.instance().redirectToExternalURL(externalURL);
		}
	}
	
	/**
	 * 1/9/2014:  jboss is having problems passivating one of three classes put into production since November.
	 * The exception trace doesn't say which one.
	 */
	@PrePassivate
	private void prePassivate() 
	{
		logger.info("In PatientInterfaceAction prePassivate.");
	}

	/*****
	 * Remove all this after I'm done with passivation testing.
	 */
	/**
	 * @param em the em to set
	 */
	public void setEm(EntityManager em) {
		this.em = em;
	}

	/**
	 * @return the em
	 */
	public EntityManager getEm() {
		return em;
	}

	/**
	 * @param engine the engine to set
	 */
	public void setEngine(MatchingEngine engine) {
		this.engine = engine;
	}

	/**
	 * @return the engine
	 */
	public MatchingEngine getEngine() {
		return engine;
	}

	/**
	 * @param patientAssociationManager the patientAssociationManager to set
	 */
	public void setPatientAssociationManager(PatientAssociationManager patientAssociationManager) {
		this.patientAssociationManager = patientAssociationManager;
	}

	/**
	 * @return the patientAssociationManager
	 */
	public PatientAssociationManager getPatientAssociationManager() {
		return patientAssociationManager;
	}

	/**
	 * @param postalCodeManager the postalCodeManager to set
	 */
	public void setPostalCodeManager(PostalCodeManager postalCodeManager) {
		this.postalCodeManager = postalCodeManager;
	}

	/**
	 * @return the postalCodeManager
	 */
	public PostalCodeManager getPostalCodeManager() {
		return postalCodeManager;
	}

	/**
	 * @param messageSender the messageSender to set
	 */
	public void setMessageSender(MessageSender messageSender) {
		this.messageSender = messageSender;
	}

	/**
	 * @return the messageSender
	 */
	public MessageSender getMessageSender() {
		return messageSender;
	}

	/**
	 * @return the facadePatientHistory
	 */
	public FacadePatientHistory getFacadePatientHistory() {
		return facadePatientHistory;
	}

	/**
	 * @param navigatorInterface the navigatorInterface to set
	 */
	public void setNavigatorInterface(NavigatorInterface navigatorInterface) {
		this.navigatorInterface = navigatorInterface;
	}

	/**
	 * @return the navigatorInterface
	 */
	public NavigatorInterface getNavigatorInterface() {
		return navigatorInterface;
	}
}
