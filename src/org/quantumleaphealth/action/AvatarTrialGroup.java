package org.quantumleaphealth.action;

import java.util.ArrayList;

/**
 * Class for use with the AvatarProfileInterfaceAction avatar trial list.
 * @author wgweis
 */
public class AvatarTrialGroup
{
	private String label;
	private ArrayList<TrialProximity> trials;
	
	/**
	 * Empty constructor for jboss serialization.
	 */
	AvatarTrialGroup() {}
	
	AvatarTrialGroup( String label )
	{
		this.label = label;
		trials = new ArrayList<TrialProximity>();
	}

	/**
	 * @return the label
	 */
	public String getLabel() 
	{
		return label;
	}

	/**
	 * @return the trials
	 */
	public ArrayList<TrialProximity> getTrials() 
	{
		return trials;
	}
}
