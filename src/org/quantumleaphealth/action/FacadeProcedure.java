/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;
import java.util.Collection;

import org.quantumleaphealth.model.patient.Procedure;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Represents a facade into a patient's procedure history. This serializable
 * JavaBean exposes <code>performed</code> and <code>current</code> fields to
 * the GUI, keeps the list of performed procedures up to date and provides
 * localized labels.
 * 
 * @author Tom Bechtold
 * @version 2008-08-12
 */
public class FacadeProcedure implements Serializable
{
	/**
	 * The kind of procedure
	 */
	private final CharacteristicCode kind;
	/**
	 * The location for this procedure
	 */
	private final CharacteristicCode location;
	/**
	 * The type of procedure performed or <code>null</code> for not specified
	 */
	private final CharacteristicCode type;
	/**
	 * The identifier for this procedure
	 */
	private final String id;
	/**
	 * If <code>true</code> then the <code>current</code> and
	 * <code>completed</code> fields should not be prompted.
	 */
	private final boolean brief;

	/**
	 * Represents the started date, guaranteed to be non-<code>null</code>
	 */
	private final FacadeYearMonth started;
	/**
	 * Represents the completed date, guaranteed to be non-<code>null</code>
	 */
	private final FacadeYearMonth completed;

	/**
	 * The underlying property that this facade represents, guaranteed to be
	 * non-<code>null</code>
	 */
	private Procedure backing;
	/**
	 * Holds all performed procedures, guaranteed to be non-<code>null</code>.
	 * This collection will be updated with the <code>backing</code> whenever
	 * <code>performed</code> changes status
	 */
	private Collection<Procedure> list;

	/**
	 * Whether or not the procedure was or is currently performed
	 */
	private boolean performed;
	/**
	 * Whether or not the procedure is currently performed or <code>0</code> if
	 * not answered yet / not applicable. The choice is stored in the following
	 * bit location:
	 * <ol>
	 * <li>No
	 * <li>Yes
	 * </ol>
	 */
	private byte current;

	/**
	 * Instantiate a facade with the specified underlying property.
	 * 
	 * @param list
	 *            holds all performed procedures
	 * @param kind
	 *            the kind of procedure
	 * @param location
	 *            where the procedure was performed
	 * @param type
	 *            the type of procedure performed or <code>null</code> for not
	 *            specified
	 * @param id
	 *            the facade's unique identifier
	 * @param brief
	 *            <code>true</code> if the <code>current</code> and
	 *            <code>completed</code> fields should not be prompted
	 * @throws IllegalArgumentException
	 *             if any parameter is <code>null</code>
	 */
	public FacadeProcedure(Collection<Procedure> list, CharacteristicCode kind, CharacteristicCode location,
			CharacteristicCode type, String id, boolean brief)
	{
		// Verify and store final instance variables
		if ((kind == null) || (location == null) || (id == null))
			throw new IllegalArgumentException("kind, location or id property not specified");
		this.kind = kind;
		this.location = location;
		this.type = type;
		this.id = id;
		this.brief = brief;
		// Create the date facades
		started = new FacadeYearMonth();
		completed = new FacadeYearMonth();
		// Store the list and update the backing property and date facades
		setProcedures(list);
	}

	/**
	 * Updates a facade with an underlying property from a list. This method
	 * updates the <code>list</code> and <code>backing</code> instance variables
	 * and the dates in the <code>started</code> and <code>completed</code>
	 * facades.
	 * 
	 * @param list
	 *            holds all performed procedures
	 * @throws IllegalArgumentException
	 *             if <code>list</code> is <code>null</code>
	 */
	void setProcedures(Collection<Procedure> list)
	{
		// Store the list and update the backing property
		if (list == null)
			throw new IllegalArgumentException("list property not specified");
		this.list = list;
		// Find the backing property in the list that matches the kind, type and
		// location
		backing = null;
		for (Procedure current : list)
			if (CharacteristicCode.equals(current.getKind(), kind)
					&& CharacteristicCode.equals(current.getType(), type)
					&& CharacteristicCode.equals(current.getLocation(), location))
			{
				backing = current;
				break;
			}
		if (backing == null)
		{
			// If not exists in the list then create a blank, unperformed one
			backing = new Procedure();
			backing.setKind(kind);
			backing.setType(type);
			backing.setLocation(location);
			performed = false;
			current = 0;
		}
		else
		{
			performed = true;
			// The current field's answer is No if there is a completed date,
			// Yes otherwise
			current = (backing.getCompleted().getYear() != null) && (backing.getCompleted().getYear().shortValue() > 0) ? (byte) (1 << 1)
					: (byte) (1 << 2);
		}
		// Update the date facades with the dates from the new backing object
		started.setYearMonth(backing.getStarted());
		completed.setYearMonth(backing.getCompleted());
	}

	/**
	 * @return the kind
	 */
	CharacteristicCode getKind()
	{
		return kind;
	}

	/**
	 * @return the id
	 */
	public String getId()
	{
		return id;
	}

	/**
	 * @return the brief
	 */
	public boolean isBrief()
	{
		return brief;
	}

	/**
	 * @return whether or not the procedure was or is currently performed
	 */
	public boolean isPerformed()
	{
		return performed;
	}

	/**
	 * Sets the performed status and updates the underlying list. If
	 * <code>performed</code> is <code>true</code> then add the backing property
	 * to the list; if <code>false</code> then remove the backing property from
	 * the list.
	 * 
	 * @param performed
	 *            whether or not the procedure was or is currently performed
	 */
	public void setPerformed(boolean performed)
	{
		// Do nothing if the status has not changed
		if (this.performed == performed)
			return;
		this.performed = performed;
		if (performed)
			list.add(backing);
		else
			list.remove(backing);
	}

	/**
	 * Returns whether or not the procedure is currently performed
	 * 
	 * @return whether or not the procedure is currently performed
	 */
	public byte getCurrent()
	{
		return current;
	}

	/**
	 * @param current
	 *            whether or not the procedure is currently performed
	 */
	public void setCurrent(byte current)
	{
		this.current = current;
	}

	/**
	 * @return the started year-month
	 */
	public FacadeYearMonth getStarted()
	{
		return started;
	}

	/**
	 * @param the
	 *            completed year-month
	 */
	public FacadeYearMonth getCompleted()
	{
		return completed;
	}

	/**
	 * Returns the values of the fields
	 * 
	 * @return the values of the fields
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder("FacadeProcedure={");
		if (!performed)
			return builder.append('}').toString();
		if (current > 0)
			builder.append("current=").append(current).append(',');
		builder.append("backing={").append(backing).append("}}");
		return builder.toString();
	}

	/**
	 * Version identifier for serializable class
	 */
	private static final long serialVersionUID = -3452526424645294532L;
}
