package org.quantumleaphealth.action;

import java.util.List;

import javax.ejb.Local;
import javax.faces.model.SelectItem;
import org.quantumleaphealth.model.patient.UserPatient;

@Local
public interface AvatarAdministratorInterface
{
	/**
	 * @return list of avatar patient profiles for use in tables of avatar profiles.
	 */
	public List<UserPatient> getAvatarPatientProfiles();
	
	/**
	 * @return return list for use in a option lists of avatar profiles.
	 */
	public List<SelectItem> getAvatarPatientProfilesOptions();
	
	/**
	 * Removes the stateful session bean from memory. This method is required by
	 * the EJB3 specification.
	 */
	public void destroy();
}
