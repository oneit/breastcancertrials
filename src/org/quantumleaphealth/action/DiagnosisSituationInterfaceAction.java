package org.quantumleaphealth.action;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.web.RequestParameter;

@Name("diagnosisSituation")
@Stateful
public class DiagnosisSituationInterfaceAction implements DiagnosisSituationInterface
{
	@RequestParameter("hostname")
	private String hostname;

	@RequestParameter("category")
	private String category;

	public String redirectToPublicMyTrials() 
	{
		return "pass";
	}

	public String getPatientProfile() 
	{
		return "Avatar3@bct.org";
	}
	
	public void setHostname(String hostname) 
	{
		this.hostname = hostname;
	}

	public String getHostname() 
	{
		return hostname;
	}

	public void setCategory(String category) 
	{
		this.category = category;
	}

	public String getCategory() 
	{
		return category;
	}

	/**
	 */
	@Remove
	@Destroy
	@Override
	public void destroy() 
	{
	}


}
