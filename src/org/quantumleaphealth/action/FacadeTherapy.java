/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;

import org.quantumleaphealth.model.patient.Therapy;
import org.quantumleaphealth.ontology.CharacteristicCode;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.*;

/**
 * Represents a facade into a patient's therapy treatment history. This
 * serializable JavaBean exposes the <code>performed</code> field to the GUI and
 * keeps the list of performed therapies up to date.
 * 
 * @author Tom Bechtold
 * @version 2009-07-09
 */
public class FacadeTherapy implements Serializable
{
	/**
	 * The type
	 */
	private final CharacteristicCode type;
	/**
	 * The agents
	 */
	private final CharacteristicCode[] agents;

	/**
	 * Represents the started date, guaranteed to be non-<code>null</code>
	 */
	private final FacadeYearMonth started;
	/**
	 * Represents the completed date, guaranteed to be non-<code>null</code>
	 */
	private final FacadeYearMonth completed;

	/**
	 * The underlying property that this facade represents
	 */
	private Therapy backing;
	/**
	 * The list that holds all performed therapies. This list will be updated
	 * with the <code>backing</code> whenever <code>performed</code> changes
	 * status
	 */
	private Collection<Therapy> list;
	/**
	 * Whether or not the therapy was performed
	 */
	private boolean performed;
	/**
	 * Whether or not the progression occurred in bit-wise format. 0 = not
	 * answered yet, 2 = No, 4 = Yes
	 */
	private long progression;
	/**
	 * The setting under which the therapy occurred in one-based bit-wise format
	 * or 1 for <code>UNSURE</code> or 0 for not answered yet.
	 */
	private long setting;
	/**
	 * The choices for settings. This list must equal the list in the view.
	 */
	private static final CharacteristicCode[] SETTINGS = { PREVENTION, NEOADJUVANT, ADJUVANT, METASTATIC, OSTEOPOROSIS,
			ADVERSE_DRUG_EFFECT, BONECANCER_SECONDARY };
	/**
	 * The status in one-based bit-wise format or 1 for <code>UNSURE</code> or 0
	 * for not answered yet / not applicable.
	 */
	private long status;
	/**
	 * The choices for states. This list must equal the list in the view.
	 */
	private static final CharacteristicCode[] STATES = { CONCURRENT, COMPLETED, INCOMPLETE };
	/**
	 * The reason for incomplete treatment in one-based bit-wise format or 1 for
	 * <code>UNSURE</code> or 0 for not answered yet / not applicable.
	 */
	private long incomplete;
	/**
	 * The choices for incomplete reason. This list must equal the list in the
	 * view.
	 */
	private static final CharacteristicCode[] INCOMPLETE_REASONS = { RECURRENT_LOCALLY, ADVERSE_DRUG_EFFECT };

	/**
	 * Instantiate a facade with the owning list and specified underlying
	 * property.
	 * 
	 * @param list
	 *            holds all performed therapies of this type
	 * @param agents
	 *            agents used in the therapy
	 * @param type
	 *            the type of therapy
	 * @throws IllegalArgumentException
	 *             if <code>list</code> is <code>null</code> or
	 *             <code>agents</code> is empty
	 */
	public FacadeTherapy(Collection<Therapy> list, CharacteristicCode[] agents, CharacteristicCode type)
	{
		if ((agents == null) || (agents.length == 0))
			throw new IllegalArgumentException("agents not specified: " + Arrays.toString(agents));
		this.agents = agents;
		if (type == null)
			throw new IllegalArgumentException("type not specified");
		this.type = type;
		// Create date facades
		started = new FacadeYearMonth();
		completed = new FacadeYearMonth();
		// Store the list and update the backing property and date facades
		setTherapies(list);
	}

	/**
	 * Updates a facade with an underlying property from a list. This method
	 * updates the <code>list</code> and <code>backing</code> instance variables
	 * and the dates in the <code>started</code> and <code>completed</code>
	 * facades.
	 * 
	 * @param list
	 *            holds all performed therapies
	 * @throws IllegalArgumentException
	 *             if <code>list</code> is <code>null</code>
	 */
	void setTherapies(Collection<Therapy> list)
	{
		if (list == null)
			throw new IllegalArgumentException("list not specified");
		this.list = list;
		// Find the backing property in the list that corresponds to the agents
		backing = null;
		for (Therapy current : list)
			if (hasSameMembers(current.getAgents(), agents))
			{
				backing = current;
				break;
			}
		if (backing != null)
		{
			performed = true;
			progression = backing.isProgression() ? 4l : 2l;
			setting = getBitValue(backing.getSetting(), SETTINGS);
			status = getBitValue(backing.getStatus(), STATES);
			incomplete = getBitValue(backing.getIncomplete(), INCOMPLETE_REASONS);
		}
		else
		{
			// If not found in the list then create a blank, unperformed one
			backing = new Therapy();
			backing.setType(type);
			backing.setAgents(agents);
			performed = false;
			progression = 0l;
			setting = 0l;
			status = 0l;
			incomplete = 0l;
		}
		// Update the date facades with the dates from the new backing object
		started.setYearMonth(backing.getStarted());
		completed.setYearMonth(backing.getCompleted());
	}

	/**
	 * @return the type
	 */
	public CharacteristicCode getType()
	{
		return type;
	}

	/**
	 * Returns whether this facade represents a chemotherapy treatment
	 * 
	 * @return whether this facade represents a chemotherapy treatment
	 */
	public boolean isChemotherapy()
	{
		return CHEMOTHERAPY.equals(type);
	}

	/**
	 * Returns whether this facade represents a targeted/biological treatment
	 * 
	 * @return whether this facade represents a targeted/biological treatment
	 */
	public boolean isBiological()
	{
		return BIOLOGICAL.equals(type);
	}

	/**
	 * Returns whether this facade represents a endocrine treatment
	 * 
	 * @return whether this facade represents a endocrine treatment
	 */
	public boolean isEndocrine()
	{
		return ENDOCRINE.equals(type);
	}

	/**
	 * Returns whether this facade represents a bisphosphonate treatment
	 * 
	 * @return whether this facade represents a bisphosphonate treatment
	 */
	public boolean isBisphosphonate()
	{
		return BISPHOSPHONATE.equals(type);
	}

	/**
	 * Returns whether or not the therapy was performed.
	 * 
	 * @return whether or not the therapy was performed
	 */
	public boolean isPerformed()
	{
		return performed;
	}

	/**
	 * Sets the performed status and updates the underlying list. If
	 * <code>performed</code> is <code>true</code> then add the backing property
	 * to the list; if <code>false</code> then remove the backing property from
	 * the list.
	 * 
	 * @param performed
	 *            whether or not the therapy was or is currently taken
	 */
	public void setPerformed(boolean performed)
	{
		// Do nothing if the status has not changed
		if (this.performed == performed)
			return;
		this.performed = performed;
		if (performed)
			list.add(backing);
		else
			list.remove(backing);
	}

	/**
	 * @return the started year-month
	 */
	public FacadeYearMonth getStarted()
	{
		return started;
	}

	/**
	 * @param the
	 *            completed year-month
	 */
	public FacadeYearMonth getCompleted()
	{
		return completed;
	}

	/**
	 * Returns the therapy length in bit-wise format
	 * 
	 * @return the therapy length in bit-wise format
	 */
	public long getLength()
	{
		int length = backing.getLength();
		return length <= 0 ? 0l : 1l << length;
	}

	/**
	 * Stores the length in bit-wise format into the backing property
	 * 
	 * @param length
	 *            the therapy length in bit-wise format
	 */
	public void setLength(long length)
	{
		backing.setLength((byte) (length <= 0l ? 0 : Long.numberOfTrailingZeros(length)));
	}

	/**
	 * Returns the progression in bit-wise format. 0 = not answered yet, 2 =
	 * false, 4 = true
	 * 
	 * @return the progression
	 */
	public long getProgression()
	{
		return progression;
	}

	/**
	 * Returns the progression in bit-wise format. 0 = not answered yet, 2 =
	 * false, 4 = true
	 * 
	 * @return the progression
	 * @param progression
	 */
	public void setProgression(long progression)
	{
		if (this.progression == progression)
			return;
		this.progression = progression;
		backing.setProgression(progression > 2l);
	}

	/**
	 * @return the setting in one-based bit-wise format or 1 for
	 *         <code>UNSURE</code> or 0 for not answered yet / not applicable.
	 */
	public long getSetting()
	{
		return setting;
	}

	/**
	 * @param setting
	 *            the setting in one-based bit-wise format or 1 for
	 *            <code>UNSURE</code> or 0 for not answered yet / not
	 *            applicable.
	 */
	public void setSetting(long setting)
	{
		if (this.setting == setting)
			return;
		this.setting = setting;
		backing.setSetting(getCharacteristic(setting, SETTINGS));
	}

	/**
	 * @return the status in one-based bit-wise format or 1 for
	 *         <code>UNSURE</code> or 0 for not answered yet / not applicable.
	 */
	public long getStatus()
	{
		return status;
	}

	/**
	 * @param status
	 *            the status in one-based bit-wise format or 1 for
	 *            <code>UNSURE</code> or 0 for not answered yet / not
	 *            applicable.
	 */
	public void setStatus(long status)
	{
		if (this.status == status)
			return;
		this.status = status;
		backing.setStatus(getCharacteristic(status, STATES));
	}

	/**
	 * @return the incomplete reason in one-based bit-wise format or 1 for
	 *         <code>UNSURE</code> or 0 for not answered yet / not applicable.
	 */
	public long getIncomplete()
	{
		return incomplete;
	}

	/**
	 * @param incomplete
	 *            the incomplete reason in one-based bit-wise format or 1 for
	 *            <code>UNSURE</code> or 0 for not answered yet / not
	 *            applicable.
	 */
	public void setIncomplete(long incomplete)
	{
		if (this.incomplete == incomplete)
			return;
		this.incomplete = incomplete;
		backing.setIncomplete(getCharacteristic(incomplete, INCOMPLETE_REASONS));
	}

	/**
	 * Returns the values of the fields
	 * 
	 * @return the values of the fields
	 */
	@Override
	public String toString()
	{
		// Only display the backing property if performed
		StringBuilder builder = new StringBuilder("FacadeTherapy={");
		if (performed)
			builder.append("type=").append(type).append(",backing={").append(backing).append("}");
		return builder.append('}').toString();
	}
	
	public String getAgentZero()
	{
		StringBuilder builder = new StringBuilder(type == null? "": type.toString());
		if ( agents.length > 0 )
		{
			builder.append(".");
			for (int i = 0; i < agents.length; i++)
			{
				if ( i > 0 )
				{
					builder.append("+");
				}
				builder.append(agents[i].toString());
			}
			return agents[0].toString();
		}
		return builder.toString();
	}

	/**
	 * @param member
	 *            the member of the set or <code>UNSURE</code>
	 * @param set
	 *            the set
	 * @return the bitwise mask of the one-based position of member within set
	 *         or 1 for <code>UNSURE</code> or 0 if the member is not in the set
	 */
	private static long getBitValue(CharacteristicCode member, CharacteristicCode[] set)
	{
		if (UNSURE.equals(member))
			return 1;
		if ((member == null) || (set == null))
			return 0l;
		int index = member.getIndex(set);
		return (index < 0) ? 0l : 1l << (index + 1);
	}

	/**
	 * @param position
	 *            one-based bit-wise position or or 1 for <code>UNSURE</code> 0
	 *            to return no member
	 * @param set
	 *            the set
	 * @return the element of a set for a one-based bit-wise position or
	 *         <code>UNSURE</code> if <code>position</code> is 1 or
	 *         <code>null</code> if there is no <code>position</code> in
	 *         <code>set</code>
	 */
	private static CharacteristicCode getCharacteristic(long position, CharacteristicCode[] set)
	{
		if (position == 1)
			return UNSURE;
		if ((set == null) || (position < 1))
			return null;
		int index = Long.numberOfTrailingZeros(position) - 1;
		return (index >= set.length) ? null : set[index];
	}

	/**
	 * Returns whether two sets share the same members
	 * 
	 * @param set1
	 *            the first set
	 * @param set2
	 *            the second set
	 * @return whether two sets share the same members
	 */
	private static boolean hasSameMembers(CharacteristicCode[] set1, CharacteristicCode[] set2)
	{
		boolean emptySet1 = (set1 == null) || (set1.length == 0);
		boolean emptySet2 = (set2 == null) || (set2.length == 0);
		if (emptySet1)
			return emptySet2;
		if (emptySet2 || (set1.length != set2.length))
			return false;
		outerLoop: for (int index1 = 0; index1 < set1.length; index1++)
		{
			for (int index2 = 0; index2 < set2.length; index2++)
				if ((set1[index1] != null) && set1[index1].equals(set2[index2]))
					continue outerLoop;
			return false;
		}
		return true;
	}

	/**
	 * BCT-405:  Therapy type other boxes can be checked but no need to save other information such as therapy begin, etc.
	 * @return true if in the therapy other group.
	 */
	public boolean isTherapyOther()
	{
		if ( isChemotherapy() || isEndocrine() ||  isBiological() || isBisphosphonate() )
		{	
			for ( CharacteristicCode code:  agents )
			{
				if ( code.isMember( CLASS_THERAPY_OTHER_SET ) )
				{
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * @return the agents
	 */
	public CharacteristicCode[] getAgents()
	{
		return agents;
	}

	/**
	 * Version identifier for serializable class
	 */
	private static final long serialVersionUID = -8592829666958645073L;
}
