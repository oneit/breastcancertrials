/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import org.quantumleaphealth.model.patient.Invitation;
import org.quantumleaphealth.model.trial.Trial;

/**
 * A sorted list of invitations that share the same trial.
 * 
 * @author Tom Bechtold
 * @version 2009-01-09
 */
public class TrialInvitations implements Comparable<TrialInvitations>, Serializable
{
	/**
	 * The list of sorted invitations, guaranteed to be non-<code>null</code>.
	 * Because additions are inserted in sort order, <code>LinkedList</code> is
	 * chosen for memory use and efficiency.
	 */
	private final LinkedList<Invitation> sortedList = new LinkedList<Invitation>();

	/**
	 * Compares the name of the site that an invitation references
	 */
	private static final Comparator<Invitation> COMPARATOR_SITENAME = new Comparator<Invitation>()
	{
		/**
		 * @param invitation
		 *            the invitation
		 * @return the name of the site that the invitation references or
		 *         <code>null</code> if not available
		 */
		private String getSiteName(Invitation invitation)
		{
			return (invitation == null) || (invitation.getTrialSite() == null)
					|| (invitation.getTrialSite().getSite() == null) ? null : invitation.getTrialSite().getSite()
					.getName();
		}

		/**
		 * @return a negative number if the site name of the first invitation is
		 *         before that of the second; 0 if the sites' names are equal or
		 *         unknown; or a positive number otherwise
		 * @param invitation1
		 *            the first invitation
		 * @param invitation2
		 *            the second invitation
		 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
		 */
		public int compare(Invitation invitation1, Invitation invitation2)
		{
			String siteName1 = getSiteName(invitation1);
			String siteName2 = getSiteName(invitation2);
			// Known entities are always sorted before unknown ones
			if (siteName1 == null)
				return (siteName2 == null) ? 0 : 1;
			return (siteName2 == null) ? -1 : siteName1.compareToIgnoreCase(siteName2);
		}
	};

	/**
	 * Creates a list with one element.
	 * 
	 * @param invitation
	 *            the first invitation to add
	 * @throws IllegalArgumentException
	 *             if the invitation's trial is not available
	 */
	public TrialInvitations(Invitation invitation) throws IllegalArgumentException
	{
		if (invitation == null)
			throw new IllegalArgumentException("invitation is not specified");
		if ((invitation.getTrialSite() == null) || (invitation.getTrialSite().getTrial() == null))
			throw new IllegalArgumentException("invitation's trial is not available");
		// Add invitation as first in list
		sortedList.add(invitation);
	}

	/**
	 * @return the trial that each invitation in the list shares, guaranteed to
	 *         be non-<code>null</code>
	 */
	public Trial getTrial()
	{
		// Since all invitations share the same trial, get it from the first
		// element
		return sortedList.getFirst().getTrialSite().getTrial();
	}

	/**
	 * Adds an invitation to the list.
	 * 
	 * @param invitation
	 *            the invitation to add
	 * @return whether or not the invitation was added, e.g., the invitation's
	 *         trial matched that of the current list elements
	 * @throws IllegalArgumentException
	 *             if the invitation's trial is not available
	 */
	public boolean add(Invitation invitation) throws IllegalArgumentException
	{
		// If trial does not match then do not add the invitation
		if (invitation == null)
			throw new IllegalArgumentException("invitation is not specified");
		if ((invitation.getTrialSite() == null) || (invitation.getTrialSite().getTrial() == null))
			throw new IllegalArgumentException("invitation's trial is not available");
		if (!getTrial().equals(invitation.getTrialSite().getTrial()))
			return false;
		// Insert the new invitation in the sorted location
		ListIterator<Invitation> listIterator = sortedList.listIterator();
		while (listIterator.hasNext())
		{
			int comparison = COMPARATOR_SITENAME.compare(invitation, listIterator.next());
			// If sorted-equal then add the invitation after this one
			if (comparison == 0)
			{
				listIterator.add(invitation);
				return true;
			}
			// If sorted-before than then add the invitation before this one
			if (comparison < 0)
			{
				listIterator.previous();
				listIterator.add(invitation);
				return true;
			}
		}
		// If not added yet then it is sorted last
		sortedList.add(invitation);
		return true;
	}

	/**
	 * @return the invitations in sorted order
	 */
	public List<Invitation> getSortedList()
	{
		return sortedList;
	}

	/**
	 * @return a negative number if the name of this invitation's trial is
	 *         before that of the other's or the other's is unknown; 0 if the
	 *         trials' names are equal or both are unknown; or a positive number
	 *         otherwise
	 * @param other
	 *            the other list of invitations
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(TrialInvitations other)
	{
		String trialName1 = getTrial().getName();
		String trialName2 = (other == null) ? null : other.getTrial().getName();
		// Known entities are always sorted before unknown ones
		if (trialName1 == null)
			return (trialName2 == null) ? 0 : 1;
		return (trialName2 == null) ? -1 : trialName1.compareToIgnoreCase(trialName2);
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -6528412792164715625L;
}
