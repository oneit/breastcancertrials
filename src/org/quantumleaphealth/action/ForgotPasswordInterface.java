package org.quantumleaphealth.action;

import javax.ejb.Local;

/**
 * This bean handles all of the user interaction on the forgot_password page.
 * It was originally in the PatientInterfaceAction class.
 * 
 * @author wgweis
 */
@Local
public interface ForgotPasswordInterface 
{
	public String sendPasswordSuccess();
	
	public String cancelPasswordReset();
	
	public void setEmailAddress(String emailAddress); 

	public String getEmailAddress(); 

	public void destroy();
}
