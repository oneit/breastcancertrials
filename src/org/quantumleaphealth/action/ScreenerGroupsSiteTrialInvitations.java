/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.ListIterator;

import org.quantumleaphealth.model.patient.Invitation;
import org.quantumleaphealth.model.screener.ScreenerGroup;
import org.quantumleaphealth.model.trial.Site;
import org.quantumleaphealth.model.trial.Trial;

/**
 * A list of screener groups associated to a list of invitations that are
 * associated with sites and trials. This class and its inner classes are used
 * to group invitations into a sorted hierarchy:
 * <ol>
 * <li><tt>ScreenerGroup</tt> sorted by name</li>
 * <li><tt>Site</tt> sorted by name</li>
 * <li><tt>Trial</tt> sorted by primary id</li>
 * <li><tt>Invitation</tt> sorted by creation date descending</li>
 * </ol>
 * 
 * @author Tom Bechtold
 * @version 2008-09-19
 */
public class ScreenerGroupsSiteTrialInvitations extends
		LinkedList<ScreenerGroupsSiteTrialInvitations.ScreenerGroupSiteTrialInvitations>
{

	/**
	 * The total number of invitations stored
	 */
	private int count;

	/**
	 * @return the total number of invitations stored
	 */
	public int getCount()
	{
		return count;
	}

	/**
	 * Adds an invitation associated with a screener group to an element. If an
	 * existing element does not match then a new one is created and inserted in
	 * the correct order.
	 * 
	 * @param invitation
	 *            the invitation
	 * @throws IllegalArgumentException
	 *             if parameter is <code>null</code> or the invitation's
	 *             screener group, site or trial is <code>null</code>
	 */
	public void add(Invitation invitation) throws IllegalArgumentException
	{
		// First try adding to an existing member
		for (ScreenerGroupSiteTrialInvitations screenerGroupSiteTrialInvitations : this)
			if (screenerGroupSiteTrialInvitations.add(invitation))
			{
				count++;
				return;
			}
		// Otherwise create a new screener group list and insert it into the
		// correct sorted index
		ScreenerGroupSiteTrialInvitations screenerGroupSiteTrialInvitations = new ScreenerGroupSiteTrialInvitations(
				invitation);
		ListIterator<ScreenerGroupSiteTrialInvitations> listIterator = listIterator();
		while (listIterator.hasNext())
			if (screenerGroupSiteTrialInvitations.compareTo(listIterator.next()) < 0)
			{
				// If the new invitation should go before the cursor then back
				// up one and add it
				listIterator.previous();
				listIterator.add(screenerGroupSiteTrialInvitations);
				count++;
				return;
			}
		// If we haven't added it by now then it must be sorted at the end
		add(screenerGroupSiteTrialInvitations);
		count++;
	}

	/**
	 * @param invitationId
	 *            the invitation's id to find
	 * @return the invitation whose id that matches the parameter or
	 *         <code>null</code> if none found
	 */
	public Invitation getInvitation(Long invitationId)
	{
		if (invitationId == null)
			return null;
		for (ScreenerGroupSiteTrialInvitations screenerGroupSiteTrialInvitations : this)
			for (SiteTrialInvitations siteTrialInvitations : screenerGroupSiteTrialInvitations)
				for (TrialInvitations trialInvitations : siteTrialInvitations)
					for (Invitation invitation : trialInvitations)
						if (invitation.getId().equals(invitationId))
							return invitation;
		return null;
	}

	/**
	 * Associates a screener group to a list of invitations that are associated
	 * with sites and trials. This class is sorted by its group's name.
	 */
	public static class ScreenerGroupSiteTrialInvitations extends LinkedList<SiteTrialInvitations> implements
			Comparable<ScreenerGroupSiteTrialInvitations>
	{
		/**
		 * The screener group that is common for all elements, guaranteed to be
		 * non-<code>null</code>
		 */
		private final ScreenerGroup screenerGroup;

		/**
		 * Store instance variable and add first member
		 * 
		 * @param invitation
		 *            the invitation to create a screener group list for
		 * @throws IllegalArgumentException
		 *             if the group's name is <code>null</code>
		 */
		private ScreenerGroupSiteTrialInvitations(Invitation invitation) throws IllegalArgumentException
		{
			// Store instance variable
			ScreenerGroup screenerGroup = (invitation == null) || (invitation.getTrialSite() == null) ? null
					: invitation.getTrialSite().getScreenerGroup();
			if (screenerGroup == null)
				throw new IllegalArgumentException("screenerGroup not specified in invitation " + invitation);
			if (screenerGroup.getName() == null)
				throw new IllegalArgumentException("screenerGroup name not specified for " + screenerGroup);
			this.screenerGroup = screenerGroup;
			// Add the invitation as the first element
			add(new SiteTrialInvitations(invitation));
		}

		/**
		 * @return the screener group that is common for all elements,
		 *         guaranteed to be non-<code>null</code>
		 */
		public ScreenerGroup getScreenerGroup()
		{
			return screenerGroup;
		}

		/**
		 * Adds an invitation to a member. This method first verifies that the
		 * invitation's screener group matches its own. If not, it does nothing
		 * and returns <code>false</code>. This method then tries to add the
		 * invitation to each member. If no members accept it, the invitation is
		 * added to a new member which is inserted in the correct order in the
		 * list.
		 * 
		 * @param invitation
		 *            the invitation to add
		 * @return whether or not the invitation's screener group matches this
		 *         one's
		 * @throws IllegalArgumentException
		 *             if the invitation is <code>null</code> or its
		 *             screenergroup name is <code>null</code> or its site's
		 *             name is <code>null</code> or its trial's primary id is
		 *             <code>null</code>
		 */
		private boolean add(Invitation invitation) throws IllegalArgumentException
		{
			// Validate screener group matches
			ScreenerGroup screenerGroup = (invitation == null) || (invitation.getTrialSite() == null) ? null
					: invitation.getTrialSite().getScreenerGroup();
			if (screenerGroup == null)
				throw new IllegalArgumentException("screenerGroup not specified in invitation " + invitation);
			if (!this.screenerGroup.equals(screenerGroup))
				return false;
			// First try to add the invitation to an existing member
			for (SiteTrialInvitations siteTrialInvitations : this)
				if (siteTrialInvitations.add(invitation))
					return true;
			// If not stored then insert a new element in the correct location
			// of the list based upon comparator
			SiteTrialInvitations siteTrialInvitations = new SiteTrialInvitations(invitation);
			ListIterator<SiteTrialInvitations> listIterator = listIterator();
			while (listIterator.hasNext())
				if (siteTrialInvitations.compareTo(listIterator.next()) < 0)
				{
					// If the new invitation should go before the cursor then
					// back up one and add it
					listIterator.previous();
					listIterator.add(siteTrialInvitations);
					return true;
				}
			// If we haven't added it by now then it must be sorted at the end
			return add(siteTrialInvitations);
		}

		/**
		 * @param screenerGroupSiteTrialInvitations
		 *            the object to be compared
		 * @return a negative integer, zero, or a positive integer as this
		 *         group's name is (case-insensitive) less than, equal to, or
		 *         greater than the specified object's group's name
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(ScreenerGroupSiteTrialInvitations screenerGroupSiteTrialInvitations)
		{
			// Known quantity is always sorted before unknown
			if (screenerGroupSiteTrialInvitations == null)
				return -1;
			// Compare group names, guaranteed by the class constructor to be
			// non-null
			return screenerGroup.getName().trim().compareToIgnoreCase(
					screenerGroupSiteTrialInvitations.screenerGroup.getName().trim());
		}

		/**
		 * Version UID for serializable class
		 */
		private static final long serialVersionUID = -7045114699549336515L;
	}

	/**
	 * Associates a site to a list of invitations that are associated with a
	 * trial. This class is sorted by its site's name.
	 */
	public static class SiteTrialInvitations extends LinkedList<TrialInvitations> implements
			Comparable<SiteTrialInvitations>
	{
		/**
		 * The site that is common for all elements, guaranteed to be non-
		 * <code>null</code>
		 */
		private final Site site;

		/**
		 * Store invitation's site into instance variable and add first element
		 * to list
		 * 
		 * @param invitation
		 *            the invitation to add
		 * @throws IllegalArgumentException
		 *             if the invitation's site's name is <code>null</code>
		 */
		private SiteTrialInvitations(Invitation invitation) throws IllegalArgumentException
		{
			// Validate parameter and store instance variable
			site = (invitation == null) || (invitation.getTrialSite() == null) ? null : invitation.getTrialSite()
					.getSite();
			if (site == null)
				throw new IllegalArgumentException("site not specified for " + invitation);
			if (site.getName() == null)
				throw new IllegalArgumentException("site name not specified for " + site);
			// Add first element to the list
			add(new TrialInvitations(invitation));
		}

		/**
		 * @return the site that is common for all elements, guaranteed to be
		 *         non-<code>null</code>
		 */
		public Site getSite()
		{
			return site;
		}

		/**
		 * Adds an invitation to a member. This method first verifies that the
		 * invitation's site matches its own. If not, it does nothing and
		 * returns <code>false</code>. Otherwise it tries to add the invitation
		 * to each member. If no members accept it, the invitation is added to
		 * new member which is inserted in the correct order in the list.
		 * 
		 * @param invitation
		 *            the invitation to add
		 * @return whether or not the invitation's site matches this one's
		 * @throws IllegalArgumentException
		 *             if the invitation's site's name is <code>null</code>
		 */
		private boolean add(Invitation invitation) throws IllegalArgumentException
		{
			// Validate parameter
			Site site = (invitation == null) || (invitation.getTrialSite() == null) ? null : invitation.getTrialSite()
					.getSite();
			if (site == null)
				throw new IllegalArgumentException("site not specified in invitation " + invitation);
			if (site.getName() == null)
				throw new IllegalArgumentException("site name not specified in site " + site);
			// If sites are not equal (id or name) then do not store
			if ((site.getId() != null) ? ((this.site.getId() == null) || !site.getId().equals(this.site.getId()))
					: ((this.site.getId() != null) || !site.getName().trim().equalsIgnoreCase(
							this.site.getName().trim())))
				return false;
			// Try to add the invitation to an existing member
			for (TrialInvitations trialInvitations : this)
				if (trialInvitations.add(invitation))
					return true;
			// If not stored then insert a new element in the correct location
			// of the list based upon comparator
			TrialInvitations trialInvitations = new TrialInvitations(invitation);
			ListIterator<TrialInvitations> listIterator = listIterator();
			while (listIterator.hasNext())
				if (trialInvitations.compareTo(listIterator.next()) < 0)
				{
					// If the new invitation should go before the cursor then
					// back up one and add it
					listIterator.previous();
					listIterator.add(trialInvitations);
					return true;
				}
			// If we haven't added it by now then it must be sorted at the end
			return add(trialInvitations);
		}

		/**
		 * @param siteTrialInvitations
		 *            the object to be compared
		 * @return a negative integer, zero, or a positive integer as this
		 *         site's name is (case-insensitive) less than, equal to, or
		 *         greater than the specified object's site's name
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(SiteTrialInvitations siteTrialInvitations)
		{
			// Known quantity is always sorted before unknown
			if (siteTrialInvitations == null)
				return -1;
			// Compare site names, guaranteed by the class constructor to be
			// non-null
			return site.getName().trim().compareToIgnoreCase(siteTrialInvitations.site.getName().trim());
		}

		/**
		 * Version UID for serializable class
		 */
		private static final long serialVersionUID = 2564095820067859814L;
	}

	/**
	 * Associates a trial to a list of invitations.
	 */
	public static class TrialInvitations extends LinkedList<Invitation> implements Comparable<TrialInvitations>
	{
		/**
		 * The trial that is common for all elements, guaranteed to be non-
		 * <code>null</code>
		 */
		private final Trial trial;
		/**
		 * Compares invitations' creation dates. This object may be used to sort
		 * in descending order of invitation creation date.
		 */
		private static final Comparator<Invitation> COMPARATOR = new Comparator<Invitation>()
		{
			/**
			 * Sorts in descending order of creation date. Unknown elements are
			 * greater than known ones.
			 * 
			 * @param invitation1
			 *            the first invitation
			 * @param invitation2
			 *            the second invitation
			 * @return -1 if invitation1's creation date is after invitation2's
			 *         1 if invitation1's creation date is before invitation2's
			 *         or 0 if the creation dates are equal or both are not
			 *         available
			 */
			public int compare(Invitation invitation1, Invitation invitation2)
			{
				// Unknown is always sorted after known
				Date created1 = (invitation1 == null) ? null : invitation1.getCreated();
				Date created2 = (invitation2 == null) ? null : invitation2.getCreated();
				if ((created1 == null) && (created2 == null))
					return 0;
				if (created1 == null)
					return 1;
				if (created2 == null)
					return -1;
				// Invert the comparison so most recent date is sorted higher
				return created2.compareTo(created1);
			}
		};

		/**
		 * Stores the invitation's trial and stores the invitation as the first
		 * in the list
		 * 
		 * @param invitation
		 *            the invitation
		 * @throws IllegalArgumentException
		 *             if the invitation's trial's primaryId is
		 *             <code>null</code>
		 */
		private TrialInvitations(Invitation invitation) throws IllegalArgumentException
		{
			// Validate parameter and store instance variable
			if (invitation == null)
				throw new IllegalArgumentException("invitation not specified");
			trial = (invitation.getTrialSite() == null) ? null : invitation.getTrialSite().getTrial();
			if (trial == null)
				throw new IllegalArgumentException("trial not specified for " + invitation);
			if (trial.getPrimaryID() == null)
				throw new IllegalArgumentException("trial primary id not specified for " + trial);
			// Add first invitation to the list
			super.add(invitation);
		}

		/**
		 * @return the trial that is common for all elements, guaranteed to be
		 *         non-<code>null</code>
		 */
		public Trial getTrial()
		{
			return trial;
		}

		/**
		 * Adds an invitation to the list in the correct sorted place if the
		 * invitation's trial matches this class'.
		 * 
		 * @param newInvitation
		 *            invitation to be added to this list
		 * @return whether the invitation's trial is equivalent to this class'
		 * @see java.util.LinkedList#add(java.lang.Object)
		 */
		@Override
		public boolean add(Invitation newInvitation) throws IllegalArgumentException
		{
			// Validate parameter
			Trial newTrial = (newInvitation == null) || (newInvitation.getTrialSite() == null) ? null : newInvitation
					.getTrialSite().getTrial();
			if (newTrial == null)
				throw new IllegalArgumentException("trial not specified in invitation " + newInvitation);
			if (newTrial.getPrimaryID() == null)
				throw new IllegalArgumentException("trial primary id not specified (" + newTrial + ") in invitation "
						+ newInvitation);
			// If trials are not equal then do nothing
			if ((newTrial.getId() != null) ? ((this.trial.getId() == null) || !newTrial.getId().equals(
					this.trial.getId())) : ((this.trial.getId() != null) || !newTrial.getPrimaryID().trim()
					.equalsIgnoreCase(this.trial.getPrimaryID().trim())))
				return false;
			// Insert the invitation in the correct location of the list based
			// upon comparator
			ListIterator<Invitation> listIterator = listIterator();
			while (listIterator.hasNext())
				if (COMPARATOR.compare(newInvitation, listIterator.next()) < 0)
				{
					// If the new invitation should go before the cursor then
					// back up one and add it
					listIterator.previous();
					listIterator.add(newInvitation);
					return true;
				}
			// If we haven't added it by now then it must be sorted at the end
			return super.add(newInvitation);
		}

		/**
		 * @param trialInvitations
		 *            the object to be compared
		 * @return a negative integer, zero, or a positive integer as this
		 *         trial's primaryId is (case-insensitive) less than, equal to,
		 *         or greater than the specified object's trial's primaryId
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		public int compareTo(TrialInvitations trialInvitations)
		{
			// Known quantity is always sorted before unknown
			if (trialInvitations == null)
				return -1;
			// Compare trial primary ids, guaranteed by the class constructor to
			// be non-null
			return trial.getPrimaryID().trim().compareToIgnoreCase(trialInvitations.trial.getPrimaryID().trim());
		}

		/**
		 * Version UID for serializable class
		 */
		private static final long serialVersionUID = 7451839138303330008L;
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -3312054008848042539L;
}
