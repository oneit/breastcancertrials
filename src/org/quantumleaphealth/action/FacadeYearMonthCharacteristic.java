/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import org.quantumleaphealth.ontology.ByteShortCharacteristicHolder;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Represents a facade into a map of year/month characteristics. The year is
 * stored using the superclass' <code>short</code> field, the month is stored
 * using this class' <code>byte</code> field. This serializable JavaBean exposes
 * the <code>byteValue</code> field to the GUI. It caches the characteristic's
 * value in order to reduce effort querying the holder.
 * 
 * @author Tom Bechtold
 * @version 2008-05-22
 */
public class FacadeYearMonthCharacteristic extends FacadeShortCharacteristic
{
	/**
	 * Holds the byte characteristics, guaranteed to be non-<code>null</code>
	 */
	private ByteShortCharacteristicHolder byteShortCharacteristicHolder;
	/**
	 * Cached value of the characteristic's byte value
	 */
	private byte cachedValue;

	/**
	 * Validates and stores the parameter and sets the cached value
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to represent
	 * @param byteShortCharacteristicHolder
	 *            holds the byteShort characteristics
	 * @throws IllegalArgumentException
	 *             if any parameter is <code>null</code>
	 */
	FacadeYearMonthCharacteristic(CharacteristicCode characteristicCode,
			ByteShortCharacteristicHolder byteShortCharacteristicHolder) throws IllegalArgumentException
	{
		super(characteristicCode, byteShortCharacteristicHolder);
		setbyteShortCharacteristicHolder(byteShortCharacteristicHolder);
	}

	/**
	 * Validates and stores the parameter and updates the cached value
	 * 
	 * @param byteShortCharacteristicHolder
	 *            holds the byteShort characteristics
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	void setbyteShortCharacteristicHolder(ByteShortCharacteristicHolder byteShortCharacteristicHolder)
			throws IllegalArgumentException
	{
		if (byteShortCharacteristicHolder == null)
			throw new IllegalArgumentException("holder not specified");
		this.byteShortCharacteristicHolder = byteShortCharacteristicHolder;
		cachedValue = byteShortCharacteristicHolder.getByte(characteristicCode);
		// Call superclass to set its holder
		setShortCharacteristicHolder(byteShortCharacteristicHolder);
	}

	/**
	 * @return the value that represents a characteristic or <code>null</code>
	 *         if this characteristic does not have any
	 */
	public byte getByteValue()
	{
		return cachedValue;
	}

	/**
	 * Sets a value for this characteristic.
	 * 
	 * @param byteValue
	 *            the byte to store in the characteristic
	 */
	public void setByteValue(byte byteValue)
	{
		if (cachedValue == byteValue)
			return;
		byteShortCharacteristicHolder.setByte(characteristicCode, byteValue);
		cachedValue = byteValue;
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = -5651597971834494496L;
}
