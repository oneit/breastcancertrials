package org.quantumleaphealth.action;

import java.util.ArrayList;
import java.util.Date;

import javax.ejb.Local;

import org.quantumleaphealth.model.patient.UserPatient;

@Local
public interface AvatarProfileInterface
{
	public UserPatient getPatient();

	public ArrayList<AvatarTrialGroup> getAvatarTrialList();

	public FacadePatientHistory getHistory();
	
	public AvatarHistoryConfiguration getAvatarHistoryConfiguration(); 

	/******* needed for the avatar_browse_trials page *****/
	public void setPostalCode( Long postalCode );

	public Long getPostalCode();

	public void setGroupedByProximity(int groupedByProximity);

	public int getGroupedByProximity();
	
	public void setPrincipal(String principal); 

	public String getPrincipal(); 

	public void setAvatarTrialListLastUpdated(Date avatarTrialListLastUpdated);
	
	public Date getAvatarTrialListLastUpdated();
	
	/**
	 * Referenced on the browse_trials page.
	 * @return
	 */
	public String getAvatarAttributesPrincipalMapJavaScriptString();
	
	public void destroy();
}
