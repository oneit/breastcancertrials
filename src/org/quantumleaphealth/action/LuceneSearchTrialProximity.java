package org.quantumleaphealth.action;

import java.io.Reader;
import java.io.StringReader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.charfilter.HTMLStripCharFilter;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.highlight.Highlighter;
import org.apache.lucene.search.highlight.NullFragmenter;
import org.apache.lucene.search.highlight.QueryScorer;
import org.apache.lucene.search.highlight.SimpleHTMLFormatter;
import org.apache.lucene.util.Version;
import org.jboss.seam.text.SeamTextLexer;
import org.jboss.seam.text.SeamTextParser;

/**
 * Extends TrialProximity so that calls for trial text fields can be overridden with marked up text version.
 * 
 * @author Warren George Weis
 * 
 * @version 5/16/2014
 *
 */
public class LuceneSearchTrialProximity extends TrialProximity
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1150171440779826867L;

	/**
	 * TrialProxmity implements the Serializable interface, so we need this here as well. 
	 */
	
    /**
     * Logger.  Can't be injected as this is not a managed bean.
     */
    private final Log logger = LogFactory.getLog(LuceneSearchTrialProximity.class);

    /**
     * The simpleHTMLFormatter instance variable will do for most tags, such as <h:outputText> or pure HTML.
     */
	private static final SimpleHTMLFormatter simpleHTMLFormatter =  new SimpleHTMLFormatter("<span class=\"highlight\">", "</span>");
	
	private String formattedSearchString;
	
	/**
	 * public constructor copying instance fields of the TrialProximity parent we are shadowing.
	 * 
	 * @param trialProxmity the trialProxmity to shadow
	 * @param formattedSearchString the search string used for the original match
	 */
	public LuceneSearchTrialProximity(TrialProximity trialProxmity, String formattedSearchString) 
	{
		// copy instance variables
		this.trial = trialProxmity.getTrial();
		this.site = trialProxmity.getSite();
		this.distance = trialProxmity.getDistance();
		this.distanceUnit = trialProxmity.getDistanceUnit();
		
		// set a local copy of the formatted search string.
		this.formattedSearchString = formattedSearchString;
	}

	/**
	 * Main method to get highlighted text for different fields and text.
	 * 
	 * @param fieldName
	 * @param text
	 * @return formatted text if a match, original text if no match (highlighting returns a null if nothing in the text can be highlighted).
	 */
	private String getHighlightedSearchString(String fieldName, String text)
	{
		try 
		{
			if ("name".equals(fieldName))
			{
				logger.debug("getHighlightedSearchString called for Trial " + trial.getId() + " name field with text = X" + text + "X, searchString = " + this.formattedSearchString);
			}
		    QueryParser parser = new QueryParser(Version.LUCENE_46, fieldName, new StandardAnalyzer(Version.LUCENE_46));
			parser.setAllowLeadingWildcard(true);
			parser.setDefaultOperator(Operator.AND);

			
			Query query = parser.parse(formattedSearchString);
			
		    TokenStream tokens = new StandardAnalyzer(Version.LUCENE_46).tokenStream(fieldName, new StringReader(text));
		    QueryScorer scorer = new QueryScorer(query, fieldName);
		    
		    Highlighter highlighter = new Highlighter(simpleHTMLFormatter, scorer);
		    highlighter. setTextFragmenter ( new NullFragmenter ( ) ) ; // get the whole string as a token, as none of the strings is large.
		    String result = highlighter.getBestFragment(tokens, text);
		    if ("name".equals(fieldName))
		    {
		    	logger.debug("getHighlightedSearchString returning result for name: " + result);
		    }
			return result == null || result.trim().length() == 0? text: result;
		} 
		catch (Exception e) 
		{
			logger.debug("Error marking up text for trial " + trial.getId() + ", field " + fieldName, e);
			return text;
		}
	}
	
	/**
	 * Some of the trial description fields can be marked up using wiki-like markup language.  The text from these fields first needs to be translated into html
	 * using a SeamTextParser instance (the backing bean for the s:format tag).  Then we need to strip out the html for the actual text markup.  Finally we need to
	 * add the HTML back.  We use an HTMLStripCharFilter for the markup tokens.  This method assumes that the seam text parse has already formatted the text {@link #getTrialTreatmentPlan()}
	 * Here is the link for the discussion of the Solr HTMLStripCharFilter use:
	 * @link http://sigabrt.blogspot.com/2010/04/highlighting-query-in-entire-html.html
	 * 
	 * HTMLStripCharFilter strips out all html tags, replacing them (?) with new line characters.  There is also a warning that the lookahead limit may need to
	 * be increased if the distance between the start and end of a tag is great (Microsoft Word documents saved as HTML tend to do this).
	 * 
	 * @param fieldName
	 * @param text
	 * @return the marked up text if successful, the original text if not.
	 */
	private String getHighlightedHTMLSearchString(String fieldName, String text)
	{
		try
		{
			logger.debug("Highlighting text " + text);
			
		    QueryParser parser = new QueryParser(Version.LUCENE_46, fieldName, new StandardAnalyzer(Version.LUCENE_46));
			parser.setAllowLeadingWildcard(true);
			parser.setDefaultOperator(Operator.AND);
			
			Query query = parser.parse(formattedSearchString);
			
		    TokenStream tokens =  new StandardAnalyzer(Version.LUCENE_46).tokenStream(fieldName, new HTMLStripCharFilter(new StringReader(text)));
		    QueryScorer scorer = new QueryScorer(query, fieldName);
		    
		    Highlighter highlighter = new Highlighter(simpleHTMLFormatter, scorer);
		    highlighter. setTextFragmenter ( new NullFragmenter ( ) ) ; // get the whole string as a token, as none of the strings is large.
		    String result = highlighter.getBestFragment(tokens, text);
		    logger.debug("Highlighted results is " + result);
			return result == null || result.trim().length() == 0? text: result;
		}
		catch (Exception e)
		{
			logger.debug("Error marking up text for trial " + trial.getId() + ", field " + fieldName, e);
			return text;
		}
	}
	
	/**
	 * Equivalent of getTrial().getName()
	 */
	@Override
	public String getTrialName()
	{
		return getHighlightedSearchString("name", trial.getName());
	}
	
	/**
	 * Equivalent of getTrial().getTitle()
	 */
	@Override
	public String getTrialTitle()
	{
		return getHighlightedSearchString("title", trial.getTitle());
	}
	
	/**
	 * Equivalent of getTrial().getPurpose()
	 */
	@Override
	public String getTrialPurpose()
	{
		return getHighlightedSearchString("purpose", trial.getPurpose());
	}
	
	/**
	 * Equivalent of getTrial().getTreatmentPlan
	 */
	@Override
	public String getTrialTreatmentPlan()
	{
		return getHighlightedHTMLSearchString("treatmentPlan", super.getTrialTreatmentPlan());
	}
	
	/**
	 * Equivalent of getTrial().getProcedures
	 */
	@Override
	public String getTrialProcedures()
	{
		return getHighlightedSearchString("procedures", trial.getProcedures());
	}
	
	/**
	 * Equivalent of getTrial().getDuration()
	 */
	@Override
	public String getTrialDuration()
	{
		return getHighlightedSearchString("duration", trial.getDuration());
	}
	
	/**
	 * Equivalent of getTrial().getFollowup()
	 */
	@Override
	public String getTrialFollowup()
	{
		return getHighlightedSearchString("followup", trial.getFollowup());
	}

	/**
	 * Equivalent of getTrial().getSponsor()
	 */
	@Override
	public String getTrialSponsor()
	{
		return getHighlightedSearchString("sponsor", trial.getSponsor());
	}

	/**
	 * Equivalent of getTrial().getBurden()
	 */
	@Override
	public String getTrialBurden()
	{
		return getHighlightedSearchString("burden", trial.getBurden());
	}
}
