package org.quantumleaphealth.action;

import javax.ejb.Local;

/**
 * methods for updating avatars. 
 * @author wgweis
 */
@Local
public interface AvatarUpdateAvatarInterface
{
	/**
	 * Factory method for SEAM to populate the read-only list for update.
	 */	
	public void loadReadOnlyUsers();

	/**
	 * Factory method for SEAM to populate the avatar list for update.
	 */
	public void loadAvatars();

	/**
	 * update the avatars.
	 */
	public void updateAvatars();
	
	/**
	 * update the database entry for this readonly user.
	 */
	public void updateReadOnly();

	/**
	 * destroy method required by EJB3.
	 */
	public void destroy();
}
