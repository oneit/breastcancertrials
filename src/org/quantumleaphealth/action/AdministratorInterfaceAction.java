/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.hibernate.CacheMode;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.jboss.seam.web.Session;
import org.quantumleaphealth.model.patient.SecureConnect_View;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.screener.UserScreener.AccessControl;
import org.quantumleaphealth.model.trial.TrialEvent;

/**
 * Navigates an administrator through viewing patient-trial screening profiles
 * and resetting a patient's credentials. This interface defines the following
 * roles for a local stateful session Enterprise JavaBean (EJB):
 * <ol>
 * <li>Provide authentication of an administrator as a privileged screener.</li>
 * <li>Provide eligibility profiles for a subset of patients</li>
 * <li>Provide password reset for patient</li>
 * </ol>
 * 
 * @author Tom Bechtold
 * @version 2009-01-31
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("administratorInterface")
public class AdministratorInterfaceAction implements AdministratorInterface, Serializable
{
	/**
	 * Persists the entity beans using EJB3 persistence context
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;
	/**
	 * Messages for Java Server Faces
	 */
	@In
	private FacesMessages facesMessages;
	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log LOGGER;
	
	@In
	FacesContext facesContext;
	
	/**
	 * The principal or <code>null</code> if not set. This instance variable is
	 * used for both authentication and for searching for patient records.
	 */
	private String principal;
	/**
	 * The credentials or <code>null</code> if not set. This instance variable
	 * is used for both authentication and for resetting a patient's record.
	 */
	private String credentials;
	/**
	 * Whether or not the administrator is authenticated
	 */
	boolean loggedIn = false;
	/**
	 * The patients that match the principal search or <code>null</code> if none
	 * match
	 */
	private List<UserPatient> patients;
	/**
	 * The selected patient or <code>null</code> if none selected
	 */
	private UserPatient patient;
	/**
	 * The begin date for displaying trial events (open,close,update)
	 */
	private Date trialEventBeginDate;
	/**
	 * The end date for displaying trial events (open,close,update)
	 */
	private Date trialEventEndDate;

	/**
	 * BCT-512: The begin date for displaying secure Connect
	 */
	private Date secureConnectBeginDate;
	/**
	 * BCT-512: The end date for displaying secure Connect
	 */
	private Date secureConnectEndDate;

	/**
	 * Make this list a JSF data model object using the @DataModel annotation.
	 */
	@SuppressWarnings("unused")
	@DataModel
	private List<SecureConnect_View> secureConnectTrials;

	@Factory("secureConnectTrials")
	public void createSecureConnectTrialsList()
	{
		secureConnectTrials = this.getSecureConnect_ViewTrials();
	}

	public void delete()
	{
		System.out.println("Del is called here!!");
	}

	/**
	 * The character which represents the "any" character in SQL query
	 * comparisons.
	 */
	private static final char SQL_WILDCARD = '%';
	private static Date DATE_02_14_2010 = null;

	/**
	 * @return the principal or <code>null</code> if not set
	 * @see org.quantumleaphealth.action.AdministratorInterface#getPrincipal()
	 */
	@BypassInterceptors
	public String getPrincipal()
	{
		return principal;
	}

	/**
	 * @param principal
	 *            the principal or <code>null</code> if not set
	 * @see org.quantumleaphealth.action.AdministratorInterface#setPrincipal(java.lang.String)
	 */
	@BypassInterceptors
	public void setPrincipal(String principal)
	{
		// Do nothing if not changed
		principal = trim(principal);
		if ((principal == null) || ((this.principal != null) && this.principal.equals(principal)))
			return;
		this.principal = principal;
		// If used for patient search then clear cached patient information
		if (loggedIn)
		{
			patients = null;
			patient = null;
		}
	}

	/**
	 * @return the credentials or <code>null</code> if not set
	 * @see org.quantumleaphealth.action.AdministratorInterface#getCredentials()
	 */
	@BypassInterceptors
	public String getCredentials()
	{
		return credentials;
	}

	/**
	 * @param credentials
	 *            the credentials or <code>null</code> if not set
	 * @see org.quantumleaphealth.action.AdministratorInterface#setCredentials(java.lang.String)
	 */
	@BypassInterceptors
	public void setCredentials(String credentials)
	{
		// Do nothing if not changed
		credentials = trim(credentials);
		if ((credentials == null) || ((this.credentials != null) && this.credentials.equals(credentials)))
			return;
		this.credentials = credentials;
	}

	/**
	 * Authenticates the administrator as a privileged screener
	 * 
	 * @see org.quantumleaphealth.action.AdministratorInterface#login()
	 */
	public void login()
	{
		// Validate authentication variables
		loggedIn = false;
		if ((principal == null) || (credentials == null))
		{
			facesMessages.add("Please enter the email address and password.");
			return;
		}
		try
		{
			LOGGER.debug("Authenticating #0", principal);
			AccessControl accessControl = (AccessControl) em
					.createQuery(
							"select us.accessControl from UserScreener us "
									+ "where LOWER(us.principal)=LOWER(:principal) and us.credentials=:credentials order by us.id desc")
					.setParameter("principal", principal).setParameter("credentials", credentials).getSingleResult();
			if (accessControl != AccessControl.SUPERUSER)
			{
				facesMessages.add("You are not authorized as an administrator.");
				LOGGER.debug("Screener #0 not authorized as administrator", principal);
				return;
			}
			loggedIn = true;
			LOGGER.debug("Authenticated #0", principal);
			// Reset instance variables so they can be used for other methods
			// (loading patients, updating patient credentials)
			principal = null;
			credentials = null;
		}
		catch (NoResultException noResultException)
		{
			facesMessages.add("The email address or password is incorrect. Please try again.");
			LOGGER.debug("Bad credentials #0 for #1", credentials, principal);
		}
		catch (NonUniqueResultException nonUniqueResultException)
		{
			// There should not be more than one record for a principal
			facesMessages.add("The email address matches more than one. Please contact the database administrator.");
			LOGGER.error("More than one screener record with principal #0", principal);
		}
		catch (Throwable throwable)
		{
			facesMessages.add("The system is down.");
			LOGGER.error("Cannot log in administrator", throwable);
		}
	}

	/**
	 * Resets instance variables, logs out the user and invalidates the session.
	 * 
	 * @see org.quantumleaphealth.action.AdministratorInterface#logout()
	 * @see Session#invalidate()
	 */
	public void logout()
	{
		destroy();
		Session.instance().invalidate();
		LOGGER.debug("Invalidating administrator session");
	}

	/**
	 * @return whether or not the administrator is authenticated
	 * @see org.quantumleaphealth.action.AdministratorInterface#isLoggedIn()
	 */
	@BypassInterceptors
	public boolean isLoggedIn()
	{
		return loggedIn;
	}

	/**
	 * @return the list of patients sorted by principal or <code>null</code> if
	 *         no patients match the search
	 * @see org.quantumleaphealth.action.AdministratorInterface#getPatients()
	 */
	@SuppressWarnings( { "unchecked", "cast" })
	public List<UserPatient> getPatients()
	{
		// Returned cached value if available
		if (principal == null)
			return null;
		if (patients == null)
		{
			// Surround principal with SQL wildcards
			LOGGER.debug("Loading patients whose principal matches #0", principal);
			String sqlCriterion = SQL_WILDCARD + principal.toLowerCase() + SQL_WILDCARD;
			// The result set is read-only so JPA will not update patient data
			patients = (List<UserPatient>) em.createQuery(
					"select p from UserPatient p " + "where LOWER(p.principal) LIKE :criterion order by p.principal")
					.setHint("org.hibernate.readOnly", Boolean.TRUE).setHint("org.hibernate.cacheMode",
							CacheMode.IGNORE).setParameter("criterion", sqlCriterion).getResultList();
			patient = null;
			LOGGER.debug("Loaded #1 patients whose principal matches #0", principal, patients.size());
		}
		return patients;
	}

	/**
	 * Selects a patient.
	 * 
	 * @param userPatientId
	 *            the unique identifier of the selected patient or
	 *            <code>null</code> if none selected
	 * @see org.quantumleaphealth.action.AdministratorInterface#setUserPatientId(java.lang.Long)
	 * @see #patients
	 * @see #patient
	 */
	@RequestParameter
	public void setUserPatientId(Long userPatientId)
	{
		// Do nothing if none requested or already selected
		if ((userPatientId == null)
				|| ((patient != null) && (patient.getId() != null) && userPatientId.equals(patient.getId())))
			return;
		// Set the instance variable to the found record
		patient = null;
		if (userPatientId.longValue() <= 0)
			return;
		LOGGER.debug("Selecting patient ##0", userPatientId);
		if (patients != null)
			for (UserPatient userPatient : patients)
				if ((userPatient != null) && (userPatient.getId() != null) && userPatient.getId().equals(userPatientId))
				{
					patient = userPatient;
					return;
				}
		LOGGER.warn("Patient ##0 not found in list '#1' of #2", userPatientId, principal, (patients == null) ? 0
				: patients.size());
	}

	/**
	 * @return the selected patient or <code>null</code> if none selected
	 * @see org.quantumleaphealth.action.AdministratorInterface#getPatient()
	 */
	public UserPatient getPatient()
	{
		return patient;
	}

	/**
	 * @return a list of recent (last 14 days) SecureConnect trial invitation
	 *         information
	 */
	@SuppressWarnings("unchecked")
	public List<SecureConnect_View> getSecureConnect_ViewTrials()
	{
        List<SecureConnect_View> myList = em.createQuery("select r from SecureConnect_View r " +
        												 "where ((r.sent between ?1 and ?2) or (r.viewed is null)) ").setParameter(1, this.getSecureConnectBeginDate())
								                         .setParameter(2, this.getSecureConnectEndDate()).getResultList();

		return myList;
	}
	
	/**
	 * @return a count of SecureConnect invitation this year
	 */
	public Long getSecureConnectCountThisYear()
	{
		Date yearDate = new GregorianCalendar(Calendar.getInstance().get(Calendar.YEAR), Calendar.JANUARY, 1).getTime();
		Long secureConnectCountThisYear = (Long) em.createQuery(
				"select count(id) from SecureConnect_View " + "where sent >= ? ").setParameter(1, yearDate, TemporalType.DATE)
				.setHint("org.hibernate.readOnly", Boolean.TRUE).getSingleResult();
		return secureConnectCountThisYear;
	}

	/**
	 * @return a count of SecureConnect invitation total
	 */
	public Long getSecureConnectCountTotal()
	{
		Long secureConnectCountTotal = (Long) em.createQuery("select count(id) from SecureConnect_View ").setHint(
				"org.hibernate.readOnly", Boolean.TRUE).getSingleResult();
		return secureConnectCountTotal;
	}

	/**
	 * @return The number of user patients with trial alerts enabled
	 */
	public Long getTrialAlertCohortCount()
	{
		Long trialAlertCohortCount = (Long) em.createQuery(
				"select count(id) from UserPatient "
						+ "where alertnewtrialsenabled = true and principalinvalidemailaddress = false").setHint(
				"org.hibernate.readOnly", Boolean.TRUE).getSingleResult();
		return trialAlertCohortCount;
	}

	/**
	 * @return the trialEventBeginDate
	 */
	public Date getTrialEventBeginDate()
	{
		if (this.trialEventBeginDate == null)
			this.trialEventBeginDate = new Date(System.currentTimeMillis() - 7 * 24 * 60 * 60 * 1000); // one
		// week
		// ago
		// (7
		// days)
		return this.trialEventBeginDate;
	}

	/**
	 * @param trialEventBeginDate
	 *            the trialEventBeginDate to set
	 */
	public void setTrialEventBeginDate(Date newTrialEventBeginDate)
	{
		if (DATE_02_14_2010 == null)
			try
			{
				DATE_02_14_2010 = new SimpleDateFormat("MM/dd/yyyy").parse("02/14/2010");
			}
			catch (Exception pex)
			{
			}
		if (newTrialEventBeginDate.before(DATE_02_14_2010))
			newTrialEventBeginDate = DATE_02_14_2010;
		this.trialEventBeginDate = newTrialEventBeginDate;
	}

	/**
	 * @return the trialEventEndDate
	 */
	public Date getTrialEventEndDate()
	{
		if (this.trialEventEndDate == null)
			this.trialEventEndDate = new Date(System.currentTimeMillis());
		return this.trialEventEndDate;
	}

	/**
	 * @param trialEventEndDate
	 *            the trialEventEndDate to set
	 */
	public void setTrialEventEndDate(Date newTrialEventEndDate)
	{
		if (DATE_02_14_2010 == null)
			try
			{
				DATE_02_14_2010 = new SimpleDateFormat("MM/dd/yyyy").parse("02/14/2010");
			}
			catch (Exception pex)
			{
			}
		if (newTrialEventEndDate.before(DATE_02_14_2010))
			newTrialEventEndDate = DATE_02_14_2010;
		if (newTrialEventEndDate.before(this.trialEventBeginDate))
			newTrialEventEndDate = this.trialEventBeginDate;
		this.trialEventEndDate = newTrialEventEndDate;
	}

	/**
	 * This method retrieves a list of 'Close' Trial Events using the
	 * trialEventBeginDate and trialEventEndDate.
	 */
	@SuppressWarnings("unchecked")
	public List<TrialEvent> getCloseTrialEvents()
	{
		List<TrialEvent> trialEventList = em.createQuery(
				"from TrialEvent te where eventType = '" + TrialEvent.CLOSED
						+ "' and te.id.eventDate between ? and ? order by te.id.eventDate, te.primaryId").setParameter(
				1, this.getTrialEventBeginDate(), TemporalType.DATE).setParameter(2, this.getTrialEventEndDate(),
				TemporalType.DATE).setHint("org.hibernate.readOnly", Boolean.TRUE).getResultList();
		return trialEventList;
	}

	/**
	 * This method retrieves a list of 'Open' Trial Events using the
	 * trialEventBeginDate and trialEventEndDate.
	 */
	@SuppressWarnings("unchecked")
	public List<TrialEvent> getOpenTrialEvents()
	{
		List<TrialEvent> trialEventList = em.createQuery(
				"from TrialEvent te where eventType = '" + TrialEvent.OPENED
						+ "' and te.id.eventDate between ? and ? order by te.id.eventDate, te.primaryId").setParameter(
				1, this.getTrialEventBeginDate(), TemporalType.DATE).setParameter(2, this.getTrialEventEndDate(),
				TemporalType.DATE).setHint("org.hibernate.readOnly", Boolean.TRUE).getResultList();
		return trialEventList;
	}

	/**
	 * This method retrieves a list of 'Update' Trial Events using the
	 * trialEventBeginDate and trialEventEndDate.
	 */
	@SuppressWarnings("unchecked")
	public List<TrialEvent> getUpdateTrialEvents()
	{
		List<TrialEvent> trialEventList = em.createQuery(
				"from TrialEvent te where eventType = '" + TrialEvent.UPDATED
						+ "' and te.id.eventDate between ? and ? order by te.id.eventDate, te.primaryId").setParameter(
				1, this.getTrialEventBeginDate(), TemporalType.DATE).setParameter(2, this.getTrialEventEndDate(),
				TemporalType.DATE).setHint("org.hibernate.readOnly", Boolean.TRUE).getResultList();
		return trialEventList;
	}

	/**
	 * Updates the selected patient's credentials and
	 * unsuccessful-authentication-count.
	 * 
	 * @see org.quantumleaphealth.action.AdministratorInterface#updateUserPatientCredentials()
	 */
	public void updateUserPatientCredentials()
	{
		// Validate selected patient and non-null new credentials
		if ((patient == null) || (patient.getId() == null) || (credentials == null))
			return;
		try
		{
			LOGGER.debug("Resetting patient ##0's password and unsuccessful-authentication-count", patient.getId());
			patient.setCredentials(credentials);
			patient.setUnsuccessfulAuthenticationCount(0);
			int rowResults = em.createQuery(
					"UPDATE UserPatient SET credentials=:credentials,unsuccessfulAuthenticationCount=0 WHERE id=:id")
					.setParameter("id", patient.getId()).setParameter("credentials", credentials).executeUpdate();
			if (rowResults == 0)
			{
				facesMessages.add(Severity.ERROR, "Updating patient ##0's credentials did not work", patient.getId());
				LOGGER.error("Updating patient ##0's credentials did not work", patient.getId());
				return;
			}
			LOGGER.debug("Reset patient ##0's password and unsuccessful-authentication-count", patient.getId());
		}
		catch (Throwable throwable)
		{
			facesMessages.add(Severity.ERROR, "The system is down.");
			LOGGER.error("Cannot update patient ##0's credentials", throwable, patient.getId());
		}
	}

	/**
	 * Sets the selected patient's invalid email address status
	 * 
	 * @see org.quantumleaphealth.action.AdministratorInterface#updateUserPatientPrincipalInvalidEmailAddress()
	 */
	public void updateUserPatientPrincipalInvalidEmailAddress()
	{
		// Validate selected patient and valid principal
		if ((patient == null) || (patient.getId() == null) || patient.isPrincipalInvalidEmailAddress())
			return;
		try
		{
			LOGGER.debug("Setting patient ##0's principal-invalid-email-address status", patient.getId());
			patient.setPrincipalInvalidEmailAddress(true);
			int rowResults = em.createQuery("UPDATE UserPatient SET principalInvalidEmailAddress=true WHERE id=:id")
					.setParameter("id", patient.getId()).executeUpdate();
			if (rowResults == 0)
			{
				facesMessages.add(Severity.ERROR,
						"Updating patient ##0's principal-invalid-email-address did not work", patient.getId());
				LOGGER.error("Updating patient ##0's principal-invalid-email-address did not work", patient.getId());
				return;
			}
			LOGGER.debug("Set patient ##0's principal-invalid-email-address status", patient.getId());
		}
		catch (Throwable throwable)
		{
			facesMessages.add(Severity.ERROR, "The system is down.");
			LOGGER.error("Cannot set patient ##0's principal-invalid-email-address status", throwable, patient.getId());
		}
	}
	
	// BCTMatch - Added download patient history link
	public void downloadHealthHistory()
	{
		// Validate selected patient and valid principal
		if ((patient == null) || (patient.getId() == null))
			return;
		
		try {
			UserPatientDownload download = new UserPatientDownload();
			download.setPrincipal(patient.getPrincipal());
			download.setPatientHistoryMarshaled(patient.getPatientHistoryMarshaled());
			JAXBContext context = JAXBContext.newInstance(UserPatientDownload.class);
			Marshaller marshaller = context.createMarshaller();
			
			HttpServletResponse response = (HttpServletResponse)facesContext.getExternalContext().getResponse();
			response.setContentType("application/xml");
	        response.addHeader("Content-disposition", "attachment; filename=\"" + download.getPrincipal() +".xml\"");
			ServletOutputStream os = response.getOutputStream();
			marshaller.marshal(download, os);
			os.flush();
			os.close();
			facesContext.responseComplete();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Reset instance variables when stateful session bean is destroyed.
	 * 
	 * @see org.quantumleaphealth.action.AdministratorInterface#destroy()
	 */
	@Remove
	@Destroy
	public void destroy()
	{
		principal = null;
		credentials = null;
		loggedIn = false;
		patients = null;
		patient = null;
	}

	/**
	 * @param string
	 *            the string to trim
	 * @return the trimmed string or <code>null</code> if the string is
	 *         <code>null</code> or empty
	 */
	private static String trim(String string)
	{
		if (string != null)
		{
			string = string.trim();
			if (string.length() == 0)
				string = null;
		}
		return string;
	}

	@Override
	public Date getSecureConnectBeginDate()
	{
		if (this.secureConnectBeginDate == null)
		{
			this.secureConnectBeginDate = new Date(System.currentTimeMillis() - 14 * 24 * 60 * 60 * 1000); // two
			// weeks
			// ago
			// (14
		} // days)
		return secureConnectBeginDate;
	}

	@Override
	public Date getSecureConnectEndDate()
	{
		if (this.secureConnectEndDate == null)
		{
			this.secureConnectEndDate = new Date(System.currentTimeMillis());
		}
		return secureConnectEndDate;
	}

	@Override
	public void setSecureConnectBeginDate(Date secureConnectBeginDate) throws ParseException
	{
		this.secureConnectBeginDate = secureConnectBeginDate;
	}

	@Override
	public void setSecureConnectEndDate(Date secureConnectEndDate) throws ParseException
	{
		this.secureConnectEndDate = secureConnectEndDate;
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -1068766151596316287L;
}
