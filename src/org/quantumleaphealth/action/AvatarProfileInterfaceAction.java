package org.quantumleaphealth.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.validator.NotNull;
import org.hibernate.validator.Pattern;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.patient.ScreenedTrial;
import org.quantumleaphealth.model.patient.UserPatient;

@Stateful
@Name("avatarProfileInterface")
public class AvatarProfileInterfaceAction implements AvatarProfileInterface
{
	private static final String avatarNotSet = "avatarNotSet";
	
	/**
	 * Persists the entity beans using EJB3 persistence context
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;

    /**
     * Injected logging facility
     */
    @Logger
    private static Log LOGGER;

	/**** Web Parameters:  see pages.xml ****/
	private String principal;
	private Integer groupedByProximity;
	private Long postalCode;
	
	private UserPatient patient;
	
	private FacadePatientHistory facadePatientHistory;
	private AvatarHistoryConfiguration avatarHistoryConfiguration;
	
	private ArrayList<Long> avatarTrialIds            = null;
	private ArrayList<AvatarTrialGroup> avatarTrials  = null;
	
	private Date avatarTrialListLastUpdated = new Date(0L);
	
	@In (create=true)
	private TrialBrowser trialBrowser;
	
	@In
	FacesMessages facesMessages;
	
	@RequestParameter
	private String[] attributes;
	/**
	 * Force the SEAM manager to initialize the patient and facadePatientHistory variables
	 * once the AvatarProfileInterfaceAction instance has been initialized.
	 */
	public UserPatient getPatient()
	{
		if ( patient == null )
		{
			loadAvatar();
		}
		return patient;
	}
	
	private void createAvatarTrialList()
	{
	    createAvatarTrialIdsList();

		avatarTrials = new ArrayList<AvatarTrialGroup>();
		for ( TrialProximityGroup trialProximityGroup: trialBrowser.getTrials().getSortedList())
		{
			AvatarTrialGroup avatarTrialGroup = new AvatarTrialGroup( trialProximityGroup.getTrialTypeName() );
			for ( TrialProximity trialProximity: trialProximityGroup )
			{
				// find maximum trial date
				if (trialProximity.getTrial() != null)
				{	
					if (trialProximity.getTrial().getLastModified() != null && getAvatarTrialListLastUpdated().before(trialProximity.getTrial().getLastModified()))
					{
						setAvatarTrialListLastUpdated(trialProximity.getTrial().getLastModified());
					}
					
					if ( avatarTrialIds.contains( trialProximity.getTrial().getId() ))
					{	
						avatarTrialGroup.getTrials().add( trialProximity );
					}	
				}
			}
			avatarTrials.add( avatarTrialGroup );
		}
	}
	
	public ArrayList<AvatarTrialGroup> getAvatarTrialList()
	{
		return avatarTrials;
	}
	
	/**
	 * Get the bitmap created from the avatar attribute list passed in when the user clicks on one of the start forms
	 * on the index or home page.
	 * 
	 * @return The "Or'ed" bitmap of the attributes passed in.
	 */
	private Integer getAttributesBitmap()
	{
		Integer avatarAttributeBitMap = null;
		if (attributes != null)
		{
			avatarAttributeBitMap = 0;
			for (String attribute: attributes)
			{
				try
				{
					avatarAttributeBitMap = avatarAttributeBitMap | Integer.parseInt(attribute);
				}
				catch (Exception e)
				{
					LOGGER.error("Error parsing numeric value of attribute value was " + attribute);
				}
			}
		}
		return avatarAttributeBitMap;
	}
		
	/**
	 * If bean is being called as a result of the user clicking on one of the links from the index page,
	 * try to retrieve the username of the avatar associated with these characteristics.
	 * @return null if not found, otherwise the values of userpatient.principal found.
	 */
	@SuppressWarnings("unchecked")
	private String getAvatarAttributesPrincipal()
	{
		Integer avatarAttributeBitMap = getAttributesBitmap();
		if (avatarAttributeBitMap != null)
		{
			// find the corresponding avatar
			List<String> result = (List<String>) em.createQuery("select p.principal from UserPatient p where p.characteristicBitmap = :characteristicBitmap")
				.setParameter("characteristicBitmap", avatarAttributeBitMap)
				.setHint("org.hibernate.readOnly", Boolean.TRUE).getResultList();
			
			if (result != null && result.size() > 0)
			{
				return result.get(0);
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String getAvatarAttributesPrincipalMapJavaScriptString()
	{
		StringBuilder avatarListAsString = new StringBuilder();
		avatarListAsString.append("var avatarCharacteristicMap = new Array();");
		
		List<UserPatient> results = (List<UserPatient>) em.createQuery("select up from UserPatient up where up.characteristicBitmap is not null and up.characteristicBitmap != 0")
														.setHint("org.hibernate.readOnly", Boolean.TRUE)
														.getResultList();
		for (UserPatient up: results)
		{
			avatarListAsString.append("avatarCharacteristicMap[" + up.getCharacteristicBitmap() + "] = '" + up.getAvatarProfileDescription().replaceAll("[\\n\\r]", "") + "';\n");
		}
		return avatarListAsString.toString();
	}
	/**
	 * set some defaults for the avatar whose trials or patient history is to be shown.  
	 */
	private void setDefaults()
	{
		if ( groupedByProximity == null )
		{
			groupedByProximity = 1;
		}
		
		if (principal == null && attributes != null)
		{
			String principal = getAvatarAttributesPrincipal();

			if (principal == null)
			{
				setPrincipal(avatarNotSet);
			}
			else
			{
				setPrincipal(principal);
			}
		}
	}

	/**
	 * initializes the variables referenced on the avatar pages 
	 */
	private void loadAvatar()
	{
		setDefaults();
		
		if ( principal != null )
		{
			try
			{
				patient = (UserPatient) em.createQuery("select p from UserPatient p where upper(principal)=:principal")
					.setParameter("principal", getPrincipal().toUpperCase())
					.setHint("org.hibernate.readOnly", Boolean.TRUE)
					.getSingleResult();
				
				if ( patient != null && !patient.isAvatarProfile() && !patient.isReadOnlyProfile())
				{
					// only avatars are acceptable.
					patient = null;
				}
			}
			catch ( javax.persistence.NoResultException nr)
			{
				LOGGER.error("No result returned for special user with principal value of " + getPrincipal());
			}
			catch (javax.persistence.NonUniqueResultException nu)
			{
				LOGGER.error("Non-unique result returned for special user with principal value of " + getPrincipal());
			}
		}
		
		if ( patient == null )
		{
			patient = new UserPatient();
			if ( principal != null )
			{
				patient.setPrincipal(principal);
			}
		}
		else
		{
			// Force Lazy load of the screened trials
			patient.getScreenedTrials().size();
		}
		// detach the object from the persistence manager.  If we don't do this it will try and update the avatar after the class goes out of scope.
	    // create the faacePatientHistory for reference on the avatar_history_review page
	    facadePatientHistory = new FacadePatientHistory(patient.getPatientHistory());

	    // create the avatarHistoryConfiguration for reference on the avatar_history_review page
	    avatarHistoryConfiguration = new AvatarHistoryConfiguration(patient.getAvatarHistory());
	    
		if (groupedByProximity == 2)
		{
			// Always sort by category.			
//			trialBrowser.getTrials().setGroupedByProximity(groupedByProximity);
			trialBrowser.setPostalCode(postalCode);
		}
		trialBrowser.getTrials().setGroupedByProximityBoolean(false);

	    createAvatarTrialList();
	}
	
	private void createAvatarTrialIdsList()
	{
		if ( patient != null )
		{
			avatarTrialIds = new ArrayList<Long>();
			for ( ScreenedTrial screenedTrial: patient.getScreenedTrials() )
			{
				avatarTrialIds.add( screenedTrial.getTrialId() );
			}
		}
	}
	  
	/**
	 * @return the patient's characteristics, guaranteed to be non-
	 *         <code>null</code>.
	 * @see org.quantumleaphealth.action.PatientInterface#getHistory()
	 */
	@Override
	public FacadePatientHistory getHistory()
	{
		if ( facadePatientHistory == null )
		{	
			loadAvatar();
		}	
		return facadePatientHistory; 
	}

	/**
	 * @return the avatarHistoryConfiguration
	 */
	public AvatarHistoryConfiguration getAvatarHistoryConfiguration() 
	{
		if (avatarHistoryConfiguration == null)
		{
			loadAvatar();
		}
		return avatarHistoryConfiguration;
	}

	/**
	 * @param principal the principal to set
	 */
	@Override
	public void setPrincipal(String principal) 
	{
		this.principal = principal;
	}

	/**
	 * @return the principal
	 */
	@Override
	public String getPrincipal() 
	{
		return principal;
	}

	@Override
	@NotNull(message="The zip field is required if sort by zip code is selected.")
	@Pattern(regex="^\\d{5}$", message="A zip code must consist of exactly five numbers.")
	public void setPostalCode( Long postalCode )
	{
		this.postalCode = postalCode;
	}

	@Override
	public Long getPostalCode()
	{
		return postalCode;
	}

	@Override
	public void setGroupedByProximity( int groupedByProximity )
	{
		this.groupedByProximity = groupedByProximity;
	}

	@Override
	public int getGroupedByProximity()
	{
		return groupedByProximity == null? 1: groupedByProximity ;
	}

	/**
	 * @param avatarTrialListLastUpdated the avatarTrialListLastUpdated to set
	 */
	@Override
	public void setAvatarTrialListLastUpdated(Date avatarTrialListLastUpdated) 
	{
		this.avatarTrialListLastUpdated = avatarTrialListLastUpdated;
	}

	/**
	 * @return the avatarTrialListLastUpdated
	 */
	@Override
	public Date getAvatarTrialListLastUpdated() 
	{
		return avatarTrialListLastUpdated;
	}

	/**
	 */
	@Remove
	@Destroy
	@Override
	public void destroy()
	{
	}
}
