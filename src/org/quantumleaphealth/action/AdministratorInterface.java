/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;
import javax.naming.NamingException;

import org.quantumleaphealth.model.patient.SecureConnect_View;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.TrialEvent;
import org.quartz.SchedulerException;
/**
 * Navigates an administrator through viewing patient-trial screening profiles
 * and resetting a patient's credentials. This interface defines the following
 * roles for a local stateful session Enterprise JavaBean (EJB):
 * <ol>
 * <li>Provide authentication of an administrator as a privileged screener.</li>
 * <li>Provide eligibility profiles for a subset of patients</li>
 * <li>Provide password reset for patient</li>
 * </ol>
 * 
 * @author Tom Bechtold
 * @version 2008-11-23
 */
@Local
public interface AdministratorInterface
{
	/**
	 * @return the principal or <code>null</code> if not set
	 */
	public String getPrincipal();

	/**
	 * @param principal
	 *            the principal or <code>null</code> if not set
	 */
	public void setPrincipal(String principal);

	/**
	 * @return the credentials or <code>null</code> if not set
	 */
	public String getCredentials();

	/**
	 * @param credentials
	 *            the credentials or <code>null</code> if not set
	 */
	public void setCredentials(String credentials);

	/**
	 * Authenticates the administrator as a privileged screener
	 */
	public void login();

	/**
	 * Logs out the user and invalidates the session
	 */
	public void logout();

	/**
	 * @return whether or not the administrator is authenticated
	 */
	public boolean isLoggedIn();

	/**
	 * @return the list of patients sorted by principal or <code>null</code> if
	 *         no patients match the search
	 */
	public List<UserPatient> getPatients();

	/**
	 * @param userPatientId
	 *            the unique identifier of the selected patient or
	 *            <code>null</code> if none selected
	 */
	public void setUserPatientId(Long userPatientId);

	/**
	 * @return the selected patient or <code>null</code> if none selected
	 */
	public UserPatient getPatient();

	/**
	 * @return a count of SecureConnect invitation this year
	 */
	public Long getSecureConnectCountThisYear();

	/**
	 * Factory method for the secureConnectTrial List.
	 */
	public void createSecureConnectTrialsList ();
	public void delete ();

	/**
	 * @return a count of SecureConnect invitation total
	 */
	public Long getSecureConnectCountTotal();

	/**
	 * @return The number of user patients with trial alerts enabled
	 */
	public Long getTrialAlertCohortCount();

	/**
	 * @return the trialEventBeginDate
	 */
	public Date getTrialEventBeginDate();

	/**
	 * @param trialEventBeginDate
	 *            the trialEventBeginDate to set
	 */
	public void setTrialEventBeginDate(Date newTrialEventBeginDate) throws ParseException;

	/**
	 * @return the trialEventEndDate
	 */
	public Date getTrialEventEndDate();

	/**
	 * @param trialEventEndDate
	 *            the trialEventEndDate to set
	 */
	public void setTrialEventEndDate(Date newTrialEventEndDate) throws ParseException;

	/**
	 * @return the secureConnectBeginDate
	 */
	public Date getSecureConnectBeginDate();

	/**
	 * @param secureConnectBeginDate
	 *            the secureConnectBeginDate to set
	 */
	public void setSecureConnectBeginDate(Date newTrialEventBeginDate) throws ParseException;

	/**
	 * @return the secureConnectEndDate
	 */
	public Date getSecureConnectEndDate();

	/**
	 * @param secureConnectEndDate
	 *            the secureConnectEndDate to set
	 */
	public void setSecureConnectEndDate(Date newTrialEventEndDate) throws ParseException;

	/**
	 * This method retrieves a list of 'Close' Trial Events.
	 */
	public List<TrialEvent> getCloseTrialEvents();

	/**
	 * This method retrieves a list of 'Open' Trial Events.
	 */
	public List<TrialEvent> getOpenTrialEvents();

	/**
	 * This method retrieves a list of 'Update' Trial Events.
	 */
	public List<TrialEvent> getUpdateTrialEvents();

	/**
	 * Updates the selected patient's credentials and
	 * unsuccessful-authentication-count
	 */
	public void updateUserPatientCredentials();

	/**
	 * Sets the selected patient's invalid email address status
	 */
	public void updateUserPatientPrincipalInvalidEmailAddress();
	
	public void downloadHealthHistory();
	
	/**
	 * Removes the stateful session bean from memory. This method is required by
	 * the EJB3 specification.
	 */
	public void destroy();
}
