/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;
import java.util.ArrayList;

import org.quantumleaphealth.model.trial.PatientTrial;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.Trial.Type;

/**
 * How close trials' sites are for trials of the same type.
 * 
 * @author Tom Bechtold
 * @version 2008-12-10
 */
public class TrialProximityGroup extends ArrayList<TrialProximity> implements Comparable<TrialProximityGroup>,
		Serializable
{
	/**
	 * The trials' type or <code>null</code> for all types
	 */
	private final Type trialType;

	private Boolean hasFavorites = false;
	
	/**
	 * Store the parameter
	 * 
	 * @param trialType
	 *            the trials' type or <code>null</code> for all types
	 */
	TrialProximityGroup(Type trialType)
	{
		super();
		this.trialType = trialType;
	}

	/**
	 * @return the trials' type or <code>null</code> for all types
	 */
	public Type getTrialType()
	{
		return trialType;
	}

	/**
	 * @return the trials' type name or other for all types
	 */
	public String getTrialTypeName()
	{
		return trialType == null ? "label.type.other" : "label.type." + trialType.name();
	}

	/**
	 * @param hasFavorites the hasFavorites to set
	 */
	public void setHasFavorites(Boolean hasFavorites) 
	{
		this.hasFavorites = hasFavorites;
	}

	/**
	 * @return the hasFavorites
	 */
	public Boolean isHasFavorites() 
	{
		return hasFavorites;
	}

	/**
	 * Appends the element to the group if its trial's type matches or if
	 * <code>trialType</code> is <code>null</code>.
	 * 
	 * @param trialProximity
	 *            the trial proximity to add to the list
	 * @return whether the trial proximity was added to the list
	 * @see java.util.ArrayList#add(java.lang.Object)
	 */
	@Override
	public boolean add(TrialProximity trialProximity)
	{
		boolean addSuccess = (trialType == null) || trialProximity.getTrial().isInCategory( trialType.getWebCategory() ) ? super.add(trialProximity) : false;
		if (addSuccess && trialProximity.getTrial() instanceof PatientTrial)
		{
			PatientTrial patientTrial = (PatientTrial) trialProximity.getTrial();
			if (patientTrial.getFavorite())
			{
				this.setHasFavorites(true);
			}
		}
		return addSuccess;
	}

	/**
	 * Compare this group's type using English alphabetical ordering or, if
	 * alphabetically equal, by greater number of trials.
	 * 
	 * @return a negative integer if this group's type is alphabetically
	 *         (English) less than the other group's type, the other group's
	 *         type is not known, or (if types are alphabetically equal) this
	 *         group has more trials than the other group; 0 if this group's
	 *         type is alphabetically (English) the same as the other group's
	 *         type and both groups have the same number of trials; a positive
	 *         integer otherwise
	 * @param trialProximityGroup
	 *            the group to compare to this one
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(TrialProximityGroup trialProximityGroup)
	{
		// Unavailable group is always greater than available one
		if (trialProximityGroup == null)
			return -1;
		// Compare types alphabetically first; if equal then compare larger
		// number of trials
		int typeCompare = Trial.TRIALTYPE_COMPARATOR_ENGLISH.compare(trialType, trialProximityGroup.trialType);
		return (typeCompare != 0) ? typeCompare : (trialProximityGroup.size() - size());
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = 4137892366782161816L;
}
