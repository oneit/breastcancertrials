package org.quantumleaphealth.action;

import javax.ejb.Local;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

/**
 * interface for EJB3 for the EmailLinkMessageAction class.  Please see
 * {@link  EmailLinkMessageAction  the EmailLinkMessageAction class} 
 * for comments
 * @author wgweis
 *
 */
@Local
public interface EmailLinkMessage 
{
	public String getBody();

	public void setBody(String body);

	public void setFromTitleName(String fromTitleName); 

	public String getFromTitleName();
	
	public void validateToList(FacesContext facesContext, UIComponent component, Object value);
	
	public void setTo(String to);

	public String getTo();

	public String getCategoryString() ;

    public String getCategoryBundleValue( String key );

	public String getPrincipal();
	
	/**
	 * Needed by EJB3
	 */
	public void destroy();
}
