/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;

/**
 * Represents a facade into a holder of a binary value. This serializable
 * JavaBean exposes the <code>booleanValue</code> and <code>byteValue</code>
 * field to the GUI. This class supports representing the boolean value using a
 * numeric value in order to capture whether the question regarding the
 * characteristic has ever been answered (usually with a radio buttons "Yes" and
 * "No"). The applicable values for the numeric representation are:
 * <ul>
 * <li>0: Question has never been answered (e.g., "fresh")</li>
 * <li>2: boolean value is <code>false</code></li>
 * <li>4: boolean value is <code>true</code></li>
 * </ul>
 * It caches the byte value in order to reduce effort querying the holder.
 * 
 * @author Tom Bechtold
 * @version 2008-06-30
 */
public abstract class FacadeBinary implements Serializable
{
	/**
	 * Caches the presence of the characteristic in the holder.
	 */
	private boolean cachedValue = false;
	/**
	 * Whether or not the question has been answered
	 */
	private boolean fresh = true;

	/**
	 * @return the underlying value from the holder
	 */
	protected abstract boolean getValue();

	/**
	 * @param value
	 *            the underlying value to set in the holder
	 */
	protected abstract void setValue(boolean value);

	/**
	 * Updates the cached value and freshness
	 * 
	 * @param fresh
	 *            whether or not the question has been answered
	 */
	protected void update(boolean fresh)
	{
		cachedValue = getValue();
		// Overwrite freshness if characteristic has already been set
		this.fresh = cachedValue ? false : fresh;
	}

	/**
	 * @return whether or not the question has been answered
	 */
	public boolean isFresh()
	{
		return fresh;
	}

	/**
	 * Returns whether the characteristic exists in the holder. This method
	 * simply returns the cached value.
	 * 
	 * @return whether the characteristic exists in the holder
	 */
	public boolean isBooleanValue()
	{
		return cachedValue;
	}

	/**
	 * Add or remove the characteristic into/from the holder and update
	 * freshness. This method does nothing if the cached value is not changed.
	 * Calling this method implies that the question regarding the
	 * characteristic has been answered one way or another.
	 * 
	 * @param value
	 *            if <code>true</code> then add the characteristic; otherwise
	 *            remove it
	 */
	public void setBooleanValue(boolean value)
	{
		// Question has been answered, so it is not fresh anymore
		if (fresh)
			fresh = false;
		// Only update if cached value differs
		if (cachedValue == value)
			return;
		setValue(value);
		cachedValue = value;
	}

	/**
	 * Returns whether the characteristic exists in the holder. This method
	 * returns 0 if the characteristic is fresh, that is, its question has never
	 * been answered. Otherwise it converts the cached value to a number where
	 * bit #1 represents <code>false</code> and bit #2 represents
	 * <code>true</code>.
	 * 
	 * @return if <code>fresh</code> then 0; if cached then 4; otherwise 2
	 */
	public byte getByteValue()
	{
		return (byte) (fresh ? 0 : (cachedValue ? 4 : 2));
	}

	/**
	 * Add or remove the characteristic into/from the holder. This method does
	 * nothing if the question was not answered (e.g., it is fresh) or if the
	 * cached value is not changed.
	 * 
	 * @param value
	 *            if <code>0</code> then not answered; if <code>4</code> then
	 *            add the characteristic; otherwise remove it
	 */
	public void setByteValue(byte value)
	{
		// Zero value means not the question is not answered; ignore
		if (value > 0)
			setBooleanValue(value == 4);
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -25592943589371480L;
}
