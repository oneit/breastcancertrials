/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.ListIterator;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.patient.Invitation;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.ScreenedTrial;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.TrialSite;
import org.quantumleaphealth.screen.MatchingEngine;
import org.quantumleaphealth.screen.PatientHistoryMatchingIterator;

/**
 * Updates the associations of a patient.
 * 
 * @author Tom Bechtold
 * @version 2009-07-10
 */
@Stateless
@Name("patientAssociationManager")
public class PatientAssociationManagerAction implements PatientAssociationManager, Serializable
{
	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = -8697999935781398287L;
	
	/**
	 * Loads the patient data using EJB3 persistence manager
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;
	/**
	 * The matching engine
	 */
	@In
	private MatchingEngine engine;
	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log logger;

	/**
	 * Updates a patient's matching trials. This method synchronizes four lists:
	 * <ol>
	 * <li>The list of trials that match the patient's history</li>
	 * <li>The list of trials persisted in the database</li>
	 * <li>The list of trials in the sorted list that will be displayed</li>
	 * <li>The list of invitations are pruned for any whose trial and site is
	 * not in the screened list</li>
	 * </ol>
	 * This method behaves differently upon patient history completion status.
	 * If the patient's history is incomplete then all screened trials and
	 * invitations are removed and the method exits.
	 * <p>
	 * This method executes quickly since it uses the cached matching engine,
	 * therefore all available trials are run against the patient's history. If
	 * this full-scan implementation proves unscalable then each trial's last
	 * update date can be compared to the patient's last update date to
	 * determine whether to run the matching algorithm. This method removes any
	 * persisted trials that do not match anymore. It also updates the patient's
	 * <code>screenedTrialLastModified</code> property if the persisted list has
	 * been updated. Finally it calls <code>updateInvitations()</code> to remove
	 * obsolete and attach available invitations.
	 * <p>
	 * Because this method accesses both the <code>screenedTrial</code> and
	 * <code>invitations</code> associations, eagerly fetching these
	 * associations before calling this method is not necessary. However, the
	 * current <code>patient</code> object must be attached to the persistence
	 * engine.
	 * </p>
	 * 
	 * @param patient
	 *            the patient
	 * @param trials
	 *            the list of trials to update or <code>null</code> if not
	 *            available
	 * @param updated
	 *            the date/time of the update or <code>null</code> for now
	 * @param patientHistoryComplete
	 *            whether or not the patient's history is complete
	 * @param informed
	 *            whether or not the patient is informed of the update
	 * @throws IllegalArgumentException
	 *             if <code>patient</code> is <code>null</code>
	 * @see org.quantumleaphealth.action.PatientAssociationManager#updateScreenedTrialsAndInvitations(org.quantumleaphealth.model.patient.UserPatient,
	 *      org.quantumleaphealth.action.TrialProximityGroups, java.util.Date,
	 *      boolean, boolean)
	 */
	public void updateScreenedTrialsAndInvitations(UserPatient patient, TrialProximityGroups trials, Date updated,
			boolean patientHistoryComplete, boolean informed) throws IllegalArgumentException
	{
		// Validate parameter
		if (patient == null)
			throw new IllegalArgumentException("no patient");
		if (updated == null)
			updated = new Date();
		Long uniqueId = patient.getUniqueId();
		if (patient.isPatientHistoryComplete() != patientHistoryComplete)
		{
			patient.setPatientHistoryComplete(patientHistoryComplete);
			logger.debug("History of ##0 is now #1", uniqueId, patientHistoryComplete ? "complete" : "incomplete");
		}

		// Avatar bct version 2.0
		if ( patient.isAvatarProfile() )
		{
			patient.getPatientHistory().setUserType( UserPatient.UserType.AVATAR_PROFILE );
		}
		// If history is incomplete then remove any matches and invitations
		if (!patientHistoryComplete)
		{
			Iterator<ScreenedTrial> screenedTrials = patient.getScreenedTrials().iterator();
			int removedScreenedTrials = 0;
			while (screenedTrials.hasNext())
			{
				ScreenedTrial screenedTrial = screenedTrials.next();
				screenedTrials.remove();
				// JPA does not remove orphaned record so we have to do it
				// manually
				if (screenedTrial.getId() != null)
					em.remove(screenedTrial);
				removedScreenedTrials++;
			}
			if (removedScreenedTrials > 0)
			{
				if (trials != null)
					trials.clear();
				patient.setScreenedTrialsLastModified(updated);
				logger.debug("Removed #0 trials from incomplete history of ##1", removedScreenedTrials, uniqueId);
			}
			Iterator<Invitation> invitations = patient.getInvitations().iterator();
			int removedInvitations = 0;
			while (invitations.hasNext())
			{
				Invitation invitation = invitations.next();
				invitations.remove();
				// JPA does not remove orphaned record so we have to do it
				// manually
				if (invitation.getId() != null)
					em.remove(invitation);
				removedInvitations++;
			}
			if (removedInvitations > 0)
				logger.debug("Removed #0 invitations from incomplete history of ##1", removedInvitations, uniqueId);
			return;
		}

		// Merge the matching trials into the persisted list since each list is
		// sorted by identifier
		if (trials != null)
			trials.clear();
		ListIterator<ScreenedTrial> screenedTrialIterator = patient.getScreenedTrials().listIterator();
		PatientHistoryMatchingIterator matchingIterator = engine.getMatchingIterator(patient.getPatientHistory(), patient.getAvatarHistory());
		ScreenedTrial screenedTrial = screenedTrialIterator.hasNext() ? screenedTrialIterator.next() : null;
		int removedCount = 0;
		int addedCount = 0;
		while (true)
		{
			// Add the matching trial to the sorted list first
			Trial matchingTrial = matchingIterator.nextMatch();
			if ((matchingTrial == null) || (matchingTrial.getId() == null))
				break;
			if (trials != null)
				if (screenedTrial != null)
					trials.addPatientTrial(matchingTrial, screenedTrial.isNotInterested(), screenedTrial.isFavorite());
				else
					trials.addPatientTrial(matchingTrial, false, false);
			// Each trial in the persisted list whose id is unavailable or less
			// than the matched one's is obsolete
			while ((screenedTrial != null)
					&& ((screenedTrial.getTrialId() == null) || (screenedTrial.getTrialId().longValue() < matchingTrial
							.getId().longValue())))
			{
				logger.debug("Patient ##0 unmatched to trial ##1", uniqueId, screenedTrial.getTrialId());
				screenedTrialIterator.remove();
				// Since JPA does not remove orphans, we have to manually
				if (screenedTrial.getId() != null)
					em.remove(screenedTrial);
				removedCount++;
				screenedTrial = screenedTrialIterator.hasNext() ? screenedTrialIterator.next() : null;
			}
			// If at end of the persisted list then add matched trial
			if (screenedTrial == null)
			{
				ScreenedTrial newScreenedTrial = new ScreenedTrial(matchingTrial);
				if (informed)
					newScreenedTrial.setInformed(updated);
				screenedTrialIterator.add(newScreenedTrial);
				addedCount++;
				logger.debug("Patient ##0 matched to #1", uniqueId, matchingTrial);
				continue;
			}
			// If the matched trial is already in the list then verify its dates
			if (screenedTrial.getTrialId().equals(matchingTrial.getId()))
			{
				// Update last modified date of the match
				if (screenedTrial.getLastModified() == null ? (matchingTrial.getLastModified() != null) : screenedTrial
						.getLastModified().equals(matchingTrial.getLastModified()))
					screenedTrial.setTrial(matchingTrial);
				// The user is now informed of the previously-matched trial
				// since he is online
				if (informed && (screenedTrial.getInformed() == null))
					screenedTrial.setInformed(updated);
				screenedTrial = screenedTrialIterator.hasNext() ? screenedTrialIterator.next() : null;
				continue;
			}
			// If we have reached this far, the matched trial is before the
			// current one in the sorted list;
			// Insert the matched trial before the current one
			screenedTrialIterator.previous();
			ScreenedTrial newScreenedTrial = new ScreenedTrial(matchingTrial);
			if (informed)
				newScreenedTrial.setInformed(updated);
			screenedTrialIterator.add(newScreenedTrial);
			addedCount++;
			screenedTrialIterator.next();
			logger.debug("Patient ##0 matched to #1", uniqueId, matchingTrial);
		}
		// All of the rest of the trials in the screened list are obsolete since
		// there are no more matching trials
		while (screenedTrial != null)
		{
			logger.debug("Patient ##0 unmatched to trial ##1", uniqueId, screenedTrial.getTrialId());
			screenedTrialIterator.remove();
			// Since JPA does not remove orphans, we have to manually
			if (screenedTrial.getId() != null)
				em.remove(screenedTrial);
			removedCount++;
			screenedTrial = screenedTrialIterator.hasNext() ? screenedTrialIterator.next() : null;
		}
		// Update the date if the list has changed
		if ((addedCount + removedCount) > 0)
		{
			patient.setScreenedTrialsLastModified(updated);
			logger.debug("Patient ##0 matched to #1 more and #2 less trials", uniqueId, addedCount, removedCount);
		}
		// Remove obsolete invitations
		updateInvitations(patient);
	}

	/**
	 * Removes a patient's obsolete invitations. This method removes invitations
	 * whose trial is not in the patient's list of screened trials or whose site
	 * is not in the trial's list or whose site is not a message recipient. It
	 * also attaches the transient <code>Invitation.trialSite</code> property.
	 * This method must only be called while the patient's entity and
	 * associations are managed by the persistent context.
	 * 
	 * @param patient
	 *            the patient
	 * @throws IllegalArgumentException
	 *             if <code>patient</code> is <code>null</code>
	 * @see org.quantumleaphealth.action.PatientAssociationManager#updateInvitations(org.quantumleaphealth.model.patient.UserPatient)
	 */
	public void updateInvitations(UserPatient patient) throws IllegalArgumentException
	{
		// Validate parameter
		if (patient == null)
			throw new IllegalArgumentException("no patient");
		Iterator<Invitation> invitations = patient.getInvitations().iterator();
		while (invitations.hasNext())
		{
			// Remove and log invalid invitations
			Invitation invitation = invitations.next();
			if (invitation == null)
			{
				logger.warn("Patient ##0 uninvited to null invitation", patient.getUniqueId());
				invitations.remove();
				continue;
			}
			if ((invitation.getSiteId() == null) || (invitation.getTrialId() == null))
			{
				logger.warn("Patient ##0 uninvited to unspecified invitation ##1", patient.getUniqueId(), invitation
						.getId());
				invitations.remove();
				// JPA does not automatically remove orphaned associations
				if (invitation.getId() != null)
					em.remove(invitation);
				continue;
			}
			// Remove obsolete invitation if trial does not match anymore
			Trial trial = new Trial();
			trial.setId(invitation.getTrialId());
			int index = Collections.binarySearch(patient.getScreenedTrials(), new ScreenedTrial(trial));
			if (index < 0)
			{
				logger.debug("Patient ##0 uninvited to unmatched trial ##1", patient.getUniqueId(), invitation
						.getTrialId());
				invitations.remove();
				// JPA does not automatically remove orphaned associations
				if (invitation.getId() != null)
					em.remove(invitation);
				continue;
			}
			// Remove obsolete invitation if site is not accruing anymore
			trial = engine.getTrial(invitation.getTrialId());
			TrialSite trialSite = null;
			if (trial != null)
				for (TrialSite currentTrialSite : trial.getTrialSite())
					if ((currentTrialSite != null) && (currentTrialSite.getSite() != null)
							&& invitation.getSiteId().equals(currentTrialSite.getSite().getId()))
					{
						trialSite = currentTrialSite;
						break;
					}
			if (trialSite == null)
			{
				logger.debug("Patient ##0 uninvited to non-accruing site ##2 for trial ##1", patient.getUniqueId(),
						invitation.getTrialId(), invitation.getSiteId());
				invitations.remove();
				// JPA does not automatically remove orphaned associations
				if (invitation.getId() != null)
					em.remove(invitation);
				continue;
			}
			// Remove invitation if site is not a messaging recipient
			if (trialSite.getScreenerGroup() == null)
			{
				logger.debug("Patient ##0 uninvited to non-messaging site ##2 for trial ##1", patient.getUniqueId(),
						invitation.getTrialId(), invitation.getSiteId());
				invitations.remove();
				// JPA does not automatically remove orphaned associations
				if (invitation.getId() != null)
					em.remove(invitation);
				continue;
			}
			// Attach trialsite to transient invitation property
			if (!trialSite.equals(invitation.getTrialSite()))
				invitation.setTrialSite(trialSite);
		}
	}

	/**
	 * Updates a patient's matching trials in uninformed mode and removes any
	 * obsolete invitations. This method is run in its own transaction so
	 * changes to the patient's record are isolated from other persistence
	 * activities.
	 * 
	 * @param patientId
	 *            the patient's id
	 * @param updated
	 *            the date/time of the update or <code>null</code> for now
	 * @throws IllegalArgumentException
	 *             if <code>patientId</code> is <code>null</code> or patient is
	 *             not found
	 * @see org.quantumleaphealth.action.PatientAssociationManager#updateScreenedTrialsAndInvitations(java.lang.Long,
	 *      java.util.Date)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateScreenedTrialsAndInvitations(Long patientId, Date updated) throws IllegalArgumentException
	{
		if (patientId == null)
			throw new IllegalArgumentException("id not provided");
		// Fetch the patient's data in its own transaction
		UserPatient patient = null;
		FacadePatientHistory facadePatientHistory = new FacadePatientHistory(new PatientHistory());
		try
		{
			// Note: handling NonuniqueResultException is not necessary as
			// UserPatient.id defined as @Id
			// WARNING: Do NOT eagerly fetch the invitation association as
			// Hibernate returns duplicate screened-trial objects
			// when duplicate (erroneous) invitations exist (e.g., same
			// mechanism as the multiple-bag fetch problem)
			// We will eagerly load the screenedtrial collection since the
			// invitation collection is less used than screenedtrial
			patient = (UserPatient) em.createQuery(
					"select distinct u from UserPatient u left join fetch u.screenedTrials st where u.id=:id")
					.setParameter("id", patientId).getSingleResult();
			// The patient's history completion status is determined by the
			// facade
			// Version 2.0 avatar:  set the user type to avatar before attempting a match.
			if ( patient.isAvatarProfile() )
			{	
				patient.getPatientHistory().setUserType( UserPatient.UserType.AVATAR_PROFILE );
			}
			facadePatientHistory.setPatientHistory(patient.getPatientHistory(), false);
			// Because s/he is loaded by this bean, the patient is offline and
			// therefore uninformed about any new matches
			// AVATAR:  avatars always have complete profiles.
			updateScreenedTrialsAndInvitations(patient, null, updated, patient.isAvatarProfile() ? true: facadePatientHistory.isComplete(), false);
		}
		catch (NoResultException noResultException)
		{
			// Patient not found: recast as illegal argument
			throw new IllegalArgumentException(noResultException);
		}
	}

	/**
	 * Removes a patient's screened trials from the underlying persistence. This
	 * method is run in its own transaction so changes to the patient's record
	 * are isolated from other persistence activities.
	 * 
	 * @param patientId
	 *            the patient's id
	 * @throws IllegalArgumentException
	 *             if <code>patientId</code> is <code>null</code>
	 * @return the number of screened trials removed
	 * @see org.quantumleaphealth.action.PatientAssociationManager#removeScreenedTrials(java.lang.Long)
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public int removeScreenedTrials(Long patientId) throws IllegalArgumentException
	{
		if (patientId == null)
			throw new IllegalArgumentException("id not provided");
		return em.createNativeQuery("DELETE ScreenedTrial WHERE UserPatient_id=:id").setParameter("id", patientId)
				.executeUpdate();
	}

	/**
	 * @param em the em to set
	 */
	public void setEm(EntityManager em) {
		this.em = em;
	}

	/**
	 * @param engine the engine to set
	 */
	public void setEngine(MatchingEngine engine) {
		this.engine = engine;
	}

	/**
	 * @param logger the logger to set
	 */
	public static void setLogger(Log logger) {
		PatientAssociationManagerAction.logger = logger;
	}

}
