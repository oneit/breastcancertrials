package org.quantumleaphealth.action;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

/**
 * This interface and the corresponding implementation 
 * are used for the browse trials page replacement.
 * @author wgweis
 */
@Local
public interface NewTrialBrowser
{
	/**
	 * Setter and getter for the postal code.  Used by the search form.
	 * @param postalCode
	 */
	public void setPostalCode( Long postalCode );
	public Long getPostalCode();

	/**
	 * Setter and getter for the category string.  Set in the category links.
	 * @param categoryString
	 */
	public void setCategoryString(String categoryString);
	public String getCategoryString();

	public String getCategoryDescription();
    public String getCategoryAltText(String key);

	/**
	 * Setter and getter for the page record we will start displaying records
	 * from.  Set in the pagination links.
	 * @param pageRecord
	 */
	public void setPageRecord(Integer pageRecord);
	public Integer getPageRecord();

	/**
	 * Returns list of categories from the engine.
	 * @return
	 */
    public List<String> getTreatmentCategoryKeys();
    public List<String> getOtherCategoryKeys();

    /**
     * Return an index into the EligibilityCriteria.properties file for the google analytics key
     * mapped to the category being viewed.
     * 
     * @param key
     * @return the index into the EligibilityCriteria.properties file.  Index to 'Unknown Category' if 
     * something is wrong with the key.
     * 
     */
    public String getCategoryGoogleAnalyticsCode( String key );

    /***
     * Get a category count for a specific category or the entire trial count
     * if the key is null.
     * @param key
     * @return the record count for this category.
     */
    public Integer getCategoryCount( String key );

    /***
     * format the key in label form for a lookup from the Eligibility.properties file.
     * @param key
     * @return the label corresponding to this key
     */
    public String getCategoryLabel( String key );

    /**
     * Get a list of trials for this category or for all trials if the category is 'All' or 
     * search is by zip proximity.
     * @return the list of trials
     */
	public List<TrialProximity> getTrialList();
	
	/** pagination support **/
	/**
	 * Return the record to start the last page from..
	 */
    public Integer getLastPage();
    
    /**
     * Return a list of pages.
     * @return the list.
     */
	public ArrayList<Integer> getPagesList();
	
	/**
	 * The value of the static number of records to show per page.
	 * @return the value
	 */
	public Integer getPageRecordIncrement(); 

	/**
	 * For debugging JSF loops.
	 * @return the current loop ordinal
	 */
    public int getRowCount();

    /**
     * @return the maximum last modified date from the list of trials to be displayed. 
     */
    public Date getLastModified();
    
    /**
     * Search parameters
     */
	public void setSearchString1(String searchString1); 
	public String getSearchString1();
	
	public void setSearchOperator(String searchOperator);
	public String getSearchOperator();

	public void setSearchString2(String searchString2); 
	public String getSearchString2(); 

	/**
	 * @return a search string enclosed in quotes if it does not already have a quote as its first character.
	 * For use in the search results header on the browse_trials page.
	 */
	public 	String getFormattedSearchString();

    /**
     * BCT-693:  return count of trials modified or registered in the last two weeks.
     */
	public Integer getNewTrialCount();

    
	/**
	 * Needed by EJB3
	 */
	public void destroy();
}
