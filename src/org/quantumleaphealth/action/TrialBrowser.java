/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import javax.ejb.Local;

/**
 * Generates a list of trials sorted by type.
 * 
 * @author Tom Bechtold
 * @version 2009-03-08
 */
@Local
public interface TrialBrowser
{

	/**
	 * @return the postal code or <code>null</code> if none
	 */
	public Long getPostalCode();

	/**
	 * @param postalCode
	 *            the postal code or <code>null</code> if none
	 */
	public void setPostalCode(Long postalCode);

	/**
	 * @return the trials sorted by type and closest research site, guaranteed
	 *         to be non-<code>null</code>
	 */
	public TrialProximityGroups getTrials();

	/**
	 * @return whether or not the matching engine is reloadable
	 */
	public boolean isReloadable();

	/***
	 * @return the current value of the category string for this trial browser.
	 */
	public String getCategoryString() ;

	/**
	 * Removes the stateful session bean from memory. This method is required by
	 * the EJB3 specification.
	 */
	public void destroy();
}
