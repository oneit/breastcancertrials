/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

import org.quantumleaphealth.action.TrialSiteProximity.DistanceUnit;
import org.quantumleaphealth.model.trial.Geocoding;
import org.quantumleaphealth.model.trial.PatientTrial;
import org.quantumleaphealth.model.trial.PostalCodeGeocoding;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.xml.DecodingException;

/**
 * A list of trial proximities grouped by type.
 * 
 * @author Tom Bechtold
 * @version 2009-01-07
 */
public class TrialProximityGroups extends PostalCodeGeocoding
{
	/**
	 * Version ID for serializable class
	 */
	private static final long serialVersionUID = 79550938513945231L;
	
	/**
	 * The unit of measure to calculate distances, guaranteed to be non-
	 * <code>null</code>
	 */
	private final DistanceUnit distanceUnit;
	/**
	 * The trials, guaranteed to be non-<code>null</code>.
	 */
	private final HashSet<Trial> trials = new HashSet<Trial>();
	/**
	 * The list of trial proximities grouped by type or <code>null</code> if not
	 * generated yet.
	 */
	private List<TrialProximityGroup> trialProximityGroupList;
	
	/**
	 * A special category of New trials
	 */
	private TrialProximityGroup newTrialProximityGroup;

	/**
	 * Whether or not the trials are grouped by proximity.
	 */
	private boolean groupedByProximity;

	/**
	 * Stores parameter and trials are not grouped by proximity
	 * 
	 * @param distanceUnit
	 *            the unit of measure to calculate distances
	 * @throws IllegalArgumentException
	 *             if <code>distanceUnit</code> is <code>null</code>
	 */
	public TrialProximityGroups(DistanceUnit distanceUnit) throws IllegalArgumentException
	{
		this(distanceUnit, false);
	}

	/**
	 * Stores parameters
	 * 
	 * @param distanceUnit
	 *            the unit of measure to calculate distances
	 * @param groupedByProximity
	 *            whether or not the trials are grouped by proximity
	 * @throws IllegalArgumentException
	 *             if <code>distanceUnit</code> is <code>null</code>
	 */
	TrialProximityGroups(DistanceUnit distanceUnit, boolean groupedByProximity) throws IllegalArgumentException
	{
		if (distanceUnit == null)
			throw new IllegalArgumentException("No distance unit specified");
		this.distanceUnit = distanceUnit;
		this.groupedByProximity = groupedByProximity;
	}

	/**
	 * Removes all trials in the trials list and clears the sorted list.
	 */
	void clear()
	{
		trials.clear();
		trialProximityGroupList = null;
	}

	/**
	 * Adds a trial to the list and clears the sorted list. This method replaces
	 * any existing trial (e.g., the trial's id is already found in the list per
	 * <code>Trial.equals()</code>).
	 * 
	 * @param trial
	 *            the trial to add to the list
	 * @throws IllegalArgumentException
	 *             if <code>trial</code> or its <code>id</code> is
	 *             <code>null</code>
	 */
	public void add(Trial trial) throws IllegalArgumentException
	{
		// Validate parameter
		if (trial == null)
			throw new IllegalArgumentException("trial is null");
		if ((trial.getId() == null) || (trial.getId().longValue() == 0))
			throw new IllegalArgumentException("trial id is " + trial.getId());

		// Replace any trial with same id and clear the sorted list
		trials.add(trial);
		trialProximityGroupList = null;
	}

	/**
	 * Adds a trial to the list and clears the sorted list. This method replaces
	 * any existing trial (e.g., the trial's id is already found in the list per
	 * <code>Trial.equals()</code>).
	 * 
	 * @param trial
	 *            the trial to add to the list
	 * @param isNotInterested
	 *            the patient's indicator if they are not interested
	 * @throws IllegalArgumentException
	 *             if <code>trial</code> or its <code>id</code> is
	 *             <code>null</code>
	 */
	void addPatientTrial(Trial trial, Boolean isNotInterested, Boolean isFavorite) throws IllegalArgumentException
	{
		// Validate parameter
		if (trial == null)
			throw new IllegalArgumentException("trial is null");
		if ((trial.getId() == null) || (trial.getId().longValue() == 0))
			throw new IllegalArgumentException("trial id is " + trial.getId());

		// Replace any trial with same id and clear the sorted list
		trials.add(this.getPatientTrial(trial, isNotInterested, isFavorite));
		trialProximityGroupList = null;
	}

	/**
	 * This method returns a PatientTrial given a Trial.
	 * 
	 * @param trial
	 *            The trial to base a patient trial on.
	 * @return The patient trial.
	 */
	private PatientTrial getPatientTrial(Trial trial, Boolean isNotInterested, Boolean isFavorite)
	{
		PatientTrial patientTrial = new PatientTrial();
		patientTrial.setId(trial.getId());
		patientTrial.setPrimaryID(trial.getPrimaryID());
		patientTrial.setClinicalTrialID(trial.getClinicalTrialID());
		patientTrial.setType(trial.getType());
		patientTrial.setPhase(trial.getPhase());
		patientTrial.setRegistry(trial.getRegistry());
		patientTrial.setSponsor(trial.getSponsor());
		patientTrial.setOpen(trial.isOpen());
		patientTrial.setListed(trial.isListed());
		patientTrial.setName(trial.getName());
		patientTrial.setTitle(trial.getTitle());
		patientTrial.setPurpose(trial.getPurpose());
		patientTrial.setBurden(trial.getBurden());
		patientTrial.setTreatmentPlan(trial.getTreatmentPlan());
		patientTrial.setProcedures(trial.getProcedures());
		patientTrial.setDuration(trial.getDuration());
		patientTrial.setFollowup(trial.getFollowup());
		patientTrial.setEligibilityCriteriaLastModified(trial.getEligibilityCriteriaLastModified());
		patientTrial.setTrialSite(trial.getTrialSite());
		patientTrial.setLastBatchMatched(trial.getLastBatchMatched());
		patientTrial.setLastModified(trial.getLastModified());
		patientTrial.setLastRegistered(trial.getLastRegistered());
		patientTrial.setPostDate(trial.getPostDate());
		try
		{
			patientTrial.setLinkMarshaled(trial.getLinkMarshaled());
		}
		catch (DecodingException e)
		{
			e.printStackTrace();
		}
		patientTrial.setNotInterested(isNotInterested);

		// BCT-432: indicates whether this trial should appear in the favorites
		// list.
		patientTrial.setFavorite(isFavorite);
		return patientTrial;
	}

	/**
	 * @param trialId
	 *            the identifier of the screened trial
	 * @return the screened trial matching the identifier or <code>null</code>
	 *         if no screened trial found that matches the identifier
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	Trial get(Long trialId) throws IllegalArgumentException
	{
		if (trialId == null)
			throw new IllegalArgumentException("id not specified");
		for (Trial trial : trials)
			if ((trial != null) && trialId.equals(trial.getId()))
				return trial;
		return null;
	}

	/**
	 * @param postalCode
	 *            the postalCode to set
	 * @param geocoding
	 *            the geocoding to set
	 * @throws IllegalArgumentException
	 *             if <code>postalCode</code> is not positive or if
	 *             <code>geocoding</code> is <code>null</code>
	 */
	public void setPostalCodeGeocoding(int postalCode, Geocoding geocoding) throws IllegalArgumentException
	{
		// BCT-909: Allow zero, e.g. foreign address
		// if (postalCode <= 0)
		if (postalCode < 0)
			throw new IllegalArgumentException("postalCode not positive: " + postalCode);
		if (geocoding == null)
			throw new IllegalArgumentException("geocoding not provided");
		super.setPostalCode(postalCode);
		super.setGeocoding(geocoding);
		// Clear the sorted list
		trialProximityGroupList = null;
	}

	/**
	 * Do not allow setting postal code without geocoding
	 * 
	 * @param postalCode
	 *            ignored
	 * @throws IllegalStateException
	 *             always
	 * @see org.quantumleaphealth.model.trial.PostalCodeGeocoding#setPostalCode(int)
	 */
	@Override
	public void setPostalCode(int postalCode) throws IllegalStateException
	{
		throw new IllegalStateException("Cannot set postalCode independently of geocoding");
	}

	/**
	 * Do not allow setting geocoding without postal code
	 * 
	 * @param geocoding
	 *            ignored
	 * @throws IllegalStateException
	 *             always
	 * @see org.quantumleaphealth.model.trial.PostalCodeGeocoding#setGeocoding(org.quantumleaphealth.model.trial.Geocoding)
	 */
	@Override
	public void setGeocoding(Geocoding geocoding)
	{
		throw new IllegalStateException("Cannot set geocoding independently of postalCode");
	}

	/**
	 * @return 1 if not grouped by proximity, 2 if grouped by proximity
	 */
	public int getGroupedByProximity()
	{
		return groupedByProximity ? 2 : 1;
	}

	/**
	 * Stores grouped-by-proximity preference and clears the list of sorted
	 * trials. This method does nothing if the parameter has not changed.
	 * 
	 * @param groupedByProximity
	 *            1 if not grouped by proximity, 2 if grouped by proximity
	 */
	public void setGroupedByProximity(int groupedByProximity)
	{
		// Do nothing if not changed
		if (this.groupedByProximity == (groupedByProximity == 2))
			return;
		// Cache preference and clear the sorted trials so they will be
		// regenerated
		this.groupedByProximity = (groupedByProximity == 2);
		trialProximityGroupList = null;
	}

	/**
	 * Stores grouped-by-proximity preference and clears the list of sorted
	 * trials. This method does nothing if the parameter has not changed.
	 * 
	 * @param groupedByProximity
	 *            whether or not the list is grouped by proximity
	 * @see #setGroupedByProximity(int)
	 */
	public void setGroupedByProximityBoolean(boolean groupedByProximity)
	{
		setGroupedByProximity(groupedByProximity ? 2 : 1);
	}

	/**
	 * Determine if the trial can be included in the NEW category.
	 * @param trial
	 * @param nowCalendar
	 * @return true if the postDate is not null and less than two weeks old.
	 */
	public static boolean isNewTrial(Trial trial, Calendar nowCalendar)
	{
		if (trial == null || trial.getPostDate() == null)
		{
			return false;
		}
		Calendar lastPostedCalendar = Calendar.getInstance();
		lastPostedCalendar.setTime(trial.getPostDate());
		
		lastPostedCalendar.add(Calendar.DAY_OF_YEAR, 14);
		
		return lastPostedCalendar.after(nowCalendar);
	}
	/**
	 * Create a list of trials grouped by type. This method also
	 * sorts each group in the returned list; the caller is responsible for
	 * sorting the list of groups.
	 * 
	 * @return a list of trial proximities grouped by type guaranteed to be non-
	 *         <code>null</code>
	 */
	/**
	 * @return the newTrialList
	 */
	public TrialProximityGroup getNewTrialProximityGroup()
	{
		// Build on-demand
		if (newTrialProximityGroup == null)
		{
			// Return empty list if no trials
			newTrialProximityGroup = new TrialProximityGroup(null);
			Calendar nowCalendar = Calendar.getInstance();
			
			if (trials.isEmpty())
			{	
				return newTrialProximityGroup;
			}	

			for (Trial trial : trials)
			{
				TrialProximity trialProximity = new TrialProximity(trial, getGeocoding(), distanceUnit);
				if (isNewTrial(trial, nowCalendar) && !newTrialProximityGroup.add(trialProximity))
				{	
					throw new IllegalStateException("Could not add " + trial + " to null proximity group " + newTrialProximityGroup);
				}	
			}
			// Add the null group if there are trials in it
			
			// Sort each group by its elements' natural ordering and then the
			// list of groups by their natural ordering
			if (newTrialProximityGroup.size() > 1)
			{
				Collections.sort(newTrialProximityGroup);
			}
			
		}
		return newTrialProximityGroup;
	}

	/**
	 * Create a list of trial proximities grouped by type. This method also
	 * sorts each group in the returned list; the caller is responsible for
	 * sorting the list of groups.
	 * 
	 * @return a list of trial proximities grouped by type guaranteed to be non-
	 *         <code>null</code>
	 */
	public List<TrialProximityGroup> getSortedList()
	{
		// Build on-demand
		if (trialProximityGroupList == null)
		{
			// Return empty list if no trials
			trialProximityGroupList = new LinkedList<TrialProximityGroup>();
			if (trials.isEmpty())
				return trialProximityGroupList;

			// Null group holds trials of no group or if grouped by proximity
			TrialProximityGroup nullGroup = new TrialProximityGroup(null);
			trialLoop: for (Trial trial : trials)
			{
				TrialProximity trialProximity = new TrialProximity(trial, getGeocoding(), distanceUnit);
				// Add it to the null group if we are not grouping by type and the zip code has been defined., or
				// if its type is really null
// BCT-915				
//				if ( ( groupedByProximity && this.getPostalCode() > 0 ) || (trial.getType() == null))
				if ( ( groupedByProximity) || (trial.getType() == null))
				{
					if (!nullGroup.add(trialProximity))
						throw new IllegalStateException("Could not add " + trial + " to null proximity group "
								+ nullGroup);
					continue;
				}
				// Add it to an existing group that matches its type
				for (TrialProximityGroup trialProximityGroup : trialProximityGroupList)
				{	
					if (trialProximityGroup.add(trialProximity))
					{
						continue trialLoop;
					}	
				}
				// Add it to a new group for its type
				TrialProximityGroup trialProximityGroup = new TrialProximityGroup(trial.getType());
				trialProximityGroupList.add(trialProximityGroup);
				if (!trialProximityGroup.add(trialProximity))
				{	
					throw new IllegalStateException("Could not add " + trial.getPrimaryID() + " to new proximity group "
							+ trialProximityGroup);
				}	
			}
			// Add the null group if there are trials in it
			if (!nullGroup.isEmpty())
				trialProximityGroupList.add(nullGroup);
			// Sort each group by its elements' natural ordering and then the
			// list of groups by their natural ordering
			for (TrialProximityGroup trialProximityGroup : trialProximityGroupList)
				if (trialProximityGroup.size() > 1)
					Collections.sort(trialProximityGroup);
			if (trialProximityGroupList.size() > 1)
				Collections.sort(trialProximityGroupList);
		}
		return trialProximityGroupList;
	}

	/**
	 * @return the number of sorted trial lists
	 */
	public int getGroupCount()
	{
		return getSortedList().size();
	}
}
