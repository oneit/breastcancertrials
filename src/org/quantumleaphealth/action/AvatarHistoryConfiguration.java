package org.quantumleaphealth.action;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANATOMIC_SITE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AVATAR_DIAGNOSISAREA_NONE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AVATAR_DIAGNOSISAREA_OTHER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AVATAR_NOLOCAL_SPREAD_DISEASE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AVATAR_DIAGNOSISAREA_BRAIN_SPINAL;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.AVATAR_STAGEIII_LOCAL_SPREAD_OF_DISEASE_SET;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BIRTHDATE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREASTMETASTASIS_BRAIN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_BONE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_BRAIN;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_LIVER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.DIAGNOSISAREA_SPINALCORD;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.METASTATIC_BREAST_CANCER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PROGRESSING;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.STAGE;

import java.io.Serializable;

import org.quantumleaphealth.model.patient.AvatarHistory;
import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.YearMonthHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * facade class to accompany the AvatarHistory class.
 * This class manages all the avatar configurations displayed on the avatar tab.
 * All of the values except for stages are used to update their PatientHistory counterparts
 * prior to storing the patient history.  Stages are used for injection during matching.
 * 
 * @author wgweis
 */
public class AvatarHistoryConfiguration implements Serializable
{
	/**
	 * A reference to the parent.
	 */
	private PatientInterfaceAction manager;
	
	/**
	 * Year-month characteristics, guaranteed to be non-<code>null</code>
	 */
	private YearMonthHistory yearMonthHistory = new YearMonthHistory();
	
	/**
	 * A reference to the backing class.
	 */
	AvatarHistory avatarHistory;
	
	/**
	 * A reference to the patientHistory class we will update.
	 */
	PatientHistory patientHistory;
	
	/**
	 * facade references which will be linked to their backing values in the AvatarHistory class.
	 */
	private FacadeAttributeCharacteristic areasOfDisease;

	private FacadeAttributeCharacteristic avatarBreastStages;

	/**
	 * Three characteristic codes used in this class only.
	 */
	private static final CharacteristicCode[] AVATAR_AREAS_OF_DISEASE_ATTRIBUTES = {AVATAR_DIAGNOSISAREA_NONE,
																					AVATAR_DIAGNOSISAREA_BRAIN_SPINAL, 
 																					DIAGNOSISAREA_BONE, 
 																					DIAGNOSISAREA_LIVER,
 																					AVATAR_DIAGNOSISAREA_OTHER}; 
	
	/**
	 * public constructor referenced from the configuration pages.  This constructor takes a reference to the 
	 * manager so that the PatientHistory record can be updated by some of the avatar configurations.
	 * 
	 * @param manager reference to the parent manager class.
	 */
	public AvatarHistoryConfiguration(PatientInterfaceAction manager)
	{
		this.manager = manager;
		patientHistory = manager.getPatient().getPatientHistory();
		avatarHistory = manager.getPatient().getAvatarHistory();
		setFacades();
	}

	/**
	 * public constructor to set the facade from a history record directly.
	 * @param AvatarHistory the history to represent.
	 */
	public AvatarHistoryConfiguration(AvatarHistory avatarHistory)
	{
		this.avatarHistory = avatarHistory;
		setFacades();
	}

	/**
	 * Set the configuration items represented by this facade.
	 */
	private void setFacades()
	{
		yearMonthHistory = avatarHistory.getYearMonthHistory();
		areasOfDisease = new FacadeAttributeCharacteristic(DIAGNOSISAREA, AVATAR_AREAS_OF_DISEASE_ATTRIBUTES, avatarHistory);
		avatarBreastStages = new FacadeAttributeCharacteristic(STAGE, FacadeDiagnosis.getStageValues(), avatarHistory);
	}
	
	/**
	 * set a diagnosis area in the PatientHistory.  Special conditions exist for brain cancer.
	 * 
	 * BCT-698:  Spinal is also set if Brain is set.
	 * 
	 * @param code the code to set
	 */
	private void updateSingleDiagnosisAreaCode(CharacteristicCode code)
	{
		if (avatarHistory.containsAttribute(DIAGNOSISAREA, code))
		{
			if (code.equals(DIAGNOSISAREA_BRAIN))
			{
				setBrainSpinalDiagnosisArea();
			}
			patientHistory.addAttribute(DIAGNOSISAREA, code);
		}
		// otherwise clear the attribute.
		else
		{
			if (code.equals(DIAGNOSISAREA_BRAIN))
			{
				this.clearBrainSpinalDiagnosisArea();
			}	
			patientHistory.removeAttribute(DIAGNOSISAREA, code);
		}
	}

	/**
	 * This function gets called if the user chooses Other under area of disease.
	 * 
	 * Evidence of Disease applies only to Stage IV.  Stage III local spread of disease attributes are ignored.
	 * 
	 */
	private void setAllOtherEvidenceOfDisease()
	{
		for (CharacteristicCode code: FacadePatientHistory.getDiagnosisAreaAttributeCharacteristics())
		{
			// ignore Stage III local spread of disease.
			if (code.isMember(AVATAR_STAGEIII_LOCAL_SPREAD_OF_DISEASE_SET))
			{
				continue;
			}
			
			if (!code.equals(DIAGNOSISAREA_BRAIN) && !code.equals(DIAGNOSISAREA_BONE) && !code.equals(DIAGNOSISAREA_LIVER) && !code.equals(DIAGNOSISAREA_SPINALCORD))
			{
				patientHistory.addAttribute(DIAGNOSISAREA, code);
			}
		}
	}

	/**
	 * This function gets called if the user chooses No areas of disease.  Brain, Bone and Breast must be cleared
	 * by hand.
	 * 
	 * Evidence of Disease applies only to Stage IV.  Stage III local spread of disease attributes are ignored.
	 */
	private void clearAllOtherEvidenceOfDisease()
	{
		for (CharacteristicCode code: FacadePatientHistory.getDiagnosisAreaAttributeCharacteristics())
		{
			// ignore Stage III local spread of disease.
			if (code.isMember(AVATAR_STAGEIII_LOCAL_SPREAD_OF_DISEASE_SET))
			{
				continue;
			}
			
			if (!code.equals(DIAGNOSISAREA_BRAIN) && !code.equals(DIAGNOSISAREA_BONE) && !code.equals(DIAGNOSISAREA_LIVER) && !code.equals(DIAGNOSISAREA_SPINALCORD))
			{
				patientHistory.removeAttribute(DIAGNOSISAREA, code);
			}
		}
	}

	/**
	 * find the diagnosis instance where brain should be defined.
	 * @return diagnosis if found, null otherwise.
	 */
	private Diagnosis findBrainDiagnosisElement()
	{
		for (Diagnosis current : patientHistory.getDiagnoses())
		{	
			if (METASTATIC_BREAST_CANCER.equals(current.getValueHistory().getValue(ANATOMIC_SITE)))
			{
				return current;
			}
		}
		return null;
	}
	
	/**
	 * All brain cancer for avatars is automatically set to PROGRESSING.
	 * BCT-698:  add setting and clearing of spinal.
	 */
	private void setBrainSpinalDiagnosisArea()
	{
		//patientInterface.history.diagnoses[0].valueCharacteristics[7].value
		Diagnosis diagnosis = findBrainDiagnosisElement();
		
		if (diagnosis == null)
		{
			diagnosis = new Diagnosis();
			diagnosis.getValueHistory().setValue(ANATOMIC_SITE, METASTATIC_BREAST_CANCER);
		}
		diagnosis.getValueHistory().put(BREASTMETASTASIS_BRAIN, PROGRESSING);
		patientHistory.getDiagnoses().add(diagnosis);
		
		//BCT-698:  add spinal chord diagnosis as well.
		patientHistory.addAttribute(DIAGNOSISAREA, DIAGNOSISAREA_SPINALCORD);
	}
	
	/**
	 * Clear the brain diagnosis.
	 */
	private void clearBrainSpinalDiagnosisArea()
	{
		Diagnosis diagnosis = findBrainDiagnosisElement();
		if (diagnosis != null)
		{
			diagnosis.getValueHistory().remove(BREASTMETASTASIS_BRAIN);
			
			//BCT-698:  remove spinal chord diagnosis as well.
			patientHistory.removeAttribute(DIAGNOSISAREA, DIAGNOSISAREA_SPINALCORD);
		}
	}
	
	/**
	 * Clears all evidence of disease.
	 * 
	 * Evidence of Disease applies only to Stage IV.  Stage III local spread of disease attributes are ignored.
	 */
	private void clearAllEvidenceOfDisease()
	{
		for (CharacteristicCode code: FacadePatientHistory.getDiagnosisAreaAttributeCharacteristics())
		{
			// ignore Stage III local spread of disease.
			if (code.isMember(AVATAR_STAGEIII_LOCAL_SPREAD_OF_DISEASE_SET))
			{
				continue;
			}
			patientHistory.removeAttribute(DIAGNOSISAREA, code);
		}
	}
	
	private void overWriteHistoryDOB()
	{
		//patientInterface.history.yearMonthCharacteristics[0].shortValue
		Object obj = yearMonthHistory.get("shortValue");
		try
		{
			String yearString = (String) obj;
			if (yearString == null || yearString.trim().length() < 1)
			{
				patientHistory.setYearMonthHistory(new YearMonthHistory());
			}
			else
			{
				patientHistory.getYearMonthHistory().setShort(BIRTHDATE, Short.valueOf(yearString));
			}	
		}
		catch(NumberFormatException nfe)
		{
			//TODO:  add logger
		}
	}
	
	private void updateAvatarHistoryEvidenceOfDisease(FacadePatientHistory facadePatientHistory)
	{
		if (avatarHistory.containsAttribute(DIAGNOSISAREA, AVATAR_DIAGNOSISAREA_NONE))
		{
			clearAllEvidenceOfDisease();
			return;
		}

		updateSingleDiagnosisAreaCode(DIAGNOSISAREA_BONE);
		updateSingleDiagnosisAreaCode(DIAGNOSISAREA_BRAIN);
		updateSingleDiagnosisAreaCode(DIAGNOSISAREA_LIVER);

		//the next two have to be mutually exclusive.
		if (avatarHistory.containsAttribute(DIAGNOSISAREA, AVATAR_DIAGNOSISAREA_OTHER))
		{
			setAllOtherEvidenceOfDisease();
		}
		else
		{
			clearAllOtherEvidenceOfDisease();
		}
	}
	
	private void updateAvatarLocalSpreadOfDisease(FacadePatientHistory facadePatientHistory)
	{
		if (avatarHistory.isNoLocalSpreadOfDisease())
		{
			for (CharacteristicCode code: AVATAR_STAGEIII_LOCAL_SPREAD_OF_DISEASE_SET)
			{
				patientHistory.removeAttribute(DIAGNOSISAREA, code);
			}
			patientHistory.addAttribute(DIAGNOSISAREA, AVATAR_NOLOCAL_SPREAD_DISEASE);
		}
	}
	
	public void updateAvatarHistory(FacadePatientHistory facadePatientHistory)
	{
		overWriteHistoryDOB();
		updateAvatarHistoryEvidenceOfDisease(facadePatientHistory);
		updateAvatarLocalSpreadOfDisease(facadePatientHistory);
	}
	

	/**
	 * @param areasOfDisease the areasOfDisease to set
	 */
	public void setAreasOfDisease(FacadeAttributeCharacteristic areasOfDisease) 
	{
		this.areasOfDisease = areasOfDisease;
	}

	/**
	 * @return the areasOfDisease
	 */
	public FacadeAttributeCharacteristic getAreasOfDisease() 
	{
		return areasOfDisease;
	}
	
	/**
	 * @param yearMonthHistory the yearMonthHistory to set
	 */
	public void setYearMonthHistory(YearMonthHistory yearMonthHistory) 
	{
		this.yearMonthHistory = yearMonthHistory;
	}

	/**
	 * @return the yearMonthHistory
	 */
	public YearMonthHistory getYearMonthHistory() 
	{
		return yearMonthHistory;
	}

	/**
	 * @param avatarBreastStages the avatarBreastStages to set
	 */
	public void setAvatarBreastStages(FacadeAttributeCharacteristic avatarBreastStages) 
	{
		this.avatarBreastStages = avatarBreastStages;
	}
	
	public AvatarHistory getAvatarHistory()
	{
		return avatarHistory;
	}
	
	/**
	 * @return the avatarBreastStages
	 */
	public FacadeAttributeCharacteristic getAvatarBreastStages() 
	{
		return avatarBreastStages;
	}

	private static final long serialVersionUID = 8862831395041878196L;
}
