/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;

import org.quantumleaphealth.ontology.StringCharacteristicHolder;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Represents a facade into a map of string characteristics. This serializable
 * JavaBean exposes the <code>value</code> field to the GUI and provides a
 * localized label. It caches the characteristic's value in order to reduce
 * effort querying the holder.
 * 
 * @author Tom Bechtold
 * @version 2008-05-21
 */
public class FacadeStringCharacteristic implements Serializable
{
	/**
	 * The code of the characteristic to represent, guaranteed to be non-
	 * <code>null</code>
	 */
	private final CharacteristicCode characteristicCode;
	/**
	 * Holds the string characteristics, guaranteed to be non-<code>null</code>
	 */
	private StringCharacteristicHolder stringCharacteristicHolder;
	/**
	 * Cached value of the characteristic's string
	 */
	private String cachedValue;

	/**
	 * Validates and stores the parameter and sets the cached value
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to represent
	 * @param stringCharacteristicHolder
	 *            holds the string characteristics
	 * @throws IllegalArgumentException
	 *             if any parameter is <code>null</code>
	 */
	FacadeStringCharacteristic(CharacteristicCode characteristicCode,
			StringCharacteristicHolder stringCharacteristicHolder) throws IllegalArgumentException
	{
		if (characteristicCode == null)
			throw new IllegalArgumentException("characteristic not specified");
		this.characteristicCode = characteristicCode;
		setStringCharacteristicHolder(stringCharacteristicHolder);
	}

	/**
	 * Validates and stores the parameter and updates the cached value
	 * 
	 * @param stringCharacteristicHolder
	 *            holds the string characteristics
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	void setStringCharacteristicHolder(StringCharacteristicHolder stringCharacteristicHolder)
			throws IllegalArgumentException
	{
		if (stringCharacteristicHolder == null)
			throw new IllegalArgumentException("holder not specified");
		this.stringCharacteristicHolder = stringCharacteristicHolder;
		cachedValue = stringCharacteristicHolder.getString(characteristicCode);
	}

	/**
	 * @return the value that represents a characteristic or <code>null</code>
	 *         if this characteristic does not have any
	 */
	public String getValue()
	{
		return cachedValue;
	}

	/**
	 * Sets a value for this characteristic. Empty strings are treated as
	 * <code>null</code>.
	 * 
	 * @param value
	 *            the string that represents a not-otherwise-specified attribute
	 *            or <code>null</code> to remove any characteristic
	 */
	public void setValue(String value)
	{
		// Empty strings are set to null
		if (value != null)
		{
			value = value.trim();
			if (value.length() == 0)
				value = null;
		}
		if ((cachedValue == value) || ((cachedValue != null) && cachedValue.equals(value)))
			return;
		stringCharacteristicHolder.setString(characteristicCode, value);
		cachedValue = value;
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = -6139625531292192376L;
}
