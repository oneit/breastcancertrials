/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import javax.ejb.Local;

import org.quantumleaphealth.model.trial.PostalCodeGeocoding;

/**
 * Fetches geocoding for a postal code
 * 
 * @author Tom Bechtold
 * @version 2009-01-31
 */
@Local
public interface PostalCodeManager
{
	/**
	 * @return the closest known postal code, guaranteed to be non-
	 *         <code>null</code>
	 * @param postalCode
	 *            the postal code to be closest to
	 */
	public PostalCodeGeocoding getPostalCodeGeocoding(int postalCode);
}
