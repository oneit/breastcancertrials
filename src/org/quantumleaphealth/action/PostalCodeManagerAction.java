/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Name;
import org.quantumleaphealth.model.trial.PostalCodeGeocoding;

/**
 * Fetches geocoding for a postal code.
 * 
 * @author Tom Bechtold
 * @version 2009-01-31
 */
@Stateless
@Name("postalCodeManager")
public class PostalCodeManagerAction implements PostalCodeManager, Serializable
{
	/**
	 * Loads the trial data using EJB persistence manager
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;

	/**
	 * The name of the JPA-implementation specific query hint to identify the
	 * result as being read-only.
	 */
	private static final String HINT_READONLY = "org.hibernate.readOnly";

	/**
	 * @return the closest known postal code, guaranteed to be non-
	 *         <code>null</code>
	 * @param postalCode
	 *            the postal code to be closest to
	 * @see org.quantumleaphealth.action.PostalCodeManager#getPostalCodeGeocoding(int)
	 */
	public PostalCodeGeocoding getPostalCodeGeocoding(int postalCode)
	{
		// The order-by, maxresults and singleresult combination fetches the
		// postal code record closest to the parameter
		return (PostalCodeGeocoding) (em.createQuery(
				"select p from PostalCodeGeocoding p order by abs(p.postalCode - :postalCode)").setParameter(
				"postalCode", postalCode).setHint(HINT_READONLY, Boolean.TRUE).setMaxResults(1).getSingleResult());
	}

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = -5198609879693159019L;
}
