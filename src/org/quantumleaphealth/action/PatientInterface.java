/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.util.List;
import java.util.Locale;

import javax.ejb.Local;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.jboss.seam.annotations.remoting.WebRemote;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.screen.EligibilityProfile;

/**
 * Navigates a patient through collecting, validating and persisting
 * information. This interface defines the following roles for a local stateful
 * session Enterprise JavaBean (EJB):
 * <ol>
 * <li>Provide facades into the patient's data. Facades are used to translate
 * the view's representation of the data into the storage format of the patient
 * object.</li>
 * <li>Provide data persistence. Methods are used to load and store patient data
 * from/to a persistence provider.</li>
 * <li>Provide data validation. The validation methods may be used to guide
 * navigation through the view.</li>
 * </ol>
 * 
 * @author Tom Bechtold
 * @version 2008-12-18
 */
@Local
public interface PatientInterface
{
	/**
	 * @return the patient, guaranteed to be non-<code>null</code>.
	 */
	public UserPatient getPatient();

	public void setPatient(UserPatient newPatient);

	/**
	 * @return the locale
	 */
	public Locale getLocale();

	/**
	 * @return the avatar history configuration object
	 */
	public AvatarHistoryConfiguration getAvatarHistoryConfiguration(); 

	/**
	 * @return the patient's characteristics, guaranteed to be non-
	 *         <code>null</code>.
	 */
	public FacadePatientHistory getHistory();

	/**
	 * @return the number of months old a patient's history is or
	 *         <code>-1</code> if the history is incomplete
	 */
	public int getPatientHistoryAgeMonths();

	/**
	 * @return the user's postal code or <code>null</code> if none
	 */
	public Long getPostalCode();

	/**
	 * @param postalCode
	 *            the user's postal code or <code>null</code> if none
	 */
	public void setPostalCode(Long postalCode);

	/**
	 * @return the user's credentials or <code>null</code> if new user
	 */
	public String getCredentials();

	/**
	 * Trims and stores the credentials.
	 * 
	 * @param credentials
	 *            the user's credentials or <code>null</code> if new user
	 */
	public void setCredentials(String credentials);

	/**
	 * @return the emailAddressVerify
	 */
	public String getEmailAddressVerify();

	/**
	 * Sets the email address to use for verification.
	 * 
	 * @param emailAddressVerify
	 */
	public void setEmailAddressVerify(String emailAddressVerify);

	/**
	 * Update the patient's history
	 */
	public void updateHistory();

	/**
	 * @return the patient's screened trials sorted by type and closest research
	 *         site, guaranteed to be non-<code>null</code>
	 */
	public TrialProximityGroups getTrials();

	/**
	 * @return the number of screened trials for the patient
	 */
	public int getScreenedTrialCount();

	/**
	 * @return the identifier of the trial to display the sites for or
	 *         <code>null</code> for no trial
	 */
	public Long getTrialId();

	/**
	 * @param trialId
	 *            the identifier of the trial to display the sites for or
	 *            <code>null</code> for no trial
	 */
	public void setTrialId(Long trialId);

	/**
	 * @return the research sites for a trial sorted by closest site or
	 *         <code>null</code> if no trial specified by
	 *         <code>setTrialId</code>
	 * @see #setTrialId(Long)
	 */
	public TrialSiteProximities getSortedTrialSites();

	/**
	 * @return the patient's eligibility profiles
	 */
	public List<EligibilityProfile> getEligibilityProfiles();

	/**
	 * Returns whether the patient is a first-time user, e.g., if not persisted
	 * yet or last login is the same as persisted time.
	 * 
	 * @return <code>true</code> if the patient is a first-time user
	 */
	public boolean isFirstTime();

	/**
	 * Returns whether the patient accepted the terms/conditions
	 * 
	 * @return <code>true</code> if the terms were accepted
	 */
	public boolean isTermsAcceptedValid();

	/**
	 * Returns whether the patient answered the category question(s).
	 * 
	 * @return <code>true</code> if the category question(s) were answered
	 */
	public boolean isCategoryValid();

	/**
 	 *  Set the avatar facade ID.
	 */
	public void setAvatarId(Long avatarId);
	
	/**
     * Get the facade avatar ID.
     */
	public Long getAvatarId();
	
	public boolean isAvatarProfile();
	
	/**
	 * Validates that the termsAccepted checkbox was checked. This method adds a
	 * non-localized faces messages if the component was not checked.
	 * 
	 * @param facesContext
	 *            the faces context
	 * @param component
	 *            the checkbox component
	 * @param value
	 *            the components value
	 */
	public void validateTermsAccepted(FacesContext facesContext, UIComponent component, Object value);

	/**
	 * Validates that the country code is entered. This method adds a
	 * non-localized faces messages if nothing was submitted.
	 * 
	 * @param facesContext
	 *            the faces context
	 * @param component
	 *            the input component
	 * @param value
	 *            the components value
	 */
	public void validateCountryCode(FacesContext facesContext, UIComponent component, Object value);

	/**
	 * Validates the verification email address is entered and matches the
	 * patient's email address. This method invalidates the component and adds a
	 * non-localized faces messages if the submitted value is not available or
	 * if the submitted value does not match the patient's email address.
	 * 
	 * @param facesContext
	 *            The faces context
	 * @param component
	 *            The input component
	 * @param value
	 *            The component's value
	 */
	public void validateEmailAddress(FacesContext facesContext, UIComponent component, Object value);

	/**
	 * Validates the principal is entered and, if changed, is a valid email
	 * address. This method invalidates the component and adds a non-localized
	 * faces messages if the submitted value is not available or if the
	 * submitted value has changed (case-insensitive) and the trial alert
	 * service is desired yet the submitted value is not a valid email address.
	 * 
	 * @param facesContext
	 *            the faces context
	 * @param component
	 *            the input component
	 * @param value
	 *            the component's value
	 */
	public void validatePrincipal(FacesContext facesContext, UIComponent component, Object value);

	/**
	 * Validates the credentials are entered and are equal to the other patient
	 * credentials. This method invalidates the component and adds a
	 * non-localized faces messages if the submitted value is not available or
	 * if the submitted value is not equal (case-sensitive) to the other patient
	 * credentials component's submitted value.
	 * 
	 * @param facesContext
	 *            the faces context
	 * @param component
	 *            the input component
	 * @param value
	 *            the component's value
	 */
	public void validateCredentials(FacesContext facesContext, UIComponent component, Object value);

	/**
	 * Switch displayed language to Spanish.
	 */
	public void switchToSpanish();

	/**
	 * Revert displayed language to English.
	 */
	public void revertToEnglish();

	/**
	 * Mark this trial as not interested by the patient.
	 * 
	 * @param trialId
	 */
	@WebRemote
	public String notInterested(Long trialId);

	/**
	 * Mark this trial as now once again interested by the patient.
	 * 
	 * @param trialId
	 */
	@WebRemote
	public String nowInterested(Long trialId);

	/**
	 * Add this trial to the patient's favorite list.
	 * 
	 * @param trialId
	 */
	@WebRemote
	public String addToFavorites(Long trialId);

	/**
	 * Remove this trial from the list of patient favorites.
	 * 
	 * @param trialId
	 */
	@WebRemote
	public String removeFromFavorites(Long trialId);

	public void setPatientType(String patientType); 

	public String getPatientType(); 
	/**
	 * Persist and log in a new user. Do nothing if the credentials do not match
	 * or there is an existing user with different credentials. If there is an
	 * existing user with same credentials, update that user's history and log
	 * her in.
	 */
	public void persist();

	/**
	 * Authenticates and loads a user whose principal and credentials are stored
	 * in its <code>patient</code> property or loads the patient's secret
	 * question if credentials are not provided and updates its
	 * <code>unsuccessfulAuthenticationCount</code>.
	 */
	public void login();

	/**
	 * Resets the patient to a new patient and invalidates the session.
	 */
	public void logout();

	/**
	 * @return whether the patient is represented in the database
	 */
	public boolean isLoggedIn();

//	/**
//	 * @param navigatorClientId the navigatorClientId to set
//	 */
//	public void setNavigatorClientId(Long navigatorClientId);
//
//	/**
//	 * @return the navigatorClientId
//	 */
//	public Long getNavigatorClientId();

	/**
	 * Removes the patient's authentication and invitations and logs out.
	 */
	public void donate();

	/**
	 * Deletes a patient's database record and logs out.
	 * 
	 * @see #logout()
	 */
	public void delete();

	/**
	 * Persists changes to an existing user's information that is not the health
	 * history
	 */
	public void update();

	/**
	 * Removes the stateful session bean from memory. This method is required by
	 * the EJB3 specification.
	 */
	public void destroy();
	
	/**
	 * @return the forwardingURL
	 */
	public String getForwardingURL();
	
	public void goToTrial();
	
}
