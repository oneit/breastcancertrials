package org.quantumleaphealth.action;

import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.patient.UserPatient;

/**
 * This bean handles all of the user interaction on the forgot_password page.
 * It was originally in the PatientInterfaceAction class.
 * 
 * @author wgweis
 */
@Stateful
@Name("forgotPasswordInterface")
public class ForgotPasswordInterfaceAction implements ForgotPasswordInterface
{
	/**
	 * The length of the randomly-generated credentials
	 */
	private static final int LENGTH_RANDOMCREDENTIALS = 6;
	/**
	 * Random number generator
	 */
	private static final Random RANDOM = new Random();

	/**
	 * Persists the entity beans using EJB3 persistence context
	 */
	@PersistenceContext(unitName = "bct" )
	private EntityManager em;

	/**
	 * The messaging engine
	 */
	@EJB
	private MessageSender messageSender;

	/**
	 * And instance of the UserPatient model class for updating.
	 */
	private UserPatient userPatient = null;

	/**
	 * Email address as it appears on the forgot_password form.
	 */
	private String emailAddress;
	
	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log logger;

	/**
	 * take the email address passed in and return an InternetAddress instance
	 * for use by the messageSender engine.
	 * @return the InternetAddress if all goes well.
	 */
	private InternetAddress parseEmailAddress()
	{
		InternetAddress internetAddress = null;
		if (!userPatient.isPrincipalInvalidEmailAddress())
		{	
			try
			{
				internetAddress = new InternetAddress(userPatient.getPrincipal(), true);
			}
			catch (AddressException addressException)
			{
				userPatient.setPrincipalInvalidEmailAddress(true);
				em.flush();
			}
		}	
		return internetAddress;
	}
	
	/**
	 * The main entry point into this class.  Checks for existence of user, validity of email address
	 * and updates the database entry if all goes well.
	 * @return a string for navigation in the pages.xml file.
	 */
	@Override
	public String sendPasswordSuccess()
	{
		try
		{
			// TODO:  for version 2.0  add check that UserPatient is *not* an avatar.
			userPatient = (UserPatient) em.createQuery(	"select u from UserPatient u where LOWER(u.principal)=LOWER(:principal)").setParameter("principal", emailAddress).getSingleResult();
		}	
		catch (NoResultException noResultException)
		{
			return "notthere";
		}
		catch (NonUniqueResultException nonUniqueResultException)
		{
			return "toomany";
		}

		InternetAddress internetAddress = parseEmailAddress();
		if ( internetAddress == null )
		{
			return "invalidemail";
		}
		
		// try to set the new password.  Rollback if the email fails.
		String newCredentials = getNewPassword();
		userPatient.setCredentials( newCredentials );
		try 
		{
			messageSender.send(internetAddress, null, "Your BCT password has been reset",
					"Your password on BreastCancerTrials.org has been changed to: " + newCredentials);
		} 
		catch (Throwable e) 
		{
			em.clear();  // throw away changes.
			logger.fatal( "Error sending email ",  e);
			return "emailfailure";
		} 
		userPatient.setUnsuccessfulAuthenticationCount(0);
		logger.debug("Reset patient credentials and sent message to '#0'", userPatient.getPrincipal());
		
		return "success";
	}

	/**
	 * setter for use by EL
	 */
	public void setEmailAddress(String emailAddress) 
	{
		this.emailAddress = emailAddress;
	}

	/**
	 * getter for use by EL on the forgot_password.xhtml page.
	 */
	public String getEmailAddress() 
	{
		return emailAddress;
	}
	
	/**
	 * @return return six random characters for the new password.
	 */
	private String getNewPassword()
	{
		// Passed: reset and send credentials, reset unsuccessful authentication
		// count, logout session
		char[] random_characters = new char[LENGTH_RANDOMCREDENTIALS];
		// The new credentials uses upper-case Roman characters and digits for
		// readability
		for (int index = 0; index < LENGTH_RANDOMCREDENTIALS; index++)
		{	
			random_characters[index] = Character.toUpperCase(Character.forDigit(RANDOM.nextInt(Character.MAX_RADIX), Character.MAX_RADIX));
		}	
		return new String(random_characters);
	}

	/**
	 * @return a string to trigger navigation to home in the pages.xml file.
	 */
	@Override
	public String cancelPasswordReset() 
	{
		return "cancel";
	}

	/**
	 * Required by EJB3 interface.
	 */
	@Remove
	@Destroy
	public void destroy()
	{
	}
}
