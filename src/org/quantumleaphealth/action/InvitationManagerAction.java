/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.patient.Invitation;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.TrialSite;

/**
 * Creates, withdraws, retrieves and sorts a patient's invitations. Each
 * instance of the implementing class services a particular patient.
 * 
 * @author Tom Bechtold
 * @version 2009-07-08
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("invitationManager")
public class InvitationManagerAction implements InvitationManager, Serializable
{
	/**
	 * Persists the entity beans using EJB3 persistence context
	 * 
	 * @see #invite()
	 * @see #withdraw()
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;
	/**
	 * The patient's data, guaranteed to be non-<code>null</code>
	 */
	@In(create = true)
	private PatientInterface patientInterface;
	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log logger;

	/**
	 * The current invitation or <code>null</code> if not set
	 */
	private Invitation invitation;
	/**
	 * The patient's name for newly created invitations or <code>null</code> if
	 * not set
	 */
	private String patientName;
	/**
	 * The patient's contact information for newly created invitations or
	 * <code>null</code> if not set
	 */
	private String patientContact;
	/**
	 * The message for newly created invitations or <code>null</code> if not set
	 */
	private String message;

	/**
	 * @return the patient's invitations grouped by trial and sorted by site
	 *         name, guaranteed to be non-<code>null</code>
	 * @see org.quantumleaphealth.action.InvitationManager#getTrialInvitations()
	 */
	public List<TrialInvitations> getTrialInvitations()
	{
		// Use LinkedList as we expect very few entries to sort
		LinkedList<TrialInvitations> trialInvitationsList = new LinkedList<TrialInvitations>();
		invitationLoop: for (Invitation storedInvitation : patientInterface.getPatient().getInvitations())
			if (storedInvitation != null)
			{
				for (TrialInvitations trialInvitations : trialInvitationsList)
					if (trialInvitations.add(storedInvitation))
						continue invitationLoop;
				// If not added to an existing grouping then create its own
				trialInvitationsList.add(new TrialInvitations(storedInvitation));
			}
		Collections.sort(trialInvitationsList);
		return trialInvitationsList;
	}

	/**
	 * @return the current invitation or <code>null</code> if not set
	 * @see org.quantumleaphealth.action.InvitationManager#getInvitation()
	 */
	@BypassInterceptors
	public Invitation getInvitation()
	{
		return invitation;
	}

	/**
	 * @return the patient's name for newly created invitations
	 * @see org.quantumleaphealth.action.InvitationManager#getPatientName()
	 */
	@BypassInterceptors
	public String getPatientName()
	{
		return patientName;
	}

	/**
	 * @param patientName
	 *            the patient's name for newly created invitations
	 * @see org.quantumleaphealth.action.InvitationManager#setPatientName(java.lang.String)
	 */
	@BypassInterceptors
	public void setPatientName(String patientName)
	{
		this.patientName = patientName;
	}

	/**
	 * @return the patient's contact information for newly created invitations
	 *         or the patient's principal if not set yet
	 * @see org.quantumleaphealth.action.InvitationManager#getPatientContact()
	 */
	public String getPatientContact()
	{
		return patientContact != null ? patientContact : patientInterface.getPatient().getPrincipal();
	}

	/**
	 * @param patientName
	 *            the patient's contact information for newly created
	 *            invitations
	 * @see org.quantumleaphealth.action.InvitationManager#setPatientContact(java.lang.String)
	 */
	@BypassInterceptors
	public void setPatientContact(String patientContact)
	{
		this.patientContact = patientContact;
	}

	/**
	 * @return the message for newly created invitations
	 * @see org.quantumleaphealth.action.InvitationManager#getMessage()
	 */
	@BypassInterceptors
	public String getMessage()
	{
		return message;
	}

	/**
	 * @param message
	 *            the message for newly created invitations
	 * @see org.quantumleaphealth.action.InvitationManager#setMessage(java.lang.String)
	 */
	@BypassInterceptors
	public void setMessage(String message)
	{
		this.message = message;
	}

	/**
	 * Sets the current invitation to either an existing one or to a new one
	 * that references a particular site within the user's screened trials. If
	 * the list of current trial sites is not available or does not contain the
	 * desired site then the current invitation is set to <code>null</code>. If
	 * a new invitation is created then it populates the following fields from
	 * the first sorted persisted invitation:
	 * <ul>
	 * <li><code>patientName</code></li>
	 * <li><code>patientContact</code></li>
	 * <li><code>message</code></li>
	 * </ul>
	 * 
	 * @param trialSiteString
	 *            a concatenation of the unique identifier of the trial and the
	 *            character <code>|</code> and the unique identifier of the
	 *            site; if either id empty then do nothing
	 * @see org.quantumleaphealth.action.InvitationManager#setTrialSite(java.lang.Long,
	 *      java.lang.Long)
	 */
	@RequestParameter
	public void setTrialSite(String trialSite)
	{
		// Parse the string into longs
		Long trialId = null;
		Long siteId = null;
		if ((trialSite != null) && (trialSite.trim().length() > 0))
		{
			String[] split = trialSite.trim().split("\\|");
			if (split.length > 0)
				try
				{
					trialId = Long.valueOf(Long.parseLong(split[0]));
				}
				catch (NumberFormatException numberFormatException)
				{
					logger.error("Cannot get trial id from '#0' for patient ##1", numberFormatException, trialSite,
							patientInterface.getPatient().getId());
				}
			if (split.length > 1)
				try
				{
					siteId = Long.valueOf(Long.parseLong(split[1]));
				}
				catch (NumberFormatException numberFormatException)
				{
					logger.error("Cannot get site id from '#0' for patient ##1", numberFormatException, trialSite,
							patientInterface.getPatient().getId());
				}
		}

		// If either id is empty then do nothing
		if ((trialId == null) || (trialId.longValue() == 0) || (siteId == null) || (siteId.longValue() == 0))
			return;
		// If current invitation matches the trial and site then do nothing
		if (equalsTrialSite(invitation, trialId, siteId))
			return;

		// Precedence is a stored invitation then a matched trial site
		for (Invitation storedInvitation : patientInterface.getPatient().getInvitations())
			if (equalsTrialSite(storedInvitation, trialId, siteId))
			{
				invitation = storedInvitation;
				logger.debug("Setting invitation ##0 with site ##1 for patient ##2", invitation.getId(), siteId,
						invitation.getUserPatient().getId());
				return;
			}
		// Find the site within the patient's current list of screened trials
		Trial trial = patientInterface.getTrials().get(trialId);
		if (trial == null)
		{
			invitation = null;
			logger.debug("Could not find screened trial ##0 for patient ##1", trialId, patientInterface.getPatient()
					.getId());
			return;
		}
		TrialSite foundTrialSite = null;
		for (TrialSite screenedTrialSite : trial.getTrialSite())
			if ((screenedTrialSite != null) && (screenedTrialSite.getSite() != null)
					&& siteId.equals(screenedTrialSite.getSite().getId()))
			{
				foundTrialSite = screenedTrialSite;
				break;
			}
		// If not found then invalidate the current invitation
		if (foundTrialSite == null)
		{
			invitation = null;
			logger.debug("Could not find site ##0 at screened trial ##1 for patient ##2", siteId, trialId,
					patientInterface.getPatient().getId());
			return;
		}

		// Create a new invitation
		invitation = new Invitation();
		invitation.setUserPatient(patientInterface.getPatient());
		invitation.setTrialSite(foundTrialSite);
		logger.debug("Creating invitation with trial ##0 and site ##1 for patient ##2", invitation.getTrialId(),
				siteId, invitation.getUserPatient().getId());
	}

	/**
	 * @param invitation
	 *            the invitation to match
	 * @param trialId
	 *            the trial's identifier to match
	 * @param siteId
	 *            the site's identifier to match
	 * @return whether an invitation matches a trial site
	 */
	private static boolean equalsTrialSite(Invitation invitation, Long trialId, Long siteId)
	{
		return (invitation != null) && (invitation.getTrialSite() != null) && (trialId != null) && (siteId != null)
				&& trialId.equals(invitation.getTrialId()) && siteId.equals(invitation.getSiteId());
	}

	/**
	 * Persists the current invitation by adding it to the persistence engine
	 * and to the patient's list of invitations. If the current invitation is
	 * not available or is already persisted then this method does nothing. If
	 * the trialsite does not have a message recipient then do nothing.
	 * 
	 * @see org.quantumleaphealth.action.InvitationManager#invite()
	 */
	public void invite()
	{
		// If already persisted then do nothing
		if ((invitation == null) || (invitation.getId() != null))
			return;

		// Invitations cannot be persisted if they do not have a message service
		// recipient
		if ((invitation.getTrialSite() == null) || (invitation.getTrialSite().getNotificationEmailAddress() == null))
		{
			logger.warn("Cannot send message to trial ##0 and site ##1 for patient ##2", invitation.getTrialId(),
					invitation.getSiteId(), invitation.getUserPatient().getId());
			return;
		}
		// Invitations cannot be persisted if the patient isn't
		if (!patientInterface.isLoggedIn())
		{
			patientInterface.persist();
			invitation.setUserPatient(patientInterface.getPatient());
			if (!patientInterface.isLoggedIn())
				return;
		}

		// Set the name, contact and message fields
		invitation.setPatientName(getPatientName());
		invitation.setPatientContact(getPatientContact());
		invitation.setMessage(getMessage());
		// If entity manager loses the transient trialSite then reattach it
		TrialSite trialSite = invitation.getTrialSite();
		em.persist(invitation);
		if (invitation.getTrialSite() == null)
			invitation.setTrialSite(trialSite);
		// Add the newly created invitation to the patient's list
		patientInterface.getPatient().getInvitations().add(invitation);
		logger.debug("Persisted invitation ##0 with trial ##1 and site ##2 for patient ##3", invitation.getId(),
				invitation.getTrialId(), invitation.getSiteId(), invitation.getUserPatient().getId());
	}

	/**
	 * Withdraws the current invitation by removing it from the patient's list
	 * of invitations and from the persistence engine. If the current invitation
	 * is not available or is already withdrawn then this method does nothing.
	 * 
	 * @see org.quantumleaphealth.action.InvitationManager#withdraw()
	 */
	public void withdraw()
	{
		// If already withdrawn then do nothing
		if ((invitation == null) || (invitation.getId() == null))
			return;
		// Cache its transient field
		TrialSite trialSite = invitation.getTrialSite();
		// em.remove does not work
		patientInterface.getPatient().getInvitations().remove(invitation);
		int rowCount = em.createQuery("DELETE Invitation WHERE id=:id").setParameter("id", invitation.getId())
				.executeUpdate();
		if (rowCount == 0)
		{
			logger.error("Could not withdraw invitation ##0 with trial ##1 and site ##2 for patient ##3", invitation
					.getId(), trialSite.getTrial().getId(), trialSite.getSite().getId(), patientInterface.getPatient()
					.getId());
			return;
		}
		logger.debug("Withdrew invitation ##0 with trial ##1 and site ##2 for patient ##3", invitation.getId(),
				trialSite.getTrial().getId(), trialSite.getSite().getId(), invitation.getUserPatient().getId());
		// Unpersist the current invitation
		invitation = new Invitation();
		invitation.setUserPatient(patientInterface.getPatient());
		invitation.setTrialSite(trialSite);
	}

	/**
	 * Reset instance variables when stateful session bean is destroyed.
	 * 
	 * @see org.quantumleaphealth.action.InvitationManager#destroy()
	 */
	@Remove
	@Destroy
	public void destroy()
	{
		invitation = null;
		patientName = null;
		patientContact = null;
		message = null;
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = -5859864515222245010L;
}
