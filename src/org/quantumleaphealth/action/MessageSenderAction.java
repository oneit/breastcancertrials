/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.jboss.seam.web.ServletContexts;
import org.quantumleaphealth.screen.DeclarativeMatchingEngine;

/**
 * Sends an email message
 * 
 * @author Tom Bechtold
 * @version 2009-03-02
 */
@Stateless
@Name("messageSender")
public class MessageSenderAction implements MessageSender, Serializable
{
	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log logger;

	/**
	 * The message sent when calling <code>send()</code>. Each user has one
	 * message that is reused during the session. If
	 */
	@In(required = false)
	@Out(required = false)
	private SimpleMessage simpleMessage;

	/**
	 * Request level bean subclass of SimpleMessage.  Used on the email_link.xhtml JSF fragment
	 */
	@In(required = false)
	private EmailLinkMessage emailLinkMessage;
	
	/**
	 * Used by the sendLink method to determine whether the send was successful.
	 */
	private boolean emailLinkSuccessful = false;
	
	/**
	 * The mail session that sends the message. JBoss resource specified in
	 * deploy/*mail-service.xml
	 */
	@Resource(mappedName = "java:/Mail")
	private Session session;

	/**
	 * Attribute prefix
	 */
	private static final String SMTP_PREFIX = "mail.";
	/**
	 * Attribute for transport protocol
	 */
	private static final String SMTP_TRANSPORT = SMTP_PREFIX + "transport.protocol";
	/**
	 * Attribute suffix to determine whether to authenticate SMTP user
	 */
	private static final String SMTP_AUTHENTICATE = "auth";
	/**
	 * Attribute suffix for authenticated SMTP server
	 */
	private static final String SMTP_ATTRIBUTE_HOST = "host";
	/**
	 * Attribute suffix for authenticated SMTP user
	 */
	private static final String SMTP_ATTRIBUTE_USER = "user";
	/**
	 * Attribute suffix for sender
	 */
	private static final String SMTP_ATTRIBUTE_FROM = "from";
	/**
	 * Attribute suffix for authenticated SMTP password
	 */
	private static final String SMTP_ATTRIBUTE_PASSWORD = "password";
	/**
	 * Attribute suffix for system contact address
	 */
	private static final String SMTP_ATTRIBUTE_CONTACT = "contact";
	/**
	 * Attribute suffix for system divert address. If specified then each
	 * original message is not delivered; rather, it is encapsulated into a
	 * diversion message sent to this address. This feature is used for testing.
	 */
	private static final String SMTP_ATTRIBUTE_DIVERT = "divert";
	/**
	 * Mime type for email message
	 */
	private static final String MIME_EMAIL = "message/rfc822";

	/**
	 * Subject strings.  The DEFAULT_SUBJECT is the one used if no other subject string
	 * has been applied.
	 */
	public static final String DEFAULT_SUBJECT 				= "Message from BCT Contact Us page ";
	public static final String AVATAR_HISTORY_SUBJECT 		= "This Sample Patient History Page Link was sent to you from www.breastcancertrials.org";
	public static final String AVATAR_TRIAL_LIST_SUBJECT 	= "This Sample Patient Trial List Page Link was sent to you from www.breastcancertrials.org";
	public static final String AVATAR_DEFAULT_SUBJECT 	    = "This Sample Patient Link was sent to you from www.breastcancertrials.org";
	public static final String EMAIL_LINK_SUBJECT           = "I\'ve sent you a link from BCT";
	
	@RequestParameter("pid")
	public String principal;
	
	@RequestParameter("linktype")
	private String linktype;
	private static final String EMAIL_LINK_TYPE = "email_link";
	
	@RequestParameter("pagetype")
	private String pagetype;

	@RequestParameter("categoryString")
	private String categoryString;

	/**
	 * Mapping between the categoryString passed in and the quickview URL to put in an email.
	 * Each new quickview should have an entry in this map.
	 */
	private static final HashMap<String, String> quickviews = new HashMap<String, String>();
	static
	{
		quickviews.put( "prevention", "prevention");
		quickviews.put( "screening", "screening");
		quickviews.put( "alternative", "cam");
		quickviews.put( "treatment_vaccine", "vaccines");
		quickviews.put( "novisitsrequired", "nvr");
		quickviews.put( "treatment_neoadjuvant", "neoadjuvant");
	}
	
	/**
	 * Mime type of plain text.
	 */
	private static final String MIMETYPE_PLAIN = "text/plain";

	/**
	 * @return the appropriate subject depending on what type of email is being sent.
	 */
	private String getSubjectLine()
	{
		if ( linktype == null )
		{
			return AVATAR_DEFAULT_SUBJECT;
		}
		else if ( linktype.equalsIgnoreCase("history"))
		{
			return AVATAR_HISTORY_SUBJECT;
		}
		else if ( linktype.equalsIgnoreCase("trials"))
		{
			return AVATAR_TRIAL_LIST_SUBJECT;
		}
		else if ( linktype.equalsIgnoreCase("email_link"))
		{
			return EMAIL_LINK_SUBJECT;
		}

		return  AVATAR_DEFAULT_SUBJECT;
	}
	
	/**
	 * @return a string with the Url to use in the email as part of the "Send this link to your friend" feature.
	 */
	private String getLinkUrl()
	{
		if ( pagetype != null && categoryString != null )
		{
			try 
			{
				URL url = new URL( 	ServletContexts.instance().getRequest().getScheme(),
									ServletContexts.instance().getRequest().getServerName(),
									"" );

				if (pagetype.equals("quickviews_page"))
				{
					return url + "/bct_nation/" +  quickviews.get( categoryString.toLowerCase() );
				}
				else if (pagetype.equals("browsetrials_page"))
				{
					return url + "/bct_nation/browse_trials.seam?categoryString=" + categoryString; 
				}
				else if (pagetype.equals("public_history_page"))
				{
					return url + "/bct_nation/public/public_history_review.seam?pid=" + principal; 
				}
				else if (pagetype.equals("public_trials_page"))
				{
					return url + "/bct_nation/public/public_browse_trials.seam?pid=" + principal; 
				}

			} 
			catch (MalformedURLException e) 
			{
				// Ignored, we'll just return the request URL if this breaks.
			}
		}
		return ServletContexts.instance().getRequest().getRequestURL().toString();
	}

	/**
	 * @return string with hyperlink to Url the user wishes to send an email with the link.
	 */
	private String getLink()
	{
		if ( ServletContexts.instance().getRequest() != null && ServletContexts.instance().getRequest().getRequestURL() != null )
		{
			String Url = getLinkUrl();
			
			String LinkText = null;
			if (emailLinkMessage != null && emailLinkMessage.getCategoryString() != null && emailLinkMessage.getCategoryString().trim().length() > 0 && !emailLinkMessage.getCategoryString().equals( "All" ))
			{
				LinkText = "\"" + emailLinkMessage.getCategoryBundleValue( emailLinkMessage.getCategoryString() ) + "\" ";
			}
			else if (emailLinkMessage != null && emailLinkMessage.getPrincipal() != null )
			{
				return EmailLinkMessageAction.avatarUrlTextFormatLine1 + Url;
			}
			return String.format( EmailLinkMessageAction.UrlTextFormatLine1, LinkText == null? "": LinkText ) + Url;
		}
		return "";
	}
	
	/**
	 * This action is referenced from the email_link.xhtml JSF fragment.
	 * It sends an "Email this link to your friend" link.
	 */
	@Override
	public void sendLink()
	{
		if ( simpleMessage == null )
		{
			simpleMessage = new SimpleMessage();
			if ( emailLinkMessage != null )
			{
				simpleMessage.setFromTitleName( emailLinkMessage.getFromTitleName() );
			}	
		}
		if ( emailLinkMessage != null )
		{
			simpleMessage.setBody( emailLinkMessage.getBody() );
			simpleMessage.setTo( emailLinkMessage.getTo() );
		}

		if ( simpleMessage.getFromTitleName() == null  || simpleMessage.getFromTitleName().trim().length() <= 0 )
		{
			FacesMessages.instance().add(Severity.ERROR, "You must provide your name.");	
			return;
		}

		if ( simpleMessage.getRecipients() == null )
		{
			FacesMessages.instance().add(Severity.ERROR, "At least one valid email must be provided.");	
			return;
		}
		
		simpleMessage.setDelivered(false);

		String body = simpleMessage.getBody();
		if ( body != null )
		{	
			simpleMessage.setBody( body.replace( SimpleMessage.defaultBodyLine2, ""));
		}	
		simpleMessage.setSubject( getSubjectLine() );
		String link = getLink();
		
		if ( link != null )
		{	
			if ( simpleMessage.getBody() != null )
			{	
				simpleMessage.setBody( simpleMessage.getBody() + "\n\n" + link );
			}
			else
			{
				simpleMessage.setBody( link );
			}
		}
		
		// Unfortunately, with the version of SEAM we are using we cannot check to see whether the
		// FacesMessages are empty after sending.  The list is empty until the response is rendered.
		// This is what we were using prior to this release.
		// this hack uses an instance level boolean to determine whether the send was successful.
		emailLinkSuccessful = true;		
		send();
		
		// if the FacesMessages instance is empty, the send was successful
		if ( emailLinkSuccessful == true )
		{
			FacesMessages.instance().add(Severity.INFO, "Message Sent.");
		}
		
		if ( emailLinkMessage != null )
		{	
			emailLinkMessage.setBody( "" );
			emailLinkMessage.setTo( "" );
		}
		simpleMessage.setBody("");
		simpleMessage.setTo("");
	}

	/**
	 * Called from the email_link.xhtml JSF fragment to reset all three fields in the email link form.
	 */
	public void reset()
	{
		FacesMessages.instance().clear();
		if ( emailLinkMessage != null )
		{	
			emailLinkMessage.setBody( "" );
			emailLinkMessage.setTo( "" );
			emailLinkMessage.setFromTitleName( "" );
		}
		
		if ( simpleMessage != null )
		{
			simpleMessage.setBody( "" );
			simpleMessage.setTo( "" );
			simpleMessage.setFromTitleName( "" );
		}
	}
	
	/**
	 * Sends the <code>SimpleMessage</code> instance to the session's
	 * <tt>divert</tt> address or to the session's <tt>contact</tt> address if
	 * <tt>divert</tt> is not specified. If the message was sent then its
	 * <code>delivered</code> property is set; otherwise a Faces message is
	 * created.
	 * 
	 * @see org.quantumleaphealth.action.MessageSender#send()
	 * @see #send(Address, Address, String, String)
	 */
	public void send()
	{
		// Validate an undelivered message exists; should never happen
		if (simpleMessage == null)
		{
			logger.debug("No message to send");
			return;
		}
		if (simpleMessage.isDelivered())
		{
			logger.debug("Message has already been delivered: #0", simpleMessage);
			return;
		}

		// Validate the body has content; should be performed by JSF component's
		// required attribute
		if (simpleMessage.getBody() == null)
		{
			FacesMessages.instance().add(Severity.ERROR, "Message not provided; please enter a message and try again.");
			logger.debug("No content in contact-us message");
			return;
		}

		// If from address is available then validate it as a reply-to
		if ((simpleMessage.getSubject() == null) || (simpleMessage.getSubject().trim().length() == 0))
 		{
			simpleMessage.setSubject( DEFAULT_SUBJECT + DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG).format( new Date() ) ); 
		}
		
		InternetAddress replyTo = null;
		
		if ( linktype != null && linktype.equalsIgnoreCase( EMAIL_LINK_TYPE ) )
		{
			// Then this is an email link and we need to reset the replyTo and From parameters
			String Address = "";
			String Personal = "";
			try 
			{
				Address = DeclarativeMatchingEngine.getProfilebundle().getString( "EmailLink.quickviews.replyToAddress" );
				if ( simpleMessage.getFromTitleName() != null )
				{
					Personal = simpleMessage.getFromTitleName() + " via BCTQuickViews";
				}
				else
				{	
					Personal = DeclarativeMatchingEngine.getProfilebundle().getString( "EmailLink.quickviews.replyToPersonal" );
				}	
				replyTo = new InternetAddress( Address, true );
                replyTo.setPersonal( Personal );
			} 
			catch ( java.util.MissingResourceException e )
			{
				logger.warn("EmailLink Either replyToAddress or replyToPersonal could not be found.", e );
			}
			catch (AddressException e) 
			{
				logger.warn("EmailLink bad Address Exception #0", e, Address );
			} 
			catch (UnsupportedEncodingException e) 
			{
				logger.warn("EmailLink Personal String Exception #0", e, Personal );
			}
		}
		else if (simpleMessage.getFrom() != null)
		{
			try
			{
				replyTo = new InternetAddress(simpleMessage.getFrom(), true);
				if (simpleMessage.getName() != null)
				{	
					replyTo.setPersonal(simpleMessage.getName());
				}	
			}
			catch (UnsupportedEncodingException unsupportedEncodingException)
			{
				// Should never happen as we are using default encoding; ignore
				// as personal name not important
				logger.warn("Bad sender personal name #0", unsupportedEncodingException, simpleMessage.getName());
			}
			catch (AddressException addressException)
			{
				// Invalid email address entered by user: add faces message and
				// return
				FacesMessages.instance().addToControl("from", Severity.ERROR,
						"\"" + simpleMessage.getFrom() + "\" is not a valid email address; please try again.");
				logger.debug("Bad message sender '#0': #1", simpleMessage.getFrom(), addressException);
				return;
			}
		}
		else if (simpleMessage.getName() != null)
		{	
			// Postpend subject if address is not available but name is
			simpleMessage.setSubject( simpleMessage.getSubject() + " (from " + simpleMessage.getName() + ')' );
		}	

		try
		{
			// If successful update delivery status
			send( simpleMessage.getRecipients(), replyTo, simpleMessage.getSubject(), simpleMessage.getBody());
			simpleMessage.setDelivered(true);
			logger.debug("Message sent from #0", replyTo == null ? "anonymous" : replyTo);
		}
		catch (MessagingException messagingException)
		{
			messagingException.printStackTrace();
			// Message service down: create faces message and log warning
			FacesMessages.instance().add(Severity.ERROR, "The message could not be sent.  Please check that the email(s) you provided are correct.");

			logger.warn("Contact Us email service is down while trying to send message from #0 using props #1",
					messagingException, replyTo == null ? "anonymous" : replyTo, session.getProperties());
			emailLinkSuccessful = false;
		}
	}

	/**
	 * Overload of the send message for backwards compatibility.
	 */
	public void send(InternetAddress recipient, InternetAddress replyTo, String subject, String body)
	throws IllegalArgumentException, MessagingException
	{
		if ( recipient == null )
		{
			send( (InternetAddress[]) null, replyTo, subject, body );
		}
		InternetAddress[] recipients = new InternetAddress[1];
		recipients[0] = recipient; 
		send( recipients, replyTo, subject, body );
	}

	/**
	 * Sends a message. The following attributes are retrieved from mail session
	 * properties; (JBoss server is set in deploy/*mail-service.xml):
	 * <ul>
	 * <li><code>mail.transport.protocol</code>: usually smtp or smtps</li>
	 * <li><code>mail.protocol.auth</code>: (optional) if <code>true</code> then
	 * authenticate session</li>
	 * <li><code>mail.protocol.host</code>: (if <code>auth</code> is
	 * <code>true</code>) SMTP server name for authenticated session</li>
	 * <li><code>mail.protocol.user</code>: (if <code>auth</code> is
	 * <code>true</code>) login name for authenticated session</li>
	 * <li><code>mail.protocol.password</code>: (if <code>auth</code> is
	 * <code>true</code>) password for authenticated session</li>
	 * <li><code>mail.protocol.from</code>: senders's email address and
	 * (optional) personal name in parentheses; if not specified then use
	 * <code>user</code></li>
	 * <li><code>mail.protocol.contact</code>: contact's email address and
	 * (optional) personal name in parentheses</li>
	 * <li><code>mail.protocol.divert</code>: (optional) divert email address
	 * and (optional) personal name in parentheses. If a divert address is
	 * specified then the message is encapsulated as an attachment into a second
	 * message and the second message is delivered to the divert address.</li>
	 * </ul>
	 * 
	 * @param recipient
	 *            the recipient or <code>null</code> to specify the session's
	 *            <tt>contact</tt> address
	 * @param replyTo
	 *            the recipient or <code>null</code> to specify the session's
	 *            <tt>contact</tt> address
	 * @param subject
	 *            the subject of the message
	 * @param body
	 *            the body of the message
	 * @throws IllegalArgumentException
	 *             if <code>subject</code> or <code>body</code> is
	 *             <code>null</code>
	 * @throws MessagingException
	 *             if the message could not be sent
	 * @see org.quantumleaphealth.action.MessageSender#send(javax.mail.internet.InternetAddress,
	 *      javax.mail.internet.InternetAddress, java.lang.String,
	 *      java.lang.String)
	 */
	public void send(InternetAddress[] recipients, InternetAddress replyTo, String subject, String body)
			throws IllegalArgumentException, MessagingException
	{
		// Validate session, subject and body
		if (session == null)
			throw new MessagingException("Session not available");
		if ((subject == null) || (subject.trim().length() == 0))
			throw new IllegalArgumentException("Subject not provided");
		
		// If an address is not provided then load the contact address from
		// session properties
		try
		{
			// Default recipient is contact address
			if (recipients == null)
			{
				InternetAddress contact = getSessionAddress(SMTP_ATTRIBUTE_CONTACT);
				if (contact == null)
				{	
					throw new AddressException("Contact address not specified under '" + SMTP_ATTRIBUTE_CONTACT + '\'');
				}	
				
				recipients = new InternetAddress[1];
				recipients[0] = contact;
			}
			// Default reply-to is contact address
			if (replyTo == null)
			{
				replyTo = getSessionAddress(SMTP_ATTRIBUTE_CONTACT);
			}	
		}
		catch (Exception exception)
		{
			// Bad contact address is a serious problem; recast as messaging
			// exception
			throw new MessagingException("Cannot retrieve contact-us email address from session", exception);
		}

		// Get any available diversion address
		InternetAddress divert = null;
		try
		{
			divert = getSessionAddress(SMTP_ATTRIBUTE_DIVERT);
		}
		catch (Exception exception)
		{
			// Bad divert address is a serious problem; recast as messaging
			// exception
			throw new MessagingException("Cannot retrieve session's divert email", exception);
		}

		// Create the message
		MimeMessage message = new MimeMessage(session);
		// From address is required
		message.setFrom();
		if (message.getFrom() == null)
			throw new MessagingException("From address not specified under either '" + SMTP_ATTRIBUTE_FROM + "' or '"
					+ SMTP_ATTRIBUTE_USER + '\'');
		message.setRecipients(Message.RecipientType.TO, recipients);
		
		if (replyTo != null)
		{
			message.setReplyTo(new InternetAddress[] { replyTo });
		}	
		
		message.setSubject(subject);
		message.setContent(body, MIMETYPE_PLAIN);
		message.setSentDate(new Date());
		message.saveChanges();

		// If divert address is available then encapsulate this message into an
		// outer message
		if (divert != null)
		{
			MimeMessage outerMessage = new MimeMessage(session);
			outerMessage.setFrom();
			if (outerMessage.getFrom() == null)
				throw new MessagingException("From address not specified under either '" + SMTP_ATTRIBUTE_FROM
						+ "' or '" + SMTP_ATTRIBUTE_USER + '\'');
			outerMessage.setRecipient(Message.RecipientType.TO, divert);
			outerMessage.setSubject("Test message: " + subject);
			// First part is explanation
			MimeMultipart mimeMultipart = new MimeMultipart();
			MimeBodyPart mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText("The following message was diverted from " + recipients[0] + " to " + divert);
			mimeMultipart.addBodyPart(mimeBodyPart);
			// Second part is encapsulated message
			mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setContent(message, MIME_EMAIL);
			mimeBodyPart.setFileName("diverted.eml");
			mimeMultipart.addBodyPart(mimeBodyPart);
			// Remember to save changes
			outerMessage.setContent(mimeMultipart);
			outerMessage.setSentDate(new Date());
			outerMessage.saveChanges();
			message = outerMessage;
		}

		if ( linktype != null && linktype.equalsIgnoreCase( EMAIL_LINK_TYPE ) )
		{
			message.setFrom( replyTo );
		}	
		// Authenticated transport needs to connect explicitly using host,
		// username and password
		// Unfortunately JavaMail 1.4 does not load the smtps protocol from the
		// mail.transport.protocol attribute automatically
		Transport transport = null;
		try
		{
			transport = session.getTransport();
			// TODO: Verify if this is still necessary; can we simply connect
			// witout parameters?
			if ("true".equals(getSessionProtocolProperty(SMTP_AUTHENTICATE)))
			{	
				transport.connect(getSessionProtocolProperty(SMTP_ATTRIBUTE_HOST),
						getSessionProtocolProperty(SMTP_ATTRIBUTE_USER),
						getSessionProtocolProperty(SMTP_ATTRIBUTE_PASSWORD));
			}	
			else
			{	
				transport.connect();
			}	

			transport.sendMessage(message, message.getAllRecipients());
		}
		finally
		{
			if (transport != null)
				try
				{
					transport.close();
				}
				catch (Throwable throwable)
				{
					logger.error("Could not close mail transport", throwable);
				}
		}
	}

	/**
	 * @param attribute
	 *            the session attribute name for the address
	 * @return the address or <code>null</code> if none specified in session
	 * @throws IllegalArgumentException
	 *             if <code>attribute</code> is empty
	 * @throws IllegalStateException
	 *             if the session or protocol is not available
	 * @throws AddressException
	 *             if the address is not valid
	 * @see #getSessionProtocolProperty(String)
	 */
	private InternetAddress getSessionAddress(String attribute) throws IllegalArgumentException, IllegalStateException,
			AddressException
	{
		String property = getSessionProtocolProperty(attribute);
		// Strict validation will throw AddressException
		return ((property == null) || (property.trim().length() == 0)) ? null : new InternetAddress(property, true);
	}

	/**
	 * @param attribute
	 *            the property's name
	 * @return the SMTP property under the current protocol, e.g.,
	 *         <code>session.getProperty("mail.protocol.attribute")</code>
	 * @throws IllegalArgumentException
	 *             if <code>attribute</code> is empty
	 * @throws IllegalStateException
	 *             if the session or protocol is not available
	 */
	private String getSessionProtocolProperty(String attribute) throws IllegalArgumentException, IllegalStateException
	{
		if ((attribute == null) || (attribute.trim().length() == 0))
			throw new IllegalArgumentException("Attribute not specified");
		if (session == null)
			throw new IllegalStateException("Session not available");
		String protocol = session.getProperty(SMTP_TRANSPORT);
		if ((protocol == null) || (protocol.trim().length() == 0))
			throw new IllegalStateException("Protocol not available in session " + session);
		return session.getProperty(SMTP_PREFIX + protocol + '.' + attribute.trim());
	}

	/**
	 * Version uid for serializable class
	 */
	private static final long serialVersionUID = -5725721915291318426L;
}
