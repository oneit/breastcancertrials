/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;

import org.quantumleaphealth.model.trial.Geocoding;
import org.quantumleaphealth.model.trial.TrialSite;

/**
 * How close a site is to a position.
 * 
 * @author Tom Bechtold
 * @version 2008-12-12
 */
public class TrialSiteProximity implements Comparable<TrialSiteProximity>, Serializable
{
	/**
	 * The units of measure for distance
	 */
	public enum DistanceUnit
	{
		MILE, KILOMETER
	}

	/**
	 * The trial site
	 */
	private final TrialSite trialSite;
	/**
	 * The distance or <code>Double.NaN</code> if unknown
	 */
	private final double distance;
	/**
	 * The distance unit of measure
	 */
	private final DistanceUnit distanceUnit;

	/**
	 * Stores site and unit into instance variables and finds distance.
	 * 
	 * @param trialSite
	 *            the trial site
	 * @param position
	 *            the position or <code>null</code> for no proximity
	 * @param distanceUnit
	 *            the unit of measure
	 * @throws IllegalArgumentException
	 *             if <code>trialSite</code> is <code>null</code> or
	 *             <code>distanceUnit</code> is <code>null</code> when
	 *             <code>position</code> is non-<code>null</code>
	 */
	TrialSiteProximity(TrialSite trialSite, Geocoding position, DistanceUnit distanceUnit)
			throws IllegalArgumentException
	{
		// Validate parameters
		if (trialSite == null)
			throw new IllegalArgumentException("trialsite must be specified");
		if (trialSite.getSite() == null)
			throw new IllegalArgumentException("site must be specified");
		if ((position != null) && (distanceUnit == null))
			throw new IllegalArgumentException("unit must be specified with position");
		this.trialSite = trialSite;
		this.distanceUnit = distanceUnit;
		// If position is not given then store large negative number in the
		// instance variable
		if (position == null)
			distance = Double.NaN;
		else
		{
			double distanceMiles = position.getDistanceMiles(trialSite.getSite().getGeocoding());
			// Convert distance units from miles
			if (!Double.isNaN(distanceMiles))
				if (DistanceUnit.KILOMETER.equals(distanceUnit))
					distanceMiles *= 1.609344d;
				else if (!DistanceUnit.MILE.equals(distanceUnit))
					throw new IllegalArgumentException("distance unit " + distanceUnit + " not supported");
			distance = distanceMiles;
		}
	}

	/**
	 * @return the trial site
	 */
	public TrialSite getTrialSite()
	{
		return trialSite;
	}

	/**
	 * @return the distance or <code>Double.NaN</code> if unknown
	 */
	public double getDistance()
	{
		return distance;
	}

	/**
	 * @return whether the distance is known
	 */
	public boolean isKnown()
	{
		return !Double.isNaN(distance);
	}

	/**
	 * @return the distance unit of measure
	 */
	public DistanceUnit getDistanceUnit()
	{
		return distanceUnit;
	}

	/**
	 * Returns whether its trial is closer or (if proximity is unavailable) has
	 * more sites than another trial. This method first tests if both
	 * proximities are unavailable; if so, then it uses number of sites for each
	 * trial. If either proximity is not available then it is deemed further
	 * than the other.
	 * 
	 * @return a negative number if a) its distance is closer than the other's
	 *         or b) neither proximity is available and its number of sites is
	 *         more than the other's; zero if the distances are equal or
	 *         positive otherwise
	 * @param the
	 *            other trial's proximity
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(TrialSiteProximity other)
	{
		// Validate parameter
		if (other == null)
			throw new NullPointerException("other site proximity");
		// If both distances unknown then compare country, political subunit,
		// city
		if (!isKnown() && !other.isKnown())
		{
			// Known data is always before unknown data
			if ((trialSite.getSite() == null) && (other.trialSite.getSite() == null))
				return 0;
			if (other.trialSite.getSite() == null)
				return -1;
			if (trialSite.getSite() == null)
				return 1;
			// Known data is always before unknown data
			int comparison = compare(trialSite.getSite().getCountryCode(), other.trialSite.getSite().getCountryCode());
			if (comparison != 0)
				return comparison;
			comparison = compare(trialSite.getSite().getPoliticalSubUnitName(), other.trialSite.getSite()
					.getPoliticalSubUnitName());
			if (comparison != 0)
				return comparison;
			return compare(trialSite.getSite().getCity(), other.trialSite.getSite().getCity());
		}
		// Known proximity is always closer than unknown
		if (!other.isKnown())
			return -1;
		if (!isKnown())
			return 1;
		return (int) Math.signum(distance - other.distance);
	}

	/**
	 * Returns how two strings compare alphabetically Known strings are ordered
	 * before unknown ones.
	 * 
	 * @param string1
	 *            the first string
	 * @param string2
	 *            the second string
	 * @return how two strings compare alphabetically
	 */
	private static int compare(String string1, String string2)
	{
		if ((string1 == null) && (string2 == null))
			return 0;
		if (string2 == null)
			return -1;
		if (string1 == null)
			return 1;
		return string1.compareToIgnoreCase(string2);
	}

	/**
	 * Returns whether this proximity contains the same trialsite as anothers'
	 * 
	 * @return whether this proximity contains the same trialsite as anothers'
	 * @param other
	 *            the other site proximity
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object other)
	{
		if (super.equals(other))
			return true;
		return (other != null) && (other instanceof TrialSiteProximity)
				&& trialSite.equals(((TrialSiteProximity) (other)).trialSite);
	}

	/**
	 * Returns the hash code of the owned site
	 * 
	 * @return the hash code of the owned site
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode()
	{
		return trialSite == null ? super.hashCode() : trialSite.hashCode();
	}

	/**
	 * Returns the class name, memory location, trial site, distance and unit
	 * 
	 * @return the class name, memory location, trial site, distance and unit
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		return getClass().getName() + '@' + super.hashCode() + '{' + trialSite + '='
				+ (!isKnown() ? '?' : Double.toString(distance) + distanceUnit) + '}';
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = 1016663494680837427L;
}
