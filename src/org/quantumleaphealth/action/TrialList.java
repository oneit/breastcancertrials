/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import static org.jboss.seam.ScopeType.EVENT;

import java.io.Serializable;
import java.util.Iterator;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.action.TrialSiteProximity.DistanceUnit;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.screen.MatchingEngine;

/**
 * The list of trials. This class supports the following sorting:
 * <ul>
 * <li>Number of research sites descending</li>
 * <li>Distance of closest research site ascending</li>
 * <li>Trial type ascending</li>
 * </ul>
 * This object is stored in the Seam event so it is valid only for a single web
 * request.
 * 
 * @author Tom Bechtold
 * @version 2009-03-09
 */
@Stateful
@Name("trialBrowser")
@Scope(EVENT)
public class TrialList implements TrialBrowser, Serializable
{
	/**
	 * The matching engine
	 */
	@In
	private MatchingEngine engine;
	/**
	 * The geocoding engine
	 */
	@EJB
	private PostalCodeManager postalCodeManager;
	/**
	 * The administrator interface for the current user or <code>null</code> if
	 * current user never accessed the administrator interface
	 */
	@In(required = false, scope = ScopeType.SESSION)
	private AdministratorInterface administratorInterface;
	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log logger;

    /**
     * urlrewrite.xml may passes this parameter in for primary and secondary
     * trial browser pages.
     */
	@RequestParameter(value="categorystring")
	private String categoryString;
	
	/**
	 * The trials sorted by type and closest research site (miles) or
	 * <code>null</code> if not generated yet
	 */
	private TrialProximityGroups trials;

	/**
	 * Returns the user's postal code or <code>null</code> if none Note: JSF's
	 * convertNumber only works with <code>Long</code>.
	 * 
	 * @return the user's postal code or <code>null</code> if none
	 * @return the user's postal code or <code>null</code> if none
	 * @see org.quantumleaphealth.action.TrialBrowser#getPostalCode()
	 */
	public Long getPostalCode()
	{
		int postalCode = getTrials().getPostalCode();
		return postalCode <= 0 ? null : Long.valueOf(postalCode);
	}

	/**
	 * Stores the user's postal code and clears the list of sorted trials. This
	 * method does nothing if the parameter is <code>null</code> or has not
	 * changed.
	 * 
	 * @param postalCode
	 *            the user's postal code or <code>null</code> if none
	 * @see org.quantumleaphealth.action.TrialBrowser#setPostalCode(java.lang.Integer)
	 */
	public void setPostalCode(Long postalCode)
	{
		// Do nothing if null or not changing
		if ((postalCode == null) || postalCode.equals(getPostalCode()))
			return;
		logger.debug("Setting zip code to sort trials to #0", postalCode);
		getTrials().setPostalCodeGeocoding(postalCode.intValue(),
				postalCodeManager.getPostalCodeGeocoding(postalCode.intValue()).getGeocoding());
	}

	/**
	 * @return the trials sorted by treatment type and closest research site,
	 *         guaranteed to be non-<code>null</code>
	 * @see org.quantumleaphealth.action.TrialBrowser#getTrials()
	 */
	public TrialProximityGroups getTrials()
	{

		// Generate on demand
		if (trials == null)
		{
			// Populate with engine's trials
			trials = new TrialProximityGroups(DistanceUnit.MILE);
			Iterator<Trial> iterator = engine.getTrialIterator();
			while (iterator.hasNext())
			{
				Trial trial = ( Trial ) iterator.next();

				// The category string parameter will be defined for quickviews.  If it is we only want trials
				// that match it.
				if ( getCategoryString() == null || trial.isInCategory( getCategoryString() ) )
				{
					trials.add( trial ); 
				}	
			}	
		}
		return trials;
	}

	/**
	 * @return whether the current user is logged in as administrator
	 * @see org.quantumleaphealth.action.TrialBrowser#isReloadable()
	 */
	public boolean isReloadable()
	{
		return (administratorInterface != null) && administratorInterface.isLoggedIn();
	}

	/**
	 * Do nothing when stateful session bean is destroyed.
	 */
	@Remove
	@Destroy
	public void destroy()
	{
	}

	public String getCategoryString() 
	{
		return categoryString;
	}

	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = 1081609403020193129L;
}
