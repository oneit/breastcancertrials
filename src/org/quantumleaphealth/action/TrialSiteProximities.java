/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.util.ArrayList;
import java.util.Collections;

import org.quantumleaphealth.action.TrialSiteProximity.DistanceUnit;
import org.quantumleaphealth.model.trial.Geocoding;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.TrialSite;

/**
 * The research site proximities for a trial. This class does not completely
 * safeguard against adding sites that do not share the same trial.
 * 
 * @author Tom Bechtold
 * @version 2008-12-12
 */
public class TrialSiteProximities extends ArrayList<TrialSiteProximity>
{
	/**
	 * Whether or not at least one site has an available proximity
	 */
	private boolean available;

	/**
	 * Builds and sorts the site proximity list for a trial.
	 * 
	 * @param trial
	 *            the trial to get the proximities for
	 * @param position
	 *            the position to calculate proximity to or <code>null</code>
	 *            for no position
	 * @param distanceUnit
	 *            the unit to calculate proximity to or <code>null</code> for no
	 *            position
	 * @throws IllegalArgumentException
	 *             if <code>trial</code> is <code>null</code> or
	 *             <code>distanceUnit</code> is <code>null</code> when
	 *             <code>position</code> is non-<code>null</code>
	 */
	public TrialSiteProximities(Trial trial, Geocoding position, DistanceUnit distanceUnit)
			throws IllegalArgumentException
	{
		super(trial.getTrialSite().size());
		if (trial == null)
			throw new IllegalArgumentException("no trial specified");
		if ((position != null) && (distanceUnit == null))
			throw new IllegalArgumentException("No unit for proximity to specified position");
		// Set available flag if any site has a position
		available = false;
		for (TrialSite trialSite : trial.getTrialSite())
		{
			super.add(new TrialSiteProximity(trialSite, position, distanceUnit));
			if (!available && (trialSite.getSite() != null) && (trialSite.getSite().getGeocoding() != null))
				available = true;
		}
		// Sort the list
		Collections.sort(this);
	}

	/**
	 * Adds a site to the list and updates the available flag. This method
	 * safeguards against adding sites that do not share the same trial.
	 * 
	 * @param trialSiteProximity
	 *            the proximity to add
	 * @return <code>true</code> per superclass' contract
	 * @see java.util.ArrayList#add(java.lang.Object)
	 */
	@Override
	public boolean add(TrialSiteProximity trialSiteProximity) throws IllegalArgumentException
	{
		if ((trialSiteProximity == null) || (trialSiteProximity.getTrialSite() == null))
			throw new IllegalArgumentException("No trial site specified");
		if ((size() > 0)
				&& ((trialSiteProximity.getTrialSite().getTrial() == null) || get(0).getTrialSite().getTrial().getId()
						.equals(trialSiteProximity.getTrialSite().getTrial().getId())))
			throw new IllegalArgumentException("Trial site does not share the same trial");
		if (!available && (trialSiteProximity.getTrialSite() != null)
				&& (trialSiteProximity.getTrialSite().getSite() != null)
				&& (trialSiteProximity.getTrialSite().getSite().getGeocoding() != null))
			available = true;
		Collections.sort(this);
		return super.add(trialSiteProximity);
	}

	/**
	 * @return whether at least one site has a geocoding
	 */
	public boolean isAvailable()
	{
		return available;
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = -7331068009089459173L;
}
