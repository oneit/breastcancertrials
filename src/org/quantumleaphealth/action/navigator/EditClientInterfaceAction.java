package org.quantumleaphealth.action.navigator;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BILATERAL_ASYNCHRONOUS;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREAST_CANCER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.METASTATIC;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.PREVENTION;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RECURRENT_LOCALLY;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.SINGULAR;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.SURVIVOR;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.Column;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.navigator.Navigator;
import org.quantumleaphealth.model.navigator.Navigator_Client;
import org.quantumleaphealth.model.patient.Invitation;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;

@Stateful
@Name("editClientInterface")
public class EditClientInterfaceAction implements EditClientInterface
{
	@Logger
	private static Log logger;

	/**
	 * Persists the entity beans using EJB3 persistence context
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;

	//The navigator is injected from the NavigatorInterfaceAction navigator instance.
	@In
	Navigator navigator;
	
	@In
	FacesMessages facesMessages;
	
	// The instance that will be exposed on the navigator_create_new_client.xhtml page and
	// will be saved.  If we are editing and existing client then it will be injected from the NavigatorInterfaceAction class.
	@In(required=false)
	private Navigator_Client navigatorClient;
	
	@In(required=false)
	private UserPatient userPatient;
	
	// These variables will be used to create the new UserPatient entry.
	private String newClientPrincipal;
	
	private String newClientValidatePrincipal;
	
	private String newClientPassword;
	
	private String newClientPasswordVerification;
	
	/**
	 * First three digits of postal code or <code>null</code> if not entered or
	 * not applicable. Note: JavaServerFaces convertNumber validation does not
	 * work on <code>Short</code> and <code>Integer</code>, so use
	 * <code>Long</code>
	 */
	private Long privatePostalCode;

	/**
	 * Whether or not the patient will be alerted to eligible newly posted
	 * trials
	 */
	private boolean alertNewTrialsEnabled;
	
	/**
	 * Whether or not the navigator will be alerted to eligible newly posted
	 * trials for this client
	 */
	private boolean navigatorAlertNewTrialsEnabled;

	/**
	 * The patient's category in bit-wise format or <code>0</code> if not
	 * answered yet.
	 * 
	 * @see #setPatient(UserPatient)
	 */
	private long category;
	
	/**
	 * The patient's primary type in bit-wise format or <code>0</code> if not
	 * answered yet
	 * 
	 * @see #setPatient(UserPatient)
	 */
	private long primary;

	ArrayList<String> errors = new ArrayList<String>();		

	public EditClientInterfaceAction()
	{
		navigatorClient = new Navigator_Client();
	}
	
	@Override
	public void cancelNewClient() 
	{
	}

	@Override
	public void deleteClient()
	{
		if (getNavigatorClient() != null)
		{
			navigatorClient = em.merge(navigatorClient); // reattach the detached navigatorClient
			em.remove(this.navigatorClient);
			//donate the userpatient record associated with this client
			if (this.userPatient != null)
			{
				userPatient = em.merge(userPatient);
				logger.debug("Donating patient ##0", userPatient.getId());
				// Attach userPatient to persistence engine
				// Because we are deleting the userPatient and its associations, we do not
				// need to re-attach invitations' transient field
				userPatient.setPrincipal(null);
				userPatient.setCredentials(null);
				userPatient.setSecretQuestion(null);
				userPatient.setSecretAnswer(null);
				userPatient.setAlertNewTrialsEnabled(false);
				userPatient.setPrincipalInvalidEmailAddress(true);
				// Remove invitations but keep screenedTrials
				Iterator<Invitation> invitations = userPatient.getInvitations().iterator();
				while (invitations.hasNext())
				{
					Invitation invitation = invitations.next();
					invitations.remove();
					if (invitation.getId() != null)
						em.remove(invitation);
				}
				em.flush();
				logger.debug("userPatient donated");
			}
		}	
	}
	
	private boolean validateMatchingEntries(String valueString, String validateString)
	{
		return !((valueString != null ^ validateString != null) || valueString.trim().equals(validateString.trim()) == false);
	}

	private boolean clientExists()
	{
		Long count = (Long) em.createQuery("select count(*) from UserPatient up where upper(up.principal) = upper(:principal)")
							.setParameter("principal", this.newClientPrincipal).getSingleResult();
		return count > 0;
	}
	
	private void applyPrimary(PatientHistory ph)
	{
		if (ph == null)
		{
			return;
		}

		if (primary == 2l)
		{
			ph.getValueHistory().put(BREAST_CANCER, SINGULAR);
		}
		else if (primary == 4l)
		{
			ph.getValueHistory().put(BREAST_CANCER, RECURRENT_LOCALLY);
		}	
		else if (primary == 8l)
		{
			ph.getValueHistory().put(BREAST_CANCER, BILATERAL_ASYNCHRONOUS);
		}	
	}
	
	private void applyCategory(PatientHistory ph)
	{
		if (ph == null)
		{
			return;
		}
		
		if (category == 2l)
		{	
			ph.getValueHistory().put(BREAST_CANCER, PREVENTION);
		}	
		else if (category == 8l)
		{
			ph.getValueHistory().put(BREAST_CANCER, METASTATIC);
		}	
		else if (category == 16l)
		{
			ph.getValueHistory().put(BREAST_CANCER, SURVIVOR);
		}	
		else if (category != 4l)
		{	
			ph.getValueHistory().remove(BREAST_CANCER);
		}	
	}

//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private boolean persistExistingClient()
	{
		this.navigatorClient = em.merge(navigatorClient);
		this.userPatient = em.merge(userPatient);
		try
		{		
			userPatient.setAlertNewTrialsEnabled(this.alertNewTrialsEnabled);
			userPatient.setCredentials(this.newClientPassword);
			userPatient.setPrincipal(this.newClientPrincipal);
			userPatient.setPrivatePostalCode(this.privatePostalCode);
			navigatorClient.setNavigatorAlertNewTrialsEnabled(this.navigatorAlertNewTrialsEnabled);
			em.persist(userPatient);
			em.persist(navigatorClient);
		}
		catch (Throwable t)
		{
			t.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * do not use getters here as this will reset the value to the currently persisted ones.
	 */
	@Override
	public void updateClient() 
	{
		if (validateMatchingEntries(this.getNewClientPrincipal(), this.getNewClientValidatePrincipal()) == false)
		{
			addError("Both Email Entries must match.");
			return;
		}

		if (validateMatchingEntries(this.getNewClientPassword(), this.getNewClientPasswordVerification()) == false)
		{
			addError("Both Password Entries must match.");
			return;
		}
		persistExistingClient();
	}

	private void addError(String msg)
	{
		facesMessages.add(Severity.ERROR, msg);
		errors.add(msg);
	}
	
	public List<String> getErrors()
	{
		return errors;
	}
	
	/**
	 * @param navigatorClient the navigatorClient to set
	 */
	public void setNavigatorClient(Navigator_Client navigatorClient) 
	{
		this.navigatorClient = navigatorClient;
	}

	/**
	 * @return the navigatorClient
	 */
	public Navigator_Client getNavigatorClient() 
	{
		return navigatorClient;
	}

	/**
	 * @param principal the principal to set
	 */
	public void setNewClientPrincipal(String newClientPrincipal) 
	{
		this.newClientPrincipal = newClientPrincipal;
	}

	/**
	 * @return the principal
	 */
	public String getNewClientPrincipal() 
	{
		if (userPatient != null && userPatient.getPrincipal() != null)
		{
			return userPatient.getPrincipal();
		}
		return newClientPrincipal;
	}

	@Override
	public String getNewClientValidatePrincipal() 
	{
		if (userPatient != null && userPatient.getPrincipal() != null)
		{
			return userPatient.getPrincipal();
		}
		return newClientValidatePrincipal;
	}

	@Override
	public void setNewClientValidatePrincipal(String newClientValidatePrincipal)
	{
		this.newClientValidatePrincipal = newClientValidatePrincipal;
	}

	/**
	 * @return the first three digits of postal code or <code>null</code> if not
	 *         entered or not applicable
	 */
	public Long getPrivatePostalCode()
	{
		if (userPatient != null && userPatient.getPrivatePostalCode() != null)
		{
			return userPatient.getPrivatePostalCode();
		}
		return privatePostalCode;
	}

	/**
	 * @param privatePostalCode
	 *            the first three digits of postal code to set or
	 *            <code>null</code> if not entered or not applicable
	 */
	public void setPrivatePostalCode(Long privatePostalCode)
	{
		this.privatePostalCode = privatePostalCode;
	}
	
	/**
	 * @param category the category to set
	 */
	public void setCategory(long category) 
	{
		this.category = category;
	}

	/**
	 * @return the category
	 */
	public long getCategory() 
	{
		return category;
	}

	/**
	 * @param primary the primary to set
	 */
	public void setPrimary(long primary) 
	{
		this.primary = primary;
	}

	/**
	 * @return the primary
	 */
	public long getPrimary() 
	{
		return primary;
	}

	/**
	 * @param newClientPassword the newClientPassword to set
	 */
	public void setNewClientPassword(String newClientPassword) 
	{
		this.newClientPassword = newClientPassword;
	}

	/**
	 * @return the newClientPassword
	 */
	public String getNewClientPassword() 
	{
		if (userPatient != null && userPatient.getCredentials() != null)
		{
			newClientPassword = userPatient.getCredentials();
		}
		return newClientPassword;
	}

	/**
	 * @param newClientPasswordVerification the newClientPasswordVerification to set
	 */
	public void setNewClientPasswordVerification(String newClientPasswordVerification) 
	{
		this.newClientPasswordVerification = newClientPasswordVerification;
	}

	/**
	 * @return the newClientPasswordVerification
	 */
	public String getNewClientPasswordVerification() 
	{
		if (userPatient != null && userPatient.getCredentials() != null)
		{
			newClientPasswordVerification = userPatient.getCredentials();
		}
		return newClientPasswordVerification;
	}

	/**
	 * @param alertNewTrialsEnabled the alertNewTrialsEnabled to set
	 */
	public void setAlertNewTrialsEnabled(boolean alertNewTrialsEnabled) 
	{
		this.alertNewTrialsEnabled = alertNewTrialsEnabled;
	}

	/**
	 * @return the alertNewTrialsEnabled
	 */
	public boolean isAlertNewTrialsEnabled() 
	{
		if (userPatient != null && userPatient.isAlertNewTrialsEnabled())
		{
			alertNewTrialsEnabled = userPatient.isAlertNewTrialsEnabled();
		}
		return alertNewTrialsEnabled;
	}

	/**
	 * @param userPatient the userPatient to set
	 */
	public void setUserPatient(UserPatient userPatient) {
		this.userPatient = userPatient;
	}

	/**
	 * @return the userPatient
	 */
	public UserPatient getUserPatient() {
		return userPatient;
	}

	@Override
	@Remove
	@Destroy
	public void destroy() 
	{
	}

	@Override
	public boolean isNavigatorAlertNewTrialsEnabled() 
	{
		if (navigatorClient != null)
		{
			navigatorAlertNewTrialsEnabled = navigatorClient.isNavigatorAlertNewTrialsEnabled();
		}
		return navigatorAlertNewTrialsEnabled;
	}

	@Override
	public void setNavigatorAlertNewTrialsEnabled(boolean navigatorAlertNewTrialsEnabled) 
	{
		this.navigatorAlertNewTrialsEnabled = navigatorAlertNewTrialsEnabled;
	}
}

