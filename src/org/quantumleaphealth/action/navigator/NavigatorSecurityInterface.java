package org.quantumleaphealth.action.navigator;

import java.util.List;

import javax.ejb.Local;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.quantumleaphealth.model.navigator.Navigator;

@Local
public interface NavigatorSecurityInterface 
{
	public Navigator getNavigator(); 

	public void setPrincipal(String validatePrincipal);
	
	public String getPrincipal(); 
	
	public void setValidatePrincipal(String validatePrincipal);
	
	public String getValidatePrincipal(); 

	public void setCredentials(String credentials);
	
	public String getCredentials();
	
	public void load();
	
	public void setNavigator(Navigator navigator);

	public void persistNewNavigator();

	public void update();
	
	public void cancelUpdate();

	public List<String> getErrors();
	
	public void validateTermsAccepted(FacesContext facesContext, UIComponent component, Object value);

	public void destroy();
}
