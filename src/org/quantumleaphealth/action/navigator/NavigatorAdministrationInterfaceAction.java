package org.quantumleaphealth.action.navigator;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.navigator.Navigator;

@Stateful
@Scope(ScopeType.SESSION)
@Name("navigatorAdministrationInterface")
public class NavigatorAdministrationInterfaceAction implements	NavigatorAdministrationInterface 
{
	@Logger
	private static Log logger;

	/**
	 * Persists the entity beans using EJB3 persistence context
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;
	
	@DataModel
	private List<Navigator> navigators;
	
	/**
	 * The instance selected from the JSF datatable.
	 */
	@SuppressWarnings("unused")
	@DataModelSelection("navigators")
    private Navigator navigatorSelection;
	

	@SuppressWarnings("unchecked")
	@Override
	@Factory("navigators")
	public void loadNavigators()
	{
		logger.debug("Loading navigator list.");
		setNavigators(em.createQuery("select n from Navigator n order by principal").getResultList());
		logger.debug("Finished Loading navigator list.");
	}

	/**
	 * @param navigators the navigators to set
	 */
	private void setNavigators(List<Navigator> navigators) 
	{
		this.navigators = navigators;
	}

	/**
	 * @return the navigators
	 */
	public List<Navigator> getNavigators() 
	{
		return navigators;
	}

	@Remove
	@Destroy
	@Override
	public void destroy() 
	{
	}
}
