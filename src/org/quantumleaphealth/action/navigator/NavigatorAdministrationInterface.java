package org.quantumleaphealth.action.navigator;

import javax.ejb.Local;

@Local
public interface NavigatorAdministrationInterface 
{
	public void loadNavigators();

	public void destroy();
}
