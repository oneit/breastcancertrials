package org.quantumleaphealth.action.navigator;

import java.util.List;

import javax.ejb.Local;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.quantumleaphealth.action.FacadeAttributeCharacteristic;
import org.quantumleaphealth.action.FacadeValueCharacteristic;
import org.quantumleaphealth.action.PatientInterface;
import org.quantumleaphealth.model.navigator.Navigator_Client;

@Local
public interface CreateClientInterface 
{
	public void saveNewClient();

	public void cancelNewClient();
	
	public void deleteNewClient();

	public Navigator_Client getNavigatorClient();
	
	public void validateCredentials(FacesContext facesContext, UIComponent component, Object value);
	
	public void setNewClientPrincipal(String newClientValidatePrincipal);
	
	public String getNewClientPrincipal();	

	public void setNewClientValidatePrincipal(String newClientValidatePrincipal);
	
	public String getNewClientValidatePrincipal(); 

	public Long getPrivatePostalCode();
	
	public void setPrivatePostalCode(Long privatePostalCode);
	
	public void setCategory(long category);
	
	public long getCategory();
	
	public void setPrimary(long primary);
	
	public long getPrimary(); 
	
	public void setRecentlyStartedTreatmentPrimary(long recentlyStartedTreatmentPrimary);
	
	public long getRecentlyStartedTreatmentPrimary(); 

	public void setNewClientPassword(String newClientPassword);
	
	public String getNewClientPassword();
	
	public FacadeValueCharacteristic[] getValueCharacteristics();
	
	public FacadeAttributeCharacteristic[] getAttributeCharacteristics();
	
	public void setNewClientPasswordVerification(String newClientPasswordVerification);
	
	public String getNewClientPasswordVerification();
	
	public void setAlertNewTrialsEnabled(boolean alertNewTrialsEnabled);
	
	public boolean isAlertNewTrialsEnabled(); 
	
	public void setNavigatorAlertNewTrialsEnabled(boolean navigatorAlertNewTrialsEnabled);
	
	public boolean isNavigatorAlertNewTrialsEnabled(); 
	
	public List<String> getErrors();
	
	public void setPatientInterface(PatientInterface patientInterface);

	public PatientInterface getPatientInterface();

	public void destroy();
}
