package org.quantumleaphealth.action.navigator;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.NoResultException;

import org.hibernate.validator.Email;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotEmpty;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.navigator.Navigator;

@Stateful
@Name("navigatorSecurityInterface")
public class NavigatorSecurityInterfaceAction implements NavigatorSecurityInterface
{
	@Logger
	private static Log logger;

	/**
	 * Persists the entity beans using EJB3 persistence context
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;
	
	//The navigator *may* be injected from the NavigatorInterfaceAction navigator instance.
	//I guess I can't use the create=true because Navigator is not listed as a Component (it is an entity).
	@In(required=false)
	@Out(scope=ScopeType.SESSION,required=false)
	private	Navigator navigator;
	
	/**
	 * Another cross-reference so that the navigator can navigate directly to the welcome page after being
	 * created.
	 */
	@In(required=false)
	NavigatorInterface navigatorInterface;
	
	@In
	FacesMessages facesMessages;
	
	private String principal;
	/**
	 * Used to double check the Principal entry.
	 */
	private String validatePrincipal;
	
	private String credentials;

	ArrayList<String> errors = new ArrayList<String>();		

	NavigatorSecurityInterfaceAction()
	{
		if (navigator == null)
		{
			navigator = new Navigator();
		}
	}
	/**
	 * @return the navigator
	 */
	public Navigator getNavigator() 
	{
		if (navigator == null)
		{
			navigator = new Navigator();
		}
		return navigator;
	}

	public void setNavigator(Navigator navigator)
	{
		this.navigator = navigator;
	}
	
	/**
	 * @param principal the principal to set
	 */
	public void setPrincipal(String principal) 
	{
		this.principal = principal;
	}

	/**
	 * @return the principal.  the Hibernate Validations will *not* be called unless the
	 * field referenced in the .xhmtl is 
	 * a.) required
	 * b.) encloses the <s:validate /> tag.
	 */
	@Length(min = 2, max=64)
	@Email(message="{InvalidEmailMessage}")
	@NotEmpty
	public String getPrincipal() 
	{
		return principal;
	}

	/**
	 * @param validatePrincipal the validatePrincipal to set
	 */
	public void setValidatePrincipal(String validatePrincipal) 
	{
		this.validatePrincipal = validatePrincipal;
	}

	/**
	 * @return the validatePrincipal
	 */
	public String getValidatePrincipal() 
	{
		return validatePrincipal;
	}

	/**
	 * @param newPasswordVerification the newPasswordVerification to set
	 */
	public void setCredentials(String credentials) 
	{
		this.credentials = credentials;
	}

	/**
	 * @return the newPasswordVerification
	 */
	public String getCredentials() 
	{
		return credentials;
	}

	// BCT-922: Added load method to initialize the screen fields
	// Do this only for email addresses, not the password due to security concerns
	public void load() {
		setPrincipal(getNavigator().getPrincipal());
		setValidatePrincipal(getNavigator().getPrincipal());
	}
	
	private boolean validatePassword()
	{
		String password = this.getNavigator().getCredentials();
		String verification = this.getCredentials();
		return !((password != null ^ verification != null) || password.trim().equals(verification.trim()) == false);
	}
	
	private void persistUpdate()
	{
		navigator.setModified(new Date());
		navigator.setModified_by(navigator.getPrincipal());
		setNavigator(em.merge(navigator));
		em.flush();
		logger.debug("Reset Navigator ##0 password", navigator.getId());
	}

	private boolean validateEmail()
	{
		if (!this.getPrincipal().trim().equalsIgnoreCase(this.getValidatePrincipal().trim()))
		{
			facesMessages.add(Severity.ERROR, "'Email Adress' and 'Verify email address' must be the same.");
			return false;
		}

		try
		{
			String principalFound = (String) em.createQuery("select principal from Navigator where Principal = :Principal").setParameter("Principal", this.getPrincipal().trim()).getSingleResult();
			if (principalFound != null)
			{
				facesMessages.add(Severity.ERROR, "The email address " + getPrincipal() + " aleady exists in the database.  Please make another choice.");
				return false;
			}
		}
		catch (NoResultException nre)
		{
			// this is actually the result we want to see.
			return true;
		}
		
		return true;
	}

	/**
	 * Validates that the termsAccepted checkbox was checked. This method adds a
	 * non-localized faces messages if the component was not checked.
	 * 
	 * @param facesContext
	 *            the faces context
	 * @param component
	 *            the checkbox component
	 * @param value
	 *            the component's value
	 * @see org.quantumleaphealth.action.PatientInterface#validateTermsAccepted(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object)
	 */
	public void validateTermsAccepted(FacesContext facesContext, UIComponent component, Object value)
	{
		logger.debug("Validating termsAccepted '#0'", value);
		if (!Boolean.TRUE.equals(value))
		{
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"Please check the Terms & Conditions box before clicking the Start button.");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
		}
	}
	
	public void persistNewNavigator()
	{
		try
		{
			if (validateEmail())
			{	
				getNavigator().setPrincipal(this.getPrincipal().trim());
				// BCT-922 - No one remembers having this requirement to set initial password to "123456"
				// getNavigator().setCredentials("123456");
				em.persist(navigator);
				
				if (navigatorInterface != null)
				{
					navigatorInterface.setNavigator(navigator);
					navigatorInterface.login();
				}
			}	
		}
		catch (Exception re)
		{
			facesMessages.add(Severity.FATAL, "Error saving " + getPrincipal() + ", please contact bcttrials.org.");
		}
	}
	
	public void update()
	{
		if (validatePassword() == false)
		{
			addError("Both Password Entries must match.");
			return;
		}
		persistUpdate();
	}

	public void cancelUpdate()
	{
	}
	
	private void addError(String msg)
	{
		facesMessages.add(Severity.ERROR, msg);
		errors.add(msg);
	}
	
	public List<String> getErrors()
	{
		return errors;
	}
	
	@Override
	@Remove
	@Destroy
	public void destroy() 
	{
	}
}
