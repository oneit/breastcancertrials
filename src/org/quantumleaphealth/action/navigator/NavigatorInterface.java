package org.quantumleaphealth.action.navigator;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Local;

import org.quantumleaphealth.action.PatientInterface;
import org.quantumleaphealth.model.navigator.Navigator;
import org.quantumleaphealth.model.navigator.Navigator_Client;

@Local
public interface NavigatorInterface extends Serializable 
{
	/**
	 * The non-localized message when authentication fails
	 */
	String ERRORMESSAGE_UNAUTHENTICATED = "The email address or password you entered is not correct. Please try again";

	public void login();
	
	public void logout();

	public boolean isLoggedIn();

	public Navigator getNavigator();
	
	public void setNavigator(Navigator navigator); 

	public void createNavigatorClientList();
	
	public List<Navigator_Client> getNavigatorClientList();
	
	public void setNavigatorClient(Navigator_Client navigatorClient);
	
	public Navigator_Client getNavigatorClient(); 
	
	public void setPatientInterface(PatientInterface patientInterface);
	
	public PatientInterface getPatientInterface(); 

	public void editClient(Navigator_Client navigatorClient);
	
	public void loginClient(Navigator_Client navigatorClient);

	public void destroy();
}
