package org.quantumleaphealth.action.navigator;

import java.util.List;

import javax.ejb.Local;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.quantumleaphealth.model.navigator.Navigator_Client;

@Local
public interface EditClientInterface 
{
	public void updateClient();

	public void cancelNewClient();
	
	public void deleteClient();

	public Navigator_Client getNavigatorClient();
	
	public void setNewClientPrincipal(String newClientValidatePrincipal);
	
	public String getNewClientPrincipal();	

	public void setNewClientValidatePrincipal(String newClientValidatePrincipal);
	
	public String getNewClientValidatePrincipal(); 

	public Long getPrivatePostalCode();
	
	public void setPrivatePostalCode(Long privatePostalCode);
	
	public void setCategory(long category);
	
	public long getCategory();
	
	public void setPrimary(long primary);
	
	public long getPrimary(); 

	public void setNewClientPassword(String newClientPassword);
	
	public String getNewClientPassword();
	
	public void setNewClientPasswordVerification(String newClientPasswordVerification);
	
	public String getNewClientPasswordVerification();
	
	public void setAlertNewTrialsEnabled(boolean alertNewTrialsEnabled);
	
	public boolean isAlertNewTrialsEnabled(); 

	public void setNavigatorAlertNewTrialsEnabled(boolean navigatorAlertNewTrialsEnabled);
	
	public boolean isNavigatorAlertNewTrialsEnabled(); 

	public List<String> getErrors();
	
	public void destroy();
}
