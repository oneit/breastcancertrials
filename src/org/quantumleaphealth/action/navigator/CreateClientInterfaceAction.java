package org.quantumleaphealth.action.navigator;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.ANATOMIC_SITE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BILATERAL_ASYNCHRONOUS;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.BREAST_CANCER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.EDUCATION;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.HISPANIC;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.METASTATIC;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.METASTATIC_BREAST_CANCER;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RACE;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.RECURRENT_LOCALLY;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.SINGULAR;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.SURVIVOR;
import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.UNSURE;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.action.FacadeAttributeCharacteristic;
import org.quantumleaphealth.action.FacadePatientHistory;
import org.quantumleaphealth.action.FacadeValueCharacteristic;
import org.quantumleaphealth.action.PatientInterface;
import org.quantumleaphealth.action.PatientInterfaceAction;
import org.quantumleaphealth.model.navigator.Navigator_Client;
import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.patient.ValueHistory;
import org.quantumleaphealth.model.patient.UserPatient.UserType;
import org.quantumleaphealth.ontology.CharacteristicCode;

@Stateful
@Name("createClientInterface")
public class CreateClientInterfaceAction implements CreateClientInterface
{
	@Logger
	private static Log logger;

	/**
	 * Persists the entity beans using EJB3 persistence context
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;
	
	//The navigator is injected from the NavigatorInterfaceAction navigator instance.
	@In
	NavigatorInterface navigatorInterface;
	
	@In
	FacesMessages facesMessages;
	
	@In
	private	PatientInterface patientInterface;
	
	// The instance that will be exposed on the navigator_create_new_client.xhtml page and
	// will be saved.
	private Navigator_Client navigatorClient;
	
	// These variables will be used to create the new UserPatient entry.
	private String newClientPrincipal;
	
	private String newClientValidatePrincipal;
	
	private String newClientPassword;
	
	private String newClientPasswordVerification;
	
	/**
	 * First three digits of postal code or <code>null</code> if not entered or
	 * not applicable. Note: JavaServerFaces convertNumber validation does not
	 * work on <code>Short</code> and <code>Integer</code>, so use
	 * <code>Long</code>
	 */
	private Long privatePostalCode;

	/**
	 * Whether or not the patient will be alerted to eligible newly posted
	 * trials
	 */
	private boolean alertNewTrialsEnabled;

	/**
	 * Whether or not the navigator will be alerted to eligible newly posted
	 * trials for this client
	 */
	private boolean navigatorAlertNewTrialsEnabled;

	/**
	 * The patient's category in bit-wise format or <code>0</code> if not
	 * answered yet.
	 * 
	 * @see #setPatient(UserPatient)
	 */
	private long category;
	
	/**
	 * The patient's primary type in bit-wise format or <code>0</code> if not
	 * answered yet
	 * 
	 * @see #setPatient(UserPatient)
	 */
	private long primary;
	
	/**
	 * for the recently Started Treatment Category
	 */
	private long recentlyStartedTreatmentPrimary;

	/**
	 * set validation errors
	 */
	ArrayList<String> errors = new ArrayList<String>();
	
	/**
	 * local copies of race and education
	 */
	private final FacadeValueCharacteristic[] valueCharacteristics;
	/**
	 * The patient's value history, guaranteed to be non-<code>null</code>.
	 */
	private ValueHistory valueHistory;

	
	private final FacadeAttributeCharacteristic[] attributeCharacteristics;

	private PatientHistory history = new PatientHistory();

	/**
	 * static for unsure in race and the Hispanic Ethnicity question.
	 */
	private static final HashSet<CharacteristicCode> unsure = new HashSet<CharacteristicCode>();
	{
		unsure.add(UNSURE);
	}	

	public CreateClientInterfaceAction()
	{
		navigatorClient = new Navigator_Client();
		history = new PatientHistory();
		valueHistory = history.getValueHistory();

		valueCharacteristics = new FacadeValueCharacteristic[FacadePatientHistory.VALUE_CHARACTERISTICS.size()];
		Iterator<Map.Entry<CharacteristicCode, CharacteristicCode[]>> entryIterator = FacadePatientHistory.VALUE_CHARACTERISTICS.entrySet().iterator();
		int entryIndex = 0;
		while (entryIterator.hasNext())
		{
			Map.Entry<CharacteristicCode, CharacteristicCode[]> entry = entryIterator.next();
			valueCharacteristics[entryIndex] = new FacadeValueCharacteristic(entry.getKey(), entry.getValue(), valueHistory);
			entryIndex++;
		}
		
		attributeCharacteristics = new FacadeAttributeCharacteristic[FacadePatientHistory.ATTRIBUTE_CHARACTERISTICS.size()];
		entryIterator = FacadePatientHistory.ATTRIBUTE_CHARACTERISTICS.entrySet().iterator();
		entryIndex = 0;
		while (entryIterator.hasNext())
		{
			Map.Entry<CharacteristicCode, CharacteristicCode[]> entry = entryIterator.next();
			attributeCharacteristics[entryIndex] = new FacadeAttributeCharacteristic(entry.getKey(), entry.getValue(),
					history);
			entryIndex++;
		}

	}
	
	@Override
	public void cancelNewClient() 
	{
	}

	@Override
	public void deleteNewClient()
	{
	}
	
	private boolean validateMatchingEntries(String valueString, String validateString)
	{
		return !((valueString != null ^ validateString != null) || valueString.trim().equals(validateString.trim()) == false);
	}

	private boolean clientExists()
	{
		Long count = (Long) em.createQuery("select count(*) from UserPatient up where upper(up.principal) = upper(:principal)")
							.setParameter("principal", this.newClientPrincipal).getSingleResult();
		return count > 0;
	}
	
	private void applyPrimary(PatientHistory ph)
	{
		if (ph == null)
		{
			return;
		}

		if (primary == 2l)
		{
			ph.getValueHistory().put(BREAST_CANCER, SINGULAR);
		}
		else if (primary == 4l)
		{
			ph.getValueHistory().put(BREAST_CANCER, RECURRENT_LOCALLY);
		}	
		else if (primary == 8l)
		{
			ph.getValueHistory().put(BREAST_CANCER, BILATERAL_ASYNCHRONOUS);
		}	
	}
	
	private void applyRecentlyStartedTreatmentPrimary(PatientHistory ph)
	{
		if (ph == null)
		{
			return;
		}

		if (recentlyStartedTreatmentPrimary == 2l)
		{
			ph.getValueHistory().put(BREAST_CANCER, SINGULAR);
		}
		else if (recentlyStartedTreatmentPrimary == 4l)
		{
			ph.getValueHistory().put(BREAST_CANCER, RECURRENT_LOCALLY);
		}	
		else if (recentlyStartedTreatmentPrimary == 8l)
		{
			ph.getValueHistory().put(BREAST_CANCER, BILATERAL_ASYNCHRONOUS);
		}	
	}
	
	private void applyCategory(PatientHistory ph)
	{		
		if (ph == null)
		{
			return;
		}
		
		if (category == 8l)
		{
			ph.getValueHistory().put(BREAST_CANCER, METASTATIC);
			Diagnosis diagnosis = new Diagnosis();
			diagnosis.getValueHistory().put(ANATOMIC_SITE, METASTATIC_BREAST_CANCER);
			ph.getDiagnoses().add(diagnosis);
		}	
		else if (category == 16l)
		{
			ph.getValueHistory().put(BREAST_CANCER, SURVIVOR);
		}	
	}
	
	private void applyGeneralCharacteristics(PatientHistory ph)
	{
		if (ph == null)
		{
			return;
		}
		
		// set education level
		if (valueHistory.get(EDUCATION) != null)
		{
			ph.getValueHistory().put(EDUCATION, valueHistory.get(EDUCATION));
		}

		// set race for all available except for hispanic
		Set<CharacteristicCode> race = history.getAttributeCharacteristics().get(RACE);
		if (race != null)
		{
			// any other answers are meaningless if Skip is clicked.
			if (race.contains(UNSURE))
			{
				ph.getAttributeCharacteristics().put(RACE, unsure);
			}
			else
			{	
				ph.getAttributeCharacteristics().put(RACE,race);
			}
		}
		else
		{
			// set a race value of 'Skip' (UNSURE) if the navigator refuses to do so.
			ph.getAttributeCharacteristics().put(RACE, unsure);
		}

		// set the race other text box
		if (getPatientInterface().getPatient().getPatientHistory().getStringCharacteristics().containsKey(RACE))
		{
			// then the race other text box has been used.
			ph.getStringCharacteristics().put(RACE, getPatientInterface().getPatient().getPatientHistory().getStringCharacteristics().get(RACE));
		}

		Set<CharacteristicCode> hispanicQuestion = history.getAttributeCharacteristics().get(HISPANIC);
		if (hispanicQuestion != null)
		{
			ph.getAttributeCharacteristics().put(HISPANIC, hispanicQuestion);
		}
		else
		{
			// set a value of 'Skip' (UNSURE) for the hispanic question if the navigator refuses to do so.
			ph.getAttributeCharacteristics().put(HISPANIC, unsure);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private boolean persistNewClient()
	{
		try
		{
			UserPatient up = new UserPatient();
			
			up.setAlertNewTrialsEnabled(this.isAlertNewTrialsEnabled());
			
			// set principal and credentials automatically for users who are not shared.
			if (this.getNavigatorClient().isShared())
			{
				up.setPrincipal(this.getNewClientPrincipal());
				up.setCredentials(this.getNewClientPassword());
			}
			else
			{
				String newPrincipalBase = navigatorClient.getLastName() == null? navigatorInterface.getNavigator().getLastName(): navigatorClient.getLastName();
				up.setPrincipal(newPrincipalBase + up.getUniqueId());
				up.setCredentials("123456");
			}
			
			up.setPrincipalInvalidEmailAddress(true);
			up.setPrivatePostalCode(this.getPrivatePostalCode());
			up.setCountryCode(PatientInterfaceAction.COUNTRY_UNITEDSTATES);
			up.setTermsAccepted(true);
			up.setUsertype(UserType.USER_PATIENT);  //clients are always userpatient types.
			applyPrimary(up.getPatientHistory());
			applyRecentlyStartedTreatmentPrimary(up.getPatientHistory());
			applyCategory(up.getPatientHistory());
			applyGeneralCharacteristics(up.getPatientHistory());
			
			em.persist(up);
			
			navigatorClient.setCreated(new Date());
			navigatorClient.setCreated_by(navigatorInterface.getNavigator().getPrincipal());
			navigatorClient.setId(new Navigator_Client.Navigator_ClientPK(navigatorInterface.getNavigator().getId(), up.getId()));
			navigatorClient.setNavigatorAlertNewTrialsEnabled(navigatorAlertNewTrialsEnabled);
			
			// Shared already set by this point.
			em.persist(navigatorClient);
			em.flush();
			
			if (getPatientInterface().isLoggedIn())
			{
				getPatientInterface().logout();
			}
			
			//reset the facadeHistory to the userpatient just created.
			if (getPatientInterface().getHistory() != null)
			{	
				getPatientInterface().getHistory().resetFacadePatientHistory(up.getPatientHistory());
			}	
			
			// set the current navigator client to point to the new instance so that the id information shows up correctly in the breadcrumb row.
			navigatorInterface.setNavigatorClient(navigatorClient);
		}
		catch (Throwable t)
		{
			t.printStackTrace();
			return false;
		}
		return true;
	}
	
	private boolean validateRequiredFields()
	{
		boolean passedValidation = true;
		if (this.getNewClientPrincipal()  == null || this.getNewClientPrincipal().trim().length() <= 0)
		{
			passedValidation = false;
		}
		else if (this.getNewClientValidatePrincipal()  == null || this.getNewClientValidatePrincipal().trim().length() <= 0)
		{
			passedValidation = false;
		}
		else if (this.getNewClientPassword()  == null || this.getNewClientPassword().trim().length() <= 0)
		{
			passedValidation = false;
		}
		else if (this.getNewClientPasswordVerification()  == null || this.getNewClientPasswordVerification().trim().length() <= 0)
		{
			passedValidation = false;
		}
		
		if (!passedValidation)
		{
			addError("Please enter a user name and password for this client.");
		}
		return passedValidation;
	}
	
	@Override
	public void saveNewClient() 
	{
		if (this.getNavigatorClient().isShared() == true)
		{	
			if (validateRequiredFields() == false)
			{
				return;
			}
			if (validateMatchingEntries(this.getNewClientPrincipal(), this.getNewClientValidatePrincipal()) == false)
			{
				addError("Both Email Entries must match.");
				return;
			}
	
			if (validateMatchingEntries(this.getNewClientPassword(), this.getNewClientPasswordVerification()) == false)
			{
				addError("Both Password Entries must match.");
				return;
			}
	
			if (clientExists())
			{
				addError("A User Patient with this email address exists already.  Please change the client's email.");
				return;
			}
		}
			
		// first persist a new userPatientId
		if (persistNewClient() == false)
		{
			addError("Error creating a new User Patient record for the client submitted.  Please notify the help desk of this failure.");
			return;
		}
		// at this point we should have successfully set the userpatient for the PatientInterface to the user we just created.
		getPatientInterface().login();
	}

	private void addError(String msg)
	{
		facesMessages.add(Severity.ERROR, msg);
		errors.add(msg);
	}
	
	public List<String> getErrors()
	{
		return errors;
	}
	
	/**
	 * @param navigatorClient the navigatorClient to set
	 */
	public void setNavigatorClient(Navigator_Client navigatorClient) 
	{
		this.navigatorClient = navigatorClient;
	}

	/**
	 * @return the navigatorClient
	 */
	public Navigator_Client getNavigatorClient() 
	{
		return navigatorClient;
	}

	public void validateCredentials(FacesContext facesContext, UIComponent component, Object value)
	{
	}
	
	/**
	 * @param principal the principal to set
	 */
	public void setNewClientPrincipal(String newClientPrincipal) 
	{
		this.newClientPrincipal = newClientPrincipal;
	}

	/**
	 * @return the principal
	 */
	public String getNewClientPrincipal() 
	{
		return newClientPrincipal;
	}

	@Override
	public String getNewClientValidatePrincipal() 
	{
		return newClientValidatePrincipal;
	}

	@Override
	public void setNewClientValidatePrincipal(String newClientValidatePrincipal)
	{
		this.newClientValidatePrincipal = newClientValidatePrincipal;
	}

	/**
	 * @return the first three digits of postal code or <code>null</code> if not
	 *         entered or not applicable
	 */
	public Long getPrivatePostalCode()
	{
		return privatePostalCode;
	}

	/**
	 * @param privatePostalCode
	 *            the first three digits of postal code to set or
	 *            <code>null</code> if not entered or not applicable
	 */
	public void setPrivatePostalCode(Long privatePostalCode)
	{
		this.privatePostalCode = privatePostalCode;
	}
	
	/**
	 * @param category the category to set
	 */
	public void setCategory(long category) 
	{
		this.category = category;
	}

	/**
	 * @return the category
	 */
	public long getCategory() 
	{
		return category;
	}

	/**
	 * @param primary the primary to set
	 */
	public void setPrimary(long primary) 
	{
		this.primary = primary;
	}

	/**
	 * @return the primary
	 */
	public long getPrimary() 
	{
		return primary;
	}

	/**
	 * @param recentlyStartedTreatmentPrimary the recentlyStartedTreatmentPrimary to set
	 */
	public void setRecentlyStartedTreatmentPrimary(long recentlyStartedTreatmentPrimary) 
	{
		this.recentlyStartedTreatmentPrimary = recentlyStartedTreatmentPrimary;
	}

	/**
	 * @return the recentlyStartedTreatmentPrimary
	 */
	public long getRecentlyStartedTreatmentPrimary() 
	{
		return recentlyStartedTreatmentPrimary;
	}

	/**
	 * @return the valueCharacteristics
	 */
	public FacadeValueCharacteristic[] getValueCharacteristics() {
		return valueCharacteristics;
	}

	/**
	 * @return the attributeCharacteristics
	 */
	public FacadeAttributeCharacteristic[] getAttributeCharacteristics() {
		return attributeCharacteristics;
	}

	/**
	 * @param newClientPassword the newClientPassword to set
	 */
	public void setNewClientPassword(String newClientPassword) 
	{
		this.newClientPassword = newClientPassword;
	}

	/**
	 * @return the newClientPassword
	 */
	public String getNewClientPassword() 
	{
		return newClientPassword;
	}

	/**
	 * @param newClientPasswordVerification the newClientPasswordVerification to set
	 */
	public void setNewClientPasswordVerification(String newClientPasswordVerification) 
	{
		this.newClientPasswordVerification = newClientPasswordVerification;
	}

	/**
	 * @return the newClientPasswordVerification
	 */
	public String getNewClientPasswordVerification() 
	{
		return newClientPasswordVerification;
	}

	/**
	 * @param alertNewTrialsEnabled the alertNewTrialsEnabled to set
	 */
	public void setAlertNewTrialsEnabled(boolean alertNewTrialsEnabled) 
	{
		this.alertNewTrialsEnabled = alertNewTrialsEnabled;
	}

	/**
	 * @return the alertNewTrialsEnabled
	 */
	public boolean isAlertNewTrialsEnabled() 
	{
		return alertNewTrialsEnabled;
	}

	/**
	 * @param navigatorAlertNewTrialsEnabled the navigatorAlertNewTrialsEnabled to set
	 */
	public void setNavigatorAlertNewTrialsEnabled(boolean navigatorAlertNewTrialsEnabled) 
	{
		this.navigatorAlertNewTrialsEnabled = navigatorAlertNewTrialsEnabled;
	}

	/**
	 * @return the navigatorAlertNewTrialsEnabled
	 */
	public boolean isNavigatorAlertNewTrialsEnabled() 
	{
		return navigatorAlertNewTrialsEnabled;
	}

	/**
	 * @param patientInterface the patientInterface to set
	 */
	public void setPatientInterface(PatientInterface patientInterface) {
		this.patientInterface = patientInterface;
	}

	/**
	 * @return the patientInterface
	 */
	public PatientInterface getPatientInterface() {
		return patientInterface;
	}

	@Override
	@Remove
	@Destroy
	public void destroy() 
	{
	}
}
