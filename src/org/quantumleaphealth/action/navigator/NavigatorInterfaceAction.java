package org.quantumleaphealth.action.navigator;

import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;

import org.jboss.annotation.ejb.SerializedConcurrentAccess;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.intercept.BypassInterceptors;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.jboss.seam.web.Session;
import org.quantumleaphealth.action.PatientInterface;
import org.quantumleaphealth.model.navigator.Navigator;
import org.quantumleaphealth.model.navigator.Navigator_Client;
import org.quantumleaphealth.model.patient.UserPatient;

@Stateful
@Scope(ScopeType.SESSION)
@Name("navigatorInterface")
@SerializedConcurrentAccess // force SEAM to serialize across conversations.  This bean has a relationship with the PatientInterfaceAction bean which is also a SESSION level bean.
public class NavigatorInterfaceAction implements NavigatorInterface
{
	/**
	 * jaxen exception if not serializable?
	 */
	private static final long serialVersionUID = -6174642708930987067L;

	@Logger
	private static Log logger;

	@Out(required=false)
	private Navigator navigator;

	@Out(required=false)
	private UserPatient userPatient;
	
	/**
	 * Make this list a JSF data model object using the @DataModel annotation.
	 */
	@SuppressWarnings("unused")
	private List<Navigator_Client> navigatorClientList;

	@Out(required=false)
	private Navigator_Client navigatorClient;

	@In
	private
	PatientInterface patientInterface;

	/**
	 * Persists the entity beans using EJB3 persistence context
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;

	public NavigatorInterfaceAction()
	{
		navigator = new Navigator();
	}
	
	public void login() 
	{
		// Step 1: Validate logged in
		if (isLoggedIn())
		{	
			return;
		}	

		// Step 2: Validate principal (redundant: should be checked by JSF
		// required attribute)
		String principal = (navigator == null) ? null : navigator.getPrincipal();
		if ((principal == null) || (principal.length() == 0))
		{
			logger.debug("Blank patient principal");
			FacesMessages.instance().addToControl("username", Severity.ERROR,
				"Please enter your email address and try again");
			return;
		}
		Navigator retrievedNavigator;
		try
		{
			// Step 3: Load record; fetch screened-trial association eagerly;
			// invitation association will be fetched by
			// updateScreenedTrialsAndInvitations()
			// WARNING: Do NOT eagerly fetch both associations in same query as
			// Hibernate returns duplicate screened-trial objects
			// when duplicate (erroneous) invitations exist (e.g., same
			// mechanism as the multiple-bag fetch problem)
			retrievedNavigator = (Navigator) (em.createQuery(
							"select n from Navigator n where LOWER(n.principal)=LOWER(:principal)")
					.setParameter("principal", principal).getSingleResult());
		}
		catch (NoResultException noResultException)
		{
			// Step 4a: Validate single record
			logger.debug("Patient '#0' not found", principal);
			FacesMessages
					.instance()
					.add(
							Severity.ERROR,
							(navigator.getCredentials() == null) ? "The email address you entered was not found. Please try again"
									: ERRORMESSAGE_UNAUTHENTICATED);
			return;
		}
		catch (NonUniqueResultException nonUniqueResultException)
		{
			// Step 4b: Validate single record
			logger.error("Navigator '#0' found in multiple records", nonUniqueResultException, principal);
			FacesMessages
					.instance()
					.add(Severity.ERROR,
							"Your record is not available. A message has been sent to the system administrator to fix the problem.");
			return;
		}
		
		// Step 6: Compare available credentials case-insensitive
		if (navigator.getCredentials() != null)
		{
			if ((retrievedNavigator.getCredentials() == null)
					|| !navigator.getCredentials().equalsIgnoreCase(retrievedNavigator.getCredentials()))
			{
				// Failed: Update the unsuccessful count (both persisted and
				// cached)
				FacesMessages.instance().add(Severity.ERROR, ERRORMESSAGE_UNAUTHENTICATED);
				logger.debug("Bad navigator credentials '#1' for '#0'", principal, navigator.getCredentials());
				return;
			}

			// Passed: reset the unauthenticated count, load the returning user
			// and update matches
			setNavigator(retrievedNavigator);
			
			// Flush the entity manager so SQL/debug logging is in chronological
			// order
			em.flush();
			logger.info("Authenticated and loaded '#0' as ##1", navigator.getPrincipal(), navigator.getId());
			
			return;
		}
	}

	/**
	 * @param navigator the navigator to set
	 */
	public void setNavigator(Navigator navigator) 
	{
		this.navigator = navigator;
	}

	/**
	 * @return the navigator
	 */
	public Navigator getNavigator() 
	{
		return navigator;
	}

	public void logout()
	{
		setNavigator(null);
		Session.instance().invalidate();
	}

	@Factory("navigatorClientList")
	public void createNavigatorClientList()
	{
		navigatorClientList = this.getNavigatorClientList();
	}
	
	@SuppressWarnings("unchecked")
	public List<Navigator_Client> getNavigatorClientList()
	{
        List<Navigator_Client> myList = 
        	em.createQuery("select nc from Navigator_Client nc where nc.id.navigatorId = :navigatorId ").
        		setParameter("navigatorId", getNavigator().getId()).getResultList();
		return myList;
	}
	
	/**
	 * @param navigatorClient the navigatorClient to set
	 */
	public void setNavigatorClient(Navigator_Client navigatorClient) 
	{
		this.navigatorClient = navigatorClient;
	}

	/**
	 * @return the navigatorClient
	 */
	public Navigator_Client getNavigatorClient() 
	{
		return navigatorClient;
	}

	/**
	 * @param patientInterface the patientInterface to set
	 */
	public void setPatientInterface(PatientInterface patientInterface) 
	{
		this.patientInterface = patientInterface;
	}

	/**
	 * @return the patientInterface
	 */
	public PatientInterface getPatientInterface() 
	{
		return patientInterface;
	}

	/**
	 * @return whether the patient is represented in the database
	 * @see org.quantumleaphealth.action.PatientInterface#isLoggedIn()
	 */
	@BypassInterceptors
	public boolean isLoggedIn()
	{
		return (navigator != null) && (navigator.getId() != null);
	}

	public void editClient(Navigator_Client navigatorClient)
	{
		setNavigatorClient(navigatorClient);
		this.userPatient = navigatorClient.getUserPatient();
	}
	
	@Override
	public void loginClient(Navigator_Client navigatorClient)
	{
		setNavigatorClient(navigatorClient);
//		UserPatient up = em.find(UserPatient.class, navigatorClient.getId().getUserpatientId());
//
////		up.setId(null);
//		patientInterface.setPatient(up);
//		patientInterface.setNavigatorPid(navigatorClient.getId().getUserpatientId());
		
		// log out the current userpatient being managed by the navigator if moving to another client
		if (patientInterface.isLoggedIn())
		{
			patientInterface.logout();
		}
		patientInterface.login();
	}
	
	@Remove
	@Destroy
	public void destroy() 
	{
	}
}
