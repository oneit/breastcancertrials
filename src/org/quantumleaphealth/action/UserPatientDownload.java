package org.quantumleaphealth.action;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.quantumleaphealth.xml.DecodingException;

@XmlRootElement(name="patient")
public class UserPatientDownload {
	
	private String principal;
	private String patientHistoryMarshaled;

	@XmlAttribute(name="principal")
	public String getPrincipal()
	{
		return principal;
	}

	public void setPrincipal(String principal)
	{
		this.principal = principal;
	}

	@XmlAttribute(name="history")
	public String getPatientHistoryMarshaled()
	{
		return patientHistoryMarshaled;
	}

	public void setPatientHistoryMarshaled(String patientHistoryMarshaled) throws DecodingException
	{
		this.patientHistoryMarshaled = patientHistoryMarshaled;
	}

}
