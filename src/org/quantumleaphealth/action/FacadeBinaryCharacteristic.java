/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import org.quantumleaphealth.ontology.BinaryCharacteristicHolder;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Represents a facade into a set of binary characteristics. This serializable
 * JavaBean exposes the <code>booleanValue</code> and <code>shortValue</code>
 * field to the GUI and provides a localized label. This class supports
 * representing the boolean characteristic using a numeric value in order to
 * capture whether the question regarding the characteristic has ever been
 * answered (usually with a radio buttons "Yes" and "No"). The applicable values
 * for the numeric representation are:
 * <ul>
 * <li>0: Question has never been answered (e.g., "fresh")</li>
 * <li>2: Characteristic not present, e.g., boolean value is <code>false</code></li>
 * <li>4: Characteristic present, e.g., boolean value is <code>true</code></li>
 * </ul>
 * It caches the existence of the characteristic in order to reduce effort
 * querying the holder.
 * 
 * @author Tom Bechtold
 * @version 2008-05-30
 */
public class FacadeBinaryCharacteristic extends FacadeBinary
{
	/**
	 * The code of the characteristic to represent, guaranteed to be non-
	 * <code>null</code>
	 */
	private final CharacteristicCode characteristicCode;
	/**
	 * Holds the binary characteristics, guaranteed to be non-<code>null</code>
	 */
	private BinaryCharacteristicHolder binaryCharacteristicHolder;

	/**
	 * Validates and stores the parameter and sets the cached value
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to represent
	 * @param binaryCharacteristicHolder
	 *            holds the binary characteristics
	 * @param fresh
	 *            whether or not the question has been answered
	 * @throws IllegalArgumentException
	 *             if any parameter is <code>null</code>
	 */
	FacadeBinaryCharacteristic(CharacteristicCode characteristicCode,
			BinaryCharacteristicHolder binaryCharacteristicHolder) throws IllegalArgumentException
	{
		if (characteristicCode == null)
			throw new IllegalArgumentException("characteristic not specified");
		this.characteristicCode = characteristicCode;
		setBinaryCharacteristicHolder(binaryCharacteristicHolder, true);
	}

	/**
	 * Validates and stores the parameter and updates the cached value and
	 * freshness
	 * 
	 * @param binaryCharacteristicHolder
	 *            holds the binary characteristics
	 * @param fresh
	 *            whether or not the question has been answered
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	void setBinaryCharacteristicHolder(BinaryCharacteristicHolder binaryCharacteristicHolder, boolean fresh)
			throws IllegalArgumentException
	{
		if (binaryCharacteristicHolder == null)
			throw new IllegalArgumentException("holder not specified");
		this.binaryCharacteristicHolder = binaryCharacteristicHolder;
		update(fresh);
	}

	/**
	 * @return the underlying value from the holder
	 * @see org.quantumleaphealth.action.FacadeBinary#getValue()
	 */
	@Override
	protected boolean getValue()
	{
		return binaryCharacteristicHolder.containsCharacteristic(characteristicCode);
	}

	/**
	 * @param value
	 *            the underlying value to set in the holder
	 * @see org.quantumleaphealth.action.FacadeBinary#setValue(boolean)
	 */
	@Override
	protected void setValue(boolean value)
	{
		if (value)
			binaryCharacteristicHolder.addCharacteristic(characteristicCode);
		else
			binaryCharacteristicHolder.removeCharacteristic(characteristicCode);
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = -4630982237150324765L;
}
