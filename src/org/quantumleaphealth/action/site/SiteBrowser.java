package org.quantumleaphealth.action.site;

import java.util.List;

import javax.ejb.Local;

import org.quantumleaphealth.model.trial.TrialSite;

@Local
public interface SiteBrowser 
{
	public abstract List<TrialSite> getTrialSites();
	
	/**
	 * Needed by EJB3
	 */
	public void destroy();
}
