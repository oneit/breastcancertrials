package org.quantumleaphealth.action.site;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.TrialSite;

/**
 * Sample use: http://localhost:8090/bct_nation/public_trialsite.seam?trialId=759365
 * 
 * @author wgweis
 *
 */
@Stateful
@Name("siteBrowser")
public class SiteBrowserAction implements SiteBrowser
{
	/**
	 * Persists the entity beans using EJB3 persistence context
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;

    @RequestParameter( "trialId" )
    private Long trialId;

	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log logger;

	private List<TrialSite> trialSites = null;

	private List<TrialSite> loadTrialSites()
	{
		List<TrialSite> trialSites = new ArrayList<TrialSite>();
		
		if (trialId != null)
		{
			try
			{
				Trial trial = (Trial) em.createQuery("select t from Trial t where id = :trialId")
							.setHint("org.hibernate.readOnly", Boolean.TRUE)
							.setParameter("trialId", trialId)
							.getSingleResult();
				
				trialSites = trial.getByStateTrialSites();
				logger.debug("Found site list of size " + trialSites.size() + " for Id " + trialId);
			}
			catch (Exception e)
			{
				logger.info("Exception retrieving results for trialId " + trialId + ": " + e);
			}
		}
		return trialSites;
	}

	public List<TrialSite> getTrialSites() 
	{
		if (trialSites == null)
		{
			trialSites = loadTrialSites();
		}
		return trialSites;
	}

	/**
	 * Reset instance variables when stateful session bean is destroyed.
	 * Required by jboss EJB. 
	 */	
	@Remove
	@Destroy
	@Override
	public void destroy()
	{
	}
}
