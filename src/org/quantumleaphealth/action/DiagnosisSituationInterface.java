package org.quantumleaphealth.action;

import javax.ejb.Local;

@Local
public interface DiagnosisSituationInterface 
{
	public String redirectToPublicMyTrials();
	
	public String getPatientProfile();
	
	public void setHostname(String hostname); 

	public String getHostname();
	
	public void setCategory(String category);

	public String getCategory(); 

	public void destroy();
}
