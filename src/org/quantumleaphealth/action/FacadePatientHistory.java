/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.*;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.quantumleaphealth.model.patient.BinaryHistory;
import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.Procedure;
import org.quantumleaphealth.model.patient.Therapy;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quantumleaphealth.model.patient.ValueHistory;
import org.quantumleaphealth.model.patient.YearMonthHistory;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * An interface into a patient's history that supports incomplete and invalid
 * answers. This serializable JavaBean distributes and exposes its patient
 * history backing properties via several sub-facades that each represent a
 * patient characteristic.
 * 
 * @author Tom Bechtold
 * @version 2009-02-18
 */
public class FacadePatientHistory implements Serializable
{
	/**
	 * The patient's value history, guaranteed to be non-<code>null</code>.
	 */
	private ValueHistory valueHistory;
	/**
	 * The patient's category in bit-wise format or <code>0</code> if not
	 * answered yet.
	 * 
	 * @see #setPatient(UserPatient)
	 */
	private long category;
	/**
	 * The patient's primary type in bit-wise format or <code>0</code> if not
	 * answered yet
	 * 
	 * @see #setPatient(UserPatient)
	 */
	private long primary;

	/**
	 * Represents the binary characteristics, guaranteed to be non-
	 * <code>null</code>
	 */
	private final FacadeBinaryCharacteristic[] binaryCharacteristics;
	/**
	 * The characteristics that constitute the binary characteristics. This
	 * array-index-to-characteristic-code relationship is used by callers to
	 * reference the appropriate characteristic.
	 */
	private static final CharacteristicCode[] BINARY_CHARACTERISTICS = { PREGNANCY, NURSING, GENETIC_TESTING,
			PARTICIPATION_CURRENT, HIV, DIABETES, OSTEOPOROSIS, HISPANIC, ASHKENAZI_JEWISH };

	/**
	 * Represents the value characteristics, guaranteed to be non-
	 * <code>null</code>
	 */
	private final FacadeValueCharacteristic[] valueCharacteristics;
	/**
	 * The characteristics that constitute the value characteristics. This
	 * array-index-to-characteristic-code relationship is used by callers to
	 * reference the appropriate characteristic.
	 */
	public static final LinkedHashMap<CharacteristicCode, CharacteristicCode[]> VALUE_CHARACTERISTICS = new LinkedHashMap<CharacteristicCode, CharacteristicCode[]>(
			7, 1.0f);
	static
	{
		VALUE_CHARACTERISTICS.put(GENDER, new CharacteristicCode[] { FEMALE, MALE });
		VALUE_CHARACTERISTICS.put(MENOPAUSAL,
				new CharacteristicCode[] { PREMENOPAUSAL, PERIMENOPAUSAL, POSTMENOPAUSAL });
		VALUE_CHARACTERISTICS.put(MENOPAUSE, new CharacteristicCode[] { MENOPAUSE_NATURAL, MENOPAUSE_OOPHRECTOMY,
				MENOPAUSE_RADIATION, MENOPAUSE_HORMONE, MENOPAUSE_CHEMO });
		VALUE_CHARACTERISTICS.put(HORMONE_REPLACEMENT_THERAPY, new CharacteristicCode[] { NEGATIVE, COMPLETED,
				CONCURRENT });
		VALUE_CHARACTERISTICS.put(BRCA1TEST, new CharacteristicCode[] { BRCA1TEST_NEGATIVE, BRCA1TEST_POSITIVE });
		VALUE_CHARACTERISTICS.put(BRCA2TEST, new CharacteristicCode[] { BRCA2TEST_NEGATIVE, BRCA2TEST_POSITIVE });
		VALUE_CHARACTERISTICS.put(ECOG_ZUBROD, new CharacteristicCode[] { ECOG_ZUBROD_0, ECOG_ZUBROD_1, ECOG_ZUBROD_2,
				ECOG_ZUBROD_3, ECOG_ZUBROD_4 });
		VALUE_CHARACTERISTICS.put(EDUCATION, new CharacteristicCode[] { EDUCATION_LESS_HIGH_SCHOOL,
				EDUCATION_HIGH_SCHOOL, EDUCATION_LESS_COLLEGE, EDUCATION_COLLEGE, EDUCATION_POSTGRADUATE });
		VALUE_CHARACTERISTICS.put(LYMPHEDEMA, new CharacteristicCode[] { NEGATIVE, AFFIRMATIVE, UNSURE });
		//VALUE_CHARACTERISTICS.put(PDL1, new CharacteristicCode[] { PDL1_POSITIVE, PDL1_NEGATIVE });
		//VALUE_CHARACTERISTICS.put(ANDROGENRECEPTOR, new CharacteristicCode[] { ANDROGENRECEPTOR_POSITIVE, ANDROGENRECEPTOR_NEGATIVE });
		//VALUE_CHARACTERISTICS.put(PI3K, new CharacteristicCode[] { PI3K_POSITIVE, PI3K_NEGATIVE });
	}

	/**
	 * Represents the attribute characteristics, guaranteed to be non-
	 * <code>null</code>
	 */
	private final FacadeAttributeCharacteristic[] attributeCharacteristics;
	/**
	 * The characteristics that constitute the attribute characteristics. This
	 * array-index-to-characteristic-code relationship is used by callers to
	 * reference the appropriate characteristic.
	 */
	public static final LinkedHashMap<CharacteristicCode, CharacteristicCode[]> ATTRIBUTE_CHARACTERISTICS = new LinkedHashMap<CharacteristicCode, CharacteristicCode[]>(
			13, 1.0f);
	static
	{
		ATTRIBUTE_CHARACTERISTICS.put(PRIMARYCANCER, new CharacteristicCode[] { PRIMARYCANCER_BONE, PRIMARYCANCER_CNS,
				PRIMARYCANCER_CERVICAL_INVASIVE, PRIMARYCANCER_CERVICAL_INSITU, PRIMARYCANCER_COLON_RECTAL,
				PRIMARYCANCER_HODGKINS, PRIMARYCANCER_INTESTINAL, PRIMARYCANCER_KIDNEY, PRIMARYCANCER_LEUKEMIA,
				PRIMARYCANCER_LUNG, PRIMARYCANCER_LYMPHOMA, PRIMARYCANCER_OVARIAN, PRIMARYCANCER_PANCREATIC,
				PRIMARYCANCER_PROSTATE, PRIMARYCANCER_BASAL_SQUAMOUS, PRIMARYCANCER_MELANOMA, PRIMARYCANCER_THYROID,
				PRIMARYCANCER_UTERINE });
		ATTRIBUTE_CHARACTERISTICS.put(HEMATOPOIETIC, new CharacteristicCode[] { HEMATOPOIETIC_ANEMIA,
				HEMATOPOIETIC_BLEEDING_DIATHESIS });
		ATTRIBUTE_CHARACTERISTICS.put(AUTOIMMUNE,
				new CharacteristicCode[] { SCLERODERMA, SYSTEMIC_LUPUS_ERYTHEMATOSUS });
		ATTRIBUTE_CHARACTERISTICS.put(PULMONARY, new CharacteristicCode[] { PULMONARY_EMBOLISM, COPD, ASTHMA });
		ATTRIBUTE_CHARACTERISTICS.put(DIGESTIVE, new CharacteristicCode[] { HEPATITIS_B, HEPATITIS_C, CIRRHOSIS });
		ATTRIBUTE_CHARACTERISTICS.put(CARDIOVASCULAR, new CharacteristicCode[] { ANGINA, ARRHYTHMIA,
				CONGESTIVE_HEART_FAILURE, DEEP_VEIN_THROMBOSIS, HEART_ATTACK, HYPERTENSION });
		ATTRIBUTE_CHARACTERISTICS.put(RENAL, new CharacteristicCode[] { RENAL_INSUFFICIENCY, RENAL_FAILURE });
		ATTRIBUTE_CHARACTERISTICS.put(NEUROPSYCHIATRIC, new CharacteristicCode[] { PERIPHERAL_NEUROPATHY, STROKE });
		ATTRIBUTE_CHARACTERISTICS.put(HORMONAL, new CharacteristicCode[] { HYPERTHYROIDISM, HYPOTHYROIDISM });
		ATTRIBUTE_CHARACTERISTICS.put(GYNECOLOGIC, new CharacteristicCode[] { ENDOMETRIAL_HYPERPLASIA, ENDOMETRIOSIS,
				ABNORMAL_VAGINAL_BLEEDING });
		ATTRIBUTE_CHARACTERISTICS.put(DIAGNOSISAREA, new CharacteristicCode[] { SUPERCLAVICULAR, INFRACLAVICULAR,
				CHESTWALL, DIAGNOSISAREA_BRAIN, DIAGNOSISAREA_SPINALCORD, DIAGNOSISAREA_BONE, DIAGNOSISAREA_LIVER,
				DIAGNOSISAREA_LUNG, DIAGNOSISAREA_LYMPHNODE, DIAGNOSISAREA_SKIN, DIAGNOSISAREA_OVARIES, DIAGNOSISAREA_BREAST, 
				DIAGNOSISAREA_OTHER_LOCATION });
		ATTRIBUTE_CHARACTERISTICS.put(RADIATION, new CharacteristicCode[] { PRIMARYCANCER_HODGKINS, THYROID_DISEASE,
				LUNG_DISEASE });
		ATTRIBUTE_CHARACTERISTICS.put(RACE, new CharacteristicCode[] { AMERICAN_INDIAN_ALASKAN_NATIVE, ASIAN, BLACK,
				HISPANIC, PACIFIC_ISLANDER, CAUCASIAN, UNSURE, RACE_OTHER });
		ATTRIBUTE_CHARACTERISTICS.put(HISPANIC, new CharacteristicCode[] { AFFIRMATIVE, NEGATIVE, UNSURE });
	}

	/**
	 * AvatarHistoryConfiguration will use this to configure the PatientHistory backing bean for areas of disease.
	 * @return the DIAGNOSISAREA entry for the private ATTRIBUTE_CHARACTERISTICS
	 */
	public static final CharacteristicCode[] getDiagnosisAreaAttributeCharacteristics()
	{
		return ATTRIBUTE_CHARACTERISTICS.get(DIAGNOSISAREA);
	}
	
	public static final List<CharacteristicCode> getDiagnosisAreaAttributeCharacteristicsAsList()
	{
		return Arrays.asList(ATTRIBUTE_CHARACTERISTICS.get(DIAGNOSISAREA));
	}
	
	/**
	 * Represents the string characteristics, guaranteed to be non-
	 * <code>null</code>
	 * 
	 * @see #PatientInterfaceAction()
	 * @see #STRING_CHARACTERISTICS
	 */
	private final FacadeStringCharacteristic[] stringCharacteristics;
	/**
	 * The characteristics that constitute the string characteristics. This
	 * array-index-to-characteristic-code relationship is used by callers to
	 * reference the appropriate characteristic.
	 */
	private static final CharacteristicCode[] STRING_CHARACTERISTICS = { DISEASE, CANCER_DIAGNOSIS, CANCER_TREATMENT, RACE };

	/**
	 * Represents the year/month characteristics, guaranteed to be non-
	 * <code>null</code>
	 * 
	 * @see #PatientInterfaceAction()
	 * @see #YEARMONTH_CHARACTERISTICS
	 */
	private final FacadeYearMonthCharacteristic[] yearMonthCharacteristics;
	/**
	 * The characteristics that constitute the year-month characteristics. This
	 * array-index-to-characteristic-code relationship is used by callers to
	 * reference the appropriate characteristic.
	 */
	private static final CharacteristicCode[] YEARMONTH_CHARACTERISTICS = { BIRTHDATE, DIAGNOSISBREAST, HEART_ATTACK };

	/**
	 * Represents the diagnoses, guaranteed to be non-<code>null</code>. The
	 * indices correspond to:
	 * <ul>
	 * <li>0: metastatic diagnosis</li>
	 * <li>1: left breast diagnosis</li>
	 * <li>2: right breast diagnosis</li>
	 * </ul>
	 * 
	 * @see #setPatient(UserPatient)
	 * @see #getDiagnoses()
	 */
	private final FacadeDiagnosis[] diagnoses;

	/**
	 * Represents whether any treatment within a subset was performed. This
	 * class is a facade for the performed attribute of either a group of
	 * procedures or a group of therapies.
	 */
	private class TreatmentPerformed extends FacadeBinary
	{
		/**
		 * Whether or not the subset is therapy or procedure
		 */
		private final boolean isTherapy;
		/**
		 * The type of therapy or kind of procedure
		 */
		private final CharacteristicCode typeOrKind;

		/**
		 * Stores parameters into instance variables
		 * 
		 * @param isTherapy
		 *            whether or not the subset is therapy or procedure
		 * @param typeOrKind
		 *            type of therapy or kind of procedure
		 */
		private TreatmentPerformed(boolean isTherapy, CharacteristicCode typeOrKind)
		{
			this.isTherapy = isTherapy;
			this.typeOrKind = typeOrKind;
		}

		/**
		 * Returns the underlying value from the holder by querying each
		 * procedure or therapy in the subset that this facade represents.
		 * 
		 * @return the underlying value from the holder
		 * @see org.quantumleaphealth.action.FacadeBinary#getValue()
		 */
		@Override
		protected boolean getValue()
		{
			if (isTherapy)
			{
				for (FacadeTherapy therapy : therapies)
					if (typeOrKind.equals(therapy.getType()) && therapy.isPerformed())
						return true;
			}
			else
			{
				for (FacadeProcedure procedure : procedures)
					if (typeOrKind.equals(procedure.getKind()) && procedure.isPerformed())
						return true;
			}
			return false;
		}

		/**
		 * Resets the performed attribute of the subset of treatments. This
		 * method does nothing if <code>value</code> is <code>true</code>.
		 * 
		 * @param value
		 *            whether or not any treatment in the subset is performed
		 * @see org.quantumleaphealth.action.FacadeBinary#setValue(boolean)
		 */
		@Override
		protected void setValue(boolean value)
		{
			if (value)
				return;
			if (isTherapy)
			{
				for (FacadeTherapy therapy : therapies)
					if (typeOrKind.equals(therapy.getType()) && therapy.isPerformed())
						therapy.setPerformed(false);
			}
			else
			{
				for (FacadeProcedure procedure : procedures)
					if (typeOrKind.equals(procedure.getKind()) && procedure.isPerformed())
						procedure.setPerformed(false);
			}
		}

		/**
		 * UID for serialization
		 */
		private static final long serialVersionUID = 5421030229619368739L;
	}

	/**
	 * Whether a treatment type was performed, guaranteed to be non-
	 * <code>null</code>. Supported treatment types:
	 * <ul>
	 * <li>0: Surgery
	 * <li>1: Radiation
	 * <li>2: Chemotherapy
	 * <li>3: Targetted/Biological therapy
	 * <li>4: Hormone/Endocrine therapy
	 * <li>5: Bisphosphonate therapy
	 * </ul>
	 */
	private final TreatmentPerformed[] treatmentPerformed;

	/**
	 * Represents the procedures, guaranteed to be non-<code>null</code>.
	 * 
	 * @see #PatientInterfaceAction()
	 * @see #setPatient(UserPatient)
	 * @see #getProcedures()
	 */
	private final FacadeProcedure[] procedures;
	/**
	 * The surgical procedures by location and type. This
	 * array-index-to-characteristic-code relationship is used by callers to
	 * reference the appropriate surgical history characteristic.
	 */
	private static final CharacteristicCode[][] SURGERIES = { { LEFT_BREAST, SURGERY_BREAST_CONSERVING },
			{ LEFT_BREAST, BREAST_REEXCISION }, { LEFT_BREAST, EXCISIONAL_BIOPSY },
			{ LEFT_BREAST, MASTECTOMY_THERAPEUTIC }, { LEFT_BREAST, MASTECTOMY_PROPHYLACTIC },
			{ LEFT_BREAST, SENTINEL_NODE_BIOPSY }, { RIGHT_BREAST, SURGERY_BREAST_CONSERVING },
			{ RIGHT_BREAST, BREAST_REEXCISION }, { RIGHT_BREAST, EXCISIONAL_BIOPSY },
			{ RIGHT_BREAST, MASTECTOMY_THERAPEUTIC }, { RIGHT_BREAST, MASTECTOMY_PROPHYLACTIC },
			{ RIGHT_BREAST, SENTINEL_NODE_BIOPSY }, { LEFT_AXILLARY_NODE, AXILLARY_NODE_DISSECTION },
			{ RIGHT_AXILLARY_NODE, AXILLARY_NODE_DISSECTION }, { LEFT_OVARY, OOPHORECTOMY },
			{ RIGHT_OVARY, OOPHORECTOMY }, { UTERUS, HYSTERECTOMY }, { BRAIN, SURGERY_BRAIN },
			{ SPINAL_CORD, SURGERY_SPINAL_CORD }, { BONE, SURGERY_BONE }, { LIVER, SURGERY_LIVER },
			{ LUNG, SURGERY_LUNG }, { LYMPH_NODE, SURGERY_LYMPH_NODE } };

	/**
	 * The radiation locations. This array-index-to-characteristic-code
	 * relationship is used by callers to reference the appropriate radiation
	 * history characteristic.
	 */
	private static final CharacteristicCode RADIATION_LOCATIONS[] = { LEFT_BREAST, RIGHT_BREAST, BRAIN, SPINAL_CORD,
			BONE, CHESTWALL, LYMPH_NODE, THORAX, LIVER, LUNG, OVARIAN_RADIOTHERAPY };

	/**
	 * Represents the therapies, guaranteed to be non-<code>null</code>.
	 * 
	 * @see #setPatient(UserPatient)
	 * @see #getTherapies()
	 */
	private final FacadeTherapy[] therapies;

	/**
	 * The agents that make up the chemotherapy regimens. This
	 * array-index-to-characteristic-code relationship is used by callers to
	 * reference the appropriate chemotherapy regime history characteristic.
	 */
	private static final CharacteristicCode[][] CHEMOTHERAPY_REGIMEN = { { DOXORUBICIN, CYCLOPHOSPHAMIDE }, // AC
			{ DOXORUBICIN, CYCLOPHOSPHAMIDE, PACLITAXEL }, // AC+Taxol
			{ DOXORUBICIN, CYCLOPHOSPHAMIDE, DOCETAXEL }, // AC+Taxotere
			{ CYCLOPHOSPHAMIDE, METHOTREXATE, FIVE_FLUOROURACIL }, // CMF
			{ EPIRUBICIN, CYCLOPHOSPHAMIDE }, // EC
			{ FIVE_FLUOROURACIL, DOXORUBICIN, CYCLOPHOSPHAMIDE }, // FAC
			{ FIVE_FLUOROURACIL, EPIRUBICIN, CYCLOPHOSPHAMIDE }, // FEC
			{ IXABEPILONE, CAPECITABINE }, // Ixempra+Xeloda
			{ DOCETAXEL, CYCLOPHOSPHAMIDE }, // TC
			{ DOCETAXEL, DOXORUBICIN, CYCLOPHOSPHAMIDE }, // TAC
			{ PACLITAXEL, CAPECITABINE }, // Taxol+Xeloda
			{ DOCETAXEL, CAPECITABINE }, // Taxotere+Xeloda
			{ PACLITAXEL, GEMCITABINE }, // Taxol+Gemzar
			{ DOCETAXEL, CARBOPLATIN }, // Taxotere+Carboplatin
			{ PACLITAXEL, CARBOPLATIN }, // Taxol+Carboplatin
			{ ABRAXANE, CAPECITABINE }, // Abraxane+Xeloda
			{ ABRAXANE, CARBOPLATIN }, // Abraxane+Carboplatin
			{ CHEMOTHERAPY_OTHER } // BCT-405:  fix the Other checkbox for chemotherapy
	};

	/**
	 * The agents that make up the chemotherapy agents. This
	 * array-index-to-characteristic-code relationship is used by callers to
	 * reference the appropriate chemotherapy agent history characteristic.
	 */
	private static final CharacteristicCode[][] CHEMOTHERAPY_AGENTS = { { ABRAXANE }, { DOXORUBICIN },
			{ FIVE_FLUOROURACIL }, { CYCLOPHOSPHAMIDE }, { LIPOSOMAL_DOXORUBICIN }, { EPIRUBICIN }, { GEMCITABINE },
			{ IRINOTECAN }, { IXABEPILONE }, { METHOTREXATE }, { VINORELBINE }, { MITOXANTRONE }, { CARBOPLATIN },
			{ CISPLATIN }, { PACLITAXEL }, { DOCETAXEL }, { TEMOZOLOMIDE }, { THIOTEPA }, { TOPOTECAN },
			{ VINCRISTINE }, { VINBLASTINE }, { CAPECITABINE },
			{ HALAVEN },           // BCT-526: Eribulin Mesylate */
			{ CHEMOAGENT_OTHER } };

	/**
	 * The agents that make up the targeted/biological agents. This
	 * array-index-to-characteristic-code relationship is used by callers to
	 * reference the appropriate biological history characteristic.
	 */
	private static final CharacteristicCode[][] BIOLOGICAL_AGENTS = { { TRASTUZUMAB }, { LAPATINIB }, { PERJETA }, { AFINITOR }, {KADCYLA}, { BEVACIZUMAB }, { BIOLOGICAL_OTHER } };

	/**
	 * The hormonal/endocrine agents. This array-index-to-characteristic-code
	 * relationship is used by callers to reference the appropriate
	 * hormonal/endocrine history characteristic.
	 */
	private static final CharacteristicCode[][] ENDOCRINE_AGENTS = { { RALOXIFENE }, { TOREMIFENE }, { FULVESTRANT },
			{ TAMOXIFEN }, { ANASTROZOLE }, { EXEMESTANE }, { LETROZOLE }, { LEUPROLIDE }, { ABARELIX }, { BUSERELIN },
			{ GOSERELIN }, { MEGESTROL_ACETATE }, { ENDOCRINE_OTHER } };

	/**
	 * The number of bisphosphonate agents. This
	 * array-index-to-characteristic-code relationship is used by callers to
	 * reference the appropriate bisphosphonate history characteristic.
	 */
	private static final CharacteristicCode[][] BISPHOSPHONATE_AGENTS = { { RISEDRONATE }, { PAMIDRONATE },
			{ IBANDRONATE }, { ALENDRONATE }, { DENOSUMAB_XGEVA }, { ZOLEDRONATE }, { BIOSPHOSPHONATE_OTHER } };

	/**
	 * Creates the instance variables using the backing history.
	 * 
	 * @param history
	 *            the patient's history
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public FacadePatientHistory(PatientHistory history) throws IllegalArgumentException
	{
		// Validate parameter and store the value history so we can update the
		// facade-less category characteristic
		if (history == null)
			throw new IllegalArgumentException("history not specified");
		valueHistory = history.getValueHistory();

		// Generate characteristic facades
		binaryCharacteristics = new FacadeBinaryCharacteristic[BINARY_CHARACTERISTICS.length];
		BinaryHistory binaryHistory = history.getBinaryHistory();
		for (int index = 0; index < binaryCharacteristics.length; index++)
			binaryCharacteristics[index] = new FacadeBinaryCharacteristic(BINARY_CHARACTERISTICS[index], binaryHistory);
		valueCharacteristics = new FacadeValueCharacteristic[VALUE_CHARACTERISTICS.size()];
		Iterator<Map.Entry<CharacteristicCode, CharacteristicCode[]>> entryIterator = VALUE_CHARACTERISTICS.entrySet()
				.iterator();
		int entryIndex = 0;
		while (entryIterator.hasNext())
		{
			Map.Entry<CharacteristicCode, CharacteristicCode[]> entry = entryIterator.next();
			valueCharacteristics[entryIndex] = new FacadeValueCharacteristic(entry.getKey(), entry.getValue(),
					valueHistory);
			entryIndex++;
		}
		attributeCharacteristics = new FacadeAttributeCharacteristic[ATTRIBUTE_CHARACTERISTICS.size()];
		entryIterator = ATTRIBUTE_CHARACTERISTICS.entrySet().iterator();
		entryIndex = 0;
		while (entryIterator.hasNext())
		{
			Map.Entry<CharacteristicCode, CharacteristicCode[]> entry = entryIterator.next();
			attributeCharacteristics[entryIndex] = new FacadeAttributeCharacteristic(entry.getKey(), entry.getValue(),
					history);
			entryIndex++;
		}
		stringCharacteristics = new FacadeStringCharacteristic[STRING_CHARACTERISTICS.length];
		for (int index = 0; index < stringCharacteristics.length; index++)
			stringCharacteristics[index] = new FacadeStringCharacteristic(STRING_CHARACTERISTICS[index], history);
		yearMonthCharacteristics = new FacadeYearMonthCharacteristic[YEARMONTH_CHARACTERISTICS.length];
		YearMonthHistory yearMonthHistory = history.getYearMonthHistory();
		for (int index = 0; index < yearMonthCharacteristics.length; index++)
			yearMonthCharacteristics[index] = new FacadeYearMonthCharacteristic(YEARMONTH_CHARACTERISTICS[index],
					yearMonthHistory);

		// Diagnoses are metastatic, left and right breast
		diagnoses = new FacadeDiagnosis[3];
		Collection<Diagnosis> diagnosisHistory = history.getDiagnoses();
		diagnoses[0] = new FacadeDiagnosis(diagnosisHistory, METASTATIC_BREAST_CANCER);
		diagnoses[1] = new FacadeDiagnosis(diagnosisHistory, LEFT_BREAST);
		diagnoses[2] = new FacadeDiagnosis(diagnosisHistory, RIGHT_BREAST);

		// Procedures include surgery and radiation; radiation does not specify
		// type
		Collection<Procedure> procedureHistory = history.getProcedures();
		procedures = new FacadeProcedure[SURGERIES.length + RADIATION_LOCATIONS.length];
		for (int index = 0; index < SURGERIES.length; index++)
			procedures[index] = new FacadeProcedure(procedureHistory, SURGERY, SURGERIES[index][0],
					SURGERIES[index][1], "surgery" + index, true);
		for (int index = 0; index < RADIATION_LOCATIONS.length; index++)
			procedures[SURGERIES.length + index] = new FacadeProcedure(procedureHistory, RADIATION,
					RADIATION_LOCATIONS[index], null, "radiation" + index, false);

		// Set up the therapies array
		// Therapies includes: chemo regimens, chemo agents, biological agents,
		// endocrine agents, bisphosphonate agents
		therapies = new FacadeTherapy[CHEMOTHERAPY_REGIMEN.length + CHEMOTHERAPY_AGENTS.length
				+ BIOLOGICAL_AGENTS.length + ENDOCRINE_AGENTS.length + BISPHOSPHONATE_AGENTS.length];
		Collection<Therapy> therapyHistory = history.getTherapies();
		int startIndex = 0;
		for (int index = 0; index < CHEMOTHERAPY_REGIMEN.length; index++)
		{				
			therapies[startIndex + index] = new FacadeTherapy(therapyHistory, CHEMOTHERAPY_REGIMEN[index], CHEMOTHERAPY);
		}
		
		startIndex += CHEMOTHERAPY_REGIMEN.length;
		for (int index = 0; index < CHEMOTHERAPY_AGENTS.length; index++)
		{	
			therapies[startIndex + index] = new FacadeTherapy(therapyHistory, CHEMOTHERAPY_AGENTS[index], CHEMOTHERAPY);
		}	
			
		startIndex += CHEMOTHERAPY_AGENTS.length;
		for (int index = 0; index < BIOLOGICAL_AGENTS.length; index++)
		{	
			therapies[startIndex + index] = new FacadeTherapy(therapyHistory, BIOLOGICAL_AGENTS[index], BIOLOGICAL);
		}
		
		startIndex += BIOLOGICAL_AGENTS.length;
		for (int index = 0; index < ENDOCRINE_AGENTS.length; index++)
		{	
			therapies[startIndex + index] = new FacadeTherapy(therapyHistory, ENDOCRINE_AGENTS[index], ENDOCRINE);
		}	
			
		startIndex += ENDOCRINE_AGENTS.length;
		for (int index = 0; index < BISPHOSPHONATE_AGENTS.length; index++)
		{	
			therapies[startIndex + index] = new FacadeTherapy(therapyHistory, BISPHOSPHONATE_AGENTS[index],
					BISPHOSPHONATE);
		}	
		// Treatment questions reflect which procedures/therapies have been
		// selected
		treatmentPerformed = new TreatmentPerformed[6];
		treatmentPerformed[0] = new TreatmentPerformed(false, SURGERY);
		treatmentPerformed[1] = new TreatmentPerformed(false, RADIATION);
		treatmentPerformed[2] = new TreatmentPerformed(true, CHEMOTHERAPY);
		treatmentPerformed[3] = new TreatmentPerformed(true, BIOLOGICAL);
		treatmentPerformed[4] = new TreatmentPerformed(true, ENDOCRINE);
		treatmentPerformed[5] = new TreatmentPerformed(true, BISPHOSPHONATE);
	}

	private void resetHistoryArray(Object[] obj, int size)
	{
		if (obj != null)
		{
			for (int ndx = 0; ndx < size; ndx++)
			{
				obj[ndx] = null;
			}
		}
	}
	/**
	 * Much of FacadePatientHistory and the PatientInterfaceAction instance of it is declared final.
	 * But we need some way to reset *all* the values for a client being created from the Navigator CreateClient
	 * interface after at least one client has been created, or we will show the values from the previous client
	 * in the patient history forms.  This is basically the FacadePatientHistory constructor minus the instance
	 * assignments labeled final.
	 * 
	 * @param history
	 * @throws IllegalArgumentException
	 */
	public void resetFacadePatientHistory(PatientHistory history) throws IllegalArgumentException
	{
		// Validate parameter and store the value history so we can update the
		// facade-less category characteristic
		if (history == null)
			throw new IllegalArgumentException("history not specified");
		
		valueHistory = history.getValueHistory();

		// Generate characteristic facades
		// reset the binary characteristics
		//	binaryCharacteristics = new FacadeBinaryCharacteristic[BINARY_CHARACTERISTICS.length];
		
		if (binaryCharacteristics != null)
		{	
			for (int ndx = 0; ndx < BINARY_CHARACTERISTICS.length; ndx++)
			{
				binaryCharacteristics[ndx] = null;
			}
		}
		
		BinaryHistory binaryHistory = history.getBinaryHistory();
		for (int index = 0; index < binaryCharacteristics.length; index++)
			binaryCharacteristics[index] = new FacadeBinaryCharacteristic(BINARY_CHARACTERISTICS[index], binaryHistory);
		
		// reset the valueCharacteristics
		// valueCharacteristics = new FacadeValueCharacteristic[VALUE_CHARACTERISTICS.size()];
		if (valueCharacteristics != null)
		{
			for (int ndx = 0; ndx < VALUE_CHARACTERISTICS.size(); ndx++)
			{
				valueCharacteristics[ndx] = null;
			}
		}
		Iterator<Map.Entry<CharacteristicCode, CharacteristicCode[]>> entryIterator = VALUE_CHARACTERISTICS.entrySet()
				.iterator();
		int entryIndex = 0;
		while (entryIterator.hasNext())
		{
			Map.Entry<CharacteristicCode, CharacteristicCode[]> entry = entryIterator.next();
			valueCharacteristics[entryIndex] = new FacadeValueCharacteristic(entry.getKey(), entry.getValue(),
					valueHistory);
			entryIndex++;
		}
		// reset the attributeCharacteristics
		// attributeCharacteristics = new FacadeAttributeCharacteristic[ATTRIBUTE_CHARACTERISTICS.size()];
		if (attributeCharacteristics != null)
		{
			for (int ndx = 0; ndx < ATTRIBUTE_CHARACTERISTICS.size(); ndx++)
			{
				attributeCharacteristics[ndx] = null;
			}
		}
		
		entryIterator = ATTRIBUTE_CHARACTERISTICS.entrySet().iterator();
		entryIndex = 0;
		while (entryIterator.hasNext())
		{
			Map.Entry<CharacteristicCode, CharacteristicCode[]> entry = entryIterator.next();
			attributeCharacteristics[entryIndex] = new FacadeAttributeCharacteristic(entry.getKey(), entry.getValue(),
					history);
			entryIndex++;
		}

		// reset the stringCharacteristics
//		stringCharacteristics = new FacadeStringCharacteristic[STRING_CHARACTERISTICS.length];
		if (stringCharacteristics != null)
		{
			for (int ndx = 0; ndx < STRING_CHARACTERISTICS.length; ndx++)
			{
				stringCharacteristics[ndx] = null;
			}
		}
		
		for (int index = 0; index < stringCharacteristics.length; index++)
			stringCharacteristics[index] = new FacadeStringCharacteristic(STRING_CHARACTERISTICS[index], history);
		
		// reset the yearMonthCharacteristics
//		yearMonthCharacteristics = new FacadeYearMonthCharacteristic[YEARMONTH_CHARACTERISTICS.length];
		if (yearMonthCharacteristics != null)
		{
			for (int ndx = 0; ndx < YEARMONTH_CHARACTERISTICS.length; ndx++)
			{
				yearMonthCharacteristics[ndx] = null;
			}
		}
		YearMonthHistory yearMonthHistory = history.getYearMonthHistory();
		for (int index = 0; index < yearMonthCharacteristics.length; index++)
			yearMonthCharacteristics[index] = new FacadeYearMonthCharacteristic(YEARMONTH_CHARACTERISTICS[index],
					yearMonthHistory);

		// Diagnoses are metastatic, left and right breast
		// reset diagnoses
//		diagnoses = new FacadeDiagnosis[3];
		resetHistoryArray(diagnoses, 3);
		Collection<Diagnosis> diagnosisHistory = history.getDiagnoses();
		diagnoses[0] = new FacadeDiagnosis(diagnosisHistory, METASTATIC_BREAST_CANCER);
		diagnoses[1] = new FacadeDiagnosis(diagnosisHistory, LEFT_BREAST);
		diagnoses[2] = new FacadeDiagnosis(diagnosisHistory, RIGHT_BREAST);

		// Procedures include surgery and radiation; radiation does not specify
		// type
		Collection<Procedure> procedureHistory = history.getProcedures();
		
		// reset procedures
		// procedures = new FacadeProcedure[SURGERIES.length + RADIATION_LOCATIONS.length];
		resetHistoryArray(procedures, SURGERIES.length + RADIATION_LOCATIONS.length);
		for (int index = 0; index < SURGERIES.length; index++)
			procedures[index] = new FacadeProcedure(procedureHistory, SURGERY, SURGERIES[index][0],
					SURGERIES[index][1], "surgery" + index, true);
		for (int index = 0; index < RADIATION_LOCATIONS.length; index++)
			procedures[SURGERIES.length + index] = new FacadeProcedure(procedureHistory, RADIATION,
					RADIATION_LOCATIONS[index], null, "radiation" + index, false);

		// Set up the therapies array
		// Therapies includes: chemo regimens, chemo agents, biological agents,
		// endocrine agents, bisphosphonate agents
		// reset therapies
//		therapies = new FacadeTherapy[CHEMOTHERAPY_REGIMEN.length + CHEMOTHERAPY_AGENTS.length
//				+ BIOLOGICAL_AGENTS.length + ENDOCRINE_AGENTS.length + BISPHOSPHONATE_AGENTS.length];
		
		resetHistoryArray(therapies, CHEMOTHERAPY_REGIMEN.length + CHEMOTHERAPY_AGENTS.length + BIOLOGICAL_AGENTS.length + ENDOCRINE_AGENTS.length + BISPHOSPHONATE_AGENTS.length);
		Collection<Therapy> therapyHistory = history.getTherapies();
		int startIndex = 0;
		for (int index = 0; index < CHEMOTHERAPY_REGIMEN.length; index++)
		{				
			therapies[startIndex + index] = new FacadeTherapy(therapyHistory, CHEMOTHERAPY_REGIMEN[index], CHEMOTHERAPY);
		}
		
		startIndex += CHEMOTHERAPY_REGIMEN.length;
		for (int index = 0; index < CHEMOTHERAPY_AGENTS.length; index++)
		{	
			therapies[startIndex + index] = new FacadeTherapy(therapyHistory, CHEMOTHERAPY_AGENTS[index], CHEMOTHERAPY);
		}	
			
		startIndex += CHEMOTHERAPY_AGENTS.length;
		for (int index = 0; index < BIOLOGICAL_AGENTS.length; index++)
		{	
			therapies[startIndex + index] = new FacadeTherapy(therapyHistory, BIOLOGICAL_AGENTS[index], BIOLOGICAL);
		}
		
		startIndex += BIOLOGICAL_AGENTS.length;
		for (int index = 0; index < ENDOCRINE_AGENTS.length; index++)
		{	
			therapies[startIndex + index] = new FacadeTherapy(therapyHistory, ENDOCRINE_AGENTS[index], ENDOCRINE);
		}	
			
		startIndex += ENDOCRINE_AGENTS.length;
		for (int index = 0; index < BISPHOSPHONATE_AGENTS.length; index++)
		{	
			therapies[startIndex + index] = new FacadeTherapy(therapyHistory, BISPHOSPHONATE_AGENTS[index],
					BISPHOSPHONATE);
		}	
		// Treatment questions reflect which procedures/therapies have been
		// selected
		// reset treatment array
//		treatmentPerformed = new TreatmentPerformed[6];
		resetHistoryArray(treatmentPerformed, 6);
		treatmentPerformed[0] = new TreatmentPerformed(false, SURGERY);
		treatmentPerformed[1] = new TreatmentPerformed(false, RADIATION);
		treatmentPerformed[2] = new TreatmentPerformed(true, CHEMOTHERAPY);
		treatmentPerformed[3] = new TreatmentPerformed(true, BIOLOGICAL);
		treatmentPerformed[4] = new TreatmentPerformed(true, ENDOCRINE);
		treatmentPerformed[5] = new TreatmentPerformed(true, BISPHOSPHONATE);
	}
	
	/**
	 * Updates the sub-facade instance variables with a new patient history
	 * 
	 * @param history
	 *            the patient's history
	 * @param fresh
	 *            if the patient's history has not populated any characteristics
	 *            yet
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	public void setPatientHistory(PatientHistory history, boolean fresh) throws IllegalArgumentException
	{
		// Validate parameter and store the value history so we can update the
		// facade-less category characteristic
		if (history == null)
			throw new IllegalArgumentException("history not specified");
		valueHistory = history.getValueHistory();

		// Update the characteristic histories
		BinaryHistory binaryHistory = history.getBinaryHistory();
		for (FacadeBinaryCharacteristic binaryCharacteristic : binaryCharacteristics)
			binaryCharacteristic.setBinaryCharacteristicHolder(binaryHistory, fresh);
		for (FacadeValueCharacteristic valueCharacteristic : valueCharacteristics)
			valueCharacteristic.setValueCharacteristicHolder(valueHistory);
		for (FacadeAttributeCharacteristic attributeCharacteristic : attributeCharacteristics)
			attributeCharacteristic.setAttributeCharacteristicHolder(history);
		for (FacadeStringCharacteristic stringCharacteristic : stringCharacteristics)
			stringCharacteristic.setStringCharacteristicHolder(history);
		YearMonthHistory yearMonthHistory = history.getYearMonthHistory();
		for (FacadeYearMonthCharacteristic yearMonthCharacteristic : yearMonthCharacteristics)
			yearMonthCharacteristic.setbyteShortCharacteristicHolder(yearMonthHistory);

		// Update the category and primary properties
		CharacteristicCode categoryCode = fresh ? null : valueHistory.getValue(BREAST_CANCER);
		category = 0l;
		primary = 0l;
		if (PREVENTION.equals(categoryCode))
			category = 2l;
		else if (SINGULAR.equals(categoryCode))
		{
			category = 4l;
			primary = 2l;
		}
		else if (RECURRENT_LOCALLY.equals(categoryCode))
		{
			category = 4l;
			primary = 4l;
		}
		else if (BILATERAL_ASYNCHRONOUS.equals(categoryCode))
		{
			category = 4l;
			primary = 8l;
		}
		else if (METASTATIC.equals(categoryCode))
			category = 8l;
		else if (SURVIVOR.equals(categoryCode))
			category = 16l;

		// Update the diagnosis and treatment facades
		for (FacadeDiagnosis facadeDiagnosis : diagnoses)
			facadeDiagnosis.setHistory(history.getDiagnoses());
		for (FacadeProcedure facadeProcedure : procedures)
			facadeProcedure.setProcedures(history.getProcedures());
		for (FacadeTherapy facadeTherapy : therapies)
			facadeTherapy.setTherapies(history.getTherapies());
		// Update the treatment performed facade only after the procedures and
		// therapies are set
		for (TreatmentPerformed treatment : treatmentPerformed)
			treatment.update(fresh);
	}

	/**
	 * @return the patient's category in bit-wise format or <code>0</code> if
	 *         not answered yet
	 * @see org.quantumleaphealth.action.PatientInterface#getCategory()
	 * @see #category
	 */
	public long getCategory()
	{
		return category;
	}

	/**
	 * Stores the patient's category and updates the diagnoses. The following
	 * values are stored in the patient object depending upon the bit-wise
	 * formatted parameters:
	 * <ul>
	 * <li>bit 1: <code>PREVENTION</code></li>
	 * <li>bit 2: ingored; the <code>primary</code> property will set the value</li>
	 * <li>bit 3: <code>METASTATIC</code></li>
	 * <li>bit 4: <code>SURVIVOR</code></li>
	 * <li>all others: <code>null</code></li>
	 * </ul>
	 * If the metastatic status changes then the diagnoses are updated.
	 * 
	 * @param category
	 *            the patient's category in bit-wise format or <code>0</code> if
	 *            not answered yet
	 * @see org.quantumleaphealth.action.PatientInterface#setCategory(long)
	 * @see #setPrimary(long)
	 */
	public void setCategory(long category)
	{
		// If not changed then return
		if (this.category == category)
			return;
		// Cache and update the value characteristic
		this.category = category;
		if (category == 2l)
			updateCategory(PREVENTION);
		else if (category == 8l)
			updateCategory(METASTATIC);
		else if (category == 16l)
			updateCategory(SURVIVOR);
		else if (category != 4l)
			updateCategory(null);
		// If metastatic updated then activate the diagnosis
		diagnoses[0].setDiagnosed(category == 8l);
	}

	/**
	 * @return the patient's primary type in bit-wise format or <code>0</code>
	 *         if not answered yet
	 * @see org.quantumleaphealth.action.PatientInterface#getPrimary()
	 * @see #primary
	 */
	public long getPrimary()
	{
		return primary;
	}

	/**
	 * Stores the patient's primary type. The following values are stored in the
	 * patient object depending upon the bit-wise formatted parameters:
	 * <ul>
	 * <li>bit 1: <code>SINGULAR</code></li>
	 * <li>bit 2: <code>RECURRENT_LOCALLY</code></li>
	 * <li>bit 3: <code>BILATERAL_ASYNCHRONOUS</code></li>
	 * <li>all others: ingored; the <code>category</code> property will set the
	 * value</li>
	 * </ul>
	 * 
	 * @param primary
	 *            the patient's primary type in bit-wise format or
	 *            <code>0</code> if not answered yet
	 * @see org.quantumleaphealth.action.PatientInterface#setPrimary(long)
	 * @see #setCategory(long)
	 */
	public void setPrimary(long primary)
	{
		// If not changed then return
		if (this.primary == primary)
			return;
		// Cache and update the value characteristic
		this.primary = primary;
		if (primary == 2l)
			updateCategory(SINGULAR);
		else if (primary == 4l)
			updateCategory(RECURRENT_LOCALLY);
		else if (primary == 8l)
			updateCategory(BILATERAL_ASYNCHRONOUS);
	}

	/**
	 * Store a new category code in the patient's value history.
	 * 
	 * @param categoryCode
	 *            the category code to store in the patient's history or
	 *            <code>null</code> to remove the category
	 */
	private void updateCategory(CharacteristicCode categoryCode)
	{
		if (categoryCode == null)
			valueHistory.remove(BREAST_CANCER);
		else
			valueHistory.put(BREAST_CANCER, categoryCode);
	}

	/**
	 * @return the binary characteristics
	 * @see org.quantumleaphealth.action.PatientInterface#getBinaryCharacteristics()
	 */
	public FacadeBinaryCharacteristic[] getBinaryCharacteristics()
	{
		return binaryCharacteristics;
	}

	/**
	 * @return the value characteristics
	 * @see org.quantumleaphealth.action.PatientInterface#getValueCharacteristics()
	 */
	public FacadeValueCharacteristic[] getValueCharacteristics()
	{
		return valueCharacteristics;
	}

	/**
	 * @return the attribute characteristics
	 * @see org.quantumleaphealth.action.PatientInterface#getAttributeCharacteristics()
	 */
	public FacadeAttributeCharacteristic[] getAttributeCharacteristics()
	{
		return attributeCharacteristics;
	}

	/**
	 * @return the string characteristics
	 * @see org.quantumleaphealth.action.PatientInterface#getStringCharacteristics()
	 */
	public FacadeStringCharacteristic[] getStringCharacteristics()
	{
		return stringCharacteristics;
	}

	/**
	 * @return the year-month characteristics
	 * @see org.quantumleaphealth.action.PatientInterface#getYearMonthCharacteristics()
	 */
	public FacadeYearMonthCharacteristic[] getYearMonthCharacteristics()
	{
		return yearMonthCharacteristics;
	}

	/**
	 * @return the diagnosis facades, guaranteed to be non-<code>null</code>
	 */
	public FacadeDiagnosis[] getDiagnoses()
	{
		return diagnoses;
	}

	/**
	 * @return the patient's answers to treatment categories, guaranteed to be
	 *         non-<code>null</code>.
	 * @see org.quantumleaphealth.action.PatientInterface#getTreatmentPerformed()
	 */
	public FacadeBinary[] getTreatmentPerformed()
	{
		return treatmentPerformed;
	}

	/**
	 * @return the procedure facades, guaranteed to be non-<code>null</code>
	 */
	public FacadeProcedure[] getProcedures()
	{
		return procedures;
	}

	/**
	 * @return the therapy facades, guaranteed to be non-<code>null</code>
	 */
	public FacadeTherapy[] getTherapies()
	{
		return therapies;
	}

	/**
	 * Returns whether the patient answered the category question(s). If the
	 * second <code>category</code> choice is made then a <code>primary</code>
	 * choice must be made. Algorithm:
	 * <ol>
	 * <li><code>category</code> must not be zero, and</li>
	 * <li><code>category</code> must not be 4 or <code>primary</code> must not
	 * be zero</li>
	 * </ol>
	 * 
	 * @return <code>true</code> if the category question(s) were answered
	 */
	boolean isCategoryValid()
	{
		return (category > 0l) && ((category != 4l) || (primary > 0));
	}

	/**
	 * Returns whether all required questions are answered. If any of the
	 * following checks fail then this method returns <code>false</code>:
	 * <ol>
	 * <li>About Me
	 * <ol>
	 * <li>Year of birth (month only)</li>
	 * <li>Gender; if Female, also check menopausal status:
	 * <ul>
	 * <li>Premenopausal
	 * <ul>
	 * <li>Pregnancy</li>
	 * <li>Nursing</li>
	 * </ul>
	 * </li>
	 * <li>Postmenopausal
	 * <ul>
	 * <li>Reason</li>
	 * <li>Hormone replacement therapy</li>
	 * </ul>
	 * </li>
	 * </ul>
	 * </li>
	 * <li>Genetic testing; if Yes then also check
	 * <ul>
	 * <li>BRCA1</li>
	 * <li>BRCA2</li>
	 * </ul>
	 * </li>
	 * <li>Current clinical trial participation</li>
	 * </ol>
	 * </li>
	 * <li>My Health
	 * <ol>
	 * <li>Performance status</li>
	 * <li>Cardiovascular Heart Attack: Year</li>
	 * </ol>
	 * </li>
	 * <li>Diagnosis (not for Prevention)</li>
	 * <li>Treatment (not for Prevention)
	 * <ol>
	 * </ol></li>
	 * </ol>
	 * <em>Note: this method depends upon the indices of the 
	 * facades defined in this class.</em>
	 * 
	 * @return <code>true</code> if the patient history is complete
	 */
	public boolean isComplete()
	{
		// My health concern: Category
		if (!isCategoryValid())
			return false;

		// About Me: Year of birth
		if (yearMonthCharacteristics[0].isVacant())
			return false;
		// About Me: Gender
		if (valueCharacteristics[0].getValue() == 0)
			return false;
		// Female gender: Menopausal status
		if ((valueCharacteristics[0].getValue() == 2) && (valueCharacteristics[1].getValue() == 0))
			return false;
		// Premenopausal: Pregnancy & Nursing
		if ((valueCharacteristics[1].getValue() == 2)
				&& (binaryCharacteristics[0].isFresh() || binaryCharacteristics[1].isFresh()))
			return false;
		// Postmenopausal: Postmenopausal reason & Hormone replacement therapy
		if ((valueCharacteristics[1].getValue() == 8)
				&& ((valueCharacteristics[2].getValue() == 0) || (valueCharacteristics[3].getValue() == 0)))
			return false;
		// About Me: Genetic testing
		if (binaryCharacteristics[2].isFresh())
			return false;
		// Genetic testing Yes: BRCA1 & BRCA2
		if (binaryCharacteristics[2].isBooleanValue()
				&& ((valueCharacteristics[4].getValue() == 0) || (valueCharacteristics[5].getValue() == 0)))
			return false;
		// About Me: Clinical trial participation
		if (binaryCharacteristics[3].isFresh())
			return false;

		// My Health: well-being
		if (valueCharacteristics[6].getValue() == 0)
			return false;
		// Cardiovascular Heart Attack: Year
		if (((attributeCharacteristics[5].getValue() & 32) != 0) && yearMonthCharacteristics[2].isVacant())
			return false;

//7/10/2013:  comment out until we fix the database for patients who have not answered this question.		
//		// Navigator Race Matching:  race is now required
//		if (attributeCharacteristics[12].getValue() == 0)
//			return false;
//
//		// Navigator Race Matching:  hispanic question answer is now required.
//		if (attributeCharacteristics[13].getValue() == 0)
//			return false;
		
		// if Skip is chosen on race/ethnicity then there can be no other choices checked.
		if (!isValidRaceEthnicity())
		{
			return false;
		}

		// Do not check diagnosis or treatment If prevention patient
		// TODO: Implement prevention-only checks when implemented
		if (category != 2)
		{

			// My Diagnosis: Year
			if (yearMonthCharacteristics[1].isVacant())
				return false;
			// Metastatic diagnosis
			if (category == 8l)
			{
				// ER, PR, HER2/neu
				if ((diagnoses[0].getValueCharacteristics()[0].getValue() == 0)
						|| (diagnoses[0].getValueCharacteristics()[1].getValue() == 0)
						|| (diagnoses[0].getValueCharacteristics()[2].getValue() == 0))
					return false;
				// Inflammatory
				if (diagnoses[0].getValueCharacteristics()[4].getValue() == 0)
					return false;
				// Spread to brain: Brain mets
				if ((diagnoses[0].getAttributeCharacteristics()[1].getValue() == 16)
						&& (diagnoses[0].getValueCharacteristics()[7].getValue() == 0))
					return false;
			}
			else
			{
				// Non-metastatic diagnosis: left [1] and right [2]
				boolean anyDiagnosis = false;
				for (int diagnosisIndex = 1; diagnosisIndex <= 2; diagnosisIndex++)
				{
					// Diagnosed; skip if no
					if (diagnoses[diagnosisIndex].getDiagnosed() == 0)
						return false;
					if (diagnoses[diagnosisIndex].getDiagnosed() == 2)
						continue;
					anyDiagnosis = true;
					// Diagnosis: Do not accept subjective answer
					if (diagnoses[diagnosisIndex].getAttributeCharacteristics()[0].getValue() == 0)
						return false;
					if ((diagnoses[diagnosisIndex].getAttributeCharacteristics()[0].getValue() & 1l) != 0)
						return false;
					// ER [0], PR [1], HER2/neu [2], stage [3], inflammatory
					// [4], tumor size [5], lymph nodes [6]
					for (int valueIndex = 0; valueIndex <= 6; valueIndex++)
						if (diagnoses[diagnosisIndex].getValueCharacteristics()[valueIndex].getValue() == 0)
							return false;
					// Survivor: first breast cancer diagnosis
					if ((category == 16) && diagnoses[diagnosisIndex].getFirstDiagnosis().isFresh())
						return false;
					// Lymph node count
					if (!diagnoses[diagnosisIndex].getLymphNodeCount().isValid())
						return false;
				}
				if (!anyDiagnosis)
					return false;
			}

			// My Treatment: Treatment history; all required except biological,
			// which is required if chemo selected
			for (int index = 0; index < treatmentPerformed.length; index++)
				if ((index != 3) && treatmentPerformed[index].isFresh())
					return false;
			if (treatmentPerformed[2].isBooleanValue() && treatmentPerformed[3].isFresh())
				return false;
			// Performed procedure
			for (FacadeProcedure facadeProcedure : procedures)
				if (facadeProcedure.isPerformed())
				{
					// Started year
					if (facadeProcedure.getStarted().isVacant())
						return false;
					if (facadeProcedure.isBrief())
						continue;
					// (if not brief) Current
					if (facadeProcedure.getCurrent() == 0)
						return false;
					// (if not current) Ended year
					if ((facadeProcedure.getCurrent() == 2) && facadeProcedure.getCompleted().isVacant())
						return false;
				}
			// Performed therapy
			for (FacadeTherapy facadeTherapy : therapies)
			{
				if (facadeTherapy.isPerformed())
				{
					if ( facadeTherapy.isTherapyOther() )
					{	
						return true;
					}
					
					// Started year
					if (facadeTherapy.getStarted().isVacant())
						return false;
					// Setting
					if (facadeTherapy.getSetting() == 0)
						return false;
					// Current status
					if (facadeTherapy.getStatus() == 0)
						return false;
					// (if incomplete) Reason
					if ((facadeTherapy.getStatus() == 8) && (facadeTherapy.getIncomplete() == 0))
						return false;
					// (if completed) End year
					if ((facadeTherapy.getStatus() == 4) && facadeTherapy.getCompleted().isVacant())
						return false;
					// (if incomplete or concurrent) Length
					if (facadeTherapy.isChemotherapy() && (facadeTherapy.getLength() == 0)
							&& ((facadeTherapy.getStatus() == 8) || (facadeTherapy.getStatus() == 2)))
						return false;
				}
			}	
		}
		// If all checks passed then history is valid
		return true;
	}

	/**
	 * If Skip (UNSURE) is checked on the Race/Ethnicity question (on tab 4), then no other options may
	 * be checked.  Other validations may be added here as well.
	 * 
	 * @return true if valid, false otherwise
	 */
	public boolean isValidRaceEthnicity()
	{
		long raceEthnicityChoices = this.getAttributeCharacteristics()[12].getValue(); // index 12 is associated with Race/Ethnicity.  We *hope* this does not change.
		CharacteristicCode[] raceEthnicity = FacadePatientHistory.ATTRIBUTE_CHARACTERISTICS.get(RACE);
		boolean skipChecked = false;
		boolean besidesSkipChecked = false;
		
		for (int i = 0; i < raceEthnicity.length; i++)
		{
			boolean checked = (raceEthnicityChoices & (1 << (i + 1))) > 0;
			if (checked)
			{	
				if (UNSURE.equals(raceEthnicity[i]))
				{
					skipChecked = true;
				}
				else
				{
					besidesSkipChecked = true;
				}
			}
		}
		return !(skipChecked && besidesSkipChecked);
	}
	
	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = 7461850578836737085L;
}
