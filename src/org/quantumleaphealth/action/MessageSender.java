/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import javax.ejb.Local;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

/**
 * Sends a message to the system's mail session. Implementations must provide
 * the following email addresses
 * 
 * @author Tom Bechtold
 * @version 2008-12-19
 */
@Local
public interface MessageSender
{
	/**
     *  Used to send avatar links by email to up to ten recipients from avatar pages.
     */
	public void sendLink();

	/**
	 * clear SimpleMessage and EmailLinkMessage fields. 
	 */
	public void reset();
	
	/**
	 * Sends the <code>SimpleMessage</code> instance to the session's
	 * <tt>divert</tt> address or to the session's <tt>contact</tt> address if
	 * <tt>divert</tt> is not specified. If the message was sent then its
	 * <code>delivered</code> property is set; otherwise a Faces message is
	 * created.
	 */
	public void send();

	/**
	 * Sends a message.
	 * 
	 * @param recipient
	 *            the recipient or <code>null</code> to specify the session's
	 *            <tt>contact</tt> address
	 * @param replyTo
	 *            the recipient or <code>null</code> to specify the session's
	 *            <tt>contact</tt> address
	 * @param subject
	 *            the subject of the message
	 * @param body
	 *            the body of the message
	 * @throws IllegalArgumentException
	 *             if <code>subject</code> or <code>body</code> is
	 *             <code>null</code>
	 * @throws MessagingException
	 *             if the message could not be sent
	 */
	public void send(InternetAddress recipient, InternetAddress replyTo, String subject, String body)
			throws IllegalArgumentException, MessagingException;
}
