package org.quantumleaphealth.action;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;

import org.hibernate.validator.Email;
import org.hibernate.validator.Length;
import org.hibernate.validator.NotNull;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.patient.UserPatient;

/***
 * Implementation of avatar create methods used on the superuser page to create new avatars.
 * Creating an avatar will create a record in the userpatient table with an empty patient history.
 * It will then be up to the administrator to configure the avatar by going to the bct home page
 * and logging in as the new avatar.
 * 
 * @author wgweis
 *
 */
@Stateful
@Name("avatarCreateInterface")
public class AvatarCreateAvatarInterfaceAction implements AvatarCreateAvatarInterface
{	
	
	private static final Long AVATAR_POSTAL_CODE = 941L; // this is the start of the Geary Office postal code.
														 // we have to set the postalcode to something for the
														 // administrator to be able to update the avatar history.
	/**
	 * Standard injected logger.
	 */
	@Logger
	private static Log LOGGER;
	
	/**
	 * injected entity manager.  We will use the default transaction management.
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;

	/**
	 * injected facesmessges so that we can directly add messages for display on the web pages.
	 */
	@In
	FacesMessages facesMessages;

	/**
	 * New Avatar variables.
	 */
	private String newAvatarTitle;
	private String newAvatarDescription;
	private String newAvatarEmail;

	/**
	 * There is one password defined for all avatars as requested by the bct UCSF staff.
	 * The password is defined in the eligibility.properties file.
	 */
	static private final String AVATAR_DEFAULT_PASSWORD = "avatarDefaultPassword";

	/**
	 * We will not allow avatars with duplicate names.  This would be caught at the database level with our 
	 * unique index, but doing it here will allow us to present a friendlier message to the user.
	 * @param principal
	 * @return
	 */
	private boolean avatarExists( String principal )
	{
		try
		{
			em.createQuery( "select p from UserPatient p where upper(principal) = upper(:principal)" ).setParameter("principal", principal ).getSingleResult();
			return true;
		}
		catch ( javax.persistence.NoResultException nre )
		{
			return false;
		}
		catch( NonUniqueResultException nure )
		{
			return true;
		}
	}
	
	/**
	 * Retrieve the avatar password from the eligibility.properties file.  If we can't we are in big trouble.
	 * @return the password if possible, null otherwise.
	 */
	private String retrieveDefaultAvatarPassword()
	{
		try
		{
			return ResourceBundle.getBundle("EligibilityCriteria").getString( AVATAR_DEFAULT_PASSWORD );
		}
		catch ( MissingResourceException missingResourceException2 )
		{
			facesMessages.add(Severity.ERROR, "The Default Avatar Password could not be retrieved.  Please contact the Administrator." );
			return null;
		} 
	}
	
	/**
	 * save the avatar profile.
	 */
	@Override
	public void saveAvatar(String utype)
	{
		try
		{
			if ( avatarExists( this.newAvatarEmail ) )
			{
				facesMessages.add(Severity.ERROR, "A UserPatient record with this Id already exists for  " + newAvatarEmail );
				return;
			}	

			String newAvatarPassword = retrieveDefaultAvatarPassword();
			
			if ( newAvatarPassword != null )
			{
				UserPatient newAvatar = new UserPatient();
				newAvatar.setUsertype(utype.equalsIgnoreCase("Avatar") ? UserPatient.UserType.AVATAR_PROFILE: UserPatient.UserType.READ_ONLY_PROFILE );
				newAvatar.setAvatarProfileDescription( this.newAvatarDescription );
				newAvatar.setAvatarProfileTitle( this.newAvatarTitle );
				newAvatar.setPrincipal( this.newAvatarEmail );
				newAvatar.setPatientHistoryComplete( true );
				newAvatar.setPrincipalInvalidEmailAddress( true );
				newAvatar.setCredentials( newAvatarPassword );
				newAvatar.setTermsAccepted( true );
				newAvatar.setCountryCode( PatientInterfaceAction.COUNTRY_UNITEDSTATES );
				newAvatar.setPrivatePostalCode( AVATAR_POSTAL_CODE );
				em.persist( newAvatar );
				
				// if all goes well add an informational message to facesMessages
				facesMessages.add(Severity.INFO, "New " + utype + " succesfully created with Id " + newAvatarEmail );
				newAvatarDescription = null;
				newAvatarEmail = null;
				newAvatarTitle = null;
			}
			else
			{
				LOGGER.fatal( "The default avatar password could not be retrieved.  Check to see if property bundle has " + AVATAR_DEFAULT_PASSWORD + " defined.", new Exception() );
			}
		}
		catch ( Exception e )
		{
			facesMessages.add(Severity.ERROR, "Record could not be updated for avatar " + newAvatarEmail );		
			LOGGER.fatal( "Record could not be updated for avatar " + newAvatarEmail, e );
		}
	}
	/**
	 * @param newAvatarTitle the newAvatarTitle to set
	 */
	@Override
	@Length( max=128 )
	public void setNewAvatarTitle(String newAvatarTitle)
	{
		this.newAvatarTitle = newAvatarTitle;
	}

	/**
	 * @return the newAvatarTitle
	 */
	@Override
	public String getNewAvatarTitle()
	{
		return newAvatarTitle;
	}

	/**
	 * @param newAvatarDescription the newAvatarDescription to set
	 */
	@Override
	@Length( max=256 )
	public void setNewAvatarDescription(String newAvatarDescription)
	{
		this.newAvatarDescription = newAvatarDescription;
	}

	/**
	 * @return the newAvatarDescription
	 */
	@Override	
	public String getNewAvatarDescription()
	{
		return newAvatarDescription;
	}

	/**
	 * @param newAvatarEmail the newAvatarEmail to set
	 */
	@Email(message="This is not a valid Email.  Cannot Save.")
	@NotNull
	@Override	
	public String getNewAvatarEmail()
	{
		return newAvatarEmail;
	}

	/**
	 * set the avatar email id.
	 * @param the avatar email to set.
	 */
	@Override
	public void setNewAvatarEmail(String newAvatarEmail)
	{
		this.newAvatarEmail = newAvatarEmail;
	}

	/**
	 * Reset instance variables when stateful session bean is destroyed.
	 * 
	 * Required by EJB3.
	 */	
	@Remove
	@Destroy
	public void destroy()
	{
	}	
}
