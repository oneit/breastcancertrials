package org.quantumleaphealth.action;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.hibernate.CacheMode;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.patient.UserPatient;

@Stateful
@Name("avatarUpdateInterface")
public class AvatarUpdateAvatarInterfaceAction implements AvatarUpdateAvatarInterface
{
	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log LOGGER;

	/**
	 * Injected entity manager.  We will manage the transcation. 
	 */
	@PersistenceContext(type = PersistenceContextType.EXTENDED, unitName = "bct")	
	private EntityManager em;

	/**
	 * Injected faces messages so that we can display messages to users on the avatar update section on the su/menu.seam page.
	 */
	@In
	FacesMessages facesMessages;

	/**
	 * Returns list of existing avatars for use in a JSF datatable.
	 */
	@DataModel
	private List<UserPatient> avatars;

	/**
	 * The instance selected from the JSF datatable.
	 */
	@SuppressWarnings("unused")
	@DataModelSelection("avatars")
    private UserPatient avatarSelection;

	/**
	 * Returns list of existing avatars for use in a JSF datatable.
	 */
	@DataModel
	private List<UserPatient> readonlyusers;

	@SuppressWarnings("unused")
	@DataModelSelection("readonlyusers")
	private UserPatient readonlyuser;
	
	/**
	 * factory method so that SEAM can construct the avatar list.
	 */
	@Override
	@Factory("avatars")
	public void loadAvatars()
	{
		LOGGER.debug("Loading avatars");
		avatars = getSpecialUsers(UserPatient.UserType.AVATAR_PROFILE);
		LOGGER.debug("Finished Loading avatars");
	}

	/**
	 * factory method so that SEAM can construct the avatar list.
	 */
	@Override
	@Factory("readonlyusers")
	public void loadReadOnlyUsers()
	{
		LOGGER.debug("Loading read-only users");
		readonlyusers = getSpecialUsers(UserPatient.UserType.READ_ONLY_PROFILE);
		LOGGER.debug("Finished Loading read-only users");
	
	}

	@SuppressWarnings( {"unchecked", "cast"})	
	private List<UserPatient> getSpecialUsers(UserPatient.UserType usertype)
	{
		
		// the following query expects an avatar principal in the format of 'AvatarXXX@bct.org' where XXX consists of digits for ordering.  A default value of '0' is
		// returned if the avatar principal does not conform to this format.  It will only work with the postgres dialect.
		// Finally, the cast must be performed last.  The 8.2.3 jdbc implementation of cast cannot handle null values. 
		List<UserPatient> ulist = (List<UserPatient>) em.createQuery(
				"select p from UserPatient p where p.usertype = :usertype order by cast(coalesce(substring(substring(p.principal, 7), '[0-9]+'), '0') as int)")
				.setParameter("usertype", usertype)
				.setHint("org.hibernate.readOnly", Boolean.TRUE)
				.setHint("org.hibernate.cacheMode", CacheMode.IGNORE).getResultList();
		
		if ( ulist == null )
		{	
			LOGGER.error( "No entries found of type " + usertype);
			return new ArrayList<UserPatient>();
		}	
		else
		{
			LOGGER.info("Finished loading #0 entries of type #1", ulist.size(), usertype );				
		}
		return ulist;
	}

	private void updateSpecialUserTypes(List<UserPatient> userList)
	{
		try
		{
			for ( UserPatient user: userList )
			{
				em.createQuery( "update UserPatient set avatarProfileDescription = :description, avatarProfileTitle = :title where id = :id")
				.setParameter("description", user.getAvatarProfileDescription())
				.setParameter("title", user.getAvatarProfileTitle() )				
				.setParameter("id", user.getId() )				
				.executeUpdate();
			}
			facesMessages.add(Severity.INFO, "Record(s) successfully updated.");
		}
		catch ( Exception e )
		{
			facesMessages.add(Severity.ERROR, "Records could not be updated.");			
		}
	}
	
	/**
	 * update the database entry for this avatar.
	 */
	@Override
	public void updateAvatars()
	{
		updateSpecialUserTypes(avatars);
	}
	
	/**
	 * update the database entry for this readonly user.
	 */
	@Override
	public void updateReadOnly()
	{
		updateSpecialUserTypes(readonlyusers);
	}

	/**
	 * Reset instance variables when stateful session bean is destroyed.
	 * 
	 * Required by EJB3.
	 */	
	@Remove
	@Destroy
	public void destroy()
	{
	}
}
