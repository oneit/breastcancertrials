package org.quantumleaphealth.action;

import java.io.Serializable;
import java.util.Date;

/**
 * Facade class for exposing the quartz jobs on the superuser menu page.
 * 
 * @author admin
 *
 */
public class FacadeQuartzJob implements Serializable 
{

        private static final long serialVersionUID = 6326971889167061423L;

        private String jobName;
        private String triggerName;
        private String triggerCronExpression;
        private Date lastTriggerTime;
        private Date nextTriggerTime;
        private Integer newTriggerDay;
        private Date newTriggerTime;

        /**
         * Default constructor
         */
        public FacadeQuartzJob() {
                super();
        }
        
        /**
         * Parameters to load quartz job with
         * 
         * @param _jobName
         * @param _lastTriggerTime Last run time of current job trigger.
         * @param _nextTriggerTime Next run time of current job trigger.
         */
        public FacadeQuartzJob( String _jobName, Date _lastTriggerTime, Date _nextTriggerTime ) {
                super();
                this.jobName = _jobName;
                this.lastTriggerTime = _lastTriggerTime;
                this.nextTriggerTime = _nextTriggerTime;
        }

        /**
         * The day is not updatable if it is not a single integer.  BCT jobs come in two flavors:
         * those that run daily and those that run once a week.  Updating the run day is meaningless
         * for a job that runs daily.
         * 
         * @return true if this job runs once a week, false otherwise.
         */
    	public boolean isDayNotUpdatable() 
    	{
    		if (triggerCronExpression == null)
    		{
    			return false;
    		}
    		
    		try
    		{
    			Integer.parseInt(triggerCronExpression.split("\\s+")[5]); 
    		}
    		catch(NumberFormatException nfe)
    		{
    			return true;
    		}
    		return false;
    	}

        /**
         * @return the jobName
         */
        public String getJobName() {
                return jobName;
        }
        /**
         * @param jobName the jobName to set
         */
        public void setJobName(String jobName) {
                this.jobName = jobName;
        }
        /**
		 * @param triggerName the triggerName to set
		 */
		public void setTriggerName(String triggerName) {
			this.triggerName = triggerName;
		}
		/**
		 * @return the triggerName
		 */
		public String getTriggerName() {
			return triggerName;
		}
		/**
		 * @param triggerCronExpression the triggerCronExpression to set
		 */
		public void setTriggerCronExpression(String triggerCronExpression) {
			this.triggerCronExpression = triggerCronExpression;
		}
		/**
		 * @return the triggerCronExpression
		 */
		public String getTriggerCronExpression() {
			return triggerCronExpression;
		}
		/**
         * @return the lastTriggerTime
         */
        public Date getLastTriggerTime() {
                return lastTriggerTime;
        }
        /**
         * @param lastTriggerTime the lastTriggerTime to set
         */
        public void setLastTriggerTime(Date lastTriggerTime) {
                this.lastTriggerTime = lastTriggerTime;
        }
        
        /**
         * @return the nextTriggerTime
         */
        public Date getNextTriggerTime() {
                return nextTriggerTime;
        }
        
        /**
         * @param nextTriggerTime the nextTriggerTime to set
         */
        public void setNextTriggerTime(Date nextTriggerTime) {
                this.nextTriggerTime = nextTriggerTime;
        }
        
		/**
		 * @param newTriggerDay the newTriggerDay to set
		 */
		public void setNewTriggerDay(Integer newTriggerDay) {
			this.newTriggerDay = newTriggerDay;
		}
		
		/**
		 * @return the newTriggerDay
		 */
		public Integer getNewTriggerDay() {
			return newTriggerDay;
		}
		
		/**
		 * @param newTriggerTime the newTriggerTime to set
		 */
		public void setNewTriggerTime(Date newTriggerTime) {
			this.newTriggerTime = newTriggerTime;
		}
		/**
		 * @return the newTriggerTime
		 */
		public Date getNewTriggerTime() {
			return newTriggerTime;
		}
}

        