/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.CacheMode;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Out;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.screen.DeclarativeMatchingEngine;
import org.quantumleaphealth.screen.MatchingEngine;

/**
 * Loads a matching engine into the Seam application context. Transactions are
 * <code>NOT_SUPPORTED</code> because this class only reads data.
 * 
 * @author Tom Bechtold
 * @version 2009-03-09
 */
@Stateless
@Name("engineManager")
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class EngineManagerAction implements EngineManagerLocal, EngineManagerRemote, Serializable
{
	/**
	 * UID for serialization
	 */
	private static final long serialVersionUID = -8110826096176729741L;
	
	/**
	 * Loads the trial data using EJB3 persistence manager
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;
	/**
	 * Injected logging facility
	 */
	@Logger
	private Log log;
	/**
	 * The matching engine, outjected to Seam's application context
	 */
	@Out(scope = ScopeType.APPLICATION)
	private MatchingEngine engine;

	/**
	 * The EJB3 query language string for eager-loading trial data. Loaded
	 * trials are both open and listed.
	 */
	public static final String EJBQL = "select distinct t from Trial t left join fetch t.trialSite ts left join fetch ts.site left join fetch ts.contact left join fetch ts.principalInvestigator left join fetch ts.screenerGroup where t.open=true and t.listed=true order by t.id";
	/**
	 * The name of the JPA-implementation specific query hint to identify the
	 * result as being read-only.
	 */
	private static final String HINT_READONLY = "org.hibernate.readOnly";

	/**
	 * Loads a matching engine into the instance variable. This method logs
	 * performance and memory statistics. It is run once after the bean is
	 * created. This method verifies that the engine has not been loaded more
	 * recently than <code>getMinimumReloadInterval</code> milliseconds. TODO:
	 * Remove SuppressWarnings once JPA fixes untyped collections
	 */
	@Factory(value = "engine", autoCreate = true)
	@SuppressWarnings( { "unchecked", "cast" })
	public void load()
	{
		// Indicate that trials are being loaded
		long beforeCreationTime = System.currentTimeMillis();
		long maximumMemory = Runtime.getRuntime().maxMemory();
		float beforeCreationMemory = Runtime.getRuntime().freeMemory() * 100f / maximumMemory;
		try
		{
			// Get all open trials in order of their id
			List<Trial> trials = (List<Trial>) (em.createQuery(EJBQL).setHint(HINT_READONLY, Boolean.TRUE).setHint(
					"org.hibernate.cacheMode", CacheMode.IGNORE).getResultList());
			MatchingEngine matchingEngine = new DeclarativeMatchingEngine(trials);
			log.info("Matching engine loaded #0 trials [#1;#2] in #3ms shrinking #4mb memory from #5% to #6%", trials
					.size(), matchingEngine.getLastModified(), matchingEngine.getLastRegistered(), System
					.currentTimeMillis()
					- beforeCreationTime, maximumMemory / 1000000.0f, beforeCreationMemory, Runtime.getRuntime()
					.freeMemory()
					* 100f / maximumMemory);
			engine = matchingEngine;
		}
		catch (Throwable throwable)
		{
			log.error("Cannot load matching engine", throwable);
		}
	}

	/**
	 * @param em the em to set
	 */
	public void setEm(EntityManager em) {
		this.em = em;
	}

	/**
	 * @param log the log to set
	 */
	public void setLog(Log log) {
		this.log = log;
	}

	/**
	 * @return the engine
	 */
	public MatchingEngine getEngine() {
		return engine;
	}
}
