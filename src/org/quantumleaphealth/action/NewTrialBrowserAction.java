package org.quantumleaphealth.action;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.queryparser.classic.QueryParser.Operator;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.Trial_Category;
import org.quantumleaphealth.model.trial.Trial.Type;
import org.quantumleaphealth.screen.MatchingEngine;
import org.quantumleaphealth.utility.LuceneSearchStringFormatter;

@Stateful
@Name("newTrialBrowser")
public class NewTrialBrowserAction implements NewTrialBrowser
{
    /**
     * The matching engine
     */
    @In
    private MatchingEngine engine;

	/**
	 * Persists the entity beans using EJB3 persistence context
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;

    /**
     * parameter for ignoring All Pagination (for su use only).
     * We won't be tracking this in the pages.xml file and it will not be
     * referenced on the trials page, so we don't provide a setter or getter. 
     */
    @RequestParameter( "ignp" )
    private Integer ignorePagination;
    
    /**
     * parameter for viewing an individual trial (for linking to an individual trial from salesforce)
     */
    @RequestParameter( "tid" )
    private Long trialID;    

	/**
	 * postal code for proximity searches.
	 */
	private Long postalCode;
	
	/**
	 * The category string web parameter
	 */
	private String categoryString;
	
	/**
	 * start record of the current page.
	 */
	private Integer pageRecord;

	/**
	 * list of available pages
	 */
	private ArrayList<Integer> pagesList = null;

	/**
	 * The special "All" category which is not tied to the
	 * database trial table "type" field.
	 */
	private static final String AllCategory = "All";
	
	/**
	 * The special "Search" category which is not tied to the
	 * database trial table "type" field.
	 */
	private static final String SearchCategory = "Search";

	/**
	 * The special "New" category which is not tied to the
	 * database trial table "type" field.
	 */
	private static final String NewCategory = "New";
	
	@In("#{bundle['NewTrialsDescription']}") private String newDescription;  

	@In("#{bundle['NewTrialsAltText']}") private String newAltText;  

	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log logger;

	/**
	 * Pagination Support
	 */
	/**
	 * How many records are currently in the trial list.  Not
	 * available outside of this class.
	 */
    private int recordCount = 0;

    /**
     * return a counter so that the number of results displayed can be double-checked.
     */
    private int rowCount = 0;
    
    /**
     * return the maximum last modified date for the list of trials to be displayed on this page.
     */
    private Date lastModified;
    
    /**
     * search parameters
     */
    private String searchString1;
    private String searchOperator;
    private String searchString2;
    
    private List<TrialProximity> searchTrialList;
    
	private Set<Long> searchResults;

    /**
     * getter for the rowCount
     * @return the pre-incremented value.
     */
    public int getRowCount()
    {
    	return ++rowCount;
    }
    
    /**
     * How many records to display per page.
     */
    private static final Integer pageRecordIncrement = 50;

    /**
     * a reference to the original class used to create the browse_trials page.
     */
	@In (create=true)
	private TrialBrowser trialBrowser;

	/**
	 * A hash of counts by category
	 */
	private HashMap<String, Integer> categoryCounts = null;

	private static final String LUCENE_SEARCH_FIELD = "contents";
	
	private String formattedSearchString;


	/**
	 * @return the current value of the postalcode
	 */
	@Override
	public Long getPostalCode()
	{
		return postalCode;
	}

	/**
	 * set the postal code
	 */
	@Override
	public void setPostalCode(Long postalCode)
	{
		this.postalCode = postalCode;
	}

	/**
	 * Return a list of categories for all Treatment categories.
	 */
	@Override
    public List<String> getTreatmentCategoryKeys()
    {
		return getCategoryKeys( true );
    }

	/**
	 * Return a list of categories for all categories other than All or Treatment
	 */
	@Override
    public List<String> getOtherCategoryKeys()
    {
		return getCategoryKeys( false );
    }
	
	/**
	 * wrapper for returning the engine's category counts.
	 * Modified to return keys in the order desired by the bct staff.
	 */
    private List<String> getCategoryKeys( boolean includeTreatment )
    {
		ArrayList<String> categoryKeys = new ArrayList<String>();
		ArrayList<Type> TypeList = includeTreatment? Trial.BROWSE_PAGE_TREATMENT_TRIALS: Trial.BROWSE_PAGE_OTHER_TRIALS;
		for ( org.quantumleaphealth.model.trial.Trial.Type type: TypeList )
		{
			if ( getCategoryCountList().containsKey( type.name() ) )
			{
				categoryKeys.add( type.name() );
			}
		}
		return categoryKeys;
    }

	/**
	 * @return the category count for the given key or the entire trial count if the key is null or the search type is by zip.
	 */
	@Override	
    public Integer getCategoryCount( String key )
    {
		if ( key != null && key.trim().length() > 0 )
		{
			if ( AllCategory.equalsIgnoreCase( key ) )
			{
				return engine.getTrialCount();
			}
			else if (SearchCategory.equals(key))
			{
				return getSearchStringHits().size();
			}
			// We should be good at this point.
			return getCategoryCountList().get( key.toUpperCase() );
		}
		return engine.getTrialCount();
    }

	/**
	 * Get a Hash of categories and counts associated with them.
	 * @return the HashMap with categories and counts.
	 */
	@SuppressWarnings("unchecked")
	private HashMap<String, Integer> getCategoryCountList()
	{
		if ( categoryCounts == null )
		{
			categoryCounts = new HashMap<String, Integer>();

			List<Object> results = 
					em.createQuery( "select count(*) as cnt, r.trial_CategoryPK.category from Trial_Category r, Trial t where t.listed = true and t.open = true and t.id = r.trial_CategoryPK.trialid group by 2" )
						.setHint("org.hibernate.readOnly", Boolean.TRUE).getResultList();	
			
			for ( Object row: results)
			{
				Object[] fields = (Object[]) row;
				Short ndx = (Short) fields[1];
				try
				{
					categoryCounts.put( Trial.Type.values()[ ndx.intValue() ].name() , ((Long) fields[0]).intValue() );
					// for some of the categories where the name of the category used on the web site doesn't actually match the category.
					categoryCounts.put( Trial.Type.values()[ ndx.intValue() ].getWebCategory() , ((Long) fields[0]).intValue() );
				}
				catch ( ArrayIndexOutOfBoundsException ae )
				{
					logger.debug("An ArrayIndexOutOfBoundsException was caught.  The index referenced was #0", ndx, ae); 
				}
			}
		}
		return categoryCounts;
	}

	/**
	 * @return return a label for the given key.
	 */
	@Override	
    public String getCategoryLabel( String key )
    {
		if ( key == null || 
				( this.getCategoryCount( key ) == null && 
						!AllCategory.equalsIgnoreCase(key) && 
						!NewCategory.equalsIgnoreCase(key) &&
						!SearchCategory.equalsIgnoreCase(key)
				)
			)
		{
			return "label.type.NOTFOUND";
		}
		return "label.type." + key.toUpperCase();
    }

	/**
	 * @return return a label for the given key.
	 */
	@Override	
    public String getCategoryAltText( String key )
    {
		if (NewCategory.equalsIgnoreCase(key))
		{
			return newAltText;
		}
		
		if (key != null  && !key.equalsIgnoreCase(AllCategory))
		{
			Trial.Type type = Trial.getTypeWebCategories().get(key.toUpperCase());
			return type == null? "": type.getAltText();
		}	
		return "";
    }
	
    /**
     * Return an index into the EligibilityCriteria.properties file for the google analytics key
     * mapped to the category being viewed.
     * 
     * @param key
     * @return the index into the EligibilityCriteria.properties file.  Index to 'Unknown Category' if 
     * something is wrong with the key.
     * 
     */
	@Override	
    public String getCategoryGoogleAnalyticsCode( String key )
    {
		if ( key == null || (this.getCategoryCount( key ) == null && 
								!AllCategory.equalsIgnoreCase(key) && 
								!NewCategory.equalsIgnoreCase(key) &&
								!SearchCategory.equalsIgnoreCase(key)))
		{
			return "google.analytics.NOTFOUND";
		}
		return "google.analytics." + key.toUpperCase();
    }

	/**
	 * @param categoryString the categoryString to set
	 */
	@Override		
	public void setCategoryString(String categoryString)
	{
		this.categoryString = categoryString;
	}

	/**
	 * @return the categoryString
	 */
	@Override	
	public String getCategoryString()
	{
		if ( categoryString == null || categoryString.trim().length() <= 0)
		{
			if ( trialID != null )
			{
				String newCategory = this.getTrialPageViewCategory();
				if ( newCategory != null )
				{
					categoryString = newCategory;
				}
			}	
			else
			{	
				categoryString = AllCategory;
			}	
		}
		return categoryString;
	}

	
	@Override
	public String getCategoryDescription()
	{
		if (categoryString.equalsIgnoreCase(NewCategory))
		{
			return newDescription;
		}
		
		if (categoryString != null  && !categoryString.equalsIgnoreCase(AllCategory))
		{	
			try
			{
				Trial.Type type = Trial.Type.valueOf(categoryString.toUpperCase());
				return type.getWebDescription();
			}
			catch (IllegalArgumentException e)
			{
				return ""; // no description for unknown category.
			}
		}	
		return "";
	}
	
	/**
	 * @param pageRecord the pageRecord to set
	 */
	@Override	
	public void setPageRecord(Integer pageRecord)
	{
		this.pageRecord = pageRecord;
	}

	/**
	 * @return the pageRecord
	 */
	@Override	
	public Integer getPageRecord()
	{
		return pageRecord == null? 0: pageRecord;
	}

	/**
	 * set the TrialBrowser's parameters for filtering.  See comment below.
	 */
	private void setTrialBrowserParameters()
	{
		if ( getPostalCode() != null )
		{
			trialBrowser.setPostalCode( getPostalCode() );
			trialBrowser.getTrials().setGroupedByProximity( 2 ); // this should ensure the trial list is destroyed and then regenerated.
		}
	}
	
	/**
	 * determine whether to add the trial to our list of trials to display
	 * @param trialProximity
	 * @return true if conditions are met for an add, false otherwise
	 */
	private boolean addToList( TrialProximity trialProximity, Calendar nowCalendar )
	{
		try {
			
			// all category always includes a trials
			if (getCategoryString().equalsIgnoreCase(AllCategory)) 
			{
				return true;
			}
			
			// for the search category always include a trial in the first phase.  Trials are filtered in a two phase process
			if (SearchCategory.equalsIgnoreCase(getCategoryString()))
			{
				return true;
			}

			// new category includes a trial if it was posted or modified in the last two weeks.
			if (getCategoryString().equalsIgnoreCase(NewCategory))
			{
				return TrialProximityGroups.isNewTrial(trialProximity.getTrial(), nowCalendar); 
			}
			
			// determine based on category
			short categoryOrdinal = (short) Trial.Type.valueOf(getCategoryString().toUpperCase()).ordinal();

			for (Trial_Category category : trialProximity.getTrial().getTrial_Categories()) 
			{
				if (category.getTrial_CategoryPK().getCategory() == categoryOrdinal) 
				{
					return true;
				}
			}
		} 
		catch (IllegalArgumentException iae) 
		{
			logger.error("Error caught when retrieving category enum for #0", getCategoryString());
		}
		return false;
	}

	/**
	 * determine whether the record should be included in this page group
	 * @return true if conditions are met, false otherwise
	 */
	private boolean recordOnCurrentPage()
	{
		// if this parameter is defined then only one page regardless of category.
		if ( ignorePagination != null && ignorePagination == 1 )
		{
			return true;
		}
		
		// never have pagination for a category other than 'All'
		if ( !getCategoryString().equalsIgnoreCase( AllCategory ) )
		{
			return true;
		}
		
		// this case is only valid for 'All' with no ignorPagination parameter defined, or defined with a value other than 1.
		return recordCount >= getPageRecord() && recordCount < getPageRecord() + pageRecordIncrement;
	}

	private boolean reachedPageLimit()
	{
		 return recordCount++ > getPageRecord() + pageRecordIncrement && 
		 			AllCategory.equalsIgnoreCase(getCategoryString()) && 
		 			(ignorePagination == null || ignorePagination != 1 );
	}
	
	public Integer getNewTrialCount()
	{
		Integer newCount = 0;
		Calendar nowCalendar = Calendar.getInstance();  // set automatically to new Date().  Only used for the New category
		
		for ( TrialProximityGroup trialProximityGroup: trialBrowser.getTrials().getSortedList())
		{
			for ( TrialProximity trialProximity: trialProximityGroup )
			{
				if (TrialProximityGroups.isNewTrial(trialProximity.getTrial(), nowCalendar))
				{
					newCount++;
				}
			}
		}
		return newCount;
	}
	   
	/**
	 * As per agenda of 4/21/2014 multiple terms in a search box will be surrounded by double quotes.
	 * @param term
	 * @return
	 */
	private String getPossiblyQuotedSearchString(String term)
	{
		String s = term == null? "": term.trim();
		
		if (s.length() > 0 && !s.startsWith("\"")) //don't add a quote if there is one there already.
		{
			return '"' + s + '"';
		}
		return s;
	}
	
	private String getBooleanSearchString()
	{
		String searchTerm1 = getPossiblyQuotedSearchString(searchString1);
		String searchTerm2 = getPossiblyQuotedSearchString(searchString2);
		
		if (searchTerm1.length() > 0 && searchTerm2.length() > 0)
		{
			return  searchTerm1 + " " + getSearchOperator() + " " + searchTerm2;
		}
		else if (searchTerm1.length() > 0)
		{
			return searchTerm1;
		}
		else if (searchTerm2.length() > 0)
		{
			return searchTerm2;
		}
		return "";
	}
	
    private Set<Long> getSearchStringHits()
    {
		if (searchResults == null)
		{	
			searchResults = new TreeSet<Long>();
			String luceneDataDirectory = System.getProperty("jboss.server.home.dir") + "/lucene_data";
	
			IndexReader reader = null;
			Analyzer analyzer = null;
			
			try
			{
				formattedSearchString = new LuceneSearchStringFormatter().formatSearchString(getBooleanSearchString());
				logger.info("pre-processed search string is " + formattedSearchString);
				
				if (formattedSearchString.trim().length() == 0)
				{
					return searchResults;
				}
					  
				reader = DirectoryReader.open(FSDirectory.open(new File(luceneDataDirectory)));
				IndexSearcher searcher = new IndexSearcher(reader);
				analyzer = new StandardAnalyzer(Version.LUCENE_46);
				    
				QueryParser parser = new QueryParser(Version.LUCENE_46, LUCENE_SEARCH_FIELD, analyzer);
				parser.setAllowLeadingWildcard(true);
				parser.setDefaultOperator(Operator.AND);
		
				Query query = parser.parse(formattedSearchString);
				logger.info("parsed query string is " + query.toString());
			    
				TopDocs results = searcher.search(query, Integer.MAX_VALUE); // get them all
				ScoreDoc[] hits = results.scoreDocs;
			    
				for (ScoreDoc scoreDoc: hits)
				{
					Document doc = searcher.doc(scoreDoc.doc);
					searchResults.add(Long.decode(doc.get("id")));
				}
			}
			catch (IOException ioe)
			{
				logger.fatal("canot open lucene data file location.", ioe);
			}
			catch (ParseException pe)
			{
				logger.fatal("cannot parse search string " + searchString1, pe);
			}
			finally
			{
				if (analyzer != null)
				{
					analyzer.close();
				}
				if (reader != null)
				{
					try 
					{
						reader.close();
					} catch (IOException e) 
					{
						// Ignore exception.  There is not much we can do at this point.
					}
				}
			}
		}
		return searchResults;
	  }
	
	private List<TrialProximity> getSearchTrialList()
	{
		if (searchTrialList == null)
		{	
			Set<Long> searchHits = getSearchStringHits();
			searchTrialList = new ArrayList<TrialProximity>();

			for (TrialProximity trial: getNonSearchTrialList())
			{
				if (searchHits.contains(trial.getTrial().getId()))
				{
					// instead of adding a TrialProximity instance, add an extension of it which will return marked up text
					searchTrialList.add(new LuceneSearchTrialProximity(trial, formattedSearchString));
				}
			}
		}
		return searchTrialList;
	}
	
	/***
	 * create the list of trials to display on this page.
	 */
	private List<TrialProximity> getNonSearchTrialList()
	{
		setTrialBrowserParameters();
		List<TrialProximity> trialList = new ArrayList<TrialProximity>();
		Calendar nowCalendar = Calendar.getInstance();  // set automatically to new Date().  Only used for the New category
		
		for ( TrialProximityGroup trialProximityGroup: trialBrowser.getTrials().getSortedList())
		{
			for ( TrialProximity trialProximity: trialProximityGroup )
			{
				if ( addToList( trialProximity, nowCalendar) )
				{
					if ( recordOnCurrentPage() )
					{
						trialList.add( trialProximity );
					}
					
					if ( reachedPageLimit() )
					{
						return trialList; // Got enough records for this page.  Only valid for the All category
					}
				}
			}
		}
		return trialList;
	}

	/***
	 * create the list of trials to display on this page.
	 */
	private Date getMaxLastModifiedDate()
	{
		Date maxLastModified = new Date(0L);
		for ( TrialProximityGroup trialProximityGroup: trialBrowser.getTrials().getSortedList())
		{
			for ( TrialProximity trialProximity: trialProximityGroup )
			{
				if (trialProximity.getTrial() != null && trialProximity.getTrial().getLastModified() != null && maxLastModified.before(trialProximity.getTrial().getLastModified()))
				{
					maxLastModified = trialProximity.getTrial().getLastModified();
				}
			}
		}
		return maxLastModified;
	}

	/**
	 * If the url contains specific trial id information, we need to retrieve the category of the
	 * trial and open with that page so that the internal link (following the hash mark on the url)
	 * will work.
	 * @return the category associated with this trial as a string.
	 */
	private String getTrialPageViewCategory()
	{
		Trial viewTrial = em.find( Trial.class, trialID );
		if ( viewTrial != null )
		{
            // detach the object from the persistence manager.  If we don't do this it will try and update after the class goes out of scope.
            org.hibernate.Session session = (org.hibernate.Session) em.getDelegate();
            session.evict(viewTrial);
            return viewTrial.getType().toString();
		}
		return null;
	}
	
	private boolean isSearchCategory()
	{
		return SearchCategory.equals(categoryString); // &&	((searchString1 != null && searchString1.trim().length() > 0) || (searchString2 != null && searchString2.trim().length() > 0));
	}
	
	/**
	 * @return the trialList.  Guaranteed to be non-null
	 */
	@Override
	public List<TrialProximity> getTrialList()
	{
		recordCount = 0;
		if (isSearchCategory())
		{
			return getSearchTrialList();
		}
		else
		{	
			//if this is not a search quickview, then clear the search form
			setSearchString1("");
			setSearchString2("");

			return getNonSearchTrialList();
		}	
	}

	/**
	 * @return a list of pages to display in the page navigation links.
	 */
	@Override
	public ArrayList<Integer> getPagesList()
	{
		if ( pagesList == null )
		{
 			pagesList = new ArrayList<Integer>();
 			int recordCount =  getCategoryCount( getCategoryString() );
			
			for ( int pageStart = 0; pageStart < recordCount; pageStart += pageRecordIncrement )
			{
				pagesList.add( ( pageStart / pageRecordIncrement ) + 1 );
			}
		}
		return pagesList;
	}
	
	/**
	 * @return the number of records per page to display.
	 */
	@Override
	public Integer getPageRecordIncrement() 
	{
		return pageRecordIncrement;
	}
	
	/**
	 * @return the start record for the last page.
	 */
	@Override
    public Integer getLastPage()
    {
    	return ( getCategoryCount( getCategoryString() ) / pageRecordIncrement ) * pageRecordIncrement;
    }

	/**
	 * @return the lastModified
	 */
	public Date getLastModified() 
	{
		if (lastModified == null)
		{
			lastModified = this.getMaxLastModifiedDate();
		}
		return lastModified;
	}

	/**
	 * @param searchString the searchString to set
	 */
	@Override
	public void setSearchString1(String searchString1) 
	{
		this.searchString1 = searchString1;
	}

	/**
	 * @return the searchString
	 */
	@Override
	public String getSearchString1() 
	{
		return isSearchCategory()? searchString1: "";
	}

	/**
	 * @param searchOperator the searchOperator to set
	 */
	@Override
	public void setSearchOperator(String searchOperator) 
	{
		this.searchOperator = searchOperator;
	}

	/**
	 * @return the searchOperator.  Default for an undefined operator is "OR"
	 */
	@Override
	public String getSearchOperator() 
	{
		return searchOperator != null && searchOperator.trim().length() > 0? searchOperator: "OR";
	}

	/**
	 * @param searchString the searchString to set
	 */
	@Override
	public void setSearchString2(String searchString2) 
	{
		this.searchString2 = searchString2;
	}

	/**
	 * @return the searchString
	 */
	@Override
	public String getSearchString2() 
	{
		return isSearchCategory()? searchString2: "";
	}

	@Override
	public String getFormattedSearchString()
	{
		return getBooleanSearchString().startsWith("\"")? getBooleanSearchString(): "\"" + getBooleanSearchString() + "\"";
	}
	/**
	 * Reset instance variables when stateful session bean is destroyed.
	 * Required by jboss EJB. 
	 */	
	@Remove
	@Destroy
	@Override
	public void destroy()
	{
	}
}
