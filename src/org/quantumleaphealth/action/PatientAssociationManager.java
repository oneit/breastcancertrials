/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.util.Date;

import javax.ejb.Local;

import org.quantumleaphealth.model.patient.UserPatient;

/**
 * Updates the associations of a patient.
 * 
 * @author Tom Bechtold
 * @version 2009-04-02
 */
@Local
public interface PatientAssociationManager
{
	/**
	 * Updates a patient's matching trials and removes any obsolete invitations
	 * 
	 * @param patient
	 *            the patient
	 * @param trials
	 *            the list of trials to update or <code>null</code> if not
	 *            available
	 * @param updated
	 *            the date/time of the update or <code>null</code> for now
	 * @param patientHistoryComplete
	 *            whether or not the patient's history is complete
	 * @param informed
	 *            whether or not the patient is informed of the update
	 * @throws IllegalArgumentException
	 *             if <code>patient</code> is <code>null</code>
	 */
	public void updateScreenedTrialsAndInvitations(UserPatient patient, TrialProximityGroups trials, Date updated,
			boolean patientHistoryComplete, boolean informed) throws IllegalArgumentException;

	/**
	 * Updates a patient's matching trials in uninformed mode and removes any
	 * obsolete invitations
	 * 
	 * @param patientId
	 *            the patient's id
	 * @param updated
	 *            the date/time of the update or <code>null</code> for now
	 * @throws IllegalArgumentException
	 *             if <code>patientId</code> is <code>null</code> or patient is
	 *             not found
	 */
	public void updateScreenedTrialsAndInvitations(Long patientId, Date updated) throws IllegalArgumentException;

	/**
	 * Removes a patient's obsolete invitations
	 * 
	 * @param patient
	 *            the patient
	 * @throws IllegalArgumentException
	 *             if <code>patient</code> is <code>null</code>
	 */
	public void updateInvitations(UserPatient patient) throws IllegalArgumentException;

	/**
	 * Removes a patient's screened trials from the underlying persistence
	 * 
	 * @param patientId
	 *            the patient's id
	 * @throws IllegalArgumentException
	 *             if <code>patientId</code> is <code>null</code>
	 * @return the number of screened trials removed
	 */
	public int removeScreenedTrials(Long patientId) throws IllegalArgumentException;
}
