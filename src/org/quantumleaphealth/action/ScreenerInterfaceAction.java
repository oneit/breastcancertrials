/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import org.hibernate.CacheMode;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.jboss.seam.web.Session;
import org.quantumleaphealth.model.patient.Invitation;
import org.quantumleaphealth.model.patient.PatientHistory;
import org.quantumleaphealth.model.patient.PatientDisposition;
import org.quantumleaphealth.model.patient.PatientDisposition.Disposition;
import org.quantumleaphealth.model.screener.UserScreener;
import org.quantumleaphealth.model.trial.Trial;
import org.quantumleaphealth.model.trial.TrialSite;
import org.quantumleaphealth.screen.EligibilityProfile;
import org.quantumleaphealth.screen.MatchingEngine;

/**
 * Navigates a screener through viewing and dispositioning patient histories.
 * This interface defines the following roles for a local stateful session
 * Enterprise JavaBean (EJB):
 * <ol>
 * <li>Provide facades into the patient's data. Facades are used to translate
 * the view's representation of the data into the storage format of the patient
 * object.</li>
 * <li>Provide data persistence. Methods are used to load and store data from/to
 * a persistence provider.</li>
 * <li>Sort and select the data. Methods are used to sort data and to select
 * data for the view.</li>
 * </ol>
 * The cached screener object is managed by the persistence context but its
 * invitations are not.
 * <em>Note: Do not use <code>BypassInterceptor</code> annotation as it breaks the <code>setInvitationId()</code> method</em>
 * 
 * @author Tom Bechtold
 * @version 2009-07-07
 */
@Stateful
@Scope(ScopeType.SESSION)
@Name("screenerInterface")
public class ScreenerInterfaceAction implements ScreenerInterface, Serializable
{
	/**
	 * Persists the entity beans using EJB3 persistence context
	 * 
	 * @see #login()
	 * @see #update()
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;
	/**
	 * The matching engine
	 */
	@In
	private MatchingEngine engine;
	/**
	 * The messaging engine
	 */
	@EJB
	private MessageSender messageSender;
	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log logger;

	/**
	 * The user screener, guaranteed to be non-<code>null</code>
	 * 
	 * @see #getUserScreener()
	 * @see #login()
	 * @see #isLoggedIn()
	 * @see #update()
	 */
	private UserScreener userScreener = new UserScreener();
	/**
	 * The credentials which must match the screeners in order for the
	 * screener's authentication to be persisted
	 * 
	 * @see #setCredentials(String)
	 */
	private String credentials;
	/**
	 * The screener's invitations or <code>null</code> no screener or if not
	 * loaded yet
	 * 
	 * @see #getInvitations()
	 */
	private ScreenerGroupsSiteTrialInvitations invitations;
	/**
	 * The selected invitation or <code>null</code> if none selected
	 * 
	 * @see #setInvitationId(Long)
	 * @see #getInvitation()
	 */
	private Invitation invitation;
	/**
	 * The selected invitation's disposition or <code>null</code> if none
	 * selected
	 * 
	 * @see #setInvitationId(Long)
	 * @see #getInvitation()
	 */
	private PatientDisposition patientDisposition;
	/**
	 * The selected invitation's patient history facade or <code>null</code> if
	 * no invitation selected.
	 * 
	 * @see #setInvitationId(Long)
	 * @see #getHistory()
	 */
	private FacadePatientHistory history;
	/**
	 * The selected invitation's patient profile or <code>null</code> if no
	 * invitation selected.
	 * 
	 * @see #getProfile()
	 */
	private EligibilityProfile profile;

	/**
	 * The non-localized message when authentication fails
	 */
	private static final String ERRORMESSAGE_UNAUTHENTICATED = "The email address or password you entered is not correct. Please try again";
	/**
	 * The maximum number of unsuccessful authentication attempts.
	 * 
	 * @see #login()
	 */
	private static final int MAXIMUM_UNSUCCESSFULAUTHENTICATIONCOUNT = 5;
	/**
	 * The length of the randomly-generated credentials
	 */
	private static final int LENGTH_RANDOMCREDENTIALS = 6;
	/**
	 * Random number generator
	 */
	private static final Random RANDOM = new Random();

	/**
	 * The EJB query language query to load the invitations and their associated
	 * patients. This query eagerly fetches the patients. It only loads
	 * invitations that are undispositioned. It sorts the list by trial id and
	 * site id.
	 * 
	 * @see ScreenerInterfaceAction#DISPOSITION_AGING
	 */
	private static final String QUERY_INVITATIONS = "select distinct i from Invitation i left join fetch i.userPatient "
			+ "where (i.dispositionDate is null) order by i.trialId, i.siteId";

	/**
	 * @return the screener, guaranteed to be non-<code>null</code>
	 * @see org.quantumleaphealth.action.ScreenerInterface#getUserScreener()
	 * @see org.quantumleaphealth.action.ScreenerInterface#getScreener()
	 * @see #userScreener
	 */
	public UserScreener getScreener()
	{
		return userScreener;
	}

	/**
	 * @return the screener's undispositioned or recently dispositioned
	 *         invitations or <code>null</code> if screener is <code>null</code>
	 * @see org.quantumleaphealth.action.ScreenerInterface#getInvitations()
	 * @see #invitations
	 * @see #DISPOSITIONDATE_INTERVAL
	 */
	@SuppressWarnings( { "unchecked", "cast" })
	public ScreenerGroupsSiteTrialInvitations getInvitations()
	{
		// Create and load if not cached
		if ((invitations == null) && (userScreener != null))
		{
			// If user does not belong to a group then return an empty list
			invitations = new ScreenerGroupsSiteTrialInvitations();
			if (userScreener.getScreenerGroups() != null)
			{
				// Get list of all undispositioned invitations in order of trial
				// & site ids
				// Results are readonly and not cached so will be unmanaged by
				// persistence context
				// Because invitation.trialSite is transient, we will have to
				// fetch it from the matching engine
				List<Invitation> resultList = em.createQuery(QUERY_INVITATIONS).setHint("org.hibernate.readOnly",
						Boolean.TRUE).setHint("org.hibernate.cacheMode", CacheMode.IGNORE).getResultList();
				// Connect each invitation to its trialsite and add to list if
				// user is member of its screener group
				Trial trial = null;
				TrialSite trialSite = null;
				for (Invitation invitation : resultList)
				{
					// If invitation does not have a patient then skip
					if (invitation.getUserPatient() == null)
					{
						logger.error("Invitation #0 does not have a patient", invitation.getId());
						continue;
					}
					// Get invitation's trialsite from engine; use cache if
					// available
					if ((trial == null) || !trial.getId().equals(invitation.getTrialId()))
					{
						trial = engine.getTrial(invitation.getTrialId());
						trialSite = null;
					}
					if ((trialSite == null) || !trialSite.getSite().getId().equals(invitation.getSiteId()))
					{
						trialSite = null;
						if (trial != null)
							for (TrialSite currentTrialSite : trial.getTrialSite())
								if (currentTrialSite.getSite().getId().equals(invitation.getSiteId()))
								{
									trialSite = currentTrialSite;
									break;
								}
					}
					// Attach trialSite; if not loaded in engine or no screener
					// group then skip
					if (trialSite == null)
					{
						logger.warn("Invitation #0 could not load trial #1 and site #2", invitation.getId(), invitation
								.getTrialId(), invitation.getSiteId());
						continue;
					}
					if (trialSite.getScreenerGroup() == null)
					{
						logger.error("Invitation #0 does not have a screener group", invitation.getId());
						continue;
					}
					invitation.setTrialSite(trialSite);
					// Add to list if user is in invitation's screener group
					if (userScreener.getScreenerGroups().contains(trialSite.getScreenerGroup()))
						invitations.add(invitation);
				}
				logger.debug("Loaded #1 invitations for screener #0", userScreener.getId(), invitations.getCount());
			}
		}
		return invitations;
	}

	/**
	 * Authenticates and loads a user whose principal and credentials are stored
	 * in its <code>screener</code> property or loads the screener's secret
	 * question if credentials are not provided and updates its
	 * <code>unsuccessfulAuthenticationCount</code>. This method transforms the
	 * <code>screener</code> property according to the following flow:
	 * <ol>
	 * <li>If the screener is already logged in then exit.</li>
	 * <li>Reset the cached <code>surName</code> field.</li>
	 * <li>If the screener's principal is unavailable then add a message and
	 * exit.</li>
	 * <li>Load the screener record that matches (case-insensitive) the
	 * property's principal.</li>
	 * <li>If the record is not found or there is more than one then add a
	 * message and exit.</li>
	 * <li>Cache the <code>surName</code> field so navigation knows the
	 * principal exists.</li>
	 * <li>Set the property's consecutive unsuccessful authentication attempts
	 * to that of the retrieved record. If the number exceeds a threshold then
	 * exit.</li>
	 * <li>If the credentials have been provided then compare to the retrieved
	 * credentials. If there is a match (case-sensitive) then store the
	 * retrieved record into the property and exit. If there is not a match then
	 * add a message, increment the unsuccessful-authentication-count and exit.</li>
	 * <li>If the user cannot receive an email message then exit.</li>
	 * <li>Set the property's secret question to the retrieved secret question.
	 * If neither secret question nor answer is provided then exit.</li>
	 * <li>Compare the secret answer to the retrieved secret answer. If there is
	 * not a match (case-insensitive) then add a message, increment the
	 * unsuccessful-authentication-count and exit. If there is a match then
	 * update the record's credentials with a new random string, reset the
	 * unsuccessful-authentication-count, send a message to the screener with
	 * the new credentials and logout the user. The random string consists of
	 * <code>LENGTH_RANDOMCREDENTIALS</code> upper-case Roman characters and/or
	 * digits.</li>
	 * </ol>
	 * Note that this class does not employ the JBoss seam <code>identity</code>
	 * component because it does not provide extra functionality and its Faces
	 * messages are awkward to handle.
	 * 
	 * @see org.quantumleaphealth.action.ScreenerInterface#login()
	 */
	public void login()
	{
		// Step 1: Validate logged in
		if (isLoggedIn())
			return;

		// Step 2: Reset the cached field surName; used by navigation to know if
		// principal exists in database
		userScreener.setSurName(null);
		// Step 3: Validate principal (redundant: should be checked by JSF
		// required attribute)
		String principal = (userScreener == null) ? null : userScreener.getPrincipal();
		if ((principal == null) || (principal.length() == 0))
		{
			logger.debug("Blank screener principal");
			FacesMessages.instance().addToControl("username", Severity.ERROR,
					"Please enter your email address and try again");
			return;
		}

		UserScreener retrievedScreener;
		try
		{
			// Step 4: load a screener and its associated screener groups
			retrievedScreener = (UserScreener) (em
					.createQuery(
							"select distinct us from UserScreener us left join fetch us.screenerGroups where LOWER(us.principal)=LOWER(:principal)")
					.setParameter("principal", principal).getSingleResult());
		}
		catch (NoResultException noResultException)
		{
			// Step 5a: Validate single record
			logger.debug("Screener '#0' not found", principal);
			FacesMessages
					.instance()
					.add(
							Severity.ERROR,
							(userScreener.getCredentials() == null) ? "The email address you entered was not found. Please try again"
									: ERRORMESSAGE_UNAUTHENTICATED);
			return;
		}
		catch (NonUniqueResultException nonUniqueResultException)
		{
			// Step 5b: Validate single record
			logger.error("Screener '#0' found in multiple records", nonUniqueResultException, principal);
			FacesMessages
					.instance()
					.add(Severity.ERROR,
							"Your record is not available. A message has been sent to the system administrator to fix the problem.");
			return;
		}
		// Step 6: Cache the surName so navigation knows the principal exists in
		// the database
		userScreener.setSurName(retrievedScreener.getSurName());

		// Step 7: Cache the number of consecutive unsuccessful login attempts
		// for navigation purposes and validate
		userScreener.setUnsuccessfulAuthenticationCount(retrievedScreener.getUnsuccessfulAuthenticationCount());
		if (retrievedScreener.getUnsuccessfulAuthenticationCount() >= MAXIMUM_UNSUCCESSFULAUTHENTICATIONCOUNT)
		{
			logger.debug("Screener '#0' exceeded authentication count", principal);
			FacesMessages
					.instance()
					.add(
							Severity.ERROR,
							"This account has been deactivated because of too many unsuccessful login attempts. Please contact BCT to reset your password.");
			return;
		}

		// Step 8: Compare available credentials case-insensitive
		if (userScreener.getCredentials() != null)
		{
			if ((retrievedScreener.getCredentials() == null)
					|| !userScreener.getCredentials().equalsIgnoreCase(retrievedScreener.getCredentials()))
			{
				// Failed: Update the unsuccessful count (both persisted and
				// cached)
				retrievedScreener.setUnsuccessfulAuthenticationCount(retrievedScreener
						.getUnsuccessfulAuthenticationCount() + 1);
				userScreener.setUnsuccessfulAuthenticationCount(retrievedScreener.getUnsuccessfulAuthenticationCount());
				FacesMessages.instance().add(Severity.ERROR, ERRORMESSAGE_UNAUTHENTICATED);
				logger.debug("Bad screener credentials '#1' for '#0' attempt #2", principal, userScreener
						.getCredentials(), retrievedScreener.getUnsuccessfulAuthenticationCount());
				return;
			}
			// Passed: reset the unauthenticated count and load the user
			if (retrievedScreener.getUnsuccessfulAuthenticationCount() != 0)
				retrievedScreener.setUnsuccessfulAuthenticationCount(0);
			userScreener = retrievedScreener;
			// Fetch invitations that belong to this screener
			refreshInvitations();
			// Flush the entity manager so SQL/debug logging is in chronological
			// order
			em.flush();
			logger.debug("Authenticated and loaded research site '#0' as ##1", userScreener.getPrincipal(),
					userScreener.getId());
			return;
		}

		// Step 9: If we cannot read the secret question/answer nor send an
		// email then we cannot authenticate using the secret question/answer
		if ((retrievedScreener.getSecretQuestion() == null) || (retrievedScreener.getSecretAnswer() == null))
		{
			// Clear the secret question so navigation knows that it cannot send
			// an email
			userScreener.setSecretQuestion(null);
			return;
		}
		InternetAddress internetAddress = null;
		try
		{
			internetAddress = new InternetAddress(retrievedScreener.getPrincipal(), true);
		}
		catch (AddressException addressException)
		{
			logger.debug("Screener '#0' cannot receive email with reset password: #1",
					retrievedScreener.getPrincipal(), addressException);
			// Reset any cached secret question so navigation knows that it
			// cannot send an email
			userScreener.setSecretQuestion(null);
			return;
		}

		// Step 10: Cache the secret question and display it to the user if
		// answer has not been provided
		userScreener.setSecretQuestion(retrievedScreener.getSecretQuestion());
		if (userScreener.getSecretAnswer() == null)
			return;

		// Step 11: Authenticate the secret answer and store the result in the
		// unsuccessful count
		if (!userScreener.getSecretAnswer().equalsIgnoreCase(retrievedScreener.getSecretAnswer()))
		{
			// Failed: Update the unsuccessful count (both persisted and cached)
			retrievedScreener
					.setUnsuccessfulAuthenticationCount(retrievedScreener.getUnsuccessfulAuthenticationCount() + 1);
			userScreener.setUnsuccessfulAuthenticationCount(retrievedScreener.getUnsuccessfulAuthenticationCount());
			FacesMessages.instance().add(Severity.ERROR,
					"The secret answer you entered is not correct. Please try again");
			logger.debug("Bad screener secret answer '#1' for '#0' attempt #2", principal, userScreener
					.getSecretAnswer(), retrievedScreener.getUnsuccessfulAuthenticationCount());
			return;
		}

		// Passed: reset and send credentials, reset unsuccessful authentication
		// count, logout session
		char[] random_characters = new char[LENGTH_RANDOMCREDENTIALS];
		// The new credentials uses upper-case Roman characters and digits for
		// readability
		for (int index = 0; index < LENGTH_RANDOMCREDENTIALS; index++)
			random_characters[index] = Character.toUpperCase(Character.forDigit(RANDOM.nextInt(Character.MAX_RADIX),
					Character.MAX_RADIX));
		String newCredentials = new String(random_characters);
		try
		{
			// Update credentials and persisted count
			retrievedScreener.setCredentials(newCredentials);
			if (retrievedScreener.getUnsuccessfulAuthenticationCount() != 0)
				retrievedScreener.setUnsuccessfulAuthenticationCount(0);
			messageSender.send(internetAddress, null, "Your BCT password has been reset",
					"Your password on BreastCancerTrials.org has been changed to: " + newCredentials);
			// Update cached count so navigation knows that password reset and
			// sending was successful
			userScreener.setUnsuccessfulAuthenticationCount(0);
			logger.debug("Reset screener credentials and sent message to '#0'", principal);
		}
		catch (Throwable throwable)
		{
			// If either storage or sending did not work then then ensure count
			// represents failure and notify user
			if (userScreener.getUnsuccessfulAuthenticationCount() <= 0)
				userScreener.setUnsuccessfulAuthenticationCount(1);
			logger.error("Cannot reset screener ##0's credentials", throwable, retrievedScreener.getId());
			FacesMessages.instance().add(Severity.ERROR,
					"The system could not reset your password. Please contact BCT.");
		}
		// Logout session
		logout();
	}

	/**
	 * @return whether or not the screener is logged in
	 * @see org.quantumleaphealth.action.ScreenerInterface#isLoggedIn()
	 * @see #userScreener
	 * @see UserScreener#getId()
	 */
	public boolean isLoggedIn()
	{
		// If its id is set then user is logged in
		return (userScreener != null) && (userScreener.getId() != null);
	}

	/**
	 * Validates that the termsAccepted checkbox was checked. This method adds a
	 * non-localized faces messages if the component was not checked.
	 * 
	 * @param facesContext
	 *            the faces context
	 * @param component
	 *            the checkbox component
	 * @param value
	 *            the components value
	 * @see org.quantumleaphealth.action.ScreenerInterface#validateTermsAccepted(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object)
	 */
	public void validateTermsAccepted(FacesContext facesContext, UIComponent component, Object value)
	{
		logger.debug("Validating termsAccepted '#0'", value);
		if (!Boolean.TRUE.equals(value))
		{
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"Please check the checkbox before clicking the I Accept button");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
		}
	}

	/**
	 * Refresh the list of invitations
	 * 
	 * @see #getInvitations()
	 * @see org.quantumleaphealth.action.ScreenerInterface#refreshInvitations()
	 */
	public void refreshInvitations()
	{
		invitations = null;
		invitation = null;
		patientDisposition = null;
		history = null;
		profile = null;
	}

	/**
	 * Log out the screener and invalidate the <code>HttpSession</code>.
	 * 
	 * @see org.quantumleaphealth.action.ScreenerInterface#logout()
	 */
	public void logout()
	{
		Session.instance().invalidate();
		logger.debug("Invalidating session for screener #0", userScreener.getId());
	}

	/**
	 * @return the credentials which must match the screener's in order for the
	 *         screener's new authentication to be persisted
	 * @see org.quantumleaphealth.action.ScreenerInterface#getCredentials()
	 */
	public String getCredentials()
	{
		return credentials;
	}

	/**
	 * @param credentials
	 *            the credentials which must match the screener's in order for
	 *            the screener's new authentication to be persisted
	 * @see org.quantumleaphealth.action.ScreenerInterface#setCredentials(java.lang.String)
	 * @see #credentials
	 */
	public void setCredentials(String credentials)
	{
		this.credentials = credentials;
	}

	/**
	 * Validates the credentials are entered and are equal to the other screener
	 * credentials. This method invalidates the component and adds a
	 * non-localized faces messages if the submitted value is not available or
	 * if the submitted value is not equal (case-sensitive) to the other
	 * screener credentials component's submitted value.
	 * 
	 * @param facesContext
	 *            the faces context
	 * @param component
	 *            the input component
	 * @param value
	 *            the component's value
	 * @see org.quantumleaphealth.action.ScreenerInterface#validateCredentials(javax.faces.context.FacesContext,
	 *      javax.faces.component.UIComponent, java.lang.Object)
	 */
	public void validateCredentials(FacesContext facesContext, UIComponent component, Object value)
	{
		// Ensure not empty; not required for JavaServerFaces 1.2 as empty
		// values are not validated; use required=false parameter
		String postedCredentials = (value instanceof String) ? (String) (value) : null;
		if ((postedCredentials == null) || (postedCredentials.trim().length() == 0))
		{
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"Please enter a password");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
			return;
		}
		postedCredentials = postedCredentials.trim();

		// If not equal (case-sensitive) then invalidate; must use value or
		// local value, not submitted value
		UIComponent passwordComponent = component.findComponent("password");
		// If password component is not available then give up
		if (!(passwordComponent instanceof UIInput))
			return;
		if (!postedCredentials.equals(((UIInput) (passwordComponent)).getValue()))
		{
			logger.debug("Invalid duplicate credentials for ##0", userScreener.getId());
			FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR,
					"The second password entered is not the same as the first. Please try again");
			if (component instanceof UIInput)
				((UIInput) (component)).setValid(false);
			return;
		}
	}

	/**
	 * Update the screener's record
	 * 
	 * @see org.quantumleaphealth.action.ScreenerInterface#update()
	 */
	public void update()
	{
		// Do nothing if not logged in
		if (!isLoggedIn())
			return;
		em.merge(userScreener);
		// Do not refresh the invitation list manually; if desired, either call
		// refreshInvitations() or setup JPA associations' refresh attribute
	}

	/**
	 * @param invitationId
	 *            the invitation's id
	 * @see #invitation
	 * @see org.quantumleaphealth.action.ScreenerInterface#setInvitationId(java.lang.Long)
	 */
	@RequestParameter
	public void setInvitationId(Long invitationId)
	{
		// Ignore null request parameters
		if (invitationId == null)
			return;
		// If the invitation is not stored already then look it up
		if ((invitation != null) && invitation.getId().equals(invitationId))
			return;
		// If requested id is set to non-positive number then clear invitation
		if (invitationId.longValue() <= 0)
		{
			invitation = null;
			patientDisposition = null;
			history = null;
			profile = null;
			logger.debug("Resetting current invitation for #0", userScreener.getId());
			return;
		}
		logger.debug("Looking up invitation #0 for #1", invitationId, userScreener.getId());
		ScreenerGroupsSiteTrialInvitations invitations = getInvitations();
		invitation = (invitations == null) ? null : invitations.getInvitation(invitationId);
		patientDisposition = new PatientDisposition();
		patientDisposition.setTrialId((invitation == null) ? null : invitation.getTrialId());
		patientDisposition.setSiteId((invitation == null) ? null : invitation.getSiteId());
		patientDisposition.setDispositionDate((invitation == null) ? null : invitation.getDispositionDate());
		history = null;
		profile = null;
		if (invitation != null)
		{
			logger.debug("Set invitation #0 for #1", invitation.getId(), userScreener.getId());
		}
	}

	/**
	 * @return the invitation
	 * @see #invitation
	 * @see org.quantumleaphealth.action.InvitationManager#getInvitation()
	 */
	public Invitation getInvitation()
	{
		return invitation;
	}

	/**
	 * Returns the invitation's patient history facade and sets the invitation's
	 * <code>viewed</code> field if not set yet.
	 * 
	 * @return the patient history facade or <code>null</code> if no selected
	 *         invitation
	 * @see org.quantumleaphealth.action.ScreenerInterface#getHistory()
	 */
	public FacadePatientHistory getHistory()
	{
		// If no invitation, not available or already cached then return
		if ((invitation == null) || (invitation.getUserPatient() == null)
				|| (invitation.getUserPatient().getPatientHistory() == null))
			return null;
		if (history != null)
			return history;
		// First create a "fresh" facade then set the non-fresh patient's
		// history to it
		history = new FacadePatientHistory(new PatientHistory());
		history.setPatientHistory(invitation.getUserPatient().getPatientHistory(), false);
		profile = null;
		// If not already viewed then update status
		if (invitation.getViewed() != null)
			return history;
		invitation.setViewed(new Date());
		int rowResults = em.createQuery("UPDATE Invitation SET viewed=:viewed WHERE id=:id").setParameter("id",
				invitation.getId()).setParameter("viewed", invitation.getViewed(), TemporalType.TIMESTAMP)
				.executeUpdate();
		if (rowResults == 1)
			logger.debug("Stored invitation #0 view status as #1", invitation.getId(), invitation.getViewed());
		else
			logger.error("Could not update invitation #0 view status", invitation.getId());
		return history;
	}

	/**
	 * @return the invitation's patient eligibility profile or <code>null</code>
	 *         if no selected invitation
	 * @see org.quantumleaphealth.action.ScreenerInterface#getProfile()
	 */
	public EligibilityProfile getProfile()
	{
		// If no invitation, not available or already cached then return
		if ((invitation == null) || (invitation.getUserPatient() == null)
				|| (invitation.getUserPatient().getPatientHistory() == null))
			return null;
		if (profile != null)
			return profile;
		// Cache the result using the patient's name as the description
		profile = engine.getEligibilityProfile(invitation.getTrialId(),
				invitation.getUserPatient().getPatientHistory(), invitation.getPatientName());
		if (profile == null)
			logger.error("Could not retrieve profile for invitation #0", invitation.getId());
		else
			logger.debug("Cached profile with #1 criteria for invitation #0", invitation.getId(), profile.getCriteria()
					.size());
		return profile;
	}

	/**
	 * @return the selected disposition or <code>null</code> if no invitation is
	 *         selected
	 * @see org.quantumleaphealth.action.ScreenerInterface#getPatientDisposition()
	 */
	@Override
	public PatientDisposition getPatientDisposition()
	{
		return patientDisposition;
	}

	/**
	 * @return the one-based ordinal value of the <code>disposition</code> field
	 *         in bit-wise format or <code>1</code> if no disposition
	 * @see org.quantumleaphealth.action.InvitationManager#getDispositionBits()
	 */
	public long getDispositionBits()
	{
		// Bit #0 is reserved for un-dispositioned, thus ordinal #n == bit#(n +
		// 1)
		return (patientDisposition == null) || (patientDisposition.getDisposition() == null) ? 1l
				: (2l << patientDisposition.getDisposition().ordinal());
	}

	/**
	 * Updates the disposition. If the disposition is changing then its date is
	 * set to now.
	 * 
	 * @param dispositionBits
	 *            the one-based ordinal value of the invitation's
	 *            <code>disposition</code> field in bit-wise format or
	 *            <code>1</code> for no disposition
	 * @see org.quantumleaphealth.action.ScreenerInterface#setDisposition(long)
	 */
	public void setDispositionBits(long dispositionBits)
	{
		// Do nothing if invitation is not available or is unchanged
		if (patientDisposition == null)
		{
			logger.debug("No disposition to update to #0 for screener #1", dispositionBits, userScreener.getId());
			return;
		}
		if (dispositionBits == getDispositionBits())
			return;
		// Bit #0 is reserved for un-dispositioned, thus ordinal #n == bit#(n +
		// 1)
		int ordinal = (dispositionBits > 1) ? (Long.numberOfTrailingZeros(dispositionBits) - 1) : -1;
		if (ordinal >= Disposition.values().length)
		{
			logger.warn("Illegal invitation disposition selection #0 for invitation #1 by screener #2",
					dispositionBits, invitation.getId(), userScreener.getId());
			return;
		}
		patientDisposition.setDisposition(ordinal < 0 ? null : Disposition.values()[ordinal]);
		logger.debug("Disposition #0 stored for trial ##1 and site ##2 for screener #3", patientDisposition
				.getDisposition(), patientDisposition.getTrialId(), patientDisposition.getSiteId(), userScreener
				.getId());
		// Date is only set if not in process
		invitation.setDispositionDate((ordinal < 0) ? null : new Date());
		patientDisposition.setDispositionDate(invitation.getDispositionDate());
	}

	/**
	 * Update the current invitation's disposition. Since the cached invitation
	 * object is not managed by the persistence context (its query was
	 * read-only), we manually update the persisted record.
	 * 
	 * @see org.quantumleaphealth.action.ScreenerInterface#updateDisposition()
	 */
	public void updateDisposition()
	{
		// If no invitation then do nothing
		if (invitation == null)
		{
			logger.debug("No invitation to update disposition for screener #0", userScreener.getId());
			return;
		}
		// If no disposition to persist then do nothing
		if (patientDisposition == null)
			logger.debug("No disposition #0 to update for screener #1", invitation.getId(), userScreener.getId());
		else if (patientDisposition.getId() != null)
			logger.debug("Disposition #0 already persisted for screener #1", patientDisposition.getId(), userScreener
					.getId());
		else
			em.persist(patientDisposition);
		int rowResults = em.createQuery("UPDATE Invitation SET dispositionDate=:dispositionDate WHERE id=:id")
				.setParameter("dispositionDate", invitation.getDispositionDate(), TemporalType.TIMESTAMP).setParameter(
						"id", invitation.getId()).executeUpdate();
		if (rowResults == 1)
		{
			logger.debug("Updated invitation #0 date to #1", invitation.getId(), invitation.getDispositionDate());
			refreshInvitations();
		}
		else
			logger.error("Could not update invitation #0 disposition date", invitation.getId());
	}

	/**
	 * Reset instance variables when stateful session bean is destroyed.
	 */
	@Remove
	@Destroy
	public void destroy()
	{
		userScreener = new UserScreener();
		credentials = null;
		refreshInvitations();
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = 5971892762348502945L;
}
