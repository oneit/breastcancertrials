package org.quantumleaphealth.action;

import javax.ejb.Local;

/**
 * Interface for the creation of avatars on the su/menu.seam page for administrators.
 * 
 * @author wgweis
 */
@Local
public interface AvatarCreateAvatarInterface
{
	/**
	 * Main method to save avatars.
	 */
	public void saveAvatar(String utype);
	
	/**
	 * setter for the avatar title.
	 * @param newAvatarTitle
	 */
	public void setNewAvatarTitle(String newAvatarTitle);
	
	/**
	 * @return The avatar title field
	 */
	public String getNewAvatarTitle();
	
	/**
	 * set the avatar description field.
	 * @param newAvatarDescription
	 */
	public void setNewAvatarDescription(String newAvatarDescription);
	
	/**
	 * @return the avatar description field.
	 */
	public String getNewAvatarDescription();
	
	/**
	 * set the avatar email field.  Actually the id for this new avatar.
	 * @param newAvatarEmail
	 */
	public void setNewAvatarEmail(String newAvatarEmail);

	/**
	 * @return the avatar email field.
	 */
	public String getNewAvatarEmail();
	
	/**
	 * required by jboss EJB3.
	 */
	public void destroy();
}
