package org.quantumleaphealth.action;

import javax.ejb.Remove;
import javax.ejb.Stateful;

import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.contexts.Context;
import org.jboss.seam.contexts.Contexts;
import org.quantumleaphealth.action.navigator.NavigatorInterface;

/**
 * 9/12/2013
 * 
 * The purpose of this class is to proxy requests to the PatientInterfaceAction and NavigatorInterfaceAction
 * session level stateful beans for the current login state.  Public pages such as browse_trials were querying
 * these beans directly.  This was causing their creation on pages that had a conversation level of Request only.
 * As each such bean has a session time out set high (currently two hours), a backup of such beans in the session
 * context was exhausting virtual memory.  The symptoms were very much like those of a memory leak.
 * 
 * @author wgweis
 *
 */
@Stateful // Default conversation level of "Request"
@Name("userProxy")
public class UserProxyAction implements UserProxyInterface 
{
	private Context context = Contexts.getSessionContext();
	
	/**
	 * Use this EL expression:  "userProxy.userPatientLoggedIn" instead of "patientInterface.loggedIn" to
	 * avoid creating a UserPatientInterfaceAction object on a request only page.
	 */
	@Override
	public boolean isUserPatientLoggedIn() 
	{
		PatientInterface userPatient = (PatientInterface) context.get("patientInterface");
		return userPatient == null? false: userPatient.isLoggedIn();
	}

	/**
	 * Use this EL expression:  "userProxy.navigatorLoggedIn" instead of "navigatorInterface.loggedIn" to
	 * avoid creating a NavigatorInterfaceAction object on a request only page.
	 */
	@Override
	public boolean isNavigatorLoggedIn() 
	{
		NavigatorInterface navigator = (NavigatorInterface) context.get("navigatorInterface");
		return navigator == null? false: navigator.isLoggedIn();
	}

	/**
	 * Reset instance variables when stateful session bean is destroyed.
	 * Required by jboss EJB. 
	 */	
	@Remove
	@Destroy
	@Override
	public void destroy() 
	{
	}
}
