/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes;
import org.quantumleaphealth.ontology.CharacteristicCode;
import org.quantumleaphealth.ontology.OntologyUtils;
import org.quantumleaphealth.ontology.ValueCharacteristicHolder;

/**
 * Represents a facade into a map of value characteristics. This serializable
 * JavaBean exposes the <code>value</code> bit-wise field in <code>long</code>
 * representation to the GUI:
 * <ul>
 * <li>Bit #0 represents whether or not the characteristic's value is
 * <tt>unsure</tt>.</li>
 * <li>Bits 1 and above represent the position of the characteristic's value in
 * the <code>valueSet</code>.</li>
 * </ul>
 * This class caches the bit-wise value in order to reduce effort querying the
 * holder.
 * 
 * @author Tom Bechtold
 * @version 2008-05-23
 */
public class FacadeValueCharacteristic extends FacadeAbstractCharacteristic
{
	/**
	 * Holds the value characteristics, guaranteed to be non-<code>null</code>
	 */
	private ValueCharacteristicHolder valueCharacteristicHolder;
	/**
	 * Caches the bit-wise representation of the one-based positions within
	 * <code>valueSet</code> combined with the <tt>unsure</tt> status in bit #0
	 * or <code>0</code> if nothing is set
	 */
	private long cachedValue;
	/**
	 * The characteristic value to use to represent the <tt>unsure</tt> state.
	 */
	private static final CharacteristicCode UNSURE = BreastCancerCharacteristicCodes.UNSURE;

	/**
	 * Validates and stores the parameter and sets the cached value
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to represent
	 * @param valueSet
	 *            the codes of the characteristics that this characteristic may
	 *            be set to
	 * @param valueCharacteristicHolder
	 *            holds the value characteristics
	 * @throws IllegalArgumentException
	 *             if any parameter is <code>null</code> or
	 *             <code>valueSet</code> is larger than <code>Long.SIZE</code>
	 * @see Long#SIZE
	 */
	public FacadeValueCharacteristic(CharacteristicCode characteristicCode, CharacteristicCode[] valueSet,
			ValueCharacteristicHolder valueCharacteristicHolder) throws IllegalArgumentException
	{
		super(characteristicCode, valueSet);
		setValueCharacteristicHolder(valueCharacteristicHolder);
	}

	/**
	 * Validates and stores the parameter and updates the cached value
	 * 
	 * @param valueCharacteristicHolder
	 *            holds the value characteristics
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	void setValueCharacteristicHolder(ValueCharacteristicHolder valueCharacteristicHolder)
			throws IllegalArgumentException
	{
		if (valueCharacteristicHolder == null)
			throw new IllegalArgumentException("holder not specified");
		this.valueCharacteristicHolder = valueCharacteristicHolder;
		// Unsure is represented in bit #0
		CharacteristicCode value = valueCharacteristicHolder.getValue(characteristicCode);
		cachedValue = UNSURE.equals(value) ? 1l : OntologyUtils.getLongValue(valueSet, value) << 1;
	}

	/**
	 * Returns the bit-wise representation of the one-based positions within
	 * <code>valueSet</code> combined with the <tt>unsure</tt> status in bit #0.
	 * This method simply returns the cached value.
	 * 
	 * @return the bit-wise representation of the one-based positions within
	 *         <code>valueSet</code> combined with the <tt>unsure</tt> status in
	 *         bit #0 or <code>0l</code> if nothing is set
	 */
	public long getValue()
	{
		return cachedValue;
	}

	/**
	 * Sets the characteristic's value into the holder. This method does nothing
	 * if the cached value has not changed. The zeroth bit represents the
	 * <tt>unsure</tt> value.
	 * 
	 * @param value
	 *            the bit-wise representation of the one-based position within
	 *            <code>valueSet</code> of the value combined with the
	 *            <tt>unsure</tt> status in bit #0 or <code>0l</code> to remove
	 *            the value
	 * @throws IllegalArgumentException
	 *             if the one-based position is higher than the size of
	 *             <code>valueSet</code>
	 */
	public void setValue(long value) throws IllegalArgumentException
	{
		// Only update if cached value differs
		if (cachedValue == value)
			return;
		if (value <= 0)
			valueCharacteristicHolder.setValue(characteristicCode, null);
		else if (value == 1l)
			valueCharacteristicHolder.setValue(characteristicCode, UNSURE);
		else
		{
			int position = Long.numberOfTrailingZeros(value >>> 1);
			if (position >= valueSet.length)
			{	
				throw new IllegalArgumentException("bit #" + position + " does not fit in set of length "
						+ valueSet.length);
			}
			valueCharacteristicHolder.setValue(characteristicCode, valueSet[position]);
		}
		cachedValue = value;
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = 1089664725143840518L;
}
