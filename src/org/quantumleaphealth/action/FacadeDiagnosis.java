/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;
import java.util.Map;
import java.util.LinkedHashMap;
import java.util.Iterator;
import java.util.Collection;

import org.quantumleaphealth.model.patient.Diagnosis;
import org.quantumleaphealth.model.patient.ValueHistory;

import static org.quantumleaphealth.ontology.BreastCancerCharacteristicCodes.*;

import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Represents a facade into a patient's breast cancer diagnosis history. This
 * serializable JavaBean exposes the <code>diagnosed</code> field to the GUI to
 * keeps the list of diagnoses up to date, and creates facades to represent the
 * diagnosis' properties.
 * 
 * @author Tom Bechtold
 * @version 2008-06-30
 */
public class FacadeDiagnosis implements Serializable
{
	/**
	 * The location that this facade represents
	 */
	private final CharacteristicCode location;
	/**
	 * Represents the diagnosis' first diagnosis characteristic
	 */
	private final FacadeBinaryCharacteristic firstDiagnosis;
	/**
	 * Represents the diagnosis' lymph node count characteristic
	 */
	private final FacadeShortCharacteristic lymphNodeCount;
	/**
	 * Represents the diagnosis' value characteristics
	 */
	private final FacadeValueCharacteristic[] valueCharacteristics;
	/**
	 * The elements that each value characteristic is allowed to be set to.
	 * These elements must match the array indices and labels in the view. Note:
	 * the <tt>unsure</tt> element is automatically included in the facade.
	 */
	private static final LinkedHashMap<CharacteristicCode, CharacteristicCode[]> VALUE = new LinkedHashMap<CharacteristicCode, CharacteristicCode[]>(
			12, 1.0f);
	static
	{
		VALUE.put(ESTROGENRECEPTOR, new CharacteristicCode[] { ESTROGENRECEPTOR_POSITIVE, ESTROGENRECEPTOR_NEGATIVE,
				ESTROGENRECEPTOR_INDETERMINATE, ESTROGENRECEPTOR_NOTTESTED });
		VALUE.put(PROGESTERONERECEPTOR, new CharacteristicCode[] { PROGESTERONERECEPTOR_POSITIVE,
				PROGESTERONERECEPTOR_NEGATIVE, PROGESTERONERECEPTOR_INDETERMINATE, PROGESTERONERECEPTOR_NOTTESTED });
		VALUE.put(HER2NEUDIAGNOSIS, new CharacteristicCode[] { HER2NEUDIAGNOSIS_POSITIVE, HER2NEUDIAGNOSIS_NEGATIVE,
				HER2NEUDIAGNOSIS_INDETERMINATE, HER2NEUDIAGNOSIS_NOTTESTED });
		VALUE.put(STAGE, new CharacteristicCode[] { STAGE_0, STAGE_I, STAGE_II, STAGE_III, STAGE_IIIA, STAGE_IIIB, STAGE_IIIC, STAGE_IV});
		VALUE.put(INFLAMMATORY, new CharacteristicCode[] { NEGATIVE, AFFIRMATIVE });
		VALUE.put(TUMOR_STAGE_FINDING, new CharacteristicCode[] { TUMOR_STAGE_FINDING_T1, TUMOR_STAGE_FINDING_T2,
				TUMOR_STAGE_FINDING_T3 });
		VALUE.put(LYMPH_NODE_COUNT, new CharacteristicCode[] { NEGATIVE, AFFIRMATIVE });
		VALUE.put(BREASTMETASTASIS_BRAIN, new CharacteristicCode[] { UNTREATED, PROGRESSING, STABLE });
		VALUE.put(LYMPHEDEMA, new CharacteristicCode[] { NEGATIVE, AFFIRMATIVE });
		VALUE.put(PDL1, new CharacteristicCode[] { PDL1_POSITIVE, PDL1_NEGATIVE });
		VALUE.put(ANDROGENRECEPTOR, new CharacteristicCode[] { ANDROGENRECEPTOR_POSITIVE, ANDROGENRECEPTOR_NEGATIVE });
		VALUE.put(PI3K, new CharacteristicCode[] { PI3K_POSITIVE, PI3K_NEGATIVE });
	}
	
	/**
	 * A static Getter for STAGE for use by the AvatarHistoryConfiguration class
	 */
	public static CharacteristicCode[] getStageValues()
	{
		return VALUE.get(STAGE);
	}
	
	/**
	 * Represents the diagnosis' attribute characteristics
	 */
	private final FacadeAttributeCharacteristic[] attributeCharacteristics;
	/**
	 * The elements that each attribute characteristic is allowed to be set to.
	 * These elements must match the array indices and labels in the view. Note:
	 * the <tt>unsure</tt> element is not supported in this facade.
	 */
	private static final LinkedHashMap<CharacteristicCode, CharacteristicCode[]> ATTRIBUTE = new LinkedHashMap<CharacteristicCode, CharacteristicCode[]>(
			2, 1.0f);
	static
	{
		ATTRIBUTE.put(DIAGNOSISBREAST, new CharacteristicCode[] { DUCTAL_INSITU, DUCTAL_INVASIVE, LOBULAR_INVASIVE });
		ATTRIBUTE.put(DIAGNOSISSPREAD, new CharacteristicCode[] { SUPERCLAVICULAR, INFRACLAVICULAR, CHESTWALL,
				DIAGNOSISAREA_BRAIN, DIAGNOSISAREA_SPINALCORD, DIAGNOSISAREA_BONE, DIAGNOSISAREA_LIVER,
				DIAGNOSISAREA_LUNG, DIAGNOSISAREA_LYMPHNODE, DIAGNOSISAREA_SKIN, DIAGNOSISAREA_OVARIES, DIAGNOSISAREA_BREAST, DIAGNOSISAREA_OTHER_LOCATION });
	}

	/**
	 * The underlying property that this facade represents
	 */
	private Diagnosis backing;
	/**
	 * Holds all diagnoses. This list will be updated with the
	 * <code>backing</code> whenever <code>diagnosed</code> changes status
	 */
	private Collection<Diagnosis> list;
	/**
	 * Whether or not the diagnosis was made in bit-wise format. 0 = not
	 * answered yet, 2 = No, 4 = Yes
	 */
	private long diagnosed;

	/**
	 * The characteristic that stores the location of the diagnosis that this
	 * facade represents
	 */
	private static final CharacteristicCode LOCATION = ANATOMIC_SITE;

	/**
	 * Instantiate a facade with the specified underlying property.
	 * 
	 * @param list
	 *            holds all diagnoses
	 * @param location
	 *            where the diagnosis was performed
	 * @throws IllegalArgumentException
	 *             if any parameter is <code>null</code>
	 */
	FacadeDiagnosis(Collection<Diagnosis> list, CharacteristicCode location) throws IllegalArgumentException
	{
		// Verify and store parameters
		if (list == null)
			throw new IllegalArgumentException("list parameter not specified");
		if (location == null)
			throw new IllegalArgumentException("location parameter not specified");
		this.list = list;
		this.location = location;
		// Find the backing property in the list that matches the location
		backing = null;
		for (Diagnosis current : list)
			if (location.equals(current.getValueHistory().getValue(LOCATION)))
			{
				backing = current;
				break;
			}
		if (backing == null)
		{
			// If not exists in the list then create a blank one whose diagnosis
			// has never been answered
			backing = new Diagnosis();
			backing.getValueHistory().setValue(LOCATION, location);
			diagnosed = 0l;
		}
		else
			diagnosed = 4l;
		// Create the facades to represent each diagnosis characteristic
		firstDiagnosis = new FacadeBinaryCharacteristic(SINGULAR, backing.getBinaryHistory());
		lymphNodeCount = new FacadeShortCharacteristic(LYMPH_NODE_COUNT, backing.getShortHistory());
		valueCharacteristics = new FacadeValueCharacteristic[VALUE.size()];
		Iterator<Map.Entry<CharacteristicCode, CharacteristicCode[]>> entryIterator = VALUE.entrySet().iterator();
		int entryIndex = 0;
		ValueHistory valueHistory = backing.getValueHistory();
		while (entryIterator.hasNext())
		{
			Map.Entry<CharacteristicCode, CharacteristicCode[]> entry = entryIterator.next();
			valueCharacteristics[entryIndex] = new FacadeValueCharacteristic(entry.getKey(), entry.getValue(),
					valueHistory);
			entryIndex++;
		}
		attributeCharacteristics = new FacadeAttributeCharacteristic[ATTRIBUTE.size()];
		entryIterator = ATTRIBUTE.entrySet().iterator();
		entryIndex = 0;
		while (entryIterator.hasNext())
		{
			Map.Entry<CharacteristicCode, CharacteristicCode[]> entry = entryIterator.next();
			attributeCharacteristics[entryIndex] = new FacadeAttributeCharacteristic(entry.getKey(), entry.getValue(),
					backing);
			entryIndex++;
		}
	}

	/**
	 * Update a facade's backing property with one in the specified list.
	 * 
	 * @param list
	 *            holds all diagnoses
	 * @throws IllegalArgumentException
	 *             if any parameter is <code>null</code>
	 */
	void setHistory(Collection<Diagnosis> list) throws IllegalArgumentException
	{
		// Verify and store parameters
		if (list == null)
			throw new IllegalArgumentException("list parameter not specified");
		if (location == null)
			throw new IllegalArgumentException("location parameter not specified");
		this.list = list;
		// Find the backing property in the list that matches the location
		backing = null;
		for (Diagnosis current : list)
		{	
			if (location.equals(current.getValueHistory().getValue(LOCATION)))
			{
				backing = current;
				break;
			}
		}	
		if (backing == null)
		{
			// If not exists in the list then create a blank, undiagnosed one
			backing = new Diagnosis();
			backing.getValueHistory().setValue(LOCATION, location);
			diagnosed = 2l;
		}
		else
			diagnosed = 4l;
		// Update the facades with the backing property
		firstDiagnosis.setBinaryCharacteristicHolder(backing.getBinaryHistory(), false);
		lymphNodeCount.setShortCharacteristicHolder(backing.getShortHistory());
		ValueHistory valueHistory = backing.getValueHistory();
		for (FacadeValueCharacteristic valueCharacteristic : valueCharacteristics)
		{	
			valueCharacteristic.setValueCharacteristicHolder(valueHistory);
		}	
		for (FacadeAttributeCharacteristic attributeCharacteristic : attributeCharacteristics)
		{	
			attributeCharacteristic.setAttributeCharacteristicHolder(backing);
		}	
	}

	/**
	 * Whether or not the diagnosis was made in bit-wise format. 0 = not
	 * answered yet, 2 = No, 4 = Yes.
	 * 
	 * @return whether or not the diagnosis was made in bit-wise format
	 */
	public long getDiagnosed()
	{
		return diagnosed;
	}

	/**
	 * Sets the diagnosed status and updates the underlying list. 0 = not
	 * answered yet, 2 = No, 4 = Yes. If <code>diagnosed</code> is
	 * <code>Yes</code> then add the backing property to the list; otherwise
	 * remove the backing property from the list.
	 * 
	 * @param diagnosed
	 *            whether or not the diagnosis was made in bit-wise format
	 */
	public void setDiagnosed(long diagnosed)
	{
		// Do nothing if the status has not changed
		if (this.diagnosed == diagnosed)
			return;
		this.diagnosed = diagnosed;
		if (diagnosed > 2l)
			list.add(backing);
		else
			list.remove(backing);
	}

	/**
	 * Sets the diagnosed status and updates the underlying list. If
	 * <code>diagnosed</code> is <code>Yes</code> then add the backing property
	 * to the list; otherwise remove the backing property from the list.
	 * 
	 * @param diagnosed
	 *            whether or not the diagnosis was made
	 */
	public void setDiagnosed(boolean diagnosed)
	{
		// Call overloaded method
		setDiagnosed(diagnosed ? 4l : 2l);
	}

	/**
	 * @return the facade to the first diagnosis characteristic
	 */
	public FacadeBinaryCharacteristic getFirstDiagnosis()
	{
		return firstDiagnosis;
	}

	/**
	 * @return the facade to the lymph node count characteristic
	 */
	public FacadeShortCharacteristic getLymphNodeCount()
	{
		return lymphNodeCount;
	}

	/**
	 * @return the value characteristics
	 */
	public FacadeValueCharacteristic[] getValueCharacteristics()
	{
		return valueCharacteristics;
	}

	/**
	 * @return the attribute characteristics
	 */
	public FacadeAttributeCharacteristic[] getAttributeCharacteristics()
	{
		return attributeCharacteristics;
	}

	/**
	 * Returns the values of the fields
	 * 
	 * @return the values of the fields
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder("FacadeDiagnosis={");
		builder.append("location=").append(backing.getValueHistory().getValue(LOCATION));
		if (diagnosed < 4l)
			return builder.append('}').toString();
		builder.append(",backing={").append(backing).append("}}");
		return builder.toString();
	}

	/**
	 * Version identifier for serializable class
	 */
	private static final long serialVersionUID = -7236798983593887094L;
}
