/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;

/**
 * A string facade for a <code>Short</code> value. This serializable JavaBean
 * exposes the <code>shortValue</code> string field to the GUI and keeps track
 * whether it is a valid and/or empty number.
 * 
 * @author Tom Bechtold
 * @version 2008-08-06
 */
public abstract class FacadeShort implements Serializable
{
	/**
	 * String representation of the characteristic's value or <code>null</code>
	 * if the characteristic's value is <code>null</code> or an invalid string
	 * if <code>valid</code> is <code>false</code>.
	 */
	private String stringValue = null;
	/**
	 * Whether or not the <code>stringValue</code> represents a valid number or
	 * is <code>null</code>
	 */
	private boolean valid = true;

	/**
	 * @return the underlying value
	 */
	protected abstract Short getValue();

	/**
	 * @param value
	 *            the underlying value
	 */
	protected abstract void setValue(Short value);

	/**
	 * Updates the cached value if the underlying value has changed
	 */
	protected void update()
	{
		Short shortValue = getValue();
		if (shortValue != null)
		{
			stringValue = shortValue.toString();
			valid = true;
		}
	}

	/**
	 * @return the value that represents a characteristic or <code>null</code>
	 *         if this characteristic does not have any
	 */
	public String getShortValue()
	{
		return stringValue;
	}

	/**
	 * Sets a value for this characteristic. Empty strings are treated as
	 * <code>null</code>.
	 * 
	 * @param value
	 *            the string that represents a short attribute or
	 *            <code>null</code> for none
	 */
	public void setShortValue(String value)
	{
		// Empty strings are set to null
		if (value != null)
		{
			value = value.trim();
			if (value.length() == 0)
				value = null;
		}
		// If value did not change then do nothing
		if ((stringValue == value) || ((stringValue != null) && stringValue.equals(value)))
			return;
		stringValue = value;

		// If value is null then store it; it is valid
		if (value == null)
		{
			setValue(null);
			valid = true;
			return;
		}

		// Convert the string to a non-negative number
		try
		{
			Short shortValue = Short.valueOf(Short.parseShort(value));
			if (shortValue.shortValue() < 0)
				throw new NumberFormatException("value cannot be negative");
			setValue(shortValue);
			valid = true;
		}
		catch (NumberFormatException numberFormatException)
		{
			// If string is invalid then store null in the holder
			setValue(null);
			valid = false;
		}
	}

	/**
	 * @return whether or not the <code>stringValue</code> represents the stored
	 *         value
	 */
	public boolean isValid()
	{
		return valid;
	}

	/**
	 * Returns whether or not the stored value is <code>null</code>. Note that
	 * we would rather use <code>isEmpty</code> as the method name but "empty"
	 * is a reserved word in the JSF Expression Language.
	 * 
	 * @return whether or not the stored value is <code>null</code>
	 */
	public boolean isVacant()
	{
		return getValue() == null;
	}

	/**
	 * Version UID for serializable class
	 */
	private static final long serialVersionUID = 3214381308667045785L;
}
