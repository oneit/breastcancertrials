/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import javax.ejb.Local;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.quantumleaphealth.model.patient.Invitation;
import org.quantumleaphealth.model.patient.PatientDisposition;
import org.quantumleaphealth.model.screener.UserScreener;
import org.quantumleaphealth.screen.EligibilityProfile;

/**
 * Navigates a screener through viewing and dispositioning patient histories.
 * This interface defines the following roles for a local stateful session
 * Enterprise JavaBean (EJB):
 * <ol>
 * <li>Provide facades into the patient's data. Facades are used to translate
 * the view's representation of the data into the storage format of the patient
 * object.</li>
 * <li>Provide data persistence. Methods are used to load and store data from/to
 * a persistence provider.</li>
 * <li>Sort and select the data. Methods are used to sort data and to select
 * data for the view.</li>
 * </ol>
 * 
 * @author Tom Bechtold
 * @version 2009-07-07
 */
@Local
public interface ScreenerInterface
{
	/**
	 * @return the screener, guaranteed to be non-<code>null</code>
	 */
	public UserScreener getScreener();

	/**
	 * @return the screener's invitations, guaranteed to be non-
	 *         <code>null</code>
	 */
	public ScreenerGroupsSiteTrialInvitations getInvitations();

	/**
	 * Authenticates and loads a screener whose principal and credentials are
	 * stored in its <code>screener</code> property or loads the screener's
	 * secret question if credentials are not provided and updates its
	 * <code>unsuccessfulAuthenticationCount</code>.
	 */
	public void login();

	/**
	 * @return whether or not the screener is logged in
	 */
	public boolean isLoggedIn();

	/**
	 * Validates that the termsAccepted checkbox was checked.
	 * 
	 * @param facesContext
	 *            the faces context
	 * @param component
	 *            the checkbox component
	 * @param value
	 *            the component's value
	 */
	public void validateTermsAccepted(FacesContext facesContext, UIComponent component, Object value);

	/**
	 * Refresh the list of invitations
	 * 
	 * @see #getInvitations()
	 */
	public void refreshInvitations();

	/**
	 * Log out the screener and invalidate the <code>HttpSession</code>
	 */
	public void logout();

	/**
	 * @return the credentials which must match the screener's in order for the
	 *         screener's new authentication to be persisted
	 */
	public String getCredentials();

	/**
	 * @param credentials
	 *            the credentials which must match the screener's in order for
	 *            the screener's new authentication to be persisted
	 */
	public void setCredentials(String credentials);

	/**
	 * Validates the credentials are entered and are equal to the other screener
	 * credentials. This method invalidates the component and adds a
	 * non-localized faces messages if the submitted value is not available or
	 * if the submitted value is not equal (case-sensitive) to the other
	 * screener credentials component's submitted value.
	 * 
	 * @param facesContext
	 *            the faces context
	 * @param component
	 *            the input component
	 * @param value
	 *            the component's value
	 */
	public void validateCredentials(FacesContext facesContext, UIComponent component, Object value);

	/**
	 * Update the screener's record
	 */
	public void update();

	/**
	 * @param invitationId
	 *            the id of the invitation to select
	 */
	public void setInvitationId(Long invitationId);

	/**
	 * @return the selected invitation or <code>null</code> if no invitation is
	 *         selected
	 */
	public Invitation getInvitation();

	/**
	 * @return the selected disposition or <code>null</code> if no invitation is
	 *         selected
	 */
	public PatientDisposition getPatientDisposition();

	/**
	 * Returns the invitation's patient history facade and sets the invitation's
	 * <code>viewed</code> field if not set yet.
	 * 
	 * @return the patient history facade or <code>null</code> if no selected
	 *         invitation
	 */
	public FacadePatientHistory getHistory();

	/**
	 * @return the invitation's patient eligibility profile or <code>null</code>
	 *         if no selected invitation
	 */
	public EligibilityProfile getProfile();

	/**
	 * @return the one-based ordinal value of the invitation's
	 *         <code>disposition</code> field in bit-wise format or
	 *         <code>null</code> if the invitation is <code>null</code>
	 */
	public long getDispositionBits();

	/**
	 * @param disposition
	 *            one-based ordinal value of the invitation's
	 *            <code>disposition</code> field in bit-wise format
	 */
	public void setDispositionBits(long dispositionBits);

	/**
	 * Update the current invitation's disposition
	 */
	public void updateDisposition();

	/**
	 * Removes the stateful session bean from memory. This method is required by
	 * the EJB3 specification.
	 */
	public void destroy();
}
