/**
 * (c) 2009 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.util.List;

import javax.ejb.Local;

import org.quantumleaphealth.model.patient.Invitation;

/**
 * Creates, withdraws, retrieves and sorts a patient's invitations. Each
 * instance of the implementing class services a particular patient.
 * 
 * @author Tom Bechtold
 * @version 2009-07-08
 */
@Local
public interface InvitationManager
{
	/**
	 * @return the patient's invitations grouped by trial and sorted by site
	 *         name, guaranteed to be non-<code>null</code>
	 */
	public List<TrialInvitations> getTrialInvitations();

	/**
	 * @return the current invitation or <code>null</code> if not set
	 */
	public Invitation getInvitation();

	/**
	 * Sets the current invitation to either an existing one or to a new one
	 * that references a particular site within the user's matching trials. If
	 * the list of current trial sites is not available or does not contain the
	 * desired site then the current invitation is set to <code>null</code>. If
	 * a new invitation is created then it populates the following fields from
	 * the first sorted persisted invitation:
	 * <ul>
	 * <li><code>patientName</code></li>
	 * <li><code>patientContact</code></li>
	 * <li><code>message</code></li>
	 * </ul>
	 * 
	 * @param trialSite
	 *            a concatenation of the unique identifier of the trial and the
	 *            character <code>|</code> and the unique identifier of the
	 *            site; if either id empty then do nothing
	 */
	public void setTrialSite(String trialSite);

	/**
	 * @return the patient's name for newly created invitations
	 */
	public String getPatientName();

	/**
	 * @param patientName
	 *            the patient's name for newly created invitations
	 */
	public void setPatientName(String patientName);

	/**
	 * @return the patient's contact information for newly created invitations
	 */
	public String getPatientContact();

	/**
	 * @param patientContact
	 *            the patient's contact information for newly created
	 *            invitations
	 */
	public void setPatientContact(String patientContact);

	/**
	 * @return the message for newly created invitations
	 */
	public String getMessage();

	/**
	 * @param message
	 *            the message for newly created invitations
	 */
	public void setMessage(String message);

	/**
	 * Persists the current invitation by adding it to the persistence engine
	 * and to the patient's list of invitations. If the current invitation is
	 * not available or is already persisted then this method does nothing.
	 * 
	 * @param trialId
	 *            the unique identifier of the trial; if empty then do nothing
	 * @param siteId
	 *            the unique identifier of the site; if empty then do nothing
	 */
	public void invite();

	/**
	 * Withdraws the current invitation by removing it from the patient's list
	 * of invitations and from the persistence engine. If the current invitation
	 * is not available or is already withdrawn then this method does nothing.
	 * The current invitation is set to a new one whose following fields are
	 * copied from the removed invitation:
	 * <ul>
	 * <li><code>patientName</code></li>
	 * <li><code>patientContact</code></li>
	 * <li><code>message</code></li>
	 * </ul>
	 * 
	 * @param trialId
	 *            the unique identifier of the trial; if empty then do nothing
	 * @param siteId
	 *            the unique identifier of the site; if empty then do nothing
	 */
	public void withdraw();

	/**
	 * Removes the stateful session bean from memory. This method is required by
	 * the EJB3 specification.
	 */
	public void destroy();
}
