package org.quantumleaphealth.action;

import javax.ejb.Local;

/**
 * 9/12/2013
 * 
 * The purpose of this class is to proxy requests to the PatientInterfaceAction and NavigatorInterfaceAction
 * session level stateful beans for the current login state.  Public pages such as browse_trials were querying
 * these beans directly.  This was causing their creation on pages that had a conversation level of Request only.
 * As each such bean has a session time out set high (currently two hours), a backup of such beans in the session
 * context was exhausting virtual memory.  The symptoms were very much like those of a memory leak.
 * 
 * @author wgweis
 *
 */
@Local
public interface UserProxyInterface 
{
	/**
	 * Use this EL expression:  "userProxy.userPatientLoggedIn" instead of "patientInterface.loggedIn" to
	 * avoid creating a UserPatientInterfaceAction object on a request only page.
	 */
	public abstract boolean isUserPatientLoggedIn();
	
	/**
	 * Use this EL expression:  "userProxy.navigatorLoggedIn" instead of "navigatorInterface.loggedIn" to
	 * avoid creating a NavigatorInterfaceAction object on a request only page.
	 */
	public abstract boolean isNavigatorLoggedIn();

	/**
	 * Removes the stateful session bean from memory. This method is required by
	 * the EJB3 specification.
	 */
	public void destroy();
}
