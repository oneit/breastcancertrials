/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import java.io.Serializable;
import java.util.ArrayList;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * A message. Objects are scoped to a Seam session so that each user would have
 * his/her own message.
 * Modified to add a list of recipients.  WGW 55/31/2011
 * 
 * @author Tom Bechtold
 * @version 2008-12-19
 */
@Name("simpleMessage")
@Scope(ScopeType.SESSION)
public class SimpleMessage implements Serializable
{
	/**
	 * Limit the number of email recipients to ten to avoid spamming.
	 */
	private static final int MAXEMAILS = 10;
	
	@In (required = false )
	private PatientInterface patientInterface;
	
	/**
	 * The sender or <code>null</code> if anonymous
	 */
	private String from;
	/**
	 * The sender's name or <code>null</code> if anonymous
	 */
	private String name;
	/**
	 * The body text
	 */
	
	/**
     *  The email's subject field.
     */
	private String Subject;
	
	/**
     *  The email's body field.
     */
	private String body;
	/**
	 * Whether or not the message was delivered
	 */	
	private boolean delivered;
	
	/**
	 * List of recipients in string form.
	 */
	private String to;
	
	/**
	 * This field will be filled out with the name the user wants to indicate this link was sent by in
	 * the email title.
	 */
	private String fromTitleName;

	public static final String defaultBodyLine2 = "(a link to this page will be included with your message)";

	/**
	 * @return the sender or <code>null</code> if anonymous
	 */
	public String getFrom()
	{
		return from;
	}

	/**
	 * @param from
	 *            the sender or <code>null</code> if anonymous
	 */
	public void setFrom(String from)
	{
		this.from = trim(from);
	}

	/**
	 * @return the name or <code>null</code> if anonymous
	 */
	public String getName()
	{
		return name;
	}

	/**
	 * @param name
	 *            the name or <code>null</code> if anonymous
	 */
	public void setName(String name)
	{
		this.name = trim(name);
	}

	/**
	 * @return the body text
	 * As of 1.4.05 a hack was put in place to allow a default text if the property is still pointing to null,
	 * and the instance is being referenced from the browse_trials or quickviews pages.  As this class is
	 * session level, rather than request level, this requires some logic here.
	 */
	public String getBody()
	{
		return body;
	}

	/**
	 * @param body
	 *            the body text to set
	 */
	public void setBody(String body)
	{
		this.body = trim(body);
	}

	/**
	 * @return whether or not the message was delivered
	 */
	public boolean isDelivered()
	{
		return delivered;
	}

	/**
	 * @param delivered
	 *            whether or not the message was delivered
	 */
	public void setDelivered(boolean delivered)
	{
		this.delivered = delivered;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo(String to)
	{
		this.to = to;
	}

	/**
	 * @return the to
	 */
	public String getTo()
	{
		return to;
	}

	/**
	 * @return the recipients
	 */
	public InternetAddress[] getRecipients()
	{
		String trimmedTo = trim( to ) == null? "": trim( to );
		ArrayList<InternetAddress> actualRecipients = new ArrayList<InternetAddress>();
		
		String[] parcedEntries = trimmedTo.split(",");
		for ( int i = 0; i < parcedEntries.length  && i < MAXEMAILS; i++ )
		{
			String entry = parcedEntries[i].trim();
			if ( entry != null && entry.length() > 0 )
			{	
				try
				{
					InternetAddress internetAddress = new InternetAddress( entry );
					if ( !actualRecipients.contains( internetAddress) )
					{	
						actualRecipients.add( internetAddress );
					}	
				}
				catch (AddressException e)
				{
					// ignored.  We'll just skip this recipient if we can't parse it.
				}
			}
		}

		if ( actualRecipients.size() <= 0 ) 
		{
			return null;
		}
		
		InternetAddress[] recipients = new InternetAddress[ actualRecipients.size() ];
		return (InternetAddress[])  actualRecipients.toArray( recipients );
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject)
	{
		Subject = subject;
	}

	/**
	 * @return the subject.  Guaranteed to be not null.
	 */
	public String getSubject()
	{
		if ( Subject == null )
		{
			Subject = new String("");
		}
		return Subject;
	}

	/**
	 * @param fromTitleName the fromTitleName to set
	 */
	public void setFromTitleName(String fromTitleName) 
	{
		this.fromTitleName = fromTitleName;
	}

	/**
	 * @return the fromTitleName.
	 */
	public String getFromTitleName() 
	{
		return trim( fromTitleName );
	}

	/**
	 * @param string
	 *            the string to trim
	 * @return the trimmed string or <code>null</code> if empty
	 */
	private static String trim(String string)
	{
		if (string == null)
			return null;
		string = string.trim();
		return (string.length() == 0) ? null : string;
	}

	/**
	 * Returns contents of the fields
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString()
	{
		StringBuilder builder = new StringBuilder(super.toString());
		builder.append('[');
		if ((from != null) && (from.trim().length() > 0))
			builder.append("from=").append(from).append(',');
		builder.append("body=").append(body);
		if (delivered)
			builder.append(",delivered");
		return builder.append(']').toString();
	}
	
	/**
	 * Version uid for serializable class
	 */
	private static final long serialVersionUID = -6584868278714160043L;
}
