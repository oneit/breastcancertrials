package org.quantumleaphealth.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import org.apache.commons.lang.StringUtils;


import org.hibernate.CacheMode;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Factory;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.patient.UserPatient;
import org.quartz.CronTrigger;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.impl.StdScheduler;

@Stateful
@Name("quartzJobUpdateInterface")
public class QuartzJobUpdateInterfaceAction implements QuartzJobUpdateInterface
{
	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log LOGGER;

	/**
	 * Injected faces messages so that we can display messages to users on the quartz job update section on the su/menu.seam page.
	 */
	@In
	FacesMessages facesMessages;

	/**
	 * weekDays starts and ends with Sunday because for Calendar Sunday has the value of 7, but for Date it has the value of
	 * 0.
	 * 
	 */
	private static final String[] weekDays = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Fri", "Sun"};
	/**
	 * Returns list of existing avatars for use in a JSF datatable.
	 */
	@DataModel("quartzjobs")
	private List<FacadeQuartzJob> quartzjobs;

	/**
	 * The instance selected from the JSF datatable.
	 */
	@SuppressWarnings("unused")
	@DataModelSelection("quartzjobs")
    private FacadeQuartzJob jobSelection;

	/**
	 * @return a list of Quartz job information
	 * @throws NamingException
	 * @throws SchedulerException
	 */
	@SuppressWarnings("unchecked")
	private List<FacadeQuartzJob> getQuartzJobs() throws NamingException, SchedulerException
	{
		InitialContext initialContext = new InitialContext();
		StdScheduler jobScheduler = (StdScheduler) initialContext.lookup("Quartz");

		ArrayList jobList = new ArrayList();
		String[] jobNames = jobScheduler.getJobNames(StdScheduler.DEFAULT_GROUP);
		FacadeQuartzJob aJob = null;
		Trigger[] jobTriggers = null;
		Trigger aTrigger = null;
		for (String jobName : jobNames)
		{
			jobTriggers = jobScheduler.getTriggersOfJob(jobName, StdScheduler.DEFAULT_GROUP);
			if (jobTriggers.length > 0)
			{
				aTrigger = jobTriggers[0];
				aJob = new FacadeQuartzJob();
				aJob.setJobName(jobName);
				CronTrigger cronTrigger = (CronTrigger) aTrigger;
				aJob.setTriggerCronExpression(cronTrigger.getCronExpression());
				aJob.setTriggerName(aTrigger.getKey().getName());
				aJob.setLastTriggerTime(aTrigger.getPreviousFireTime());
				aJob.setNextTriggerTime(aTrigger.getNextFireTime());
				jobList.add(aJob);
			}
		}
		return jobList;
	}

	/**
	 * factory method so that SEAM can construct the quartz job list.
	 */
	@Override
	@Factory("quartzjobs")
	public void loadQuartzJobs() throws NamingException, SchedulerException 
	{
		quartzjobs = getQuartzJobs();
		formatNewRunDateComponents();
	}

	private void formatNewRunDateComponents()
	{
		for (FacadeQuartzJob job: quartzjobs)
		{
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(job.getNextTriggerTime());
			job.setNewTriggerDay(calendar.get(Calendar.DAY_OF_WEEK));
			job.setNewTriggerTime(calendar.getTime());
		}
	}

	private String newCronExpression(String oldCronExpression, int newDayOfWeek, int newHour, int newMinute)
	{
		String[] cronComponents = oldCronExpression.split("\\s+");
		
		cronComponents[1] = Integer.toString(newMinute); // set the minute field
		cronComponents[2] = Integer.toString(newHour); // set the hour field

		try
		{
			Integer.parseInt(cronComponents[5]); 
			cronComponents[5] = Integer.toString(newDayOfWeek); // set the day of the week field if it is a single integer (as in Tuesday)
		}
		catch(NumberFormatException nfe)
		{
			//ignored.  We just won't set this field if the parsing fails
		}
		
		return StringUtils.join(cronComponents, " ");
	}
	
	@Override
	public void updateQuartzJobs()  throws NamingException, SchedulerException, ParseException
	{
		InitialContext initialContext = new InitialContext();
		StdScheduler jobScheduler = (StdScheduler) initialContext.lookup("Quartz");
		boolean updateFacade = false;

		for (FacadeQuartzJob job: quartzjobs)
		{
			Calendar newTriggerWeekDay = Calendar.getInstance();
			
			Date weekDay = new SimpleDateFormat("E").parse(weekDays[job.getNewTriggerDay() - 1]);
			newTriggerWeekDay.setTime(weekDay);

			Calendar newTriggerTime = Calendar.getInstance();
			newTriggerTime.setTime(job.getNewTriggerTime());

			Calendar oldTriggerDate = Calendar.getInstance();
			oldTriggerDate.setTime(job.getNextTriggerTime());
			
			Calendar oldTriggerDateTruncated = Calendar.getInstance();
			oldTriggerDateTruncated.set(oldTriggerDate.get(Calendar.YEAR), 
								   		oldTriggerDate.get(Calendar.MONTH), 
								   		oldTriggerDate.get(Calendar.DATE), 
								   		oldTriggerDate.get(Calendar.HOUR_OF_DAY), 
								   		oldTriggerDate.get(Calendar.MINUTE), 0);
			
			if (oldTriggerDateTruncated.get(Calendar.MINUTE) != newTriggerTime.get(Calendar.MINUTE) || 
				oldTriggerDateTruncated.get(Calendar.HOUR_OF_DAY) != newTriggerTime.get(Calendar.HOUR_OF_DAY) || 
				oldTriggerDateTruncated.get(Calendar.DAY_OF_WEEK) != newTriggerWeekDay.get(Calendar.DAY_OF_WEEK))
			{	
				CronTrigger trigger = new CronTrigger( job.getTriggerName(), StdScheduler.DEFAULT_GROUP );
				trigger.setJobName(job.getJobName());
				try 
				{
					trigger.setCronExpression(newCronExpression(job.getTriggerCronExpression(), newTriggerWeekDay.get(Calendar.DAY_OF_WEEK), newTriggerTime.get(Calendar.HOUR_OF_DAY), newTriggerTime.get(Calendar.MINUTE)));
				} 
				catch (ParseException e) 
				{
					LOGGER.error("Error setting new cron expression for " + job.getJobName(), e);
				}
				jobScheduler.rescheduleJob(job.getTriggerName(), StdScheduler.DEFAULT_GROUP, trigger);
				updateFacade = true;
			}	
		}
		
		if (updateFacade)
		{
			loadQuartzJobs();
		}
	}
	
	/**
	 * Reset instance variables when stateful session bean is destroyed.
	 * 
	 * Required by EJB3.
	 */	
	@Remove
	@Destroy
	public void destroy()
	{
	}
}
