package org.quantumleaphealth.action;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;

import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.screen.DeclarativeMatchingEngine;

/**
 * The SimpleMessage class has a session level scope.  For emailing links, however, we need a bean with a scope of request
 * so that we can pick up changes to the categoryString request parameter.  This bean extends SimpleMessage and customizes
 * some of the SimpleMessage fields.  Currently we are using the body and to fields, but this may change.
 * 
 * @author wgweis
 *
 */
@Name("emailLinkMessage")
@Stateful
public class EmailLinkMessageAction extends SimpleMessage implements EmailLinkMessage
{
	/**
	 * Necessary because SimpleMessage implements Serializable
	 */
	private static final long serialVersionUID = -6064523014488881117L;
	
	private static final String defaultBodyLine2 = "(a link to this page will be included with your message)";

	private static final String defaultBodyFormat = "I thought you might be interested in these %sstudies from BreastCancerTrials.org:\n\n" +
									   				defaultBodyLine2;
	
	public static final String avatarUrlTextFormatLine1 = "Click, or cut and paste the URL below into your browser.\n";
	
	private static final String avatarBodyFormat = "I thought you might be interested in this sample patient profile from BreastCancerTrials.org:\n\n" +
													defaultBodyLine2;
		
	public static final String UrlTextFormatLine1 = "Click, or cut and paste the URL below into your browser to view %sstudies from BCT.\n";
	
	@Logger
	private static Log logger;
	
	/**
	 * The request parameter categoryString.  The whole reason for this bean.
	 */
	@RequestParameter( "categoryString" )
	private String categoryString;

	@RequestParameter( "pid" )
	private String principal;

	/**
	 * @param key
	 * @return the bundle label associated with this category for use in the default email body.
	 */
    private String getCategoryLabel( String key )
    {
		if ( key == null )
		{
			return "label.type.NOTFOUND";
		}
		return "label.type." + key.toUpperCase();
    }
    
    /**
     * get the text associated in the property bundle with this category for use in the default email body.
     */
    public String getCategoryBundleValue( String key )
    {
		if ( key == null || key.trim().length() <= 0 )
		{
			return null;
		}
		try
		{
			return DeclarativeMatchingEngine.getProfilebundle().getString( getCategoryLabel( key ) );
		}
		catch (java.util.MissingResourceException ex)
		{
			logger.error("There is no resource for key #0.", key);
			return null;
		}
    }
    
    /**
     * Override of the SimpleMessage body method.
     */
    @Override
	public String getBody()
	{
		if ( super.getBody() == null )
		{
			String categoryBundleString = null;
			if ( categoryString != null && !categoryString.equals( "All" ) )
			{
				categoryBundleString = getCategoryBundleValue( categoryString );
				return String.format(defaultBodyFormat, categoryBundleString == null? "": "\"" + categoryBundleString + "\"" + " ");
			}
			else if ( getPrincipal() != null )
			{
				return avatarBodyFormat;
			}
			return String.format(defaultBodyFormat, categoryBundleString == null? "": "\"" + categoryBundleString + "\"" + " ");
		}
		return super.getBody();
	}

	/**
	 * @param body
	 *            the body text to set
	 */
    @Override
	public void setBody(String body)
	{
		super.setBody( body );
	}
	
	/**
	 * The FromTitleName is currently used from the SimpleMessage class as it needs survive across requests.
	 */
    @Override    
	public void setFromTitleName(String fromTitleName) 
	{
		super.setFromTitleName( fromTitleName );
	}

	/**
	 * @return the fromTitleName.
	 */
    @Override    
	public String getFromTitleName()
	{
		return super.getFromTitleName();
	}

    /**
     * Custom validator for the email list of recipients. Fails if any of the emails in the list is invalid.
     */
	public void validateToList(FacesContext facesContext, UIComponent component, Object value)
	{
		final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		try
		{
			String to = (value instanceof String) ? (String) value: null;
			logger.debug("validateToList validating to list X" + to + "X");

			if (to == null || to.trim().length() <= 0)
			{
				FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR, "The Email List of email addresses to send to cannot be empty.");
				if (component instanceof UIInput)
				{	
					((UIInput) (component)).setValid(false);
				}	
				return;
			}
			
			String[] emailList = to.split("\\s*,\\s*");
			for (String s: emailList)
			{
				String email = s.trim();
				logger.debug("Validating X" + email + "X");
				if (!email.matches(EMAIL_PATTERN))
				{
					FacesMessages.instance().addToControl(component.getClientId(facesContext), Severity.ERROR, "One or more email addresses are invalid. All emails must be written in the form of \"johndoe@bct.org\".");
					((UIInput) (component)).setValid(false);
					return;
				}
			}
		}
		catch (Exception e)
		{
			logger.error("Exception validating email list", e);
		}
	}

	/**
	 * Override of the SimpleMessage class methods.  "to" is a request level parameter on the email_link.xhtml page.
	 * @param to the to to set
	 */
    @Override    
	public void setTo(String to)
	{
		super.setTo( to );
	}
    
	/**
	 * @return the to
	 */
    @Override    
	public String getTo()
	{
		return super.getTo();
	}

	/**
	 * @return the categoryString
	 */
    @Override    
	public String getCategoryString() 
	{
		return categoryString;
	}

	/**
	 * @return the aid
	 */
	public String getPrincipal() 
	{
		return principal;
	}

	/**
	 * Reset instance variables when stateful session bean is destroyed.
	 * Required by jboss EJB. 
	 */	
	@Remove
	@Destroy
	@Override
	public void destroy()
	{
	}
}
