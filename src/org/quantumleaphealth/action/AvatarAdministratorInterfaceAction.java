package org.quantumleaphealth.action;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.faces.model.SelectItem;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;

import org.hibernate.CacheMode;
import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.datamodel.DataModel;
import org.jboss.seam.annotations.datamodel.DataModelSelection;
import org.jboss.seam.annotations.web.RequestParameter;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.faces.Renderer;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.patient.UserPatient;

/**
 * This class is used on a number of existing pages for avatar bct version 2.0 modifications. 
 * @author wgweis
 *
 */
@Stateful
@Name("avatarInterface")
public class AvatarAdministratorInterfaceAction implements AvatarAdministratorInterface
{
	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log LOGGER;
	
	/**
	 * Injected entity manager.  We do not need a transaction with rollback capability for this class.
	 */
	@PersistenceContext(type = PersistenceContextType.EXTENDED, unitName = "bct")
	private EntityManager em;

	/**
	 * list for use with JSF datatables.
	 */
	@DataModel
	private List<UserPatient> avatars;

	/**
	 * The selected instance in the JSF datatable.
	 */
	@SuppressWarnings("unused")
	@DataModelSelection
    private UserPatient avatarSelection;
	
	/**
	 * The avatar ID for the avatar trials page.
	 */
	@RequestParameter("aid")
	private Long avatarId;
	
	/**
	 * An injected instance of the FacesMessages so that we can add messages for display on the web page.
	 */
	@In
	FacesMessages facesMessages;

	/**
	 * 
	 */
//	@In(create=true)
//	private Renderer renderer;
	   
	/**
	 * load the avatar from the database.  Used by the avatar trials view and history review pages.
	 */
	@SuppressWarnings( { "unchecked", "cast" })	
	private void loadAvatars()
	{
		
		LOGGER.debug("Loading avatars");
		// the following query expects an avatar principal in the format of 'AvatarXXX@bct.org' where XXX consists of digits for ordering.  A default value of '0' is
		// returned if the avatar principal does not conform to this format.  It will only work with the postgres dialect.
		// Finally, the cast must be performed last.  The 8.2.3 jdbc implementation of cast cannot handle null values. 
		avatars = (List<UserPatient>) em.createQuery(
				"select p from UserPatient p where p.usertype = :usertype order by cast(coalesce(substring(substring(p.principal, 7), '[0-9]+'), '0') as int)")
				.setParameter("usertype", UserPatient.UserType.AVATAR_PROFILE)
				.setHint("org.hibernate.readOnly", Boolean.TRUE)
				.setHint("org.hibernate.cacheMode", CacheMode.IGNORE).getResultList();
		
		if ( avatars == null )
		{	
			LOGGER.error( "No avatars found." );
		}	
		else
		{
			LOGGER.debug("Finished loading #0 avatars", avatars.size() );				
		}
	}

	/**
	 * return a list of avatar profiles for use in JSF datatables.
	 */
	@Override
	public List<UserPatient> getAvatarPatientProfiles()
	{
		if ( avatars == null )
		{
			loadAvatars();
		}
		return avatars;
	}
	
	/**
	 * return a list of avatars for use in JSF select items.
	 */
	@Override
	public List<SelectItem> getAvatarPatientProfilesOptions()
	{		
		ArrayList<SelectItem> options = new ArrayList<SelectItem>();
		options.add( new SelectItem( -1, "None" ));
		for ( UserPatient avatar : getAvatarPatientProfiles() )
		{
			options.add( new SelectItem( avatar.getId(), avatar.getPrincipal() ) );
		}
		
		if ( avatarId != null )
		{
			try
			{
				avatarSelection = (UserPatient) em.createQuery( "select p from UserPatient p where id = :id and usertype = :usertype" )
														.setParameter("id", avatarId )
														.setParameter("usertype", UserPatient.UserType.AVATAR_PROFILE)
														.getSingleResult();
			}
			catch ( javax.persistence.NoResultException nre )
			{
				LOGGER.error("Failed in Attempt to retrieve Avatar " + avatarId);
			}
		}
		return options;
	}

	/**
	 * Reset instance variables when stateful session bean is destroyed.
	 * Required by jboss EJB. 
	 */	
	@Remove
	@Destroy
	public void destroy()
	{
	}
}
