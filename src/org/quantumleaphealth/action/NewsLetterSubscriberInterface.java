package org.quantumleaphealth.action;

import javax.ejb.Local;

import org.jboss.seam.annotations.remoting.WebRemote;

/**
 * interface for EJB3 for the NewsLetterSubscriberAction class.  Please see
 * {@link  NewsLetterSubscriberAction  the NewsLetterSubscriberAction class} 
 * for comments
 * @author wgweis
 *
 */
@Local
public interface NewsLetterSubscriberInterface 
{
	/**
	 * remote version for use by the static pages.
	 * @param newsletter_Subscriber_Principal
	 * @return String for error messages, etc.
	 */
	@WebRemote
	public String remoteSave(String newsletter_Subscriber_Principal);

	/**
	 * Save the entered email if it passes a couple of simple validations
	 */
	public String save();

	/**
	 * @param newsletter_Subscriber_Principal the newsletter_Subscriber_Principal to set
	 */
	public void setNewsletterSubscriberPrincipal(String newsletter_Subscriber_Principal);

	/**
	 * @return the newsletter_Subscriber_Principal
	 */
	public String getNewsletterSubscriberPrincipal(); 
	
	/**
	 * Reset instance variables when stateful session bean is destroyed.
	 * Required by jboss EJB. 
	 */	
	public void destroy();
}
