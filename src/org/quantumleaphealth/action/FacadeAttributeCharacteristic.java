/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import org.quantumleaphealth.ontology.AttributeCharacteristicHolder;
import org.quantumleaphealth.ontology.CharacteristicCode;

/**
 * Represents a facade into a map of attribute characteristics. This
 * serializable JavaBean exposes the <code>value</code> bit-wise field in
 * <code>long</code> representation to the GUI:
 * <ul>
 * <li>Bit #0 represents whether or not an attribute is
 * <tt>not-otherwise-specified</tt> (e.g., "other") as stored in the string
 * variable.</li>
 * <li>Bits 1 and above represent the position in the <code>valueSet</code> of
 * each attribute of the characteristic.</li>
 * </ul>
 * This class caches the bit-wise value in order to reduce effort querying the
 * holder.
 * 
 * @author Tom Bechtold
 * @version 2008-05-23
 */
public class FacadeAttributeCharacteristic extends FacadeAbstractCharacteristic
{
	/**
	 * Holds the attribute characteristics, guaranteed to be non-
	 * <code>null</code>
	 */
	private AttributeCharacteristicHolder attributeCharacteristicHolder;
	/**
	 * Caches the bit-wise representation of the zero-based positions within
	 * <code>valueSet</code> of the characteristic's attributes or
	 * <code>0</code> if nothing is set
	 */
	private long cachedValue;
	/**
	 * Cached value of the <tt>otherwise-not-specified</tt> attribute of the
	 * characteristic
	 */
	private String cachedOther;

	/**
	 * Validates and stores the parameter and sets the cached value.
	 * 
	 * @param characteristicCode
	 *            the code of the characteristic to represent
	 * @param valueSet
	 *            the codes of the characteristics that this characteristic's
	 *            attributes may be set to
	 * @param attributeCharacteristicHolder
	 *            holds the attribute characteristics
	 * @throws IllegalArgumentException
	 *             if any parameter is <code>null</code> or
	 *             <code>valueSet</code> is larger than <code>Long.SIZE</code>
	 * @see Long#SIZE
	 */
	public FacadeAttributeCharacteristic(CharacteristicCode characteristicCode, CharacteristicCode[] valueSet,
			AttributeCharacteristicHolder attributeCharacteristicHolder) throws IllegalArgumentException
	{
		super(characteristicCode, valueSet);
		setAttributeCharacteristicHolder(attributeCharacteristicHolder);
	}

	/**
	 * Validates and stores the parameter and updates the cached values.
	 * 
	 * @param attributeCharacteristicHolder
	 *            holds the attribute characteristics
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	void setAttributeCharacteristicHolder(AttributeCharacteristicHolder attributeCharacteristicHolder)
			throws IllegalArgumentException
	{
		if (attributeCharacteristicHolder == null)
			throw new IllegalArgumentException("holder not specified");
		this.attributeCharacteristicHolder = attributeCharacteristicHolder;
		// Turn the bit on for each member of the value set that is now stored
		// in the holder
		cachedValue = 0l;
		for (int index = 0; index < valueSet.length; index++)
			if (attributeCharacteristicHolder.containsAttribute(characteristicCode, valueSet[index]))
				cachedValue += 1l << index;
		cachedOther = attributeCharacteristicHolder.getString(characteristicCode);
	}

	/**
	 * Returns the bit-wise representation of the one-based positions within
	 * <code>valueSet</code> combined with the <tt>otherwise-not-specified</tt>
	 * status in bit #0. This method simply returns the cached value in the
	 * upper bits; bit 0 is set if the not-otherwise-specified attribute is set.
	 * 
	 * @return the bit-wise representation of the one-based positions within
	 *         <code>valueSet</code> with bit #0 representing whether the
	 *         not-otherwise-specified attribute is set or <code>0l</code> if
	 *         nothing is set
	 * @see org.quantumleaphealth.action.FacadeAbstractCharacteristic#getValue()
	 */
	@Override
	public long getValue()
	{
		return (cachedValue << 1) | (cachedOther == null ? 0l : 1l);
	}

	/**
	 * Sets the characteristic's attributes into the holder. This method does
	 * nothing if the cached value has not changed. The zeroth bit is ignored.
	 * 
	 * @param value
	 *            the bit-wise representation of the one-based position within a
	 *            <code>valueSet</code> of the attributes or <code>0l</code> to
	 *            remove all attributes
	 * @throws IllegalArgumentException
	 *             if the one-based position is higher than the size of
	 *             <code>valueSet</code>
	 */
	public void setValue(long value) throws IllegalArgumentException
	{
		// Convert positions from one-based to zero-based because bit #0 is
		// reserved for "other"
		value >>>= 1;
		// Only update if cached value differs
		if (cachedValue == value)
			return;
		// Turn off the bits that were cached
		long turnOff = cachedValue & ~value;
		int index = 0;
		while ((turnOff > 0) && (index < valueSet.length))
		{
			// Test bit #index
			if ((turnOff & 1l) != 0)
				attributeCharacteristicHolder.removeAttribute(characteristicCode, valueSet[index]);
			turnOff >>>= 1l;
			index++;
		}
		// Turn on the bits that were not cached
		long turnOn = ~cachedValue & value;
		index = 0;
		while ((turnOn > 0) && (index < valueSet.length))
		{
			// Test bit #index
			if ((turnOn & 1l) != 0)
				attributeCharacteristicHolder.addAttribute(characteristicCode, valueSet[index]);
			turnOn >>>= 1l;
			index++;
		}
		// Cache value
		cachedValue = value;
	}

	/**
	 * @return the string that represents a not-otherwise-specified attribute or
	 *         <code>null</code> if this characteristic does not have any
	 */
	public String getOther()
	{
		return cachedOther;
	}

	/**
	 * Sets a not-otherwise-specified attribute for this characteristic. Empty
	 * strings are treated as <code>null</code>.
	 * 
	 * @param other
	 *            the string that represents a not-otherwise-specified attribute
	 *            or <code>null</code> to remove any characteristic
	 */
	public void setOther(String other)
	{
		// Empty strings are set to null
		if (other != null)
		{
			other = other.trim();
			if (other.length() == 0)
				other = null;
		}
		if ((cachedOther == other) || ((cachedOther != null) && cachedOther.equals(other)))
			return;
		attributeCharacteristicHolder.setString(characteristicCode, other);
		cachedOther = other;
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = 4042100340336191852L;
}
