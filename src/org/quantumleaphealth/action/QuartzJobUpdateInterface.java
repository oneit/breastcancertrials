package org.quantumleaphealth.action;

import java.text.ParseException;

import javax.ejb.Local;
import javax.naming.NamingException;

import org.quartz.SchedulerException;

/**
 * methods for updating the next Quartz Job run time. 
 * @author wgweis
 */
@Local
public interface QuartzJobUpdateInterface
{
	/**
	 * Factory method for SEAM to populate the Quartz Job list for update.
	 */	
	public void loadQuartzJobs() throws NamingException, SchedulerException;

	/**
	 * update the Quartz Jobs
	 */
	public void updateQuartzJobs()  throws NamingException, SchedulerException, ParseException;
	
	/**
	 * destroy method required by EJB3.
	 */
	public void destroy();
}
