package org.quantumleaphealth.action;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.Remove;
import javax.ejb.Stateful;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.jboss.seam.annotations.Destroy;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Logger;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage.Severity;
import org.jboss.seam.log.Log;
import org.quantumleaphealth.model.patient.Newsletter_Subscriber;

/**
 * Action class for managing the Newsletter_Subscriber entity bean.
 * @author wgweis
 *
 */
@Name("newsLetterSubscriber")
@Stateful
public class NewsLetterSubscriberAction implements NewsLetterSubscriberInterface
{
	/**
	 * Injected logging facility
	 */
	@Logger
	private static Log logger;
	
	/**
	 * injected entity manager.  We will use the default transaction management.
	 */
	@PersistenceContext(unitName = "bct")
	private EntityManager em;

	/**
	 * injected facesmessges so that we can directly add messages for display on the web pages.
	 */
	@In
	FacesMessages facesMessages;

	private String newsletterSubscriberPrincipal;
	
	/**
	 * @return whether there is already and entry for this email in the database.
	 */
	@SuppressWarnings("unchecked")
	private boolean alreadySubscribed()
	{
		List<Newsletter_Subscriber> nls = em.createQuery("select n from Newsletter_Subscriber n where upper(principal) = upper(:principal)").setParameter("principal", getNewsletterSubscriberPrincipal()).getResultList();
		return nls.isEmpty() == false;
	}

	/**
	 * remote version for use by the static pages.
	 * @param newsletter_Subscriber_Principal
	 * @return String for error messages, etc.
	 */
	public String remoteSave(String newsletter_Subscriber_Principal)
	{
		setNewsletterSubscriberPrincipal(newsletter_Subscriber_Principal);
		return save();
	}
	
	/**
	 * Save the entered email if it passes a couple of simple validations
	 */
	public String save()
	{
		if (getNewsletterSubscriberPrincipal() == null || getNewsletterSubscriberPrincipal().trim().isEmpty())
		{	
			facesMessages.add(Severity.ERROR, "Your Email Address is required.");
			return "Your Email Address is required.";
		}
		
		if (alreadySubscribed())
		{
			facesMessages.add(Severity.ERROR, "You are already subscribed.");
			return "You are already subscribed.";
		}
		
		Newsletter_Subscriber nls = new Newsletter_Subscriber();
		nls.setPrincipal(getNewsletterSubscriberPrincipal());
		em.persist(nls);
		facesMessages.add(Severity.INFO, "Thank you for subscribing.");
		logger.debug("Added subscriber " + getNewsletterSubscriberPrincipal());
		return "success";
	}
	
	/**
	 * @param newsletter_Subscriber_Principal the newsletter_Subscriber_Principal to set
	 */
	public void setNewsletterSubscriberPrincipal(String newsletter_Subscriber_Principal) 
	{
		this.newsletterSubscriberPrincipal = newsletter_Subscriber_Principal;
	}

	/**
	 * @return the newsletter_Subscriber_Principal
	 */
	public String getNewsletterSubscriberPrincipal() 
	{
		return newsletterSubscriberPrincipal;
	}

	/**
	 * Reset instance variables when stateful session bean is destroyed.
	 * Required by jboss EJB. 
	 */	
	@Remove
	@Destroy
	@Override
	public void destroy()
	{
	}
}
