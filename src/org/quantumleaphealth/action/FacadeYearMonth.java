/**
 * (c) 2008 Quantum Leap Healthcare Collaborative, Inc.
 * All Rights Reserved
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation; 
 * either version 3.0 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License 
 * along with this library; if not, If not, see <http://www.gnu.org/licenses/>.
 */
package org.quantumleaphealth.action;

import org.quantumleaphealth.model.patient.YearMonth;

/**
 * A string facade into a year-month object.
 * 
 * @author Tom Bechtold
 * @version 2008-05-27
 */
public class FacadeYearMonth extends FacadeShort
{
	/**
	 * The year-month that this facade represents
	 */
	private YearMonth yearMonth;

	/**
	 * Validates and stores the parameter and updates the cached value
	 * 
	 * @param yearMonth
	 *            holds the year-month
	 * @throws IllegalArgumentException
	 *             if the parameter is <code>null</code>
	 */
	void setYearMonth(YearMonth yearMonth) throws IllegalArgumentException
	{
		if (yearMonth == null)
			throw new IllegalArgumentException("date not specified");
		this.yearMonth = yearMonth;
		update();
	}

	/**
	 * @return the underlying value
	 * @see org.quantumleaphealth.action.FacadeShort#getValue()
	 */
	@Override
	protected Short getValue()
	{
		return yearMonth.getYear();
	}

	/**
	 * @param value
	 *            the underlying value
	 * @see org.quantumleaphealth.action.FacadeShort#setValue(java.lang.Short)
	 */
	@Override
	protected void setValue(Short value)
	{
		yearMonth.setYear(value);
	}

	/**
	 * @return the month or 0 if not answered yet or unsure
	 */
	public byte getMonth()
	{
		return yearMonth.getMonth();
	}

	/**
	 * @param month
	 *            the month or 0 if not answered yet or unsure
	 */
	public void setMonth(byte month)
	{
		yearMonth.setMonth(month);
	}

	/**
	 * @return the year or <code>null</code> if not answered yet
	 */
	public String getYear()
	{
		return getShortValue();
	}

	/**
	 * @param value
	 *            the year or <code>null</code> if not answered yet
	 */
	public void setYear(String value)
	{
		setShortValue(value);
	}

	/**
	 * Version UID for serialization
	 */
	private static final long serialVersionUID = -7161331670354463858L;
}
