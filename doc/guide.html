<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>BreastCancerTrials.org Developer's Guide</title>
<meta name="author" content="Tom Bechtold"/>
<meta name="Last-Modified" content="Sun, 01 Feb 2009 13:00:00 PST"/>
</head><body>

<h1>BreastCancerTrials.org Developer's Guide</h1>

<p>
This document describes the software components of the BreastCancerTrials.org website (BCT.org)
and its associated programs.
Its intention is to provide enough detail to assist operations staff familiar with the technologies
and BCT.org use cases.
</p>

<ol>
<li><a href="#overview">Data Access and Technologies</a></li>
<li><a href="#design">Design patterns</a></li>
</ol>

<hr /><h2><a name="overview">Data Access and Technologies</a></h2>
<p>
	The mission of the BreastCancerTrials.org website is to help patients
	accrue to breast cancer clinical trials.
</p>

<h3>Data Access</h3>
<p><em>Note that there are no real-time interfaces to external systems.</em>
The system's data is accessed in several ways:
</p>
<ol>
	<li>Matching application (secure website): 
		Patients enter their health history into forms 
		and the website displays a list of matching trials.
		Patients may store their information and send invitations to research sites.</li>
	<li>Message service: (secure website): 
		Research site personnel review the history of patients who have sent invitations.</li>
	<li>User Administration: (secure website):
		Administrators reset patient passwords and perform other administrative functionality.</li>
	<li>Trial Administration: (Java desktop application):
		Analysts enter clinical trial descriptions and code eligibility criteria.</li>
	<li>Metrics Reporting: (Java desktop application):
		Management generates usage reports.</li>
	<li>Contact Data Administration (Adobe Flex): 
		Research site personnel assign contact information to clinical trials accruing at their site(s).</li>
	<li>Messaging (SMTP):
		A notification is sent to an individual whose data has a change in state or quality.</li>
	<li>Offline updates (Java command-line application):
		Trial data is periodically refreshed from the 
		National Cancer Institute's (NCI) Physician Data Query (PDQ) registry.
		Patients are matched to newly listed and recently updated trials.</li>
</ol>

<h3>Technologies and Versions</h3>

<ul>
	<li>Web Server: Apache httpd 2.2.x</li>
	<li>Application Server: JBoss 4.2.x (EJB3, JSF 1.2), Seam 2.x, iText PDF, Yahoo UI Library, Google Maps, Google Analytics, Scriptaculous library</li>
	<li>Database server: PostgreSQL 8.x</li>
	<li>Mail server: SMTP, IMAP</li>
	<li>Other development tools: Subversion source control, Issue tracking (JIRA), Document management (wiki)</li>
</ul>

<h3>JBoss Seam Framework features</h3>

<p>
	The following features of the <a href="http://seamframework.org">JBoss Seam Framework</a> are employed by BCT.org:
</p>
<ul>
	<li><b>Stateful contexts</b><br />
		BCT.org stores user state in the session context. 
		It employs Seam's event context for trial browsing since this activity may be requested by visitors
		who will not become users, therefore its state is stored in the view not in the server session.
		BCT.org does not employ Seam's long-running conversations because there is no logical branching point
		in the application that would call for multiple discrete conversations; 
		all actions that a user takes should be visible to all of the user's browser windows.  
	</li>
	<li><b>Templating view</b><br />
		Seam's recommends the <a href="https://facelets.dev.java.net/">Facelets view technology</a> 
		over JavaServer Pages (JSP) as Facelets supports templating and is a better fit for the 
		object-oriented backend components.
	</li>
	<li><b>PDF Integration</b><br />
		Seam's integration of the <a href="http://www.lowagie.com/iText/">iText PDF library</a> 
		with <a href="https://facelets.dev.java.net/">Facelets</a> 
		is employed to produce downloadable reports such as the patient's health history and list of matching trials.
	</li>
	<li><b>Transaction Management</b><br />
		BCT.org defines its Seam objects using Enterprise JavaBeans 3 (EJB3) entity and session beans 
		rather than Plain Old Java Objects (POJOs) in order to support transactions.
	</li>
	<li><b>Decoupled auditing</b><br />
		BCT.org's metrics data are gathered using Seam's asynchronous event feature in order to decouple
		the business logic from the metrics.
	</li>
	<li><b>Enhanced expression language</b><br />
		Seam's enhanced expression language is used throughout the Facelets pages
		to provide easier navigation rules and form validation.
	</li>
	<li><b>RESTful URLs</b><br />
		Seam's page parameters and <code>@RequestParameter</code> syntax are used to provide
		RESTful URLs for easier auditing and user bookmarks.
	</li>
</ul>

<hr /><h2><a name="design">Design Patterns</a></h2>

<h3>Ontology <code>[org.quantumleaphealth.ontology]</code></h3>
<p>
	Elements of a patient's health history and a trial's eligibility criteria 
	are defined using the National Library of Medicine's (NLM) 
	Unified Medical Language System (UMLS) ontology in <code>BreastCancerCharacteristicCodes</code>.
	Characteristics are stored according to their type:
</p>
<ul>
	<li><em>Binary characteristic:</em> The characteristic is either present or absent (ex:, HIV, Diabetes).
		Binary characteristics are stored in an instance of <code>BinaryCharacteristicHolder</code>.</li>
	<li><em>Value characteristic:</em> The characteristic contains a single value (ex:, gender is female, cancer stage is III).
		Value characteristics are stored in an instance of <code>ValueCharacteristicHolder</code>.</li>
	<li><em>Attribute characteristic:</em> The characteristic has zero or more children (ex:, cardiovascular conditions are hypertension &amp; heart attack).
		Attribute characteristics are stored in an instance of <code>AttributeCharacteristicHolder</code>.</li>
	<li><em>Short characteristic:</em> The characteristic is a short number (ex:, year of birth, number of axillary nodes).
		Short characteristics are stored in an instance of <code>ShortCharacteristicHolder</code>.</li>
	<li><em>ByteShort characteristic:</em> The characteristic specifies both byte and short numbers (ex:, 3/2008).
		ByteShort characteristics are stored in an instance of <code>BinaryCharacteristicHolder</code>.</li>
</ul>

<h3>Entities <code>[org.quantumleaphealth.model.*]</code></h3>
<p>
	Enterprise JavaBean 3 (EJB3) entity classes represent a patient user, 
	a research site user ("screener") and a clinical trial.
</p>
<h4>Ordinal enumerated properties</h4>
<p>
	Enumeration properties are stored into numeric fields by their ordinal value.
</p>
<h4>Marshaling variable data</h4>
<p>
	Health history and eligibility criteria are marshaled into XML fields
	using <code>java.beans.XMLEncoder</code> and <code>org.quantumleaphealth.xml.DecodingExceptionListener</code>.
</p>
<h4>Health history associations</h4>
<p>
	The health history contains characteristics as described above and:
</p>
<ul>
	<li><em>Diagnosis:</em> The most recent breast cancer diagnosis for a patient 
		(ex:, left breast, right breast, metastatic)
		including pathological analysis (ex: cancer stage, type, markers, node size).</li>
	<li><em>Procedure:</em> Each relevant procedure that a patient has had performed (ex:, surgical, radiation)
		including type, kind and location and start/stop dates.</li>
	<li><em>Therapy:</em> Each relevant therapy that a patient has taken (ex:, chemotherapy, endocrine therapy)
		including type, agent, start/stop dates and status.</li>
</ul>
<h4>Eligibility criteria</h4>
<p>
	Each type of association in the patient's health history is represented by a trial's eligibility criterion.
</p>

<h3>Eligibility Criteria Matching Engine (caMATCH) <code>[org.quantumleaphealth.screen]</code></h3>
<p>
	The caMATCH engine <code>DeclarativeMatchingEngine</code> applies a trial's eligibility criteria to a patient's health history.
</p>
<h4>Precompiling declarative criteria into imperative code</h4>
<p>
	Eligibility criteria are stored declaratively to ensure that they are not tied 
	to a particular health history format (e.g., <em>decoupled</em>).
	Likewise, patient characteristics are stored declaratively to ensure that they are decoupled
	from any particular eligibility format.
	For example, a simple eligibility criterion is <em>gender must be female</em>
	and a simple patient characteristic is <em>gender is female</em>.
	The matching engine <code>org.quantumleaphealth.screen.MatchingEngine</code> is specific to the 
	storage formats specified in the <code>org.quantumleaphealth.model.patient.PatientHistory</code> 
	and <code>org.quantumleaphealth.model.trial.EligibilityCriteria</code> objects that are being matched.
	It pre-compiles each loaded trial's eligibility criteria
	into instances of <code>org.quantumleaphealth.screen.Matchable</code>
	using <code>org.quantumleaphealth.screen.BreastCancerConverter</code>.
	This conversion of declarative criteria into imperative logic 
	makes the subsequent matching process more efficient.
	The engine does not need to interpret each criterion; 
	it simply needs to gather the relevant characteristic 
	from the patient history and apply the criterion's requirement.
</p>
<h4>Subjective matching</h4>
<p>
	An eligibility criterion's type determines whether it is 
	objective (<em>Requires</em> or <em>Excludes</em>), subjective (<em>Alert</em>),
	voluntary (<em>Allows</em>) or unsupported (<em>Not Matched</em>).
</p>
<h4>Hierarchical criteria</h4>
<p>
	Multiple criteria may be grouped hierarchically using boolean operators.
</p>
<h4>Eligibility profile describes matching result</h4>
<p>
	The matching engine provides the details of which patient characteristic(s) are
	relevant to each criterion
	in an instance of <code>org.quantumleaphealth.screen.EligibilityProfile</code>.
</p>

<h3>Actions <code>[org.quantumleaphealth.action]</code></h3>
<p>
	BCT.org design employs the <em>Data Transfer Object</em>, 
	<em>Facade</em> and <em>Manager</em> patterns.
</p>
<h4>Data Transfer Objects</h4>
<p>
	In several situations read-only data displayed in the view does not align with how it is 
	stored in the server state. 
	For each situation a data transfer object is defined to hold an object graph that
	is populated with state data, aggregated into a collection and displayed in the view
	using iterator controls. 
	The data transfer object is transient and only used to convey state to the view.
	For example, the list of closest research sites to the user's zip code is implemented as 
	a collection of data transfer objects, each of which holds a site and its proximity.
</p>
<h4>Facades</h4>
<p>
	Facades are also employed to overcome the impedance mismatch between the server state and the view. 
	Facades differ from data transfer objects in that facades are writable while data transfer objects are read-only.
	BCT's facades allow a patient to enter and view her health characteristics.
	For example, the diabetes health condition is collected using a standard JSF check-box control.
	However, the diabetes condition is stored in the patient record by the occurrence or absence  of the diabetes code 
	-- Unified Medical Language System (UMLS) concept code #11849 -- 
	in a collection of patient characteristics.
	In this case a <code>FacadeBinaryCharacteristic</code> facade is created 
	that wraps the patient history, exposing <code>getBooleanValue()</code> and <code>setBooleanValue()</code>
	methods to the view. 
	The JSF check-box control calls these get/set methods which operate on the underlying collection of characteristics.
</p>
<h4>Manager Session Beans</h4>
<p>
	Persistent state is held in EJB3 entity beans and managed using EJB3 session beans.
	For example, the <code>Patient</code> entity is managed by 
	an instance of the <code>PatientInterfaceAction</code> class 
	that is defined by the <code>PatientInterface</code> interface. 
	Each action instance is stored in Seam's session context and holds 
	not only the entity bean but also transient state such as whether or not the user is authenticated,
	the trial that the user is currently viewing, etc. 
	The manager classes also perform custom validation on JSF input controls
	and support error handling and logging.
	Transaction support is automatically provided by the EJB3 container to each session bean method.
</p>

<h3>JavaServer Faces custom features <code>[org.quantumleaphealth.jsf]</code></h3>
<h4>BooleanSet Components</h4>
<p>
	Displays and edits a set of choices such as a value and an attribute characteristic.
	A single-choice set (e.g., value characteristic) is represented with each choice displayed as a radio-button. 
	A multiple-choice set (e.g., attribute characteristic) is represented with each choice displayed as a check-box.
	Each binary state of a button/check-box is stored as a bit within a number.
</p>
<h4>Formatting functions</h4>
<p>
	Returns strings that represent complex objects such as names and dates.
</p>
<h4>XML research site servlet</h4>
<p>
	Responds to <tt>HTTP GET</tt> requests for research site contact data for a particular trial
	by extracting the trial's data from the cached trial repository.
	Provides last-modified information to support browser caching.
</p>

<h3>Facelets <code>[view/*.xhtml]</code></h3>
<h4>View Reuse</h4>
<p>
	Seam's recommends the <a href="https://facelets.dev.java.net/">Facelets view technology</a> 
	over JavaServer Pages (JSP) as Facelets supports templating and is a better fit for the 
	object-oriented backend components. 
	While some view definition is accomplished using server-side includes, 
	most reuse of the view is employed via three Facelet templates:
</p>
<ol>
	<li><code>/template.xhtml</code>: This master template lays out the header 
	(with logo and pull-down navigation menus) and footer.
	It also employs <a href="http://analytics.google.com">Google Analytics</a> to track visitor usage.</li>
	<li><code>/template-secondary.xhtml</code>: This secondary template inherits the master template
	and lays out left-navigation links and a central area for static (a.k.a. "public") content.</li>
	<li><code>/template-app.xhtml</code>: This secondary template inherits the master template
	and lays out an area for left-navigation and a central area for the dynamic (a.k.a. "application") content.</li>
</ol>
<h4>PDF reports</h4>
<p>
	BCT.org employs Seam's <a href="http://www.lowagie.com/iText/">iText PDF library</a> tags 
	to serve patient history and trial listing reports in PDF format.
</p>
<h4>Internationalization</h4>
<p>
	Internationalization of BCT.org is specified as a low-priority feature
	due to the challenges of translating medical terminology into patient-friendly language.
	BCT.org mixes text between the view pages and a resource bundle.
	The view pages hold English static content, instructions and data-element names 
	while the resource bundle holds data-element values, descriptions and messages.
</p>
<h4>JSF Components and tag libraries</h4>
<p>
	BCT.org uses Facelets, Java Standard Tag Library (JSTL) and JavaServer Faces (JSF) components
	and their respective tag libraries to display server state and collect user input. 
	Because Facelets (e.g., <code>ui:</code>) components are executed at run-time, 
	they are preferred to JSTL components (e.g., <code>c:</code>) which are executed at build-time.
	However, there are several instances where build-time components are necessary 
	or the <code>c:choose</code> component is more efficient. 
</p>
<h4>Cascading Style Sheets (CSS)</h4>
<p>
	BCT.org's style sheets were created by a graphics designer for a static prototype.
	Applying the CSS techniques to the Facelets view was a significant effort as the prototype 
	did not describe all combinations of layout and page elements. 
	The following sheets are employed:
</p>
<ul>
	<li><code>/css/screen.css</code>: Styles used in the master template.</li>
	<li><code>/css/application.css</code>: Styles used in the application sub-template.</li>
	<li><code>/css/contact.css</code>: Styles used in the JavaScript overlay.</li>
</ul>
<h4>JavaScript animation</h4>
<p>
	BCT.org employs the <a href="http://script.aculo.us/">script.aculo.us</a> JavaScript library
	to provide show/hide animation effects to 
	patient forms, trial listings, frequently asked questions, etc.
</p>
<h4>JavaScript pull-down navigation</h4>
<p>
	BCT.org employs the <a href="http://developer.yahoo.com/yui/">Yahoo UI</a> JavaScript library
	to provide pull-down navigation menus in the page header.
	The menus must be built using HTML rather than JavaScript 
	to support Seam's requirement for rewriting links.
</p>
<h4>JavaScript tabs</h4>
<p>
	BCT.org employs the <a href="http://developer.yahoo.com/yui/">Yahoo UI</a> JavaScript library
	to provide tabs for the patient health history forms
	and the research site message service invitation.
</p>
<h4>JavaScript overlays</h4>
<p>
	BCT.org employs the <a href="http://developer.yahoo.com/yui/">Yahoo UI</a> JavaScript library
	to provide a model dialog overlay for the research site contact information.
</p>
<h4>Google Map</h4>
<p>
	BCT.org employs the <a href="http://code.google.com/apis/maps/">Google map</a> JavaScript library
	to display research site locations.
	BCT.org has registered three keys for the map API, one for each server: 
	<tt>www</tt>, <tt>test</tt> and <tt>deploy</tt>.
	The research site location information is served from the trial data cache 
	using a custom servlet that supports last-modified browser caching.
</p>

<h3>Database [postgresql]</h3>
<p>
	PostgreSQL was chosen as the database because it is mature, robust, open-source and has a JDBC driver. 
	The expected load on the BCT.org website does not require sophisticated configurations 
	such as clustering, load-balancing or high availability.
	Table-level access-control permissions are defined using roles.
	Each user and system process (e.g., JBoss datasource) is assigned a database login which belongs to one or more roles.
</p>
<p>
	The <tt>pdq</tt> schema holds trial data parsed from the National Cancer Institute (NCI)'s 
	Physician's Data Query (PDQ) repository. 
	This schema is only accessed by the server process that periodically refreshes trial data
	from the PDQ data feed.
	The <tt>public</tt> schema holds all of the real-time data for BCT.org.
	The tables are divided into three logical categories:
</p>
<ol>
	<li>Trial data are specific to a trial, research sites and contact information.
		The trial data does not have dependencies on any other data category.</li>
	<li>Screener data are specific to research-site personnel.
		These tables are associated with those in the trial data category.</li>
	<li>Patient data are specific to a patient.
		These tables are associated with those in the trial data 
		and screener data categories.</li>
</ol>

<h3>Trial data cache</h3>
<p>
	Because trial data -- including research site and contact information -- 
	is frequently accessed by several components, it is loaded into a custom read-only cache.
	The trial data cache consumes about 400MB to hold 60 trials and 1200 research sites,
	well within the hardware capacity of recently-built servers.
</p>
<p>
	The read-only data is cached in Seam's application context 
	and accessed by components via Seam dependency injection.
	An EJB3 timer service periodically refreshes the cache from the database.
</p>

<h3>Messaging [smtp]</h3>
<p>
	A Simple Mail Transport Protocol (SMTP) server is setup as a JBoss resource 
	and used to send on-line messages. 
	The online messaging feature is implemented by the <code>MessageSenderAction</code> class
	which sends transient messages encapsulated by a <code>SimpleMessage</code> data transfer object.
	The off-line (e.g., batch) messaging features are implemented in a separate EJB 3.0 Enterprise Application
	which employs templating technology to send persisted data and records when each message was sent.
</p>
<p>
	The on-line messaging features are:
</p>
<ol>
	<li><strong>Contact Us</strong> web page allows users to send e-mail to help-desk personnel.</li> 
	<li><strong>Forgot password</strong> feature allows users to reset and send their password to themselves.</li>
</ol>
<p>
	Two roles defined by the on-line messaging are fulfilled using e-mail addresses:
</p>
<ol>
	<li><em>Bounce-back</em> address is used as the <tt>from</tt> address when sending messages.
		Undeliverable messages will be collected in this address' in-box.</li> 
	<li><em>Help-desk</em> address is used as the <tt>to</tt> address for Contact Us messages
		and as the <tt>reply-to</tt> address for password-reset messages.
		Therefore any messages delivered to this address' in-box are from actual users.</li>
</ol>

</body>
</html>