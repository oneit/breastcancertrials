// Breast Cancer Trials.org tab functionality uses YUI library
	var tabView;
	function BCT_tab_init() {
	    // If tab parameter specified then set it; defaults to first tab
	    var initialTab = 0;
	    var queryString = window.top.location.search.substring(1);
	    var hashString;
	    if (queryString.length > 0) {
	      var begin = queryString.indexOf('tab=');
	      if (begin >= 0) {
	        begin += 4;
	        var end = queryString.indexOf("&", begin);
	        if (end < 0)
	           end = queryString.length;
	        initialTab = parseInt(unescape(queryString.substring(begin, end)));
	      }
	      var begin = queryString.indexOf('#');
	      if ((begin >= 0) && ((begin + 1) < queryString.length)) {
	        hashString = unescape(queryString.substring(begin + 1, queryString.length));
	      }
	    }
	    tabView = new YAHOO.widget.TabView('tabcontainer');
	    tabView.set('activeIndex', initialTab);
	    // This next code fixes FireFox's scrolling problem
	    if (hashString != null) {
	      window.location.hash = hashString;
	    }
	}
	BCT_tab_init();

	function advancetab() {
		var initialTab = tabView.get('activeIndex');
		tabView.set('activeIndex', initialTab + 1);
		return false;
	};
