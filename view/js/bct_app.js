// Please note that this function is for use only with the older javascript libraries.
function ar(el,ops) { // animate radio click
        var op=ops.split(',');
        for (var i=0;i<op.length;i++) {
                var opa=op[i].split(':');
                var d=document.getElementById(el.name+opa[0]+'_d');
                if (null==d) continue;
                var v=opa[opa.length-1].split('+');
                var av=false;
                for (var j=0;j<v.length;j++)
                        if (el.value==v[j]) {av=true;break;}
                if (av) {
                        if(d.style.display=='none') Effect.PhaseIn(d);
                        continue;
                }
                if(d.style.display!='none') Effect.PhaseOut(d);
                cl(d);
        }
}

//Please note that this function will not work properly unless JQuery.js is loaded as well.
//This is used for radio buttons that share a common info div.
//
function flip_radio_info_div_new(el,ops,info_id)
{
	var op=ops.split(',');
	for (var i=0; i< op.length; i++)
	{
		var el_id='#' + el.name + op[i];
		if ($(el_id).is(':checked'))
		{
			$(info_id).slideDown(1500);
			break;
		}
		else
		{	
			$(info_id).slideUp(1500);
		}
	}
}

function flip_radio_info_div(el,ops,info_id)
{
	var info_div = document.getElementById(info_id);
	var op=ops.split(',');
	for (var i=0; i< op.length; i++)
	{
		var el_option=document.getElementById(el.name + op[i]);
		if (el_option.checked)
		{
			if (info_div.style.display == 'none')
			{
				Effect.PhaseIn( info_id );
			}	
			return;
		}
	}
	if (info_div.style.display != 'none')
	{	
		Effect.PhaseOut( info_id );
	}	
}

// Please note that this function will not work properly unless JQuery.js is loaded as well.
//
function ar_new(el,ops) { // animate radio click

	var op=ops.split(',');
	for (var i=0;i<op.length;i++) {
		var opa=op[i].split(':');
		var d=document.getElementById(el.name+opa[0]+'_d');
		
		if (null == d  && windows.frames.length > 0)
		{
			// try to find the element under the first iframe
			d = window.frames[0].document.getElementById(el.name+opa[0]+'_d');
		}	
		
		// if d is still null then continue
		if (null==d)
		{
			continue;
		}	
		var v=opa[opa.length-1].split('+');
		var av=false;
		for (var j=0;j<v.length;j++)
		{	
			if (el.value==v[j]) 
				{
					av=true;break;
				}
		}
		
		if (av) 
		{
			if(d.style.display=='none')
			{
				$('[id=' + d.id +']').fadeIn(1500);
			}	
			continue;
		}
		if(d.style.display!='none')
		{	
			$('[id=' + d.id +']').fadeOut(1500);
		}	
		cl(d);
	}
}

function ac(el) { // animate checkbox click
    var d=document.getElementById(el.id+'_d');
    if (null==d) return;
    if (el.checked) {
            if(d.style.display=='none') Effect.PhaseIn(d);
            return;
    }
    if(d.style.display!='none') Effect.PhaseOut(d);
    cl(d);
}

function ac_new(el) { // animate checkbox click
	var d=document.getElementById(el.id+'_d');
	if (null==d) return;
	if (el.checked) {
		if(d.style.display=='none') $('[id=' + d +']').fadeIn(1500);
		return;
	}
	if(d.style.display!='none') $('[id=' + d +']').fadeOut(1500);
	cl(d);
}

function cl(dv) { // clear input elements
	var inp=dv.getElementsByTagName('input')
	for (var j=0;j<inp.length;j++)
		switch (inp[j].type) {
			case 'radio':
			case 'checkbox': inp[j].checked=false; break;
			case 'password':
			case 'text': inp[j].value=''; break;
		}
}

function ct(el,dv) { // clear input text div when checkbox cleared
	var d=document.getElementById(dv);
	if ((null!=d) && ('text'==d.type) && (!el.checked)) d.value='';
}

function findContentDiv(classname) 
{
	var d=document.getElementById(classname);
	
	if (d != null)
	{
		return d;
	}	

	var myclass = new RegExp('\\b' + classname +'\\b');
	var elem = document.getElementsByTagName('div');
	for (var i = 0; i < elem.length; i++) 
	{
		var classes = elem[i].className;
		if (myclass.test(classes))
		{	
			return elem[i];
		}	
	}
	return null;
};

function sizeTextLarger(el) 
{
	var d=findContentDiv('ContentFramedBox');
	if (null==d)
	{	
		return;
	}	
	d.style.fontSize = "130%";
}

function sizeTextSmaller(el) 
{
	var d=findContentDiv('ContentFramedBox');
	if (null==d) return;
	d.style.fontSize = "";
//	d.style.fontSize = null;
}

function fzip() {
	if (document.zip.Ecom_ReceiptTo_Postal_PostalCode.focus) document.zip.Ecom_ReceiptTo_Postal_PostalCode.focus();
}

function avonZip() {
	if (document.zipForm.zip.focus) document.zipForm.zip.focus();
}

/**
 * Validate the zip code with cross-field validation on the groupedBy type.
 * @param groupedBy
 * @param zip
 * @param errdiv
 * @return true if all goes well, false if the field does not validate.
 */
function validatePostalCode(zip, errdiv, clearPostal)
{
        var postalCode   = document.forms.zip.elements[zip];
        var errorDiv     = document.getElementById( errdiv );

        if (postalCode === null || postalCode.value.length <= 0)
        {
        	errorDiv.style.display = '';
               	errorDiv.innerHTML = 'The zip field is required if sort by zip code is selected.';
      		return false;
	}
	else if (!postalCode.value.match(/^\d{1,5}$/))
	{
		errorDiv.style.display = '';
		errorDiv.innerHTML = 'A zip code must consist of one to five numbers.';
		return false;
        }
        // clear the errors.
        errorDiv.innerHTML = '';
        errorDiv.style.display = 'none';
        return true;
}

/**
 * Validate the zip code with cross-field validation on the groupedBy type.
 * We are allowing empty or zero length zip codes.  We'll just ignore them on 
 * the back end.
 * @param groupedBy
 * @param zip
 * @param errdiv
 * @return true if all goes well, false if the field does not validate.
 */
function browseTrialsValidatePostalCode( zip, errdiv )
{
        var postalCode   = document.forms.zip.elements[zip];
        var errorDiv     = document.getElementById( errdiv );

        if ( !( postalCode === undefined || errorDiv === undefined ) && postalCode.value.length > 0 )
        {
        	if ( !postalCode.value.match(/^\d{1,5}$/) )
            {
        		errorDiv.style.display = '';
                errorDiv.innerHTML = 'A zip code must consist of one to five numbers.';
                return false;
            }
        	else
        	{
                // clear the errors.
                errorDiv.innerHTML = '';
                errorDiv.style.display = 'none';
        	}	
        }
        return true;
}

function prt() {
	var w = window.open('','','scrollbars=yes,resizable=yes,width=600,height=600');
	w.document.open("text/html");
	w.document.write('<html xmlns="http://www.w3.org/1999/xhtml"><head><title>');
	w.document.write(document.title);
	w.document.write('</title></head><body style="padding-left:20px;background-image:none;background-color:#FFFFFF;">');
	w.document.write(document.getElementById('contentMain').innerHTML);
	w.document.write('</body></html>');
	w.document.close();
	w.print();
}

function clearZipForm()
{
	var errorDiv     = document.getElementById( 'errormessages' );
	var groupedByProximity = document.getElementById('groupedByProximity');
	document.forms.zip.elements['Ecom_ReceiptTo_Postal_PostalCode'].value = '';
        // clear the errors.
        errorDiv.innerHTML = '';
        errorDiv.style.display = 'none';

	if (groupedByProximity != null)
        {
		groupedByProximity.value = '1';
        }
        return true;
}

function hideProgress()
{
	document.getElementById('loader').style.display = 'none';

	var winW = 630, winH = 460;
	if (document.body && document.body.offsetWidth) {
	 winW = document.body.offsetWidth;
	 winH = document.body.offsetHeight;
	}
	if (document.compatMode=='CSS1Compat' &&
	    document.documentElement &&
	    document.documentElement.offsetWidth ) {
	 winW = document.documentElement.offsetWidth;
	 winH = document.documentElement.offsetHeight;
	}
	if (window.innerWidth && window.innerHeight) {
	 winW = window.innerWidth;
	 winH = window.innerHeight;
	}
	document.getElementById('framediv').style.height = (winH + 10) + 'px';
	document.getElementById('form_history_iframe').style.height = winH + 'px';
}

