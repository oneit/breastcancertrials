﻿/// <summary>
///   This JavaScript provides common functions for the menu 
///   and other dynamic navigation functionality.
/// </summary>
/// <history>
///  <change author="Konrad Kyc" copyright="2011 © Vantagesoft.ca" date="2011-01-19">Creation</change>
/// </history>

// Define the Name Spaces
var BCT = {};
BCT.Home = {};
BCT.FindATrial = {};

// Virtual Functions
BCT.PageOnLoad = function () { };

// When the Document Loads, call the initialize on load function
$(document).ready(function() {
    BCT.InitializeOnLoad();
});

/// <summary>
///  Initialize function that loads on PageLoad.
/// </summary>
BCT.InitializeOnLoad = function () {
    // Setup the menu
    BCT.SetupMainMenu();

    try {
        // Initialize the Photo Gallery pop-up (on-click)
        $(".PopupWindow").fancybox({
            'width': 950,
            'height': 465,
            'overlayShow': true,
            'cyclic': false,
            'scrolling': 'no',
            'autoScale': false,
            'padding': 10,
            'transitionIn': 'fade',
            'transitionOut': 'fade',
            'type': 'iframe'
        });
    }
    catch (ex) { }

    // Call the Page-On Load function (which may be specific to each page).
    BCT.PageOnLoad();
};

/// <summary>
///  Method which sets up the main menu naviagtion
///  and its animations.
/// </summary>
BCT.SetupMainMenu = function () {
    $(".MainMenuList li[class^='MenuItem'] a span span").mouseover(function () {

        var selectedMenuItems = $(".MenuItemSelected");
        var currentMenuItemSelected = false;
        var menuListItem = $(this).parent().parent().parent();

        // Traverse through all the menu items and determine whether to hide visible items
        for (var i = 0; i < selectedMenuItems.length; i++) {
            if ($(selectedMenuItems[i]).attr("class") != menuListItem.attr("class")) {
                var itemClasses = ($(selectedMenuItems[i]).attr("class")).split(' ');

                // If the item has more than one class defined, then remove all of them except this one.
                if (itemClasses.length > 1) {
                    $(selectedMenuItems[i]).attr("class", itemClasses[0]);

                    // Traverse through the item classes and determine whether to remove it based on the menu selection
                    for (var j = 1; j < itemClasses.length; j++) {
                        if (itemClasses[j].indexOf('-Expanded') == -1 && itemClasses[j] != "MenuItemSelected")
                            $(selectedMenuItems[i]).addClass(itemClasses[j]);
                    }
                }

                // Fade the other menu item out
                $(".SubMenuContainer").fadeOut("fast");
            }
            else
                currentMenuItemSelected = true;
        }

        // Make sure that the item selected is not the current menu expanded
        if (!currentMenuItemSelected) {
            var itemClass = menuListItem.attr("class").split(' ')[0];

            // Display and change the state of the menu to indicate that it's expanded
            $(".SubMenuContainer", menuListItem).fadeIn("fast");
            menuListItem.addClass(itemClass + "-Expanded");
            menuListItem.addClass("MenuItemSelected");
        }

        // Attach a Mouse-Out event to hide the expanded menu if the mouse isn't over it
    }).mouseout(function (e) {
        var menuListItem = $(this).parent().parent();
        var subMenu = $(".SubMenuContainer", menuListItem);

        // Determine whether to hide the Sub-Menu
        ValidateSubMenuVisibility(menuListItem, subMenu, e);
    });

    // Attach a mouse-out event to the Sub-Menu container to hide the expanded menu if the mouse isn't over it
    $(".SubMenuContainer").mouseout(function (e) {
        var menuListItem = $(this).parent();
        var subMenu = $(this);

        // Determine whether to hide the Sub-Menu
        ValidateSubMenuVisibility(menuListItem, subMenu, e);
    });

    // Private function to determine whether the Sub-Menu should be shown or hidden (depending on the mouse position)
    function ValidateSubMenuVisibility(menuListItem, subMenu, mouseEvent) {
        var isNotInRange = false;
        var isRightAligned = false;
        var offsetPosition = subMenu.offset();
        var xStart = offsetPosition.left;
        var xEnd = offsetPosition.left + subMenu.width();
        var yStart = offsetPosition.top;
        var yEnd = offsetPosition.top + subMenu.height();
        var xMainMenuEnd = offsetPosition.left + menuListItem.width();
        var yMainMenuEnd = offsetPosition.top + menuListItem.height();

        if ($(subMenu).hasClass("SubMenuContainer-Right")) {
            isRightAligned = true;
        }

        // If the mouse is not within the Sub-Menu, then hide it.
        if (mouseEvent.pageX < xStart || mouseEvent.pageX > xEnd || mouseEvent.pageY < yStart || mouseEvent.pageY > yEnd) {
            isNotInRange = true;
        }
        else if (!isRightAligned && mouseEvent.pageX >= xMainMenuEnd && mouseEvent.pageY <= yMainMenuEnd) {
            isNotInRange = true;
        }
        else if (isRightAligned && mouseEvent.pageX <= (xStart + 80) && mouseEvent.pageY <= yMainMenuEnd) {
            isNotInRange = true;
        }

        if (isNotInRange) {
            //alert(mouseEvent.pageX + " < " + xStart);
            var itemClasses = (menuListItem.attr("class")).split(' ');

            // If the item has more than one class defined, then remove all of them except this one.
            if (itemClasses.length > 1) {
                menuListItem.attr("class", itemClasses[0]);

                // Traverse through the item classes and determine whether to remove it based on the menu selection
                for (var i = 1; i < itemClasses.length; i++) {
                    if (itemClasses[i].indexOf('-Expanded') == -1 && itemClasses[i] != "MenuItemSelected")
                        $(menuListItem).addClass(itemClasses[i]);
                }
            }

            // Hide the Sub-Menu
            subMenu.fadeOut("fast");
        }
    }
};

/// <summary>
///  Method which sets up the collapsible items
/// </summary>
BCT.SetupCollapsibleItems = function () {
    $(".ExpandableItemHeader").click(function () {
        // If it's already expand it, then collapse it
        if ($(this).hasClass("ExpandableItemHeader-Expanded")) {
            $(".ExpandableItemText", $(this).parent()).slideUp("normal");
            $(this).removeClass("ExpandableItemHeader-Expanded");
            $(".ExpandableTextContinuedDots", $(this).parent()).show();
        }
        else {
            $(".ExpandableItemText", $(this).parent()).slideDown("normal");
            $(this).addClass("ExpandableItemHeader-Expanded");
            $(".ExpandableTextContinuedDots", $(this).parent()).hide();
        }
    });

    // Expand All Items button click
    $(".FAQExpandAll").click(function () {
        var expandableItems = $(".ExpandableItemHeader");

        for (var i = 0; i < expandableItems.length; i++) {
            if (!$(expandableItems[i]).hasClass("ExpandableItemHeader-Expanded")) {
                $(".ExpandableItemText", $(expandableItems[i]).parent()).slideDown("normal");
                $(expandableItems[i]).addClass("ExpandableItemHeader-Expanded");
                $(".ExpandableTextContinuedDots", $(expandableItems[i]).parent()).hide();
            }
        }
    });
}

/// <summary>
///  Method which sets up the slideshow items.
/// </summary>
BCT.SetupSlideShowItems = function () {
    var slides = $(".SlideShowSlide");
    
    if (slides.length > 0) {
        $(slides[0]).show();

        // Set Previous Button on-click event
        $(".SlideShowPreviousButton").click(function () {
            var selectableSlides = $(".SlideShowSlide");

            for (var i = 0; i < selectableSlides.length; i++) {
                // Check which slide is the currently visible slide
                if ($(selectableSlides[i]).is(':visible')) {
                    $(selectableSlides[i]).fadeOut("normal", function () {
                        // Select the previous slide
                        if (i > 0) {
                            $(selectableSlides[i - 1]).fadeIn("normal");
                        }
                        // Select the Last slide
                        else {
                            $(selectableSlides[selectableSlides.length - 1]).fadeIn("normal");
                        }
                    });

                    break;
                }
            }
        });

        // Set Previous Button on-click event
        $(".SlideShowNextButton").click(function () {
            var selectableSlides = $(".SlideShowSlide");

            for (var i = 0; i < selectableSlides.length; i++) {
                // Check which slide is the currently visible slide
                if ($(selectableSlides[i]).is(':visible')) {
                    $(selectableSlides[i]).fadeOut("normal", function () {
                        // Select the next slide
                        if (i < selectableSlides.length - 1) {
                            $(selectableSlides[i + 1]).fadeIn("normal");
                        }
                        // Select the First slide
                        else {
                            $(selectableSlides[0]).fadeIn("normal");
                        }
                    });

                    break;
                }
            }
        });
    }
};

/// <summary>
///  Method which sets upt the Home POV Navigation by creating
///  the dot navigation items based on the number of the defined
///  POV images.
/// </summary>
/// <param name="numberOfPOVImages">The number of defined POV images.</param>
BCT.Home.SetupPOVNavigation = function (numberOfPOVImages) {
    var povNavContainer = $(".POVImageNavigation");
    var preloadedImages = [];

    // Create a defined number of Dot navigation items
    for (var i = 0; i < numberOfPOVImages; i++) {
        var navItem = "";

        // If this is the first item, then automatically select it
        if (i == 0)
            navItem = '<div class="POVImageNavigationItem POVImageNavigationItem-Selected"></div>';
        else
            navItem = '<div class="POVImageNavigationItem"></div>';

        // Append the newely created Navigation item
        povNavContainer.append(navItem);
    }

    // Add a clear element at the end of the navigation dots
    povNavContainer.append('<div style="clear: both;"></div>');

    // Attach a click handler to the POV Navigation items
    $(".POVImageNavigationItem").click(function () {
        BCT.Home.SelectPOV(this);

        // Clear the automatic POV change after the user clicks the button
        clearInterval(BCT.Home.POVTimer);
    });
};

/// <summary>
///  Method which selects the specified POV. This method
///  first determines which POV was selected, and then
///  calls a separate to change the POV according to
///  the selected index.
/// </summary>
/// <param name="context">The context object that was selected.</param>
BCT.Home.AutoChangePOV = function () {
    var povNavContainer = $(".POVImageNavigationItem");

    // Create a defined number of Dot navigation items
    for (var i = 0; i < povNavContainer.length; i++) {
        if (BCT.Home.POVCurrentIndex == i) {
            BCT.Home.SelectPOV(povNavContainer[i]);
        }
    }

    // Reset the index if it's at the end
    if (BCT.Home.POVCurrentIndex >= (povNavContainer.length - 1))
        BCT.Home.POVCurrentIndex = 0;
    else
        BCT.Home.POVCurrentIndex++;
}

/// <summary>
///  Method which selects the specified POV. This method
///  first determines which POV was selected, and then
///  calls a separate to change the POV according to
///  the selected index.
/// </summary>
/// <param name="context">The context object that was selected.</param>
BCT.Home.SelectPOV = function (context) {
    var povNavItems = $(".POVImageNavigationItem");
    $(povNavItems).removeClass("POVImageNavigationItem-Selected");
    $(context).addClass("POVImageNavigationItem-Selected");

    // Traverse through all the POV items to determine which one was selected.
    for (var i = 0; i < povNavItems.length; i++) {
        var currentItem = povNavItems[i];
        
        // If the current item matches the context object, then call
        // the external method with the current index
        if (currentItem == context) {
            BCT.Home.ChangePOV(i);
        }
    }
};

/// <summary>
///  Method which changes the POV based on the specified Index
/// </summary>
/// <param name="index">The index of the POV that is to be selected.</param>
BCT.Home.ChangePOV = function (index) {
    // Fade-Out the old POV, and fade-In the selected one
    $(".POVSlide:visible").fadeOut("slow", function () {
        var nextSlide = $(".POVSlide")[index];
        $(nextSlide).fadeIn("slow");
    });
};

/// <summary>
///  Method which pauses the POV Animation
/// </summary>
BCT.Home.PausePOVAnimation = function () {
    clearInterval(BCT.Home.POVTimer);
}

/// <summary>
///  Method which restarts the POV Animation
/// </summary>
BCT.Home.RestartPOVAnimation = function () {
    BCT.Home.POVTimer = setInterval("BCT.Home.AutoChangePOV()", 10000);
}

/// <summary>
///  Method auto changes the Collaborators Images
/// </summary>
BCT.Home.AutoChangeCollaboratorsImage = function () {
    // Retrieve the proper image
    var currentImage = BCT.Home.Collaborators[BCT.Home.CollaboratorsCurrentIndex];

    $(".CollaboratorsImage").fadeOut("normal", function () {
        $("img", $(this)).attr("src", currentImage.ImagePath);
        $("img", $(this)).attr("alt", currentImage.AlternateText);
    }).fadeIn("normal");

    if (BCT.Home.CollaboratorsCurrentIndex < BCT.Home.Collaborators.length - 1) {
        // Increase the counter
        BCT.Home.CollaboratorsCurrentIndex++
    }
    else {
        // Reset the counter
        BCT.Home.CollaboratorsCurrentIndex = 0;
    }
};

/// <summary>
///  Method to setup the Email Address Text box
/// </summary>
BCT.SetupYourEmailAddressTextbox = function () {
    $(".InputTextYourEmailAddress").val("Your Email");

    $(".InputTextYourEmailAddress").click(function () {
        if ($(this).val() == "Your Email")
            $(this).val("");
    }).blur(function () {
        if ($(this).val() == "")
            $(this).val("Your Email");
    });
};

/// <summary>
///  Method to setup the Email Address Text box
/// </summary>
BCT.SetupFriendsEmailAddressTextbox = function () {
    $(".InputTextFriendsEmailAddress").val("Friend's Email");

    $(".InputTextFriendsEmailAddress").click(function () {
        if ($(this).val() == "Friend's Email")
            $(this).val("");
    }).blur(function () {
        if ($(this).val() == "")
            $(this).val("Friend's Email");
    });
};

/// <summary>
///  Method to setup the Your Name Text box
/// </summary>
BCT.SetupYourNameTextbox = function () {
    $(".InputTextYourName").val("Your Name");

    $(".InputTextYourName").click(function () {
        if ($(this).val() == "Your Name")
            $(this).val("");
    }).blur(function () {
        if ($(this).val() == "")
            $(this).val("Your Name");
    });
};

/// <summary>
///  Method to setup the Friends Name Text box
/// </summary>
BCT.SetupFriendsNameTextbox = function () {
    $(".InputTextFriendsName").val("Friend's Name");

    $(".InputTextFriendsName").click(function () {
        if ($(this).val() == "Friend's Name")
            $(this).val("");
    }).blur(function () {
        if ($(this).val() == "")
            $(this).val("Friend's Name");
    });
};

/// <summary>
///  Method to setup the Subject Text box
/// </summary>
BCT.SetupSubjectTextbox = function () {
    $(".InputTextSubject").val("Subject");

    $(".InputTextSubject").click(function () {
        if ($(this).val() == "Subject")
            $(this).val("");
    }).blur(function () {
        if ($(this).val() == "")
            $(this).val("Subject");
    });
};


/// <summary>
///  Method to setup the MessageText Area
/// </summary>
BCT.SetupMessageTextbox = function () {
    $(".InputTextAreaMessage").val("Message");

    $(".InputTextAreaMessage").click(function () {
        if ($(this).val() == "Message")
            $(this).val("");
    }).blur(function () {
        if ($(this).val() == "")
            $(this).val("Message");
    });
};

/// <summary>
///  Method to setup the Zip Code Text box
/// </summary>
BCT.FindATrial.SetupZipCodeTextbox = function () {
    $(".InputTextBoxZipCode").val("Zip Code");

    $(".InputTextBoxZipCode").click(function () {
        if ($(this).val() == "Zip Code")
            $(this).val("");
    }).blur(function () {
        if ($(this).val() == "")
            $(this).val("Zip Code");
    });
};
