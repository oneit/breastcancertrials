/// <summary>
///  Method which sets up the collapsible items
/// </summary>
BCT.SetupSlowCollapsibleItems = function () {
    $(".ExpandableItemHeader").click(function () {
        // If it's already expand it, then collapse it
        if ($(this).hasClass("ExpandableItemHeader-Expanded")) {
            $(".ExpandableItemText", $(this).parent()).slideUp("normal");
            $(this).removeClass("ExpandableItemHeader-Expanded");
            $(".ExpandableTextContinuedDots", $(this).parent()).show(5000);
        }
        else {
            $(".ExpandableItemText", $(this).parent()).slideDown("normal");
            $(this).addClass("ExpandableItemHeader-Expanded");
            $(".ExpandableTextContinuedDots", $(this).parent()).hide(5000);
        }
    });

    // Expand All Items button click
/*    
    $(".FAQExpandAll").click(function () {
        var expandableItems = $(".ExpandableItemHeader");

        for (var i = 0; i < expandableItems.length; i++) {
            if (!$(expandableItems[i]).hasClass("ExpandableItemHeader-Expanded")) {
                $(".ExpandableItemText", $(expandableItems[i]).parent()).slideDown(1500);
                $(expandableItems[i]).addClass("ExpandableItemHeader-Expanded");
                $(".ExpandableTextContinuedDots", $(expandableItems[i]).parent()).hide();
            }
        }
    });
*/    
}
